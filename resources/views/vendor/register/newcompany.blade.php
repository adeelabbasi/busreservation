

<body lang=EN-US link=blue vlink=purple style='tab-interval:.5in'>

<div class=Section1>

<center><img src="https://vpss.online/itravel/public/itravel_logo.png" v:shapes="Picture_x0020_1"></center>

<p class=MsoNoSpacing align=center style='text-align:center'><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNoSpacing align=center style='text-align:center'><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNoSpacing align=center style='text-align:center'><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNoSpacing align=center style='text-align:center'><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNoSpacing><span style='font-size:10.0pt;mso-bidi-font-size:11.0pt'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNoSpacing align=center style='text-align:center'><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNoSpacing align=center style='text-align:center'><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt'>Hi <b style='mso-bidi-font-weight:
normal'>{{ $info_Company->name }}<o:p></o:p></b></span></p>

<p class=MsoNoSpacing align=center style='text-align:center'><b
style='mso-bidi-font-weight:normal'><span style='font-size:10.0pt;mso-bidi-font-size:
11.0pt'><o:p>&nbsp;</o:p></span></b></p>

<p class=MsoNoSpacing align=center style='text-align:center'><b
style='mso-bidi-font-weight:normal'>Congratulations </b><span style='font-size:
10.0pt;mso-bidi-font-size:11.0pt'>for choosing to partner with I-travel.<o:p></o:p></span></p>

<p class=MsoNoSpacing><span style='font-size:10.0pt;mso-bidi-font-size:11.0pt'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNoSpacing align=center style='text-align:center'><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt'>Your Unique USSD Code is now
live on<o:p></o:p></span></p>

<p class=MsoNoSpacing align=center style='text-align:center'><b
style='mso-bidi-font-weight:normal'><span style='font-size:34.0pt;mso-bidi-font-size:
11.0pt;color:#7F7F7F;mso-themecolor:text1;mso-themetint:128'>*8014*UNIQUECODE#<o:p></o:p></span></b></p>

<p class=MsoNoSpacing align=center style='text-align:center'><span
style='font-size:12.0pt;mso-bidi-font-size:11.0pt'>Dial it now for a quick
test.<o:p></o:p></span></p>

<p class=MsoNoSpacing><span style='font-size:12.0pt;mso-bidi-font-size:11.0pt'>______________________________________________________________________________<o:p></o:p></span></p>

<p class=MsoNoSpacing><span style='font-size:12.0pt;mso-bidi-font-size:11.0pt'>To
properly set-up your operations on our platform, please find your login
credentials as shared below.<o:p></o:p></span></p>

<p class=MsoNoSpacing><span style='font-size:12.0pt;mso-bidi-font-size:11.0pt'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNoSpacing><span style='font-size:12.0pt;mso-bidi-font-size:11.0pt'>Login
URL: </span><a name="_GoBack"></a><span class=MsoHyperlink><span
style='font-size:13.0pt;mso-bidi-font-size:11.0pt'>https://vpss.online/itravel/public/login</span></span><span
style='font-size:13.0pt;mso-bidi-font-size:11.0pt'><o:p></o:p></span></p>

<p class=MsoNoSpacing><b style='mso-bidi-font-weight:normal'><span
style='font-size:12.0pt;mso-bidi-font-size:11.0pt'>Username: </span></b><span
style='font-size:12.0pt;mso-bidi-font-size:11.0pt'>{{ $info_Company->username }}<o:p></o:p></span></p>

<p class=MsoNoSpacing><b style='mso-bidi-font-weight:normal'><span
style='font-size:12.0pt;mso-bidi-font-size:11.0pt'>Password:</span></b><span
style='font-size:12.0pt;mso-bidi-font-size:11.0pt'>{{ $info_Company->password }}<o:p></o:p></span></p>

<p class=MsoNoSpacing><span style='font-size:12.0pt;mso-bidi-font-size:11.0pt'>______________________________________________________________________________<o:p></o:p></span></p>

<p class=MsoNoSpacing><span style='font-size:12.0pt;mso-bidi-font-size:11.0pt'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNoSpacing><span style='font-size:12.0pt;mso-bidi-font-size:11.0pt'>Not
to worry, our support team will help you all the way through the set-up

process. <o:p></o:p></span></p>

<p class=MsoNoSpacing><span style='font-size:12.0pt;mso-bidi-font-size:11.0pt'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNoSpacing><span style='font-size:12.0pt;mso-bidi-font-size:10.0pt;
color:#2C363A;background:white'>Kindly reply this mail with skype id and
contact no. of your technical team.</span><span style='font-size:12.0pt;
mso-bidi-font-size:11.0pt'><o:p></o:p></span></p>

<p class=MsoNoSpacing><span style='font-size:12.0pt;mso-bidi-font-size:11.0pt'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNoSpacing><span style='font-size:12.0pt;mso-bidi-font-size:11.0pt'>Welcome
onboard<o:p></o:p></span></p>

<p class=MsoNoSpacing><span style='font-size:12.0pt;mso-bidi-font-size:11.0pt'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNoSpacing><span style='font-size:12.0pt;mso-bidi-font-size:11.0pt'>Team
I-Travel<o:p></o:p></span></p>

<p class=MsoNoSpacing><span style='font-size:12.0pt;mso-bidi-font-size:11.0pt'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNoSpacing><span style='font-size:12.0pt;mso-bidi-font-size:11.0pt;
mso-no-proof:yes'><!--[if gte vml 1]><v:shapetype id="_x0000_t75" coordsize="21600,21600"
 o:spt="75" o:preferrelative="t" path="m@4@5l@4@11@9@11@9@5xe" filled="f"
 stroked="f">
 <v:stroke joinstyle="miter"/>
 <v:formulas>
  <v:f eqn="if lineDrawn pixelLineWidth 0"/>
  <v:f eqn="sum @0 1 0"/>
  <v:f eqn="sum 0 0 @1"/>
  <v:f eqn="prod @2 1 2"/>
  <v:f eqn="prod @3 21600 pixelWidth"/>
  <v:f eqn="prod @3 21600 pixelHeight"/>
  <v:f eqn="sum @0 0 1"/>
  <v:f eqn="prod @6 1 2"/>
  <v:f eqn="prod @7 21600 pixelWidth"/>
  <v:f eqn="sum @8 21600 0"/>
  <v:f eqn="prod @7 21600 pixelHeight"/>
  <v:f eqn="sum @10 21600 0"/>
 </v:formulas>
 <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
 <o:lock v:ext="edit" aspectratio="t"/>
</v:shapetype><v:shape id="Picture_x0020_1" o:spid="_x0000_i1025" type="#_x0000_t75"
 style='width:51pt;height:55.5pt;visibility:visible;mso-wrap-style:square'>
 <v:imagedata src="ussd%20code%20email%20from%20portal_files/image001.png"
  o:title="itravel_favicon"/>
</v:shape><![endif]--><![if !vml]><img width=68 height=74
src="https://vpss.online/itravel/public/itravel_favicon.png" v:shapes="Picture_x0020_1"><![endif]></span><span
style='font-size:12.0pt;mso-bidi-font-size:11.0pt'><o:p></o:p></span></p>

<p class=MsoNoSpacing><span style='font-size:10.0pt;mso-bidi-font-size:11.0pt'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNoSpacing><a href="mailto:Support@i-travel.ng"><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt'>Support@i-travel.ng</span></a><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt'><o:p></o:p></span></p>

<p class=MsoNoSpacing><a href="http://www.i-travel.ng"><span style='font-size:
10.0pt;mso-bidi-font-size:11.0pt'>www.i-travel.ng</span></a><span
style='font-size:10.0pt;mso-bidi-font-size:11.0pt'><o:p></o:p></span></p>

<p class=MsoNoSpacing><span style='font-size:10.0pt;mso-bidi-font-size:11.0pt'>M:
08164806744<o:p></o:p></span></p>

<p class=MsoNoSpacing><span style='font-size:10.0pt;mso-bidi-font-size:11.0pt'>W:
08099444309</span><b style='mso-bidi-font-weight:normal'><span
style='font-size:12.0pt;mso-bidi-font-size:11.0pt'><o:p></o:p></span></b></p>

<p class=MsoNormal><span style='font-size:13.0pt;mso-bidi-font-size:11.0pt;
line-height:115%'><o:p>&nbsp;</o:p></span></p>

</div>

 