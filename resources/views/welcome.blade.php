<!DOCTYPE html>
<html lang="en">
  <!--================================================================================
	Item Name: Materialize - Material Design Admin Template
	Version: 4.0
	Author: PIXINVENT
	Author URL: https://themeforest.net/user/pixinvent/portfolio
================================================================================ -->
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Bus resetvation system">
    <meta name="keywords" content="Bus resetvation system">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="base-url" content="{{ url('') }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Favicons-->
    <link rel="icon" href="{{url('itravel_favicon.png')}}" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="{{url('itravel_favicon.png')}}">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="{{url('itravel_favicon.png')}}">
    <!-- For Windows Phone -->
    <!-- CORE CSS-->
    <link href="{{ asset('material/css/themes/overlay-menu/materialize.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('material/css/themes/overlay-menu/style.css') }}" type="text/css" rel="stylesheet">
    <!-- CSS for Overlay Menu (Layout Full Screen)-->
    <link href="{{ asset('material/css/layouts/style-fullscreen.css') }}" type="text/css" rel="stylesheet">
    <!-- Custome CSS-->
    <link href="{{ asset('material/css/custom/custom.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('material/css/layouts/page-center.css') }}" type="text/css" rel="stylesheet">
    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="{{ asset('material/vendors/perfect-scrollbar/perfect-scrollbar.css') }}" type="text/css" rel="stylesheet">
  </head>
  <body class="cyan">
    <!-- Start Page Loading -->
 
    <!-- End Page Loading -->
    <div id="login-page" class="row">
      <div class="col s12 z-depth-4 card-panel">
        <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
        @csrf
          <div class="row">
            <div class="input-field col s12 center">
              <img src="{{url('itravel_logo.png')}}" alt="" class="">
              <p class="center login-form-text">Bus Reservation System Admin</p>
            </div>
          </div>
          @if ($message = Session::get('warning'))
                <div id="card-alert" class="card red">
                    <div class="card-content white-text">
                        <p>{{ $message }}</p>
                    </div>
                	<button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                	<span aria-hidden="true">×</span>
                	</button>
                </div>
          @endif
          <div class="row margin">
            <div class="input-field col s12">
              <i class="material-icons prefix pt-5">person_outline</i>
              <input id="identity" type="text" class="form-control{{ $errors->has('identity') ? ' is-invalid' : '' }}" name="identity" value="{{ old('identity') }}" required="" autofocus>
			  @if ($errors->has('identity'))
					<span class="invalid-feedback" role="alert">
						<strong>{{ $errors->first('identity') }}</strong>
					</span>
			  @endif
              <label for="identity" class="center-align">Username/Email</label>
            </div>
          </div>
          <div class="row margin">
            <div class="input-field col s12">
              <i class="material-icons prefix pt-5">lock_outline</i>
              <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
              @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
              @endif
              <label for="password">Password</label>
            </div>
          </div>
          <div class="row">
            <div class="col s12 m12 l12 ml-2 mt-3">
              <input type="checkbox" id="remember-me" name="remember"/>
              <label for="remember-me">Remember me</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <button type="submit" class="btn waves-effect waves-light col s12">{{ __('Login') }}</button>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s6 m6 l6">
              <p class="margin right-align medium-small"><a href="{{ route('password.request') }}">Forgot password ?</a></p>
            </div>
          </div>
        </form>
      </div>
    </div>
    <!-- ================================================
    Scripts
    ================================================ -->
    <!-- jQuery Library -->
    <script type="text/javascript" src="{{ asset('material/vendors/jquery-3.2.1.min.js') }}"></script>
    <!--materialize js-->
    <script type="text/javascript" src="{{ asset('material/js/materialize.min.js') }}"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="{{ asset('material/vendors/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="{{ asset('material/js/plugins.js') }}"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="{{ asset('material/js/custom-script.js') }}"></script>
  </body>
</html>