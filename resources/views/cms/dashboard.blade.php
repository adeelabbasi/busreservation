@extends('cms.layout.app')
@section('breadcrumbs')
<div id="breadcrumbs-wrapper">
   <div class="container">
      <div class="row">
         <div class="col s10 m6 l6">
            <h5 class="breadcrumbs-title">Dashboard</h5>
            <ol class="breadcrumbs">
               <li><a href="{{ url($guard_url) }}">Home</a>
               </li>
            </ol>
         </div>
         <div class="col s2 m6 l6">
            <a class="btn waves-effect waves-light breadcrumbs-btn right" href="{{ url('/admin') }}" data-activates="dropdown1">
            <i class="material-icons">home</i>
            </a>
            
         </div>
      </div>
   </div>
</div>
@endsection
@section('content')

<h5>REGISTRATION</h5>
<!-- main-menu start-->
@if($guard == "admin")
<!--card stats start-->
<div id="card-stats">
  <div class="row">
    <div class="col s12 m6 l3">
      <div class="card">
        <div class="card-content cyan white-text">
          <p class="card-stats-title">
            <i class="material-icons">account_balance</i> Registered Company</p>
          <h4 class="card-stats-number">{{ $data['CompanyCount'] }}</h4>
        </div>
        <div class="card-action cyan darken-1">
          <div id="clients-bar" class="center-align"></div>
        </div>
      </div>
    </div>
    <div class="col s12 m6 l3">
      <div class="card">
        <div class="card-content red accent-2 white-text">
          <p class="card-stats-title">
            <i class="material-icons">flag</i>Countries</p>
          <h4 class="card-stats-number">{{ $data['CountryCount'] }}</h4>
        </div>
        <div class="card-action red darken-1">
          <div id="sales-compositebar" class="center-align"></div>
        </div>
      </div>
    </div>
    <div class="col s12 m6 l3">
      <div class="card">
        <div class="card-content teal accent-4 white-text">
          <p class="card-stats-title">
            <i class="material-icons">location_city</i>Cities</p>
          <h4 class="card-stats-number">{{ $data['CityCount'] }}</h4>
        </div>
        <div class="card-action teal darken-1">
          <div id="profit-tristate" class="center-align"></div>
        </div>
      </div>
    </div>
    <div class="col s12 m6 l3">
      <div class="card">
        <div class="card-content deep-orange accent-2 white-text">
          <p class="card-stats-title">
            <i class="material-icons">content_copy</i> Lgas</p>
          <h4 class="card-stats-number">{{ $data['LgaCount'] }}</h4>
        </div>
        <div class="card-action  deep-orange darken-1">
          <div id="invoice-line" class="center-align"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--card stats end-->
@endif

<!--card widgets start-->
<div id="card-widgets">
  <div class="row">
    <?php 
	$chartColors = [
		'#4dc9f6',
		'#f67019',
		'#f53794',
		'#537bc4',
		'#acc236',
		'#166a8f',
		'#00a950',
		'#58595b',
		'#8549ba'
	];
	
	
	$totalCounts = 0; 
	$TerminalbackgroundColor = "";
	$TerminalborderColor = "";
	$TerminalLables = "";
	$TerminalCounts = "";
    foreach($data['TerminalReports'] as $TerminalReport) 
	{
		$TerminalbackgroundColor .= '"#FFFFFF",';
		$TerminalborderColor .=  '"#FFFFFF",';
		$TerminalLables .= '"'.$TerminalReport->name.'",';
		$TerminalCounts .= $TerminalReport->counts.',';
		$totalCounts += $TerminalReport->counts; 
	}
	?>
    <div class="col s12 m3 l3">
      <ul id="task-card" class="collection with-header">
        <li class="deep-orange accent-2 white-text">
          <h5 class="task-card-title">              
              <table style="border-bottom: 1px solid #FFFFFF;">
                <tr>
                    <td style="width: 15%; padding:0px; margin:-20px;">
                        <img width="100%" src="http://brs.test/material/images/icon/terminal.png">
                    </td>
                    <td style="text-align: left;">
                        Terminals
                    </td>
                    <td>
                        <span class="right">{{ $totalCounts }}</span>
                    </td>
                </tr>
              </table>
          </h5>
        </li>
        <li class="collection-item deep-orange accent-2 white-text">
          <!--<table class="bordered responsive-table display" style="background:#FFF">
          	<tr>
            	<th>Company</th>
                <th>Counts</th>
            </tr>
            <?php $totalCounts = 0; ?>
            @foreach($data['TerminalReports'] as $TerminalReport)
            <tr>
            	<td>{{ $TerminalReport->name }}</td>
                <td>{{ $TerminalReport->counts }}</td>
            </tr>
            <?php $totalCounts += $TerminalReport->counts; ?>
            @endforeach
            <tr>
            	<th>Total</th>
                <th>{{ $totalCounts }}</th>
            </tr>
          </table>-->
          <canvas id="TerminalChart"></canvas>
        </li>
      </ul>
    </div>
    <!--Fleet-->
    <?php 
	$totalCounts = 0; 
	$FleetbackgroundColor = "";
	$FleetborderColor = "";
	$FleetLables = "";
	$FleetCounts = "";
	$i=0;
    foreach($data['FleetReports'] as $FleetReport) 
	{
		$FleetbackgroundColor .= '"#FFFFFF",';
		$FleetborderColor .=  '"#FFFFFF",';
		$FleetLables .= '"'.$FleetReport->name.'",';
		$FleetCounts .= $FleetReport->counts.',';
		$totalCounts += $FleetReport->counts; 
		$i++;
	}
	?>
    <div class="col s12 m3 l3">
      <ul id="task-card" class="collection with-header">
        <li class="teal accent-4 white-text">
          <h5 class="task-card-title">
              <table style="border-bottom: 1px solid #FFFFFF;">
                <tr>
                    <td style="width: 15%; padding:0px; margin:-20px;">
                        <img width="100%" src="http://brs.test/material/images/icon/bus.png">
                    </td>
                    <td style="text-align: left;">
                        Fleets
                    </td>
                    <td>
                        <span class="right">{{ $totalCounts }}</span>
                    </td>
                </tr>
              </table>
          </h5>
        </li>
        <li class="collection-item teal accent-4 white-text">
          <canvas id="FleetChart"></canvas>
        </li>
      </ul>
    </div>
	
    <!--Route-->
    <?php 
	$totalCounts = 0; 
	$RoutebackgroundColor = "";
	$RouteborderColor = "";
	$RouteLables = "";
	$RouteCounts = "";
	$i=0;
    foreach($data['RouteReports'] as $RouteReport) 
	{
		$RoutebackgroundColor .= '"#FFFFFF",';
		$RouteborderColor .=  '"#FFFFFF",';
		$RouteLables .= '"'.$RouteReport->name.'",';
		$RouteCounts .= $RouteReport->counts.',';
		$totalCounts += $RouteReport->counts; 
		$i++;
	}
	?>
    <div class="col s12 m3 l3">
      <ul id="task-card" class="collection with-header">
        <li class="cyan accent-4 white-text">
          <h5 class="task-card-title">
              <table style="border-bottom: 1px solid #FFFFFF;">
                <tr>
                    <td style="width: 15%; padding:0px; margin:-20px;">
                        <img width="100%" src="http://brs.test/material/images/icon/route.png">
                    </td>
                    <td style="text-align: left;">
                        Routes
                    </td>
                    <td>
                        <span class="right">{{ $totalCounts }}</span>
                    </td>
                </tr>
              </table>
          </h5>
        </li>
        <li class="collection-item cyan accent-4 white-text">
          <canvas id="RouteChart"></canvas>
        </li>
      </ul>
    </div>
    
    <!--User-->
    <?php 
		$totalCounts = 0; 
		$UserbackgroundColor = "";
		$UserborderColor = "";
		$UserLables = "";
		$UserCounts = "";
		$i=0;
	    foreach($data['UserReports'] as $UserReport) 
	{
		$UserbackgroundColor .= '"#FFFFFF",';
		$UserborderColor .=  '"#FFFFFF",';
		$UserLables .= '"'.$UserReport->name.'",';
		$UserCounts .= $UserReport->counts.',';
		$totalCounts += $UserReport->counts; 
		$i++;
	}
	?>
    <div class="col s12 m3 l3">
      <ul id="task-card" class="collection with-header">
        <li class="red accent-2 white-text">
          <h5 class="task-card-title">
              <table style="border-bottom: 1px solid #FFFFFF;">
                <tr>
                    <td style="width: 15%; padding:0px; margin:-20px;">
                        <img width="100%" src="http://brs.test/material/images/icon/user.png">
                    </td>
                    <td style="text-align: left;">
                        Users
                    </td>
                    <td>
                        <span class="right">{{ $totalCounts }}</span>
                    </td>
                </tr>
              </table>
          </h5>
        </li>
        <li class="collection-item red accent-2 white-text">
          <canvas id="UserChart"></canvas>
        </li>
      </ul>
    </div>
    
  </div>
</div>
<!--card widgets end-->
<!--card widgets start-->
<div id="card-widgets">
  <div class="row">   
    
    <!--Schedule-->
    <?php 
		$totalCounts = 0; 
		$SchedulebackgroundColor = "";
		$ScheduleborderColor = "";
		$ScheduleLables = "";
		$ScheduleCounts = "";
		$i=0;
	    foreach($data['ScheduleReports'] as $ScheduleReport) 
	{
		$SchedulebackgroundColor .= '"#FFFFFF",';
		$ScheduleborderColor .=  '"#FFFFFF",';
		$ScheduleLables .= '"'.$ScheduleReport->name.'",';
		$ScheduleCounts .= $ScheduleReport->counts.',';
		$totalCounts += $ScheduleReport->counts; 
		$i++;
	}
	?>
    <div class="col s12 m3 l3">
      <ul id="task-card" class="collection with-header">
        <li class="purple accent-4 white-text">
          <h5 class="task-card-title">
              <table style="border-bottom: 1px solid #FFFFFF;">
                <tr>
                    <td style="width: 15%; padding:0px; margin:-20px;">
                        <img width="100%" src="http://brs.test/material/images/icon/schedule.png">
                    </td>
                    <td style="text-align: left;">
                        Schedules
                    </td>
                    <td>
                        <span class="right">{{ $totalCounts }}</span>
                    </td>
                </tr>
              </table>
          </h5>
        </li>
        <li class="collection-item purple accent-4 white-text">
          <canvas id="ScheduleChart"></canvas>
        </li>
      </ul>
    </div>
    
    <!--Schedule by week day-->
    <?php 
		$totalCounts = 0; 
		$SchedulebackgroundColor = "";
		$ScheduleborderColor = "";
		$ScheduleLables = "";
		$ScheduleMondayCounts = "";
		$ScheduleTuesdayCounts = "";
		$ScheduleWednesdayCounts = "";
		$ScheduleThursdayCounts = "";
		$ScheduleFridayCounts = "";
		$ScheduleSaturdayCounts = "";
		$ScheduleSundayCounts = "";
		
		$totalMonday = 0; 
		$totalTuesday = 0; 
		$totalWednesday = 0; 
		$totalThursday = 0; 
		$totalFriday = 0; 
		$totalSaturday = 0; 
		$totalSunday = 0;
		
		$i=0;
	    foreach($data['ScheduleByDayReports'] as $ScheduleByDayReport)
		{
			$SchedulebackgroundColor .= '"#FFFFFF",';
			$ScheduleborderColor .=  '"#FFFFFF",';
			$ScheduleLables .= '"'.$ScheduleByDayReport->name.'",';
			
			$ScheduleMondayCounts .= $ScheduleByDayReport->monday.',';
			$ScheduleTuesdayCounts .= $ScheduleByDayReport->tuesday.',';
			$ScheduleWednesdayCounts .= $ScheduleByDayReport->wednesday.',';
			$ScheduleThursdayCounts .= $ScheduleByDayReport->thursday.',';
			$ScheduleFridayCounts .= $ScheduleByDayReport->friday.',';
			$ScheduleSaturdayCounts .= $ScheduleByDayReport->saturday.',';
			$ScheduleSundayCounts .= $ScheduleByDayReport->sunday.',';
			
			
			$totalMonday += $ScheduleByDayReport->monday; 
			$totalTuesday += $ScheduleByDayReport->tuesday;  
			$totalWednesday += $ScheduleByDayReport->wednesday; ; 
			$totalThursday += $ScheduleByDayReport->thursday; 
			$totalFriday += $ScheduleByDayReport->friday; 
			$totalSaturday += $ScheduleByDayReport->saturday;  
			$totalSunday += $ScheduleByDayReport->sunday;  
			$i++;
		}
	?>
    
    <div class="col s12 m9 l9">
      <ul id="task-card" class="collection with-header">
        
        <li class="collection-item purple accent-2 white-text" style="overflow:auto">
          <table class="purple accent-2 white-text">
          	<tr>
                <th style="border-right: 1px solid #FFF; border-bottom: 1px solid #FFF;">Monday <b>({{ $totalMonday }})</b></th>
                <th style="border-right: 1px solid #FFF; border-bottom: 1px solid #FFF;">Tuesday<b>({{ $totalTuesday }})</b></th>
                <th style="border-right: 1px solid #FFF; border-bottom: 1px solid #FFF;">Wednesday<b>({{ $totalWednesday }})</b></th>
                <th style="border-right: 1px solid #FFF; border-bottom: 1px solid #FFF;">Thursday<b>({{ $totalThursday }})</b></th>
                <th style="border-right: 1px solid #FFF; border-bottom: 1px solid #FFF;">Friday<b>({{ $totalFriday }})</b></th>
                <th style="border-right: 1px solid #FFF; border-bottom: 1px solid #FFF;">Saturday<b>({{ $totalSaturday }})</b></th>
                <th style="border-bottom: 1px solid #FFF;">Sunday<b>({{ $totalSunday }})</b></th>
            </tr>
            <tr>
            	<td style="border-right: 1px solid #FFF;"><canvas id="ScheduleMondayChart" width="100%" height="100%"></canvas></td>
                <td style="border-right: 1px solid #FFF;"><canvas id="ScheduleTuesdayChart" width="100%" height="100%"></canvas></td>
                <td style="border-right: 1px solid #FFF;"><canvas id="ScheduleWednesdayChart" width="100%" height="100%"></canvas></td>
                <td style="border-right: 1px solid #FFF;"><canvas id="ScheduleThursdayChart" width="100%" height="100%"></canvas></td>
                <td style="border-right: 1px solid #FFF;"><canvas id="ScheduleFridayChart" width="100%" height="100%"></canvas></td>
                <td style="border-right: 1px solid #FFF;"><canvas id="ScheduleSaturdayChart" width="100%" height="100%"></canvas></td>
                <td style=""><canvas id="ScheduleSundayChart" width="100%" height="100%"></canvas></td>
            </tr>
          </table>
        </li>
      </ul>
    </div>
    
    
    <div class="col s12 m9 l9" style="display:none">
      <ul id="task-card" class="collection with-header">
        <li class="collection-header purple accent-2 white-text">
          <h6 class="task-card-title">Schedule by week day</h6>
        </li>
        <li class="collection-item">
          <table class="bordered responsive-table display" style="background:#FFF">
          	<tr>
            	<th>Company</th>
                <th>Monday</th>
                <th>Tuesday</th>
                <th>Wednesday</th>
                <th>Thursday</th>
                <th>Friday</th>
                <th>Saturday</th>
                <th>Sunday</th>
            </tr>
            <?php 
				$totalMonday = 0; 
				$totalTuesday = 0; 
				$totalWednesday = 0; 
				$totalThursday = 0; 
				$totalFriday = 0; 
				$totalSaturday = 0; 
				$totalSunday = 0; 
			?>
            @foreach($data['ScheduleByDayReports'] as $ScheduleByDayReport)
            <tr>
            	<td>{{ $ScheduleByDayReport->name }}</td>
                <td>{{ $ScheduleByDayReport->monday }}</td>
                <td>{{ $ScheduleByDayReport->tuesday }}</td>
                <td>{{ $ScheduleByDayReport->wednesday }}</td>
                <td>{{ $ScheduleByDayReport->thursday }}</td>
                <td>{{ $ScheduleByDayReport->friday }}</td>
                <td>{{ $ScheduleByDayReport->saturday }}</td>
                <td>{{ $ScheduleByDayReport->sunday }}</td>
            </tr>
            <?php 
				$totalMonday += $ScheduleByDayReport->monday; 
				$totalTuesday += $ScheduleByDayReport->tuesday;  
				$totalWednesday += $ScheduleByDayReport->wednesday; ; 
				$totalThursday += $ScheduleByDayReport->thursday; 
				$totalFriday += $ScheduleByDayReport->friday; 
				$totalSaturday += $ScheduleByDayReport->saturday;  
				$totalSunday += $ScheduleByDayReport->sunday;  
			?>
            @endforeach
            <tr>
            	<th>Total</th>
                <th>{{ $totalMonday }}</th>
				<th>{{ $totalTuesday }}</th>
				<th>{{ $totalWednesday }}</th> 
				<th>{{ $totalThursday }}</th>
				<th>{{ $totalFriday }}</th> 
				<th>{{ $totalSaturday }}</th> 
				<th>{{ $totalSunday }}</th>
            </tr>
          </table>
        </li>
      </ul>
    </div>
    
  </div>
</div>
<!--card widgets end-->


<!--card widgets start-->
<div id="card-widgets">
  <div class="row">
    <div class="col s12 m12 l12">
      <ul id="task-card" class="collection with-header">
        <li class="collection-header red accent-2 white-text">
          <h5>ACTIVITY/USAGE </h5>
        </li>
        <li class="collection-item">
          <div class="row">
             	<div class="col s12 m3 l3">
                  <select id="Report">
                      <option value="Customer" selected="selected">Customer</option>
                      <option value="Unique_Customer">Unique Customer</option>
                      <option value="Company_Requests_Report">Company Code Requests Report</option>
                      <option value="Shortcode_Strings_Report">Shortcode Strings Report</option>
                      <option value="Routes_Report">Routes Report</option>
                      <option value="Schedules_Report">Schedules Report</option>
                      <option value="USSD_Menu_General_Items_Report">USSD Menu General Items Report</option>
                      <option value="USSD_Menu_Company_Items_Report">USSD Menu Company Items Report</option>
                      <option value="Subscriptions_Report">Subscriptions Report</option>
                      <option value="Subscriptions_Company_Report">Company Subscriptions Report</option>
                      <option value="Revenues_Report">Revenues Report</option>
                      <option value="Revenues_Company_Report">Company Revenues Report</option>
                  </select>
                </div>
                <div class="col s12 m4 l4" style="padding: 15px; border-left:1px solid #3d3d3d;">
                    <div class="row">
                            <div class="col s3 m3 l3">
                      <input class="with-gap" type="radio" name="ReportType" id="R1" value="Total" />
                        <label for="R1">Total</label>
                    </div>
                    <div class="col s3 m3 l3">
                        <input class="with-gap" type="radio" name="ReportType" id="R2" value="Day" checked="checked" />
                        <label for="R2">Day</label>
                      </div>
                      <div class="col s3 m3 l3">
                        <input class="with-gap" type="radio" name="ReportType" id="R3" value="Month" />
                        <label for="R3">Month</label>
                      </div>
                      <div class="col s3 m3 l3">
                        <input class="with-gap" type="radio" name="ReportType" id="R4" value="Year" />
                        <label for="R4">Year</label>
                    </div>
                  </div>
                </div>
                <div class="col s12 m3 l3" style="padding: 15px; border-left:1px solid #3d3d3d;">
                    <div class="row">
                    	<div class="col s3 m3 l3">
                      		<input class="with-gap" type="radio" name="ChartType" id="C1" value="line" checked="checked"/>
                        	<label for="C1">Line</label>
                    	</div>
                    	<div class="col s3 m3 l3">
                        	<input class="with-gap" type="radio" name="ChartType" id="C2" value="bar" />
                        	<label for="C2">Bar</label>
                      	</div>
                  	</div>
                </div>
                <div class="col s12 m2 l2">
                    <a href="export/Customer/Day" class="waves-effect waves-light btn border-round right" id="DownloadReport" target="_blank"><i class="material-icons">file_download</i></a>
                </div>
              </div>
	        <hr />
          <div class="chart-wrapper" style="width:90%; margin-left:5%">
        	   <canvas id="Chart"></canvas>
          </div>
        </li>
      </ul>
    </div>
    
  </div>
</div>
<!--card widgets end-->
@endsection
@push('scripts')

<script type="text/javascript">
	var chartColors = [
		'rgb(255, 64, 129)',
		'rgb(255, 102, 64)',
		'rgb(75, 162, 192)',
		'rgb(54, 192, 235)',
		'rgb(153, 159, 255)',
		'rgb(201, 99, 207)',
		'rgb(255, 99, 132)',
		'rgb(255, 159, 64)',
		'rgb(75, 192, 192)',
		'rgb(54, 162, 235)',
		'rgb(153, 102, 255)',
		'rgb(201, 203, 207)'
	];
  	
	var baseUrl = $('meta[name="base-url"]').attr('content');
	var suburl = $('meta[name="sub-url"]').attr('content');
	
	
	$("#Report").on('change', function (e) { 
		var Type = $("input[name='ReportType']:checked").val();
		var Report = $(this).val();
		pullReport(Report, Type)
	});
	
	$("input[name='ReportType']").on('change', function (e) { 
		var Type = $("input[name='ReportType']:checked").val();
		var Report = $("#Report").val();
		pullReport(Report, Type)
	});
	
	$("input[name='ChartType']").on('change', function (e) { 
		var Type = $("input[name='ReportType']:checked").val();
		var Report = $("#Report").val();
		pullReport(Report, Type)
	});
	
	function pullReport(Report, Type)
	{
		//alert(Report+'-'+Type);
		switch(Report)
		{
			case 'Customer':
				Customer_Report(Type);
			break;
			case 'Unique_Customer':
				Unique_Customer_Report(Type);
			break;
			case 'Company_Requests_Report':
				Company_Requests_Report(Type);
			break;
			case 'Routes_Report':
				Routes_Report(Type);
			break;
			case 'Schedules_Report':
				Schedules_Report(Type);
			break;
			case 'Shortcode_Strings_Report':
				Shortcode_Strings_Report(Type);
			break;
			case 'USSD_Menu_General_Items_Report':
				USSD_Menu_General_Items_Report(Type);
			break;
			case 'USSD_Menu_Company_Items_Report':
				USSD_Menu_Company_Items_Report(Type);
			break;
			case 'Subscriptions_Report':
				Subscriptions_Report(Type);
			break;
			case 'Subscriptions_Company_Report':
				Subscriptions_Company_Report(Type);
			break;
			case 'Revenues_Report':
				Revenues_Report(Type);
			break;
			case 'Revenues_Company_Report':
				Revenues_Company_Report(Type);
			break;
		}
	}
	var ChartVar="";;
	function CreateChart(LineChartSampleData, Options)
	{
		$("#chart-wrapper").html("").html('<canvas id="Chart"></canvas>');
		if(ChartVar!="")
		{
			ChartVar.destroy();
		}
		ChartVar = new Chart(document.getElementById("Chart"), {
			type: $("input[name='ChartType']:checked").val(),
			data: LineChartSampleData,
			options: Options
		});
		
	}
	//Customer
	Customer_Report('Day');
	function Customer_Report(Type)
	{
		$("#DownloadReport").attr("href", 'export/Customer/'+Type);
		var Options = {
			responsive: true,
			title: {
				display: true,
				text: 'Customer Report'
			},
			scales: {
				xAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: Type
					}
				}],
				yAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Counts'
					}
				}]
			}
		};
		$.ajax({
			url: baseUrl+suburl+'chart/Customer/'+Type,
			type: 'GET',
			dataType: 'json',
			data: {method: '_GET'},
			success: function (response) {
				console.log(response)
				var dataLables = response.Labels;
				
				var dataValues = [];
				$.each(response.Data,function(key,value){
					dataValues.push(value.Count);
				});
				var LineChartSampleData = {
					labels: dataLables,
					datasets: [{ 
						data: dataValues,
						label: "Customer",
						backgroundColor: chartColors[0],
						borderColor: chartColors[0],
						fill: false
					  }
					]
				};
				CreateChart(LineChartSampleData, Options)
			},
			error: function (result, status, err) {
				alert(result.responseText);
				alert(status.responseText);
				alert(err.Message);
			},
		});
	}
	
	//Unique_Customer_Report
	function Unique_Customer_Report(Type)
	{
		$("#DownloadReport").attr("href", 'export/Unique_Customer/'+Type);
		var Options = {
			responsive: true,
			title: {
				display: true,
				text: 'Customer Report'
			},
			scales: {
				xAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: Type
					}
				}],
				yAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Counts'
					}
				}]
			}
		};
		$.ajax({
			url: baseUrl+suburl+'chart/Unique_Customer/'+Type,
			type: 'GET',
			dataType: 'json',
			data: {method: '_GET'},
			success: function (response) {
				console.log(response)
				var dataLables = response.Labels;
				
				var dataValues = [];
				$.each(response.Data,function(key,value){
					dataValues.push(value.Count);
				});
				var LineChartSampleData = {
					labels: dataLables,
					datasets: [{ 
						data: dataValues,
						label: "Customer",
						backgroundColor: chartColors[0],
						borderColor: chartColors[0],
						fill: false
					  }
					]
				};
				CreateChart(LineChartSampleData, Options)
			},
			error: function (result, status, err) {
				alert(result.responseText);
				alert(status.responseText);
				alert(err.Message);
			},
		});
	}
	
	//Company_Requests_Report
	function Company_Requests_Report(Type)
	{
		$("#DownloadReport").attr("href", 'export/Company_Requests_Report/'+Type);
		var Options = {
			responsive: true,
			title: {
				display: true,
				text: 'Company Code Requests Report'
			},
			scales: {
				xAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: Type
					}
				}],
				yAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Counts'
					}
				}]
			}
		};
		$.ajax({
			url: baseUrl+suburl+'chart/Company_Requests_Report/'+Type,
			type: 'GET',
			dataType: 'json',
			data: {method: '_GET'},
			success: function (response) {
				console.log(response)
				var dataLables = response.Labels;
				
				var datasets=[];
				var i=0;
				
				$.each(response.Data,function(key,value){
					
					var GraphLable="";
					var dataValues = [];
					
					$.each(value,function(keyInner,valueInner){
						GraphLable = valueInner.ussd_code;
						dataValues.push(valueInner.Count);
					});
					var dataset=[{
			            data: dataValues,
						label: GraphLable,
						backgroundColor: chartColors[i],
						borderColor: chartColors[i],
						fill: false
			        }];
					datasets = $.merge(dataset, datasets);
					i++;
				});
				console.log(datasets);
				
				var LineChartSampleData = {
					labels: dataLables,
					datasets: datasets
				};
				
				CreateChart(LineChartSampleData, Options)
			},
			error: function (result, status, err) {
				alert(result.responseText);
				alert(status.responseText);
				alert(err.Message);
			},
		});
	}
	
	//Shortcode_Strings_Report
	function Shortcode_Strings_Report(Type)
	{
		$("#DownloadReport").attr("href", 'export/Shortcode_Strings_Report/'+Type);
		var Options = {
			responsive: true,
			title: {
				display: true,
				text: 'Shortcode Strings Report'
			},
			scales: {
				xAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: Type
					}
				}],
				yAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Counts'
					}
				}]
			}
		};
		$.ajax({
			url: baseUrl+suburl+'chart/Shortcode_Strings_Report/'+Type,
			type: 'GET',
			dataType: 'json',
			data: {method: '_GET'},
			success: function (response) {
				console.log(response)
				var dataLables = response.Labels;
				
				var datasets=[];
				var i=0;
				
				$.each(response.Data,function(key,value){
					
					var GraphLable="";
					var dataValues = [];
					
					$.each(value,function(keyInner,valueInner){
						GraphLable = valueInner.ussd_code;
						dataValues.push(valueInner.Count);
					});
					var dataset=[{
			            data: dataValues,
						label: GraphLable,
						backgroundColor: chartColors[i],
						borderColor: chartColors[i],
						fill: false
			        }];
					datasets = $.merge(dataset, datasets);
					i++;
				});
				console.log(datasets);
				
				var LineChartSampleData = {
					labels: dataLables,
					datasets: datasets
				};
				
				CreateChart(LineChartSampleData, Options)
			},
			error: function (result, status, err) {
				alert(result.responseText);
				alert(status.responseText);
				alert(err.Message);
			},
		});
	}
	
	//Routes_Report
	function Routes_Report(Type)
	{
		$("#DownloadReport").attr("href", 'export/Route_Report/'+Type);
		var Options = {
			responsive: true,
			title: {
				display: true,
				text: 'Route Report'
			},
			scales: {
				xAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: Type
					}
				}],
				yAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Counts'
					}
				}]
			}
		};
		$.ajax({
			url: baseUrl+suburl+'chart/Route_Report/'+Type,
			type: 'GET',
			dataType: 'json',
			data: {method: '_GET'},
			success: function (response) {
				console.log(response)
				var dataLables = response.Labels;
				
				var datasets=[];
				var i=0;
				
				$.each(response.Data,function(key,value){
					
					var GraphLable="";
					var dataValues = [];
					
					$.each(value,function(keyInner,valueInner){
						GraphLable = valueInner.route_name;
						dataValues.push(valueInner.Count);
					});
					var dataset=[{
			            data: dataValues,
						label: GraphLable,
						backgroundColor: chartColors[i],
						borderColor: chartColors[i],
						fill: false
			        }];
					datasets = $.merge(dataset, datasets);
					i++;
				});
				console.log(datasets);
				
				var LineChartSampleData = {
					labels: dataLables,
					datasets: datasets
				};
				
				CreateChart(LineChartSampleData, Options)
			},
			error: function (result, status, err) {
				alert(result.responseText);
				alert(status.responseText);
				alert(err.Message);
			},
		});
	}
	
	//Schedules_Report
	function Schedules_Report(Type)
	{
		$("#DownloadReport").attr("href", 'export/Schedules_Report/'+Type);
		var Options = {
			responsive: true,
			title: {
				display: true,
				text: 'Schedules Report'
			},
			scales: {
				xAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: Type
					}
				}],
				yAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Counts'
					}
				}]
			}
		};
		$.ajax({
			url: baseUrl+suburl+'chart/Schedules_Report/'+Type,
			type: 'GET',
			dataType: 'json',
			data: {method: '_GET'},
			success: function (response) {
				console.log(response)
				var dataLables = response.Labels;
				
				var datasets=[];
				var i=0;
				
				$.each(response.Data,function(key,value){
					
					var GraphLable="";
					var dataValues = [];
					
					$.each(value,function(keyInner,valueInner){
						GraphLable = valueInner.schedule_name;
						dataValues.push(valueInner.Count);
					});
					var dataset=[{
			            data: dataValues,
						label: GraphLable,
						backgroundColor: chartColors[i],
						borderColor: chartColors[i],
						fill: false
			        }];
					datasets = $.merge(dataset, datasets);
					i++;
				});
				console.log(datasets);
				
				var LineChartSampleData = {
					labels: dataLables,
					datasets: datasets
				};
				
				CreateChart(LineChartSampleData, Options)
			},
			error: function (result, status, err) {
				alert(result.responseText);
				alert(status.responseText);
				alert(err.Message);
			},
		});
	}
	
	//Company_Requests_Report
	function USSD_Menu_General_Items_Report(Type)
	{
		$("#DownloadReport").attr("href", 'export/USSD_Menu_General_Items_Report/'+Type);
		var Options = {
			responsive: true,
			title: {
				display: true,
				text: 'USSD Menu General Items Report'
			},
			scales: {
				xAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: Type
					}
				}],
				yAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Counts'
					}
				}]
			}
		};
		$.ajax({
			url: baseUrl+suburl+'chart/USSD_Menu_General_Items_Report/'+Type,
			type: 'GET',
			dataType: 'json',
			data: {method: '_GET'},
			success: function (response) {
				console.log(response)
				var dataLables = response.Labels;
				
				var datasets=[];
				var i=0;
				
				$.each(response.Data,function(key,value){
					
					var GraphLable="";
					var dataValues = [];
					
					$.each(value,function(keyInner,valueInner){
						GraphLable = valueInner.action;
						dataValues.push(valueInner.Count);
					});
					var dataset=[{
			            data: dataValues,
						label: GraphLable,
						backgroundColor: chartColors[i],
						borderColor: chartColors[i],
						fill: false
			        }];
					datasets = $.merge(dataset, datasets);
					i++;
				});
				console.log(datasets);
				
				var LineChartSampleData = {
					labels: dataLables,
					datasets: datasets
				};
				CreateChart(LineChartSampleData, Options)
				
			},
			error: function (result, status, err) {
				alert(result.responseText);
				alert(status.responseText);
				alert(err.Message);
			},
		});
	}
	
	//Company_Requests_Report
	function USSD_Menu_Company_Items_Report(Type)
	{
		$("#DownloadReport").attr("href", 'export/USSD_Menu_Company_Items_Report/'+Type);
		var Options = {
			responsive: true,
			title: {
				display: true,
				text: 'USSD Menu Company Items Report'
			},
			scales: {
				xAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: Type
					}
				}],
				yAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Counts'
					}
				}]
			}
		};
		$.ajax({
			url: baseUrl+suburl+'chart/USSD_Menu_Company_Items_Report/'+Type,
			type: 'GET',
			dataType: 'json',
			data: {method: '_GET'},
			success: function (response) {
				console.log(response)
				var dataLables = response.Labels;
				
				var datasets=[];
				var i=0;
				
				$.each(response.Data,function(key,value){
					
					var GraphLable="";
					var dataValues = [];
					
					$.each(value,function(keyInner,valueInner){
						GraphLable = valueInner.action;
						dataValues.push(valueInner.Count);
					});
					var dataset=[{
			            data: dataValues,
						label: GraphLable,
						backgroundColor: chartColors[i],
						borderColor: chartColors[i],
						fill: false
			        }];
					datasets = $.merge(dataset, datasets);
					i++;
				});
				console.log(datasets);
				
				var LineChartSampleData = {
					labels: dataLables,
					datasets: datasets
				};
				
				CreateChart(LineChartSampleData, Options)
			},
			error: function (result, status, err) {
				alert(result.responseText);
				alert(status.responseText);
				alert(err.Message);
			},
		});
	}
	
	//Subscriptions Report
	function Subscriptions_Report(Type)
	{
		$("#DownloadReport").attr("href", 'export/Subscriptions_Report/'+Type);
		var Options = {
			responsive: true,
			title: {
				display: true,
				text: 'Subscriptions Report'
			},
			scales: {
				xAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: Type
					}
				}],
				yAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Counts'
					}
				}]
			}
		};
		$.ajax({
			url: baseUrl+suburl+'chart/Subscriptions_Report/'+Type,
			type: 'GET',
			dataType: 'json',
			data: {method: '_GET'},
			success: function (response) {
				console.log(response)
				var dataLables = response.Labels;
				
				var dataValues = [];
				$.each(response.Data,function(key,value){
					dataValues.push(value.Count);
				});
				var LineChartSampleData = {
					labels: dataLables,
					datasets: [{ 
						data: dataValues,
						label: "Customer",
						backgroundColor: chartColors[0],
						borderColor: chartColors[0],
						fill: false
					  }
					]
				};
				CreateChart(LineChartSampleData, Options)
			},
			error: function (result, status, err) {
				alert(result.responseText);
				alert(status.responseText);
				alert(err.Message);
			},
		});
	}
	
	//Subscriptions_Company_Report
	function Subscriptions_Company_Report(Type)
	{
		$("#DownloadReport").attr("href", 'export/Subscriptions_Company_Report/'+Type);
		var Options = {
			responsive: true,
			title: {
				display: true,
				text: 'Subscriptions Company Report'
			},
			scales: {
				xAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: Type
					}
				}],
				yAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Counts'
					}
				}]
			}
		};
		$.ajax({
			url: baseUrl+suburl+'chart/Subscriptions_Company_Report/'+Type,
			type: 'GET',
			dataType: 'json',
			data: {method: '_GET'},
			success: function (response) {
				console.log(response)
				var dataLables = response.Labels;
				
				var datasets=[];
				var i=0;
				
				$.each(response.Data,function(key,value){
					
					var GraphLable="";
					var dataValues = [];
					
					$.each(value,function(keyInner,valueInner){
						GraphLable = valueInner.ussd_code;
						dataValues.push(valueInner.Count);
					});
					var dataset=[{
			            data: dataValues,
						label: GraphLable,
						backgroundColor: chartColors[i],
						borderColor: chartColors[i],
						fill: false
			        }];
					datasets = $.merge(dataset, datasets);
					i++;
				});
				console.log(datasets);
				
				var LineChartSampleData = {
					labels: dataLables,
					datasets: datasets
				};
				
				CreateChart(LineChartSampleData, Options)
			},
			error: function (result, status, err) {
				alert(result.responseText);
				alert(status.responseText);
				alert(err.Message);
			},
		});
	}
	
	//Revenues Report
	function Revenues_Report(Type)
	{
		$("#DownloadReport").attr("href", 'export/Revenues_Report/'+Type);
		var Options = {
			responsive: true,
			title: {
				display: true,
				text: 'Revenues Report'
			},
			scales: {
				xAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: Type
					}
				}],
				yAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Counts'
					}
				}]
			}
		};
		$.ajax({
			url: baseUrl+suburl+'chart/Revenues_Report/'+Type,
			type: 'GET',
			dataType: 'json',
			data: {method: '_GET'},
			success: function (response) {
				console.log(response)
				var dataLables = response.Labels;
				
				var dataValues = [];
				$.each(response.Data,function(key,value){
					dataValues.push(value.Count);
				});
				var LineChartSampleData = {
					labels: dataLables,
					datasets: [{ 
						data: dataValues,
						label: "Customer",
						backgroundColor: chartColors[0],
						borderColor: chartColors[0],
						fill: false
					  }
					]
				};
				CreateChart(LineChartSampleData, Options)
			},
			error: function (result, status, err) {
				alert(result.responseText);
				alert(status.responseText);
				alert(err.Message);
			},
		});
	}
	
	//Revenues_Company_Report
	function Revenues_Company_Report(Type)
	{
		$("#DownloadReport").attr("href", 'export/Revenues_Company_Report/'+Type);
		var Options = {
			responsive: true,
			title: {
				display: true,
				text: 'Revenues Company Report'
			},
			scales: {
				xAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: Type
					}
				}],
				yAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Counts'
					}
				}]
			}
		};
		$.ajax({
			url: baseUrl+suburl+'chart/Revenues_Company_Report/'+Type,
			type: 'GET',
			dataType: 'json',
			data: {method: '_GET'},
			success: function (response) {
				console.log(response)
				var dataLables = response.Labels;
				
				var datasets=[];
				var i=0;
				
				$.each(response.Data,function(key,value){
					
					var GraphLable="";
					var dataValues = [];
					
					$.each(value,function(keyInner,valueInner){
						GraphLable = valueInner.ussd_code;
						dataValues.push(valueInner.Count);
					});
					var dataset=[{
			            data: dataValues,
						label: GraphLable,
						backgroundColor: chartColors[i],
						borderColor: chartColors[i],
						fill: false
			        }];
					datasets = $.merge(dataset, datasets);
					i++;
				});
				console.log(datasets);
				
				var LineChartSampleData = {
					labels: dataLables,
					datasets: datasets
				};
				
				CreateChart(LineChartSampleData, Options)
			},
			error: function (result, status, err) {
				alert(result.responseText);
				alert(status.responseText);
				alert(err.Message);
			},
		});
	}

</script>
<script>
var Options = {
	responsive: true,
	title: false,
	lable: false,
	scales: {
		xAxes: [{
			display: false,
			scaleLabel: false
		}],
		yAxes: [{
			display: false,
			scaleLabel: false
		}]
	},
	legend: {
        display: false
    },
};
var ctxTerminal = document.getElementById("TerminalChart").getContext('2d');
var TerminalChart = new Chart(ctxTerminal, {
	type: 'bar',
	data: {
		labels: {!!"[".$TerminalLables."]"!!},
		datasets: [{
			data: {!!"[".$TerminalCounts."]"!!},
			backgroundColor: {!!"[".$TerminalbackgroundColor."]"!!},
			borderColor: {!!"[".$TerminalborderColor."]"!!},
			borderWidth: 1
		}]
	},
	options: Options
});

var ctxFleet = document.getElementById("FleetChart").getContext('2d');
var FleetChart = new Chart(ctxFleet, {
	type: 'bar',
	data: {
		labels: {!!"[".$FleetLables."]"!!},
		datasets: [{
			data: {!!"[".$FleetCounts."]"!!},
			backgroundColor: {!!"[".$FleetbackgroundColor."]"!!},
			borderColor: {!!"[".$FleetborderColor."]"!!},
			borderWidth: 1
		}]
	},
	options: Options
});


var ctxRoute = document.getElementById("RouteChart").getContext('2d');
var RouteChart = new Chart(ctxRoute, {
	type: 'bar',
	data: {
		labels: {!!"[".$RouteLables."]"!!},
		datasets: [{
			data: {!!"[".$RouteCounts."]"!!},
			backgroundColor: {!!"[".$RoutebackgroundColor."]"!!},
			borderColor: {!!"[".$RouteborderColor."]"!!},
			borderWidth: 1
		}]
	},
	options: Options
});

var ctxSchedule = document.getElementById("ScheduleChart").getContext('2d');
var ScheduleChart = new Chart(ctxSchedule, {
	type: 'bar',
	data: {
		labels: {!!"[".$ScheduleLables."]"!!},
		datasets: [{
			data: {!!"[".$ScheduleCounts."]"!!},
			backgroundColor: {!!"[".$SchedulebackgroundColor."]"!!},
			borderColor: {!!"[".$ScheduleborderColor."]"!!},
			borderWidth: 1
		}]
	},
	options: Options
});

var ctxUser = document.getElementById("UserChart").getContext('2d');
var UserChart = new Chart(ctxUser, {
	type: 'bar',
	data: {
		labels: {!!"[".$UserLables."]"!!},
		datasets: [{
			data: {!!"[".$UserCounts."]"!!},
			backgroundColor: {!!"[".$UserbackgroundColor."]"!!},
			borderColor: {!!"[".$UserborderColor."]"!!},
			borderWidth: 1
		}]
	},
	options: Options
});


var ctxScheduleMonday = document.getElementById("ScheduleMondayChart").getContext('2d');
var ScheduleMondayChart = new Chart(ctxScheduleMonday, {
	type: 'bar',
	data: {
		labels: {!!"[".$ScheduleLables."]"!!},
		datasets: [{
			data: {!!"[".$ScheduleMondayCounts."]"!!},
			backgroundColor: {!!"[".$SchedulebackgroundColor."]"!!},
			borderColor: {!!"[".$ScheduleborderColor."]"!!},
			borderWidth: 1
		}]
	},
	options: Options
});

var ctxScheduleTuesday = document.getElementById("ScheduleTuesdayChart").getContext('2d');
var ScheduleTuesdayChart = new Chart(ctxScheduleTuesday, {
	type: 'bar',
	data: {
		labels: {!!"[".$ScheduleLables."]"!!},
		datasets: [{
			data: {!!"[".$ScheduleTuesdayCounts."]"!!},
			backgroundColor: {!!"[".$SchedulebackgroundColor."]"!!},
			borderColor: {!!"[".$ScheduleborderColor."]"!!},
			borderWidth: 1
		}]
	},
	options: Options
});

var ctxScheduleWednesday = document.getElementById("ScheduleWednesdayChart").getContext('2d');
var ScheduleWednesdayChart = new Chart(ctxScheduleWednesday, {
	type: 'bar',
	data: {
		labels: {!!"[".$ScheduleLables."]"!!},
		datasets: [{
			data: {!!"[".$ScheduleWednesdayCounts."]"!!},
			backgroundColor: {!!"[".$SchedulebackgroundColor."]"!!},
			borderColor: {!!"[".$ScheduleborderColor."]"!!},
			borderWidth: 1
		}]
	},
	options: Options
});

var ctxScheduleThursday = document.getElementById("ScheduleThursdayChart").getContext('2d');
var ScheduleThursdayChart = new Chart(ctxScheduleThursday, {
	type: 'bar',
	data: {
		labels: {!!"[".$ScheduleLables."]"!!},
		datasets: [{
			data: {!!"[".$ScheduleThursdayCounts."]"!!},
			backgroundColor: {!!"[".$SchedulebackgroundColor."]"!!},
			borderColor: {!!"[".$ScheduleborderColor."]"!!},
			borderWidth: 1
		}]
	},
	options: Options
});

var ctxScheduleFriday = document.getElementById("ScheduleFridayChart").getContext('2d');
var ScheduleFridayChart = new Chart(ctxScheduleFriday, {
	type: 'bar',
	data: {
		labels: {!!"[".$ScheduleLables."]"!!},
		datasets: [{
			data: {!!"[".$ScheduleFridayCounts."]"!!},
			backgroundColor: {!!"[".$SchedulebackgroundColor."]"!!},
			borderColor: {!!"[".$ScheduleborderColor."]"!!},
			borderWidth: 1
		}]
	},
	options: Options
});

var ctxScheduleSaturday = document.getElementById("ScheduleSaturdayChart").getContext('2d');
var ScheduleSaturdayChart = new Chart(ctxScheduleSaturday, {
	type: 'bar',
	data: {
		labels: {!!"[".$ScheduleLables."]"!!},
		datasets: [{
			data: {!!"[".$ScheduleSaturdayCounts."]"!!},
			backgroundColor: {!!"[".$SchedulebackgroundColor."]"!!},
			borderColor: {!!"[".$ScheduleborderColor."]"!!},
			borderWidth: 1
		}]
	},
	options: Options
});

var ctxScheduleSunday = document.getElementById("ScheduleSundayChart").getContext('2d');
var ScheduleSundayChart = new Chart(ctxScheduleSunday, {
	type: 'bar',
	data: {
		labels: {!!"[".$ScheduleLables."]"!!},
		datasets: [{
			data: {!!"[".$ScheduleSundayCounts."]"!!},
			backgroundColor: {!!"[".$SchedulebackgroundColor."]"!!},
			borderColor: {!!"[".$ScheduleborderColor."]"!!},
			borderWidth: 1
		}]
	},
	options: Options
});
</script>
@endpush
