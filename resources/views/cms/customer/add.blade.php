@extends('cms.layout.app')
@section('breadcrumbs')
<div id="breadcrumbs-wrapper">
   <!-- Search for small screen -->
   <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
      <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
   </div>
   <div class="container">
      <div class="row">
         <div class="col s10 m6 l6">
            <h5 class="breadcrumbs-title">Add Customer</h5>
            <ol class="breadcrumbs">
               <li><a href="{{ url($guard_url) }}">Home</a>
               </li>
               <li><a href="{{ url($guard_url.'customer') }}">Customer</a>
               </li>
            </ol>
         </div>
         <div class="col s2 m6 l6">
            <a class="btn waves-effect waves-light breadcrumbs-btn right" href="{{ url($guard_url) }}" data-activates="dropdown1">
            <i class="material-icons">home</i>
            </a>
            
         </div>
      </div>
   </div>
</div>
@endsection
@section('content')
{!! Form::open([ 'url' => $guard_url.'customer/', 'id' => 'main-form' ]) !!}
<br />
<div class="divider"></div>
<br />
<div class="row">
   <div class="col s12 m8 l8">
      <div id="flight-card" class="card">
          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
            <div class="card-title">
              <h6 class="flight-card-title">City Information</h6>
            </div>
          </div>
         <div class="card-content" style="background-color:#FFF">
            <div class="col s12 m4 input-field">
			  <?php
                $defaultSelection = ["" => "Select Title", "1" => "Mr.", "2" => "Mrs."];
              ?>
              {!! Form::select('title', $defaultSelection, null, ['class' => 'form-control']) !!}
              @if ($errors->has('title'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('title')!!}</span>@endif
            </div>
            <div class="col s12 m4 input-field">
              <label for="first_name">First Name:</label>
              {!! Form::text('first_name', null, ['class' => 'form-control' , 'required' , 'id' => 'first_name']) !!}
              @if ($errors->has('first_name'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('first_name')!!}</span>@endif
           </div>
            <div class="col s12 m4 input-field">
              <label for="last_name">Last Name:</label>
              {!! Form::text('last_name', null, ['class' => 'form-control' , 'required' , 'id' => 'last_name']) !!}
              @if ($errors->has('last_name'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('last_name')!!}</span>@endif
           </div>
           <div class="col s12 m4 input-field">
              <label for="other_name">Other Name:</label>
              {!! Form::text('other_name', null, ['class' => 'form-control' , 'required', 'id' => 'other_name']) !!}
              @if ($errors->has('other_name'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('other_name')!!}</span>@endif
           </div>
           <div class="col s12 m4 input-field">
              <?php
                $defaultSelection = ["" => "Select Marital Status", "1" => "Single", "2" => "Married"];
              ?>
              {!! Form::select('martial_status', $defaultSelection, null, ['class' => 'form-control']) !!}
              @if ($errors->has('martial_status'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('martial_status')!!}</span>@endif
            </div>
            <div class="col s12 m4 input-field">
              <?php
                $defaultSelection = ["" => "Select Work Status ", "1" => "Student", "2" => "Employee"];
              ?>
              {!! Form::select('work_status', $defaultSelection, null, ['class' => 'form-control']) !!}
              @if ($errors->has('work_status'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('work_status')!!}</span>@endif
            </div>
            <div class="col s12 m4 input-field">
               <label for="address">Address:</label>
               {!! Form::textarea('address', null, ['required' , 'class' => 'materialize-textarea', 'id' => 'address', 'rows' => '5']) !!}
               @if ($errors->has('address'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('address')!!}</span>@endif
            </div>
            <div class="col s12 m4">
              <label class="active">Gender:</label><br /><br />
              <input type="radio" name="gender" id="male" value="1" checked="checked"/><label for="male">Male</label>
              <input type="radio" name="gender" id="female" value="2"/><label for="female">Female</label>
              @if ($errors->has('gender'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('gender')!!}</span>@endif
            </div>
            <div class="col s12 m4">
               <br /><br />
               <div class="switch">
                    <label>
                    Inactive
                    <input type="checkbox" checked="checked" name="status" id="status"/>
                    <span class="lever"></span>
                    Active
                    </label>
                </div>
               
            </div>
            <div class="row">
              <div class="input-field col s12">
                
              </div>
            </div>
		</div>
      </div>
   </div>
</div>
   
<div class="row">
   <div class="col s12 m8 l8">
      <div id="flight-card" class="card">
          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
            <div class="card-title">
              <h6 class="flight-card-title">MPRS Information</h6>
            </div>
          </div>
         <div class="card-content" style="background-color:#FFF">
            <div class="col s12 m4 input-field">
              <label for="mprs_gsm_no">MPRS GSM No.:</label>
              {!! Form::text('mprs_gsm_no', null, ['class' => 'form-control' , 'required', 'id' => 'mprs_gsm_no']) !!}
              @if ($errors->has('mprs_gsm_no'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('mprs_gsm_no')!!}</span>@endif
           </div>
           <div class="col s12 m4 input-field">
              <label for="gsm_no_2">GSM Nos 2:</label>
              {!! Form::text('gsm_no_2', null, ['class' => 'form-control' , 'required' , 'id' => 'gsm_no_2']) !!}
              @if ($errors->has('gsm_no_2'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('gsm_no_2')!!}</span>@endif
           </div>
           <div class="col s12 m4 input-field">
              <label for="gsm_no_3">GSM Nos 3:</label>
              {!! Form::text('gsm_no_3', null, ['class' => 'form-control' , 'required' , 'id' => 'gsm_no_3']) !!}
              @if ($errors->has('gsm_no_3'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('gsm_no_3')!!}</span>@endif
           </div>
           <div class="col s12 m4 input-field">
              <label for="email">Email:</label>
              {!! Form::text('email', null, ['class' => 'form-control' , 'required', 'id' => 'email']) !!}
              @if ($errors->has('email'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('email')!!}</span>@endif
           </div>
           <div class="col s12 m4">
              <label class="active">Use Pin:</label><br /><br />
              <input type="radio" name="use_pin" id="yes" value="1"/><label for="yes">Yes</label>
              <input type="radio" name="use_pin" id="no" value="0" checked="checked"/><label for="no">No</label>
              @if ($errors->has('gender'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('gender')!!}</span>@endif
            </div>
           <div class="col s12 m4 input-field">
              <label for="pin">Pin:</label>
	          {!! Form::text('pin', null, ['class' => 'form-control' , 'required', 'id' => 'pin', 'disabled'=>'disabled']) !!}
              @if ($errors->has('pin'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('pin')!!}</span>@endif
			</div>
            <div class="row">
              <div class="input-field col s12">
                
              </div>
            </div>
		</div>
      </div>
   </div>
</div>
   
<div class="row">
   <div class="col s12 m8 l8">
      <div id="flight-card" class="card">
          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
            <div class="card-title">
              <h6 class="flight-card-title">NEXT OF KIN INFORMATION</h6>
            </div>
          </div>
         <div class="card-content" style="background-color:#FFF">
            <div class="col s12 m4 input-field">
              <label for="kin_fullname">Full Name:</label>
              {!! Form::text('kin_fullname', null, ['class' => 'form-control' , 'required', 'id' => 'kin_fullname']) !!}
              @if ($errors->has('kin_fullname'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('kin_fullname')!!}</span>@endif
           </div>
           <div class="col s12 m4 input-field">
              <label for="kin_phone">Phone No.:</label>
              {!! Form::text('kin_phone', null, ['class' => 'form-control' , 'required', 'id' => 'kin_phone']) !!}
              @if ($errors->has('kin_phone'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('kin_phone')!!}</span>@endif
           </div> 
           <div class="col s12 m4 input-field">
              <label for="address">Address:</label>
               {!! Form::textarea('kin_address', null, ['required' , 'class' => 'materialize-textarea', 'id' => 'kin_address', 'rows' => '5']) !!}
               @if ($errors->has('kin_address'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('kin_address')!!}</span>@endif
           </div>
           <div class="row">
              <div class="input-field col s12">
                <button class="btn cyan waves-effect waves-light right" type="submit">Submit
                  <i class="material-icons right">send</i>
                </button>
              </div>
            </div>
         </div>
         
      </div>
   </div>
</div>
</form>
@endsection

@push('scripts')

<script type="text/javascript">
	$('input[name=use_pin]').on('change', function() {
		if($('input[name=use_pin]:checked').val()=="0")
		{
			$("#pin").val('');
			$("#pin").attr('disabled','disabled');
		}
		else
		{
			$("#pin").val('');
			$("#pin").removeAttr('disabled');
		}
	});
</script>

@endpush

