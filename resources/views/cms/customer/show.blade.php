@extends('cms.layout.app')
@section('breadcrumbs')
<div id="breadcrumbs-wrapper">
   <!-- Search for small screen -->
   <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
      <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
   </div>
   <div class="container">
      <div class="row">
         <div class="col s10 m6 l6">
            <h5 class="breadcrumbs-title">Show Customer</h5>
            <ol class="breadcrumbs">
               <li><a href="{{ url($guard_url) }}">Home</a>
               </li>
               <li><a href="{{ url($guard_url.'customer') }}">Customer</a>
               </li>
            </ol>
         </div>
         <div class="col s2 m6 l6">
            <a class="btn waves-effect waves-light breadcrumbs-btn right" href="{{ url($guard_url) }}" data-activates="dropdown1">
            <i class="material-icons">home</i>
            </a>
            
  <br />
       </div>
      </div>
   </div>
</div>
@endsection
@section('content')
<br />
<div class="divider"></div>
<br />
<div class="row">
   <div class="col s12 m8 l8">
      <div id="flight-card" class="card" style="overflow: hidden;">
          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
            <div class="card-title">
              <h6 class="flight-card-title">Customer Information</h6>
            </div>
          </div>
         <div class="card-content" style="background-color:#FFF">
            <div class="col s12 m4 input-field">
               <label class="active">Title:</label>
               <p id="lbltype">
               	<?php
					switch($info_Customer->title)
					{
						case 1:
							echo "Mr.";
						break;
						case 2:
							echo "Mrs.";
						break;
					}
				?>
               </p>
            </div>
            <div class="col s12 m4 input-field">
               <label class="active">First Name:</label>
               <p id="lblcountry">{{ $info_Customer->first_name }}</p>
            </div>
            <div class="col s12 m4 input-field">
               <label class="active">Last Name:</label>
               <p id="lblcountry">{{ $info_Customer->last_name }}</p>
            </div>
            <div class="col s12 m4 input-field">
               <label class="active">Other Name:</label>
               <p id="lblcountry">{{ $info_Customer->other_name }}</p>
            </div>
           <div class="col s12 m4 input-field">
               <label class="active">Marital Status:</label>
               <p id="lbltype">
               	<?php
					switch($info_Customer->gender)
					{
						case 1:
							echo "Single";
						break;
						case 2:
							echo "Married";
						break;
					}
				?>
               </p>
            </div>
           <div class="col s12 m4 input-field">
               <label class="active">Work Status:</label>
               <p id="lbltype">
               	<?php
					switch($info_Customer->title)
					{
						case 1:
							echo "Student";
						break;
						case 2:
							echo "Employee";
						break;
					}
				?>
               </p>
            </div>
            <div class="col s12 m4 input-field">
               <label class="active">Address:</label>
               <p id="lblcountry">{{ $info_Customer->address }}</p>
            </div>
            <div class="col s12 m4 input-field">
               <label class="active">Gender:</label>
               <p id="lbltype">
               	<?php
					switch($info_Customer->gender)
					{
						case 1:
							echo "Male";
						break;
						case 2:
							echo "Female";
						break;
					}
				?>
               </p>
            </div>
			<div class="col s12 m4 input-field">
               <label class="active">Status:</label>
               <p id="lblstatus">{{ ($info_Customer->status==1?"Active":"Inactive") }}</p>
            </div>
		</div>
      </div>
   </div>
</div>
   
<div class="row">
   <div class="col s12 m8 l8">
      <div id="flight-card" class="card" style="overflow: hidden;">
          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
            <div class="card-title">
              <h6 class="flight-card-title">MPRS Information</h6>
            </div>
          </div>
         <div class="card-content" style="background-color:#FFF">
            <div class="col s12 m4 input-field">
               <label class="active">MPRS GSM No.:</label>
               <p id="lblcountry">{{ $info_Customer->mprs_gsm_no }}</p>
            </div>
            <div class="col s12 m4 input-field">
               <label class="active">GSM Nos 2:</label>
               <p id="lblcountry">{{ $info_Customer->gsm_no_2 }}</p>
            </div>
            <div class="col s12 m4 input-field">
               <label class="active">GSM Nos 3:</label>
               <p id="lblcountry">{{ $info_Customer->gsm_no_3 }}</p>
            </div>
            <div class="col s12 m4 input-field">
               <label class="active">Email:</label>
               <p id="lblcountry">{{ $info_Customer->email }}</p>
            </div>
			<div class="col s12 m4 input-field">
               <label class="active">Pin:</label>
               <p id="lblcountry">{{ $info_Customer->pin }}</p>
            </div>
		</div>
      </div>
   </div>
</div>
   
<div class="row">
   <div class="col s12 m8 l8">
      <div id="flight-card" class="card" style="overflow: hidden;">
          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
            <div class="card-title">
              <h6 class="flight-card-title">NEXT OF KIN INFORMATION</h6>
            </div>
          </div>
         <div class="card-content" style="background-color:#FFF">
            <div class="col s12 m4 input-field">
               <label class="active">Full Name:</label>
               <p id="lblcountry">{{ $info_Customer->kin_fullname }}</p>
            </div>
            <div class="col s12 m4 input-field">
               <label class="active">Phone No.:</label>
               <p id="lblcountry">{{ $info_Customer->kin_phone }}</p>
            </div>
            <div class="col s12 m4 input-field">
               <label class="active">Address:</label>
               <p id="lblcountry">{{ $info_Customer->kin_address }}</p>
            </div>
  <br />
       </div>
      </div>
   </div>
</div>
@endsection