@extends('cms.layout.app')
@section('breadcrumbs')
<div id="breadcrumbs-wrapper">
   <!-- Search for small screen -->
   <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
      <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
   </div>
   <div class="container">
      <div class="row">
         <div class="col s10 m6 l6">
            <h5 class="breadcrumbs-title">Customer Log</h5>
            <ol class="breadcrumbs">
               <li><a href="{{ url($guard_url) }}">Home</a>
               </li>
            </ol>
         </div>
         <div class="col s2 m6 l6">
            <a class="btn waves-effect waves-light breadcrumbs-btn right" href="{{ url($guard_url) }}" data-activates="dropdown1">
            <i class="material-icons">home</i>
            </a>
            
         </div>
      </div>
   </div>
</div>
@endsection
@section('content')
<div class="row">
   <div class="col s12 m12 l12">
      <div id="flight-card" class="card">
          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
            <div class="card-title">
              <h5 class="flight-card-title">Customer log detail</h5>
            </div>
          </div>
          <div class="row card-content" style="background-color:#FFF">
            <div class="row">
               <div class="col s12 m2 input-field">
                  <label for="code">From:</label>
                  {!! Form::text('from', null, ['class' => 'datepicker' , 'required' , 'id' => 'from']) !!}
               </div>
               <div class="col s12 m2 input-field">
                  <label for="code">To:</label>
                  {!! Form::text('to', null, ['class' => 'datepicker' , 'required' , 'id' => 'to']) !!}
               </div>
               <div class="col s12 m2 input-field">
                  <button class="btn cyan waves-effect waves-light right" type="button" id="Search">Search</button>
               </div>
            </div>
            <table id="viewForm" class="bordered striped responsive-table display" cellspacing="0">
              <thead>
                <tr>
                  <th>Date</th>
                  <th>USSD Code</th>
                  <th>Msisdn</th>
                  <th>Action</th>
                  <th>Session Time</th>
                  <th>Session Cost</th>
                  <th>Gateway</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
             </table>
 		  </div>
        </div>
      </div>
    </div>
 </div>     

@endsection

@push('scripts')

<script type="text/javascript">
    $('#viewForm').DataTable({
        "processing": true,
        "serverSide": true,
		"ajax": "{{url($guard_url.'customerloggrid')}}",
        "columns": [
			{ data: 'created_at', name: 'created_at' },
			{ data: 'ussd_code', name: 'ussd_code' },
			{ data: 'msisdn', name: 'msisdn' },
			{ data: 'action', name: 'action' },
			{ data: 'session_time', name: 'session_time' , "orderable": "false"},
			{ data: 'session_cost', name: 'session_cost' , "orderable": "false"},
			{ data: 'gateway', name: 'gateway' },
		],
		"responsive": true,
		"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "All"] ],
		dom: 'Blfrtip',
		buttons: [
			'copy', 'csv'
		],
		order: [ [0, 'desc'] ]
    });
	$('#Search').on('click', function (e) { 
		var from = $("#from").val();
		var to = $("#to").val();
		//alert(FromCity+' '+ToCity+' '+FromTerminal+' '+ToTerminal+' '+effectiveDate);
		//alert("{{url($guard_url.'schedule/grid')}}/?Search=true&FromCity="+FromCity+"&ToCity="+ToCity+"&FromTerminal="+FromTerminal+"&ToTerminal="+ToTerminal+"&effectiveDate="+effectiveDate);
		$('#viewForm').DataTable().ajax.url( "{{url($guard_url.'customerloggrid')}}/?Search=true&from="+from+"&to="+to).load();
	});
</script>

@endpush