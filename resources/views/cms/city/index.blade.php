@extends('cms.layout.app')
@section('breadcrumbs')
<div id="breadcrumbs-wrapper">
   <!-- Search for small screen -->
   <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
      <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
   </div>
   <div class="container">
      <div class="row">
         <div class="col s10 m6 l6">
            <h5 class="breadcrumbs-title">City</h5>
            <ol class="breadcrumbs">
               <li><a href="{{ url($guard_url) }}">Home</a>
               </li>
            </ol>
         </div>
         <div class="col s2 m6 l6">
            <a class="btn waves-effect waves-light breadcrumbs-btn right" href="{{ url($guard_url) }}" data-activates="dropdown1">
            <i class="material-icons">home</i>
            </a>
            
         </div>
      </div>
   </div>
</div>
@endsection
@section('content')
<div class="row">
   <div class="col s12 m12 l12">
      <div id="flight-card" class="card">
          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
            <div class="card-title">
              <h5 class="flight-card-title">State/City detail <span> 
        @if(Auth::guard($guard)->User()->can('Add State/City'))<a class="btn-floating waves-effect waves-light right top-add" href="{{ url($guard_url.'city/create') }}"><i class="material-icons">add</i></a>@endif</span></h5>
            </div>
          </div>
          <div class="card-content" style="background-color:#FFF">
            <table id="viewForm" class="bordered striped responsive-table display" cellspacing="0" style="width:100%;">
              <thead>
                <tr>
                  <th>Country</th>
                  <th>Name</th>
                  <th>Short Name</th>
                  <th>Status</th>
                  <th>Sequence</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
        
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
 </div>   

@endsection

@push('scripts')

<script type="text/javascript">
    $('#viewForm').DataTable({
        "processing": true,
        "serverSide": true,
		"ajax": "{{url($guard_url.'city/grid')}}",
        "columns": [
			{ data: 'country_id', name: 'country_id' },
			{ data: 'name', name: 'id' },
			{ data: 'short_name', name: 'short_name' },
            { data: 'status', name: 'status' },
			{ data: 'sequence', name: 'sequence' },
			{ data: 'edit', name: 'edit', orderable: false, searchable: false }
		],
		
		"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "All"] ],
		dom: 'Blfrtip',
		buttons: [
			{
				extend : 'csv',
				title : 'All Values',
				exportOptions : {
					modifier : {
						// DataTables core
						order : 'index', // 'current', 'applied',
						//'index', 'original'
						page : 'all', // 'all', 'current'
						search : 'none' // 'none', 'applied', 'removed'
					},
					columns: [ 0, 1, 2, 3 ]
				}
			},
			{
				extend : 'print',
				title : 'Cities',
				exportOptions : {
					modifier : {
						// DataTables core
						order : 'index', // 'current', 'applied',
						//'index', 'original'
						page : 'all', // 'all', 'current'
						search : 'none' // 'none', 'applied', 'removed'
					},
					columns: [ 0, 1, 2, 3 ]
				}
			}
		],
		//order: [ [0, 'desc'] ]
    });
	
	$(document).on('blur','.sequence_id', function(){
		var sequence = $(this).val();
		var city_id = $(this).data("city_id");
		if (!$.isNumeric(sequence)) {
		   alert("Invalid integr value");
		   return false;
		}
		
		var baseUrl = $('meta[name="base-url"]').attr('content');
		var subUrl = $('meta[name="sub-url"]').attr('content');
		var url = baseUrl+subUrl+'city/sequence/'+city_id;
		// confirm then
		$.ajaxSetup({
			headers: {
			  'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
			}
		});
		//alert(url)
		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'json',
			data: {method: '_POST', "sequence":sequence, submit: true},
			success: function (response) {	
				
			},
			error: function (result, status, err) {
				console.log(result.responseText);
				//alert(status.responseText);
				//alert(err.Message);
			},
		});
		
		return false;
	});
</script>

@endpush