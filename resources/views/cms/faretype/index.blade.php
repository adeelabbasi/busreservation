@extends('cms.layout.app')
@section('breadcrumbs')
<div id="breadcrumbs-wrapper">
   <!-- Search for small screen -->
   <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
      <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
   </div>
   <div class="container">
      <div class="row">
         <div class="col s10 m6 l6">
            <h5 class="breadcrumbs-title">Add fare type</h5>
            <ol class="breadcrumbs">
               <li><a href="{{ url($guard_url) }}">Home</a>
               </li>
               <li><a href="{{ url($guard_url.'schedule') }}">schedule</a>
               </li>
            </ol>
         </div>
         <div class="col s2 m6 l6">
            <a class="btn waves-effect waves-light breadcrumbs-btn right" href="{{ url($guard_url) }}" data-activates="dropdown1">
            <i class="material-icons">home</i>
            </a>
            
         </div>
      </div>
   </div>
</div>
@endsection
@section('content')


<!--Preselecting a tab-->
<div class="row">
  <div class="col s12">
     <div class="row">
        <div class="col s8">
           <ul class="tabs z-depth-1 cyan">
                
                <li class="tab col s4"><a class="white-text waves-effect waves-light" onclick="location.href='{{ url($guard_url.'schedule/'.$info_Schedule->id.'/edit') }}'" href="#">Schedule</a>
                </li>
                <li class="tab col s4"><a class="white-text waves-effect waves-light active" onclick="location.href='{{ url($guard_url.'fare_type?ScheduleID='.$info_Schedule->id) }}'" href="#">Fare Type</a>
                </li>
                <li class="tab col s4"><a class="white-text waves-effect waves-light" onclick="location.href='{{ url($guard_url.'fare?ScheduleID='.$info_Schedule->id) }}'" href="#">Fare</a>
                </li>
           </ul>
        </div>
     </div>
  </div>
</div>

<br />
<div class="divider"></div>
<br />
<div class="row">
   <div class="col s12 m8 l8"> 
      <div id="flight-card" class="card">
          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
            <div class="card-title">
              <h6 class="flight-card-title">Fare Type Information</h6>
            </div>
          </div>
         <div class="card-content" style="background-color:#FFF">
           {!! Form::open([ 'url' => $guard_url.'fare_type/', 'id' => 'main-form' ]) !!}
           <input type="hidden" name="company_id" value="{{ $info_Schedule->company_id }}"  />
           <input type="hidden" name="schedule_id" value="{{ $info_Schedule->id }}"  />
           
           <div class="col s12 m4 input-field">
              <label for="code">Fare Type:</label>
              {!! Form::text('name', null, ['class' => 'form-control' , 'required', 'id' => 'name']) !!}
              @if ($errors->has('name'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('name')!!}</span>@endif
           </div>
           <div class="col s12 m4 input-field">
              <?php
                $defaultSelection = [];//["" => "Select Service"];
				foreach($info_Services as $info_Service)
                {
                    $defaultSelection = $defaultSelection +  array($info_Service->id => ($info_Service->name));
                }
              ?>
              {!! Form::select('service_id', $defaultSelection, null, ['class' => 'form-control', 'required', 'id' => 'ScheduleService']) !!}
              @if ($errors->has('service_id'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('service_id')!!}</span>@endif
           </div>
           <div class="col s12 m4 input-field">
            <button class="btn cyan waves-effect waves-light left" type="submit">Add new
            </button>
           </div>
           </form>
           <div class="row">
              <table id="fareForm" class="bordered striped responsive-table display" cellspacing="0">
              <thead>
                <tr>
                  <th>Fare Type</th>
                  <th>Services</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              @foreach($info_FareTypes as $info_FareType)
              	<tr id="Del{{$info_FareType->id}}">
                  <td>{{ $info_FareType->name }}</td>
                  <td>{{ $info_FareType->Service()->First()->name }}</td>
                  <td> 
                  	<a class="btn-floating waves-effect waves-light red" href="javascript(0)" title="Delete Data" id="btnDelete" name="btnDelete" data-message="Are you sure to delete fare type?" data-remote="{{ $guard_url.'fare_type/' . $info_FareType->id }}" data-fid="{{ $info_FareType->id }}"><i class="material-icons">clear</i></a></td>
                </tr>
              @endforeach
              </tbody>
              </tr>
            </table>
           </div>
           <div class="row">
              <div class="input-field col s12">
                <a href="{{ url($guard_url.'fare?ScheduleID='.$info_Schedule->id) }}" class="btn cyan waves-effect waves-light right">Next
                  <i class="material-icons right">send</i>
                </a>
              </div>
            </div>
       </div>
      </div>
   </div>
</div>
@endsection

@push('scripts')

<script>
	$('#fareForm').on('click', '#btnDelete[data-remote]', function (e) { 
		if (confirm($(this).data('message'))) {		
			e.preventDefault();		
			var baseUrl = $('meta[name="base-url"]').attr('content');
			var url = baseUrl+$(this).data('remote');
			$("#Del"+$(this).data('fid')).fadeOut();
			// confirm then
			$.ajaxSetup({
				headers: {
				  'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
				}
			});
			//alert(url)
			$.ajax({
				url: url,
				type: 'DELETE',
				dataType: 'json',
				data: {method: '_DELETE' , submit: true},
				error: function (result, status, err) {
					console.log(result.responseText);
					//alert(status.responseText);
					//alert(err.Message);
				},
			});
		}
		return false;
	});
</script>
@endpush



