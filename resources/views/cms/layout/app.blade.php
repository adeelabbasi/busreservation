<html lang="en">
  <!--================================================================================
  Item Name: Materialize - Material Design Admin Template
  Version: 4.0
  Author: PIXINVENT
  Author URL: https://themeforest.net/user/pixinvent/portfolio
  ================================================================================ -->
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests"> 
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Bus resetvation system">
    <meta name="keywords" content="Bus resetvation system">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="base-url" content="{{ url('') }}">
    <meta name="sub-url" content="{{ $guard_url }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Favicons-->
    <link rel="icon" href="{{url('itravel_favicon.png')}}" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="{{url('itravel_favicon.png')}}">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="{{url('itravel_favicon.png')}}">
    <!-- For Windows Phone -->
    <!-- CORE CSS-->
    <link href="{{ asset('material/css/themes/overlay-menu/materialize.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('material/css/themes/overlay-menu/style.css') }}" type="text/css" rel="stylesheet">
    <!-- CSS for Overlay Menu (Layout Full Screen)-->
    <link href="{{ asset('material/css/layouts/style-fullscreen.css') }}" type="text/css" rel="stylesheet">
    <!-- Custome CSS-->
    <link href="{{ asset('material/css/custom/custom.css') }}" type="text/css" rel="stylesheet">
    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="{{ asset('material/vendors/prism/prism.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('material/vendors/perfect-scrollbar/perfect-scrollbar.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('material/vendors/jvectormap/jquery-jvectormap.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('material/vendors/perfect-scrollbar/perfect-scrollbar.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('material/vendors/data-tables/css/jquery.dataTables.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('material/vendors/flag-icon/css/flag-icon.min.css') }}" type="text/css" rel="stylesheet">

    @stack('style')
    <style>
		table.dataTable.stripe tbody tr.odd, table.dataTable.display tbody tr.odd
		{
			background-color: #e0f7fa; !important
		}
		
		table.dataTable.stripe tbody tr.even, table.dataTable.display tbody tr.even
		{
			background-color: #ffffff; !important
		}
		
		table.dataTable.display tbody tr.odd>.sorting_1
		{
			background-color: #e0f7fa; !important
		}
		table.dataTable.display tbody tr.odd:hover>.sorting_1
		{
			background-color: #f5f5f5; !important
		}
		
		table.dataTable.display tbody tr.even>.sorting_1
		{
			background-color: #ffffff; !important
		}
		table.dataTable.display tbody tr.even:hover>.sorting_1
		{
			background-color: #f5f5f5; !important
		}
		.buttons-csv
		{
			margin-right:10px;
		}
	</style>
  </head>
  <body>
    <!-- Start Page Loading -->
    <!-- End Page Loading -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START HEADER -->
    <header id="header" class="page-topbar">
      <div class="navbar-fixed">
        <nav class="navbar-color gradient-45deg-indigo-light-blue">
          <div class="nav-wrapper">
            <ul class="left">
              <li>
                <h1 class="logo-wrapper">
                  <a href="{{ url('/admin') }}" class="brand-logo darken-1" style="padding: 17px 60px;">
                    
                    <span class="logo-text hide-on-med-and-down"><img src="{{url('itravel_logo.png')}}" alt="materialize logo" style="height:33px; width:100px"></span>
                  </a>
                </h1>
              </li>
            </ul>
            <!--<div class="header-search-wrapper hide-on-med-and-down sideNav-lock">
              <i class="material-icons">search</i>
              <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore ..." />
            </div>-->
            <ul class="right hide-on-med-and-down">
              <li>
                <a href="javascript:void(0);" class="waves-effect waves-block waves-light profile-button" data-activates="profile-dropdown">
                  <span class="avatar-status avatar-online">
                    @if(Auth::guard($guard)->User()->avatar=="")
                    	<img src="{{ asset('media/avatar/').'/demo_user.png' }}" alt="{{ Auth::guard($guard)->User()->name }}" alt="avatar">
                    @else
                    	<img src="{{ asset('media/avatar/').'/'.Auth::guard($guard)->User()->avatar }}" alt="{{ Auth::guard($guard)->User()->name }}" alt="avatar">
                    @endif
                    <i></i>
                  </span>
                </a>
              </li>
            </ul>
            
            <!-- profile-dropdown -->
            <ul id="profile-dropdown" class="dropdown-content">
              <li>
                <a href="{{ url($guard_url.'profile') }}" class="grey-text text-darken-1">
                  <i class="material-icons">face</i> Profile</a>
              </li>
              <li>
                	<a href="{{ url(($guard=='admin'?'admin/logout':'/logout')) }}" class="grey-text text-darken-1"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                    <i class="material-icons">keyboard_tab</i> Logout </a>
                    <form id="logout-form" action="{{ url(($guard=='admin'?'admin/logout':'/logout')) }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    </header>
    <!-- END HEADER -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START MAIN -->
    <div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START LEFT SIDEBAR NAV-->
        <aside id="left-sidebar-nav">
          <ul id="slide-out" class="side-nav leftside-navigation">
            <li class="no-padding">
              <ul class="collapsible" data-collapsible="accordion">
                @if(Auth::guard($guard)->User()->can('Dashboard'))
                <li class="bold">
                  <a href="{{ url($guard_url.'dashboard') }}" class="waves-effect waves-cyan">
                    <img style="top:10px" src="{{ asset('material/images/icon/LeftIcon/dashboard.png') }}">
                    <span class="nav-text">Dashboard</span>
                  </a>
                </li>
                @endif
                <li class="li-hover">
                  <p class="ultra-small margin more-text">Administration</p>
                </li>
                
                @if(Auth::guard($guard)->User()->can('View Company') || Auth::guard($guard)->User()->can('View Country') || Auth::guard($guard)->User()->can('View State/City') || Auth::guard($guard)->User()->can('View LGA/Municipal'))
                <li class="bold">
                  <a class="collapsible-header waves-effect waves-cyan">
                    <img style="top:10px" src="{{ asset('material/images/icon/LeftIcon/setup.png') }}">
                    <span class="nav-text">Setup</span>
                  </a>
                  <div class="collapsible-body">
                    <ul>
                    	@if(Auth::guard($guard)->User()->can('View Company'))
                        <li><a href="{{ url($guard_url.'company') }}"><i class="material-icons">keyboard_arrow_right</i>Company</a></li>
                        @endif
                        @if(Auth::guard($guard)->User()->can('View Country'))
                        <li><a href="{{ url($guard_url.'country') }}"><i class="material-icons">keyboard_arrow_right</i>Country</a></li>
                        @endif
                        @if(Auth::guard($guard)->User()->can('View State/City'))
                        <li><a href="{{ url($guard_url.'city') }}"><i class="material-icons">keyboard_arrow_right</i>State/City</a></li>
                        @endif
                        @if(Auth::guard($guard)->User()->can('View LGA/Municipal'))
                        <li><a href="{{ url($guard_url.'lga') }}"><i class="material-icons">keyboard_arrow_right</i>LGA/Municipal</a></li>
                        @endif
                    </ul>
                  </div>
                </li>
                @endif
                
                @if(Auth::guard($guard)->User()->can('View Terminal'))
                <li class="bold">
                  <a href="{{ url($guard_url.'terminal') }}" class="waves-effect waves-cyan">
                    <img style="top:10px" src="{{ asset('material/images/icon/LeftIcon/terminal.png') }}">
                    <span class="nav-text">Terminal</span>
                  </a>
                </li>
                @endif
                
                @if(Auth::guard($guard)->User()->can('View Fleet'))
                <li class="bold">
                  <a href="{{ url($guard_url.'bus') }}" class="waves-effect waves-cyan">
                    <img style="top:10px" src="{{ asset('material/images/icon/LeftIcon/bus.png') }}">
                    <span class="nav-text">Fleet</span>
                  </a>
                </li>
                @endif
                
                @if(Auth::guard($guard)->User()->can('View Service'))
                <li class="bold">
                  <a href="{{ url($guard_url.'service') }}" class="waves-effect waves-cyan">
                    <img style="top:10px" src="{{ asset('material/images/icon/LeftIcon/service.png') }}">
                    <span class="nav-text">Service</span>
                  </a>
                </li>
                @endif
                                
                @if(Auth::guard($guard)->User()->can('View Route'))
                <li class="bold">
                  <a href="{{ url($guard_url.'route') }}" class="waves-effect waves-cyan">
                    <img style="top:10px" src="{{ asset('material/images/icon/LeftIcon/route.png') }}">
                    <span class="nav-text">Route</span>
                  </a>
                </li>
                @endif
                
                @if(Auth::guard($guard)->User()->can('View Schedule'))
                <li class="bold">
                  <a href="{{ url($guard_url.'schedule') }}" class="waves-effect waves-cyan">
                    <img style="top:10px" src="{{ asset('material/images/icon/LeftIcon/schedule.png') }}">
                    <span class="nav-text">Book Now</span>
                  </a>
                </li>
                @endif
                
                @if(Auth::guard($guard)->User()->can('View User') || Auth::guard($guard)->User()->can('Add User') || Auth::guard($guard)->User()->can('View User Role') || Auth::guard($guard)->User()->can('View User Activity'))
                <li class="bold">
                  <a class="collapsible-header waves-effect waves-cyan">
                    <img style="top:10px" src="{{ asset('material/images/icon/LeftIcon/user.png') }}">
                    <span class="nav-text">User</span>
                  </a>
                  <div class="collapsible-body">
                    <ul>
                        @if(Auth::guard($guard)->User()->can('View User'))
                        <li><a href="{{ url($guard_url.'user') }}"><i class="material-icons">keyboard_arrow_right</i>User</a></li>
                        @endif
                        @if(Auth::guard($guard)->User()->can('Add User'))
                        <li><a href="{{ url($guard_url.'user/create') }}"><i class="material-icons">keyboard_arrow_right</i>Add User</a></li>
                        @endif
                        @if(Auth::guard($guard)->User()->can('View User Role'))
                        <li><a href="{{ url($guard_url.'role') }}"><i class="material-icons">keyboard_arrow_right</i>Role</a></li>
                        @endif
                        @if(Auth::guard($guard)->User()->can('View User Activity'))
                        <li><a href="{{ url($guard_url.'userlog') }}"><i class="material-icons">keyboard_arrow_right</i>User Activity</a></li>
                        @endif
                    </ul>
                  </div>
                </li>
                @endif
                
                @if(Auth::guard($guard)->User()->can('View Customer') || Auth::guard($guard)->User()->can('View Customer Activity'))
                <li class="bold">
                  <a class="collapsible-header waves-effect waves-cyan">
                    <img style="top:10px" src="{{ asset('material/images/icon/LeftIcon/customer.png') }}">
                    <span class="nav-text">Customer</span>
                  </a>
                  <div class="collapsible-body">
                    <ul>
                        @if(Auth::guard($guard)->User()->can('View Customer'))
                        <li><a href="{{ url($guard_url.'customer') }}"><i class="material-icons">keyboard_arrow_right</i>Customer</a></li>
                        @endif
                        @if(Auth::guard($guard)->User()->can('View Customer Activity'))
                        <li><a href="{{ url($guard_url.'customerlog') }}"><i class="material-icons">keyboard_arrow_right</i>Customer Activity</a></li>
                        @endif
                    </ul>
                  </div>
                </li>
                @endif
                
                @if(Auth::guard($guard)->User()->can('View Subscription'))
                <li class="bold">
                  <a href="{{ url($guard_url.'subscription') }}" class="waves-effect waves-cyan">
                    <img style="top:10px" src="{{ asset('material/images/icon/LeftIcon/subscription.png') }}">
                    <span class="nav-text">Subscription</span>
                  </a>
                </li>
                @endif
                
                @if(Auth::guard($guard)->User()->can('View Policy'))
                <li class="bold">
                  <a href="{{ url($guard_url.'policy') }}" class="waves-effect waves-cyan">
                    <img style="top:10px" src="{{ asset('material/images/icon/LeftIcon/policy.png') }}">
                    <span class="nav-text">About US</span>
                  </a>
                </li>
                @endif
                
                @if(Auth::guard($guard)->User()->can('View Promotion/Deals'))
                <li class="bold">
                  <a href="{{ url($guard_url.'deal') }}" class="waves-effect waves-cyan">
                    <img style="top:10px" src="{{ asset('material/images/icon/LeftIcon/deal.png') }}">
                    <span class="nav-text">Promotion/Deals</span>
                  </a>
                </li>
                @endif
                
                @if(Auth::guard($guard)->User()->can('View Booking'))
                <li class="bold">
                  <a href="{{ url($guard_url.'booking') }}" class="waves-effect waves-cyan">
                    <img style="top:10px" src="{{ asset('material/images/icon/LeftIcon/booking.png') }}">
                    <span class="nav-text">Booking</span>
                  </a>
                </li>
                @endif
                
                @if(Auth::guard($guard)->User()->can('View Payment'))
                <li class="bold">
                  <a href="{{ url($guard_url.'payment') }}" class="waves-effect waves-cyan">
                    <img style="top:10px" src="{{ asset('material/images/icon/LeftIcon/payment.png') }}">
                    <span class="nav-text">Payment</span>
                  </a>
                </li>
                @endif
                
                
                <li class="li-hover">
                  <p class="ultra-small margin more-text">&nbsp;</p>
                </li>
              </ul>
            </li>
          </ul>
          <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light pink accent-2">
            <i class="material-icons">menu</i>
          </a>
        </aside>
        <!-- END LEFT SIDEBAR NAV-->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!-- START CONTENT -->
        <section id="content">
          @yield('breadcrumbs')
          <!--start container-->
          	<div class="container">
                @yield('content')
              
              <!-- //////////////////////////////////////////////////////////////////////////// -->
            </div>
            <!--end container-->
        </section>
        <!-- END CONTENT -->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        </div>
        <!-- END WRAPPER -->
      </div>
      <!-- END MAIN -->
      <!-- //////////////////////////////////////////////////////////////////////////// -->
      <!-- START FOOTER -->
      <footer class="page-footer">
        <div class="footer-copyright">
          <div class="container">
            <span>Copyright ©
              <script type="text/javascript">
                document.write(new Date().getFullYear());
              </script> <a class="grey-text text-lighten-4" href="http://iyconserver.com" target="_blank">IYCONSERVER</a> All rights reserved.</span>
          </div>
        </div>
      </footer>
      <!-- END FOOTER -->
      <!-- ================================================
    Scripts
    ================================================ -->      
    <!-- jQuery Library -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
     <!--materialize js-->
    <script type="text/javascript" src="{{ asset('material/js/materialize.min.js') }}"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="{{ asset('material/vendors/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
    <!--prism-->
    <script type="text/javascript" src="{{ asset('material/vendors/prism/prism.js') }}"></script>
    <!-- chartjs -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
    <!-- Datatables -->
    <script src="{{ asset('vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
    <script src="{{ asset('vendor/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
    <!--data-tables.js - Page Specific JS codes -->
    <script type="text/javascript" src="{{ asset('material/js/scripts/data-tables.js') }}"></script>
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="{{ asset('material/js/plugins.js') }}"></script>
    <!--form-elements.js - Page Specific JS codes-->
    <script type="text/javascript" src="{{ asset('material/js/scripts/form-elements.js') }}"></script>
    <!--advanced-ui-modals.js - Some Specific JS codes -->
    <script type="text/javascript" src="{{ asset('material/js/scripts/advanced-ui-modals.js') }}"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="{{ asset('material/js/custom-script.js') }}"></script>
      @stack('scripts')
  </body>
</html>
