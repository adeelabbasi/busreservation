@extends('cms.layout.app')
@section('breadcrumbs')
<div id="breadcrumbs-wrapper">
   <!-- Search for small screen -->
   <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
      <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
   </div>
   <div class="container">
      <div class="row">
         <div class="col s10 m6 l6">
            <h5 class="breadcrumbs-title">Booking</h5>
            <ol class="breadcrumbs">
               <li><a href="{{ url($guard_url) }}">Home</a>
               </li>
            </ol>
         </div>
         <div class="col s2 m6 l6">
            <a class="btn waves-effect waves-light breadcrumbs-btn right" href="{{ url($guard_url) }}" data-activates="dropdown1">
            <i class="material-icons">home</i>
            </a>
            
         </div>
      </div>
   </div>
</div>
@endsection
@section('content')
<div class="row">
   <div class="col s12 m12 l12">
      <div id="flight-card" class="card">
          <div class="card-content" style="background-color:#FFF">
            <div class="row">
               <div class="col s12 m4 input-field">
                  {!! Form::text('from_date', null, ['class' => 'datepicker' , 'required' , 'id' => 'from_date']) !!}
               </div>
               <div class="col s12 m4 input-field">
                  {!! Form::text('to_date', null, ['class' => 'datepicker' , 'required' , 'id' => 'to_date']) !!}
               </div>
               <div class="col s12 m4 input-field">
                  <button class="btn cyan waves-effect waves-light left" type="button" id="Search">Search</button>
               </div>
            </div>
            <table id="viewForm" class="bordered striped responsive-table display" cellspacing="0" style="width:100%;">
              <thead>
                <tr>
                  <th>Transaction Date</th>
                  <th>Phone</th>
                  <th>Fullname</th>
                  <th>Bookingref</th>
                  <th>Tripref</th>
                  <th>Payment ref</th>
                  <th>Trace ID</th>
                  <th>New Customer</th>
                  <th>Date</th>
                  <th>Departure Terminal</th>
                  <th>Arrival Terminal</th>
                  <th>No of Seats</th>
                  <th>Amount</th>
				  <th>Commission</th>
				  <th>Discount</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
 		  </div>
        </div>
      </div>
    </div>
 </div>     

@endsection

@push('scripts')

<script type="text/javascript">
    $('#viewForm').DataTable({
        "processing": true,
        "serverSide": true,
		"ajax": "{{url($guard_url.'booking/grid')}}",
        "columns": [
			{ data: 'updated_at', name: 'updated_at' },
			{ data: 'msisdn', name: 'msisdn' },
			{ data: 'Fullname', name: 'Fullname' },
			{ data: 'OrderID', name: 'OrderID' },
			{ data: 'TripID', name: 'TripID' },
			{ data: 'refcode', name: 'refcode' },
			{ data: 'traceId', name: 'traceId' },
			{ data: 'new_customer', name: 'new_customer' },
			{ data: 'schedule_date', name: 'schedule_date' },
			{ data: 'from_terminal_name', name: 'from_terminal_name' },
			{ data: 'to_terminal_name', name: 'to_terminal_name' },
			{ data: 'MaxSeat', name: 'MaxSeat' },
			{ data: 'amount', name: 'amount' },
			{ data: 'commission', name: 'commission' },
			{ data: 'discount', name: 'discount' },
			{ data: 'status', name: 'status' },
		],
		
		"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "All"] ],
		dom: 'Blfrtip',
		buttons: [
			{
				extend : 'csv',
				title : 'Bookings',
				exportOptions : {
					modifier : {
						// DataTables core
						order : 'index', // 'current', 'applied',
						//'index', 'original'
						page : 'all', // 'all', 'current'
						search : 'none' // 'none', 'applied', 'removed'
					},
					columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,15 ]
				}
			},
			{
				extend : 'print',
				title : 'All Values',
				exportOptions : {
					modifier : {
						// DataTables core
						order : 'index', // 'current', 'applied',
						//'index', 'original'
						page : 'all', // 'all', 'current'
						search : 'none' // 'none', 'applied', 'removed'
					},
					columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,15 ]
				}
			}
		],
		order: [ [0, 'desc'] ]
    });
	$('#Search').on('click', function (e) { 
		var from_date = $("#from_date").val();
		var to_date = $("#to_date").val();
		$('#viewForm').DataTable().ajax.url( "{{url($guard_url.'booking/grid')}}/?Search=true&from_date="+from_date+"&to_date="+to_date).load();
	});
</script>

@endpush
