@extends('cms.layout.app')
@section('breadcrumbs')
<div id="breadcrumbs-wrapper">
   <!-- Search for small screen -->
   <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
      <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
   </div>
   <div class="container">
      <div class="row">
         <div class="col s10 m6 l6">
            <h5 class="breadcrumbs-title">Schedule</h5>
            <ol class="breadcrumbs">
               <li><a href="{{ url($guard_url) }}">Home</a>
               </li>
            </ol>
         </div>
         <div class="col s2 m6 l6">
            <a class="btn waves-effect waves-light breadcrumbs-btn right" href="{{ url($guard_url) }}" data-activates="dropdown1">
            <i class="material-icons">home</i>
            </a>
            
         </div>
      </div>
   </div>
</div>
@endsection
@section('content')
<div class="row">
   <div class="col s12 m12 l12">
      <div id="flight-card" class="card">
          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
            <div class="card-title">
              <h5 class="flight-card-title">Schedule detail <span> 
        @if(Auth::guard($guard)->User()->can('Add Schedule'))<a class="btn-floating waves-effect waves-light right top-add" href="{{ url($guard_url.'schedule/create') }}"><i class="material-icons">add</i></a>@endif</span></h5>
            </div>
          </div>
          <div class="card-content" style="background-color:#FFF;">
            <div class="row">
            	<div class="col s12 m2 input-field">
				  <?php
                    $defaultSelection = ["" => "From City"];
                    foreach($info_Cities as $Cities)
                    {
                        $defaultSelection = $defaultSelection +  array($Cities->id => ($Cities->name));
                    }
                  ?>
                  {!! Form::select('city_id', $defaultSelection, null, ['class' => 'form-control', 'id' => 'FromCity']) !!}
               </div>
               <div class="col s12 m2 input-field">
				  <?php
                    $defaultSelection = ["" => "To City"];
                    foreach($info_Cities as $Cities)
                    {
                        $defaultSelection = $defaultSelection +  array($Cities->id => ($Cities->name));
                    }
                  ?>
                  {!! Form::select('city_id', $defaultSelection, null, ['class' => 'form-control', 'id' => 'ToCity']) !!}
               </div>
               <div class="col s12 m2 input-field">
				  <?php
                    $defaultSelection = ["" => "Dept Terminal"];
                    foreach($info_Terminals as $Terminals)
                    {
                        $defaultSelection = $defaultSelection +  array($Terminals->id => ($Terminals->name));
                    }
                  ?>
                  {!! Form::select('terminal_id', $defaultSelection, null, ['class' => 'form-control', 'id' => 'FromTerminal']) !!}
               </div>
               <div class="col s12 m2 input-field">
				  <?php
                    $defaultSelection = ["" => "Arrv Terminal"];
                    foreach($info_Terminals as $Terminals)
                    {
                        $defaultSelection = $defaultSelection +  array($Terminals->id => ($Terminals->name));
                    }
                  ?>
                  {!! Form::select('terminal_id', $defaultSelection, null, ['class' => 'form-control', 'id' => 'ToTerminal']) !!}
               </div>
               <div class="col s12 m2 input-field">
                  {!! Form::text('effective_Date', null, ['class' => 'datepicker' , 'required' , 'id' => 'effectiveDate']) !!}
               </div>
               <div class="col s12 m2 input-field">
                  <button class="btn cyan waves-effect waves-light right" type="button" id="Search">Search</button>
               </div>
            </div>
            <table id="viewForm" class="bordered striped responsive-table display" cellspacing="0" style="width:100%;">
              <thead>
                <tr>
                  <th>Sr#</th>
                  <th>Company</th>
                  <th>Schedule Code</th>
                  <th>Buses</th>
                  <th>Depart</th>
                  <th>Arrive</th>
                  <th>Effective Date</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
 </div>   

@endsection

@push('scripts')

<script type="text/javascript">
    $('#viewForm').DataTable({
        "processing": true,
        "serverSide": true,
		"ajax": "{{url($guard_url.'schedule/grid')}}",
        "columns": [
			{ data: 'id', name: 'id' },
			{ data: 'company_id', name: 'company_id' },
			{ data: 'short_name', name: 'short_name' },
			{ data: 'bus_id', name: 'bus_id' },
			{ data: 'depart_city', name: 'depart_city' },
			{ data: 'arrive_city', name: 'arrive_city' },
			{ data: 'effective_Date', name: 'effective_Date' },
			{ data: 'status', name: 'status' },
			{ data: 'edit', name: 'edit', orderable: false, searchable: false }
		],
		
		"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "All"] ],
		dom: 'Blfrtip',
		buttons: [
			{
				extend : 'csv',
				title : 'Schedules',
				exportOptions : {
					modifier : {
						// DataTables core
						order : 'index', // 'current', 'applied',
						//'index', 'original'
						page : 'all', // 'all', 'current'
						search : 'none' // 'none', 'applied', 'removed'
					},
					columns: [ 0, 1, 2, 3, 4, 5, 6, 7 ]
				}
			},
			{
				extend : 'print',
				title : 'All Values',
				exportOptions : {
					modifier : {
						// DataTables core
						order : 'index', // 'current', 'applied',
						//'index', 'original'
						page : 'all', // 'all', 'current'
						search : 'none' // 'none', 'applied', 'removed'
					},
					columns: [ 0, 1, 2, 3, 4, 5, 6, 7 ]
				}
			}
		],
		//order: [ [0, 'desc'] ]
    });
	
	
	$('#Search').on('click', function (e) { 
		var FromCity = $("#FromCity").val();
		var ToCity = $("#ToCity").val();
		var FromTerminal = $("#FromTerminal").val();
		var ToTerminal = $("#ToTerminal").val();
		var effectiveDate = $("#effectiveDate").val();
		//alert(FromCity+' '+ToCity+' '+FromTerminal+' '+ToTerminal+' '+effectiveDate);
		//alert("{{url($guard_url.'schedule/grid')}}/?Search=true&FromCity="+FromCity+"&ToCity="+ToCity+"&FromTerminal="+FromTerminal+"&ToTerminal="+ToTerminal+"&effectiveDate="+effectiveDate);
		$('#viewForm').DataTable().ajax.url( "{{url($guard_url.'schedule/grid')}}/?Search=true&FromCity="+FromCity+"&ToCity="+ToCity+"&FromTerminal="+FromTerminal+"&ToTerminal="+ToTerminal+"&effectiveDate="+effectiveDate).load();
	});
</script>

@endpush