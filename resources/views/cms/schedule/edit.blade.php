@extends('cms.layout.app')
@section('breadcrumbs')
<div id="breadcrumbs-wrapper">
   <!-- Search for small screen -->
   <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
      <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
   </div>
   <div class="container">
      <div class="row">
         <div class="col s10 m6 l6">
            <h5 class="breadcrumbs-title">Edit Schedule</h5>
            <ol class="breadcrumbs">
               <li><a href="{{ url($guard_url) }}">Home</a>
               </li>
               <li><a href="{{ url($guard_url.'schedule') }}">schedule</a>
               </li>
            </ol>
         </div>
         <div class="col s2 m6 l6">
            <a class="btn waves-effect waves-light breadcrumbs-btn right" href="{{ url($guard_url) }}" data-activates="dropdown1">
            <i class="material-icons">home</i>
            </a>
            
         </div>
      </div>
   </div>
</div>
@endsection
@section('content')


<!--Preselecting a tab-->
<div class="row">
  <div class="col s12">
     <div class="row">
        <div class="col s8">
           <ul class="tabs z-depth-1 cyan">
                
                <li class="tab col s4"><a class="white-text waves-effect waves-light active" onclick="location.href='{{ Request::fullUrl() }}'" href="#">Schedule</a>
                </li>
                <li class="tab col s4"><a class="white-text waves-effect waves-light" onclick="location.href='{{ url($guard_url.'fare_type?ScheduleID='.$info_Schedule->id) }}'" href="#">Fare Type</a>
                </li>
                <li class="tab col s4"><a class="white-text waves-effect waves-light" onclick="location.href='{{ url($guard_url.'fare?ScheduleID='.$info_Schedule->id) }}'" href="#">Fare</a>
                </li>
           </ul>
        </div>
     </div>
  </div>
</div>

<!--
************************************
Start Schedule
************************************
-->
{!! Form::model($info_Schedule, ['method' => 'PATCH', 'url' => [$guard_url.'schedule', $info_Schedule->id], 'id' => 'main-form']) !!}
<br />
<div class="divider"></div>
<br />
<div class="row">
   <div class="col s12 m8 l8">
      <div id="flight-card" class="card">
          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
            <div class="card-title">
              <h6 class="flight-card-title">Schedule Information</h6>
            </div>
          </div>
         <div class="card-content" style="background-color:#FFF">
            @if($guard=="admin")
            <div class="col s12 m4 input-field">
              <?php
                $defaultSelection = ["" => "Select Company"];
                foreach($info_Companies as $Companies)
                {
                    $defaultSelection = $defaultSelection +  array($Companies->id => ($Companies->name));
                }
              ?>
              {!! Form::select('company_id', $defaultSelection, null, ['class' => 'form-control', 'id' => 'CompanySchedule']) !!}
              @if ($errors->has('company_id'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('company_id')!!}</span>@endif
           </div>
           @else
           <input type="hidden" name="company_id" value="{{ Auth::guard($guard)->User()->Company()->First()->id }}"  />
           @endif
           <div class="col s12 m4 input-field">
              <label for="code">Code:</label>
              {!! Form::text('short_name', null, ['class' => 'form-control' , 'required', 'id' => 'code']) !!}
              @if ($errors->has('short_name'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('short_name')!!}</span>@endif
           </div>
           <div class="col s12 m4 input-field">
              <select id="CompanyBuses" name="bus_ids[]" multiple required>
			  	<option disabled="disabled" value="">Select Fleet</option>
			  <?php
                $defaultSelection = ["" => "Select Fleet"];
                foreach($info_Buses as $Buses)
                {
					if(in_array($Buses->name,$info_Schedule->Bus()->Pluck('name')->ToArray()))
					{
						echo '<option value="'.$Buses->id.'" Selected>'.$Buses->name.'</option>';
					}
					else
					{
						echo '<option value="'.$Buses->id.'">'.$Buses->name.'</option>';
					}
                }
              ?>
              </select>
              @if ($errors->has('bus_id'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('bus_id')!!}</span>@endif
           </div>
           <div class="col s12 m12 input-field">
              <label>:</label>
              <?php
                $defaultSelection = ["" => "Select Route(From/To)"];
                foreach($info_Routes as $Routes)
                {
                    $defaultSelection = $defaultSelection +  array($Routes['id'] => ($Routes['name']));
                }
              ?>
              {!! Form::select('route_id', $defaultSelection, null, ['class' => 'form-control', 'id' => 'CompanyRoute']) !!}
              @if ($errors->has('route_id'))<p style="color:red;">{!!$errors->first('route_id')!!}</p>@endif
           </div>
           <div class="col s12 m12 input-field">
                <div class="row">
                    <div class="col s12 m6 input-field" id="CompanyRouteTerminal">
                    	<?php 
							$i=1;
							$length = $info_Schedule->Schedule_time()->Get()->Count();
						?>
                        @foreach($info_Schedule->Schedule_time()->Orderby('sequence')->Get() as $Schedult_time)
                            @if($i==1)
                                <div class="row" style="border-bottom: 1px #000 solid; border-top: 1px #000 solid;">
                                   <div class="col s6 m6">
                                      <h6>{{ $Schedult_time->Terminal()->First()->name }}:</h6>
                                   </div>
                                   <div class="col s6 m6">
                                      <div class="row">
                                        <div class="col s12 m12 input-field">				
                                            <label for="time_dept_{{ $Schedult_time->route_id }}_{{ $Schedult_time->terminal_id }}_{{ $Schedult_time->sequence }}">Departure</label>				
                                            <input type="text" class="timepicker" id="time_dept_{{ $Schedult_time->route_id }}_{{ $Schedult_time->terminal_id }}_{{ $Schedult_time->sequence }}" name="time_dept_{{ $Schedult_time->route_id }}_{{ $Schedult_time->terminal_id }}_{{ $Schedult_time->sequence }}" value="{{ date('h:iA', strtotime($Schedult_time->departure_time)) }}">			
                                       	</div>
                                      </div>
                                   </div>
                                </div>
                            @elseif($i==$length)
                                <div class="row" style="border-bottom: 1px #000 solid;">
                                   <div class="col s6 m6">
                                      <h6>{{ $Schedult_time->Terminal()->First()->name }}:</h6>
                                   </div>
                                   <div class="col s6 m6">
                                      <div class="row">
                                        <div class="col s12 m12 input-field">				
                                            <label for="time_arrv_{{ $Schedult_time->route_id }}_{{ $Schedult_time->terminal_id }}_{{ $Schedult_time->sequence }}">Arrival</label>				
                                            <input type="text" class="timepicker" id="time_arrv_{{ $Schedult_time->route_id }}_{{ $Schedult_time->terminal_id }}_{{ $Schedult_time->sequence }}" name="time_arrv_{{ $Schedult_time->route_id }}_{{ $Schedult_time->terminal_id }}_{{ $Schedult_time->sequence }}" value="{{ date('h:iA', strtotime($Schedult_time->arrival_time)) }}">			
                                        </div>
                                      </div>
                                   </div>
                                </div>
                            @else
                                <div class="row" style="border-bottom: 1px #000 solid;">
                                   <div class="col s6 m6">
                                      <h6>{{ $Schedult_time->Terminal()->First()->name }}:</h6>
                                   </div>
                                   <div class="col s6 m6">
                                      <div class="row">
                                        <div class="col s12 m12 input-field">				
                                            <label for="time_arrv_{{ $Schedult_time->route_id }}_{{ $Schedult_time->terminal_id }}_{{ $Schedult_time->sequence }}">Arrival</label>				
                                            <input type="text" class="timepicker" id="time_arrv_{{ $Schedult_time->route_id }}_{{ $Schedult_time->terminal_id }}_{{ $Schedult_time->sequence }}" name="time_arrv_{{ $Schedult_time->route_id }}_{{ $Schedult_time->terminal_id }}_{{ $Schedult_time->sequence }}" value="{{ date('h:iA', strtotime($Schedult_time->arrival_time)) }}">			
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col s12 m12 input-field">				
                                            <label for="time_dept_{{ $Schedult_time->route_id }}_{{ $Schedult_time->terminal_id }}_{{ $Schedult_time->sequence }}">Departure</label>				
                                            <input type="text" class="timepicker" id="time_dept_{{ $Schedult_time->route_id }}_{{ $Schedult_time->terminal_id }}_{{ $Schedult_time->sequence }}" name="time_dept_{{ $Schedult_time->route_id }}_{{ $Schedult_time->terminal_id }}_{{ $Schedult_time->sequence }}" value="{{ date('h:iA', strtotime($Schedult_time->departure_time)) }}">			
                                       	</div>
                                      </div>
                                   </div>
                                </div>
                            @endif
                            <?php $i++; ?>
                        @endforeach
                    </div>
                </div>
           </div>
           <div class="col s12 m12 input-field">
           &nbsp;
           </div>
           <!--<div class="col s12 m4 input-field">
              <label for="departure_time">Depart Time:</label>
              {!! Form::text('departure_time', date('h:iA', strtotime($info_Schedule->departure_time)), ['class' => 'timepicker' , 'required' , 'id' => 'departure_time']) !!}
               @if ($errors->has('departure_time'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('departure_time')!!}</span>@endif
           </div>
           <div class="col s12 m4 input-field">
              <label for="code">Duration (In hours):</label>
              {!! Form::text('duration', null, ['class' => 'form-control' , 'required', 'id' => 'duration']) !!}
              @if ($errors->has('duration'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('duration')!!}</span>@endif
           </div>-->
           <div class="col s12 m4 input-field">
              <label for="effective_Date">Schedule Effective Date:</label>
              {!! Form::text('effective_Date', null, ['class' => 'datepicker' , 'required' , 'id' => 'effective_Date']) !!}
               @if ($errors->has('effective_Date'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('effective_Date')!!}</span>@endif
           </div>
           <div class="col s12 m4 input-field">
              <label for="operatingend_date">Operating End Date:</label>
              {!! Form::text('operatingend_date', null, ['class' => 'datepicker' , 'required' , 'id' => 'operatingend_date']) !!}
               @if ($errors->has('operatingend_date'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('operatingend_date')!!}</span>@endif
           </div>
           <div class="col s12 m12">
              <label >Schedule Frequency:</label><br /><br />
              <div class="row">
                <div class="col s12 m4">
                    @if($info_Schedule->monday==1)
                         <input name="monday" value="1" checked="checked" type="checkbox" id="monday" />
                    @else
                         <input name="monday" value="1" type="checkbox" id="monday" />
                    @endif
                    <label for="monday">Monday</label>
                </div>
                <div class="col s12 m4">
                    @if($info_Schedule->tuesday==1)
                         <input name="tuesday" value="1" checked="checked" type="checkbox" id="tuesday" />
                    @else
                         <input name="tuesday" value="1" type="checkbox" id="tuesday" />
                    @endif
                    <label for="tuesday">Tuesday</label>
                </div>
                <div class="col s12 m4">
                    @if($info_Schedule->wednesday==1)
                         <input name="wednesday" value="1" checked="checked" type="checkbox" id="wednesday" />
                    @else
                         <input name="wednesday" value="1" type="checkbox" id="wednesday" />
                    @endif
                    <label for="wednesday">Wednesday</label>
                </div>
                <div class="col s12 m4">
                    @if($info_Schedule->thursday==1)
                         <input name="thursday" value="1" checked="checked" type="checkbox" id="thursday" />
                    @else
                         <input name="thursday" value="1" type="checkbox" id="thursday" />
                    @endif
                    <label for="thursday">Thursday</label>
                </div>
                <div class="col s12 m4">
                    @if($info_Schedule->friday==1)
                         <input name="friday" value="1" checked="checked" type="checkbox" id="friday" />
                    @else
                         <input name="friday" value="1" type="checkbox" id="friday" />
                    @endif
                    <label for="friday">Friday</label>
                </div>
                <div class="col s12 m4">
                    @if($info_Schedule->saturday==1)
                         <input name="saturday" value="1" checked="checked" type="checkbox" id="saturday" />
                    @else
                         <input name="saturday" value="1" type="checkbox" id="saturday" />
                    @endif                    <label for="saturday">Saturday</label>
                </div>
                <div class="col s12 m4">
                    @if($info_Schedule->sunday==1)
                         <input name="sunday" value="1" checked="checked" type="checkbox" id="sunday" />
                    @else
                         <input name="sunday" value="1" type="checkbox" id="sunday" />
                    @endif
                    <label for="sunday">Sunday</label>
                </div>
              </div>
            </div>
            <div class="col s12 m4">
               <br /><br />
               <div class="switch">
                    <label>
                        Inactive
                        @if($info_Schedule->status==1)
                            <input type="checkbox" checked="checked" name="status"/>
                        @else
                            <input type="checkbox" name="status"/>
                        @endif
                        <span class="lever"></span>
                        Active
                    </label>
                </div>
               
            </div>
            <div class="row">
              <div class="input-field col s12">
                <button class="btn cyan waves-effect waves-light right" type="submit">Next
                  <i class="material-icons right">send</i>
                </button>
              </div>
            </div>
       </div>
      </div>
   </div>
</div>
</form>
<!--
************************************
End Schedule
************************************
-->
@endsection

@push('scripts')

<script type="text/javascript">
    @if($guard=="admin")
		$('#CompanySchedule').on('change', function (e) { 
			var company_id = $('#CompanySchedule option:selected').val();
			if(company_id=="")
				return false;
			var baseUrl = $('meta[name="base-url"]').attr('content');
			
			var url = baseUrl+'{{$guard_url}}company/route/'+company_id;
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'json',
				data: {method: '_GET', "_token": "{{ csrf_token() }}" , submit: true},
				success: function (response) {
					$('#CompanyRoute').material_select('destroy');
					$("#CompanyRoute").empty();
					$("#CompanyRoute").append('<option>Select Route(From/To)</option>');
					
					var dataArray = [];
					for (value in response) {
						var word = response[value]['name'];
						dataArray.push({value: parseInt(response[value]['id']), word: word});
					}
					
					dataArray.sort(function(a, b){
						if (a.word < b.word) return -1;
						if (b.word < a.word) return 1;
						return 0;
					});
					
					$.each(dataArray,function(key,value){
						$("#CompanyRoute").append('<option value="'+value.value+'">'+value.word+'</option>');
					});
					
					$("#CompanyRoute").material_select();
				},
				error: function (result, status, err) {
					alert(result.responseText);
					alert(status.responseText);
					alert(err.Message);
				},
			});
			
			var url = baseUrl+'{{$guard_url}}company/bus/'+company_id;
			
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'json',
				data: {method: '_GET', "_token": "{{ csrf_token() }}" , submit: true},
				success: function (response) {
					$('#CompanyBuses').material_select('destroy');
					$("#CompanyBuses").empty();
					$("#CompanyBuses").append('<option value="" disabled>Select Fleet</option>');
					var dataArray = [];
					for (value in response) {
						var word = response[value];
						dataArray.push({value: parseInt(value), word: word});
					}
					
					dataArray.sort(function(a, b){
						if (a.word < b.word) return -1;
						if (b.word < a.word) return 1;
						return 0;
					});
					$.each(dataArray,function(key,value){
						$("#CompanyBuses").append('<option value="'+value.value+'" selected>'+value.word+'</option>');
					});
					$("#CompanyBuses").material_select();
				},
				error: function (result, status, err) {
					alert(result.responseText);
					alert(status.responseText);
					alert(err.Message);
				},
			});
			return false;
		});
		$('#CompanyRoute').on('change', function (e) { 
			var route_id = $('#CompanyRoute option:selected').val();
			if(route_id=="")
				return false;
			var baseUrl = $('meta[name="base-url"]').attr('content');
			
			var url = baseUrl+'{{$guard_url}}company/route_detail/'+route_id;
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'json',
				data: {method: '_GET', "_token": "{{ csrf_token() }}" , submit: true},
				success: function (response) {
					$("#CompanyRouteTerminal").html('');
					
					var loop=1;
					var length = response.length;
					var html = "";
					$.each(response,function(key,value){
						if(loop==1)
						{
							html = '<div class="row" style="border-top: 1px #000 solid; border-bottom: 1px #000 solid;">' +
									'	<div class="col s6 m6">' +
									'		<h6>'+value.terminal_name+':</h6>'+
									'	</div>' +
									'	<div class="col s6 m6">' +
									'		<div class="row">' +
									'			<div class="col s12 m12 input-field">' +
									'				<label for="time_dept_'+value.route_id+'_'+value.terminal_id+'_'+value.sequence+'">Departure</label>' +
									'				<input type="text" class="timepicker" id="time_dept_'+value.route_id+'_'+value.terminal_id+'_'+value.sequence+'" name="time_dept_'+value.route_id+'_'+value.terminal_id+'_'+value.sequence+'" value="" />' +
									'			</div>' +
									'		</div>' +
									'	</div>' +
									'</div>';
						}
						else
						if(loop==length)
						{
							html = '<div class="row" style="border-bottom: 1px #000 solid;">' +
									'	<div class="col s6 m6">' +
									'		<h6>'+value.terminal_name+':</h6>'+
									'	</div>' +
									'	<div class="col s6 m6">' +
									'		<div class="row">' +
									'			<div class="col s12 m12 input-field">' +
									'				<label for="time_arrv_'+value.route_id+'_'+value.terminal_id+'_'+value.sequence+'">Arrival</label>' +
									'				<input type="text" class="timepicker" id="time_arrv_'+value.route_id+'_'+value.terminal_id+'_'+value.sequence+'" value"'+value.terminal_name+'" name="time_arrv_'+value.route_id+'_'+value.terminal_id+'_'+value.sequence+'" value=""/>' +
									'			</div>' +
									'		</div>' +
									'	</div>' +
									'</div>';
						}
						else
						{
							html = '<div class="row" style="border-bottom: 1px #000 solid;">' +
									'	<div class="col s6 m6">' +
									'		<h6>'+value.terminal_name+':</h6>'+
									'	</div>' +
									'	<div class="col s6 m6">' +
									'		<div class="row">' +
									'			<div class="col s12 m12 input-field">' +
									'				<label for="time_arrv_'+value.route_id+'_'+value.terminal_id+'_'+value.sequence+'">Arrival</label>' +
									'				<input type="text" class="timepicker" id="time_arrv_'+value.route_id+'_'+value.terminal_id+'_'+value.sequence+'" name="time_arrv_'+value.route_id+'_'+value.terminal_id+'_'+value.sequence+'" value="" />' +
									'			</div>' +
									'		</div>' +
									'		<div class="row">' +
									'			<div class="col s12 m12 input-field">' +
									'				<label for="time_dept_'+value.route_id+'_'+value.terminal_id+'_'+value.sequence+'">Departure</label>' +
									'				<input type="text" class="timepicker" id="time_dept_'+value.route_id+'_'+value.terminal_id+'_'+value.sequence+'" name="time_dept_'+value.route_id+'_'+value.terminal_id+'_'+value.sequence+'" value="" />' +
									'			</div>' +
									'		</div>' +
									'	</div>' +
									'</div>';
						}
						loop++;
						$("#CompanyRouteTerminal").append(html);
					});
					
					$('.timepicker').pickatime({
						default: 'now', // Set default time
						fromnow: 0, // set default time to * milliseconds from now (using with default = 'now')
						twelvehour: true, // Use AM/PM or 24-hour format
						donetext: 'OK', // text for done-button
						cleartext: 'Clear', // text for clear-button
						canceltext: 'Cancel', // Text for cancel-button
						autoclose: false, // automatic close timepicker
						ampmclickable: true, // make AM PM clickable
						aftershow: function() {} //Function for after opening timepicker
				  	});
				},
				error: function (result, status, err) {
					alert(result.responseText);
					alert(status.responseText);
					alert(err.Message);
				},
			});
		});
	@else
		$( document ).ready(function() {
			var company_id = $("input[name='company_id']").val();	
			if(company_id=="")
				return false;
			var baseUrl = $('meta[name="base-url"]').attr('content');
			
			var url = baseUrl+'{{$guard_url}}company/route/'+company_id;
			
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'json',
				data: {method: '_GET', "_token": "{{ csrf_token() }}" , submit: true},
				success: function (response) {
					//$('#CompanyRoute').material_select('destroy');
					//$("#CompanyRoute").empty();
					$("#CompanyRoute").append('<option>Select Route(From/To)</option>');
					
					var dataArray = [];
					for (value in response) {
						var word = response[value]['name'];
						dataArray.push({value: parseInt(response[value]['id']), word: word});
					}
					
					dataArray.sort(function(a, b){
						if (a.word < b.word) return -1;
						if (b.word < a.word) return 1;
						return 0;
					});
					
					$.each(dataArray,function(key,value){
						$("#CompanyRoute").append('<option value="'+value.value+'">'+value.word+'</option>');
					});
					
					//$("#CompanyRoute").material_select();
				},
				error: function (result, status, err) {
					alert(result.responseText);
					alert(status.responseText);
					alert(err.Message);
				},
			});
			
			var url = baseUrl+'{{$guard_url}}company/bus/'+company_id;
			
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'json',
				data: {method: '_GET', "_token": "{{ csrf_token() }}" , submit: true},
				success: function (response) {
					$('#CompanyBuses').material_select('destroy');
					$("#CompanyBuses").empty();
					$("#CompanyBuses").append('<option value="" disabled>Select Fleet</option>');
					var dataArray = [];
					for (value in response) {
						var word = response[value];
						dataArray.push({value: parseInt(value), word: word});
					}
					
					dataArray.sort(function(a, b){
						if (a.word < b.word) return -1;
						if (b.word < a.word) return 1;
						return 0;
					});
					$.each(dataArray,function(key,value){
						$("#CompanyBuses").append('<option value="'+value.value+'" selected>'+value.word+'</option>');
					});
					$("#CompanyBuses").material_select();
				},
				error: function (result, status, err) {
					alert(result.responseText);
					alert(status.responseText);
					alert(err.Message);
				},
			});
		});
		$('#CompanyRoute').on('change', function (e) { 
			var route_id = $('#CompanyRoute option:selected').val();
			if(route_id=="")
				return false;
			var baseUrl = $('meta[name="base-url"]').attr('content');
			
			var url = baseUrl+'{{$guard_url}}company/route_detail/'+route_id;
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'json',
				data: {method: '_GET', "_token": "{{ csrf_token() }}" , submit: true},
				success: function (response) {
					$("#CompanyRouteTerminal").html('');
					
					var loop=1;
					var length = response.length;
					var html = "";
					$.each(response,function(key,value){
						if(loop==1)
						{
							html = '<div class="row" style="border-top: 1px #000 solid; border-bottom: 1px #000 solid;">' +
									'	<div class="col s6 m6">' +
									'		<h6>'+value.terminal_name+':</h6>'+
									'	</div>' +
									'	<div class="col s6 m6">' +
									'		<div class="row">' +
									'			<div class="col s12 m12 input-field">' +
									'				<label for="time_dept_'+value.route_id+'_'+value.terminal_id+'_'+value.sequence+'">Departure</label>' +
									'				<input type="text" class="timepicker" id="time_dept_'+value.route_id+'_'+value.terminal_id+'_'+value.sequence+'" name="time_dept_'+value.route_id+'_'+value.terminal_id+'_'+value.sequence+'" value="" />' +
									'			</div>' +
									'		</div>' +
									'	</div>' +
									'</div>';
						}
						else
						if(loop==length)
						{
							html = '<div class="row" style="border-bottom: 1px #000 solid;">' +
									'	<div class="col s6 m6">' +
									'		<h6>'+value.terminal_name+':</h6>'+
									'	</div>' +
									'	<div class="col s6 m6">' +
									'		<div class="row">' +
									'			<div class="col s12 m12 input-field">' +
									'				<label for="time_arrv_'+value.route_id+'_'+value.terminal_id+'_'+value.sequence+'">Arrival</label>' +
									'				<input type="text" class="timepicker" id="time_arrv_'+value.route_id+'_'+value.terminal_id+'_'+value.sequence+'" value"'+value.terminal_name+'" name="time_arrv_'+value.route_id+'_'+value.terminal_id+'_'+value.sequence+'" value=""/>' +
									'			</div>' +
									'		</div>' +
									'	</div>' +
									'</div>';
						}
						else
						{
							html = '<div class="row" style="border-bottom: 1px #000 solid;">' +
									'	<div class="col s6 m6">' +
									'		<h6>'+value.terminal_name+':</h6>'+
									'	</div>' +
									'	<div class="col s6 m6">' +
									'		<div class="row">' +
									'			<div class="col s12 m12 input-field">' +
									'				<label for="time_arrv_'+value.route_id+'_'+value.terminal_id+'_'+value.sequence+'">Arrival</label>' +
									'				<input type="text" class="timepicker" id="time_arrv_'+value.route_id+'_'+value.terminal_id+'_'+value.sequence+'" name="time_arrv_'+value.route_id+'_'+value.terminal_id+'_'+value.sequence+'" value="" />' +
									'			</div>' +
									'		</div>' +
									'		<div class="row">' +
									'			<div class="col s12 m12 input-field">' +
									'				<label for="time_dept_'+value.route_id+'_'+value.terminal_id+'_'+value.sequence+'">Departure</label>' +
									'				<input type="text" class="timepicker" id="time_dept_'+value.route_id+'_'+value.terminal_id+'_'+value.sequence+'" name="time_dept_'+value.route_id+'_'+value.terminal_id+'_'+value.sequence+'" value="" />' +
									'			</div>' +
									'		</div>' +
									'	</div>' +
									'</div>';
						}
						loop++;
						$("#CompanyRouteTerminal").append(html);
					});
					
					$('.timepicker').pickatime({
						default: 'now', // Set default time
						fromnow: 0, // set default time to * milliseconds from now (using with default = 'now')
						twelvehour: true, // Use AM/PM or 24-hour format
						donetext: 'OK', // text for done-button
						cleartext: 'Clear', // text for clear-button
						canceltext: 'Cancel', // Text for cancel-button
						autoclose: false, // automatic close timepicker
						ampmclickable: true, // make AM PM clickable
						aftershow: function() {} //Function for after opening timepicker
				  	});
				},
				error: function (result, status, err) {
					alert(result.responseText);
					alert(status.responseText);
					alert(err.Message);
				},
			});
		});
	@endif
</script>

@endpush




