@extends('cms.layout.app')
@section('breadcrumbs')
<div id="breadcrumbs-wrapper">
   <!-- Search for small screen -->
   <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
      <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
   </div>
   <div class="container">
      <div class="row">
         <div class="col s10 m6 l6">
            <h5 class="breadcrumbs-title">Show Schedule</h5>
            <ol class="breadcrumbs">
               <li><a href="{{ url($guard_url) }}">Home</a>
               </li>
               <li><a href="{{ url($guard_url.'schedule') }}">schedule</a>
               </li>
            </ol>
         </div>
         <div class="col s2 m6 l6">
            <a class="btn waves-effect waves-light breadcrumbs-btn right" href="{{ url($guard_url) }}" data-activates="dropdown1">
            <i class="material-icons">home</i>
            </a>
            
  <br />
       </div>
      </div>
   </div>
</div>
@endsection
@section('content')
<br />
<div class="divider"></div>
<br />
<div class="row">
   <div class="col s12 m8 l8">
      <div id="flight-card" class="card" style="overflow: hidden;">
          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
            <div class="card-title">
              <h6 class="flight-card-title">Schedule Information</h6>
            </div>
          </div>
         <div class="card-content" style="background-color:#FFF">
           	<div class="col s12 m4 input-field">
               <label class="active">Company Name:</label>
               <p id="lblname">{{ $info_Schedule->Company()->First()->name }}</p>
            </div>
           <div class="col s12 m4 input-field">
              <label class="active">Code:</label>
              <p>{{ $info_Schedule->short_name }}</p>
           </div>
           <div class="col s12 m4 input-field">
              <label class="active">Bus:</label>
              <p>{{ $info_Schedule->Bus()->First()->name }}</p>
           </div>
           <div class="col s12 m4 input-field">
              <label class="active">Schedule Effective Date:</label>
              <p>{{ $info_Schedule->effective_Date }}</p>
           </div>
           <div class="col s12 m4 input-field">
              <label class="active">Operating End Date:</label>
              <p>{{ $info_Schedule->operatingend_date }}</p>
           </div>
           <div class="col s12 m12 input-field">
                <label  class="active">Route:</label>
                <p><b>{{ $info_Schedule->Route()->First()->name }}</b></p>
                <div class="row">
                    <div class="col s12 m6 input-field" id="CompanyRouteTerminal">
                    	<?php 
							$i=1;
							$length = $info_Schedule->Schedule_time()->Get()->Count();
						?>
                        @foreach($info_Schedule->Schedule_time()->Orderby('sequence')->Get() as $Schedult_time)
                            @if($i==1)
                                <div class="row" style="border-bottom: 1px #000 solid; border-top: 1px #000 solid;">
                                   <div class="col s6 m6">
                                      <h6>{{ $Schedult_time->Terminal()->First()->name }}:</h6>
                                   </div>
                                   <div class="col s6 m6">
                                      <div class="row">
                                        <div class="col s12 m12 input-field">				
                                            <label class="active">Departure</label>				
                                            <p>{{ date('h:iA', strtotime($Schedult_time->departure_time)) }}</p>
                                       	</div>
                                      </div>
                                   </div>
                                </div>
                            @elseif($i==$length)
                                <div class="row" style="border-bottom: 1px #000 solid;">
                                   <div class="col s6 m6">
                                      <h6>{{ $Schedult_time->Terminal()->First()->name }}:</h6>
                                   </div>
                                   <div class="col s6 m6">
                                      <div class="row">
                                        <div class="col s12 m12 input-field">				
                                            <label class="active">Arrival</label>				
                                            <p>{{ date('h:iA', strtotime($Schedult_time->arrival_time)) }}</p>			
                                        </div>
                                      </div>
                                   </div>
                                </div>
                            @else
                                <div class="row" style="border-bottom: 1px #000 solid;">
                                   <div class="col s6 m6">
                                      <h6>{{ $Schedult_time->Terminal()->First()->name }}:</h6>
                                   </div>
                                   <div class="col s6 m6">
                                      <div class="row">
                                        <div class="col s12 m12 input-field">				
                                           <label class="active">Arrival</label>				
                                            <p>{{ date('h:iA', strtotime($Schedult_time->arrival_time)) }}</p>			
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col s12 m12 input-field">				
                                            <label class="active">Departure</label>				
                                            <p>{{ date('h:iA', strtotime($Schedult_time->departure_time)) }}</p>			
                                       	</div>
                                      </div>
                                   </div>
                                </div>
                            @endif
                            <?php $i++; ?>
                        @endforeach
                    </div>
                </div>
           </div>
           <div class="col s12 m12">
              <br /><label >Schedule Frequency:</label>
              <div class="row">
                <div class="col s12 m4">
                    <label class="active">Monday</label>
                    <p>@if($info_Schedule->monday==1)
                         Yes
                    @else
                         No
                    @endif</p>
                </div>
                <div class="col s12 m4">
                    <label class="active">Tuesday</label>
                    <p>@if($info_Schedule->tuesday==1)
                         Yes
                    @else
                         No
                    @endif</p>
                </div>
                <div class="col s12 m4">
                    <label class="active">Wednesday</label>
                    <p>@if($info_Schedule->wednesday==1)
                         Yes
                    @else
                         No
                    @endif</p>
                </div>
                <div class="col s12 m4">
                    <label class="active">Thursday</label>
                    <p>@if($info_Schedule->thursday==1)
                         Yes
                    @else
                         No
                    @endif</p>
                </div>
                <div class="col s12 m4">
                    <label class="active">Friday</label>
                    <p>@if($info_Schedule->friday==1)
                         Yes
                    @else
                         No
                    @endif</p>
                </div>
                <div class="col s12 m4">
                    <label class="active">Saturday</label>
                    <p>@if($info_Schedule->saturday==1)
                         Yes
                    @else
                         No
                    @endif</p>
                </div>
                <div class="col s12 m4">
                    <label class="active">Sunday</label>
                    <p>@if($info_Schedule->sunday==1)
                         Yes
                    @else
                         No
                    @endif</p>
                    
                </div>
              </div>
            </div>
            <div class="col s12 m4">
          		<label class="active">Status:</label>
                <p id="lblstatus">{{ ($info_Schedule->status==1?"Active":"Inactive") }}</p>
            </div>
            </div>
       </div>
       
      </div>
   </div>
</div>
<br />
<div class="row">
   <div class="col s12 m8 l8"> 
      <div id="flight-card" class="card">
          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
            <div class="card-title">
              <h6 class="flight-card-title">Fare Type Information</h6>
            </div>
          </div>
         <div class="card-content" style="background-color:#FFF">
           <div class="row">
              <table id="fareForm" class="bordered striped responsive-table display dataTable no-footer" cellspacing="0">
              <thead>
                <tr>
                  <th>Fare Type</th>
                  <th>Services</th>
                </tr>
              </thead>
              <tbody>
              @foreach($info_Schedule->Fare_type()->Get() as $info_FareType)
              	<tr id="Del{{$info_FareType->id}}">
                  <td>{{ $info_FareType->name }}</td>
                  <td>{{ $info_FareType->Service()->First()->name }}</td>
              @endforeach
              </tbody>
              </tr>
            </table>
           </div>
       </div>
      </div>
   </div>
</div>
<br />
<div class="row">
   <div class="col s12 m8 l8"> 
      <div id="flight-card" class="card">
          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
            <div class="card-title">
              <h6 class="flight-card-title">Fare Type Information</h6>
            </div>
          </div>
         <div class="card-content" style="background-color:#FFF">
           <div class="row">
            <div class="col s12 m12">
              	<br />
                <div class="divider"></div>
                <br />
              @foreach($info_Schedule->Fare_type()->Get() as $info_FareType)
              <table id="fareForm" class="bordered striped responsive-table display dataTable no-footer" cellspacing="0" border="1px">
              <thead>
                <tr>
                  <th></th>
                  @foreach($info_Schedule->Route()->First()->Route_detail()->Where('sequence','>','1')->OrderBy('sequence')->Get() as $Route_detail1)
                  <th>{{ $Route_detail1->Terminal()->First()->name }}</th>
                  @endforeach
                </tr>
              </thead>
              <tbody>
                  @foreach($info_Schedule->Route()->First()->Route_detail()->OrderBy('sequence')->Get() as $Route_detail2)
                  @if($info_Schedule->Route()->First()->Route_detail()->Get()->Max('sequence')!=$Route_detail2->sequence)
                      <tr>
                        <th>{{ $Route_detail2->Terminal()->First()->name }}</th>
                        <?php
                        for($i=1; $i<$Route_detail2->sequence; $i++)                       	
                        {
                            echo '<td></td>';
                        }
                        ?>
                        @foreach($info_Schedule->Route()->First()->Route_detail()->Where('sequence','>',$Route_detail2->sequence)->OrderBy('sequence')->Get() as $Route_detail3)
                            <td>
                                    <label class="active">Fare:</label>
                                    <p>{{ $info_Schedule->Fare()->Where(['from_terminal_id' => $Route_detail2->Terminal()->First()->id, 'to_terminal_id' => $Route_detail3->Terminal()->First()->id, 'fare_type_id' => $info_FareType->id])->First()->fare }}</p>
              
                            </td>
                        @endforeach
                      </tr>
                  @endif
                  @endforeach
              </tbody>
              </tr>
            </table>
            @endforeach
            </div>
       </div>
      </div>
   </div>
</div>
@endsection