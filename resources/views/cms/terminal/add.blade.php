@extends('cms.layout.app')
@section('breadcrumbs')
<div id="breadcrumbs-wrapper">
   <!-- Search for small screen -->
   <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
      <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
   </div>
   <div class="container">
      <div class="row">
         <div class="col s10 m6 l6">
            <h5 class="breadcrumbs-title">Add Terminal</h5>
            <ol class="breadcrumbs">
               <li><a href="{{ url($guard_url) }}">Home</a>
               </li>
               <li><a href="{{ url($guard_url.'terminal') }}">Terminal</a>
               </li>
            </ol>
         </div>
         <div class="col s2 m6 l6">
            <a class="btn waves-effect waves-light breadcrumbs-btn right" href="{{ url($guard_url) }}" data-activates="dropdown1">
            <i class="material-icons">home</i>
            </a>
            
         </div>
      </div>
   </div>
</div>
@endsection
@section('content')
{!! Form::open([ 'url' => $guard_url.'terminal/', 'id' => 'main-form' ]) !!}
<br />
<div class="divider"></div>
<br />
<div class="row">
   <div class="col s12 m8 l8">
      <div id="flight-card" class="card">
          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
            <div class="card-title">
              <h6 class="flight-card-title">Terminal Information</h6>
            </div>
          </div>
         <div class="card-content" style="background-color:#FFF">
           	@if($guard=="admin")
            <div class="col s12 m4 input-field">
              <?php
                $defaultSelection = ["" => "Select Company"];
				foreach($info_Companies as $Companies)
                {
                    $defaultSelection = $defaultSelection +  array($Companies->id => ($Companies->name));
                }
              ?>
              {!! Form::select('company_id', $defaultSelection, null, ['class' => 'form-control', 'id' => 'CompanyRoute']) !!}
              @if ($errors->has('company_id'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('company_id')!!}</span>@endif
           </div>
           @else
           <input type="hidden" name="company_id" value="{{ Auth::guard($guard)->User()->Company()->First()->id }}"  />
           @endif
           <div class="col s12 m4 input-field">
              <?php
                $defaultSelection = ["" => "Select Country"];
				foreach($info_Countries as $info_Countries)
                {
                    $defaultSelection = $defaultSelection +  array($info_Countries->id => ($info_Countries->name));
                }

              ?>
              {!! Form::select('country_id', $defaultSelection, null, ['class' => 'form-control', 'id' => 'countryCity']) !!}
              @if ($errors->has('country_id'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('country_id')!!}</span>@endif
           </div>
           <div class="col s12 m4 input-field">
              <?php
                $defaultSelection = ["" => "Select State/City"];
              ?>
              {!! Form::select('city_id', $defaultSelection, null, ['class' => 'form-control', 'id' => 'cityLga']) !!}
              @if ($errors->has('city_id'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('city_id')!!}</span>@endif
           </div>
           <div class="col s12 m4 input-field">
              <?php
                $defaultSelection = ["" => "Select Lga"];
              ?>
              {!! Form::select('lga_id', $defaultSelection, null, ['class' => 'form-control', 'id' => 'Lga']) !!}
              @if ($errors->has('lga_id'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('lga_id')!!}</span>@endif
           </div>
           <div class="col s12 m4 input-field">
              <label for="name">Terminal Name:</label>
              {!! Form::text('name', null, ['class' => 'form-control' , 'required', 'id' => 'name']) !!}
              @if ($errors->has('name'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('name')!!}</span>@endif
           </div>
           <div class="col s12 m4 input-field">
              <label for="code">Code:</label>
              {!! Form::text('short_name', null, ['class' => 'form-control' , 'required', 'id' => 'code']) !!}
              @if ($errors->has('short_name'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('short_name')!!}</span>@endif
           </div>
           <div class="col s12 m4 input-field">
              <label for="name">Email:</label>
              {!! Form::text('email', null, ['class' => 'form-control' , 'required', 'id' => 'email']) !!}
              @if ($errors->has('email'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('email')!!}</span>@endif
           </div>
           <div class="col s12 m4 input-field">
              <label for="name">Phone No.:</label>
              {!! Form::text('phone_number', null, ['class' => 'form-control' , 'required', 'id' => 'phone_number']) !!}
              @if ($errors->has('phone_number'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('phone_number')!!}</span>@endif
           </div>
           <div class="col s12 m4 input-field">
               <label for="address">Address:</label>
               {!! Form::textarea('address', null, ['required' , 'class' => 'materialize-textarea', 'id' => 'address', 'rows' => '5']) !!}
               @if ($errors->has('address'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('address')!!}</span>@endif
           </div>
       	   <div class="col s12 m4">
               <div class="switch">
                    <label>
                    Inactive
                    <input type="checkbox" checked="checked" name="status"/>
                    <span class="lever"></span>
                    Active
                    </label>
                </div>
               
            </div>
            <div class="row">
              <div class="input-field col s12">
                <button class="btn cyan waves-effect waves-light right" type="submit">Submit
                  <i class="material-icons right">send</i>
                </button>
              </div>
            </div>
       </div>
      </div>
   </div>
</div>

</form>
@endsection

