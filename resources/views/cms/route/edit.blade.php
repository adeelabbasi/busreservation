@extends('cms.layout.app')
@section('breadcrumbs')
<div id="breadcrumbs-wrapper">
   <!-- Search for small screen -->
   <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
      <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
   </div>
   <div class="container">
      <div class="row">
         <div class="col s10 m6 l6">
            <h5 class="breadcrumbs-title">Edit Route</h5>
            <ol class="breadcrumbs">
               <li><a href="{{ url($guard_url) }}">Home</a>
               </li>
               <li><a href="{{ url($guard_url.'route') }}">Route</a>
               </li>
            </ol>
         </div>
         <div class="col s2 m6 l6">
            <a class="btn waves-effect waves-light breadcrumbs-btn right" href="{{ url($guard_url) }}" data-activates="dropdown1">
            <i class="material-icons">home</i>
            </a>
            
         </div>
      </div>
   </div>
</div>
@endsection
@section('content')
{!! Form::model($info_Route, ['method' => 'PATCH', 'url' => [$guard_url.'route', $info_Route->id], 'id' => 'main-form']) !!}
<br />
<div class="divider"></div>
<br />
<div class="row">
   <div class="col s12 m8 l8">
      <div id="flight-card" class="card">
          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
            <div class="card-title">
              <h6 class="flight-card-title">Route Information</h6>
            </div>
          </div>
         <div class="card-content" style="background-color:#FFF">
           	@if($guard=="admin")
            <div class="col s12 m4 input-field">
              <?php
                $defaultSelection = ["" => "Select Company"];
				foreach($info_Companies as $Companies)
                {
                    $defaultSelection = $defaultSelection +  array($Companies->id => ($Companies->name));
                }
              ?>
              {!! Form::select('company_id', $defaultSelection, null, ['class' => 'form-control', 'id' => 'CompanyRoute' , 'required']) !!}
              @if ($errors->has('company_id'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('company_id')!!}</span>@endif
           </div>
           @else
           <input type="hidden" name="company_id" value="{{ Auth::guard($guard)->User()->Company()->First()->id }}"  />
           @endif
           <div class="col s12 m4 input-field">
              <label for="name">Route Name:</label>
              {!! Form::text('name', null, ['class' => 'form-control' , 'required', 'id' => 'name']) !!}
              @if ($errors->has('name'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('name')!!}</span>@endif
           </div>
           <div class="col s12 m4 input-field">
              <label for="short_name">Code:</label>
              {!! Form::text('short_name', null, ['class' => 'form-control' , 'required', 'id' => 'code']) !!}
              @if ($errors->has('short_name'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('short_name')!!}</span>@endif
           </div>
           
           <div class="row citiesRow">
           		<input type="hidden" id="sequenceID" value="" />
                <input type="hidden" class="sequenceNo" value="{{ $info_Route->Route_detail->Count() }}" />
				<?php $index=1; ?>
                @foreach($info_Route->Route_detail()->OrderBy('sequence')->Get() as $Route_detail)
           		<div class="addNewCityRow"><div class="col s12 m12 input-field">
                    <div class="col s4 m4 input-field">
                      <select class="form-control RouteFromCity" data-sequence="{{ $index }}" id="RouteFromCity{{ $index }}" name="from_city_id[]" required><option value="" selected="selected">Select State/City</option>
					  <?php
                        foreach($info_Cities as $Cities)
                        {
							if($Route_detail->Terminal()->First()->Lga()->First()->City()->First()->id == $Cities->id)
								echo '<option value="'.$Cities->id.'" selected>'.$Cities->name.'</option>';
							else
								echo '<option value="'.$Cities->id.'">'.$Cities->name.'</option>';	
                        }
                      ?>
                      </select>
                   </div>
                   <div class="col s4 m4 input-field">
                      <select class="form-control CompanyFromTerminal" id="CompanyFromTerminal{{ $index }}" name="terminal_id[]"><option value="" selected="selected" required>Select Terminal</option>
					  <?php
                        foreach($info_Terminals->Where('lga_id',$Route_detail->Terminal()->First()->Lga()->First()->id) as $Terminals)
                        {
							if($Route_detail->Terminal()->First()->Lga()->First()->id == $Terminals->lga_id)
								echo '<option value="'.$Terminals->id.'" selected>'.$Terminals->name.'</option>';
							else
								echo '<option value="'.$Terminals->id.'">'.$Terminals->name.'</option>';	
                        }
                      ?>
                      </select>
                   </div>
                   <div class="col s2 m2 input-field">
                   		@if($index==1)
                        <button type="button" class="btn-floating waves-effect waves-light btn-add-cities"><i class="material-icons">add</i></button>
                        @else
                        <button class="prefix btn-floating waves-effect waves-light red accent-2 btn-remove-cities" type="button"><i class="material-icons">clear</i></button>
                        @endif
                   </div>
                </div></div>
                <?php $index++; ?>
                @endforeach
           </div>
           
       	   <div class="col s12 m4">
           <br />
               <div class="switch">
                    <label>
                        Inactive
                        @if($info_Route->status==1)
                            <input type="checkbox" checked="checked" name="status"/>
                        @else
                            <input type="checkbox" name="status"/>
                        @endif
                        <span class="lever"></span>
                        Active
                    </label>
                </div>
               
            </div>
            <div class="row">
              <div class="input-field col s12">
                <button class="btn cyan waves-effect waves-light right" type="submit">Submit
                  <i class="material-icons right">send</i>
                </button>
              </div>
            </div>
       </div>
      </div>
   </div>
</div>

</form>
@endsection
@push('scripts')

<script type="text/javascript">
	$(document).on('change', '.RouteFromCity[data-sequence]', function (e) { 
		@if($guard=="admin")
		var company_id = $('#CompanyRoute option:selected').val();
		@else
		var company_id = $("input[name='company_id']").val();	
		@endif
		var sequenceID = $(this).data("sequence");
		$("#sequenceID").val(sequenceID);
		var city_id = $('#RouteFromCity'+sequenceID+' option:selected').val();
		var baseUrl = $('meta[name="base-url"]').attr('content');
		var url = baseUrl+'{{$guard_url}}company/city_terminal/'+company_id;
		if(company_id=="")
			return false;
		//alert(company_id)
		$.ajax({
			url: url,
			type: 'GET',
			dataType: 'json',
			data: {method: '_GET', "city_id":city_id, "_token": "{{ csrf_token() }}" , submit: true},
			success: function (response) {
				$('#CompanyFromTerminal'+$("#sequenceID").val()).material_select('destroy');
				$("#CompanyFromTerminal"+$("#sequenceID").val()).empty();
				$("#CompanyFromTerminal"+$("#sequenceID").val()).append('<option>Select Terminal</option>');

				var dataArray = [];
				for (value in response) {
					var word = response[value];
					dataArray.push({value: parseInt(value), word: word});
				}
				
				dataArray.sort(function(a, b){
					if (a.word < b.word) return -1;
					if (b.word < a.word) return 1;
					return 0;
				});
				$.each(dataArray,function(key,value){
					$("#CompanyFromTerminal"+$("#sequenceID").val()).append('<option value="'+value.value+'">'+value.word+'</option>');
				});
				
				$("#CompanyFromTerminal"+$("#sequenceID").val()).material_select();
			},
			error: function (result, status, err) {
				alert(result.responseText);
				alert(status.responseText);
				alert(err.Message);
			},
		});
	});
	
	$('.btn-add-cities').click(function(){
		var index = $('.sequenceNo').val() + 1;
		$('.sequenceNo').val(index);
		$('.citiesRow').append(''+
				'<div class="addNewCityRow"><div class="col s12 m12 input-field"><input type="hidden" class="sequenceNo" value="'+index+'" />'+
                    '<div class="col s4 m4 input-field">'+
                      
					  '<select class="form-control RouteFromCity" data-sequence="'+index+'" id="RouteFromCity'+index+'" name="from_city_id[]" required><option value="" selected="selected">Select State/City</option>' +
					  <?php
                        foreach($info_Cities as $Cities)
                        {
							echo "'<option value=\"".$Cities->id."\">".$Cities->name."</option>' +";
                        }
                      ?>
				   '</select>'+
                   '</div>'+
                   '<div class="col s4 m4 input-field">'+
                      <?php
                        $defaultSelection = ["" => "Select Terminal"];
                      ?>
                   '<select class="form-control CompanyFromTerminal" id="CompanyFromTerminal'+index+'" name="terminal_id[]" required><option value="" selected="selected">Select Terminal</option></select>'+
                   '</div>'+
                   '<div class="col s2 m2 input-field">'+
                   		'<button class="prefix btn-floating waves-effect waves-light red accent-2 btn-remove-cities" type="button"><i class="material-icons">clear</i></button>'+
                  ' </div>'+
               '</div></div>');
		$("#CompanyFromTerminal"+index).material_select();
		$("#RouteFromCity"+index).material_select();
	});
	
	$(document.body).on('click', '.btn-remove-cities' ,function(){
		$(this).closest('.addNewCityRow').remove();
	});
	
	$('#main-form').submit(function() {
		
		var res = true;
		$(".CompanyFromTerminal").each(function() {
			if($('option:selected',this).text()=="Select Terminal")
			{
				$(this).focus();
				res = false;
			}
		});
		
		return res;
	});
</script>

@endpush


