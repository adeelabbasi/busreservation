@extends('cms.layout.app')
@section('breadcrumbs')
<div id="breadcrumbs-wrapper">
   <!-- Search for small screen -->
   <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
      <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
   </div>
   <div class="container">
      <div class="row">
         <div class="col s10 m6 l6">
            <h5 class="breadcrumbs-title">About US</h5>
            <ol class="breadcrumbs">
               <li><a href="{{ url($guard_url) }}">Home</a>
               </li>
               <li><a href="">Add Company Services</a>
               </li>
            </ol>
         </div>
         <div class="col s2 m6 l6">
            <a class="btn waves-effect waves-light breadcrumbs-btn right" href="{{ url($guard_url) }}" data-activates="dropdown1">
            <i class="material-icons">home</i>
            </a>
            
         </div>
      </div>
   </div>
</div>
@endsection
@section('content')
{!! Form::open([ 'url' => $guard_url.'cservice/', 'id' => 'main-form' ]) !!}
<!--Preselecting a tab-->
<div id="preselecting-tab" class="section">
   <div class="row">
      <div class="col s12">
         <div class="row">
            <div class="col s12">
               <ul class="tabs tab-demo-active z-depth-1 cyan">
                    
                    <li class="tab col s4"><a class="white-text waves-effect waves-light" href="javascript:void(0)" onclick="location.href = '{{ url($guard_url.'policy') }}';">Policy</a>
                    </li>
                    <li class="tab col s4"><a class="white-text waves-effect waves-light active" href="javascript:void(0)" onclick="location.href = '{{ url($guard_url.'cservice') }}';">Company Services</a>
                    </li>
                    <li class="tab col s4"><a class="white-text waves-effect waves-light" href="javascript:void(0)" onclick="location.href = '{{ url($guard_url.'about') }}';">About us</a>
                    </li>
               </ul>
            </div>
            <div class="col s12"> 
            	<br />
                <div class="divider"></div>
                <br />             
                <div class="row">
                   <div class="col s12 m8 l8">
                      <div id="flight-card" class="card">
                          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
                            <div class="card-title">
                              <h6 class="flight-card-title">Company Services Information</h6>
                            </div>
                          </div>
                         <div class="card-content" style="background-color:#FFF">
                            @if($guard=="admin")
                            <div class="col s12 m4 input-field">
                              <?php
                                $defaultSelection = ["" => "Select Company"];
                                foreach($info_Companies as $Companies)
                                {
                                    $defaultSelection = $defaultSelection +  array($Companies->id => ($Companies->name));
                                }
                              ?>
                              {!! Form::select('company_id', $defaultSelection, null, ['class' => 'form-control', 'id' => 'CompanyRoute']) !!}
                              @if ($errors->has('company_id'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('company_id')!!}</span>@endif
                           </div>
                           @else
                           <input type="hidden" name="company_id" value="{{ Auth::guard($guard)->User()->Company()->First()->id }}"  />
                           @endif
                            <div class="col s12 m4 input-field">
                                <label for="name">Name:</label>
                                {!! Form::text('name', null, ['class' => 'form-control' , 'required', 'id' => 'name']) !!}
                                @if ($errors->has('name'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('name')!!}</span>@endif
                            </div>
                            <div class="col s12 m4 input-field">
                            <label for="detail">Detail:</label>
                                {!! Form::textarea('detail', null, ['required' , 'class' => 'materialize-textarea', 'id' => 'detail', 'rows' => '5']) !!}
                               @if ($errors->has('detail'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('detail')!!}</span>@endif
                            </div>
                            <div class="col s12 m4">
                            <br />
                               <div class="switch">
                                    <label>
                                    Inactive
                                    <input type="checkbox" checked="checked" name="status"/>
                                    <span class="lever"></span>
                                    Active
                                    </label>
                                </div>
                               
                            </div>
                            <div class="row">
                              <div class="input-field col s12">
                                <button class="btn cyan waves-effect waves-light right" type="submit">Submit
                                  <i class="material-icons right">send</i>
                                </button>
                              </div>
                            </div>
                       </div>
                      </div>
                   </div>
                </div>

			</div>
         </div>
      </div>
   </div>
</div>
</form>
@endsection

