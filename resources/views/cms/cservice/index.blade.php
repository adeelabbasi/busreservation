@extends('cms.layout.app')
@section('breadcrumbs')
<div id="breadcrumbs-wrapper">
   <!-- Search for small screen -->
   <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
      <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
   </div>
   <div class="container">
      <div class="row">
         <div class="col s10 m6 l6">
            <h5 class="breadcrumbs-title">About US</h5>
            <ol class="breadcrumbs">
               <li><a href="{{ url($guard_url) }}">Home</a>
               </li>
               <li><a href="{{ url($guard_url.'cservice') }}">Company Services</a>
               </li>
            </ol>
         </div>
         <div class="col s2 m6 l6">
            <a class="btn waves-effect waves-light breadcrumbs-btn right" href="{{ url($guard_url) }}" data-activates="dropdown1">
            <i class="material-icons">home</i>
            </a>
            
         </div>
      </div>
   </div>
</div>
@endsection
@section('content')
<!--Preselecting a tab-->
<div id="preselecting-tab" class="section">
   <div class="row">
      <div class="col s12">
         <div class="row">
            <div class="col s12">
               <ul class="tabs tab-demo-active z-depth-1 cyan">
                    
                    <li class="tab col s4"><a class="white-text waves-effect waves-light" href="javascript:void(0)" onclick="location.href = '{{ url($guard_url.'policy') }}';">Policy</a>
                    </li>
                    <li class="tab col s4"><a class="white-text waves-effect waves-light active" href="javascript:void(0)" onclick="location.href = '{{ url($guard_url.'cservice') }}';">Company Services</a>
                    </li>
                    <li class="tab col s4"><a class="white-text waves-effect waves-light" href="javascript:void(0)" onclick="location.href = '{{ url($guard_url.'about') }}';">About us</a>
                    </li>
               </ul>
            </div>
            <div class="col s12">
            	<br />
                <div class="divider"></div>
                <br />
               <div class="row">
                   <div class="col s12 m12 l12">
                      <div id="flight-card" class="card">
                          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
                            <div class="card-title">
                              <h5 class="flight-card-title">Company Services detail <span> 
                        @if(Auth::guard($guard)->User()->can('Add Cservice'))<a class="btn-floating waves-effect waves-light right top-add" href="{{ url($guard_url.'cservice/create') }}"><i class="material-icons">add</i></a>@endif</span></h5>
                            </div>
                          </div>
                          <div class="card-content" style="background-color:#FFF">
                            <table id="viewForm" class="bordered striped responsive-table display" cellspacing="0" style="width:100%;">
                              <thead>
                                <tr>
                                  <th>Company</th>
                                  <th>Name</th>
                                  <th>Detail</th>
                                  <th>Status</th>
                                  <th>Action</th>
                                </tr>
                              </thead>
                              <tbody>
                              </tbody>
                              </tr>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                 </div>    
            </div>
         </div>
      </div>
   </div>
</div>

@endsection

@push('scripts')

<script type="text/javascript">
    $('#viewForm').DataTable({
        "processing": true,
        "serverSide": true,
		"ajax": "{{url($guard_url.'cservice/grid')}}",
        "columns": [
			{ data: 'company_id', name: 'company_id' },
			{ data: 'name', name: 'name' },
			{ data: 'detail', name: 'detail' },
			{ data: 'status', name: 'status' },
			{ data: 'edit', name: 'edit', orderable: false, searchable: false }
		],
		
		"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "All"] ],
		dom: 'Blfrtip',
		buttons: [
			{
				extend : 'csv',
				title : 'Policies',
				exportOptions : {
					modifier : {
						// DataTables core
						order : 'index', // 'current', 'applied',
						//'index', 'original'
						page : 'all', // 'all', 'current'
						search : 'none' // 'none', 'applied', 'removed'
					},
					columns: [ 0, 1, 2, 3 ]
				}
			},
			{
				extend : 'print',
				title : 'All Values',
				exportOptions : {
					modifier : {
						// DataTables core
						order : 'index', // 'current', 'applied',
						//'index', 'original'
						page : 'all', // 'all', 'current'
						search : 'none' // 'none', 'applied', 'removed'
					},
					columns: [ 0, 1, 2, 3 ]
				}
			}
		],
		//order: [ [0, 'desc'] ]
    });
</script>

@endpush