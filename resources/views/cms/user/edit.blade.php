@extends('cms.layout.app')
@section('breadcrumbs')
<div id="breadcrumbs-wrapper">
   <!-- Search for small screen -->
   <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
      <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
   </div>
   <div class="container">
      <div class="row">
         <div class="col s10 m6 l6">
            <h5 class="breadcrumbs-title">Edit User</h5>
            <ol class="breadcrumbs">
               <li><a href="{{ url($guard_url) }}">Home</a>
               </li>
               <li><a href="{{ url($guard_url.'user') }}">User</a>
               </li>
            </ol>
         </div>
         <div class="col s2 m6 l6">
            <a class="btn waves-effect waves-light breadcrumbs-btn right" href="{{ url($guard_url) }}" data-activates="dropdown1">
            <i class="material-icons">home</i>
            </a>
            
         </div>
      </div>
   </div>
</div>
@endsection
@section('content')
{!! Form::model($info_User, ['method' => 'PATCH', 'url' => [$guard_url.'user', $info_User->id], 'id' => 'main-form']) !!}
<br />
<div class="divider"></div>
<br />
<div class="row">
   <div class="col s12 m8 l8">
      <div id="flight-card" class="card">
          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
            <div class="card-title">
              <h6 class="flight-card-title">User Information</h6>
            </div>
          </div>
         <div class="card-content" style="background-color:#FFF">
           @if($guard=="admin")
           <div class="col s12 m4 input-field">
               <?php
                $defaultSelection = ["" => "Select Parent", "-1" => "Super Admin"];
				foreach($info_Companies as $Companies)
                {
                    $defaultSelection = $defaultSelection +  array($Companies->id => ($Companies->name));
                }
                ?>
                {!! Form::select('company_id', $defaultSelection, null, ['class' => 'form-control', 'id' => 'CompanyRole']) !!}
                @if ($errors->has('company_id'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('company_id')!!}</span>@endif
           </div>
           @else
           <input type="hidden" name="company_id" value="{{ Auth::guard($guard)->User()->Company()->First()->id }}"  />
           @endif
           <div class="col s12 m4 input-field">
              <label for="name">Name:</label>
              {!! Form::text('name', null, ['class' => 'form-control' , 'required', 'id' => 'name']) !!}
              @if ($errors->has('name'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('name')!!}</span>@endif
           </div>
           <div class="col s12 m4 input-field">
              <label for="username">Username:</label>
              {!! Form::text('username', null, ['class' => 'form-control' , 'required', 'id' => 'username']) !!}
              @if ($errors->has('username'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('username')!!}</span>@endif
           </div>
           <div class="col s12 m4 input-field">
              <label>Email</label>
              {!! Form::text('email', null, ['class' => 'form-control' , 'required' , 'placeholder' => 'Email', 'id' => 'email']) !!}
              @if ($errors->has('email'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('email')!!}</span>@endif
           </div>
           <div class="col s12 m4 input-field">
              <label for="password">Password</label>
              {!! Form::text('password', $info_User->cpassword, ['class' => 'form-control' , 'required', 'id' => 'password']) !!}
              @if ($errors->has('password'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('password')!!}</span>@endif
           </div>
           <div class="col s12 m4 input-field">
              <?php
                $defaultSelection = ["" => "Select Role "];
				foreach($info_Roles as $Roles)
                {
                    $defaultSelection = $defaultSelection +  array($Roles->id => ($Roles->name));
                }
              ?>
              {!! Form::select('role_id', $defaultSelection, null, ['class' => 'form-control', 'required', 'id' => 'userRoles']) !!}
              @if ($errors->has('role_id'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('role_id')!!}</span>@endif
           </div>
    	   <div class="col s12 m4">
           <br />
               <div class="switch">
                    <label>
                        Inactive
                        @if($info_User->status==1)
                            <input type="checkbox" checked="checked" name="status"/>
                        @else
                            <input type="checkbox" name="status"/>
                        @endif
                        <span class="lever"></span>
                        Active
                    </label>
                </div>
            </div>
            <div class="row">
              <div class="input-field col s12">
                <button class="btn cyan waves-effect waves-light right" type="submit">Submit
                  <i class="material-icons right">send</i>
                </button>
              </div>
            </div>
       </div>
      </div>
   </div>
</div>

</form>
@endsection
@push('scripts')

<script type="text/javascript">
    $('#CompanyRole').on('change', function (e) { 
	var company_id = $('#CompanyRole option:selected').val();
	if(company_id=="")
		return false;
	
	var baseUrl = $('meta[name="base-url"]').attr('content');
	
	var url = baseUrl+'/admin/role/user/'+company_id;
	//alert(url)
	$.ajax({
		url: url,
		type: 'GET',
		dataType: 'json',
		data: {method: '_GET', "_token": "{{ csrf_token() }}" , submit: true},
		success: function (response) {
			$('#userRoles').material_select('destroy');
			$("#userRoles").empty();
			$("#userRoles").append('<option>Select Role</option>');

			$.each(response,function(key,value){
				$("#userRoles").append('<option value="'+key+'">'+value+'</option>');
			});
			
			$("#userRoles").material_select();
		},
		error: function (result, status, err) {
			alert(result.responseText);
			alert(status.responseText);
			alert(err.Message);
		},
	});
	return false;
});
</script>

@endpush





