@extends('cms.layout.app')
@section('breadcrumbs')
<div id="breadcrumbs-wrapper">
   <!-- Search for small screen -->
   <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
      <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
   </div>
   <div class="container">
      <div class="row">
         <div class="col s10 m6 l6">
            <h5 class="breadcrumbs-title">User Log</h5>
            <ol class="breadcrumbs">
               <li><a href="{{ url($guard_url) }}">Home</a>
               </li>
            </ol>
         </div>
         <div class="col s2 m6 l6">
            <a class="btn waves-effect waves-light breadcrumbs-btn right" href="{{ url($guard_url) }}" data-activates="dropdown1">
            <i class="material-icons">home</i>
            </a>
            
         </div>
      </div>
   </div>
</div>
@endsection
@section('content')
<div class="row">
   <div class="col s12 m12 l12">
      <div id="flight-card" class="card">
          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
            <div class="card-title">
              <h5 class="flight-card-title">Users log detail</h5>
            </div>
          </div>
          <div class="row card-content" style="background-color:#FFF">
            <table id="viewForm" class="bordered striped responsive-table display" cellspacing="0">
              <thead>
                <tr>
                  <th>Parent</th>
                  <th>Name</th>
                  <th>Action</th>
                  <th>Date</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
             </table>
 		  </div>
        </div>
      </div>
    </div>
 </div>     

@endsection

@push('scripts')

<script type="text/javascript">
    $('#viewForm').DataTable({
        "processing": true,
        "serverSide": true,
		"ajax": "{{url($guard_url.'userloggrid')}}",
        "columns": [
			{ data: 'parent', name: 'parent' },
			{ data: 'admin_id', name: 'admin_id' },
			{ data: 'action', name: 'action' },
			{ data: 'created_at', name: 'created_at' },
		],
		"responsive": true,
		dom: 'Bfrtip',
		buttons: [
			'copy', 'csv'
		],
		order: [ [3, 'desc'] ]
    });
</script>

@endpush