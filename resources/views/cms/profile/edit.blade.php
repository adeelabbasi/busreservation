@extends('cms.layout.app')
@section('breadcrumbs')
<div id="breadcrumbs-wrapper">
   <!-- Search for small screen -->
   <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
      <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
   </div>
   <div class="container">
      <div class="row">
         <div class="col s10 m6 l6">
            <h5 class="breadcrumbs-title">Profile</h5>
            <ol class="breadcrumbs">
               <li><a href="{{ url($guard_url) }}">Home</a>
               </li>
            </ol>
         </div>
         <div class="col s2 m6 l6">
            <a class="btn waves-effect waves-light breadcrumbs-btn right" href="{{ url($guard_url) }}" data-activates="dropdown1">
            <i class="material-icons">home</i>
            </a>
            
         </div>
      </div>
   </div>
</div>
@endsection
@section('content')
{!! Form::model(Auth::guard($guard)->User(), ['method' => 'PATCH', 'url' => [$guard_url.'profile', Auth::guard($guard)->User()->id], 'files' => true,'id' => 'main-form']) !!}
<br />
<div class="divider"></div>
<br />
<div class="row">
   <div class="col s12 m2 l2 card-panel">
       <div class="profile_img">
          <div id="crop-avatar">
          	@if(Auth::guard($guard)->User()->avatar=="")
            	<img src="{{ asset('media/avatar/').'/demo_user.png' }}" alt="" class="circle z-depth-2 responsive-img activator gradient-45deg-white-cyan">
            @else
            	<img src="{{ asset('media/avatar/').'/'.Auth::guard($guard)->User()->avatar }}" alt="" class="circle z-depth-2 responsive-img activator gradient-45deg-white-cyan">	
            @endif
          </div>
       </div>
    </div>
   <div class="col s12 m8 l8">
   		<div id="flight-card" class="card">
          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
            <div class="card-title">
              <h6 class="flight-card-title">Profile Information</h6>
            </div>
          </div>
          <div class="card-content" style="background-color:#FFF">
              <div class="row">
                <div class="col s12 m4 input-field">
                  <label for="exampleInputEmail1">Name</label>
                  {!! Form::text('name', null, ['class' => 'form-control' , 'required', 'id' => 'name']) !!}
                  @if ($errors->has('name'))<p style="color:red;">{!!$errors->first('name')!!}</p>@endif
               </div>
               <div class="col s12 m4 input-field">
                  <label for="exampleInputEmail1">Username</label>
                  {!! Form::text('username', null, ['class' => 'form-control' , 'required', 'id' => 'username']) !!}
                  @if ($errors->has('username'))<p style="color:red;">{!!$errors->first('username')!!}</p>@endif
               </div>
               <div class="col s12 m4 input-field">
                  <label for="exampleInputEmail1">Email</label>
                  {!! Form::text('email', null, ['class' => 'form-control' , 'required', 'id' => 'email']) !!}
                  @if ($errors->has('email'))<p style="color:red;">{!!$errors->first('email')!!}</p>@endif
               </div>
               <div class="col s12 m4 input-field">
                  <label for="exampleInputEmail1">Password</label>
                  {!! Form::password('password', ['class' => 'form-control']) !!}
                  @if ($errors->has('password'))<p style="color:red;">{!!$errors->first('password')!!}</p>@endif
               </div>
               <div class="col s12 m4 input-field">
                  <label for="exampleInputEmail1">Confirm Password</label>
                  {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                  @if ($errors->has('password_confirmation'))<p style="color:red;">{!!$errors->first('password_confirmation')!!}</p>@endif
               </div>
               <div class="col s12 m4 input-field">
                   <label class="active">Picture:</label><br />
                   {!! Form::file('avatar', null, ['class' => 'form-control', 'required', 'id' => 'avatar']) !!}
                   @if ($errors->has('avatar'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('avatar')!!}</span>@endif
                </div>
           </div>
           <div class="row">
              <div class="input-field col s12">
                <button class="btn cyan waves-effect waves-light right" type="submit">Submit
                  <i class="material-icons right">send</i>
                </button>
              </div>
            </div>
            </div>
          </div>
        </div>
   </div>
</div>

</form>
@endsection

