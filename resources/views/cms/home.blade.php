@extends('cms.layout.app')

@section('content')
<?php
$gradientColor = array("light-blue-cyan", "green-teal", "semi-dark", "red-pink", "amber-amber", "indigo-light-blue",  "blue-grey-blue-grey", "orange-amber", "purple-amber", "purple-light-blue","purple-deep-orange", "blue-indigo", "purple-deep-purple",  "deep-orange-orange", "cyan-cyan", "yellow-teal", "indigo-blue","cyan-light-gree",  "indigo-purple", "deep-purple-blue", "blue-grey-blue");
$i=0;
?>
<!-- main-menu start-->
<div id="main-menu">
	<div class="row">
    	<div class="col s12 m9">
            <div class="row">
              @if(Auth::guard($guard)->User()->can('Dashboard'))
              <div class="col s6 m3">
                <a href="{{ url($guard_url.'dashboard') }}" class="white-text lighten-4">
                <div class="card gradient-shadow gradient-45deg-{{ $gradientColor[$i] }} border-round">
                  <div class="card-content center">
                    <img class="card gradient-shadow gradient-45deg-{{ $gradientColor[$i] }} width-80 border-round" style="" src="{{ asset('material/images/icon/dashboard.png') }}">
                    <p>
                    <a href="{{ url($guard_url.'dashboard') }}" class="white-text lighten-4">Dashboard</a> </p> 
                  </div>
                </div>
                </a>
              </div>
              @endif
              <?php $i++; ?>
              
              @if($guard=="admin")
                  @if(Auth::guard($guard)->User()->can('View Company') || Auth::guard($guard)->User()->can('View Country') || Auth::guard($guard)->User()->can('View State/City') || Auth::guard($guard)->User()->can('View LGA/Municipal'))
                  <div class="col s6 m3">
                    <div class="card gradient-shadow gradient-45deg-{{ $gradientColor[$i] }} border-round">
                      <div class="card-content center" style="overflow: hidden;">
                        <p><a href="javascript:void(0)" class="activator white-text lighten-4"><img src="{{ asset('material/images/icon/setup.png') }}" class="card gradient-shadow gradient-45deg-{{ $gradientColor[$i] }} width-80 border-round">
                        Setup</a></p> 
                      </div>
                      <div class="card-reveal gradient-shadow gradient-45deg-green-teal border-radius-3" style="padding:0px; display: none; transform: translateY(0px);">
                          
                          <div class="collection">
                            <a href="#!" class="collection-item">
                                <span class="task-card-title card-title grey-text text-darken-4">
                                    <i class="material-icons right">close</i>&nbsp;
                                </span> 
                            </a>
                            @if(Auth::guard($guard)->User()->can('View Company'))
                            <a href="{{ url($guard_url.'company') }}" class="collection-item">Company</a>
                            @endif
                            @if(Auth::guard($guard)->User()->can('View Country'))
                            <a href="{{ url($guard_url.'country') }}" class="collection-item">Country</a>
                            @endif
                            @if(Auth::guard($guard)->User()->can('View State/City'))
                            <a href="{{ url($guard_url.'city') }}" class="collection-item">State/City</a>
                            @endif
                            @if(Auth::guard($guard)->User()->can('View LGA/Municipal'))
                            <a href="{{ url($guard_url.'lga') }}" class="collection-item">LGA/Municipal</a>
                            @endif
                          </div>
                       </div>
                    </div>
                  </div>
                  @endif
              @endif
              <?php $i++; ?>
              
              @if(Auth::guard($guard)->User()->can('View Terminal'))
              <div class="col s6 m3">
                <a href="{{ url($guard_url.'terminal') }}" class="white-text lighten-4">
                <div class="card gradient-shadow gradient-45deg-{{ $gradientColor[$i] }} border-round">
                  <div class="card-content center">
                    <img src="{{ asset('material/images/icon/terminal.png') }}" class="card gradient-shadow gradient-45deg-{{ $gradientColor[$i] }} width-80 border-round">
                    <p><a href="{{ url($guard_url.'terminal') }}" class="white-text lighten-4">Terminal</a></p>   
                  </div>
                </div>
                </a>
              </div>
              @endif
              <?php $i++; ?>
              
              @if(Auth::guard($guard)->User()->can('View Fleet'))
              <div class="col s6 m3">
                <a href="{{ url($guard_url.'bus') }}" class="white-text lighten-4">
                <div class="card gradient-shadow gradient-45deg-{{ $gradientColor[$i] }} border-round">
                  <div class="card-content center">
                    <img src="{{ asset('material/images/icon/bus.png') }}" class="card gradient-shadow gradient-45deg-{{ $gradientColor[$i] }} width-80 border-round">
                    <p><a href="{{ url($guard_url.'bus') }}" class="white-text lighten-4">Fleet</a></p> 
                  </div>
                </div>
                </a>
              </div>
              @endif
              <?php $i++; ?>
              
              @if(Auth::guard($guard)->User()->can('View Service'))
              <div class="col s6 m3">
                <a href="{{ url($guard_url.'service') }}" class="white-text lighten-4">
                <div class="card gradient-shadow gradient-45deg-{{ $gradientColor[$i] }} border-round">
                  <div class="card-content center">
                    <img src="{{ asset('material/images/icon/service.png') }}" class="card gradient-shadow gradient-45deg-{{ $gradientColor[$i] }} width-80 border-round">
                    <p><a href="{{ url($guard_url.'service') }}" class="white-text lighten-4">Service</a></p>   
                  </div>
                </div>
                </a>
              </div>

              @endif
              <?php $i++; ?>
              
              @if(Auth::guard($guard)->User()->can('View Route'))
              <div class="col s6 m3">
                <a href="{{ url($guard_url.'route') }}" class="white-text lighten-4">
                <div class="card gradient-shadow gradient-45deg-{{ $gradientColor[$i] }} border-round">
                  <div class="card-content center">
                    <img src="{{ asset('material/images/icon/route.png') }}" class="card gradient-shadow gradient-45deg-{{ $gradientColor[$i] }} width-80 border-round">
                    <p><a href="{{ url($guard_url.'route') }}" class="white-text lighten-4">Route</a></p>   
                  </div>
                </div>
                </a>
              </div>
              @endif
              <?php $i++; ?>
              
              @if(Auth::guard($guard)->User()->can('View Schedule'))
              <div class="col s6 m3">
                <a href="{{ url($guard_url.'schedule') }}" class="white-text lighten-4"> 
                <div class="card gradient-shadow gradient-45deg-{{ $gradientColor[$i] }} border-round">
                  <div class="card-content center">
                    <img src="{{ asset('material/images/icon/schedule.png') }}" class="card gradient-shadow gradient-45deg-{{ $gradientColor[$i] }} width-80 border-round">
                    <p><a href="{{ url($guard_url.'schedule') }}" class="white-text lighten-4">Book Now</a></p>   
                  </div>
                </div>
                </a>
              </div>
              @endif
              <?php $i++; ?>
              
              @if(Auth::guard($guard)->User()->can('View User') || Auth::guard($guard)->User()->can('Add User') || Auth::guard($guard)->User()->can('View User Role') || Auth::guard($guard)->User()->can('View User Activity'))
              <div class="col s6 m3">
                <div class="card gradient-shadow gradient-45deg-{{ $gradientColor[$i] }} border-round">
                  <div class="card-content center" style="overflow: hidden;">
                    <p><a href="javascript:void(0)" class="activator white-text lighten-4"><img src="{{ asset('material/images/icon/user.png') }}" class="card gradient-shadow gradient-45deg-{{ $gradientColor[$i] }} width-80 border-round">
                    &nbsp;User&nbsp;</a></p> 
                  </div>
                  <div class="card-reveal gradient-shadow gradient-45deg-green-teal border-radius-3" style="padding:0px; display: none; transform: translateY(0px);">
                      
                      <div class="collection">
                      	<a href="#!" class="collection-item">
                        	<span class="task-card-title card-title grey-text text-darken-4">
                    			<i class="material-icons right">close</i>&nbsp;
		                    </span> 
                        </a>
                        @if(Auth::guard($guard)->User()->can('View User'))
                        <a href="{{ url($guard_url.'user') }}" class="collection-item">User</a>
                        @endif
                        @if(Auth::guard($guard)->User()->can('Add User'))
                        <a href="{{ url($guard_url.'user/create') }}" class="collection-item">Add User</a>
                        @endif
                        @if(Auth::guard($guard)->User()->can('View User Role'))
                        <a href="{{ url($guard_url.'role') }}" class="collection-item">Role</a>
                        @endif
                        @if(Auth::guard($guard)->User()->can('View User Activity'))
                        <a href="{{ url($guard_url.'userlog') }}" class="collection-item">User Activity</a>
                        @endif
                      </div>
                   </div>
                </div>
              </div>
              @endif
              <?php $i++; ?>
              
              @if(Auth::guard($guard)->User()->can('View Customer') || Auth::guard($guard)->User()->can('View Customer Activity'))
              <div class="col s6 m3">
                <div class="card gradient-shadow gradient-45deg-{{ $gradientColor[$i] }} border-round">
                  <div class="card-content center" style="overflow: hidden;">
                    <p><a href="javascript:void(0)" class="activator white-text lighten-4"><img src="{{ asset('material/images/icon/customer.png') }}" class="card gradient-shadow gradient-45deg-{{ $gradientColor[$i] }} width-80 border-round">
                    Customer</a></p> 
                  </div>
                  <div class="card-reveal gradient-shadow gradient-45deg-green-teal border-radius-3" style="padding:0px; display: none; transform: translateY(0px);">
                      
                      <div class="collection">
                      	<a href="#!" class="collection-item">
                        	<span class="task-card-title card-title grey-text text-darken-4">
                    			<i class="material-icons right">close</i>&nbsp;
		                    </span> 
                        </a>
                        @if(Auth::guard($guard)->User()->can('View Customer'))
                        <a href="{{ url($guard_url.'customer') }}" class="collection-item">Customer</a>
                        @endif
                        @if(Auth::guard($guard)->User()->can('View Customer Activity'))
                        <a href="{{ url($guard_url.'customerlog') }}" class="collection-item">Customer Activity</a>
                        @endif
                      </div>
                   </div>
                </div>
              </div>
              @endif
              <?php $i++; ?>
              
              @if(Auth::guard($guard)->User()->can('View Subscription'))
              <div class="col s6 m3">
                <a href="{{ url($guard_url.'subscription') }}" class="white-text lighten-4">
                <div class="card gradient-shadow gradient-45deg-{{ $gradientColor[$i] }} border-round">
                  <div class="card-content center">
                    <img src="{{ asset('material/images/icon/subscription.png') }}" class="card gradient-shadow gradient-45deg-{{ $gradientColor[$i] }} width-80 border-round">
                    <p><a href="{{ url($guard_url.'subscription') }}" class="white-text lighten-4">Subscription</a></p>   
                  </div>
                </div>
                </a>
              </div>
              @endif
              <?php $i++; ?>
              
              @if(Auth::guard($guard)->User()->can('View Policy'))
              <div class="col s6 m3">
                <a href="{{ url($guard_url.'policy') }}" class="white-text lighten-4">
                <div class="card gradient-shadow gradient-45deg-{{ $gradientColor[$i] }} border-round">
                  <div class="card-content center">
                    <img src="{{ asset('material/images/icon/policy.png') }}" class="card gradient-shadow gradient-45deg-{{ $gradientColor[$i] }} width-80 border-round">
                    <p><a href="{{ url($guard_url.'policy') }}" class="white-text lighten-4">About US</a></p> 
                  </div>
                </div>
                </a>
              </div>
              @endif
              <?php $i++; ?>
              
              @if(Auth::guard($guard)->User()->can('View Promotion/Deals'))
              <div class="col s6 m3">
                <a href="{{ url($guard_url.'deal') }}" class="white-text lighten-4">
                <div class="card gradient-shadow gradient-45deg-{{ $gradientColor[$i] }} border-round">
                  <div class="card-content center">
                    <img src="{{ asset('material/images/icon/deal.png') }}" class="card gradient-shadow gradient-45deg-{{ $gradientColor[$i] }} width-80 border-round">
                    <p><a href="{{ url($guard_url.'deal') }}" class="white-text lighten-4">Promotion/Deals</a></p>   
                  </div>
                </div>
                </a>
              </div>
              @endif
              <?php $i++; ?>
              @if(Auth::guard($guard)->User()->can('View Booking'))
              <div class="col s6 m3">
                <a href="{{ url($guard_url.'booking') }}" class="white-text lighten-4">
                <div class="card gradient-shadow gradient-45deg-{{ $gradientColor[$i] }} border-round">
                  <div class="card-content center">
                    <img src="{{ asset('material/images/icon/booking.png') }}" class="card gradient-shadow gradient-45deg-{{ $gradientColor[$i] }} width-80 border-round">
                    <p><a href="{{ url($guard_url.'booking') }}" class="white-text lighten-4">Booking</a></p>   
                  </div>
                </div>
                </a>
              </div>
              @endif
              <?php $i++; ?>
              @if(Auth::guard($guard)->User()->can('View Payment'))
              <div class="col s6 m3">
                <a href="{{ url($guard_url.'payment') }}" class="white-text lighten-4">
                <div class="card gradient-shadow gradient-45deg-{{ $gradientColor[$i] }} border-round">
                  <div class="card-content center">
                    <img src="{{ asset('material/images/icon/payment.png') }}" class="card gradient-shadow gradient-45deg-{{ $gradientColor[$i] }} width-80 border-round">
                    <p><a href="{{ url($guard_url.'payment') }}" class="white-text lighten-4">Payment</a></p>   
                  </div>
                </div>
                </a>
              </div>
              @endif
            </div>
      	</div>
        
        
        <!--Right stats-->
        <div class="col s12 m3" style="background:#FFF">
        	<h6 class="mt-5 mb-3 ml-3">SYSTEM</h6>
        	<div class="row">
                <div class="col s12 m12">
                  <div class="card gradient-45deg-light-blue-cyan gradient-shadow min-height-100 white-text" style="overflow:hidden">
                    <div class="padding-4">
                      <div class="col s7 m7">
                        <i class="material-icons background-round mt-5">account_balance</i>
                        <p>Companies</p>
                      </div>
                      <div class="col s5 m5 right-align">
                        <p>{{ $data['CompanyCount'] }}</p>
                      </div>
                    </div>
                  </div>
                </div>
             </div>
             <div class="row">
                <div class="col s12 m12">
                  <div class="card gradient-45deg-amber-amber gradient-shadow min-height-100 white-text" style="overflow:hidden">
                    <div class="padding-4">
                      <div class="col s7 m7">
                        <i class="material-icons background-round mt-5">flag</i>
                        <p>Countries</p>
                      </div>
                      <div class="col s5 m5 right-align">
                        <p>{{ $data['CountryCount'] }}</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
             <div class="row">
                <div class="col s12 m12">
                  <div class="card gradient-45deg-red-pink gradient-shadow min-height-100 white-text" style="overflow:hidden">
                    <div class="padding-4">
                      <div class="col s7 m7">
                        <i class="material-icons background-round mt-5">location_city</i>
                        <p>States</p>
                      </div>
                      <div class="col s5 m5 right-align">
                        <p>{{ $data['CityCount'] }}</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <div class="row">
                <div class="col s12 m12">
                  <div class="card gradient-45deg-green-teal gradient-shadow min-height-100 white-text" style="overflow:hidden">
                    <div class="padding-4">
                      <div class="col s7 m7">
                        <i class="material-icons background-round mt-5">content_copy</i>
                        <p>Lgas</p>
                      </div>
                      <div class="col s5 m5 right-align">
                        <p>{{ $data['LgaCount'] }}</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
           <div class="row">
                <div class="col s12 m12">
                  <a href="{{ url($guard_url.'dashboard') }}" class="lighten-4">
                    more...
                    </a>
                </div>
              </div>
        </div>
  	</div>
</div>
<!-- main-menu end-->
@endsection
