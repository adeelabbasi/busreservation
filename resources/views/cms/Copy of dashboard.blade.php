@extends('cms.layout.app')
@section('breadcrumbs')
<div id="breadcrumbs-wrapper">
   <!-- Search for small screen -->
   <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
      <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
   </div>
   <div class="container">
      <div class="row">
         <div class="col s10 m6 l6">
            <h5 class="breadcrumbs-title">Dashboard</h5>
            <ol class="breadcrumbs">
               <li><a href="{{ url($guard_url) }}">Home</a>
               </li>
            </ol>
         </div>
         <div class="col s2 m6 l6">
            <a class="btn waves-effect waves-light breadcrumbs-btn right" href="{{ url('/admin') }}" data-activates="dropdown1">
            <i class="material-icons">home</i>
            </a>
            
         </div>
      </div>
   </div>
</div>
@endsection
@section('content')

<h5>REGISTRATION</h5>
<!-- main-menu start-->
@if($guard == "admin")
<!--card stats start-->
<div id="card-stats">
  <div class="row">
    <div class="col s12 m6 l3">
      <div class="card">
        <div class="card-content cyan white-text">
          <p class="card-stats-title">
            <i class="material-icons">account_balance</i> Registered Company</p>
          <h4 class="card-stats-number">{{ $data['CompanyCount'] }}</h4>
        </div>
        <div class="card-action cyan darken-1">
          <div id="clients-bar" class="center-align"></div>
        </div>
      </div>
    </div>
    <div class="col s12 m6 l3">
      <div class="card">
        <div class="card-content red accent-2 white-text">
          <p class="card-stats-title">
            <i class="material-icons">flag</i>Countries</p>
          <h4 class="card-stats-number">{{ $data['CountryCount'] }}</h4>
        </div>
        <div class="card-action red darken-1">
          <div id="sales-compositebar" class="center-align"></div>
        </div>
      </div>
    </div>
    <div class="col s12 m6 l3">
      <div class="card">
        <div class="card-content teal accent-4 white-text">
          <p class="card-stats-title">
            <i class="material-icons">location_city</i>Cities</p>
          <h4 class="card-stats-number">{{ $data['CityCount'] }}</h4>
        </div>
        <div class="card-action teal darken-1">
          <div id="profit-tristate" class="center-align"></div>
        </div>
      </div>
    </div>
    <div class="col s12 m6 l3">
      <div class="card">
        <div class="card-content deep-orange accent-2 white-text">
          <p class="card-stats-title">
            <i class="material-icons">content_copy</i> Lgas</p>
          <h4 class="card-stats-number">{{ $data['LgaCount'] }}</h4>
        </div>
        <div class="card-action  deep-orange darken-1">
          <div id="invoice-line" class="center-align"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--card stats end-->
@endif

<!--card widgets start-->
<div id="card-widgets">
  <div class="row">
    
    <div class="col s12 m4 l4">
      <ul id="task-card" class="collection with-header">
        <li class="collection-header deep-orange accent-2 white-text">
          <h6 class="task-card-title">Terminals</h6>
        </li>
        <li class="collection-item">
          <table class="bordered responsive-table display" style="background:#FFF">
          	<tr>
            	<th>Company</th>
                <th>Counts</th>
            </tr>
            <?php $totalCounts = 0; ?>
            @foreach($data['TerminalReports'] as $TerminalReport)
            <tr>
            	<td>{{ $TerminalReport->name }}</td>
                <td>{{ $TerminalReport->counts }}</td>
            </tr>
            <?php $totalCounts += $TerminalReport->counts; ?>
            @endforeach
            <tr>
            	<th>Total</th>
                <th>{{ $totalCounts }}</th>
            </tr>
          </table>
        </li>
      </ul>
    </div>
    
    <div class="col s12 m4 l4">
      <ul id="task-card" class="collection with-header">
        <li class="collection-header cyan accent-4 white-text">
          <h6 class="task-card-title">Fleets</h6>
        </li>
        <li class="collection-item">
          <table class="bordered responsive-table display" style="background:#FFF">
          	<tr>
            	<th>Company</th>
                <th>Counts</th>
            </tr>
            <?php $totalCounts = 0; ?>
            @foreach($data['FleetReports'] as $FleetReports)
            <tr>
            	<td>{{ $FleetReports->name }}</td>
                <td>{{ $FleetReports->counts }}</td>
            </tr>
            <?php $totalCounts += $FleetReports->counts; ?>
            @endforeach
            <tr>
            	<th>Total</th>
                <th>{{ $totalCounts }}</th>
            </tr>
          </table>
        </li>
      </ul>
    </div>
    
    <div class="col s12 m4 l4">
      <ul id="task-card" class="collection with-header">
        <li class="collection-header red accent-2 white-text">
          <h6 class="task-card-title">Services</h6>
        </li>
        <li class="collection-item">
          <table class="bordered responsive-table display" style="background:#FFF">
          	<tr>
            	<th>Company</th>
                <th>Counts</th>
            </tr>
            <?php $totalCounts = 0; ?>
            @foreach($data['ServiceReports'] as $ServiceReport)
            <tr>
            	<td>{{ $ServiceReport->name }}</td>
                <td>{{ $ServiceReport->counts }}</td>
            </tr>
            <?php $totalCounts += $ServiceReport->counts; ?>
            @endforeach
            <tr>
            	<th>Total</th>
                <th>{{ $totalCounts }}</th>
            </tr>
          </table>
        </li>
      </ul>
    </div>
    
  </div>
</div>
<!--card widgets end-->
<!--card widgets start-->
<div id="card-widgets">
  <div class="row">   
    <div class="col s12 m4 l4">
      <ul id="task-card" class="collection with-header">
        <li class="collection-header red accent-2 white-text">
          <h6 class="task-card-title">Routes</h6>
        </li>
        <li class="collection-item">
          <table class="bordered responsive-table display" style="background:#FFF">
          	<tr>
            	<th>Company</th>
                <th>Counts</th>
            </tr>
            <?php $totalCounts = 0; ?>
            @foreach($data['RouteReports'] as $RouteReport)
            <tr>
            	<td>{{ $RouteReport->name }}</td>
                <td>{{ $RouteReport->counts }}</td>
            </tr>
            <?php $totalCounts += $RouteReport->counts; ?>
            @endforeach
            <tr>
            	<th>Total</th>
                <th>{{ $totalCounts }}</th>
            </tr>
          </table>
        </li>
      </ul>
    </div>
    
    <div class="col s12 m4 l4">
      <ul id="task-card" class="collection with-header">
        <li class="collection-header teal accent-4 white-text">
          <h6 class="task-card-title">Schedules</h6>
        </li>
        <li class="collection-item">
          <table class="bordered responsive-table display" style="background:#FFF">
          	<tr>
            	<th>Company</th>
                <th>Counts</th>
            </tr>
            <?php $totalCounts = 0; ?>
            @foreach($data['ScheduleReports'] as $ScheduleReport)
            <tr>
            	<td>{{ $ScheduleReport->name }}</td>
                <td>{{ $ScheduleReport->counts }}</td>
            </tr>
            <?php $totalCounts += $ScheduleReport->counts; ?>
            @endforeach
            <tr>
            	<th>Total</th>
                <th>{{ $totalCounts }}</th>
            </tr>
          </table>
        </li>
      </ul>
    </div>
    
    <div class="col s12 m4 l4">
      <ul id="task-card" class="collection with-header">
        <li class="collection-header deep-orange accent-2 white-text">
          <h6 class="task-card-title">Users</h6>
        </li>
        <li class="collection-item">
          <table class="bordered responsive-table display" style="background:#FFF">
          	<tr>
            	<th>Company</th>
                <th>Counts</th>
            </tr>
            <?php $totalCounts = 0; ?>
            @foreach($data['UserReports'] as $UserReport)
            <tr>
            	<td>{{ $UserReport->name }}</td>
                <td>{{ $UserReport->counts }}</td>
            </tr>
            <?php $totalCounts += $UserReport->counts; ?>
            @endforeach
            <tr>
            	<th>Total</th>
                <th>{{ $totalCounts }}</th>
            </tr>
          </table>
        </li>
      </ul>
    </div>
    
  </div>
</div>
<!--card widgets end-->

<!--card widgets start-->
<div id="card-widgets">
  <div class="row">
    
    <div class="col s12 m12 l12">
      <ul id="task-card" class="collection with-header">
        <li class="collection-header purple accent-2 white-text">
          <h6 class="task-card-title">Schedule by week day</h6>
        </li>
        <li class="collection-item">
          <table class="bordered responsive-table display" style="background:#FFF">
          	<tr>
            	<th>Company</th>
                <th>Monday</th>
                <th>Tuesday</th>
                <th>Wednesday</th>
                <th>Thursday</th>
                <th>Friday</th>
                <th>Saturday</th>
                <th>Sunday</th>
            </tr>
            <?php 
				$totalMonday = 0; 
				$totalTuesday = 0; 
				$totalWednesday = 0; 
				$totalThursday = 0; 
				$totalFriday = 0; 
				$totalSaturday = 0; 
				$totalSunday = 0; 
			?>
            @foreach($data['ScheduleByDayReports'] as $ScheduleByDayReport)
            <tr>
            	<td>{{ $ScheduleByDayReport->name }}</td>
                <td>{{ $ScheduleByDayReport->monday }}</td>
                <td>{{ $ScheduleByDayReport->tuesday }}</td>
                <td>{{ $ScheduleByDayReport->wednesday }}</td>
                <td>{{ $ScheduleByDayReport->thursday }}</td>
                <td>{{ $ScheduleByDayReport->friday }}</td>
                <td>{{ $ScheduleByDayReport->saturday }}</td>
                <td>{{ $ScheduleByDayReport->sunday }}</td>
            </tr>
            <?php 
				$totalMonday += $ScheduleByDayReport->monday; 
				$totalTuesday += $ScheduleByDayReport->tuesday;  
				$totalWednesday += $ScheduleByDayReport->wednesday; ; 
				$totalThursday += $ScheduleByDayReport->thursday; 
				$totalFriday += $ScheduleByDayReport->friday; 
				$totalSaturday += $ScheduleByDayReport->saturday;  
				$totalSunday += $ScheduleByDayReport->sunday;  
			?>
            @endforeach
            <tr>
            	<th>Total</th>
                <th>{{ $totalMonday }}</th>
				<th>{{ $totalTuesday }}</th>
				<th>{{ $totalWednesday }}</th> 
				<th>{{ $totalThursday }}</th>
				<th>{{ $totalFriday }}</th> 
				<th>{{ $totalSaturday }}</th> 
				<th>{{ $totalSunday }}</th>
            </tr>
          </table>
        </li>
      </ul>
    </div>
    
  </div>
</div>
<!--card widgets end-->
<h5>ACTIVITY/USAGE </h5>
<!--card widgets start-->
<div id="card-widgets">
  <div class="row">
    
    <div class="col s12 m4 l4">
      <ul id="task-card" class="collection with-header">
        <li class="collection-header red accent-2 white-text">
          <h6 class="task-card-title">Customer</h6>
        </li>
        <li class="collection-item">
          <table class="bordered responsive-table display" style="background:#FFF">
          	<tr>
            	<th>Total</th>
                <th>Day</th>
                <th>Month</th>
                <th>Year</th>
            </tr>
            @foreach($data['getCustomersReportCounts'] as $getCustomersReportCount)
            <tr>
            	<td><a href="export/Customer/Total" target="_blank">{{ $getCustomersReportCount->Total }}</a></td>
            	<td><a href="export/Customer/Day" target="_blank">{{ $getCustomersReportCount->Day }}</a></td>
                <td><a href="export/Customer/Month" target="_blank">{{ $getCustomersReportCount->Month }}</a></td>
                <td><a href="export/Customer/Year" target="_blank">{{ $getCustomersReportCount->Year }}</a></td>
            </tr>
            @endforeach
          </table>
        </li>
      </ul>
    </div>
    
    <div class="col s12 m4 l4">
      <ul id="task-card" class="collection with-header">
        <li class="collection-header cyan accent-4 white-text">
          <h6 class="task-card-title">Unique Customer</h6>
        </li>
        <li class="collection-item">
          <table class="bordered responsive-table display" style="background:#FFF">
          	<tr>
            	<th>Total</th>
                <th>Day</th>
                <th>Month</th>
                <th>Year</th>
            </tr>
            @foreach($data['getUniqueCustomersReportCounts'] as $getUniqueCustomersReportCount)
            <tr>
            	<td><a href="export/Unique_Customer/Total" target="_blank">{{ $getUniqueCustomersReportCount->Total }}</a></td>
            	<td><a href="export/Unique_Customer/Day" target="_blank">{{ $getUniqueCustomersReportCount->Day }}</a></td>
                <td><a href="export/Unique_Customer/Month" target="_blank">{{ $getUniqueCustomersReportCount->Month }}</a></td>
                <td><a href="export/Unique_Customer/Year" target="_blank">{{ $getUniqueCustomersReportCount->Year }}</a></td>
            </tr>
            @endforeach
          </table>
        </li>
      </ul>
    </div>
    
    <div class="col s12 m4 l4">
      <ul id="task-card" class="collection with-header">
        <li class="collection-header deep-orange accent-2 white-text">
          <h6 class="task-card-title">Request by company code</h6>
        </li>
        <li class="collection-item">
          <table class="bordered responsive-table display" style="background:#FFF">
          	<tr>
            	<th>Company</th>
            	<th>Total</th>
                <th>Day</th>
                <th>Month</th>
                <th>Year</th>
            </tr>
            @foreach($data['getCompanyRequestsReportCounts'] as $getCompanyRequestsReportCount)
            <tr>
            	<td>{{ $getCompanyRequestsReportCount->Company }}({{ $getCompanyRequestsReportCount->ussd_code }})</td>
            	<td><a href="export/Company_Requests_Report/Total?company_name={{$getCompanyRequestsReportCount->Company}}" target="_blank">{{ $getCompanyRequestsReportCount->Total }}</a></td>
            	<td><a href="export/Company_Requests_Report/Day?company_name={{$getCompanyRequestsReportCount->Company}}" target="_blank">{{ $getCompanyRequestsReportCount->Day }}</a></td>
                <td><a href="export/Company_Requests_Report/Month?company_name={{$getCompanyRequestsReportCount->Company}}" target="_blank">{{ $getCompanyRequestsReportCount->Month }}</a></td>
                <td><a href="export/Company_Requests_Report/Year?company_name={{$getCompanyRequestsReportCount->Company}}" target="_blank">{{ $getCompanyRequestsReportCount->Year }}</a></td>
            </tr>
            @endforeach
          </table>
        </li>
      </ul>
    </div>
    
  </div>
</div>
<!--card widgets end-->

<!--card widgets start-->
<div id="card-widgets">
  <div class="row">
    
    <div class="col s12 m4 l4">
      <ul id="task-card" class="collection with-header">
        <li class="collection-header deep-orange accent-2 white-text">
          <h6 class="task-card-title">Requests by shortcode strings</h6>
        </li>
        <li class="collection-item">
          <table class="bordered responsive-table display" style="background:#FFF">
          	<tr>
            	<th>Code</th>
            	<th>Total</th>
                <th>Day</th>
                <th>Month</th>
                <th>Year</th>
            </tr>
            @foreach($data['getShortcodeStringsReportCounts'] as $getShortcodeStringsReportCount)
            <tr>
            	<td>{{ $getShortcodeStringsReportCount->Ussd }}</td>
            	<td><a href="export/Shortcode_Strings/Total?ussd_code={{$getShortcodeStringsReportCount->Ussd}}" target="_blank">{{ $getShortcodeStringsReportCount->Total }}</a></td>
            	<td><a href="export/Shortcode_Strings/Day?ussd_code={{$getShortcodeStringsReportCount->Ussd}}" target="_blank">{{ $getShortcodeStringsReportCount->Day }}</a></td>
                <td><a href="export/Shortcode_Strings/Month?ussd_code={{$getShortcodeStringsReportCount->Ussd}}" target="_blank">{{ $getShortcodeStringsReportCount->Month }}</a></td>
                <td><a href="export/Shortcode_Strings/Year?ussd_code={{$getShortcodeStringsReportCount->Ussd}}" target="_blank">{{ $getShortcodeStringsReportCount->Year }}</a></td>
            </tr>
            @endforeach
          </table>
        </li>
      </ul>
    </div>
    
    <div class="col s12 m4 l4">
      <ul id="task-card" class="collection with-header">
        <li class="collection-header purple accent-2 white-text">
          <h6 class="task-card-title">Requests by routes</h6>
        </li>
        <li class="collection-item">
          <table class="bordered responsive-table display" style="background:#FFF">
          	<tr>
            	<th>Company</th>
            	<th>Route</th>
            	<th>Total</th>
                <th>Day</th>
                <th>Month</th>
                <th>Year</th>
            </tr>
            @foreach($data['getRoutesReportCounts'] as $getRoutesReportCount)
            <tr>
            	<td>{{ $getRoutesReportCount->Company }}</td>
                <td>{{ $getRoutesReportCount->Route }}</td>
            	<td><a href="export/Routes/Total?company_name={{$getRoutesReportCount->Company}}&route_name={{$getRoutesReportCount->Route}}" target="_blank">{{ $getRoutesReportCount->Total }}</a></td>
            	<td><a href="export/Routes/Day?company_name={{$getRoutesReportCount->Company}}&route_name={{$getRoutesReportCount->Route}}" target="_blank">{{ $getRoutesReportCount->Day }}</a></td>
                <td><a href="export/Routes/Month?company_name={{$getRoutesReportCount->Company}}&route_name={{$getRoutesReportCount->Route}}" target="_blank">{{ $getRoutesReportCount->Month }}</a></td>
                <td><a href="export/Routes/Year?company_name={{$getRoutesReportCount->Company}}&route_name={{$getRoutesReportCount->Route}}" target="_blank">{{ $getRoutesReportCount->Year }}</a></td>
            </tr>
            @endforeach
          </table>
        </li>
      </ul>
    </div>
    
    <div class="col s12 m4 l4">
      <ul id="task-card" class="collection with-header">
        <li class="collection-header red accent-2 white-text">
          <h6 class="task-card-title">Requests  to each USSD Menu Items - General</h6>
        </li>
        <li class="collection-item">
          <table class="bordered responsive-table display" style="background:#FFF">
          	<tr>
            	<th>Menu</th>
            	<th>Total</th>
                <th>Day</th>
                <th>Month</th>
                <th>Year</th>
            </tr>
            @foreach($data['getUSSDMenuGeneralItemsReportCounts'] as $getUSSDMenuGeneralItemsReportCount)
            <tr>
            	<td>{{ $getUSSDMenuGeneralItemsReportCount->Main_USSD_Menu_Items }}</td>
            	<td><a href="export/USSD_Menu_General_Items/Total?action={{$getUSSDMenuGeneralItemsReportCount->Main_USSD_Menu_Items}}" target="_blank">{{ $getUSSDMenuGeneralItemsReportCount->Total }}</a></td>
            	<td><a href="export/USSD_Menu_General_Items/Day?action={{$getUSSDMenuGeneralItemsReportCount->Main_USSD_Menu_Items}}" target="_blank">{{ $getUSSDMenuGeneralItemsReportCount->Day }}</a></td>
                <td><a href="export/USSD_Menu_General_Items/Month?action={{$getUSSDMenuGeneralItemsReportCount->Main_USSD_Menu_Items}}" target="_blank">{{ $getUSSDMenuGeneralItemsReportCount->Month }}</a></td>
                <td><a href="export/USSD_Menu_General_Items/Year?action={{$getUSSDMenuGeneralItemsReportCount->Main_USSD_Menu_Items}}" target="_blank">{{ $getUSSDMenuGeneralItemsReportCount->Year }}</a></td>
            </tr>
            @endforeach
          </table>
        </li>
      </ul>
    </div>
    
  </div>
</div>
<!--card widgets end-->

<!--card widgets start-->
<div id="card-widgets">
  <div class="row">
    
    <div class="col s12 m4 l4">
      <ul id="task-card" class="collection with-header">
        <li class="collection-header red accent-2 white-text">
          <h6 class="task-card-title">Requests  to each USSD Menu Items - Company</h6>
        </li>
        <li class="collection-item">
          <table class="bordered responsive-table display" style="background:#FFF">
          	<tr>
            	<th>Company</th>
            	<th>Menu</th>
            	<th>Total</th>
                <th>Day</th>
                <th>Month</th>
                <th>Year</th>
            </tr>
            @foreach($data['getUSSDMenuCompanyItemsReportCounts'] as $getUSSDMenuCompanyItemsReportCount)
            <tr>
            	<?php
                	$Items = explode("-",$getUSSDMenuCompanyItemsReportCount->Company_USSD_Menu_Items);
				?>
            	<td>{{ $Items[0] }}</td>
                <td>{{ $Items[1] }}</td>
            	<td><a href="export/USSD_Menu_Company_Items/Total?action={{$Items[1]}}&company_name={{$Items[0]}}" target="_blank">{{ $getUSSDMenuCompanyItemsReportCount->Total }}</a></td>
            	<td><a href="export/USSD_Menu_Company_Items/Day?action={{$Items[1]}}&company_name={{$Items[0]}}" target="_blank">{{ $getUSSDMenuCompanyItemsReportCount->Day }}</a></td>
                <td><a href="export/USSD_Menu_Company_Items/Month?action={{$Items[1]}}&company_name={{$Items[0]}}" target="_blank">{{ $getUSSDMenuCompanyItemsReportCount->Month }}</a></td>
                <td><a href="export/USSD_Menu_Company_Items/Year?action={{$Items[1]}}&company_name={{$Items[0]}}" target="_blank">{{ $getUSSDMenuCompanyItemsReportCount->Year }}</a></td>
            </tr>
            @endforeach
          </table>
        </li>
      </ul>
    </div>
    
    <div class="col s12 m4 l4">
      <ul id="task-card" class="collection with-header">
        <li class="collection-header red accent-2 white-text">
          <h6 class="task-card-title">Requests  to each USSD Menu Items - Company</h6>
        </li>
        <li class="collection-item">
          <table class="bordered responsive-table display" style="background:#FFF">
          	<tr>
            	<th>Company</th>
            	<th>Menu</th>
            	<th>Total</th>
                <th>Day</th>
                <th>Month</th>
                <th>Year</th>
            </tr>
            @foreach($data['getUSSDMenuCompanyItemsReportCounts'] as $getUSSDMenuCompanyItemsReportCount)
            <tr>
            	<?php
                	$Items = explode("-",$getUSSDMenuCompanyItemsReportCount->Company_USSD_Menu_Items);
				?>
            	<td>{{ $Items[0] }}</td>
                <td>{{ $Items[1] }}</td>
            	<td><a href="export/USSD_Menu_Company_Items/Total?action={{$Items[1]}}&company_name={{$Items[0]}}" target="_blank">{{ $getUSSDMenuCompanyItemsReportCount->Total }}</a></td>
            	<td><a href="export/USSD_Menu_Company_Items/Day?action={{$Items[1]}}&company_name={{$Items[0]}}" target="_blank">{{ $getUSSDMenuCompanyItemsReportCount->Day }}</a></td>
                <td><a href="export/USSD_Menu_Company_Items/Month?action={{$Items[1]}}&company_name={{$Items[0]}}" target="_blank">{{ $getUSSDMenuCompanyItemsReportCount->Month }}</a></td>
                <td><a href="export/USSD_Menu_Company_Items/Year?action={{$Items[1]}}&company_name={{$Items[0]}}" target="_blank">{{ $getUSSDMenuCompanyItemsReportCount->Year }}</a></td>
            </tr>
            @endforeach
          </table>
        </li>
      </ul>
    </div>
    
  </div>
</div>
<!--card widgets end-->
@endsection
@push('scripts')

<script type="text/javascript">
	//Sampel Line Chart 
	var LineChartSampleData = {
	labels: ["January", "February", "March", "April", "May", "June", "July"],
	datasets: [{
	label: "My First dataset",
	fillColor: "rgba(0, 191, 165,0.2)",
	strokeColor: "rgba(0, 191, 165,1)",
	pointColor: "rgba(0, 191, 165,1)",
	pointStrokeColor: "#fff",
	pointHighlightFill: "#fff",
	pointHighlightStroke: "rgba(0, 191, 165,1)",
	data: [65, 30, 50, 81, 56, 55, 40]
	}, {
	label: "My Second dataset",
	fillColor: "rgba(255, 110, 64,0.2)",
	strokeColor: "rgba(255, 110, 64,1)",
	pointColor: "rgba(255, 110, 64,1)",
	pointStrokeColor: "#fff",
	pointHighlightFill: "#fff",
	pointHighlightStroke: "rgba(255, 110, 64,1)",
	data: [28, 90, 40, 19, 86, 27, 90]
	}]
	};
</script>

@endpush
