@extends('cms.layout.app')
@section('breadcrumbs')
<div id="breadcrumbs-wrapper">
   <!-- Search for small screen -->
   <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
      <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
   </div>
   <div class="container">
      <div class="row">
         <div class="col s10 m6 l6">
            <h5 class="breadcrumbs-title">Add Role</h5>
            <ol class="breadcrumbs">
               <li><a href="{{ url($guard_url) }}">Home</a>
               </li>
               <li><a href="{{ url($guard_url.'role') }}">Role</a>
               </li>
            </ol>
         </div>
         <div class="col s2 m6 l6">
            <a class="btn waves-effect waves-light breadcrumbs-btn right" href="{{ url($guard_url) }}" data-activates="dropdown1">
            <i class="material-icons">home</i>
            </a>
            
         </div>
      </div>
   </div>
</div>
@endsection
@section('content')
{!! Form::model($info_Role, ['method' => 'PATCH', 'url' => [$guard_url.'role', $info_Role->id], 'id' => 'main-form']) !!}

<br />
<div class="divider"></div>
<br />
<div class="row">
   <div class="col s12 m8 l8">
      <div id="flight-card" class="card" style="overflow:hidden">
          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
            <div class="card-title">
              <h6 class="flight-card-title">Role Information</h6>
            </div>
          </div>
         <div class="card-content" style="background-color:#FFF">
           @if($guard=="admin")
            <input type="hidden" name="company_id" value="{{ $info_Role->company_id }}"  />
           @else
           <input type="hidden" name="company_id" value="{{ Auth::guard($guard)->User()->Company()->First()->id }}"  />
           @endif
           <div class="col s12 m4 input-field">
              <label for="name">Name:</label>
              {!! Form::text('name', null, ['class' => 'form-control' , 'required', 'id' => 'name']) !!}
              @if ($errors->has('name'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('name')!!}</span>@endif
           </div>
        </div>
        <div id="Permissions">
        </div>
   </div>
</div>
</div>
</form>

<div id="parentPer" style="display:none">
	<div class="row">
        <div class="col s12 m12">
            <div class="row" style="border-bottom: 1px solid; padding: 15px;">
                <div class="col s12 m4">
                    Name
                </div>
                <div class="col s2 m2">
                    View
                </div>
                <div class="col s2 m2">
                    Add
                </div>
                <div class="col s2 m2">
                    Update
                </div>
                <div class="col s2 m2">
                    Delete
                </div>
            </div>
            <?php 
            $i=1; 
            $oldName = "";
            ?>
            @foreach($info_Permissions as $Permissions)
            @if($Permissions->name=="Dashboard")
                <div class="row" style="border-bottom: 1px solid; padding: 15px;">
                    <div class="col s12 m4">
                        <label>{{ $Permissions->name }}</label>
                    </div>
                    <div class="col s2 m2">
                        <input name="permission[]" value="{{ $Permissions->id }}" type="checkbox" {{ in_array($Permissions->id, $rolePermissions) ? 'checked' : '' }} id="permission{!! $i !!}" />
                        <label for="permission{!! $i !!}">&nbsp;</label>
                    </div>
                    <div class="col s2 m2">
                        
                    </div>
                    <div class="col s2 m2">
                        
                    </div>
                    <div class="col s2 m2">
                        
                    </div>
                </div>
            @else
                <?php
                    if($oldName=="") 
                    {
                        $oldName = trim(str_replace(strtok($Permissions->name, " "), "", $Permissions->name)); 
                        echo '<div class="row" style="border-bottom: 1px solid; padding: 15px;">';
                        echo '<div class="col s12 m4">';
                        echo    '<label>'.$Permissions->name.'</label>';
                        echo '</div>';
                    }
                    if($oldName!=trim(str_replace(strtok($Permissions->name, " "), "", $Permissions->name)))
                    {
                        $oldName = trim(str_replace(strtok($Permissions->name, " "), "", $Permissions->name)); 
                        echo '</div>';
                        echo '<div class="row" style="border-bottom: 1px solid; padding: 15px;">';
                        echo '<div class="col s12 m4">';
                        echo    '<label>'.$Permissions->name.'</label>';
                        echo '</div>';
                        echo '<div class="col s2 m2">';
                        echo '	<input name="permission[]" value="'.$Permissions->id.'" type="checkbox" '.(in_array($Permissions->id, $rolePermissions) ? 'checked' : '').' id="permission'.$i.'" />';
                        echo '	<label for="permission'.$i.'">&nbsp;</label>';
                        echo '</div>';
                    }
                    else
                    {
                ?>
                    <div class="col s2 m2">
                        <input name="permission[]" value="{{ $Permissions->id }}" type="checkbox" {{ in_array($Permissions->id, $rolePermissions) ? 'checked' : '' }} id="permission{!! $i !!}" />
                        <label for="permission{!! $i !!}">&nbsp;</label>
                    </div>
                <?php
                    }
                ?>
            @endif
            
            <?php $i++; ?>
            @endforeach
        
        <div class="row">
          <div class="input-field col s12">
            <button class="btn cyan waves-effect waves-light right" type="submit">Submit
              <i class="material-icons right">send</i>
            </button>
          </div>
        </div>
      </div>  
    </div>
    </div>
</div>

<div id="webPer" style="display:none">
	<div class="row">
        <div class="col s12 m12">
            <div class="row" style="border-bottom: 1px solid; padding: 15px;">
                <div class="col s12 m4">
                    Name 
                </div>
                <div class="col s2 m2">
                    View
                </div>
                <div class="col s2 m2">
                    Add
                </div>
                <div class="col s2 m2">
                    Update
                </div>
                <div class="col s2 m2">
                    Delete
                </div>
            </div>
            <?php 
            $i=1; 
            $oldName = "";
            ?>
            @foreach($info_PermissionsWeb as $PermissionsWeb)
            @if($PermissionsWeb->name=="Dashboard")
                <div class="row" style="border-bottom: 1px solid; padding: 15px;">
                    <div class="col s12 m4">
                        <label>{{ $PermissionsWeb->name }}</label>
                    </div>
                    <div class="col s2 m2">
                        <input name="permission[]" value="{{ $PermissionsWeb->id }}" type="checkbox" {{ in_array($PermissionsWeb->id, $rolePermissionsWeb) ? 'checked' : '' }} id="permission{!! $i !!}" />
                        <label for="permission{!! $i !!}">&nbsp;</label>
                    </div>
                    <div class="col s2 m2">
                        
                    </div>
                    <div class="col s2 m2">
                        
                    </div>
                    <div class="col s2 m2">
                        
                    </div>
                </div>
            @else
                <?php
                    if($oldName=="") 
                    {
                        $oldName = trim(str_replace(strtok($PermissionsWeb->name, " "), "", $PermissionsWeb->name)); 
                        echo '<div class="row" style="border-bottom: 1px solid; padding: 15px;">';
                        echo '<div class="col s12 m4">';
                        echo    '<label>'.$PermissionsWeb->name.'</label>';
                        echo '</div>';
                    }
                    if($oldName!=trim(str_replace(strtok($PermissionsWeb->name, " "), "", $PermissionsWeb->name)))
                    {
                        $oldName = trim(str_replace(strtok($PermissionsWeb->name, " "), "", $PermissionsWeb->name)); 
                        echo '</div>';
                        echo '<div class="row" style="border-bottom: 1px solid; padding: 15px;">';
                        echo '<div class="col s12 m4">';
                        echo    '<label>'.$PermissionsWeb->name.'</label>';
                        echo '</div>';
                        echo '<div class="col s2 m2">';
                        echo '	<input name="permission[]" value="'.$PermissionsWeb->id.'" type="checkbox" '.(in_array($PermissionsWeb->id, $rolePermissionsWeb) ? 'checked' : '').' id="permission'.$i.'" />';
                        echo '	<label for="permission'.$i.'">&nbsp;</label>';
                        echo '</div>';
                    }
                    else
                    {
                ?>
                    <div class="col s2 m2">
                        <input name="permission[]" value="{{ $PermissionsWeb->id }}" type="checkbox" {{ in_array($PermissionsWeb->id, $rolePermissionsWeb) ? 'checked' : '' }} id="permission{!! $i !!}" />
                        <label for="permission{!! $i !!}">&nbsp;</label>
                    </div>
                <?php
                    }
                ?>
            @endif
            
            <?php $i++; ?>
            @endforeach
        
        <div class="row">
          <div class="input-field col s12">
            <button class="btn cyan waves-effect waves-light right" type="submit">Submit 
              <i class="material-icons right">send</i>
            </button>
          </div>
        </div>
      </div>  
    </div>
    </div>
</div></div>
@endsection
@push('scripts')

<script type="text/javascript">
    @if($guard=="admin")
		$('#CompanyRole').on('change', function (e) { 
			var company_id = $('#CompanyRole option:selected').val();
			$('#Permissions').html("");
			if(company_id=="-1")
				$('#Permissions').html($('#parentPer').html());
			else
				$('#Permissions').html($('#webPer').html());
		});
		$( document ).ready(function() {
			@if($info_Role->company_id=="-1")
				$('#Permissions').html($('#parentPer').html());
			@else
				$('#Permissions').html($('#webPer').html());
			@endif
		});
	@else
		$( document ).ready(function() {
			$('#Permissions').html($('#webPer').html());
			return false;
		});
	@endif
</script>

@endpush