@extends('cms.layout.app')
@section('breadcrumbs')
<div id="breadcrumbs-wrapper">
   <!-- Search for small screen -->
   <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
      <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
   </div>
   <div class="container">
      <div class="row">
         <div class="col s10 m6 l6">
            <h5 class="breadcrumbs-title">Add Company</h5>
            <ol class="breadcrumbs">
               <li><a href="{{ url($guard_url) }}">Home</a>
               </li>
               <li><a href="{{ url($guard_url.'company') }}">Company</a>
               </li>
            </ol>
         </div>
         <div class="col s2 m6 l6">
            <a class="btn waves-effect waves-light breadcrumbs-btn right" href="{{ url($guard_url) }}" data-activates="dropdown1">
            <i class="material-icons">home</i>
            </a>
            
         </div>
      </div>
   </div>
</div>
@endsection
@section('content')

{!! Form::open([ 'url' => $guard_url.'company/', 'files' => true, 'id' => 'main-form' ]) !!}
<br />
<div class="divider"></div>
<br />
<div class="row">
   <div class="col s12 m8 l8">
      <div id="flight-card" class="card">
          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
            <div class="card-title">
              <h6 class="flight-card-title">Basic Information</h6>
            </div>
          </div>
         <div class="card-content" style="background-color:#FFF">
            <div class="col s12 m4 input-field">
               <label for="name">Company Name:</label>
               {!! Form::text('name', null, ['required', 'id' => 'name']) !!}
               @if ($errors->has('name'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('name')!!}</span>@endif
            </div>
            <div class="col s12 m4 input-field">
               <label for="username">Company Code:</label>
               {!! Form::text('short_name', null, ['required', 'id' => 'code']) !!}
               @if ($errors->has('short_name'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('short_name')!!}</span>@endif
            </div>
            <div class="col s12 m4 input-field">
               <?php
                  $defaultSelection = ["" => "Select Type", "1" => "Airline", "2" => "Bus", "3" => "Train", "4" => "Hotel"];
                  ?>
               {!! Form::select('type', $defaultSelection, null, ['class' => 'form-control', 'id' => 'type']) !!}
               @if ($errors->has('type'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('type')!!}</span>@endif
            </div>
            <div class="col s12 m4 input-field">
               <?php
                  $defaultSelection = ["" => "Select Coverage ", "1" => "Domestic", "2" => "International"];
                  ?>
               {!! Form::select('coverage', $defaultSelection, null, ['class' => 'form-control', 'id' => 'coverage']) !!}
               @if ($errors->has('coverage'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('coverage')!!}</span>@endif
            </div>
            <div class="col s12 m4 input-field">
               <?php
                  $defaultSelection = ["" => "Select Country"];
                  foreach($info_Countries as $Countries)
                  {
                      $defaultSelection = $defaultSelection +  array($Countries->id => ($Countries->name));
                  }
                  ?>
               {!! Form::select('country_id', $defaultSelection, null, ['class' => 'form-control', 'id' => 'country']) !!}
               @if ($errors->has('country_id'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('country_id')!!}</span>@endif
            </div>
            <div class="col s12 m4 input-field">
               <label for="rc_no">RC No.:</label>
               {!! Form::text('rc_no', null, ['required' , 'id' => 'username', 'id' => 'rc_no']) !!}
               @if ($errors->has('rc_no'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('rc_no')!!}</span>@endif
            </div>
            <div class="col s12 m4 input-field">
               <label for="reg_date">Reg Date:</label>
               {!! Form::text('reg_date', null, ['class' => 'datepicker' , 'required' , 'id' => 'reg_date']) !!}
               @if ($errors->has('reg_date'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('reg_date')!!}</span>@endif
            </div>
            <div class="col s12 m4 input-field">
               <label for="ussd_code">USSD Code:</label>
               {!! Form::text('ussd_code', null, ['required', 'id' => 'ussd_code']) !!}
               @if ($errors->has('ussd_code'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('ussd_code')!!}</span>@endif
            </div>
            <div class="col s12 m4 input-field">
               <?php
                  $defaultSelection = ["" => "Select Company Type", "1" => "Private", "2" => "Commercial", "3" => "Government "];
               ?>
               {!! Form::select('local_govt', $defaultSelection, null, ['class' => 'form-control', 'id' => 'company_type']) !!}
               @if ($errors->has('local_govt'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('local_govt')!!}</span>@endif
            </div>
            <div class="col s12 m4 input-field">
               <?php
                  $defaultSelection = ["" => "Select USSD Datasource", "1" => "Database", "2" => "API"];
               ?>
               {!! Form::select('ussd_type', $defaultSelection, null, ['class' => 'form-control', 'id' => 'ussd_type']) !!}
            </div>
            <div class="col s12 m4 input-field">
                <label for="api_url">API URL:</label>
                {!! Form::text('api_url', null, ['id' => 'api_url']) !!}
                @if ($errors->has('api_url'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('api_url')!!}</span>@endif
            </div>
            <div class="col s12 m4 input-field">
               <label for="api_url">API Token:</label>
                {!! Form::text('api_token', null, ['id' => 'api_token']) !!}
                @if ($errors->has('api_token'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('api_token')!!}</span>@endif
            </div>
            <div class="col s12 m4 input-field">
               <label for="description">Description:</label>
               {!! Form::textarea('description', null, ['required' , 'class' => 'materialize-textarea', 'id' => 'description', 'rows' => '5']) !!}
               @if ($errors->has('description'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('description')!!}</span>@endif
            </div>
            <div class="col s12 m4">
            <br /><br /><br />
               <div class="switch">
                    <label>
                    Inactive
                    <input type="checkbox" checked="checked" name="status" id="status"/>
                    <span class="lever"></span>
                    Active
                    </label>
                </div>
               
            </div>
            <div class="col s12 m4 input-field">
               <label class="active">Logo:</label><br /><br />
               {!! Form::file('logo', null, ['class' => 'form-control', 'required', 'id' => 'logo']) !!}
               @if ($errors->has('logo'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('logo')!!}</span>@endif
            </div>
            <div class="row">
              <div class="input-field col s12">
                
              </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="row">
   <div class="col s12 m8 l8">
      <div id="flight-card" class="card">
          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
            <div class="card-title">
              <h6 class="flight-card-title">Company Information</h6>
            </div>
          </div>
         <div class="card-content" style="background-color:#FFF">
            <div class="col s12 m4 input-field">
               <label for="address">Address:</label>
               {!! Form::textarea('address', null, ['required' , 'class' => 'materialize-textarea', 'id' => 'address', 'rows' => '5']) !!}
               @if ($errors->has('address'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('address')!!}</span>@endif
            </div>
            <div class="col s12 m4 input-field">
               <label for="email">Email</label>
               {!! Form::text('email', null, ['required' , 'id' => 'email']) !!}
               @if ($errors->has('email'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('email')!!}</span>@endif
            </div>
            <div class="col s12 m4 input-field">
               <label for="website">Website:</label>
               {!! Form::text('website', null, ['required', 'id' => 'website']) !!}
               @if ($errors->has('website'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('website')!!}</span>@endif
            </div>
            <div class="col s12 m4 input-field">
               <label for="linkedin">LinkedIn:</label>
               {!! Form::text('linkedin', null, ['required', 'id' => 'linkedin']) !!}
               @if ($errors->has('linkedin'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('linkedin')!!}</span>@endif
            </div>
            <div class="col s12 m4 input-field">
               <label for="twitter">Twitter:</label>
               {!! Form::text('twitter', null, ['required', 'id' => 'twitter']) !!}
               @if ($errors->has('twitter'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('twitter')!!}</span>@endif
            </div>
            <div class="col s12 m4 input-field">
               <label for="id">IG:</label>
               {!! Form::text('ig', null, ['required' , 'id' => 'ig']) !!}
               @if ($errors->has('ig'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('ig')!!}</span>@endif
            </div>
            <div class="col s12 m4 input-field">
               <label for="contact_person">Contact Person</label>
               {!! Form::text('contact_person', null, ['required', 'id' => 'contact_person']) !!}
               @if ($errors->has('contact_person'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('contact_person')!!}</span>@endif
            </div>
            <div class="col s12 m4 input-field">
               <label for="contact_person_no">Contact Number</label>
               {!! Form::text('contact_person_no', null, ['required', 'id' => 'contact_person_no']) !!}
               @if ($errors->has('contact_person_no'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('contact_person_no')!!}</span>@endif
            </div>
            <div class="col s12 m4 input-field">
               <label class="active">WhatsApp:</label>
               <div id="whatsappDiv">
                  <div class="whatsapp-input">
                     <input class="form-control" required="" id="whatsapp" name="whatsapp[]" type="text" value="">
                  </div>
               </div>
               <button type="button" class="btn-floating waves-effect waves-light btn-add-whatsapp"><i class="material-icons">add</i></button>
               @if ($errors->has('whatsapp'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('whatsapp')!!}</span>@endif
            </div>
            <div class="col s12 m4 input-field">
               <label class="active">Phone Number:</label>
               <div id="phoneDiv">
                  <div class="phone-input">
                     <input class="form-control" required="" id="phone_number" name="phone_number[]" type="text" value="">
                  </div>
               </div>
               <button type="button" class="btn-floating waves-effect waves-light btn-add-phone"><i class="material-icons">add</i></button>
               @if ($errors->has('phone_number'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('phone_number')!!}</span>@endif
            </div>
            <div class="row">
              <div class="input-field col s12">
                
              </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="row">
   <div class="col s12 m8 l8">
      <div id="flight-card" class="card">
          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
            <div class="card-title">
              <h6 class="flight-card-title">RTSS Information</h6>
            </div>
          </div>
          <div class="card-content" style="background-color:#FFF">
         	<div class="col s12 m4 input-field">
               <label for="rtsss_reg_no">RTSS Reg No:</label>
               {!! Form::text('rtss_reg_no', null, ['required' , 'id' => 'rtsss_reg_no']) !!}
               @if ($errors->has('rtss_reg_no'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('rtss_reg_no')!!}</span>@endif
            </div>
            <div class="col s12 m4 input-field">
               <label for="rtss_cert_code">RTSS Cert. Code:</label>
               {!! Form::text('rtss_cert_code', null, ['required', 'id' => 'rtss_cert_code']) !!}
               @if ($errors->has('rtss_cert_code'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('rtss_cert_code')!!}</span>@endif
            </div>
            <div class="col s12 m4 input-field">
               <label for="rtss_certification">RTSSS Certification:</label>
               {!! Form::text('rtss_certification', null, ['required' , 'id' => 'rtss_certification']) !!}
               @if ($errors->has('rtss_certification'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('rtss_certification')!!}</span>@endif
            </div>
            <div class="row">
              <div class="input-field col s12">
                 
              </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="row">
   <div class="col s12 m8 l8">
      <div id="flight-card" class="card">
          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
            <div class="card-title">
              <h6 class="flight-card-title">Billing Information</h6>
            </div>
          </div>
         <div class="card-content" style="background-color:#FFF">
         	<div class="row">
                <div class="col s12 m4 input-field">
                   <label class="active">Minimum Fee:</label>
                   {!! Form::text('minimum_fee', null, ['id' => 'minimum_fee']) !!} 
                   @if ($errors->has('minimum_fee'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('minimum_fee')!!}</span>@endif
                </div>
            </div>
            <div class="col s12 m4 input-field">
               <label class="active">Charge Type:</label>
               <?php
                  $defaultSelection = ["1" => "Inclusive ", "2" => "Exclusive "];
               ?>
               {!! Form::select('charge_type', $defaultSelection, null, ['class' => 'form-control', 'id' => 'charge_type']) !!}
               @if ($errors->has('charge_type'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('charge_type')!!}</span>@endif
            </div>
            <div class="col s12 m4 input-field">
               <label  class="active">Channel Fee Type:</label>
               <?php
                  $defaultSelection = ["1" => "Percentage of total ticket amount ", "2" => "Flat amount per seat", "3" => "Flat amount on total seats"];
               ?>
               {!! Form::select('channel_fee_type', $defaultSelection, null, ['class' => 'form-control', 'id' => 'channel_fee_type']) !!}
               @if ($errors->has('channel_fee_type'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('channel_fee_type')!!}</span>@endif
            </div>
            <div class="col s12 m4 input-field">
               <label for="rtss_certification">Channel Fee:</label>
               {!! Form::text('channel_fee', null, ['id' => 'channel_fee']) !!}
               @if ($errors->has('channel_fee'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('channel_fee')!!}</span>@endif
            </div>
            <div class="col s12 m4 input-field">
               <label class="active">Gateway:</label>
               <?php
                  $defaultSelection = ["1" => "CorelPay"];
               ?>
               {!! Form::select('gateway_id', $defaultSelection, null, ['class' => 'form-control', 'id' => 'gateway_id']) !!}
               @if ($errors->has('gateway_id'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('gateway_id')!!}</span>@endif
            </div>
            <div class="col s12 m4 input-field">
               <label  class="active">Gateway Fee Type:</label>
               <?php
                  $defaultSelection = ["1" => "Percentage of total ticket amount", "2" => "Flat amount"];
               ?>
               {!! Form::select('gateway_fee_type', $defaultSelection, null, ['class' => 'form-control', 'id' => 'gateway_fee_type']) !!}
               @if ($errors->has('gateway_fee_type'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('gateway_fee_type')!!}</span>@endif
            </div>
            <div class="col s12 m4 input-field">
               <label for="rtss_certification">Gateway Fee:</label>
               {!! Form::text('gateway_fee', null, ['id' => 'gateway_fee']) !!}
               @if ($errors->has('gateway_fee'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('gateway_fee')!!}</span>@endif
            </div>
            <div class="col s12 m4 input-field">
               <label class="active">No. of Days Booking (Days):</label>
               {!! Form::text('booking_no_of_days', null, ['id' => 'booking_no_of_days']) !!} 
               @if ($errors->has('booking_no_of_days'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('booking_no_of_days')!!}</span>@endif
            </div>
            <div class="col s12 m4 input-field">
               <label class="active">Seat Lock time (Minutes):</label>
               {!! Form::text('seat_block_time', null, ['id' => 'seat_block_time']) !!} 
               @if ($errors->has('seat_block_time'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('seat_block_time')!!}</span>@endif
            </div>
        	<div class="row">
              <div class="input-field col s12">
                <button class="modal-action modal-close btn cyan waves-effect waves-light right" type="submit">Submit
                  <i class="material-icons right">send</i>
              	</button>
                <!--<a id="confirmInfo" class="btn cyan waves-effect waves-light right modal-trigger" href="#modalInfo">Submit
                  <i class="material-icons right">send</i>
                </a>-->
              </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Modal Structure -->
<div id="modalInfo" class="modal modal-fixed-footer">
   <div class="modal-content">
      <!--Start modal content-->
		<div class="row">
           <div class="col s12 m12 l12">
              <div class="card-panel">
                 <h4 class="header2">Basic Information</h4>
                 <div class="row">
                    <div class="col s12 m4 input-field">
                       <label class="active">Company Name:</label>
                       <p id="lblname"></p>
                    </div>
                    <div class="col s12 m4 input-field">
                       <label class="active">Company Code:</label>
                       <p id="lblcode"></p>
                    </div>
                    <div class="col s12 m4 input-field">
                       <label class="active">Type:</label>
                       <p id="lbltype"></p>
                    </div>
                    <div class="col s12 m4 input-field">
                       <label class="active">Coverage:</label>
                       <p id="lblcoverage"></p>
                    </div>
                    <div class="col s12 m4 input-field">
                       <label class="active">Country:</label>
                       <p id="lblcountry"></p>
                    </div>
                    <div class="col s12 m4 input-field">
                       <label class="active">RC No.:</label>
                       <p id="lblrc_no"></p>
                    </div>
                    <div class="col s12 m4 input-field">
                       <label class="active">Reg Date:</label>
                       <p id="lblreg_date"></p>
                    </div>
                    <div class="col s12 m4 input-field">
                       <label class="active">USSD Code:</label>
                       <p id="lblussd_code"></p>
                    </div>
                    <div class="col s12 m4 input-field">
                       <label class="active">Company Type:</label>
                       <p id="lblcompany_type"></p>
                    </div>
                    <div class="col s12 m4 input-field">
                       <label class="active">Description:</label>
                       <p id="lbldescription"></p>
                    </div>
                    <div class="col s12 m4 input-field">
                       <label class="active">Status:</label>
                       <p id="lblstatus"></p>
                    </div>
                    <div class="col s12 m4 input-field">
                       <label class="active">Image:</label>
                       <p id="lbllogo"></p>
                    </div>
                 </div>
              </div>
           </div>
        </div>
        <div class="row">
           <div class="col s12 m12 l12">
              <div class="card-panel">
                 <h4 class="header2">Company Contact</h4>
                 <div class="row">
                    <div class="col s12 m4 input-field">
                       <label class="active">Address:</label>
                       <p id="lbladdress"></p>
                    </div>
                    <div class="col s12 m4 input-field">
                       <label class="active">Email:</label>
                       <p id="lblemail"></p>
                    </div>
                    <div class="col s12 m4 input-field">
                       <label class="active">Password:</label>
                       <p id="lblpassword">******</p>
                    </div>
                    <div class="col s12 m4 input-field">
                       <label class="active">Website:</label>
                       <p id="lblwebsite"></p>
                    </div>
                    <div class="col s12 m4 input-field">
                       <label class="active">LinkedIn:</label>
                       <p id="lbllinkedin"></p>
                    </div>
                    <div class="col s12 m4 input-field">
                       <label class="active">Twitter:</label>
                       <p id="lbltwitter"></p>
                    </div>
                    <div class="col s12 m4 input-field">
                       <label class="active">IG:</label>
                       <p id="lblig"></p>
                    </div>
                    <div class="col s12 m4 input-field">
                       <label class="active">Contact Person_no:</label>
                       <p id="lblcontact_person"></p>
                    </div>
                    <div class="col s12 m4 input-field">
                       <label class="active">Contact Person No:</label>
                       <p id="lblcontact_person_no"></p>
                    </div>
                    <div class="col s12 m4 input-field">
                       <label class="active">WhatsApp:</label>
                       <p id="lblwhatsapp"></p>
                    </div>
                    <div class="col s12 m4 input-field">
                       <label class="active">Phone Number:</label>
                       <p id="lblphone_number"></p>
                    </div>
                 </div>
              </div>
           </div>
        </div>
        <div class="row">
           <div class="col s12 m12 l12">
              <div class="card-panel">
                 <h4 class="header2">RTSS Information</h4>
                 <div class="row">
                 	<div class="col s12 m4 input-field">
                       <label class="active">RTSS Reg No:</label>
                       <p id="lblrtss_reg_no"></p>
                    </div>
                    <div class="col s12 m4 input-field">
                       <label class="active">RTSS Cert. Code:</label>
                       <p id="lblrtss_cert_code"></p>
                    </div>
                    <div class="col s12 m4 input-field">
                       <label class="active">RTSSS Certification:</label>
                       <p id="lblrtss_certification"></p>
                    </div>
                 </div>
              </div>
           </div>
        </div>
        
        <div class="row">
           <div class="col s12 m12 l12">
              <div class="card-panel">
                 <h4 class="header2">Billing Information</h4>
                 <div class="row">
                 	<div class="col s12 m4 input-field">
                       <label class="active">Charge Type:</label>
                       <p id="charge_type"></p>
                    </div>
                    <div class="col s12 m4 input-field">
                       <label class="active">Channel Fee Type:</label>
                       <p id="channel_fee_type"></p>
                    </div>
                    <div class="col s12 m4 input-field">
                       <label class="active">Channel Fee:</label>
                       <p id="channel_fee"></p>
                    </div>
                    <div class="col s12 m4 input-field">
                       <label class="active">Gateway:</label>
                       <p id="gateway_id"></p>
                    </div>
                    <div class="col s12 m4 input-field">
                       <label class="active">Gateway Fee Type:</label>
                       <p id="gateway_fee_type"></p>
                    </div>
                    <div class="col s12 m4 input-field">
                       <label class="active">Gateway Fee:</label>
                       <p id="gateway_fee"></p>
                    </div>
                    <div class="col s12 m4 input-field">
                       <label class="active">No. of Days Booking:</label>
                       <p id="booking_no_of_days"></p>
                    </div>
                    <div class="col s12 m4 input-field">
                       <label class="active">Seat Lock time:</label>
                       <p id="seat_block_time"></p>
                    </div>
                 </div>
              </div>
           </div>
        </div>
      <!--End modal content-->
   </div>
   <div class="modal-footer">
   	  <a href="#!" class="modal-action modal-close btn red waves-effect waves-light">Close</a> &nbsp;
      <button class="modal-action modal-close btn cyan waves-effect waves-light right" type="submit">Submit
          <i class="material-icons right">send</i>
      </button>
   </div>
</div>
</form>
@endsection

@push('scripts')

<script type="text/javascript">
    	$('.btn-add-phone').click(function(){
			var index = $('.phone-input').length + 1;
			$('#phoneDiv').append(''+
					'<div class="input-group phone-input"><button class="prefix btn-floating waves-effect waves-light red accent-2 btn-remove-phone" type="button"><i class="material-icons">clear</i></button>'+
						'<input class="form-control" required="" placeholder="Phone Number" id="phone_number" name="phone_number[]" type="text" value="">'+
					'</div>'
			);
		});
		
		$(document.body).on('click', '.btn-remove-phone' ,function(){
			$(this).closest('.phone-input').remove();
		});
		
		$('.btn-add-whatsapp').click(function(){
			var index = $('.whatsapp-input').length + 1;
			$('#whatsappDiv').append(''+
					'<div class="input-group whatsapp-input"><button class="prefix btn-floating waves-effect waves-light red accent-2 btn-remove-whatsapp" type="button"><i class="material-icons">clear</i></button>'+
						'<input class="form-control" required="" placeholder="Whatsapp" id="whatsapp" name="whatsapp[]" type="text" value="">'+
					'</div>'
			);
		});
		
		$(document.body).on('click', '.btn-remove-whatsapp' ,function(){
			$(this).closest('.whatsapp-input').remove();
		});
		
		$("#confirmInfo").on('click',function(){
			$("#lblname").html($("#name").val());
			$("#lblcode").html($("#code").val());
			$("#lbltype").html($("#type option:selected").html());
			$("#lblcoverage").html($("#coverage option:selected").html());
			$("#lblcountry").html($("#country option:selected").html());
			$("#lblrc_no").html($("#rc_no").val());
			$("#lblreg_date").html($("#reg_date").val());
			$("#lblussd_code").html($("#ussd_code").val());
			$("#lblcompany_type").html($("#company_type").val());
			$("#lbldescription").html($("#description").val());
			$("#lblstatus").html("Inactive");
			if ($("#status").is(':checked'))
			{
				$("#lblstatus").html("Active");
			}
			$("#lbllogo").html($('input[type=file]').val().replace(/C:\\fakepath\\/i, ''));
			$("#lbladdress").html($("#address").val());
			$("#lblemail").html($("#email").val());
			$("#lblwebsite").html($("#website").val());
			$("#lbllinkedin").html($("#linkedin").val());
			$("#lbltwitter").html($("#twitter").val());
			$("#lblig").html($("#ig").val());
			$("#lblcontact_person").html($("#contact_person").val());
			$("#lblcontact_person_no").html($("#contact_person_no").val());
							
			$("#lblphone_number").html('')
			$('input[name^="phone_number"]').each(function() {
				$("#lblphone_number").append($(this).val()+"<br />");
			});
			
			$("#lblwhatsapp").html('');
			$('input[name^="whatsapp"]').each(function() {
				$("#lblwhatsapp").append($(this).val()+"<br />");
			});
						
			$("#lblrtss_reg_no").html($("#rtss_reg_no").val());
			$("#lblrtss_cert_code").html($("#rtss_cert_code").val());
			$("#lblrtss_certification").html($("#rtss_certification").val());
			
			
			$("#charge_type").html($("#charge_type option:selected").html());
			$("#channel_fee_type").html($("#channel_fee_type option:selected").html());
			$("#channel_fee").html($("#channel_fee").val());
			$("#gateway_id").html($("#gateway_id option:selected").html());
			$("#gateway_fee_type").html($("#gateway_fee_type option:selected").html());
			$("#gateway_fee").html($("#gateway_fee").val());
			$("#booking_no_of_days").html($("#booking_no_of_days").val());
			$("#seat_block_time").html($("#seat_block_time").val());
		});
</script>

@endpush
