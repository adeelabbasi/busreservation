@extends('cms.layout.app')
@section('breadcrumbs')
<div id="breadcrumbs-wrapper">
   <!-- Search for small screen -->
   <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
      <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
   </div>
   <div class="container">
      <div class="row">
         <div class="col s10 m6 l6">
            <h5 class="breadcrumbs-title">Show Company</h5>
            <ol class="breadcrumbs">
               <li><a href="{{ url($guard_url) }}">Home</a>
               </li>
               <li><a href="{{ url($guard_url.'company') }}">Company</a>
               </li>
            </ol>
         </div>
         <div class="col s2 m6 l6">
            <a class="btn waves-effect waves-light breadcrumbs-btn right" href="{{ url($guard_url) }}" data-activates="dropdown1">
            <i class="material-icons">home</i>
            </a>
            
  <br />
       </div>
      </div>
   </div>
</div>
@endsection
@section('content')
<br />
<div class="divider"></div>
<br />
<div class="row">
   <div class="col s12 m8 l8">
      <div id="flight-card" class="card" style="overflow: hidden;">
          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
            <div class="card-title">
              <h6 class="flight-card-title">Basic Information</h6>
            </div>
          </div>
          <div class="card-content" style="background-color:#FFF">
            <div class="col s12 m4">
               <label class="active">Company Name:</label>
               <p id="lblname">{{ $info_Company->name }}</p>
            </div>
            <div class="col s12 m4">
               <label class="active">Company Code:</label>
               <p id="lblcode">{{ $info_Company->short_name }}</p>
            </div>
            <div class="col s12 m4">
               <label class="active">Type:</label>
               <p id="lbltype">
               	<?php
					switch($info_Company->type)
					{
						case 1:
							echo "Airline";
						break;
						case 2:
							echo "Bus";
						break;
						case 3:
							echo "Train";
						break;
						case 4:
							echo "Hotel";
						break;
					}
				?>
               </p>
            </div>
            <div class="col s12 m4">
               <label class="active">Coverage:</label>
               <p id="lblcoverage">
               	<?php
					switch($info_Company->coverage)
					{
						case 1:
							echo "Domestic";
						break;
						case 2:
							echo "International";
						break;
					}
				?>
               </p>
            </div>
            <div class="col s12 m4">
               <label class="active">Country:</label>
               <p id="lblcountry">{{ $info_Company->Country()->First()->name }}</p>
            </div>
            <div class="col s12 m4">
               <label class="active">RC No.:</label>
               <p id="lblrc_no">{{ $info_Company->rc_no }}</p>
            </div>
            <div class="col s12 m4">
               <label class="active">Reg Date:</label>
               <p id="lblreg_date">{{ $info_Company->reg_date }}</p>
            </div>
            <div class="col s12 m4">
               <label class="active">USSD Code:</label>
               <p id="lblussd_code">{{ $info_Company->Company_ussd()->First()->code }}</p>
            </div>
            <div class="col s12 m4">
               <label class="active">Company Type:</label>
               <p id="lblcompany_type">
               	<?php
					switch($info_Company->local_govt)
					{
						case 1:
							echo "Private";
						break;
						case 2:
							echo "Commercial";
						break;
						case 3:
							echo "Government";
						break;
					}
				?>
               </p>
            </div>
            <div class="col s12 m4">
               <label class="active">Description:</label>
               <p id="lbldescription">{{ $info_Company->description }}</p>
            </div>
            <div class="col s12 m4">
               <label class="active">Status:</label>
               <p id="lblstatus">{{ ($info_Company->status==1?"Active":"Inactive") }}</p>
            </div>
         </div>
      </div>
   </div>
   <div class="col s12 m4 l4">
     <div id="flight-card" class="card" style="overflow: hidden;">
          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
            <div class="card-title">
              <h6 class="flight-card-title">Company Logo</h6>
            </div>
          </div>
          <div class="card-content" style="background-color:#FFF">
         	<div class="col s12 m12 l12">
            	<img src="{{ url('media/logo').'/'.$info_Company->logo }}" alt="{{ $info_Company->name }}" />
            </div>
         </div>
  	  </div>
    </div>
</div>
<div class="row">
   <div class="col s12 m8 l8">
      <div id="flight-card" class="card" style="overflow: hidden;">
          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
            <div class="card-title">
              <h6 class="flight-card-title">Company Information</h6>
            </div>
          </div>
          <div class="card-content" style="background-color:#FFF">
            <div class="col s12 m4">
               <label class="active">Address:</label>
               <p id="lbladdress">{{ $info_Company->address }}</p>
            </div>
            <div class="col s12 m4">
               <label class="active">Email:</label>
               <p id="lblemail">{{ $info_Company->email }}</p>
            </div>
            <div class="col s12 m4">
               <label class="active">Password:</label>
               <p id="lblpassword">******</p>
            </div>
            <div class="col s12 m4">
               <label class="active">Website:</label>
               <p id="lblwebsite">{{ $info_Company->website }}</p>
            </div>
            <div class="col s12 m4">
               <label class="active">LinkedIn:</label>
               <p id="lbllinkedin">{{ $info_Company->linkedin }}</p>
            </div>
            <div class="col s12 m4">
               <label class="active">Twitter:</label>
               <p id="lbltwitter">{{ $info_Company->twitter }}</p>
            </div>
            <div class="col s12 m4">
               <label class="active">IG:</label>
               <p id="lblig">{{ $info_Company->ig }}</p>
            </div>
            <div class="col s12 m4">
               <label class="active">Contact Person_no:</label>
               <p id="lblcontact_person">{{ $info_Company->contact_person }}</p>
            </div>
            <div class="col s12 m4">
               <label class="active">Contact Person No:</label>
               <p id="lblcontact_person_no">{{ $info_Company->contact_person_no }}</p>
            </div>
            <div class="col s12 m4">
               <label class="active">WhatsApp:</label>
               <p id="lblwhatsapp">{{ rtrim(implode(',', $info_Company->Company_whatsapp()->Get()->Pluck('number')->ToArray()), ',') }}</p>
            </div>
            <div class="col s12 m4">
               <label class="active">Phone Number:</label>
               <p id="lblphone_number">{{ rtrim(implode(', ', $info_Company->Company_phone()->Get()->Pluck('number')->ToArray()), ',') }}</p>
            </div>
  <br />
       </div>
      </div>
   </div>
</div>
<div class="row">
   <div class="col s12 m8 l8">
       <div id="flight-card" class="card" style="overflow: hidden;">
          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
            <div class="card-title">
              <h6 class="flight-card-title">RTSS Information</h6>
            </div>
          </div>
          <div class="card-content" style="background-color:#FFF">
            <div class="col s12 m4">
               <label class="active">RTSS Reg No:</label>
               <p id="lblrtss_reg_no">{{ $info_Company->rtss_reg_no }}</p>
            </div>
            <div class="col s12 m4">
               <label class="active">RTSS Cert. Code:</label>
               <p id="lblrtss_cert_code">{{ $info_Company->rtss_cert_code }}</p>
            </div>
            <div class="col s12 m4">
               <label class="active">RTSSS Certification:</label>
               <p id="lblrtss_certification">{{ $info_Company->rtss_certification }}</p>
            </div>
  <br />
       </div>
      </div>
   </div>
</div>
@endsection