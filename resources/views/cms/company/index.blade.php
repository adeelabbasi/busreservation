@extends('cms.layout.app')
@section('breadcrumbs')
<div id="breadcrumbs-wrapper">
   <!-- Search for small screen -->
   <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
      <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
   </div>
   <div class="container">
      <div class="row">
         <div class="col s10 m6 l6">
            <h5 class="breadcrumbs-title">Company</h5>
            <ol class="breadcrumbs">
               <li><a href="{{ url($guard_url) }}">Home</a>
               </li>
            </ol>
         </div>
         <div class="col s2 m6 l6">
            <a class="btn waves-effect waves-light breadcrumbs-btn right" href="{{ url($guard_url) }}" data-activates="dropdown1">
            <i class="material-icons">home</i>
            </a>
            
         </div>
      </div>
   </div>
</div>
@endsection
@section('content')
<div class="row">
   <div class="col s12 m12 l12">
      <div id="flight-card" class="card">
          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
            <div class="card-title">
              <h5 class="flight-card-title">Companies detail <span> 
        @if(Auth::guard($guard)->User()->can('Add Company'))<a class="btn-floating waves-effect waves-light right top-add" href="{{ url($guard_url.'company/create') }}"><i class="material-icons">add</i></a>@endif</span></h5>
            </div>
          </div>
          <div class="card-content" style="background-color:#FFF">
            <table id="viewForm" class="bordered striped responsive-table display" cellspacing="0" style="width:100%;">
              <thead>
                <tr>
                  <th>Company Name</th>
                  <th>Company ID</th>
                  <th>Type</th>
                  <th>Coverage</th>
                  <th>Country</th>
                  <th>Phone No.</th>
                  <th>Reg Date.</th>
                  <th>Status</th>
                  <th>Sequence</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
 		  </div>
        </div>
      </div>
    </div>
 </div>     
<!-- Modal Structure -->
<div id="passModel" class="modal" style="width:30%">
    <div class="modal-content">
    	<input type="hidden" id="deleteUrl" />
    	<h4>Delete <span id="deleteName"></span></h4>
    	<p id="deleteText">A bunch of text</p>
		<div class="row">
            <div class="col s12 m12 input-field" style="padding:0px">
               <label for="name" style="left:0px">Password:</label>
                <input type="password" id="password" name="password" />
            </div>
        </div>
    </div>

    <div class="modal-footer" style="padding: 0px 20px;">
    	<a href="#!" class="modal-action modal-close btn waves-effect waves-light red left">Submit</a>
    </div>
</div>
@endsection

@push('scripts')

<script type="text/javascript">
    $('#viewForm').DataTable({
        "processing": true,
        "serverSide": true,
		"ajax": "{{url($guard_url.'company/grid')}}",
        "columns": [
			{ data: 'name', name: 'name' },
			{ data: 'short_name', name: 'short_name' },
            { data: 'type', name: 'type' },
			{ data: 'coverage', name: 'coverage' },
			{ data: 'country_id', name: 'country_id' },
			{ data: 'phone_number', name: 'phone_number' },
			{ data: 'reg_date', name: 'reg_date' },
			{ data: 'status', name: 'status' },
			{ data: 'sequence', name: 'sequence' },
			{ data: 'edit', name: 'edit', orderable: false, searchable: false }
		],
		
		"lengthMenu": [ [5, 10, 25, 50, -1], [5, 10, 25, 50, "All"] ],
		dom: 'Blfrtip',
		buttons: [
			{
				extend : 'csv',
				title : 'All Values',
				exportOptions : {
					modifier : {
						// DataTables core
						order : 'index', // 'current', 'applied',
						//'index', 'original'
						page : 'all', // 'all', 'current'
						search : 'none' // 'none', 'applied', 'removed'
					},
					columns: [ 0, 1, 2, 3, 4, 5, 6, 7 ]
				}
			},
			{
				extend : 'print',
				title : 'Companies',
				exportOptions : {
					modifier : {
						// DataTables core
						order : 'index', // 'current', 'applied',
						//'index', 'original'
						page : 'all', // 'all', 'current'
						search : 'none' // 'none', 'applied', 'removed'
					},
					columns: [ 0, 1, 2, 3, 4, 5, 6, 7 ]
				}
			}
		],
		//order: [ [0, 'desc'] ]
    });
	
	$('#viewForm').on('click', '#btnDeletePassword[data-remote]', function (e) { 
		$('#deleteUrl').val($(this).data('remote'));
		$('#deleteName').html($(this).data('name'));
		$('#deleteText').html($(this).data('message'));
		$('#passModel').modal('open');
		return false;
	});
	
	$('.modal-close').on('click', function (e) { 
		var baseUrl = $('meta[name="base-url"]').attr('content');
		var url = baseUrl+$('#deleteUrl').val();
		// confirm then
		$.ajaxSetup({
			headers: {
			  'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
			}
		});
		//alert(url)
		$.ajax({
			url: url,
			type: 'DELETE',
			dataType: 'text',
			data: {method: '_DELETE', "password":$('#password').val(), submit: true},
			success: function (response) {	
				if(response=="0")
				{
					alert("Incorrect password");
				}
				else
				{
					$('#viewForm').DataTable().draw(false);
					$('#deleteUrl').val('');
					$('#deleteName').html('');
					$('#deleteText').html('');
					$('#passModel').modal('close');
				}
			},
			error: function (result, status, err) {
				console.log(result.responseText);
				alert(status.responseText);
				alert(err.Message);
			},
		});
		
		return false;
	});
	
	$(document).on('blur','.sequence_id', function(){
		var sequence = $(this).val();
		var company_id = $(this).data("company_id");
		if (!$.isNumeric(sequence)) {
		   alert("Invalid integr value");
		   return false;
		}
		
		var baseUrl = $('meta[name="base-url"]').attr('content');
		var subUrl = $('meta[name="sub-url"]').attr('content');
		var url = baseUrl+subUrl+'company/sequence/'+company_id;
		// confirm then
		$.ajaxSetup({
			headers: {
			  'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
			}
		});
		//alert(url)
		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'json',
			data: {method: '_POST', "sequence":sequence, submit: true},
			success: function (response) {	
				
			},
			error: function (result, status, err) {
				console.log(result.responseText);
				//alert(status.responseText);
				//alert(err.Message);
			},
		});
		
		return false;
	});
</script>

@endpush