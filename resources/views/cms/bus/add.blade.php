@extends('cms.layout.app')
@section('breadcrumbs')
<div id="breadcrumbs-wrapper">
   <!-- Search for small screen -->
   <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
      <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
   </div>
   <div class="container">
      <div class="row">
         <div class="col s10 m6 l6">
            <h5 class="breadcrumbs-title">Add Fleet</h5>
            <ol class="breadcrumbs">
               <li><a href="{{ url($guard_url) }}">Home</a>
               </li>
               <li><a href="{{ url($guard_url.'bus') }}">Fleet</a>
               </li>
            </ol>
         </div>
         <div class="col s2 m6 l6">
            <a class="btn waves-effect waves-light breadcrumbs-btn right" href="{{ url($guard_url) }}" data-activates="dropdown1">
            <i class="material-icons">home</i>
            </a>
            
         </div>
      </div>
   </div>
</div>
@endsection
@section('content')
{!! Form::open([ 'url' => $guard_url.'bus/', 'id' => 'main-form' ]) !!}
<br />
<div class="divider"></div>
<br />
<div class="row">
   <div class="col s12 m8 l8">
      <div id="flight-card" class="card">
          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
            <div class="card-title">
              <h6 class="flight-card-title">Fleet Information</h6>
            </div>
          </div>
         <div class="card-content" style="background-color:#FFF">
           	@if($guard=="admin")
            <div class="col s12 m4 input-field">
              <?php
                $defaultSelection = ["" => "Select Company"];
				foreach($info_Companies as $Companies)
                {
                    $defaultSelection = $defaultSelection +  array($Companies->id => ($Companies->name));
                }
              ?>
              {!! Form::select('company_id', $defaultSelection, null, ['class' => 'form-control', 'id' => 'CompanyRoute']) !!}
              @if ($errors->has('company_id'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('company_id')!!}</span>@endif
           </div>
           @else
           <input type="hidden" name="company_id" value="{{ Auth::guard($guard)->User()->Company()->First()->id }}"  />
           @endif
           <div class="col s12 m4 input-field">
              <label for="name">Name:</label>
              {!! Form::text('name', null, ['class' => 'form-control' , 'required', 'id' => 'name']) !!}
              @if ($errors->has('name'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('name')!!}</span>@endif
           </div>
           <div class="col s12 m4 input-field">
              <label for="seats">Seats:</label>
              {!! Form::text('seats', null, ['class' => 'form-control' , 'required', 'id' => 'seats']) !!}
              @if ($errors->has('seats'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('seats')!!}</span>@endif
           </div>
           <div class="col s12 m4">
           <br />
               <div class="switch">
                    <label>
                    Inactive
                    <input type="checkbox" checked="checked" name="status"/>
                    <span class="lever"></span>
                    Active
                    </label>
                </div>
            </div>
           <div class="row">
              <div class="input-field col s12">
                <button class="btn cyan waves-effect waves-light right" type="submit">Submit
                  <i class="material-icons right">send</i>
                </button>
              </div>
            </div>
        </div>
      </div>
   </div>
</div>

</form>
@endsection
