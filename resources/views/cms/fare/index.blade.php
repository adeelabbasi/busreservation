@extends('cms.layout.app')
@section('breadcrumbs')
<div id="breadcrumbs-wrapper">
   <!-- Search for small screen -->
   <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
      <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
   </div>
   <div class="container">
      <div class="row">
         <div class="col s10 m6 l6">
            <h5 class="breadcrumbs-title">Add fare type</h5>
            <ol class="breadcrumbs">
               <li><a href="{{ url($guard_url) }}">Home</a>
               </li>
               <li><a href="{{ url($guard_url.'schedule') }}">schedule</a>
               </li>
            </ol>
         </div>
         <div class="col s2 m6 l6">
            <a class="btn waves-effect waves-light breadcrumbs-btn right" href="{{ url($guard_url) }}" data-activates="dropdown1">
            <i class="material-icons">home</i>
            </a>
            
         </div>
      </div>
   </div>
</div>
@endsection
@section('content')


<!--Preselecting a tab-->
<div class="row">
  <div class="col s12">
     <div class="row">
        <div class="col s8">
           <ul class="tabs z-depth-1 cyan">
                
                <li class="tab col s4"><a class="white-text waves-effect waves-light" onclick="location.href='{{ url($guard_url.'schedule/'.$info_Schedule->id.'/edit') }}'" href="#">Schedule</a>
                </li>
                <li class="tab col s4"><a class="white-text waves-effect waves-light" onclick="location.href='{{ url($guard_url.'fare_type?ScheduleID='.$info_Schedule->id) }}'" href="#">Fare Type</a>
                </li>
                <li class="tab col s4"><a class="white-text waves-effect waves-light active" onclick="location.href='{{ url($guard_url.'fare?ScheduleID='.$info_Schedule->id) }}'" href="#">Fare</a>
                </li>
           </ul>
        </div>
     </div>
  </div>
</div>

<br />
<div class="divider"></div>
<br />
<div class="row">
   <div class="col s12 m8 l8"> 
      <div id="flight-card" class="card">
          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
            <div class="card-title">
              <h6 class="flight-card-title">Fare Type Information</h6>
            </div>
          </div>
         <div class="card-content" style="background-color:#FFF">
           {!! Form::open([ 'url' => $guard_url.'fare/', 'id' => 'main-form' ]) !!}
           <input type="hidden" id="company_id" name="company_id" value="{{ $info_Schedule->company_id }}"  />
           <input type="hidden" id="schedule_id" name="schedule_id" value="{{ $info_Schedule->id }}"  />
           <input type="hidden" id="route_id" name="route_id" value="{{ $info_Schedule->route_id }}"  />
           <div class="col s12 m4 input-field">
              <?php
                $defaultSelection = [];//["" => "Select Service"];
				foreach($info_FareTypes as $info_FareType)
                {
                    $defaultSelection = $defaultSelection +  array($info_FareType->id => ($info_FareType->name));
                }
              ?>
              {!! Form::select('fare_type_id', $defaultSelection, null, ['class' => 'form-control', 'id' => 'ScheduleFareType']) !!}
              @if ($errors->has('fare_type_id'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('fare_type_id')!!}</span>@endif
           </div>

           <div class="col s12 m4 input-field">
                <label for="name" id="lblFareReturn">Fare return(%):</label>
                {!! Form::text('fare_return', '0', ['class' => 'form-control' , 'required', 'id' => 'fare_return']) !!}
                @if ($errors->has('fare_return'))<span style="color:red; right:10px; font-size:10px; position:absolute; top:45px;">{!!$errors->first('fare_return')!!}</span>@endif
            </div>

           <div class="row">
            <div class="col s12 m12">
              	<br />
                <div class="divider"></div>
                <br />
              <table id="fareForm" class="responsive-table display" cellspacing="0" border="1px">
              <thead class="gradient-45deg-indigo-light-blue accent-1" style="color:#FFF">
                <tr>
                  <th style="padding: 35px; text-align:center; border: 1px solid #000;"></th>
                  @foreach($info_Schedule->Route()->First()->Route_detail()->Where('sequence','>','1')->OrderBy('sequence')->Get() as $Route_detail1)
                  <th style="text-align:center; border: 1px solid #000; ">{{ $Route_detail1->Terminal()->First()->name }}</th>
                  @endforeach
                </tr>
              </thead>
              <tbody>
                  @foreach($info_Schedule->Route()->First()->Route_detail()->OrderBy('sequence')->Get() as $Route_detail2)
                  @if($info_Schedule->Route()->First()->Route_detail()->Get()->Max('sequence')!=$Route_detail2->sequence)
                      <tr>
                        <th style="text-align:center; border: 1px solid;">{{ $Route_detail2->Terminal()->First()->name }}</th>
                        <?php
                        for($i=1; $i<$Route_detail2->sequence; $i++)                       	
                        {
                            echo '<td style="text-align:center; border: 1px solid;"></td>';
                        }
                        ?>
                        @foreach($info_Schedule->Route()->First()->Route_detail()->Where('sequence','>',$Route_detail2->sequence)->OrderBy('sequence')->Get() as $Route_detail3)
                            <td style="text-align:center; border: 1px solid;">
                                    <input type="text" class="form-control fare_values" name="{{ 'terminal_'.$Route_detail2->Terminal()->First()->id.'_'.$Route_detail3->Terminal()->First()->id }}" id="{{ 'terminal_'.$Route_detail2->Terminal()->First()->id.'_'.$Route_detail3->Terminal()->First()->id }}" value="" placeholder="Fare" required/>                                
                                <!--{{ $Route_detail2->Terminal()->First()->name.' - '.$Route_detail3->Terminal()->First()->name }}-->
                            </td>
                        @endforeach
                      </tr>
                  @endif
                  @endforeach
              </tbody>
              </tr>
            </table>
            </div>

            <div class="col s12 m12 input-field">
            <button class="btn cyan waves-effect waves-light right" type="submit">Submit
              <i class="material-icons right">send</i>
            </button>
            <a href="{{ url($guard_url.'schedule/create') }}"  class="btn green waves-effect waves-light" type="submit">New
            </a>
            <a href="{{ url($guard_url.'schedule/'.$info_Schedule->id.'/copy') }}"  class="btn blue waves-effect waves-light" type="submit">Copy
            </a>
            </div>
            </form>
           </div>
       </div>
      </div>
   </div>
   <div class="col s12 m4 l4"> 
   		<div id="flight-card" class="card">
          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
            <div class="card-title">
              <h6 class="flight-card-title">Status</h6>
            </div>
          </div>
         <div class="card-content" style="background-color:#FFF">
             <ul id="task-card" class="collection with-header">
             @foreach($info_FareTypes as $info_FareType)
                @if($info_Schedule->Fare()->Where(['fare_type_id' => $info_FareType->id])->Get()->Count()>0)
                    <li class="collection-item">
                         {{ $info_FareType->name }} / {{ $info_FareType->Service()->First()->name }} <i class="material-icons" style="color:#0F0">done</i>
                    </li>
                @else
                    <li class="collection-item">
                         {{ $info_FareType->name }} / {{ $info_FareType->Service()->First()->name }} <i class="material-icons" style="color:#F00">clear</i>
                    </li>
                @endif
             @endforeach
             </ul>
         </div>
        </div>
   </div>
</div>
@endsection

@push('scripts')

<script>
$( document ).ready(function() {
	updateFare();
});
$('#ScheduleFareType').on('change', function (e) { 
	updateFare();
});

function updateFare()
{
	$("#lblFareReturn").addClass("active"); 
	$("#fare_return").val('0');
	$(".fare_values").each(function() {
		$(this).val('');
	});
	var company_id = $("#company_id").val();
	var schedule_id = $("#schedule_id").val();
	var fare_type_id = $('#ScheduleFareType option:selected').val();
	var baseUrl = $('meta[name="base-url"]').attr('content');
	var url = baseUrl+'{{$guard_url}}company/schedule/fare/'+company_id;
	$.ajax({
		url: url,
		type: 'GET',
		dataType: 'json',
		data: {method: '_GET', "schedule_id":schedule_id, "fare_type_id":fare_type_id, "_token": "{{ csrf_token() }}" , submit: true},
		success: function (response) {
			console.log(response);
			$("#fare_return").val(response[0].fare_return);
			$("#lblFareReturn").addClass("active"); 
			$.each(response,function(key,value){
				$("#"+value.id).val(value.price);
			});
		},
		error: function (result, status, err) {
			alert(result.responseText);
			alert(status.responseText);
			alert(err.Message);
		},
	});
}
</script>
@endpush



