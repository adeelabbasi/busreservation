@extends('cms.layout.app')
@section('breadcrumbs')
<div id="breadcrumbs-wrapper">
   <!-- Search for small screen -->
   <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
      <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
   </div>
   <div class="container">
      <div class="row">
         <div class="col s10 m6 l6">
            <h5 class="breadcrumbs-title">Show Country</h5>
            <ol class="breadcrumbs">
               <li><a href="{{ url($guard_url) }}">Home</a>
               </li>
               <li><a href="{{ url($guard_url.'country') }}">Country</a>
               </li>
            </ol>
         </div>
         <div class="col s2 m6 l6">
            <a class="btn waves-effect waves-light breadcrumbs-btn right" href="{{ url($guard_url) }}" data-activates="dropdown1">
            <i class="material-icons">home</i>
            </a>
            
  <br />
       </div>
      </div>
   </div>
</div>
@endsection
@section('content')
<br />
<div class="divider"></div>
<br />
<div class="row">
   <div class="col s12 m8 l8">
      <div id="flight-card" class="card" style="overflow: hidden;">
          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
            <div class="card-title">
              <h6 class="flight-card-title">Country Information</h6>
            </div>
          </div>
         <div class="card-content" style="background-color:#FFF">
           <div class="col s12 m4 input-field">
              <label class="active">Name:</label>
              <p>{{ $info_Country->name }}</p>
           </div>
           <div class="col s12 m4 input-field">
               <label class="active">Short Name:</label>
               <p>{{ $info_Country->short_name }}</p>
           </div>
           <div class="col s12 m4">
          		<label class="active">Status:</label>
                <p id="lblstatus">{{ ($info_Country->status==1?"Active":"Inactive") }}</p>
           </div>
       </div>
   </div>
</div>

</form>
@endsection
