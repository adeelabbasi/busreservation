@extends('cms.layout.app')
@section('breadcrumbs')
<div id="breadcrumbs-wrapper">
   <!-- Search for small screen -->
   <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
      <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
   </div>
   <div class="container">
      <div class="row">
         <div class="col s10 m6 l6">
            <h5 class="breadcrumbs-title">About US</h5>
            <ol class="breadcrumbs">
               <li><a href="{{ url($guard_url) }}">Home</a>
               </li>
               <li><a href="{{ url($guard_url.'policy') }}">Policy</a>
               </li>
            </ol>
         </div>
         <div class="col s2 m6 l6">
            <a class="btn waves-effect waves-light breadcrumbs-btn right" href="{{ url($guard_url) }}" data-activates="dropdown1">
            <i class="material-icons">home</i>
            </a>
            
  <br />
       </div>
      </div>
   </div>
</div>
@endsection
@section('content')
<!--Preselecting a tab-->
<div id="preselecting-tab" class="section">
   <div class="row">
      <div class="col s12">
         <div class="row">
            <div class="col s12">
               <ul class="tabs tab-demo-active z-depth-1 cyan">
                    
                    <li class="tab col s4"><a class="white-text waves-effect waves-light active" href="javascript:void(0)" onclick="location.href = '{{ url($guard_url.'policy') }}';">Policy</a>
                    </li>
                    <li class="tab col s4"><a class="white-text waves-effect waves-light" href="javascript:void(0)" onclick="location.href = '{{ url($guard_url.'cservice') }}';">Company Services</a>
                    </li>
                    <li class="tab col s4"><a class="white-text waves-effect waves-light" href="javascript:void(0)" onclick="location.href = '{{ url($guard_url.'about') }}';">About us</a>
                    </li>
               </ul>
            </div>
            <div class="col s12">
                <br />
                <div class="divider"></div>
                <br />
                <div class="row">
                   <div class="col s12 m8 l8">
                      <div id="flight-card" class="card" style="overflow: hidden;">
                          <div class="card-header gradient-45deg-indigo-light-blue accent-2">
                            <div class="card-title">
                              <h6 class="flight-card-title">Policy Information</h6>
                            </div>
                          </div>
                         <div class="card-content" style="background-color:#FFF">
                            <div class="col s12 m4 input-field">
                               <label class="active">Company Name:</label>
                               <p>{{ $info_Policy->Company()->First()->name }}</p>
                            </div>
                            <div class="col s12 m4 input-field">
                                <label class="active">Name:</label>
                                <p>{{ $info_Policy->name }}</p>
                            </div>
                            <div class="col s12 m4 input-field">
                                <label class="active">Detail:</label>
                                <p>{{ $info_Policy->detail }}</p>
                            </div>
                            <div class="col s12 m4">
                                <label class="active">Status:</label>
                                <p id="lblstatus">{{ ($info_Policy->status==1?"Active":"Inactive") }}</p>
                            </div>
                <br />
                       </div>
                      </div>
                   </div>
                </div>
                
                </form>
			</div>
         </div>
      </div>
   </div>
</div>
@endsection

