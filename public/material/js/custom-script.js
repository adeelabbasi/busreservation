$('#countryCity').on('change', function (e) { 
	var country_id = $('#countryCity option:selected').val();
	if(country_id=="")
		return false;
	var baseUrl = $('meta[name="base-url"]').attr('content');
	var suburl = $('meta[name="sub-url"]').attr('content');
	var url = baseUrl+suburl+'country_city/'+country_id;
	//alert(url)
	$.ajax({
		url: url,
		type: 'GET',
		dataType: 'json',
		data: {method: '_GET', "_token": "{{ csrf_token() }}" , submit: true},
		success: function (response) {
			$('#cityLga').material_select('destroy');
			$("#cityLga").empty();
			$("#cityLga").append('<option>Select State/City</option>');
			
			var dataArray = [];
			for (value in response) {
				var word = response[value];
				dataArray.push({value: parseInt(value), word: word});
			}
			
			dataArray.sort(function(a, b){
				if (a.word < b.word) return -1;
				if (b.word < a.word) return 1;
				return 0;
			});
			$.each(dataArray,function(key,value){
				$("#cityLga").append('<option value="'+value.value+'">'+value.word+'</option>');
			});
			$("#cityLga").material_select();
		},
		error: function (result, status, err) {
			alert(result.responseText);
			alert(status.responseText);
			alert(err.Message);
		},
	});
	return false; 
});

$('#cityLga').on('change', function (e) { 
	var state_id = $('#cityLga option:selected').val();
	if(state_id=="")
		return false;
	var baseUrl = $('meta[name="base-url"]').attr('content');
	var suburl = $('meta[name="sub-url"]').attr('content');
	var url = baseUrl+suburl+'city_lga/'+state_id;
	// confirm then
	$.ajax({
		url: url,
		type: 'GET',
		dataType: 'json',
		data: {method: '_GET', "_token": "{{ csrf_token() }}" , submit: true},
		success: function (response) {			
			$('#Lga').material_select('destroy');
			$("#Lga").empty();
			$("#Lga").append('<option>Select Lga</option>');
			
			var dataArray = [];
			for (value in response) {
				var word = response[value];
				dataArray.push({value: parseInt(value), word: word});
			}
			
			dataArray.sort(function(a, b){
				if (a.word < b.word) return -1;
				if (b.word < a.word) return 1;
				return 0;
			});
			$.each(dataArray,function(key,value){
				$("#Lga").append('<option value="'+value.value+'">'+value.word+'</option>');
			});
			$("#Lga").material_select();

		},
		error: function (result, status, err) {
			alert(result.responseText);
			alert(status.responseText);
			alert(err.Message);
		},
	});
	return false;
});


$('#viewForm').on('click', '#btnDelete[data-remote]', function (e) { 
	if (confirm($(this).data('message'))) {		
		e.preventDefault();		
		var baseUrl = $('meta[name="base-url"]').attr('content');
		var url = baseUrl+$(this).data('remote');
		// confirm then
		$.ajaxSetup({
			headers: {
			  'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
			}
		});
		//alert(url)
		$.ajax({
			url: url,
			type: 'DELETE',
			dataType: 'json',
			data: {method: '_DELETE' , submit: true},
			error: function (result, status, err) {
				console.log(result.responseText);
				//alert(status.responseText);
				//alert(err.Message);
			},
		}).always(function (data) {
			$('#viewForm').DataTable().draw(false);
		});
	}
	return false;
});