<!--replace "http://devserver.test/busreservation/public/ussd.php" "http://localhost/itraveltest/public/ussd.php" -- emulator.php-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Test USSD</title>
</head>
<style>
	body
	{
		color:#2d2d2d;
		background:#F0f0f0;
	}
</style>
<body>
<?php
	error_reporting(0);

    $baseURL = "http://devserver.test/busreservation/public";              
	if(isset($_REQUEST["submit"]))
	{
		if($_REQUEST["submit"]=="Push")
		{
			$msisdn = $_REQUEST["msisdn"];
			$keyword = $_REQUEST["keyword"];
			$servicecode = $_REQUEST["servicecode"];
			$url = $baseURL."/ussd.php?msisdn=$msisdn&shortcode=$servicecode&smsc=USSD&message=".($keyword)."&testing=1";
		}
		else
		if($_REQUEST["submit"]=="Begin")
		{
			$msisdn = $_REQUEST["msisdn"];
			$keyword = $_REQUEST["keyword"];
			$servicecode = $_REQUEST["servicecode"];
			$url = $baseURL."/ussd.php?msisdn=$msisdn&shortcode=$servicecode&smsc=USSD&message=*88111&testing=1";
		}
		else
		{
			
			$msisdn = $_REQUEST["msisdn"];
			$keyword = $_REQUEST["keyword"];
			$shortcode = $_REQUEST["shortcode"];
			$servicecode = $_REQUEST["servicecode"];
			$url = $baseURL."/ussd.php?msisdn=$msisdn&shortcode=$servicecode&smsc=USSD&message=$shortcode&testing=1";
		}
		
		echo $url."<br />";
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
	
		$data = curl_exec($ch);
		curl_close($ch);
		
		
		$parseData = json_decode($data, true);
		
		//echo str_replace("\n","<br />",$parseData['session_msg']);
	}
	
	?>
<form action="emulator.php" action="get">
    <table align="center" cellpadding="2" cellspacing="10" style="background:#CCC; border-radius:10px;">
        <tr>
            <td colspan="2" align="center">
                <b>USSD Testing</b>
            </td>
        </tr>
        <tr>
        	<td colspan="2">
            	<div style="background:#F7F7F7; padding-left:10px; padding-top:10px; width:275px; height:20px; border:1px solid #CCC; text-align:right">
					<?php echo strlen(str_replace("\n"," ",$parseData['session_msg'])); ?>/150
                </div>
                <div style="background:#F7F7F7; padding-left:10px; padding-top:10px; width:275px; height:350px; border:1px solid #CCC">
					<?php echo str_replace("~","<br />",substr(str_replace("\n","~",$parseData['session_msg']),0,153));  ?>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                Msisdn:
            </td>
            <td>
                <input id="msisdn" name="msisdn" value="<?php 
				if(isset($_REQUEST["msisdn"])) 
					echo $_REQUEST["msisdn"]; 
				else 
					echo "2348099444309"; ?>"/>
            </td>
        </tr>
        <tr>
            <td>
                Keyword:
            </td>
            <td>
            	<?php if($parseData['session_operation'] == "continue")  {?>
                <input id="keyword" name="keyword" value="<?php if(isset($_REQUEST["keyword"])) echo $_REQUEST["keyword"];?>"/>
                <?php } else {?>
                <input id="keyword" name="keyword" value="<?php if(isset($_REQUEST["keyword"])) echo $_REQUEST["keyword"];?>" disabled="disabled"/>
                <?php }?>
            </td>
        </tr>
        <tr>
        	<td align="left">
                <!--<input id="submit" name="submit" type="submit" value="Begin"/>-->
            </td>
            <td align="right">
            	<?php if($parseData['session_operation'] == "continue") {?>
                <input id="submit" name="submit" type="submit" value="Push"/>
                <?php } else {?>
                <input id="submit" name="submit" type="submit" value="Push" disabled="disabled"/>
                <?php }?>
                
            </td>
        </tr>
        <tr>
        	<td align="left">
                <input id="submit" name="submit" type="submit" value="Company"/>
            </td>
        	<td>
                <input id="shortcode" name="shortcode" value="<?php if(isset($_REQUEST["shortcode"])) echo $_REQUEST["shortcode"]; else echo "*8014*8"; ?>" style="width:100%"/>
            </td>
        </tr>
        <tr>
            <td>
                ServiceCode:
            </td>
            <td>
                <input id="servicecode" name="servicecode" value="<?php if(isset($_REQUEST["servicecode"])) echo $_REQUEST["servicecode"]; else echo "8014"; ?>"/>
            </td>
        </tr>
    </table>
</form>
<br />
<br />
</body>
</html>
