<!--replace "http://brs.test/" "http://localhost/brs/public/" -- emulator.php-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Test USSD</title>
</head>
<style>
	body
	{
		color:#2d2d2d;
		background:#F0f0f0;
	}
</style>
<body>
<?php
                    
	if(isset($_REQUEST["submit"]))
	{
		if($_REQUEST["submit"]=="Push")
		{
			$msisdn = $_REQUEST["msisdn"];
			$keyword = $_REQUEST["keyword"];
			echo $data = file_get_contents("http://brs.test/ussd.php?session_operation=continue&session_msisdn=$msisdn&smsc=USSD&session_from=88111&session_id=75075&session_type=1&session_msg=$keyword");
			$parseData = json_decode($data, true);
			
			//echo str_replace("\n","<br />",$parseData['session_msg']);
		}
		else
		if($_REQUEST["submit"]=="Begin")
		{
			$msisdn = $_REQUEST["msisdn"];
			$keyword = $_REQUEST["keyword"];
			echo $data = file_get_contents("http://brs.test/ussd.php?session_operation=begin&session_msisdn=$msisdn&smsc=USSD&session_from=88111&session_id=75075&session_type=1&session_msg=$keyword");
			$parseData = json_decode($data, true);
			
			//echo str_replace("\n","<br />",$parseData['session_msg']);
		}
		else
		{
			$msisdn = $_REQUEST["msisdn"];
			$keyword = $_REQUEST["keyword"];
			echo $data = file_get_contents("http://brs.test/ussd.php?session_msisdn=$msisdn&smsc=USSD&session_id=5985&session_operation=begin&session_type=1&session_from=88111&session_msg=*88111*1");
			$parseData = json_decode($data, true);
			
			//echo str_replace("\n","<br />",$parseData['session_msg']);
		}
	}
	
	?>
<form action="emulator.php" action="get">
    <table align="center" cellpadding="2" cellspacing="10" style="background:#CCC; border-radius:10px;">
        <tr>
            <td colspan="2" align="center">
                <b>USSD Testing</b>
            </td>
        </tr>
        <tr>
        	<td colspan="2">
            	<div style="background:#F7F7F7; padding-left:10px; padding-top:10px; width:250px; height:20px; border:1px solid #CCC; text-align:right">
					<?php echo strlen(str_replace("\n"," ",$parseData['session_msg'])); ?>/150
                </div>
                <div style="background:#F7F7F7; padding-left:10px; padding-top:10px; width:250px; height:350px; border:1px solid #CCC">
					<?php echo str_replace("\n","<br />",$parseData['session_msg']); ?>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                Msisdn:
            </td>
            <td>
                <input id="msisdn" name="msisdn" value="<?php 
				if(isset($_REQUEST["msisdn"])) 
					echo $_REQUEST["msisdn"]; 
				else 
					echo "2348099444309"; ?>"/>
            </td>
        </tr>
        <tr>
            <td>
                Keyword:
            </td>
            <td>
            	<?php if($parseData['session_type']==1) {?>
                <input id="keyword" name="keyword" value="<?php if(isset($_REQUEST["keyword"])) echo $_REQUEST["keyword"];?>"/>
                <?php } else {?>
                <input id="keyword" name="keyword" value="<?php if(isset($_REQUEST["keyword"])) echo $_REQUEST["keyword"];?>" disabled="disabled"/>
                <?php }?>
            </td>
        </tr>
        <tr>
        	<td align="left">
                <input id="submit" name="submit" type="submit" value="Begin"/>
            </td>
            <td align="right">
            	<?php if($parseData['session_type']==1) {?>
                <input id="submit" name="submit" type="submit" value="Push"/>
                <?php } else {?>
                <input id="submit" name="submit" type="submit" value="Push" disabled="disabled"/>
                <?php }?>
                
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <input id="submit" name="submit" type="submit" value="Begin Company"/>(*88111*1)
            </td>
        </tr>
    </table>
</form>
<br />
<br />
</body>
</html>
