<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRouteDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('route_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('route_id')->unsigned();
			$table->integer('terminal_id')->unsigned();
			$table->smallInteger('sequence');
			$table->timestamps();
        });
		
		Schema::table('route_details', function(Blueprint $table) {
			$table->foreign('route_id')->references('id')->on('routes')
						->onDelete('CASCADE')
						->onUpdate('CASCADE');
		});
		
		Schema::table('route_details', function(Blueprint $table) {
			$table->foreign('terminal_id')->references('id')->on('terminals')
						->onDelete('CASCADE')
						->onUpdate('CASCADE');
		});
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('route_details');
    }
}
