<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
			$table->string('first_name',10);
            $table->string('last_name');
            $table->string('other_name');
			$table->text('address');
			$table->smallInteger('martial_status');
            $table->smallInteger('gender');
			$table->smallInteger('work_status');
			$table->string('mprs_gsm_no');
            $table->string('gsm_no_2');
            $table->string('gsm_no_3');
			$table->string('email');
			$table->string('pin');
			$table->string('kin_fullname');
            $table->string('kin_phone');
			$table->string('kin_address');
			$table->smallInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
