<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned()->nullable();
			$table->integer('customer_id')->unsigned()->nullable();
			$table->integer('schedule_id')->unsigned()->nullable();
			$table->string('refcode')->nullable();
			$table->string('msisdn');
			$table->string('TripID');
			$table->string('SelectedSeats');
			$table->string('MaxSeat');
			$table->string('DestinationID');
			$table->string('OrderID');
			$table->string('Fullname')->nullable();
			$table->string('email')->nullable();
			$table->string('nextKin')->nullable();
			$table->string('nextKinPhone')->nullable();
			$table->string('Sex')->nullable();
			$table->float('sub_total')->nullable();
			$table->float('tax')->nullable();
			$table->integer('charge_type')->nullable();
			$table->float('channel_fee')->nullable();
			$table->float('gateway_fee')->nullable();
			$table->float('total')->nullable();
			$table->string('traceId')->nullable();
			
			$table->integer('from_city_id')->nullable();
			$table->string('from_city_name')->nullable();
			$table->integer('from_terminal_id')->nullable();
			$table->string('from_terminal_name')->nullable();
			$table->integer('to_city_id')->nullable();
			$table->string('to_city_name')->nullable();
			$table->integer('to_terminal_id')->nullable();
			$table->string('to_terminal_name')->nullable();
			$table->date('schedule_date')->nullable();
			$table->string('dept_time')->nullable();
			$table->string('bus')->nullable();
			$table->string('gateway')->nullable();
			$table->smallInteger('status');
			$table->timestamps();
        });
		
		/*Schema::table('bookings', function(Blueprint $table) {
			$table->foreign('company_id')->references('id')->on('companies')
						->onDelete('CASCADE')
						->onUpdate('CASCADE');
		});
		
		Schema::table('bookings', function(Blueprint $table) {
			$table->foreign('customer_id')->references('id')->on('customers')
						->onDelete('CASCADE')
						->onUpdate('CASCADE');
		});
		
		Schema::table('bookings', function(Blueprint $table) {
			$table->foreign('schedule_id')->references('id')->on('schedules')
						->onDelete('CASCADE')
						->onUpdate('CASCADE');
		});*/
		

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
