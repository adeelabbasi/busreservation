<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLgasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lgas', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('city_id')->unsigned();
			$table->string('name',100);
			$table->string('short_name',5);
			$table->smallInteger('status');
			$table->softDeletes();
            $table->timestamps();
        });
		
		Schema::table('lgas', function(Blueprint $table) {
			$table->foreign('city_id')->references('id')->on('cities')
						->onDelete('NO ACTION')
						->onUpdate('NO ACTION');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lgas');
    }
}
