<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->unsigned()->nullable();
			$table->string('msisdn',20);
			$table->string('channel',20);
			$table->char('is_new',1)->default('0');
			$table->char('is_complete',1)->default('0');
			$table->string('ussd_code',20)->nullable();
			$table->char('is_main',1)->nullable();
			$table->string('action',50)->nullable();
			$table->integer('company_id')->nullable();
			$table->string('company_name')->nullable();
			$table->integer('route_id')->nullable();
			$table->string('route_name')->nullable();
			$table->integer('schedule_id')->nullable();
			$table->string('schedule_name')->nullable();
			$table->integer('from_city_id')->nullable();
			$table->string('from_city_name')->nullable();
			$table->integer('to_city_id')->nullable();
			$table->string('to_city_name')->nullable();
			$table->string('start_time')->nullable();
			$table->string('end_time')->nullable();
			$table->string('gateway')->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_logs');
    }
}
