<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTerminalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('terminals', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('company_id')->unsigned();
			$table->integer('lga_id')->unsigned();
			$table->string('name');
			$table->string('short_name',10);
			$table->string('address');
			$table->string('email');
			$table->string('phone_number');
			$table->smallInteger('status');
			$table->integer('sequence')->default('999');
			$table->timestamps();
        });
		
		Schema::table('terminals', function(Blueprint $table) {
			$table->foreign('company_id')->references('id')->on('companies')
						->onDelete('CASCADE')
						->onUpdate('CASCADE');
		});
		
		Schema::table('terminals', function(Blueprint $table) {
			$table->foreign('lga_id')->references('id')->on('lgas')
						->onDelete('NO ACTION')
						->onUpdate('NO ACTION');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('terminals');
    }
}
