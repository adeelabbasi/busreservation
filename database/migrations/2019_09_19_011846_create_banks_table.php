<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banks', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('bank_code')->nullable();
			$table->string('nip_code')->nullable();
			$table->string('ussd_code')->nullable();
			$table->smallInteger('status')->default(1);
			$table->timestamps();
        });
		
		/*insert into banks (name, bank_code, nip_code, ussd_code) value ('Access', '44', '14', '901');
		insert into banks (name, bank_code, nip_code, ussd_code) value ('GTB', '58', '13', '737');
		insert into banks (name, bank_code, nip_code, ussd_code) value ('First Bank', '11', '16', '894');
		insert into banks (name, bank_code, nip_code, ussd_code) value ('Zenith', '57', '15', '966');
		insert into banks (name, bank_code, nip_code, ussd_code) value ('UBA', '33', '4', '919');
		insert into banks (name, bank_code, nip_code, ussd_code) value ('Stanbic', '221', '12', '909');
		insert into banks (name, bank_code, nip_code, ussd_code) value ('EcoBank', '50', '10', '326');
		insert into banks (name, bank_code, nip_code, ussd_code) value ('Union', '32', '18', '826');
		insert into banks (name, bank_code, nip_code, ussd_code) value ('Fidelity', '70', '7', '770');
		insert into banks (name, bank_code, nip_code, ussd_code) value ('Sterling', '232', '1', '822');
		insert into banks (name, bank_code, nip_code, ussd_code) value ('FCMB', '214', '3', '329');
		insert into banks (name, bank_code, nip_code, ussd_code) value ('Keystone', '82', '2', '7111');
		insert into banks (name, bank_code, nip_code, ussd_code) value ('Standard Chattered', '68', '21', '');
		insert into banks (name, bank_code, nip_code, ussd_code) value ('Polaris', '76', '8', '833');
		insert into banks (name, bank_code, nip_code, ussd_code) value ('Wema', '35', '17', '945');
		insert into banks (name, bank_code, nip_code, ussd_code) value ('Heritage', '30', '20', '322');*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cservices');
    }
}
