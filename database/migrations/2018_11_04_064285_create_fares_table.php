<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fares', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
			$table->integer('schedule_id')->unsigned();
			$table->integer('fare_type_id')->unsigned();
			$table->integer('route_id')->unsigned();
			$table->integer('from_terminal_id')->unsigned();
			$table->integer('to_terminal_id')->unsigned();
			$table->smallInteger('fare');
			$table->smallInteger('fare_return');
			$table->timestamps();
        });
		
		Schema::table('fares', function(Blueprint $table) {
			$table->foreign('company_id')->references('id')->on('companies')
						->onDelete('CASCADE')
						->onUpdate('CASCADE');
		});
		
		Schema::table('fares', function(Blueprint $table) {
			$table->foreign('schedule_id')->references('id')->on('schedules')
						->onDelete('CASCADE')
						->onUpdate('CASCADE');
		});
		
		Schema::table('fares', function(Blueprint $table) {
			$table->foreign('fare_type_id')->references('id')->on('fare_types')
						->onDelete('CASCADE')
						->onUpdate('CASCADE');
		});
		
		Schema::table('fares', function(Blueprint $table) {
			$table->foreign('route_id')->references('id')->on('routes')
						->onDelete('CASCADE')
						->onUpdate('CASCADE');
		});
		
		Schema::table('fares', function(Blueprint $table) {
			$table->foreign('from_terminal_id')->references('id')->on('terminals')
						->onDelete('CASCADE')
						->onUpdate('CASCADE');
		});
		
		Schema::table('fares', function(Blueprint $table) {
			$table->foreign('to_terminal_id')->references('id')->on('terminals')
						->onDelete('CASCADE')
						->onUpdate('CASCADE');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fares');
    }
}
