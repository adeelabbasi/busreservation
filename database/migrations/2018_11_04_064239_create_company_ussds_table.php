<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyUssdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_ussds', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('company_id')->unsigned();
            $table->string('code');
			$table->smallInteger('status');
            $table->timestamps();
        });
		
		Schema::table('company_ussds', function(Blueprint $table) {
			$table->foreign('company_id')->references('id')->on('companies')
						->onDelete('CASCADE')
						->onUpdate('CASCADE');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_ussds');
    }
}
