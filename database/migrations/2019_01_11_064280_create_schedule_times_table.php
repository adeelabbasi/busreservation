<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScheduleTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule_times', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned()->nullable();
			$table->integer('schedule_id')->unsigned()->nullable();
			$table->integer('route_id')->unsigned()->nullable();
			$table->integer('terminal_id')->unsigned()->nullable();
			$table->integer('sequence');
			$table->time('departure_time')->nullable();
			$table->time('arrival_time')->nullable();
			$table->timestamps();
        });
		
		Schema::table('schedule_times', function(Blueprint $table) {
			$table->foreign('company_id')->references('id')->on('companies')
						->onDelete('CASCADE')
						->onUpdate('CASCADE');
		});
		
		Schema::table('schedule_times', function(Blueprint $table) {
			$table->foreign('schedule_id')->references('id')->on('schedules')
						->onDelete('CASCADE')
						->onUpdate('CASCADE');
		});
		
		
		Schema::table('schedule_times', function(Blueprint $table) {
			$table->foreign('route_id')->references('id')->on('routes')
						->onDelete('CASCADE')
						->onUpdate('CASCADE');
		});
		
		Schema::table('schedule_times', function(Blueprint $table) {
			$table->foreign('terminal_id')->references('id')->on('terminals')
						->onDelete('CASCADE')
						->onUpdate('CASCADE');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedule_times');
    }
}
