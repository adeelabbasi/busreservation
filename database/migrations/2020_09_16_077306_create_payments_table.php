<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned()->nullable();
			$table->integer('booking_id')->unsigned()->nullable();
			$table->integer('bank_id')->unsigned()->nullable();
			$table->string('msisdn');
			$table->float('channel_fee');
			$table->float('gateway_fee');
			$table->float('amount');
			$table->string('paymentReference');
			$table->string('traceId');
			$table->string('customerRef');
			$table->smallInteger('status');
			$table->timestamps();
        });
		
		/*Schema::table('payments', function(Blueprint $table) {
			$table->foreign('company_id')->references('id')->on('companies')
						->onDelete('CASCADE')
						->onUpdate('CASCADE');
		});
		
		Schema::table('payments', function(Blueprint $table) {
			$table->foreign('bank_id')->references('id')->on('banks')
						->onDelete('CASCADE')
						->onUpdate('CASCADE');
		});
		
		Schema::table('payments', function(Blueprint $table) {
			$table->foreign('booking_id')->references('id')->on('bookings')
						->onDelete('CASCADE')
						->onUpdate('CASCADE');
		});*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
