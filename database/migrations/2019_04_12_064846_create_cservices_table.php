<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCservicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cservices', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('company_id')->unsigned();
			$table->string('name');
			$table->string('detail');
			$table->smallInteger('status');
			$table->timestamps();
        });
		
		Schema::table('cservices', function(Blueprint $table) {
			$table->foreign('company_id')->references('id')->on('companies')
						->onDelete('CASCADE')
						->onUpdate('CASCADE');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cservices');
    }
}
