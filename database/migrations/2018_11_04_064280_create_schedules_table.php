<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
			$table->string('short_name',10);
			$table->integer('route_id')->unsigned();
			$table->time('departure_time');
			$table->float('duration');
			$table->date('effective_Date');
			$table->date('operatingend_date');
			$table->smallInteger('monday');
			$table->smallInteger('tuesday');
			$table->smallInteger('wednesday');
			$table->smallInteger('thursday');
			$table->smallInteger('friday');
			$table->smallInteger('saturday');
			$table->smallInteger('sunday');
			$table->smallInteger('status');
			$table->timestamps();
        });
		
		Schema::table('schedules', function(Blueprint $table) {
			$table->foreign('company_id')->references('id')->on('companies')
						->onDelete('CASCADE')
						->onUpdate('CASCADE');
		});

		Schema::table('schedules', function(Blueprint $table) {
			$table->foreign('route_id')->references('id')->on('routes')
						->onDelete('CASCADE')
						->onUpdate('CASCADE');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
    }
}
