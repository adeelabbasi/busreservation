<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScheduleBusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule_buses', function (Blueprint $table) {
            $table->integer('schedule_id')->unsigned();
			$table->integer('bus_id')->unsigned();
        });
		
		Schema::table('schedule_buses', function(Blueprint $table) {
			$table->foreign('schedule_id')->references('id')->on('schedules')
						->onDelete('CASCADE')
						->onUpdate('CASCADE');
		});
		
		Schema::table('schedule_buses', function(Blueprint $table) {
			$table->foreign('bus_id')->references('id')->on('buses')
						->onDelete('CASCADE')
						->onUpdate('CASCADE');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedule_buses');
    }
}
