<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
			$table->string('short_name',10);
			$table->string('email');
			$table->smallInteger('type');
			$table->smallInteger('coverage');
			$table->integer('country_id')->unsigned();
            $table->text('description');
            $table->text('address');
			$table->smallInteger('local_govt');
			$table->string('website');
            $table->string('twitter');
			$table->string('ig');
			$table->string('linkedin');
            $table->string('logo')->default('NULL');
            $table->string('rc_no');
			$table->date('reg_date');
			$table->string('contact_person');
			$table->string('contact_person_no');
            $table->string('rtss_reg_no');
			$table->string('rtss_cert_code');
			$table->string('rtss_certification');
			$table->smallInteger('status');
			$table->smallInteger('ussd_type');
			$table->string('api_url');
			$table->string('api_token');
			$table->integer('charge_type');
			$table->integer('channel_fee_type');
			$table->integer('channel_fee');
			$table->integer('gateway_id');
			$table->integer('gateway_fee_type');
			$table->integer('gateway_fee');
			$table->integer('booking_no_of_days');
			$table->integer('seat_block_time');
			$table->integer('sequence')->default('999');
            $table->timestamps();
        });
		
		Schema::table('companies', function(Blueprint $table) {
			$table->foreign('country_id')->references('id')->on('countries')
						->onDelete('CASCADE')
						->onUpdate('CASCADE');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
