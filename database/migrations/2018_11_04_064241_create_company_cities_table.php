<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_cities', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('company_id')->unsigned();
			$table->integer('country_id')->unsigned();
			$table->integer('city_id')->unsigned();
            $table->timestamps();
        });
		
		Schema::table('company_cities', function(Blueprint $table) {
			$table->foreign('company_id')->references('id')->on('companies')
						->onDelete('CASCADE')
						->onUpdate('CASCADE');
		});
		
		Schema::table('company_cities', function(Blueprint $table) {
			$table->foreign('country_id')->references('id')->on('countries')
						->onDelete('CASCADE')
						->onUpdate('CASCADE');
		});
		
		Schema::table('company_cities', function(Blueprint $table) {
			$table->foreign('city_id')->references('id')->on('cities')
						->onDelete('CASCADE')
						->onUpdate('CASCADE');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_cities');
    }
}
