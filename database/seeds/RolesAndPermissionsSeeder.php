<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\Admin;
use App\Models\Country;
use App\Models\City;
use App\Models\Lga;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        // create permissions
        $permissions = [
			'Dashboard' ,
			'View Company',
			'Add Company',
			'Update Company',
			'Delete Company',
			'View Country',
			'Add Country',
			'Update Country',
			'Delete Country',
			'View State/City',
			'Add State/City',
			'Update State/City',
			'Delete State/City',
			'View LGA/Municipal',
			'Add LGA/Municipal',
			'Update LGA/Municipal',
			'Delete LGA/Municipal',
			'View Fleet',
			'Add Fleet',
			'Update Fleet',
			'Delete Fleet',
			'View Service',
			'Add Service',
			'Update Service',
			'Delete Service',
			'View Terminal',
			'Add Terminal',
			'Update Terminal',
			'Delete Terminal',
			'View Route',
			'Add Route',
			'Update Route',
			'Delete Route',
			'View Schedule',
			'Add Schedule',
			'Update Schedule',
			'Delete Schedule',
			'View User',
			'Add User',
			'Update User',
			'Delete User',
			'View User Role',
			'Add User Role',
			'Update User Role',
			'Delete User Role',
			'View User Activity',
			'View Customer',
			'Add Customer',
			'Update Customer',
			'Delete Customer',
			'View Customer Activity',
			'View Subscription',
			'Add Subscription',
			'Update Subscription',
			'Delete Subscription',
			'View Policy',
			'Add Policy',
			'Update Policy',
			'Delete Policy',
			'View Promotion/Deals',
			'Add Promotion/Deals',
			'Update Promotion/Deals',
			'Delete Promotion/Deals',
			'View Booking',
			'Add Booking',
			'Update Booking',
			'Delete Booking',
			'View Payment',
			'Add Payment',
			'Update Payment',
			'Delete Payment',
		];
		
		
		foreach ($permissions as $permission) {
			 Permission::create(['guard_name' => 'admin', 'name' => $permission]);
		}
        $roleAdmin = Role::create(['guard_name' => 'admin', 'name' => 'Admin']);
        $roleAdmin->givePermissionTo(Permission::where('guard_name','admin')->Get());

		$admin = Admin::create(['name' => 'Admin', 'username' => 'Admin', 'email' => 'admin@localhost.com', 'cpassword' => '123456', 'password' => '$2y$10$vvTR6ABhGHqtyePfi0ve3.gkBQf6RZvDeGKg1wRPXyDr2CeSdWQyq', 'avatar' => NULL, 'status' => '1']);
		$admin->assignRole($roleAdmin);
		
		
		
		//User
		// create permissions
        $permissions = [
			'Dashboard' ,
			'View Fleet',
			'Add Fleet',
			'Update Fleet',
			'Delete Fleet',
			'View Service',
			'Add Service',
			'Update Service',
			'Delete Service',
			'View Terminal',
			'Add Terminal',
			'Update Terminal',
			'Delete Terminal',
			'View Route',
			'Add Route',
			'Update Route',
			'Delete Route',
			'View Schedule',
			'Add Schedule',
			'Update Schedule',
			'Delete Schedule',
			'View Fare',
			'Add Fare',
			'Update Fare',
			'Delete Fare',
			'View User',
			'Add User',
			'Update User',
			'Delete User',
			'View User Role',
			'Add User Role',
			'Update User Role',
			'Delete User Role',
			'View User Activity',
			'View Customer',
			'Add Customer',
			'Update Customer',
			'Delete Customer',
			'View Customer Activity',
			'View Subscription',
			'Add Subscription',
			'Update Subscription',
			'Delete Subscription',
			'View Policy',
			'Add Policy',
			'Update Policy',
			'Delete Policy',
			'View Promotion/Deals',
			'Add Promotion/Deals',
			'Update Promotion/Deals',
			'Delete Promotion/Deals',
			'View Booking',
			'Add Booking',
			'Update Booking',
			'Delete Booking',
			'View Payment',
			'Add Payment',
			'Update Payment',
			'Delete Payment',
		];
		foreach ($permissions as $permission) {
			 Permission::create(['guard_name' => 'web', 'name' => $permission]);
		}
        
		
		$country = Country::create(['name' => 'Nigeria', 'short_name' => 'NIG', 'status' => '1']);
		
		$city = City::create(['name' => 'ABIA', 'short_name' => 'ABA', 'country_id' => $country->id, 'status' => '1']);
		
		$lga = Lga::create(['name' => 'ABIA', 'short_name' => 'AB', 'city_id' => $city->id, 'status' => '1']);
		$lga = Lga::create(['name' => 'UMUAHIA', 'short_name' => 'UM', 'city_id' => $city->id, 'status' => '1']);
		
		///////////////////////
		//Additional Permission
		///////////////////////
		/*INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(135, 'View Booking', 'admin', '2018-11-29 09:01:22', '2018-11-29 09:01:22'),
(136, 'Add Booking', 'admin', '2018-11-29 09:01:22', '2018-11-29 09:01:22'),
(137, 'Update Booking', 'admin', '2018-11-29 09:01:22', '2018-11-29 09:01:22'),
(138, 'Delete Booking', 'admin', '2018-11-29 09:01:22', '2018-11-29 09:01:22'),
(139, 'View Payment', 'admin', '2018-11-29 09:01:22', '2018-11-29 09:01:22'),
(140, 'Add Payment', 'admin', '2018-11-29 09:01:22', '2018-11-29 09:01:22'),
(141, 'Update Payment', 'admin', '2018-11-29 09:01:22', '2018-11-29 09:01:22'),
(142, 'Delete Payment', 'admin', '2018-11-29 09:01:22', '2018-11-29 09:01:22');


INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(143, 'View Booking', 'web', '2018-11-29 09:01:22', '2018-11-29 09:01:22'),
(144, 'Add Booking', 'web', '2018-11-29 09:01:22', '2018-11-29 09:01:22'),
(145, 'Update Booking', 'web', '2018-11-29 09:01:22', '2018-11-29 09:01:22'),
(146, 'Delete Booking', 'web', '2018-11-29 09:01:22', '2018-11-29 09:01:22'),
(147, 'View Payment', 'web', '2018-11-29 09:01:22', '2018-11-29 09:01:22'),
(148, 'Add Payment', 'web', '2018-11-29 09:01:22', '2018-11-29 09:01:22'),
(149, 'Update Payment', 'web', '2018-11-29 09:01:22', '2018-11-29 09:01:22'),
(150, 'Delete Payment', 'web', '2018-11-29 09:01:22', '2018-11-29 09:01:22');



INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(135, 1),
(136, 1),
(137, 1),
(138, 1),
(139, 1),
(140, 1),
(141, 1),
(142, 1);*/
    }
}
