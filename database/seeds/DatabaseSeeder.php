<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\Admin;
use App\Models\Country;
use App\Models\City;
use App\Models\Lga;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        // create permissions
        $permissions = [
			'Dashboard' ,
			'View Company',
			'Add Company',
			'Update Company',
			'Delete Company',
			'View Country',
			'Add Country',
			'Update Country',
			'Delete Country',
			'View State/City',
			'Add State/City',
			'Update State/City',
			'Delete State/City',
			'View LGA/Municipal',
			'Add LGA/Municipal',
			'Update LGA/Municipal',
			'Delete LGA/Municipal',
			'View Fleet',
			'Add Fleet',
			'Update Fleet',
			'Delete Fleet',
			'View Service',
			'Add Service',
			'Update Service',
			'Delete Service',
			'View Terminal',
			'Add Terminal',
			'Update Terminal',
			'Delete Terminal',
			'View Route',
			'Add Route',
			'Update Route',
			'Delete Route',
			'View Schedule',
			'Add Schedule',
			'Update Schedule',
			'Delete Schedule',
			'View Fare',
			'Add Fare',
			'Update Fare',
			'Delete Fare',
			'View User',
			'Add User',
			'Update User',
			'Delete User',
			'View User Role',
			'Add User Role',
			'Update User Role',
			'Delete User Role',
			'View User Activity',
			'View Customer',
			'Add Customer',
			'Update Customer',
			'Delete Customer',
			'View Customer Activity',
			'View Subscription',
			'Add Subscription',
			'Update Subscription',
			'Delete Subscription',
			'View Policy',
			'Add Policy',
			'Update Policy',
			'Delete Policy',
			'View Promotion/Deals',
			'Add Promotion/Deals',
			'Update Promotion/Deals',
			'Delete Promotion/Deals'
		];
		
		
		foreach ($permissions as $permission) {
			 Permission::create(['guard_name' => 'admin', 'name' => $permission]);
		}
        $roleAdmin = Role::create(['guard_name' => 'admin', 'name' => 'Admin']);
        $roleAdmin->givePermissionTo(Permission::where('guard_name','admin')->Get());
		
		foreach ($permissions as $permission) {
			 Permission::create(['guard_name' => 'web', 'name' => $permission]);
		}
        $role = Role::create(['guard_name' => 'web', 'name' => 'Admin']);
        $role->givePermissionTo(Permission::where('guard_name','web'));
		
		$admin = Admin::create(['name' => 'Admin', 'username' => 'Admin', 'email' => 'admin@localhost.com', 'cpassword' => '123456', 'password' => '$2y$10$vvTR6ABhGHqtyePfi0ve3.gkBQf6RZvDeGKg1wRPXyDr2CeSdWQyq', 'avatar' => NULL, 'status' => '1']);
		$admin->assignRole($roleAdmin);
		
		$country = Country::create(['name' => 'Nigeria', 'short_name' => 'NIG', 'status' => '1']);
		
		$city = City::create(['name' => 'ABIA', 'short_name' => 'ABA', 'country_id' => $country->id, 'status' => '1']);
		
		$lga = Lga::create(['name' => 'ABIA', 'short_name' => 'AB', 'city_id' => $city->id, 'status' => '1']);
		$lga = Lga::create(['name' => 'UMUAHIA', 'short_name' => 'UM', 'city_id' => $city->id, 'status' => '1']);
    }
}
