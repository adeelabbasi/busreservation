<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
		view()->composer('*', function($view){
			if(\Auth::guard('admin')->check())
			{
				$view->with('guard', 'admin');
				$view->with('guard_url', '/admin/');
			}
			else
			{
				$view->with('guard', 'web');
				$view->with('guard_url', '/company/');
			}
		});
		
		if (env('APP_ENV') === 'production') {
			\Illuminate\Support\Facades\URL::forceScheme('https');
		}
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
