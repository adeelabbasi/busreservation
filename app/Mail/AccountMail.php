<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AccountMail extends Mailable
{
    use Queueable, SerializesModels;
	public $info_Company;
	/**
     * The order instance.
     *
     * @var Order
     */

	
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($info_Company)
    {
        $this->info_Company = $info_Company;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('support@itravel.ng')->markdown('emails.account');
    }
}
