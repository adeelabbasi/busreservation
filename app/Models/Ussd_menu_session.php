<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/*
CREATE TABLE `ussd_menu_session` (
  `session_id` int(11) NOT NULL,
  `msisdn` varchar(20) NOT NULL,
  `ussd_code` varchar(20) NOT NULL,
  `user_level` int(11) NOT NULL,
  `keyword` varchar(10) NOT NULL,
  `parent_id` int(11) DEFAULT '0',
  `last_parent_id` int(11) DEFAULT '0',
  `g_parent` int(11) DEFAULT '0',
  `s_parent` int(11) DEFAULT '0',
  `c_parent` int(11) DEFAULT '0',
  `c_parent_id` int(11) DEFAULT '0',
  `c_last_parent_id` int(11) DEFAULT '0',
  `last_skip_level` int(11) DEFAULT '0',
  `is_child` char(1) DEFAULT '0',
  `page_no` int(11) DEFAULT '0',
  `per_page` int(11) DEFAULT '0',
  `more_keyword` int(11) DEFAULT '0',
  `from_city_id` int(11) DEFAULT NULL,
  `from_city_name` varchar(200) DEFAULT NULL,
  `from_terminal_id` int(11) DEFAULT NULL,
  `from_terminal_name` varchar(200) DEFAULT NULL,
  `to_city_id` int(11) DEFAULT NULL,
  `to_city_name` varchar(200) DEFAULT NULL,
  `to_terminal_id` int(11) DEFAULT NULL,
  `to_terminal_name` varchar(200) DEFAULT NULL,
  `schedule_date` date DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `company_name` varchar(200) DEFAULT NULL,
  `company_ussd_type` int(11) DEFAULT '1',
  `company_api_url` varchar(255) DEFAULT NULL,
  `company_api_token` varchar(255) DEFAULT NULL,
  `schedule_id` int(11) DEFAULT NULL,
  `fare_id` varchar(50) DEFAULT NULL,
  `is_delete` int(11) DEFAULT '0',
  `booking_msisdn` varchar(255) DEFAULT NULL,
  `no_of_traveler` varchar(255) DEFAULT NULL,
  `no_of_seats` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `nextkin` varchar(255) DEFAULT NULL,
  `nextkinnumber` varchar(255) DEFAULT NULL,
  `fare` int(11) DEFAULT '0',
  `tripid` varchar(255) DEFAULT NULL,
  `destid` varchar(255) DEFAULT NULL,
  `orderid` varchar(255) DEFAULT NULL,
  `dept_time` varchar(255) DEFAULT NULL,
  `bus` varchar(255) DEFAULT NULL,
  `available_seats` text DEFAULT NULL,
  `block_seats` text DEFAULT NULL,
  `level_1` varchar(50) DEFAULT '0',
  `level_2` varchar(50) DEFAULT '0',
  `level_3` varchar(50) DEFAULT '0',
  `level_4` varchar(50) DEFAULT '0',
  `level_5` varchar(50) DEFAULT '0',
  `level_6` varchar(50) DEFAULT '0',
  `level_7` varchar(50) DEFAULT '0',
  `level_8` varchar(50) DEFAULT '0',
  `level_9` varchar(50) DEFAULT '0',
  `level_10` varchar(50) DEFAULT '0',
  `level_11` varchar(50) DEFAULT NULL,
  `level_12` varchar(50) DEFAULT NULL,
  `level_13` varchar(50) DEFAULT NULL,
  `level_14` varchar(50) DEFAULT NULL,
  `level_15` varchar(50) DEFAULT NULL,
  `level_16` varchar(50) DEFAULT NULL,
  `channel_fee` double(8,2) DEFAULT NULL,
  `gateway_fee` double(8,2) DEFAULT NULL,
  `total_amount` double(8,2) DEFAULT NULL,
  `discount_code` varchar(255) DEFAULT NULL,
  `couponvalue` float DEFAULT NULL,
  `usePercent` float DEFAULT NULL,
  `couponpercent` float DEFAULT NULL,
  `record_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE ussd_menu_session ADD PRIMARY KEY (Msisdn);

CREATE TABLE `ussd_menu_schedule_search` (
  `msisdn` varchar(20) NOT NULL,
  `ussd_code` varchar(20) NOT NULL,
  `from_city_id` int(11) DEFAULT NULL,
  `from_city_name` varchar(200) DEFAULT NULL,
  `from_terminal_id` int(11) DEFAULT NULL,
  `from_terminal_name` varchar(200) DEFAULT NULL,
  `to_city_id` int(11) DEFAULT NULL,
  `to_city_name` varchar(200) DEFAULT NULL,
  `to_terminal_id` int(11) DEFAULT NULL,
  `to_terminal_name` varchar(200) DEFAULT NULL,
  `schedule_date` date DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `company_name` varchar(200) DEFAULT NULL,
  `schedule_id` int(11) DEFAULT NULL,
  `fare_id` varchar(50) DEFAULT NULL,
  `discount_code` varchar(255) DEFAULT NULL,
  `couponvalue` float DEFAULT NULL,
  `usePercent` float DEFAULT NULL,
  `couponpercent` float DEFAULT NULL,
  `record_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

*/

class Ussd_menu_session extends Model
{
	protected $table = 'ussd_menu_session';
	protected $primaryKey = 'msisdn';
	public $timestamps = false;
	
    protected $fillable = [
         'msisdn', 'ussd_code', 'user_level', 'keyword', 'parent_id', 'last_parent_id', 'g_parant', 'is_child', 'page_no', 'per_page', 'more_keyword', 'from_city_id', 'from_city_name', 'to_city_id', 'to_city_name', 'schedule_date', 'company_id', 'company_name', 'company_ussd_type', 'company_api_url', 'company_api_token', 'schedule_id', 'fare_id', 'record_on', 
    ];
	
	public function Company()
	{
		return $this->belongsTo('App\Models\Company' , 'company_id');
	}
}
