<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles; 

class User extends Authenticatable
{
    use Notifiable;
	use HasRoles;
	protected $guard_name = 'web';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'name', 'email', 'username', 'avatar', 'password', 'cpassword', 'type',
    ];
	
	/**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [ 
		'company_id' => 'required|numeric',
		'name' => 'required|string|max:255',
		'email' => 'required|email|max:255|unique:users',
		'username'  => 'required|alpha_dash|string|max:255|unique:users',
		'password' => 'required|string|min:6|confirmed',
		'type' => 'string|max:1',
		'role_id' => 'required|numeric',
    ];
	
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
	
	public function Company()
	{
		return $this->belongsTo('App\Models\Company' , 'company_id');
	}
	
	public function Userlog()
	{
		return $this->hasMany('App\Models\Userlog' , 'user_id');
	}
}
