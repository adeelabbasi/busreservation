<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Charging_log extends Model
{
	public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'msisdn', 'ussd_code', 'billing_date', 'amount', 'is_charge'
    ];
}
