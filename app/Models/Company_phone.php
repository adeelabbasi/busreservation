<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company_phone extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'company_id', 'number'
    ];
	
	/**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [ 
		'company_id' => 'required|numeric',
        'number' => 'required|string|max:100'
    ];
	
	public function Company()
	{
		return $this->belongsTo('App\Models\Company' , 'company_id');
	}
}