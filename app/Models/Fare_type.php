<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fare_type extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'company_id', 'schedule_id', 'service_id', 'name' 
    ];
	
	/**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [ 
		'name' => 'required|string|max:200', 
		'service_id' => 'required|numeric',

    ];
	
	public function Company()
	{
		return $this->belongsTo('App\Models\Company' , 'company_id');
	}
	
	public function Schedule()
	{
		return $this->belongsTo('App\Models\Schedule', 'schedule_id');
	}
	
	public function Fare()
	{
		return $this->hasMany('App\Models\Fare');
	}
	
	public function Service()
	{
		return $this->belongsTo('App\Models\Service' , 'service_id');
	}
}
