<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company_ussd extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'company_id', 'code', 'status'
    ];
	
	/**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [ 
		'company_id' => 'required|numeric',
        'code' => 'required|string|max:100',
		'status' => 'required|numeric'
    ];
	
	public function Company()
	{
		return $this->belongsTo('App\Models\Company' , 'company_id');
	}
}