<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fare extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'company_id', 'schedule_id', 'fare_type_id', 'route_id', 'bus_id', 'from_terminal_id', 'to_terminal_id', 'fare', 'fare_return'
    ];
	
	/**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [ 
		'company_id' => 'required|numeric',
		'bus_id' => 'required|numeric',
		'schedule_id' => 'required|numeric',
		'route_id' => 'required|numeric',
		'bus_id' => 'required|numeric',
		'from_terminal_id' => 'required|numeric',
		'to_terminal_id' => 'required|numeric',
		'fare' => 'required|numeric',
		'fare_return' => 'required|numeric',
    ];
	
	public function Company()
	{
		return $this->belongsTo('App\Models\Company' , 'company_id');
	}
	
	public function Schedule()
	{
		return $this->belongsTo('App\Models\Schedule', 'schedule_id');
	}
	
	public function Fare_type()
	{
		return $this->belongsTo('App\Models\Fare_type' , 'fare_type_id');
	}
	
	public function FromTerminal()
	{
		return $this->belongsTo('App\Models\Terminals' , 'from_terminal_id');
	}
	
	public function ToTerminal()
	{
		return $this->belongsTo('App\Models\Terminals' , 'to_terminal_id');
	}
	
	public function Bus()
	{
		return $this->belongsTo('App\Models\Bus' , 'bus_id');
	}
	
	public function Route()
	{
		return $this->belongsTo('App\Models\Route' , 'route_id');
	}
}
