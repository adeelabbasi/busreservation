<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer_log extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
		 'customer_id', 'msisdn', 'channel', 'is_complete', 'ussd_code', 'is_main', 'action', 'company_id','company_name', 'route_id', 'route_name', 'schedule_id', 'schedule_name', 'from_city_id', 'from_city_name', 'to_city_id', 'to_city_name', 'start_time', 'end_time'
    ]; 
	
}

