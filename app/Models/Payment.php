<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'company_id', 'booking_id', 'bank_id', 'msisdn', 'channel_fee', 'gateway_fee', 'amount', 'status', 'paymentReference', 'traceId', 'customerRef', 'payment_provider'
    ];
	
	public function Company()
	{
		return $this->belongsTo('App\Models\Company' , 'company_id');
	}
	
	public function Booking()
	{
		return $this->belongsTo('App\Models\Booking' , 'booking_id');
	}
	
	public function Bank()
	{
		return $this->belongsTo('App\Models\Bank' , 'bank_id');
	}
}
