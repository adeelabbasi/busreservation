<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'company_id', 'name', 'short_name', 'detail', 'status' 
    ];
	
	/**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [ 
		'company_id' => 'required|numeric',
		'name' => 'required|string|max:200', 
		'short_name' => 'required|string|max:10',
		'detail' => 'required|string|max:200', 
    ];
	
	public function Company()
	{
		return $this->belongsTo('App\Models\Company' , 'company_id');
	}
	
	public function Fare()
	{
		return $this->hasMany('App\Models\Fare' , 'fare_id');
	}
}
