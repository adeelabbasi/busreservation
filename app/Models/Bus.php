<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class bus extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'company_id', 'name', 'map', 'seats', 'status' 
    ];
	
	/**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [ 
		'company_id' => 'required|numeric',
		'name' => 'required|string|max:200', 
		'map' => 'required|string|max:200', 
		'seats' => 'required|integer', 
		'status' => 'required|numeric'
    ];
	
	public function Company()
	{
		return $this->belongsTo('App\Models\Company' , 'company_id');
	}
	
	public function Schedule()
	{
		return $this->belongsToMany('App\Models\Schedule', 'schedule_buses', 'schedule_id', 'bus_id');
	}
	public function Fare()
	{
		return $this->hasMany('App\Models\Fare' , 'bus_id');
	}
}

