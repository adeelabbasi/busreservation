<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Model
{
	use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'name', 'short_name', 'status'
    ];
	/**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
	/**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [ 
        'name' => 'required|string|max:100',
        'short_name' => 'required|string|max:10',
    ];
	
	public function Company()
	{
		return $this->hasMany('App\Models\Company' , 'country_id');
	}
	
	public function State()
	{
		return $this->hasMany('App\Models\State' , 'country_id');
	}
	
	public function Company_city()
	{
		return $this->hasMany('App\Models\State' , 'country_id');
	}
}