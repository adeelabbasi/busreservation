<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company_city extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'company_id', 'country_id', 'city_id', 'lga_id'
    ];
	
	/**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [ 
		'company_id' => 'required|numeric',
        'country_id' => 'required|numeric',
		'city_id' => 'required|numeric',
    ];
	
	public function Company()
	{
		return $this->belongsTo('App\Models\Company' , 'company_id');
	}
	
	public function Country()
	{
		return $this->belongsTo('App\Models\Country' , 'country_id');
	}
	
	public function City()
	{
		return $this->belongsTo('App\Models\City' , 'city_id');
	}
}