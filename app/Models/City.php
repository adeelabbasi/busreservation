<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
	use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'country_id', 'name', 'short_name', 'status', 'sequence'
    ];
	/**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
	/**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [ 
		'country_id' => 'required|numeric',
        'name' => 'required|string|max:100',
        'short_name' => 'required|string|max:10',
    ];
	
	
	public function Country()
	{
		return $this->belongsTo('App\Models\Country' , 'country_id');
	}


	public function Lga()
	{
		return $this->hasMany('App\Models\Lga' , 'state_id');
	}
	
	public function Company_city()
	{
		return $this->hasMany('App\Models\Company_city' , 'city_id');
	}
}