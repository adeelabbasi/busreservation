<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'name', 'short_name', 'email', 'coverage', 'type', 'country_id', 'description', 'address', 'local_govt', 'website', 'whatsapp', 'twitter', 'ig', 'linkedin', 'logo', 'rc_no', 'reg_date', 'contact_person', 'contact_person_no', 'rtss_reg_no', 'rtss_cert_code', 'rtss_certification', 'status', 'sequence', 'ussd_type', 'api_url', 'api_token', 'charge_type', 'channel_fee_type', 'channel_fee', 'gateway_id', 'gateway_fee_type', 'gateway_fee', 'booking_no_of_days', 'seat_block_time', 'minimum_fee'
    ];
	
	/**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [ 
		'email' => 'required|string|email|max:255',
		'name' => 'required|string|max:200|unique:companies', 
		'short_name' => 'required|string|max:10', 
		'type' => 'required|numeric',
		'coverage' => 'required|numeric',
		'country_id' => 'required|numeric',
		'description' => 'required|string|max:1000', 
		'address' => 'required|string', 
		'local_govt' => 'required|numeric', 
		'website' => 'required|string|max:100', 
		'twitter' => 'required|string|max:100', 
		'ig' => 'required|string|max:100', 
		'linkedin' => 'required|string|max:100', 
		'logo' => 'image|mimes:jpg,png,jpeg|min:1|max:2048',
		'rc_no' => 'required|string|max:100', 
		'reg_date' => 'required|date', 
		'contact_person' => 'required|string|max:100', 
		'contact_person_no' => 'required|string|max:100', 
		'rtss_reg_no' => 'required|string|max:100', 
		'rtss_cert_code' => 'required|string|max:100', 
		'rtss_certification' => 'required|string|max:100', 
		'whatsapp.*' => 'required|string|max:50',
		'phone_number.*' => 'required|string|max:50', 
    ];
	
	public function Country()
	{
		return $this->belongsTo('App\Models\Country' , 'country_id');
	}
	
	public function User()
	{
		return $this->hasMany('App\Models\User' , 'company_id');
	}
	
	public function Company_city()
	{
		return $this->hasMany('App\Models\Company_city' , 'company_id');
	}
	
	public function Company_phone()
	{
		return $this->hasMany('App\Models\Company_phone' , 'company_id');
	}
	
	public function Company_ussd()
	{
		return $this->hasMany('App\Models\Company_ussd' , 'company_id');
	}
	
	public function Company_whatsapp()
	{
		return $this->hasMany('App\Models\Company_whatsapp' , 'company_id');
	}
	
	public function Deal()
	{
		return $this->hasMany('App\Models\Deal' , 'company_id');
	}
	
	public function Policy()
	{
		return $this->hasMany('App\Models\Policy' , 'company_id');
	}
	
	public function Service()
	{
		return $this->hasMany('App\Models\Service' , 'company_id');
	}
	
	public function Bus()
	{
		return $this->hasMany('App\Models\Bus' , 'company_id');
	}
	
	public function Route()
	{
		return $this->hasMany('App\Models\Route' , 'company_id');
	}
	
	public function Schedule()
	{
		return $this->hasMany('App\Models\Schedule' , 'company_id');
	}
	
	public function Fare_type()
	{
		return $this->hasMany('App\Models\Fare_type' , 'company_id');
	}
	
	public function Fare()
	{
		return $this->hasMany('App\Models\Fare' , 'company_id');
	}
	
	public function Booking()
	{
		return $this->hasMany('App\Models\Booking' , 'company_id');
	}
}
