<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Route_detail extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'route_id', 'terminal_id', 'sequence' 
    ];
	
	/**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [ 
		'route_id' => 'required|numeric',
		'terminal_id' => 'required|string|max:200', 
    ];
	
	public function Route()
	{
		return $this->belongsTo('App\Models\Route' , 'route_id');
	}
		
	public function Terminal()
	{
		return $this->belongsTo('App\Models\Terminal' , 'terminal_id');
	}
	
	public function Fare()
	{
		return $this->hasMany('App\Models\Fare' , 'route_id');
	}
}

