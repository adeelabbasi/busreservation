<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Schedule_time extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'company_id', 'schedule_id', 'route_id', 'terminal_id', 'sequence', 'departure_time', 'arrival_time'
    ];
	
	/**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [ 
		'company_id' => 'required|numeric',
		'bus_id' => 'required|numeric',
		'schedule_id' => 'required|numeric',
		'route_id' => 'required|numeric',
		'terminal_id' => 'required|numeric',
		'departure_time' => 'required|numeric',
		'duration' => 'required|numeric',
    ];
	
	public function Company()
	{
		return $this->belongsTo('App\Models\Company' , 'company_id');
	}
	
	public function Schedule()
	{
		return $this->belongsTo('App\Models\Schedule', 'schedule_id');
	}
	
	public function Terminal()
	{
		return $this->belongsTo('App\Models\Terminal' , 'terminal_id');
	}
	
	public function Route()
	{
		return $this->belongsTo('App\Models\Route' , 'route_id');
	}
}
