<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Terminal extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'company_id', 'lga_id', 'name', 'short_name', 'address', 'email', 'phone_number', 'status', 'sequence'
    ];
	
	/**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [ 
		'company_id' => 'required|numeric',
		'lga_id' => 'required|numeric',
		'name' => 'required|string|max:200', 
		'short_name' => 'required|string|max:10',
		'address' => 'required|string|max:1000', 
		'email' => 'required|string|max:200', 
		'phone_number' => 'required|string|max:200', 
    ];
	
	public function Company()
	{
		return $this->belongsTo('App\Models\Company' , 'company_id');
	}
	
	public function Lga()
	{
		return $this->belongsTo('App\Models\Lga' , 'lga_id');
	}
	
	public function Route_detail()
	{
		return $this->hasMany('App\Models\Route_detail' , 'terminal_id');
	}
	
	public function FromFare()
	{
		return $this->hasMany('App\Models\Fare' , 'from_terminal_id');
	}
	
	public function ToFare()
	{
		return $this->hasMany('App\Models\Fare' , 'to_terminal_id');
	}
	
	public function Schedule()
	{
		return $this->hasMany('App\Models\Schedule' , 'from_terminal_id');
	}
}
