<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'company_id', 'customer_id', 'schedule_id', 'refcode', 'msisdn', 'TripID', 'SelectedSeats', 'MaxSeat', 'DestinationID', 'OrderID', 'Fullname', 'email', 'nextKin', 'nextKinPhone', 'Sex', 'sub_total', 'tax', 'charge_type', 'channel_fee', 'gateway_fee', 'total', 'traceId', 'from_city_id', 'from_city_name', 'from_terminal_id', 'from_terminal_name', 'to_city_id', 'to_city_name', 'to_terminal_id', 'to_terminal_name', 'schedule_date', 'dept_time', 'bus', 'gateway', 'status'
    ];
	
	/**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [ 
		
    ];
	
	public function Company()
	{
		return $this->belongsTo('App\Models\Company' , 'company_id');
	}
	
	public function Customer()
	{
		return $this->belongsTo('App\Models\Customer' , 'customer_id');
	}
	
	public function Schedule()
	{
		return $this->belongsTo('App\Models\Schedule' , 'schedule_id');
	}
}

