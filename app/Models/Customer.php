<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'title', 'first_name', 'last_name', 'other_name', 'address', 'martial_status', 'gender', 'work_status', 'mprs_gsm_no', 'gsm_no_2', 'gsm_no_3', 'email', 'pin', 'kin_fullname', 'kin_phone', 'kin_address', 'status',
    ];
	
	/**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [ 
		'title' => 'required|string|max:200', 
		'first_name' => 'required|string|max:200', 
		'last_name' => 'required|string|max:200',  
		'other_name' => 'required|string|max:200',  
		'address' => 'required|string',  
		'martial_status' => 'required|numeric', 
		'gender' => 'required|numeric', 
		'work_status' => 'required|numeric',
		'mprs_gsm_no' => 'required|string|max:200', 
		'gsm_no_2' => 'required|string|max:200',  
		'gsm_no_3' => 'required|string|max:200',  
		'email' => 'required|string|max:200',  
		'pin' => 'string|max:200',  
		'kin_fullname' => 'required|string|max:200',  
		'kin_phone' => 'required|string|max:200',  
		'kin_address' => 'required|string',  
    ];
	
	public function Booking()
	{
		return $this->hasMany('App\Models\Booking' , 'customer_id');
	}
}

