<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'company_id', 'name', 'short_name', 'status' 
    ];
	
	/**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [ 
		'company_id' => 'required|numeric',
		'name' => 'required|string|max:200', 
		'short_name' => 'required|string|max:10',
    ];
	
	public function Company()
	{
		return $this->belongsTo('App\Models\Company' , 'company_id');
	}
	
	public function Route_detail()
	{
		return $this->hasMany('App\Models\Route_detail' , 'route_id');
	}
	
	public function Schedule()
	{
		return $this->hasMany('App\Models\Schedule' , 'route_id');
	}
	
	public function Fare()
	{
		return $this->hasMany('App\Models\Fare' , 'route_id');
	}
}

