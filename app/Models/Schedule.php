<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'company_id', 'short_name', 'route_id', 'departure_time', 'duration', 'effective_Date', 'operatingend_date', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday', 'status' 
    ];
	
	/**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [  		
		'company_id' => 'required|numeric', 
		'short_name' => 'required|string|max:10',
		'route_id' => 'required|numeric', 
		'bus_ids' => 'required|array',
		'effective_Date' => 'required|date_format:Y-m-d', 
		'monday' => 'numeric',
		'tuesday' => 'numeric', 
		'wednesday' => 'numeric', 
		'thursday' => 'numeric', 
		'friday' => 'numeric', 
		'saturday' => 'numeric',
		'sunday' => 'numeric', 
		
    ];
	
	public function Company()
	{
		return $this->belongsTo('App\Models\Company' , 'company_id');
	}
	
	public function Bus()
	{
		return $this->belongsToMany('App\Models\Bus', 'schedule_buses', 'schedule_id', 'bus_id');
	}
	
	public function Route()
	{
		return $this->belongsTo('App\Models\Route' , 'route_id');
	}
	
	public function FromTerminal()
	{
		return $this->belongsTo('App\Models\Terminal' , 'from_terminal_id');
	}
	
	public function Fare()
	{
		return $this->hasMany('App\Models\Fare' , 'schedule_id');
	}
	
	public function Fare_type()
	{
		return $this->hasMany('App\Models\Fare_type' , 'schedule_id');
	}
	
	public function Booking()
	{
		return $this->hasMany('App\Models\Booking' , 'schedule_id');
	}
	
	public function Schedule_time()
	{
		return $this->hasMany('App\Models\Schedule_time');
	}
}

