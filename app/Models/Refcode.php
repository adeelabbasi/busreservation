<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Refcode extends Model
{
    public $timestamps = false;
	
    protected $fillable = [
         'code'
    ];
}
