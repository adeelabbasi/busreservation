<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApiCache extends Model
{
    //Table name
    protected $table = 'api_cache';
    //primary key
    public $primarykey = 'id';
    //timestamps
    public $timestamps = false;

}
