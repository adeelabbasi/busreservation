<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Userlog extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'user_id', 'admin_id', 'action' 
    ];
	
	/**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [ 
		'user_id' => 'required|numeric',
		'admin_id' => 'required|numeric',
		'action' => 'required|string|max:200'
    ];
	
	public function User()
	{
		return $this->belongsTo('App\Models\User' , 'user_id');
	}
	
	public function Admin()
	{
		return $this->belongsTo('App\Models\Admin' , 'admin_id');
	}
}
