<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lga extends Model
{
	use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'city_id', 'name', 'short_name', 'status'
    ];
	/**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
	/**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [ 
		'city_id' => 'required|numeric',
        'name' => 'required|string|max:100',
        'short_name' => 'required|string|max:10',
    ];
	
	public function City()
	{
		return $this->belongsTo('App\Models\City' , 'city_id');
	}
	
	public function Terminal()
	{
		return $this->hasMany('App\Models\Terminal' , 'lga_id');
	}
}
