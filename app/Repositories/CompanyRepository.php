<?php

namespace App\Repositories;

use App\Models\Company;
use App\Models\User;
use App\Models\Company_phone;
use App\Models\Company_ussd;
use App\Models\Company_whatsapp;
use Illuminate\Support\Facades\Input;
use App\Repositories\ImageRepository;
use DB;
use App\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;
use App\Mail\AccountMail;
use Illuminate\Support\Facades\Mail;

class CompanyRepository {
	
	/**
	 * @var App\Models\Company
	 */
	protected $db_company;
	protected $db_company_phone;
	protected $db_company_whatsapp;
	protected $db_company_ussd;
	protected $db_user;
	protected $imageRps;
	
	protected $uploadAvatar = 'media/avatar/';
	protected $uploadLogo = 'media/logo/';
		
    public function __construct(Company $db_company, ImageRepository $imageRps, Company_phone $db_company_phone, Company_ussd $db_company_ussd, Company_whatsapp $db_company_whatsapp, User $db_user)  
    {
        $this->db_company = $db_company;
		$this->imageRps = $imageRps;
		$this->db_company_phone = $db_company_phone;
		$this->db_company_whatsapp = $db_company_whatsapp;
		$this->db_company_ussd = $db_company_ussd;
		$this->db_user = $db_user;
    }
	
	public function addCompany($inputs)
    {
        $db_company = $this->storeCompany(new $this->db_company ,  $inputs);
        return $db_company;
    }
	
	public function updateCompany($inputs, $id)
	{
		$db_company = $this->db_company->findOrFail($id);
		$answer_id = $this->storeCompany($db_company, $inputs, $id);
		return $answer_id;
	}
	
	public function deleteCompany($id)
    {
		$ResultDel = DB::delete("delete from userlogs where user_id in (select id from users where company_id = '$id')");
		$ResultDel = DB::delete("delete from users where company_id = '$id'");
		$ResultDel = DB::delete("delete from schedule_times where company_id = '$id'");
		$ResultDel = DB::delete("delete from fare_types where company_id = '$id'");
		$ResultDel = DB::delete("delete from fares where company_id = '$id'");
		$ResultDel = DB::delete("delete from schedule_buses where schedule_id in (select id from schedules where company_id = '$id')");
		$ResultDel = DB::delete("delete from schedules where company_id = '$id'");
		$ResultDel = DB::delete("delete from route_details where route_id in (select id from routes where company_id = '$id')");
		$ResultDel = DB::delete("delete from routes where company_id = '$id'");
		$ResultDel = DB::delete("delete from terminals where company_id = '$id'");
		$ResultDel = DB::delete("delete from buses where company_id = '$id'");
		$ResultDel = DB::delete("delete from services where company_id = '$id'");
		$ResultDel = DB::delete("delete from policies where company_id = '$id'");
		$ResultDel = DB::delete("delete from deals where company_id = '$id'");
		$ResultDel = DB::delete("delete from bookings where company_id = '$id'");
		$ResultDel = DB::delete("delete from company_whatsapps where company_id = '$id'");
		$ResultDel = DB::delete("delete from company_ussds where company_id = '$id'");
		$ResultDel = DB::delete("delete from company_phones where company_id = '$id'");
		
		$db_company = $this->db_company->findOrFail($id);
        $db_company->delete();
        return true;
    }
	
	
	public function updateSequence($id, $sequence)
    {
		$db_company = $this->db_company->findOrFail($id);
        $db_company->sequence = $sequence;
		$db_company->save();
        return true;
    }

	function storeCompany($db_company , $inputs, $id = null)
	{	
		if ($id) 
		{
			$db_company->id = $id;
			
			//Delete & update
			if(isset($inputs['logo']))
			{
				$this->imageRps->delete($db_company->logo, $this->uploadLogo);
				$db_company->logo  = $this->imageRps->upload($inputs['logo'], $id, $this->uploadLogo, '300', '300');
			}
		}
		else
		{
			//Add logo
			if(isset($inputs['logo']))
			{
				$db_company->logo = $this->imageRps->upload($inputs['logo'], $id, $this->uploadLogo, '300', '300');
			}
		}

		$db_company->name = $inputs['name'];
		$db_company->email = $inputs['email'];
		$db_company->type = $inputs['type'];
		$db_company->coverage = $inputs['coverage'];
		$db_company->country_id = $inputs['country_id'];
		$db_company->short_name = $inputs['short_name'];
		$db_company->description = $inputs['description'];
		$db_company->address = $inputs['address'];
		$db_company->local_govt = $inputs['local_govt'];
		$db_company->website = $inputs['website'];
		$db_company->twitter = $inputs['twitter'];
		$db_company->ig = $inputs['ig'];
		$db_company->linkedin = $inputs['linkedin'];
		$db_company->rc_no = $inputs['rc_no'];
		$db_company->reg_date = $inputs['reg_date'];
		$db_company->contact_person = $inputs['contact_person'];
		$db_company->contact_person_no = $inputs['contact_person_no'];
		$db_company->rtss_reg_no = $inputs['rtss_reg_no'];
		$db_company->rtss_cert_code = $inputs['rtss_cert_code'];
		$db_company->rtss_certification = $inputs['rtss_certification'];
		$db_company->ussd_type = $inputs['ussd_type'];
		
		$db_company->api_url = $inputs['api_url'];
		$db_company->api_token = $inputs['api_token'];
		$db_company->charge_type = $inputs['charge_type'];
		$db_company->channel_fee_type = $inputs['channel_fee_type'];
		$db_company->channel_fee = $inputs['channel_fee'];
		$db_company->gateway_id = $inputs['gateway_id'];
		$db_company->gateway_fee_type = $inputs['gateway_fee_type'];
		$db_company->gateway_fee = $inputs['gateway_fee'];
		$db_company->booking_no_of_days = $inputs['booking_no_of_days'];
		$db_company->seat_block_time = $inputs['seat_block_time'];
		$db_company->minimum_fee = $inputs['minimum_fee'];
		//dd($db_company);
		//Set status
		if(isset($inputs['status'])=="on")
			$db_company->status = 1;
		else
			$db_company->status = 0;
		
		$db_company->save();
		
		//Add/Update phone number
		if(isset($inputs['phone_number']))
		{
			$this->db_company_phone->where('company_id','=',$db_company->id)->delete();
			foreach($inputs['phone_number'] as $phone_number)
			{
				$db_company_phone_new = new $this->db_company_phone;
				$db_company_phone_new->company_id = $db_company->id;
				$db_company_phone_new->number = $phone_number;
				$db_company_phone_new->save();
			}
		}
		
		//Add/Update whatsapp
		if(isset($inputs['whatsapp']))
		{
			$this->db_company_whatsapp->where('company_id','=',$db_company->id)->delete();
			foreach($inputs['whatsapp'] as $whatsapp)
			{
				$db_company_whatsapp_new = new $this->db_company_whatsapp;
				$db_company_whatsapp_new->company_id = $db_company->id;
				$db_company_whatsapp_new->number = $whatsapp;
				$db_company_whatsapp_new->save();
			}
		}
		
		
		//Add/Update ussd
		$this->db_company_ussd->where('company_id','=',$db_company->id)->delete();
		$db_company_ussd_new = new $this->db_company_ussd;
		$db_company_ussd_new->company_id = $db_company->id;
		$db_company_ussd_new->code = $inputs['ussd_code'];
		$db_company_ussd_new->status = 1;
		$db_company_ussd_new->save();
		
		if ($id==null)
		{
			$role = Role::create(['company_id' => $db_company->id, 'guard_name' => 'web', 'name' => 'Admin']);
			$role->givePermissionTo(Permission::where('guard_name','web')->Get());
			//$info_Company = $db_company;
			//$info_Company->ussd_code = $inputs['ussd_code'];
			//Mail::to($db_company)->send(new AccountMail($info_Company));
		}
		
	}
	
	public function getCompany($id = null)
    {
		if($id==null)
		{
			$info_Company = $this->db_company->select('id', 'name', 'type', 'coverage', 'short_name', 'email', 'coverage', 'country_id', 'description', 'address', 'local_govt', 'website', 'twitter', 'ig', 'linkedin', 'logo', 'rc_no', 'reg_date', 'contact_person', 'contact_person_no', 'rtss_reg_no', 'rtss_cert_code', 'rtss_certification', 'status', 'sequence', 'ussd_type', 'api_url', 'api_token', 'charge_type', 'channel_fee_type', 'channel_fee', 'gateway_id', 'gateway_fee_type', 'gateway_fee', 'booking_no_of_days', 'seat_block_time', 'minimum_fee', 'created_at', 'updated_at')->Where('status',1)->orderBy('name', 'ASC')->get();
		}
		else
		{
			$info_Company = $this->db_company->select('id', 'name', 'type', 'coverage', 'short_name', 'email', 'coverage', 'country_id', 'description', 'address', 'local_govt', 'website', 'twitter', 'ig', 'linkedin', 'logo', 'rc_no', 'reg_date', 'contact_person', 'contact_person_no', 'rtss_reg_no', 'rtss_cert_code', 'rtss_certification', 'status', 'sequence', 'ussd_type', 'api_url', 'api_token', 'charge_type', 'channel_fee_type', 'channel_fee', 'gateway_id', 'gateway_fee_type', 'gateway_fee', 'booking_no_of_days', 'seat_block_time', 'minimum_fee', 'created_at', 'updated_at')->findOrFail($id);
		}
        return $info_Company;
    }
	
	public function getAllCompanies()
    {
		$info_Company = $this->db_company->select('id', 'name', 'type', 'coverage', 'short_name', 'email', 'coverage', 'country_id', 'description', 'address', 'local_govt', 'website', 'twitter', 'ig', 'linkedin', 'logo', 'rc_no', 'reg_date', 'contact_person', 'contact_person_no', 'rtss_reg_no', 'rtss_cert_code', 'rtss_certification', 'status', 'sequence', 'ussd_type', 'api_url', 'api_token', 'charge_type', 'channel_fee_type', 'channel_fee', 'gateway_id', 'gateway_fee_type', 'gateway_fee', 'booking_no_of_days', 'seat_block_time', 'minimum_fee', 'created_at', 'updated_at')->orderBy('created_at', 'DESC')->get();
		
        return $info_Company;
    }
}

