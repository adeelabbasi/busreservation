<?php

namespace App\Repositories;
use App\Models\City;
use App\Models\Company;
use App\Models\Company_ussd;
use App\Models\Schedule;
use App\Models\Fare;
use App\Models\Terminal;
use App\Models\Subscription;
use App\Models\Charging_log;
use App\Models\Customer_log;
use App\Models\Booking;
use App\Models\Refcode;
use Carbon\Carbon;
use DateTime;
use DB;
use App\Jobs\CallBookingAPI;
use App\Jobs\FinalBookNewMessage;


class UssdRepository { 
 	
	//88111 
	//7433
	public function Level1($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$info_Company, $testing)
	{
		//****************************************
		// Start Transporters Level 1
		//****************************************
		$output['session_operation'] = "continue";
		$output['session_msg'] = "Thanks for Choosing\n".$info_Company->name."\nSelect\n1. Book Now\n2. My Bookings\n3. Terminals\n4. Contact Us"; 
		
		$session_id = $this->addCustomerLog($testing, $session_msisdn, $session_msg, 0, $info_Company->id, $info_Company->name);
		
		$ResultDel = DB::delete("delete from ussd_menu_session where msisdn = '$session_msisdn'");
		$ResultIns = DB::insert("INSERT INTO ussd_menu_session (session_id, msisdn, ussd_code, user_level, keyword, parent_id, g_parent, s_parent, is_child, c_parent_id, company_id, company_name, company_ussd_type, company_api_url, company_api_token, level_4, record_on) VALUES ('$session_id', '$session_msisdn', '$session_msg', '1', '$session_msg', '1', '1', '1', '1', '1' ,'".$info_Company->id."','".$info_Company->name."', '".$info_Company->ussd_type."', '".$info_Company->api_url."', '".$info_Company->api_token."', '".$info_Company->id."', CURRENT_TIMESTAMP)");		
		//****************************************
		// End Transporters
		//****************************************
		return $output;
	}
	
	public function Level2($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing)
	{
		if(!$moreKeyword)
			$userSession->level_2=$session_msg;
		//Uncomment echo "Menu 5";
		
		//Change page no on level change, check if level is same else change page no
		if($userSession->user_level != 2)
		{
			$userSession->page_no = 0;
		}
		
		$userSession->c_parent=$session_msg;

		//Transporters
		//****************************************
		// Start Transporters Level 2
		//****************************************
		//Set company menu parent 
		switch($session_msg)
		{
			case 1: //Transporters > Schedule > From Cities
				if($moreKeyword)
				{
					$userSession->page_no = $userSession->page_no+7;
				}
				else
				{
					$userSession->c_last_parent_id=$userSession->c_parent_id;
				}
				
				$output['session_operation'] = "continue";
				$per_page = 7;
				
				$info_Cities = $this->GetCompanyCities($userSession, $userSession->page_no);
				
				$menuCity = "";
				foreach($info_Cities as $City)
				{
					$menuCity .= $City->rownum.". ".ucfirst(strtolower($City->name))."\n";
				}
				
				$info_Cities = $this->GetCompanyCities($userSession, $userSession->page_no + $per_page);
				//dd($info_Cities);
				$userSession->more_keyword=0;
				if($info_Cities)
				{
					$menuCity .= ($userSession->page_no+8).". More\n";
					$userSession->more_keyword=($userSession->page_no+8);
				}
				$output['session_msg'] = "From".chr(58)."\n".$menuCity."* Back";
				$userSession->per_page=$per_page;
				$userSession->user_level=2;
				$userSession->keyword=$session_msg;
				$userSession->c_parent_id=$session_msg;
				$userSession->is_child=1;
				$userSession->save();
			break;
			case 2: //Transporters > Bookings > Status/New
				if($moreKeyword)
				{
					$userSession->page_no = $userSession->page_no+4;
				}
				else
				{
					$userSession->c_last_parent_id=$userSession->c_parent_id;
				}
				
				$output['session_operation'] = "continue";
				$output['session_msg'] = "Select".chr(58)."\n1. New Booking\n2. Booking Status\n* Back";
				$userSession->per_page=4;
				$userSession->user_level=2;
				$userSession->keyword=$session_msg;
				$userSession->c_parent_id=$session_msg;
				$userSession->is_child=1;
				$userSession->save();
				
				if($userSession->ussd_code=="*".$session_from)
				{
					$output['session_msg'] .= "\n"."* Back";
				}
				
			break;
			case 3: //Transporters > Terminals > Terminals Cities
				if($moreKeyword)
				{
					$userSession->page_no = $userSession->page_no+7;
				}
				else
				{
					$userSession->c_last_parent_id=$userSession->c_parent_id;
				}
				
				$per_page = 7;
				
				$output['session_operation'] = "continue";
				$info_Cities = $this->GetCompanyCities($userSession, $userSession->page_no);
				
				$menuCity = "";
				foreach($info_Cities as $City)
				{
					//dd($City);
					$menuCity .= $City->rownum.". ".ucfirst(strtolower($City->name))."\n";
				}
				
				$info_Cities = $this->GetCompanyCities($userSession, $userSession->page_no+$per_page);
				
				$userSession->more_keyword=0;
				if($info_Cities)
				{
					$menuCity .= ($userSession->page_no+8).". More\n";
					$userSession->more_keyword=($userSession->page_no+8);
				}
				
				//dd($menuCity);
				$output['session_msg'] = "Terminal Location".chr(58)."\n".$menuCity."* Back";
				$userSession->per_page=$per_page;
				$userSession->user_level=2;
				$userSession->keyword=$session_msg;
				$userSession->c_parent_id=$session_msg;
				$userSession->is_child=1;
				$userSession->save();
				
			break;
			case 4: //Transporters > Services > Services Cities Change to contact US
				$this->updateCustomerLog($testing, "update customer_logs set is_complete=1, action='Contact US', company_id='".$userSession->company_id."', company_name='".$userSession->company_name."' where id=".$userSession->session_id);
				
				$info_Company = $userSession->Company()->First();
				$output['session_operation'] = "endcontinue";		
				
				$phone="";
				foreach($info_Company->Company_phone()->Get() as $Company_phone)
				{
					$phone = $phone.$Company_phone->number.",";
				}
				$phone = substr($phone,0,-1);
				
				$whatsapp="";
				foreach($info_Company->Company_whatsapp()->Get() as $Company_whatsapp)
				{
					$whatsapp = $whatsapp.$Company_whatsapp->number.",";
				}

				$smsMessage = "Thanks for Choosing\n".$info_Company->name."\nYou can reach us on ".$phone." Whatsapp line: ".$whatsapp." or visit our website: ".$info_Company->website; 
				
				$this->sendSMS($session_from,$session_msisdn, $smsMessage, $gateway);
				$output['session_msg'] = "Thanks, your request has being received. You will receive SMS response shortly";
				
			break;
		}
		//****************************************
		// End Transporters
		return $output;
	}
	
	public function Level3($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing)
	{
		if(!$moreKeyword)
			$userSession->level_3=$session_msg;
		//Uncomment echo "Menu 3";
		
		//Change page no on level change, check if level is same else change page no
		if($userSession->user_level != 3)
		{
			$userSession->page_no = 0;
		}
		//dd($userSession->c_parent);
		//****************************************
		// Start Transporters Level 3
		//****************************************
		switch($userSession->c_parent)
		{
			case 1: //Transporters > Schedule > To Cities
				
				$correctDate = 0;
				if($moreKeyword)
				{
					$userSession->page_no = $userSession->page_no+7;
					
					$info_FromCity = $this->GetSelectedCompanyCity($userSession,$session_msg, $userSession->from_city_id);
				}
				else
				{
					$userSession->c_last_parent_id=$userSession->c_parent_id;
					$info_FromCity = $this->GetSelectedCompanyCity($userSession,$session_msg);
				}
				
				$output['session_operation'] = "continue";
				
				
				$info_Terminals = $this->GetFromTerminals($userSession, $info_FromCity[0]->id, "", $userSession->page_no);
				$menuTerminal = "";
				foreach($info_Terminals as $Terminal)
				{
					$menuTerminal .= $Terminal->rownum.". ".ucfirst(strtolower($Terminal->name))."\n";
				}
				
				$info_Terminals = $this->GetFromTerminals($userSession, $info_FromCity[0]->id, "", $userSession->page_no+7);
				
				$userSession->more_keyword=0;
				if($info_Terminals)
				{
					$menuTerminal .= ($userSession->page_no+8).". More\n";
					$userSession->more_keyword=($userSession->page_no+8);
				}
				if($menuTerminal=="")

				{
					$output['session_msg'] = "No service\n* Back";
				}
				else
				{
					$output['session_msg'] = ucfirst($info_FromCity[0]->name)." Terminal".chr(58)."\n".$menuTerminal."* Back";
				}
				
				$userSession->from_city_id=$info_FromCity[0]->id;
				$userSession->from_city_name=$info_FromCity[0]->name;
				
				$userSession->per_page=7;
				$userSession->user_level=3;
				$userSession->keyword=$session_msg;
				$userSession->parent_id=$session_msg;
				$userSession->is_child=1;
				$userSession->save();	
			
				
			break;
			case 2: //Transporters > Bookings  > My Booking list
			
				if($userSession->level_3 == "1")//Booking
				{
					if($moreKeyword)
					{
						$userSession->page_no = $userSession->page_no+4;
					}
					else
					{
						$userSession->c_last_parent_id=$userSession->c_parent_id;
					}
					
					$output['session_operation'] = "continue";
					
					$info_SchLists = $this->GetScheduleSearch($userSession->msisdn,$userSession->page_no);
					$countOfSchLists = sizeof($info_SchLists);
					$menuSchLists = "";
					foreach($info_SchLists as $SchList)
					{
						$menuSchLists .= $SchList->rownum.". ".$SchList->from_terminal_name." to ".$SchList->to_terminal_name."\n";
					}
					
					$info_SchLists = $this->GetScheduleSearch($userSession->msisdn,$userSession->page_no+4);
					$userSession->more_keyword=0;
					if($info_SchLists)
					{
						$menuSchLists .= ($userSession->page_no+5).". More\n";
						$userSession->more_keyword=($userSession->page_no+5);
					}
					$output['session_msg'] = "Choose your Search".chr(58)."\n".$menuSchLists."* Back";
					if($countOfSchLists==0)
					{
						$action = "Booking";
						if($userSession->ussd_code=="*".$session_from)
						{
							$action = "Transporters";
						}
						$this->updateCustomerLog($testing, "update customer_logs set is_complete=0, action='$action' where id=".$userSession->session_id);
						
						$output['session_operation'] = "endcontinue";
						$output['session_msg'] = "No search schedule at the moment, please search schedule list. \n* Back\n0 Main Menu";
					}
					$userSession->per_page=4;				
					$userSession->user_level=3;
					$userSession->keyword=$session_msg;
					$userSession->c_parent_id=$session_msg;
					$userSession->is_child=1;
					$userSession->save();
					
					
				}
				if($userSession->level_3 == "3")//My Ticket
				{
					if($moreKeyword)
					{
						$userSession->page_no = $userSession->page_no+4;
					}
					else
					{
						$userSession->last_parent_id=$userSession->parent_id;
					}
					
					$output['session_operation'] = "continue";
					$info_Bookings = $this->GetActiveBooking($userSession->msisdn, $userSession->page_no);
					//dd($info_Bookings);
					$menuBooking = "";
					foreach($info_Bookings as $info_Booking)
					{
						$menuBooking .= $info_Booking->rownum.". ".$info_Booking->from_terminal_name." to ".$info_Booking->to_terminal_name."\n";
					}
					
					$info_Bookings = $this->GetActiveBooking($userSession->msisdn, $userSession->page_no+4);
					
					$userSession->more_keyword=0;
					if($info_Bookings)
					{
						$menuBooking .= ($userSession->page_no+5).". More\n";
						$userSession->more_keyword=($userSession->page_no+5);
					}
					$output['session_msg'] = "Choose your Booking".chr(58)."\nPlease select".chr(58)."\n".$menuBooking."* Back";
					$userSession->per_page=4;
					$userSession->user_level=3;
					$userSession->keyword=$session_msg;
					$userSession->parent_id=$session_msg;
					$userSession->is_child=1;
					$userSession->save();
					
				}
				else
				if($userSession->level_3 == "2")//Booking Status
				{
					$output['session_operation'] = "continue";							
					$output['session_msg'] = "Enter Booking Reference Code\n\n99. Main Menu";
					$userSession->per_page=4;
					$userSession->user_level=3;
					$userSession->keyword=$session_msg;
					$userSession->parent_id=$session_msg;
					$userSession->is_child=1;
					$userSession->save();
					
				}
				
			break;
			
			case 3: //Transporters > Terminals > Cities
				
				if($moreKeyword)
				{
					$userSession->page_no = $userSession->page_no+4;
					$info_FromCity = $this->GetSelectedCompanyCity($userSession->company_id,$session_msg, $userSession->from_city_id);
				}
				else
				{
					$userSession->c_last_parent_id=$userSession->c_parent_id;
					
					$info_FromCity = $this->GetSelectedCompanyCity($userSession->company_id,$session_msg);
				}
				
				$output['session_operation'] = "continue";
				
				$per_page = 4;
				$info_Terminals = $this->GetFromTerminals($userSession, $info_FromCity[0]->id, "", $userSession->page_no);
				
				$menuTerminals = "";
				foreach($info_Terminals as $Terminals)
				{
					$menuTerminals .= $Terminals->rownum.". ".ucfirst(strtolower($Terminals->name))."\n";
				}
										
				$info_Terminals = $this->GetFromTerminals($userSession, $info_FromCity[0]->id, "", $userSession->page_no+4);
				
				$userSession->more_keyword=0;
				if($info_Terminals)
				{
					$menuTerminals .= ($userSession->page_no+5).". More\n";
					$userSession->more_keyword=($userSession->page_no+5);
				}
				$output['session_msg'] = $info_FromCity[0]->name."\n".$menuTerminals."* Back";
				
				$userSession->from_city_id=$info_FromCity[0]->id;
				$userSession->from_city_name=$info_FromCity[0]->name;
				$userSession->per_page=$per_page;
				$userSession->user_level=3;
				$userSession->keyword=$session_msg;
				$userSession->c_parent_id=$session_msg;
				$userSession->is_child=1;
				$userSession->save();
			break;
			
			case 4: //Transporters > Service > Detail
				//Already ended
			break;
		}
		//****************************************
		// End Transporters
		//****************************************
		
		return $output;
	}
	
	public function Level4($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing)
	{
		if(!$moreKeyword)
			$userSession->level_4=$session_msg;
		//Uncomment echo "Menu 4";
		
		//Change page no on level change, check if level is same else change page no
		if($userSession->user_level != 4)
		{
			$userSession->page_no = 0;
		}
		
		//Transporters
		//****************************************
		// Start Transporters Level 4
		//****************************************
		switch($userSession->c_parent)
		{
			
			case 1: //Transporters > Schedule > Date List
				
				if($moreKeyword)
				{
					$userSession->page_no = $userSession->page_no+$userSession->per_page;
					$perPage = $userSession->per_page;
					
					$info_FromTerminal = $this->GetSelectedFromTerminal($userSession, $userSession->from_city_id, $userSession->to_city_id, $session_msg, $userSession->from_terminal_id);
					//dd($info_FromTerminal);
				}
				else
				{
					$userSession->last_parent_id=$userSession->parent_id;
					
					$info_FromTerminal = $this->GetSelectedFromTerminal($userSession, $userSession->from_city_id, $userSession->to_city_id, $session_msg);
					
					
				}
				
				$output['session_operation'] = "continue";
				
				
				$info_Cities = $this->GetCompanyToCities($userSession->from_city_id, $userSession->company_id, $userSession->page_no);
				//dd($info_Cities);
				$menuCity = "";
				foreach($info_Cities as $City)
				{
					$menuCity .= $City->rownum.". ".ucfirst(strtolower($City->name))."\n";
				}
				
				$info_Cities = $this->GetCompanyToCities($userSession->from_city_id, $userSession->company_id, $userSession->page_no+7);
				
				$userSession->more_keyword=0;

				if($info_Cities)
				{
					$menuCity .= ($userSession->page_no+8).". More\n";
					$userSession->more_keyword=($userSession->page_no+8);
				}
				$output['session_msg'] = "To".chr(58)."\n".$menuCity."* Back";
				
				$userSession->from_terminal_id=$info_FromTerminal[0]->id;
				$userSession->from_terminal_name=$info_FromTerminal[0]->name;
				  
				$userSession->per_page=7;
				$userSession->user_level=4;
				$userSession->keyword=$session_msg;
				$userSession->c_parent_id=$session_msg;
				$userSession->is_child=1;
				$userSession->save();	
				
			break;
			case 2: //Transporters > My Bookings 
				
				if($userSession->level_3 == "1")//Booking
				{
					//Schedule List
					$perPage = 1000;
				
					$userSession->last_parent_id=$userSession->parent_id;
						
					$info_Search = $this->GetSelectedScheduleSearch($userSession->msisdn,$session_msg);
					//dd($info_Search);
					$userSession->from_city_id = $info_Search[0]->from_city_id;
					$userSession->from_city_name = $info_Search[0]->from_city_name;
					$userSession->from_terminal_id = $info_Search[0]->from_terminal_id;
					$userSession->from_terminal_name = $info_Search[0]->from_terminal_name;
					$userSession->to_city_id = $info_Search[0]->to_city_id;
					$userSession->to_city_name = $info_Search[0]->to_city_name;
					$userSession->to_terminal_id = $info_Search[0]->to_terminal_id;
					$userSession->to_terminal_name = $info_Search[0]->to_terminal_name;
					$userSession->schedule_date = $info_Search[0]->schedule_date;
					$userSession->company_id = $info_Search[0]->company_id;
					$userSession->company_name = $info_Search[0]->company_name;
					
					$userSession->discount_code = $info_Search[0]->discount_code;
					$userSession->couponvalue = $info_Search[0]->couponvalue;
					$userSession->usePercent = $info_Search[0]->usePercent;
					$userSession->couponpercent = $info_Search[0]->couponpercent;
					
					$userSession->save();
					
					$output['session_operation'] = "continue";
					
					
					$info_Company = $userSession->Company()->First();
					$company_Ussd_Code = $info_Company->Company_ussd()->First()->code;
					$output['session_msg'] = $info_Company->name."\n".$userSession->from_city_name." to ".$userSession->to_city_name."\n".date_format(date_create($userSession->schedule_date),"D d M Y")."\nTerminal: ".$userSession->to_terminal_name." \n\nEnter Bus No:";
					
					$userSession->per_page=$perPage;
					$userSession->user_level=4;
					$userSession->keyword=$session_msg;
					$userSession->c_parent_id=$session_msg;
					$userSession->is_child=1;
					$userSession->save();
				}
				if($userSession->level_3 == "3")//My Booking
				{
					
					$userSession->c_last_parent_id=$userSession->c_parent_id;
					$userSession->user_level=7;
					$userSession->save();
					
					$action = "My Bookings";
					if($userSession->ussd_code=="*".$session_from)
					{
						$action = "Transporters";
					}
					
					$this->updateCustomerLog($testing, "update customer_logs set is_complete=1, action='$action' where id=".$userSession->session_id);
					
					$info_Booking = $this->GetSelectedActiveBooking($userSession->msisdn,$session_msg);
					$info_Company = Company::FindOrFail($info_Booking[0]->company_id);
					$output['session_operation'] = "endcontinue";		
					$smsMessage = $info_Booking[0]->Fullname."\nRef: ".$info_Booking[0]->refcode." \n".$info_Company->name.": \n".$info_Booking[0]->from_terminal_name." to ".$info_Booking[0]->to_terminal_name." \n".date('M d Y', strtotime($info_Booking[0]->schedule_date))." \n".$info_Booking[0]->dept_time." \n".$info_Booking[0]->bus." \nSeat Number: ".$info_Booking[0]->SelectedSeats;
					//dd($smsMessage);
					$this->sendSMS($session_from,$session_msisdn, $smsMessage, $gateway);
					$output['session_msg'] = "Thanks, your request has being received. You will receive SMS response shortly";
				}
				else
				if($userSession->level_3 == "2")//Booking Status
				{   
					$userSession->c_last_parent_id=$userSession->c_parent_id;
					$userSession->user_level=4;
					$userSession->save();
					
					$action = "My Status";
					if($userSession->ussd_code=="*".$session_from)
					{
						$action = "Transporters";
					}
					
					$this->updateCustomerLog($testing, "update customer_logs set is_complete=1, action='$action' where id=".$userSession->session_id);
					
					//echo "<br />Call GetSelectedCompanyCityTerminalAPI<br />";
					$url = env('API_URL')."BookingStatus/".$userSession->company_id."?Phone=".$userSession->msisdn."&RefCode=".$session_msg;
					
					$options = array(
					  'http'=>array(
						'method'=>"GET",
						'header'=>"Authorization: Bearer ".env('API_TOKEN')
					  )
					);
					
					$context = stream_context_create($options);
					$data = file_get_contents($url, false, $context);
					
					$decodeData = json_decode($data, true);
					
					$output['session_operation'] = "endcontinue";							
					$smsMessage = $userSession->company_name."\nRef: ".$session_msg."\n".$decodeData["data"]["Details"][0]["Loading_Office"]." to ".$decodeData["data"]["Details"][0]["Destination"]."\nAddress: ".$decodeData["data"]["Details"][0]["Terminal"][0]["Address"]."\nDepart: ".$decodeData["data"]["Details"][0]["Departure_Time"]."\nSeat: ".$decodeData["data"]["Details"][0]["OrderDetails"]["ordertotalseat"]."\nAmount: ".$decodeData["data"]["Details"][0]["OrderDetails"]["fareamount"]."\nName: ".$decodeData["data"]["Details"][0]["CustomerDetails"][0]["Name"]."\nPhone: ".$decodeData["data"]["Details"][0]["OrderDetails"]["phonenumber"]."\nStatus: ".$decodeData["data"]["Details"][0]["OrderDetails"]["responsecode"];
												
					$this->sendSMS($session_from,$session_msisdn, $smsMessage, $gateway);
					$output['session_msg'] = "Thanks, your request has being received. You will receive SMS response shortly";
					
				}
			break;
			case 3: //Transporters > Terminals > Detail 
				$userSession->c_last_parent_id=$userSession->c_parent_id;
				$userSession->user_level=4;
				$userSession->save();
				
				$action = "Terminals";
				if($userSession->ussd_code=="*".$session_from)
				{
					$action = "Transporters";
				}
				
				$this->updateCustomerLog($testing, "update customer_logs set is_complete=1, action='$action', from_city_id='".$userSession->from_city_id."', from_city_name='".$userSession->from_city_name."' where id=".$userSession->session_id);

				$info_FromTerminal = $this->GetSelectedFromTerminal($userSession, $userSession->from_city_id, $userSession->to_city_id, $session_msg);
				
				$output['session_operation'] = "endcontinue";		
				
				$smsMessage = $userSession->Company()->First()->name.": ".$info_Terminal[0]->name."\n".$info_Terminal[0]->address."\n".$info_Terminal[0]->phone_number;//."\n* Back\n0 Main Menu";
				
				$this->sendSMS($session_from,$session_msisdn, $smsMessage, $gateway);
				$output['session_msg'] = "Thanks, your request has being received. You will receive SMS response shortly";
			break;
			case 4: //Transporters > Company Services
					//Already ended	
			break;
		}
		//****************************************
		// End Transporters
		//****************************************
		
		return $output;
	}
	
	public function Level5($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing)
	{
		if(!$moreKeyword)
			$userSession->level_5=$session_msg;
		//Uncomment echo "Menu 5";
		
		//Change page no on level change, check if level is same else change page no
		if($userSession->user_level != 5)
		{
			$userSession->page_no = 0;
		}
		
		//Transporters
		//****************************************
		// Start Transporters Level 5
		//****************************************
		switch($userSession->c_parent)
		{
			case 1: //Transporters > Schedule > From Terminal
				$perPage = 1000;
				
				if($moreKeyword)
				{
					$userSession->page_no = $userSession->page_no+7;
					$info_ToCity = $this->GetCompanyToCities($userSession->from_city_id, $userSession->company_id, $userSession->page_no, $userSession->to_city_id);
					$info_Schedules = ["1"=>"1"];
					//dd($info_ToCity);
				}
				else
				{
					$userSession->c_last_parent_id=$userSession->c_parent_id;
				}
				$output['session_operation'] = "continue";
				
				$info_Terminals = $this->GetToTerminals($userSession, $userSession->from_city_id, $info_ToCity[0]->id, $userSession->page_no);
				$menuTerminal = "";   
				foreach($info_Terminals as $Terminal)
				{
					$menuTerminal .= $Terminal->rownum.". ".ucfirst(strtolower($Terminal->name))."\n";
				}
				
				$info_Terminals = $this->GetToTerminals($userSession->company_id, $userSession->from_city_id, $info_ToCity[0]->id, $userSession->page_no+7);
				
				//dd($info_Terminals);
				$userSession->more_keyword=0;
				if($info_Terminals)
				{
					$menuTerminal .= ($userSession->page_no+8).". More\n";
					$userSession->more_keyword=($userSession->page_no+8);
				}
				if($menuTerminal=="")

				{
					$output['session_msg'] = "No service\n* Back";
				}
				else
				{
					$output['session_msg'] = ucfirst($info_ToCity[0]->name)." Terminal".chr(58)."\n".$menuTerminal."* Back";
				}
				
				$userSession->to_city_id=$info_ToCity[0]->id;
				$userSession->to_city_name=$info_ToCity[0]->name;
				
				$userSession->per_page=7;
				$userSession->user_level=5;
				$userSession->keyword=$session_msg;
				$userSession->parent_id=$session_msg;
				$userSession->is_child=1;
				$userSession->save();
				
			break;
			case 2: //Transporters > Bookings > How many travellers
				if($userSession->level_3 == "1")//Booking
				{
					
					$info_CompanySchedule = $this->GetSelectedCompanyScheduleAPI($userSession->company_id,$userSession->schedule_date,$userSession->from_terminal_id,$userSession->to_terminal_id,$userSession->to_city_id,$session_msg);
					
					if($info_CompanySchedule)
					{
						$info_Company = $userSession->Company()->First();
						$output['session_operation'] = "continue";	
						
						//."\n".$userSession->from_city_name." to ".$userSession->to_city_name
						$output['session_msg'] = $userSession->company_name.chr(58)."\n".$userSession->from_terminal_name." to ".$userSession->to_terminal_name."\n".$userSession->schedule_date."\n".$info_CompanySchedule[0]["dept_time"]."\n".$info_CompanySchedule[0]["bus"]."\n".sizeof($info_CompanySchedule[0]["AvailableSeat"])." Seats\n"."1. Confirm\n99. Main Menu";
						//dd($output['session_msg']);
						$userSession->per_page=4;
						$userSession->available_seats=json_encode($info_CompanySchedule[0]["AvailableSeat"]);
						$userSession->block_seats=json_encode($info_CompanySchedule[0]["BlockedSeat"]);
						$userSession->fare=$info_CompanySchedule[0]["fare"];
						$userSession->tripid=$info_CompanySchedule[0]["TripID"];
						$userSession->destid=$info_CompanySchedule[0]["DestinationID"];
						$userSession->orderid=$info_CompanySchedule[0]["OrderID"];
						$userSession->user_level=5;
						$userSession->keyword=$session_msg;
						$userSession->c_parent_id=$session_msg;
						$userSession->is_child=1;
						//dd($userSession);
						$userSession->save();		
						
					}
					else
					{
						$output['session_operation'] = "endcontinue";
						$output['session_msg'] = "Invalid booking request";
					}
				}
				else //Status
				{
					$output['session_operation'] = "continue";
					$output['session_msg'] = "Booking Status"."* Back";
				}
			break;
			case 3: //Transporters > Company Services
				//Already ended	
			break;
			case 4: //Transporters > Terminals
					//Already ended
			break;
		}
		//****************************************
		// End Transporters
		//****************************************
		
		return $output;
	}
	
	
	public function Level6($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing)
	{
		if(!$moreKeyword)
			$userSession->level_6=$session_msg;
		//Uncomment echo "Menu 6";
		
		//Change page no on level change, check if level is same else change page no
		if($userSession->user_level != 6)
		{
			$userSession->page_no = 0;
		}
		
		//Transporters
		//****************************************
		// Start Transporters Level 6
		//****************************************
		switch($userSession->c_parent)
		{
			case 1: //Transporters > Schedule > To Terminal
				
				if($moreKeyword)
				{
					$userSession->page_no = $userSession->page_no+$userSession->per_page;
					$perPage = $userSession->per_page;
					
					$info_ToTerminal = $this->GetSelectedToTerminal($userSession->company_id, $userSession->from_city_id, $userSession->to_city_id, $session_msg, $userSession->to_terminal_id);

				}
				else
				{
					$userSession->last_parent_id=$userSession->parent_id;
					
					$info_ToTerminal = $this->GetSelectedToTerminal($userSession->company_id, $userSession->from_city_id, $userSession->to_city_id, $session_msg);
					
				}
				
				$output['session_operation'] = "continue";
				
				$date = date('D d M', mktime(0, 0, 0, date("m") , date("d")-1,date("Y")));
				$menuDate = "";
				
				$info_Company = $userSession->Company()->First();
				$booking_no_of_days = $info_Company->booking_no_of_days;
					
				if($userSession->company_ussd_type == "1")
				{
					
					for($i=$userSession->page_no+1; $i < ($userSession->page_no+$booking_no_of_days); $i++)
					{
						$menuDate .= $i.". ".date('D d M', strtotime("+$i day", strtotime($date)))."\n";
					}
				}
				else
				{
					$info_GetGeneralSetting = $this->GetGeneralSetting($userSession->company_id);						
					
					$begin = new DateTime($info_GetGeneralSetting['StartDate']);
					$end = new DateTime($info_GetGeneralSetting['EndDate']);
					
					$menuDateArr = array();
					$j = 0;
					for($i = $begin; $i <= $end; $i->modify('+1 day')){
						if($j <= $booking_no_of_days)
						{
							$menuDateArr[] = $i->format("D d M");
						}
						$j++;
					}
					
					for($i=$userSession->page_no+1; $i < sizeof($menuDateArr); $i++)
					{
						$menuDate .= $i.". ".$menuDateArr[$i-1]."\n";
					}
					//dd($menuDateArr);
				}
				//$menuDate .= $i.". More\n";
				//$userSession->more_keyword=$i;
				
				//$menuDate .= "Or Reply with dd/mm/yyyy\n";
				$userSession->more_keyword=0;
				
				$output['session_msg'] = "Date".chr(58)."\n".$menuDate."* Back";
				$userSession->to_terminal_id=$info_ToTerminal[0]->id;
				$userSession->to_terminal_name=$info_ToTerminal[0]->name;
				$userSession->per_page=5;
				$userSession->user_level=6;
				$userSession->keyword=$session_msg;
				$userSession->c_parent_id=$session_msg;
				$userSession->is_child=1;
				$userSession->save();

			break;
			case 2: //Transporters > Bookings > No of seats
			
				if($userSession->level_3 == "1")//Booking
				{
					if($moreKeyword)
					{
						$userSession->page_no = $userSession->page_no+$userSession->per_page;
						$infoSeatsList = $this->AvailableSeatPaging(json_decode($userSession->available_seats, true),json_decode($userSession->block_seats, true),$userSession->page_no);
					}
					else
					{
						$userSession->last_parent_id=$userSession->parent_id;
						$infoSeatsList = $this->AvailableSeatPaging(json_decode($userSession->available_seats, true),json_decode($userSession->block_seats, true),1);
					}
					
					$output['session_operation'] = "continue";
					
					if(sizeof(json_decode($userSession->available_seats, true)) > $userSession->page_no+20)
					{							
						$output['session_msg'] = "Seats (".implode(', ', $infoSeatsList).")\n Choose Seat(s)".chr(58)." (e.g 6 for seat 6)\n99 More\n* Back";
						$userSession->more_keyword=99;
					}
					else
					{
						$output['session_msg'] = "Seats (".implode(', ', $infoSeatsList).")\n Choose Seat(s)".chr(58)." (e.g 6 for seat 6)\n* Back";
						$userSession->more_keyword=0;
					}
					$userSession->per_page=20;
					$userSession->user_level=6;
					$userSession->keyword=$session_msg;
					$userSession->c_parent_id=$session_msg;
					$userSession->is_child=1;
					$userSession->save();	
				}
				else //Status
				{
					$output['session_operation'] = "continue";
					$output['session_msg'] = "Booking Status"."* Back";
				}
			break;
			case 3: //Transporters > Company Services
				//Already ended	
			break;
			case 4: //Transporters > Terminals
					//Already ended
			break;
			case 5: //Transporters > Promo & Deals
					//Already ended	
			break;
			
			case 6: //Transporters > Policy Statement
				//Already ended	
			break;
			
			case 7: //Transporters > About
				//Already ended	
			break;
			
			case 8: //Transporters > Exit
				//Already ended	
			break;
		}
		//****************************************
		// End Transporters
		//****************************************
		
		return $output;

	}
	
	public function Level7($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing)
	{
		if(!$moreKeyword)
			$userSession->level_7=$session_msg;
		//Uncomment echo "Menu 7";
		
		//Change page no on level change, check if level is same else change page no
		if($userSession->user_level != 7)
		{
			$userSession->page_no = 0;
		}
		
		//Transporters
		//****************************************
		// Start Transporters Level 7
		//****************************************
		switch($userSession->c_parent)
		{
			case 1: //Transporters > Schedule > To Terminal
				
				$perPage = 1000;
				
				if($moreKeyword)
				{
					$userSession->page_no = $userSession->page_no+4;
					$info_Date = $userSession->schedule_date;
				}
				else
				{
					if($userSession->company_ussd_type == "1")
					{
						$userSession->c_last_parent_id=$userSession->c_parent_id;
						if($session_msg >= 1 && $session_msg <= 5)
						{
							$correctDate = 1;
							$date = date('D d M', mktime(0, 0, 0, date("m") , date("d")-1,date("Y")));
							$info_Date = date('Y-m-d', strtotime("+$session_msg day", strtotime($date)));
						}
						else
						{
							if($this->isDate($session_msg))
							{
								$correctDate = 1;
								$date = DateTime::createFromFormat('d/m/Y', $session_msg);
								$info_Date = $date->format('Y-m-d');
							}
						}
					}
					else
					{
						$info_GetGeneralSetting = $this->GetGeneralSetting($userSession->company_id);						
						
						$info_Company = $userSession->Company()->First();
						$booking_no_of_days = $info_Company->booking_no_of_days;
				
						$begin = new DateTime($info_GetGeneralSetting['StartDate']);
						$end = new DateTime($info_GetGeneralSetting['EndDate']);
						
						$menuDateArr = array();
						$j = 0;
						for($i = $begin; $i <= $end; $i->modify('+1 day')){
							if($j <= $booking_no_of_days)
							{
								$menuDateArr[] = $i->format("D d M Y");
							}
							$j++;
						}
						if($session_msg >= 1 && $session_msg <= 5)
						{
							$correctDate = 1;
							$info_Date = date('Y-m-d', strtotime($menuDateArr[$session_msg-1]));

						}
						else
						{
							if($this->isDate($session_msg))
							{
								$correctDate = 1;
								$date = DateTime::createFromFormat('d/m/Y', $session_msg);
								$info_Date = $date->format('Y-m-d');
							}
						}
					}
					
					//dd($info_ToTerminal);
				}
				
				$output['session_operation'] = "continue";
				
				if($correctDate == 1)
				{
					$output['session_operation'] = "endcontinue";
					$output['session_msg'] = "Thanks for choosing ".$userSession->Company()->First()->name.". You will receive SMS with Buses available shortly.";
					
					dispatch(new FinalBookNewMessage($userSession, $session_from,$session_msisdn, $gateway, $info_Date));
				}
			break;
			
			case 2: //Transporters > Bookings > Full name
			
				if($userSession->level_3 == "1")//Booking
				{
					$correctMessage = true;
					$partMessages = explode(",",$session_msg);
					//dd($partMessages);
					foreach($partMessages as $partMessage)
					{
						if(is_numeric($partMessage))
						{
							if(!in_array($partMessage, json_decode($userSession->available_seats, true)))
							{
								$correctMessage = false;
							}
						}
						else
						{
							if(!in_array("'".$partMessage."'", json_decode($userSession->available_seats, true)))
							{
								$correctMessage = false;
							}
						}
					}

					//dd($correctMessage);
					
					
					$blockMessage = false;
					foreach($partMessages as $partMessage)
					{
						if(is_numeric($partMessage))
						{
							
							if(in_array($partMessage, json_decode($userSession->block_seats, true)))
							{
								$blockMessage = true;
							}
						}
						else
						{
							if(in_array("'".$partMessage."'", json_decode($userSession->block_seats, true)))
							{
								$blockMessage = true;
							}
						}
					}
					//dd($blockMessage);
					if(!$correctMessage)
					{
						if($moreKeyword)
						{
							$userSession->page_no = $userSession->page_no+$userSession->per_page;
							$infoSeatsList = $this->AvailableSeatPaging(json_decode($userSession->available_seats, true),json_decode($userSession->block_seats, true),$userSession->page_no);
						}
						else
						{
							$userSession->last_parent_id=$userSession->parent_id;
							$infoSeatsList = $this->AvailableSeatPaging(json_decode($userSession->available_seats, true),json_decode($userSession->block_seats, true),1);
						}
						
						$output['session_operation'] = "continue";
						
						if(sizeof(json_decode($userSession->available_seats, true)) > $userSession->page_no+20)
						{							
							$output['session_msg'] = "Wrong Reply.\nSeats (".implode(', ', $infoSeatsList).")\n Choose Seat(s)".chr(58)." (e.g 6 for seat 6)\n99 More\n* Back";
							$userSession->more_keyword=99;
						}
						else
						{
							$output['session_msg'] = "Wrong Reply.\nSeats (".implode(', ', $infoSeatsList).")\n Choose Seat(s)".chr(58)." (e.g 6 for seat 6)\n* Back";
							$userSession->more_keyword=0;
						}
						$userSession->per_page=20;
						$userSession->user_level=7;
						$userSession->keyword=$session_msg;
						$userSession->c_parent_id=$session_msg;
						$userSession->is_child=1;
						$userSession->save();
					}
					else
					if($blockMessage)
					{
						//Check block seat////////////
						if($moreKeyword)
						{
							$userSession->page_no = $userSession->page_no+$userSession->per_page;
							$infoSeatsList = $this->AvailableSeatPaging(json_decode($userSession->available_seats, true),json_decode($userSession->block_seats, true),$userSession->page_no);
						}
						else
						{
							$userSession->last_parent_id=$userSession->parent_id;
							$infoSeatsList = $this->AvailableSeatPaging(json_decode($userSession->available_seats, true),json_decode($userSession->block_seats, true),1);
						}
						
						$output['session_operation'] = "continue";
						
						if(sizeof(json_decode($userSession->available_seats, true)) > $userSession->page_no+20)
						{							
							$output['session_msg'] = "Seat already taken.\nSeats (".implode(', ', $infoSeatsList).")\n Choose Seat(s)".chr(58)." (e.g 6 for seat 6)\n99 More\n* Back";
							$userSession->more_keyword=99;
						}
						else
						{
							$output['session_msg'] = "Seat already taken.\nSeats (".implode(', ', $infoSeatsList).")\n Choose Seat(s)".chr(58)." (e.g 6 for seat 6)\n* Back";
							$userSession->more_keyword=0;
						}
						$userSession->per_page=20;
						$userSession->user_level=7;
						$userSession->keyword=$session_msg;
						$userSession->c_parent_id=$session_msg;
						$userSession->is_child=1;
						$userSession->save();		
						/////////////////////////////
					}
					else
					{
						
						//Remove Seats
						/*$url = env('API_URL')."ActivateLockSeat/".$userSession->company_id."?Destinationid=".$userSession->destid."&SelectedSeats=".$session_msg."&TripID=".$userSession->tripid."&lockedby=".$userSession->orderid;
						$options = array(
						  'http'=>array(
							'method'=>"GET",
							'header'=>"Authorization: Bearer ".env('API_TOKEN')
						  )
						);
						
						$context = stream_context_create($options);
						$data = file_get_contents($url, false, $context);
						
						$decodeData = json_decode($data, true);*/
						//End Remove seats
						
						$output['session_operation'] = "continue";	
						$output['session_msg'] = "Enter Booking Phone number\nReply 1 for this phone number\n.* Back";
						
						$userSession->no_of_traveler=sizeof(explode(",",$session_msg));
						$userSession->no_of_seats=trim(str_replace(" ", "",$session_msg));				
						$userSession->user_level=7;
						$userSession->keyword=$session_msg;
						$userSession->c_parent_id=$session_msg;
						$userSession->is_child=1;
						$userSession->save();
					}
				}
				else //Status
				{
					$output['session_operation'] = "continue";
					$output['session_msg'] = "Booking Status"."* Back";
				}
			break;
			case 3: //Transporters > Company Services
				//Already ended	
			break;
			case 4: //Transporters > Terminals
					//Already ended
			break;
		}
		//****************************************
		// End Transporters
		//****************************************

		return $output;
	}
	
	public function Level8($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing)
	{
		if(!$moreKeyword)
			$userSession->level_8=$session_msg;
		//Uncomment echo "Menu 8";
		
		//Change page no on level change, check if level is same else change page no
		if($userSession->user_level != 8)
		{
			$userSession->page_no = 0;
		}
		
		//Transporters
		//****************************************
		// Start Transporters Level 8
		//****************************************
		switch($userSession->c_parent)
		{
			case 1: //Transporters > Schedule > Schedule detail
				
				$userSession->c_last_parent_id=$userSession->c_parent_id;
				
				$output['session_operation'] = "endcontinue";
				
				
				//Database schedule
				if($userSession->company_ussd_type == "1")
				{
					$info_CompanySchedule = $this->GetSelectedCompanySchedule($userSession->company_id,$userSession->schedule_date,$userSession->from_terminal_id,$userSession->to_city_id,$session_msg);
					$info_Company = Company::FindOrFail($userSession->company_id);
					$info_Schedule = Schedule::FindOrFail($info_CompanySchedule[0]['sch_id']);
					$info_Fare =  Fare::FindOrFail($info_CompanySchedule[0]['fid']);
					
					$BusName = $info_Schedule->Bus()->First()->name;
					$FareType = $info_Fare->Fare_type()->First()->name;
					$ServiceName = $info_Fare->Fare_type()->First()->Service()->First()->name;
					
					$FirstSchTime = $info_Schedule->Schedule_time()->Orderby('sequence')->First();
					$DeptTime = date('h:iA',strtotime($FirstSchTime->departure_time));
					$DeptDate = date('D d M',strtotime($info_CompanySchedule[0]['dept_time']));				
					
					$LastSchTime = $info_Schedule->Schedule_time()->Orderby('sequence', 'desc')->First();
					$ArrvTime = date('h:iA',strtotime($LastSchTime->arrival_time));
					$time1 = strtotime($FirstSchTime->departure_time);
					$time2 = strtotime($LastSchTime->arrival_time);
					$interval = $time1-$time2;
					if($interval<0)
						$interval = abs($interval);
				
					$ArrvDate =  date('D d M',strtotime($FirstSchTime->departure_time)+$interval);
					
					$OtherSchTime="";
					foreach($info_Schedule->Schedule_time()->where('sequence','>','1')->Orderby('sequence')->Get() as $Schedult_time)
					{
						//$OtherSchTime .= $Schedult_time->Terminal()->First()->name." ".date('h:iA',strtotime($Schedult_time->arrival_time))."\n";
						//Just show the last one
						$OtherSchTime = $Schedult_time->Terminal()->First()->name." ".date('h:iA',strtotime($Schedult_time->arrival_time))."\n";
					}
					
					$action = "Schedule & Fare";
					if($userSession->ussd_code=="*".$session_from)
					{
						$action = "Transporters";
					}
					
					$this->updateCustomerLog($testing, "update customer_logs set is_complete=1, action='$action', from_city_id='".$userSession->from_city_id."', from_city_name='".$userSession->from_city_name."', to_city_id='".$userSession->to_city_id."', to_city_name='".$userSession->to_city_name."', route_id='".$info_Schedule->Route()->First()->id."', route_name='".$info_Schedule->Route()->First()->name."', schedule_id='".$info_Schedule->id."', schedule_name='".$info_Schedule->short_name."' where id=".$userSession->session_id);
					
					$output['session_msg'] = "1. ".$info_Company->name."".chr(58)."\n".$userSession->from_city_name." to ".$userSession->to_city_name."\n\n".$BusName."\n".$FareType." ".$ServiceName." N".$info_CompanySchedule[0]['fare']."\n\nDept".chr(58)."\n".$DeptDate."\n".$FirstSchTime->Terminal()->First()->name." ".$DeptTime."\n\nArrv".chr(58)."\n".$ArrvDate."\n".$OtherSchTime."\n* Back\n0 Main Menu";
				}
				else //API schedule
				{
					$info_CompanySchedule = $this->GetSelectedCompanyScheduleAPI($userSession->company_id,$userSession->schedule_date,$userSession->from_terminal_id,$userSession->to_terminal_id,$userSession->to_city_id,$session_msg);
											
					$output['session_msg'] = "1. ".$userSession->company_name."".chr(58)."\n".$userSession->from_city_name." to ".$userSession->to_city_name."\n\n".$info_CompanySchedule[0]["bus"]."\n N".$info_CompanySchedule[0]['fare']."\n\nDept".chr(58)."\n".$info_CompanySchedule[0]["dept_time"]."\n".$info_CompanySchedule[0]["terminal_name"]." ".$info_CompanySchedule[0]["dept_time"]."\n\n* Back\n0 Main Menu";
					
					//dd($info_CompanySchedule);
				}

				$userSession->schedule_id=$info_CompanySchedule[0]['sch_id'];
				$userSession->fare_id=$info_CompanySchedule[0]['fid'];
				
				$userSession->user_level=8;
				$userSession->keyword=$session_msg;
				$userSession->parent_id=$session_msg;
				$userSession->is_child=1;
				$userSession->save();
			break;
			
			case 2: //Transporters > Bookings >  Next of Kin Full name:
				if($userSession->level_3 == "1")//Booking
				{
					$output['session_operation'] = "continue";	
					$output['session_msg'] = "Enter Your Fullname (e.g. John Mark)\n.* Back";
					if($session_msg=="1")
						$userSession->booking_msisdn=$userSession->msisdn;				
					else
						$userSession->booking_msisdn=$session_msg;	
					$userSession->user_level=8;
					$userSession->keyword=$session_msg;
					$userSession->c_parent_id=$session_msg;
					$userSession->is_child=1;
					$userSession->save();
					
					//If user exist in booking the then skip name & kin info
					if($userSession->company_ussd_type == "1")
					{
						$info_Booking = Booking::Where('msisdn',$userSession->booking_msisdn)->First();
					}
					else
					{
						$url = env('API_URL')."ValidatePhone/".$userSession->company_id."?Msisdn=".$userSession->booking_msisdn;
						$options = array(
						  'http'=>array(
							'method'=>"GET",
							'header'=>"Authorization: Bearer ".env('API_TOKEN')
						  )
						);
						
						$context = stream_context_create($options);
						$data = file_get_contents($url, false, $context);
						
						$decodeData = json_decode($data, true);
						
						//dd($decodeData["data"][0]);
						$info_Booking = NULL;
						if(isset($decodeData["data"][0]))
						{
							$info_Booking = new \stdClass();
							$info_Booking->email = $decodeData["data"][0]["Email"];
							$info_Booking->Fullname = $decodeData["data"][0]["Name"];
							$info_Booking->nextKin = $decodeData["data"][0]["Next_Of_Kin_Name"];
							$info_Booking->nextKinPhone = $decodeData["data"][0]["Next_Of_Kin_Addy"];
							$info_Booking->Sex = $decodeData["data"][0]["Sex"];
						}
						
						//dd($info_Booking);
					}
					if($info_Booking && $info_Booking!=NULL)
					{
						$userSession->name=$info_Booking->Fullname;	
						$userSession->nextkin=$info_Booking->nextKin;	
						$userSession->nextkinnumber=$info_Booking->nextKinPhone;	
						//$userSession->Sex=$info_Booking->Sex;	
						//$userSession->email=$info_Booking->email;	
						$userSession->user_level=10;
						$userSession->keyword=$session_msg;
						$userSession->c_parent_id=$session_msg;
						$userSession->is_child=1;
						$userSession->last_skip_level=8;
						$userSession->new_customer='0';
						$userSession->save();
						
						$output = $this->Level11($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing);
						//dd($output);
						//dd(1);
					}
					else
					{
						$userSession->last_skip_level='';
						$userSession->new_customer='1';
						$userSession->save();
					}
				}
				else //Status
				{
					$output['session_operation'] = "continue";
					$output['session_msg'] = "Booking Status"."* Back";
				}
			break;
			case 3: //Transporters > Company Services
				//Already ended	
			break;
			case 4: //Transporters > Terminals
					//Already ended
			break;
		}
		//****************************************
		// End Transporters
		//****************************************
		
		return $output;
	}
	
	public function Level9($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing)
	{
		if(!$moreKeyword)
			$userSession->level_9=$session_msg;
		//Uncomment echo "Menu 9";
		
		//Change page no on level change, check if level is same else change page no
		if($userSession->user_level != 9)
		{
			$userSession->page_no = 0;
		}
		
		//****************************************
		// Start Transporters Level 9
		//****************************************
		switch($userSession->c_parent)
		{
			case 1: //Transporters > Schedule > Schedule detail
				//Already ended	
			break;
			case 2: //Transporters > Bookings >  Next of Kin Full name:
				if($userSession->level_3 == "1")//Booking
				{
					$output['session_operation'] = "continue";	
					$output['session_msg'] = "Enter  Next of Kin  Full name and Phone no:\n(Full name,Phone no)\n.* Back";
					$userSession->name=$session_msg;				
					$userSession->user_level=9;
					$userSession->keyword=$session_msg;
					$userSession->c_parent_id=$session_msg;
					$userSession->is_child=1;
					$userSession->save();		
				}
				else //Status
				{
					$output['session_operation'] = "continue";
					$output['session_msg'] = "Booking Status"."* Back";
				}
			break;
			case 3: //Transporters > Company Services
				//Already ended	
			break;
			case 4: //Transporters > Terminals
					//Already ended
			break;
		}
		//****************************************
		// End Transporters
		//****************************************

		return $output;
	}
	
	public function Level10($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing)
	{
		if(!$moreKeyword)
			$userSession->level_10=$session_msg;
		//Uncomment echo "Menu 10";
		
		//Change page no on level change, check if level is same else change page no
		if($userSession->user_level != 10)
		{
			$userSession->page_no = 0;
		}
		
		//****************************************
		// Start Transporters Level 10
		//****************************************
		switch($userSession->c_parent)
		{
			case 1: //Transporters > Schedule > Schedule detail
				//Already ended	
			break;
			case 2: //Transporters > Bookings > Fare Detail confirm
				if($userSession->level_3 == "1")//Booking
				{
					$output['session_operation'] = "continue";	
					$output['session_msg'] = "Enter Next of Kin phone no:\n.* Back";
					$userSession->nextkin=$session_msg;				
					$userSession->user_level=10;
					$userSession->keyword=$session_msg;
					$userSession->c_parent_id=$session_msg;
					$userSession->is_child=1;
					$userSession->save();								
				}
				else //Status
				{
					$output['session_operation'] = "continue";
					$output['session_msg'] = "Booking Status"."* Back";
				}
			break;
			case 3: //Transporters > Company Services
				//Already ended	
			break;
			case 4: //Transporters > Terminals
					//Already ended
			break;
		}
		//****************************************
		// End Transporters
		//****************************************
	
		return $output;
	}
	
	public function Level11($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing)
	{
		if(!$moreKeyword)
			$userSession->level_11=$session_msg;
		//Uncomment echo "Menu 11";
		
		//Change page no on level change, check if level is same else change page no
		if($userSession->user_level != 11)
		{
			$userSession->page_no = 0;
		}
		
		//****************************************
		// Start Transporters Level 4
		//****************************************
		switch($userSession->c_parent)
		{
			case 1: //Transporters > Schedule > Schedule detail
				//Already ended	
			break;
			
			case 2: //Transporters > Bookings > Ticket Detail
				if($userSession->level_3 == "1")//Booking
				{
					//$info_CompanySchedule = $this->GetSelectedCompanyScheduleAPI($userSession->company_id,$userSession->schedule_date,$userSession->from_terminal_id,$userSession->to_terminal_id,$userSession->to_city_id,$userSession->level_8);
					if($userSession->last_skip_level == "")
					{
						$parseKin = explode(",",$session_msg);
						$userSession->nextkin=$parseKin[0];	
						$userSession->nextkinnumber=$parseKin[1];
						$userSession->save();
					}
					//dd($info_CompanySchedule);
					$info_Company = $userSession->Company()->First();
					
					$totalFare = 0;
					$channelAmount = 0;
					$gatewayAmount = 0;
					$ticketAmount = $userSession->no_of_traveler*$userSession->fare;
					
					if($info_Company->gateway_id=="1")//CorelPay
					{
						if($info_Company->gateway_fee_type=="2")//Set fees flat

						{
							$gatewayAmount = $info_Company->gateway_fee;
						}
						else //Percentage of ticket

						{
							$gatewayAmount = ($ticketAmount/100)*$info_Company->gateway_fee;
						}
					}
					
					if($info_Company->channel_fee_type=="1")//Set as percentage of total ticket amount  (e.g. 5% of ticket amount)

					{
						$channelAmount = ($ticketAmount/100)*$info_Company->channel_fee;
					}
					else
					if($info_Company->channel_fee_type=="2")//Set as flat amount per seat (e.g. 100*no of passengers)

					{
						$channelAmount = $userSession->no_of_traveler*$info_Company->channel_fee;
					}
					else //Set as a flat amount on total seats (e.g. 2000)

					{
						$channelAmount = $info_Company->channel_fee;
					}

					$output['session_operation'] = "continue";	
					
					$output['session_msg'] = "Confirm your details\nName: ".$userSession->name."\nPhone: ".$userSession->booking_msisdn."\nSeat Taken: ".$userSession->no_of_seats."\nNext of Kin: ".$userSession->nextkin."\nNext of Kin Phone: ".$userSession->nextkinnumber."\n1. Confirm\n* Back\n0 Main Menu";
					
					$userSession->gateway_fee = $gatewayAmount;	
					$userSession->channel_fee = $channelAmount;		
					$userSession->total_amount = ($ticketAmount + $gatewayAmount + $channelAmount);		
					$userSession->user_level=11;
					$userSession->keyword=$session_msg;
					$userSession->c_parent_id=$session_msg;
					$userSession->is_child=1;
					$userSession->save();						
				}
				else //Status
				{
					$output['session_operation'] = "continue";
					$output['session_msg'] = "Booking Status"."* Back";
				}
			break;
			case 3: //Transporters > Company Services
				//Already ended	
			break;
			case 4: //Transporters > Terminals
					//Already ended
			break;
		}
		//****************************************
		// End Transporters
		//****************************************
		
		return $output;
	}
	
	public function Level12($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing)
	{
		if(!$moreKeyword)
			$userSession->level_12=$session_msg;
		//Uncomment echo "Menu 12";
		
		//Change page no on level change, check if level is same else change page no
		if($userSession->user_level != 12)
		{
			$userSession->page_no = 0;
		}
		
		//****************************************
		// Start Transporters Level 12
		//****************************************
		switch($userSession->c_parent)
		{
			case 1: //Transporters > Schedule > Schedule detail
				//Already ended	
			break;
			
			case 2: //Transporters > Bookings > Bank
				if($userSession->level_3 == "1")//Booking
				{
					
					if($moreKeyword)
					{
						$userSession->page_no = $userSession->page_no+6;
						//dd($moreKeyword);
					}
					else
					{
						$userSession->c_last_parent_id=$userSession->c_parent_id;
					}
					
					$output['session_operation'] = "continue";
					
					$info_Banks = $this->GetBanks($userSession->page_no);
					
					$countOfMenu = sizeof($info_Banks);
					$menuBanks = "";
					foreach($info_Banks as $Bank)
					{
						$menuBanks .= $Bank->rownum.". ".$Bank->name."\n";
					}
					
					$info_Banks = $this->GetBanks($userSession->page_no+6);
					
					
					$userSession->more_keyword=0;
					if($info_Banks)
					{
						$menuBanks .= ($userSession->page_no+7).". More\n";
						$userSession->more_keyword=($userSession->page_no+7);
					}
					$output['session_msg'] = "Choose Bank for payment".chr(58)."\n\n".$menuBanks."* Back";
					if($countOfMenu==0)
					{
						$action = "Banks";
						if($userSession->ussd_code=="*".$session_from)
						{
							$action = "Transporters";
						}
						$this->updateCustomerLog($testing, "update customer_logs set is_complete=0, action='$action' where id=".$userSession->session_id);
						
						$output['session_operation'] = "endcontinue";
						$output['session_msg'] = "No bank at the moment, please check later. \n* Back\n0 Main Menu";
					}
					//dd($output['session_msg']);
					$userSession->per_page=6;				
					$userSession->user_level=12;
					$userSession->keyword=$session_msg;
					$userSession->c_parent_id=$session_msg;
					$userSession->is_child=1;
					$userSession->save();							
				}
				else //Status
				{
					$output['session_operation'] = "continue";
					$output['session_msg'] = "Booking Status"."* Back";  
				}
			break;
			case 3: //Transporters > Company Services
				//Already ended	
			break;
			case 4: //Transporters > Terminals
					//Already ended
			break;
			case 5: //Transporters > Promo & Deals
					//Already ended	
			break;
			
			case 6: //Transporters > Policy Statement
				//Already ended	
			break;
			
			case 7: //Transporters > About
				//Already ended	
			break;

			
			case 8: //Transporters > Exit
				//Already ended	
			break;
		}
		//****************************************
		// End Transporters
		//****************************************
		return $output;
	}
	
	

	public function Level13($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing)
	{
		if(!$moreKeyword)
			$userSession->level_13=$session_msg;
		//Uncomment echo "Menu 13";
		
		//Change page no on level change, check if level is same else change page no
		if($userSession->user_level != 13)
		{
			$userSession->page_no = 0;
		}
		
		//****************************************
		// Start Transporters Level 13
		//****************************************
		switch($userSession->c_parent)
		{
			case 1: //Transporters > Schedule > Schedule detail
				//Already ended	
			break;
			
			case 2: //Transporters > Bookings > Fare Detail confirm
				if($userSession->level_3 == "1")//Booking
				{
					
					$info_Bank = $this->GetSelectedBank($session_msg);
					
					
					{
						$output['session_operation'] = "endcontinue";	
						$output['session_msg'] = "Thank you for choosing ".$userSession->company_name.". Your booking is being processed. You will receive your booking confirmation SMS shortly.";
						
						
						$info_Company = $userSession->Company()->First();
						$db_booking = new Booking;

						$totalFare = 0;
						$channelAmount = 0;
						$gatewayAmount = 0;
						$ticketAmount = $userSession->no_of_traveler*$userSession->fare;

						$discountAmount=0;
						$ActualAmount=$ticketAmount;
						
						if($userSession->usePercent == "1")
						{
							$ticketAmount = $ActualAmount - ($ticketAmount*($userSession->couponpercent/100));
							$discountAmount = $ActualAmount-$ticketAmount;
						}
						else
						{
							$ticketAmount = $ActualAmount - $userSession->couponvalue;
							$discountAmount = $ActualAmount-$ticketAmount;
						}

						$db_booking->discount = $discountAmount;
						
						if($info_Company->gateway_id=="1")//CorelPay
						{
							if($info_Company->gateway_fee_type=="2")//Set fees flat

							{
								$gatewayAmount = $info_Company->gateway_fee;
							}
							else //Percentage of ticket

							{
								$gatewayAmount = ($ticketAmount/100)*$info_Company->gateway_fee;
							}
						}
						
						if($info_Company->channel_fee_type=="1")//Set as percentage of total ticket amount  (e.g. 5% of ticket amount)

						{
							$channelAmount = ($ticketAmount/100)*$info_Company->channel_fee;
						}
						else
						if($info_Company->channel_fee_type=="2")//Set as flat amount per seat (e.g. 100*no of passengers)

						{
							$channelAmount = $userSession->no_of_traveler*$info_Company->channel_fee;
						}
						else //Set as a flat amount on total seats (e.g. 2000)

						{
							$channelAmount = $info_Company->channel_fee;
						}
						
						//Random Str
						$not_unique = true;
						$randomStr = '';
						while($not_unique)
						{
							$randomStr = $this->random_strings(4);
							$info_Refcode = Refcode::Create(['code' => $randomStr]);
							if($info_Refcode)
							{
								$not_unique=false;
							}
						}
						///////////////////////////////////////////////////
						//SAVE BOOKING
						///////////////////////////////////////////////////
						$db_booking->company_id = $userSession->company_id;
						//$db_booking->customer_id = 
						//$db_booking->schedule_id = 
						$db_booking->new_customer = $userSession->new_customer;
						$db_booking->msisdn = $userSession->booking_msisdn;
						$db_booking->TripID = $userSession->tripid;

						$db_booking->SelectedSeats = $userSession->no_of_seats;
						$db_booking->MaxSeat = $userSession->no_of_traveler;
						$db_booking->DestinationID = $userSession->destid;
						$db_booking->OrderID = $userSession->orderid;
						$db_booking->Fullname = $userSession->name;
						$db_booking->email = '';
						$db_booking->nextKin = $userSession->nextkin;
						$db_booking->nextKinPhone = $userSession->nextkinnumber;
						$db_booking->Sex = $userSession->gender;
						//$db_booking->tax = 
						$db_booking->charge_type = $info_Company->charge_type;
						$db_booking->channel_fee = $userSession->channel_fee;
						$db_booking->gateway_fee = $userSession->gateway_fee;
						
						$db_booking->refcode = '732'.$randomStr;
						$db_booking->status = 0;
						
						$db_booking->from_city_id = $userSession->from_city_id;
						$db_booking->from_city_name = $userSession->from_city_name;
						$db_booking->from_terminal_id = $userSession->from_terminal_id;
						$db_booking->from_terminal_name = $userSession->from_terminal_name;
						$db_booking->to_city_id = $userSession->to_city_id;
						$db_booking->to_city_name = $userSession->to_city_name;
						$db_booking->to_terminal_id = $userSession->to_terminal_id;
						$db_booking->to_terminal_name = $userSession->to_terminal_name;
						$db_booking->schedule_date = $userSession->schedule_date;
						$db_booking->dept_time = $userSession->dept_time;
						$db_booking->bus = $userSession->bus;
						
						$db_booking->gateway = $gateway;
						//dd($db_booking);
						$db_booking->save();
						  
						
						///////////////////////////////////////////////////
						//END SAVE BOOKING
						///////////////////////////////////////////////////
						//"\n".$userSession->from_city_name." to ".$userSession->to_city_name.
						if($info_Company->charge_type == "1")//DO NOT ADD channel fee or gateway fee
						{	
							if(($gatewayAmount + $channelAmount) < $info_Company->minimum_fee)
							{
								$commission  = ($info_Company->minimum_fee);// * $userSession->no_of_traveler;
							}
							else
							{
								$commission = ($gatewayAmount + $channelAmount);
							}
							$db_booking->commission = $commission;
							
							$db_booking->sub_total = $ticketAmount;
							$db_booking->total = ($ticketAmount + $discountAmount);
							
							$BankAccount = $this->GetMonifyAccount($userSession->company_id, $db_booking->Fullname, $db_booking->email, $ticketAmount, $db_booking->id);
							$db_booking->traceId = $BankAccount->accountNumber;
							//"\n".$userSession->from_city_name." to ".$userSession->to_city_name.
							$smsMessage = "Dear ".$userSession->name.",\n\nPay with USSD. For ".$info_Bank[0]->name."\ndial *".$info_Bank[0]->ussd_code."*000*732".($randomStr)."# or *BANKUSSD*000*732".($randomStr)."#\n\nPay N".$db_booking->sub_total." with Bank Transfer, POS or ATM to GUO acct: ".$BankAccount->accountNumber.", Wema Bank\n\nRef: ".$userSession->orderid."\n".$userSession->company_name.chr(58)."\n".$userSession->from_terminal_name." to ".$userSession->to_terminal_name."\n".$userSession->schedule_date."\n".$userSession->dept_time."\n".$userSession->bus."\n".$userSession->no_of_traveler." Passenger(s)"."\n"."T.Fee".chr(58)." N".($ticketAmount)."\nSeat No. ".$userSession->no_of_seats."\n\nPlease pay to validate booking. Booking valid for ".$info_Company->seat_block_time."mins.\n\nT.C. - 1.Arrive 30mins before departure 2.Max of 10kg luggage 3.No livestock.\nHotline: +234 706 535 4195";
						}
						else //Exclusive in ticket amount (Total ticket alone)

						{
							if(($gatewayAmount + $channelAmount) < $info_Company->minimum_fee)
							{
								$commission  = ($info_Company->minimum_fee);// * $userSession->no_of_traveler;
							}
							else
							{
								$commission = ($gatewayAmount + $channelAmount);
							}
							$db_booking->commission = $commission;
							$db_booking->sub_total = $ticketAmount;
							$db_booking->total = ($ticketAmount + $commission);
							$BankAccount = $this->GetMonifyAccount($userSession->company_id, $db_booking->Fullname, $db_booking->email, $ticketAmount, $db_booking->id);
							$db_booking->traceId = $BankAccount->accountNumber;
							//"\n".$userSession->from_city_name." to ".$userSession->to_city_name.
							$smsMessage = "Dear ".$userSession->name.",\n\nPay with USSD. For ".$info_Bank[0]->name."\ndial *".$info_Bank[0]->ussd_code."*000*732".($randomStr)."# or *BANKUSSD*000*732".($randomStr)."#\n\nPay N".$db_booking->total." with Bank Transfer, POS or ATM to GUO acct: ".$BankAccount->accountNumber.", Wema Bank\n\nRef: ".$userSession->orderid."\n".$userSession->company_name.chr(58)."\n".$userSession->from_terminal_name." to ".$userSession->to_terminal_name."\n".$userSession->schedule_date."\n".$userSession->dept_time."\n".$userSession->bus."\n".$userSession->no_of_traveler." Passenger(s)"."\n"."T. Fee".chr(58)." N".($ticketAmount)."\nSeat No. ".$userSession->no_of_seats."\n\nPlease pay to validate booking. Booking valid for ".$info_Company->seat_block_time."mins.\n\nT.C. - 1.Arrive 30mins before departure 2.Max of 10kg luggage 3.No livestock.\nHotline: +234 706 535 4195";
						}
						
						\Log::info("Before CallBookingAPI gatewayAmount:".$gatewayAmount." channelAmount:".$channelAmount." charge_type:".$info_Company->charge_type." commission:".$db_booking->commission." sub_total:".$db_booking->sub_total." total:".$db_booking->total." ticketAmount:".$ticketAmount." userSession->fare:".$userSession->fare);
						$db_booking->save();
						//dd($smsMessage);
						dispatch(new CallBookingAPI($userSession));
						
						$this->sendSMS($session_from,$session_msisdn, $smsMessage, $gateway);
						
						$smsMessageNotify = $userSession->name."\n".$session_msisdn."\nRef: ".$userSession->orderid."\n".$userSession->company_name.chr(58)."\n".$userSession->from_terminal_name." to ".$userSession->to_terminal_name."\n".$userSession->schedule_date."\n".$userSession->no_of_traveler." Passengers"."\nSeat No. ".$userSession->no_of_seats."\n\n Payment String: dial *".$info_Bank[0]->ussd_code."*000*732".($randomStr)."#";
						$this->sendSMS($session_from,'2348096029576', $smsMessageNotify, '9mobile');
						$this->sendSMS($session_from,'2349063628281', $smsMessageNotify, 'mtn');
						$this->sendSMS($session_from,'2348066989805', $smsMessageNotify, '9mobile');
					}
					/*else
					{
						$output['session_operation'] = "endcontinue";	
						$output['session_msg'] = "Sorry Your booking was unsuccessful. Please book again";
					}*/
					
					$userSession->gender=$session_msg;				
					$userSession->user_level=13;
					$userSession->keyword=$session_msg;
					$userSession->c_parent_id=$session_msg;
					$userSession->is_child=1;
					$userSession->save();							
				}
				/*else //Status
				{
					$output['session_operation'] = "continue";
					$output['session_msg'] = "Booking Status"."* Back";
				}*/
			break;
			case 3: //Transporters > Company Services
				//Already ended	
			break;
			case 4: //Transporters > Terminals
					//Already ended
			break;
		}
		//****************************************
		// End Transporters
		//****************************************

		return $output;
	}
	
	
	/*
	* /////////////////////////////////////////////////////////////
	* /////////////////////////////////////////////////////////////
	* /////////////////////////////////////////////////////////////
	*
	*	FUNTIONS STARTED
	*
	* /////////////////////////////////////////////////////////////
	* /////////////////////////////////////////////////////////////
	* /////////////////////////////////////////////////////////////
	*/
	public function getCompanyByUssd($ussd_code)
	{
		$info_Company_Ussd = Company_ussd::where('code',ltrim($ussd_code, '*'))->First();
		if($info_Company_Ussd)
			return Company::FindOrFail($info_Company_Ussd->company_id);
		else
			return null;
	}
	public function addCustomerLog($testing, $msisdn, $ussd_code, $is_main, $company_id=null, $company_name=null)
	{
		if($testing == "")
		{
			$info_Customer_Count = Customer_log::where('msisdn',$msisdn)->Get()->Count();
			
			$info_Customer_Log = new Customer_log;
			$info_Customer_Log->msisdn = $msisdn;
			$info_Customer_Log->ussd_code = $ussd_code;
			$info_Customer_Log->channel = 'USSD';
			$info_Customer_Log->is_main = $is_main;
			$info_Customer_Log->start_time = Carbon::now();
			$info_Customer_Log->end_time = Carbon::now();
			
			if($info_Customer_Count==0)
			{
				$info_Customer_Log->is_new = 1;
			}
			
			if($company_id!=null)
			{
				$info_Customer_Log->company_id = $company_id;
				$info_Customer_Log->company_name = $company_name;
			}
			
			$info_Customer_Log->save();
			
			return $info_Customer_Log->id;
		}
		else
		{
			return 0;
		}
	}
	
	public function updateCustomerLog($testing, $sql)
	{
		if($testing == "")
		{
			$Results = DB::Select($sql);
		}
	}
	function curl_min($url)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}
	
	function isDate($date)
	{
		$matches = array();
		$pattern = '/^([0-9]{1,2})\\/([0-9]{1,2})\\/([0-9]{4})$/';
		if (!preg_match($pattern, $date, $matches)) return 0;
		if (!checkdate($matches[2], $matches[1], $matches[3])) return 0;
		return 1;
	}
	
	function convertToObject($array) {
        $mainArray = array();
		foreach ($array as $key => $value) {
            $mainArray[] =  $this->convertSingleArrayToObject($value);
        }	
        return $mainArray;
    }

	function convertSingleArrayToObject($array) {
        $object = new \stdClass();
		foreach ($array as $key => $value) {
            $object->$key = $value;
        }
        return $object;
    }
	// This function will return a random 
	// string of specified length 
	function random_strings($length_of_string) 
	{ 
		// String of all alphanumeric character 
		$str_result = '0123456789'; 
		// Shufle the $str_result and returns substring 
		// of specified length 
		return substr(str_shuffle($str_result), 0, $length_of_string); 
	} 
	
	// string of specified length 
	function AvailableSeatPaging($seatList, $blockList, $page_no) 
	{ 
		//dd($seatList);
		$infoSeatsList = array();
		$newSeatsList = array_diff($seatList,$blockList);
		for($i=$page_no-1; $i<$page_no+19; $i++)
		{
			if(isset($newSeatsList[$i]))
			{
				array_push($infoSeatsList, $seatList[$i]);
			}
		}
		//dd($infoSeatsList);
		return $infoSeatsList;
	} 
	public function GetScheduleSearch($msisdn,$page)
	{	
		$sql = "select * from (SELECT c.from_city_id, c.from_city_name, c.from_terminal_id, c.from_terminal_name, c.to_terminal_id, c.to_terminal_name, c.to_city_id, c.to_city_name, @rownum := @rownum + 1 AS rownum FROM ussd_menu_schedule_search c, (SELECT @rownum := 0) r where msisdn='".$msisdn."' and schedule_date>DATE(NOW()) order by record_on desc) data limit 4 offset ".$page;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	public function GetSelectedScheduleSearch($msisdn,$rownum)
	{
		$sql = "select * from (SELECT c.*, @rownum := @rownum + 1 AS rownum FROM ussd_menu_schedule_search c, (SELECT @rownum := 0) r where msisdn='".$msisdn."' and schedule_date>DATE(NOW()) order by record_on desc) data where rownum=".$rownum;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	public function GetActiveBooking($msisdn, $page)
	{
		$sql = "select * from (SELECT c.*, @rownum := @rownum + 1 AS rownum FROM bookings c, (SELECT @rownum := 0) r where msisdn=".$msisdn." and status=1 order by updated_at desc) data limit 4 offset ".$page;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	
	public function GetSelectedActiveBooking($msisdn,$rownum)
	{
		$sql = "select * from (SELECT c.*, @rownum := @rownum + 1 AS rownum FROM bookings c, (SELECT @rownum := 0) r where msisdn=".$msisdn." and status=1 order by updated_at desc) data where rownum=".$rownum;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	public function GetLastSelectedScheduleSearch($msisdn,$rownum)
	{
		$sql = "SELECT c.*, @rownum := @rownum + 1 AS rownum FROM ussd_menu_schedule_search c, (SELECT @rownum := 0) r where msisdn='".$msisdn."' and schedule_date>DATE(NOW()) order by record_on desc limit 1";
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	
	/*
	***********************
	GetCompanyCities
	***********************
	*/
	public function GetCompanyCities($userSession,$page)
	{	
		if($userSession->company_id == "6")
		{
			$per_page = 7;
			$url = env('API_URL')."AllRouteDetails/".$userSession->company_id;
			$options = array(
			  'http'=>array(
				'method'=>"GET",
				'header'=>"Authorization: Bearer ".env('API_TOKEN')
			  )
			);
			//dd($url);
			$context = stream_context_create($options);
			$data = file_get_contents($url, false, $context);
			
			$decodeData = json_decode($data, true);
			
			//echo "<pre>";
			//print_r($decodeData);
			//echo "</pre>";
			//print($decodeData["data"][9]["StateID"])."<br>";
			$Response = array();
			$row = 1;
			$nOfRecords = 1;
			//echo "$row>=$page && $nOfRecords<=3<br />";
			foreach($decodeData["data"]["States"] as $index => $Cities)
			{
				if($row>$page && $nOfRecords<=$per_page)
				{
					//echo ($index+1).". ".$Cities["StateName"]."<br>";
					$Response[] = array('rownum' => ($index+1),
						'id' => $Cities["StateID"], 
						'name' => $Cities["StateName"],  
						'short_name' => $Cities["StateName"], 
					);
					$nOfRecords ++;
				}
				$row++;
			}    
			//dd($Response);
			if(sizeof($Response) == 0)
				return [];
			else
				return $this->convertToObject($Response);
		}
		else
		{
			if($userSession->company_ussd_type == "2")
			{
				$info_Company = $userSession->Company()->First();
				$baseURL = 	$info_Company->api_url;
				$apiToken = $info_Company->api_token;
			}
			else
			{
				$baseURL = 	env('API_URL');
				$apiToken = env('API_TOKEN');
			}
			
			$per_page = 7;
			//echo "<br />Call GetSelectedCompanyCityAPI<br />";
			$url = $baseURL."Cities/".$userSession->company_id;
			$options = array(
			  'http'=>array(
				'method'=>"GET",
				'header'=>"Authorization: Bearer ".$apiToken
			  )
			);
			
			$context = stream_context_create($options);
			$data = file_get_contents($url, false, $context);
			$decodeData = json_decode($data, true);
			
			$row = 1;
			$nOfRecords = 1;
			$Response = array();
			foreach($decodeData as $index => $Cities)
			{
				if($row>$page && $nOfRecords<=$per_page)
				{
					//echo ($index+1).". ".$Cities["StateName"]."<br>";
					$Response[] = array('rownum' => ($index+1),
						'id' => $Cities["id"], 
						'name' => $Cities["name"],  
						'short_name' => $Cities["short_name"], 
					);	
					$nOfRecords++;
				}
				$row++;
			}   
			
			//dd($Response);
			if(sizeof($Response) == 0)
				return [];
			else
				return $this->convertToObject($Response);
		}
	}
	/*
	***********************
	GetSelectedCompanyCity
	***********************
	*/
	public function GetSelectedCompanyCity($userSession,$rownum,$city_id=null)
	{
		if($userSession->company_id == "6")
		{
			$per_page = 7;
			$url = env('API_URL')."AllRouteDetails/".$userSession->company_id;
			$options = array(
			  'http'=>array(
				'method'=>"GET",
				'header'=>"Authorization: Bearer ".env('API_TOKEN')
			  )
			);
			//dd($url);
			$context = stream_context_create($options);
			$data = file_get_contents($url, false, $context);
			
			if($city_id != null)
			{
				$Response = array();
				foreach($decodeData["data"]["States"] as $index => $Cities)
				{
					if($Cities["StateID"] == $city_id)
					{
						//echo ($index+1).". ".$Cities["StateName"]."<br>";
						$Response[] = array('rownum' => ($index+1),
							'id' => $Cities["StateID"], 
							'name' => $Cities["StateName"],  
							'short_name' => $Cities["StateName"], 
						);
					}
				}   
			}
			else
			{
				$decodeData = json_decode($data, true);
				
				$Response = array();
				$Response[] = array('rownum' => (1),
				'id' => $decodeData["data"]["States"][($rownum-1)]["StateID"], 
				'name' => $decodeData["data"]["States"][($rownum-1)]["StateName"],  
				'short_name' => $decodeData["data"]["States"][($rownum-1)]["StateName"], 
				);
			}
			
			if(sizeof($Response) == 0)
				return [];
			else
				return $this->convertToObject($Response);
		}
		else
		{
			if($userSession->company_ussd_type == "2")
			{
				$info_Company = $userSession->Company()->First();
				$baseURL = 	$info_Company->api_url;
				$apiToken = $info_Company->api_token;
			}
			else
			{
				$baseURL = 	env('API_URL');
				$apiToken = env('API_TOKEN');
			}
			
			$per_page = 7;
			//echo "<br />Call GetSelectedCompanyCityAPI<br />";
			$url = $baseURL."Cities/".$userSession->company_id;
			$options = array(
			  'http'=>array(
				'method'=>"GET",
				'header'=>"Authorization: Bearer ".$apiToken
			  )
			);
			
			$context = stream_context_create($options);
			$data = file_get_contents($url, false, $context);
			$decodeData = json_decode($data, true);
			
			if($city_id != null)
			{
				$Response = array();
				foreach($decodeData as $index => $Cities)
				{
					if($Cities["id"] == $city_id)
					{
						//echo ($index+1).". ".$Cities["StateName"]."<br>";
						$Response[] = array('rownum' => ($index+1),
							'id' => $Cities["id"], 
							'name' => $Cities["name"],  
							'short_name' => $Cities["short_name"], 
						);	
						$nOfRecords++;
					}
					$row++;
				}   
			}
			else
			{
				$Response = array();
				$Response[] = array('rownum' => ($index+1),
					'id' => $decodeData[($rownum-1)]["id"], 
					'name' => $decodeData[($rownum-1)]["name"],  
					'short_name' => $decodeData[($rownum-1)]["short_name"], 
				);	
			}
				
			if(sizeof($Response) == 0)
				return [];
			else
				return $this->convertToObject($Response);
		}
	}
	/*
	***********************
	GetCompanyToCities
	***********************
	*/
	public function GetCompanyToCities($userSession,$from_city_id,$page)
	{	
		if($userSession->company_id == "6")
		{
			$per_page = 7;
			$url = env('API_URL')."AllRouteDetails/".$userSession->company_id;
			$options = array(
			  'http'=>array(
				'method'=>"GET",
				'header'=>"Authorization: Bearer ".env('API_TOKEN')
			  )
			);
			//dd($url);
			$context = stream_context_create($options);
			$data = file_get_contents($url, false, $context);
			
			$decodeData = json_decode($data, true);
			
			//echo "<pre>";
			//print_r($decodeData);
			//echo "</pre>";
			//print($decodeData["data"][9]["StateID"])."<br>";
			$Response = array();
			$row = 1;
			$nOfRecords = 1;
			//echo "$row>=$page && $nOfRecords<=3<br />";
			foreach($decodeData["data"]["States"] as $index => $Cities)
			{
				if($from_city_id != $Cities["StateID"])//Exclude selected from city
				{
					if($row>$page && $nOfRecords<=$per_page)
					{
						//echo ($index+1).". ".$Cities["StateName"]."<br>";
						$Response[] = array('rownum' => ($index+1),
							'id' => $Cities["StateID"], 
							'name' => $Cities["StateName"],  
							'short_name' => $Cities["StateName"], 
						);
						$nOfRecords ++;
					}
					$row++;
				}
			}    
			//dd($Response);
			if(sizeof($Response) == 0)
				return [];
			else
				return $this->convertToObject($Response);
		}
		else
		{
			if($userSession->company_ussd_type == "2")
			{
				$info_Company = $userSession->Company()->First();
				$baseURL = 	$info_Company->api_url;
				$apiToken = $info_Company->api_token;
			}
			else
			{
				$baseURL = 	env('API_URL');
				$apiToken = env('API_TOKEN');
			}
			
			$per_page = 7;
			//echo "<br />Call GetSelectedCompanyCityAPI<br />";
			$url = $baseURL."Cities/".$userSession->company_id;
			$options = array(
			  'http'=>array(
				'method'=>"GET",
				'header'=>"Authorization: Bearer ".$apiToken
			  )
			);
			
			$context = stream_context_create($options);
			$data = file_get_contents($url, false, $context);
			$decodeData = json_decode($data, true);
			
			$row = 1;
			$nOfRecords = 1;
			$Response = array();
			foreach($decodeData as $index => $Cities)
			{
				if($from_city_id != $Cities["id"])//Exclude selected from city
				{
					if($row>$page && $nOfRecords<=$per_page)
					{
						//echo ($index+1).". ".$Cities["StateName"]."<br>";
						$Response[] = array('rownum' => ($index+1),
							'id' => $Cities["id"], 
							'name' => $Cities["name"],  
							'short_name' => $Cities["short_name"], 
						);	
						$nOfRecords++;
					}
					$row++;
				}
			}   
			
			//dd($Response);
			if(sizeof($Response) == 0)
				return [];
			else
				return $this->convertToObject($Response);
		}
	}
	/*
	***********************
	GetSelectedCompanyToCity
	***********************
	*/
	public function GetSelectedCompanyToCity($userSession,$from_city_id,$rownum, $city_id=null)
	{
		if($userSession->company_id == "6")
		{
			$per_page = 7;
			$url = env('API_URL')."AllRouteDetails/".$userSession->company_id;
			$options = array(
			  'http'=>array(
				'method'=>"GET",
				'header'=>"Authorization: Bearer ".env('API_TOKEN')
			  )
			);
			//dd($url);
			$context = stream_context_create($options);
			$data = file_get_contents($url, false, $context);
			
			if($city_id != null)
			{
				$Response = array();
				foreach($decodeData["data"]["States"] as $index => $Cities)
				{
					if($Cities["StateID"] == $city_id)
					{
						//echo ($index+1).". ".$Cities["StateName"]."<br>";
						$Response[] = array('rownum' => ($index+1),
							'id' => $Cities["StateID"], 
							'name' => $Cities["StateName"],  
							'short_name' => $Cities["StateName"], 
						);
					}
				}   
			}
			else
			{
				$Response = array();
				foreach($decodeData["data"]["States"] as $index => $Cities)
				{
					if($from_city_id != $Cities["StateID"])//Exclude selected from city
					{
						if($rownum == $row)
						{
							//echo ($index+1).". ".$Cities["StateName"]."<br>";
							$Response[] = array('rownum' => ($index+1),
								'id' => $Cities["StateID"], 
								'name' => $Cities["StateName"],  
								'short_name' => $Cities["StateName"], 
							);
						}
					}
				} 
			}
			
			if(sizeof($Response) == 0)
				return [];
			else
				return $this->convertToObject($Response);
		}
		else
		{
			if($userSession->company_ussd_type == "2")
			{
				$info_Company = $userSession->Company()->First();
				$baseURL = 	$info_Company->api_url;
				$apiToken = $info_Company->api_token;
			}
			else
			{
				$baseURL = 	env('API_URL');
				$apiToken = env('API_TOKEN');
			}
			
			$per_page = 7;
			//echo "<br />Call GetSelectedCompanyCityAPI<br />";
			$url = $baseURL."Cities/".$userSession->company_id;
			$options = array(
			  'http'=>array(
				'method'=>"GET",
				'header'=>"Authorization: Bearer ".$apiToken
			  )
			);
			
			$context = stream_context_create($options);
			$data = file_get_contents($url, false, $context);
			$decodeData = json_decode($data, true);
			
			if($city_id != null)
			{
				$Response = array();
				foreach($decodeData as $index => $Cities)
				{
					if($Cities["id"] == $city_id)
					{
						//echo ($index+1).". ".$Cities["StateName"]."<br>";
						$Response[] = array('rownum' => ($index+1),
							'id' => $Cities["id"], 
							'name' => $Cities["name"],  
							'short_name' => $Cities["short_name"], 
						);	
						$nOfRecords++;
					}
				}   
			}
			else
			{
				$Response = array();
				foreach($decodeData as $index => $Cities)
				{
					if($from_city_id != $Cities["StateID"])//Exclude selected from city
					{
						if($rownum == $row)
						{
							//echo ($index+1).". ".$Cities["StateName"]."<br>";
							$Response[] = array('rownum' => ($index+1),
								'id' => $Cities["id"], 
								'name' => $Cities["name"],  
								'short_name' => $Cities["short_name"], 
							);	
							$nOfRecords++;
						}
					}
				} 
			}
				
			if(sizeof($Response) == 0)
				return [];
			else
				return $this->convertToObject($Response);
		}
	}
	/*
	***********************
	GetFromTerminals
	***********************
	*/
	public function GetFromTerminals($userSession, $city_id, $to_city_id, $page)
	{
		if($userSession->company_id == "6")
		{
			//echo "<br />Call GetFromTerminalsAPI<br />";
			$per_page = 7;
			$url = env('API_URL')."AllRouteDetails/".$userSession->company_id;
			$options = array(
			  'http'=>array(
				'method'=>"GET",
				'header'=>"Authorization: Bearer ".env('API_TOKEN')
			  )
			);
			//dd($url);
			$context = stream_context_create($options);
			$data = file_get_contents($url, false, $context);
			
			$decodeData = json_decode($data, true);
			
			//Terminal With State
			$urlTs = env('API_URL')."TerminalsWithState/".$userSession->company_id;
			$optionsTs = array(
			  'http'=>array(
				'method'=>"GET",
				'header'=>"Authorization: Bearer ".env('API_TOKEN')
			  )
			);
			
			$contextTs = stream_context_create($options);
			$dataTs = file_get_contents($urlTs, false, $contextTs);
			
			$decodeDataTs = json_decode($dataTs, true);
			
			$Response = array();
			$ResponseArr = array();
			$row = 1;
			$nOfRecords = 1;
			//echo "$row>=$page && $nOfRecords<=3<br />";
			
			foreach($decodeData["data"]["Terminals"] as $index => $Terminals)
			{
				//echo "$city_id ".$Terminals["StateID"]."<br />";
				if($city_id == $Terminals["StateID"])
				{
					$ResponseArr[] = array('rownum' => $row,
						'id' => $Terminals["LoadinID"], 
						'name' => $Terminals["Loading_Office"],  
						'short_name' => $Terminals["Loading_Office"], 
						'address' => $this->ParseTerminalData($decodeDataTs['data'], $Terminals["LoadinID"], 'TerminalAddress'),
						'email' => $this->ParseTerminalData($decodeDataTs['data'], $Terminals["LoadinID"], 'Terminal'),
						'phone_number' => $this->ParseTerminalData($decodeDataTs['data'], $Terminals["LoadinID"], 'TerminalPhone')
					);
					$nOfRecords ++;
					$row++;
				}
				
			}  
			//dd($ResponseArr);
			$row = 1;
			$nOfRecords = 1;
			foreach($ResponseArr as $index => $Terminals)
			{
				//echo "$row>$page && $nOfRecords<=$per_page<br />";
				if($row>$page && $nOfRecords<=$per_page)
				{
					$Response[] = array('rownum' => ($index+1),
						'id' => $Terminals["id"], 
						'name' => $Terminals["name"],  
						'short_name' => $Terminals["short_name"], 
					);
					$nOfRecords++;
				}
				$row++;
			}
			//if($page>0)
			//dd($Response);
			if(sizeof($Response) == 0)
				return [];
			else
				return $this->convertToObject($Response);
		}
		else
		{
			if($userSession->company_ussd_type == "2")
			{
				$info_Company = $userSession->Company()->First();
				$baseURL = 	$info_Company->api_url;
				$apiToken = $info_Company->api_token;
			}
			else
			{
				$baseURL = 	env('API_URL');
				$apiToken = env('API_TOKEN');
			}
			
			$per_page = 7;
			//echo "<br />Call GetSelectedCompanyCityAPI<br />";
			$url = $baseURL."CityTerminals/".$userSession->company_id."?CityID=".$city_id;
			$options = array(
			  'http'=>array(
				'method'=>"GET",
				'header'=>"Authorization: Bearer ".$apiToken
			  )
			);
			
			$context = stream_context_create($options);
			$data = file_get_contents($url, false, $context);
			$decodeData = json_decode($data, true);
			
			$row = 1;
			$nOfRecords = 1;
			$Response = array();
			foreach($decodeData as $index => $Terminals)
			{
				if($row>$page && $nOfRecords<=$per_page)
				{
					//echo ($index+1).". ".$Cities["StateName"]."<br>";
					$Response[] = array('rownum' => ($index+1),
						'id' => $Terminals["id"], 
						'name' => $Terminals["name"],  
						'short_name' => $Terminals["short_name"], 
					);	
					$nOfRecords++;
				}
				$row++;
			}   
			
			//dd($Response);
			if(sizeof($Response) == 0)
				return [];
			else
				return $this->convertToObject($Response);
		}
	}
	/*
	***********************
	GetSelectedFromTerminal
	***********************
	*/
	public function ParseTerminalData($data, $terminal_id, $column)
	{
		foreach($data as $Terminal)
		{
			if($Terminal['TerminalID'] == $terminal_id)
			{
				return $Terminal[$column];
			}
		}
	}
	public function GetSelectedFromTerminal($userSession, $city_id, $to_city_id, $rownum, $terminal_id = null)
	{
		if($userSession->company_id == "6")
		{
			//echo "<br />Call GetFromTerminalsAPI<br />";
			$per_page = 7;
			$url = env('API_URL')."AllRouteDetails/".$userSession->company_id;
			$options = array(
			  'http'=>array(
				'method'=>"GET",
				'header'=>"Authorization: Bearer ".env('API_TOKEN')
			  )
			);
			//dd($url);
			$context = stream_context_create($options);
			$data = file_get_contents($url, false, $context);
			
			$decodeData = json_decode($data, true);
			
			//Terminal With State
			$urlTs = env('API_URL')."TerminalsWithState/".$userSession->company_id;
			$optionsTs = array(
			  'http'=>array(
				'method'=>"GET",
				'header'=>"Authorization: Bearer ".env('API_TOKEN')
			  )
			);
			
			$contextTs = stream_context_create($options);
			$dataTs = file_get_contents($urlTs, false, $contextTs);
			
			$decodeDataTs = json_decode($dataTs, true);
			
			
			$Response = array();
			$ResponseArr = array();
			$row = 1;
			$nOfRecords = 1;
			//echo "$row>=$page && $nOfRecords<=3<br />";
			
			foreach($decodeData["data"]["Terminals"] as $index => $Terminals)
			{
				//echo "$city_id ".$Terminals["StateID"]."<br />";
				if($city_id == $Terminals["StateID"])
				{
					$ResponseArr[] = array('rownum' => $row,
						'id' => $Terminals["LoadinID"], 
						'name' => $Terminals["Loading_Office"],  
						'short_name' => $Terminals["Loading_Office"], 
					);
					$nOfRecords ++;
					$row++;
				}
				
			}  
			//dd($ResponseArr);
			
			if($terminal_id != null)
			{
				$row = 1;
				$nOfRecords = 1;
				foreach($ResponseArr as $index => $Terminals)
				{
					//dd($Terminals);
					//echo "$row>$page && $nOfRecords<=$per_page<br />";
					if($terminal_id == $Terminals["LoadinID"])
					{
						$Response[] = array('rownum' => ($index+1),
							'id' => $Terminals["id"], 
							'name' => $Terminals["name"],  
							'short_name' => $Terminals["short_name"], 
							'address' => $this->ParseTerminalData($decodeDataTs['data'], $Terminals["LoadinID"], 'TerminalAddress'),
							'email' => $this->ParseTerminalData($decodeDataTs['data'], $Terminals["LoadinID"], 'Terminal'),
							'phone_number' => $this->ParseTerminalData($decodeDataTs['data'], $Terminals["LoadinID"], 'TerminalPhone')
						);
						$nOfRecords++;
					}
					$row++;
				}

			}
			else
			{
				$row = 1;
				$nOfRecords = 1;
				foreach($decodeData as $index => $Terminals)
				{
					//echo "$rownum == $row<br />";
					if($rownum == $row)
					{
						//dd($Terminals);
						$Response[] = array('rownum' => ($index+1),
							'id' => $Terminals["id"], 
							'name' => $Terminals["name"],  
							'short_name' => $Terminals["short_name"], 
							'address' => $this->ParseTerminalData($decodeDataTs['data'], $Terminals["LoadinID"], 'TerminalAddress'),
							'email' => $this->ParseTerminalData($decodeDataTs['data'], $Terminals["LoadinID"], 'Terminal'),
							'phone_number' => $this->ParseTerminalData($decodeDataTs['data'], $Terminals["LoadinID"], 'TerminalPhone')
						);
						$nOfRecords++;
					}
					$row++;
				}
			}
			//if($page>0)
			//dd($Response);
			if(sizeof($Response) == 0)
				return [];
			else
				return $this->convertToObject($Response);
		}
		else
		{
			if($userSession->company_ussd_type == "2")
			{
				$info_Company = $userSession->Company()->First();
				$baseURL = 	$info_Company->api_url;
				$apiToken = $info_Company->api_token;
			}
			else
			{
				$baseURL = 	env('API_URL');
				$apiToken = env('API_TOKEN');
			}
			
			$per_page = 7;
			//echo "<br />Call GetSelectedCompanyCityAPI<br />";
			$url = $baseURL."CityTerminals/".$userSession->company_id."?CityID=".$city_id;
			$options = array(
			  'http'=>array(
				'method'=>"GET",
				'header'=>"Authorization: Bearer ".$apiToken
			  )
			);
			
			$context = stream_context_create($options);
			$data = file_get_contents($url, false, $context);
			$decodeData = json_decode($data, true);
			
			if($terminal_id != null)
			{
				$row = 1;
				$nOfRecords = 1;
				$Response = array();
				foreach($decodeData as $index => $Terminals)
				{
					if($terminal_id == $Terminals["id"])
					{
						//echo ($index+1).". ".$Cities["StateName"]."<br>";
						$Response[] = array('rownum' => ($index+1),
							'id' => $Terminals["id"], 
							'name' => $Terminals["name"],  
							'short_name' => $Terminals["short_name"], 
						);	
						$nOfRecords++;
					}
					$row++;
				}   

			}
			else
			{
				$row = 1;
				$nOfRecords = 1;
				$Response = array();
				foreach($decodeData as $index => $Terminals)
				{
					if($rownum == $row)
					{
						//echo ($index+1).". ".$Cities["StateName"]."<br>";
						$Response[] = array('rownum' => ($index+1),
							'id' => $Terminals["id"], 
							'name' => $Terminals["name"],  
							'short_name' => $Terminals["short_name"], 
						);	
						$nOfRecords++;
					}
					$row++;
				}   
			}
			//dd($Response);
			if(sizeof($Response) == 0)
				return [];
			else
				return $this->convertToObject($Response);
		}
	}
	
	/*
	***********************
	GetToTerminals
	***********************
	*/
	public function GetToTerminals($userSession, $city_id, $to_city_id, $from_terminal, $page)
	{
		if($userSession->company_id == "6")
		{
			//echo "<br />Call GetToTerminals<br />";
			$per_page = 7;
			$url = env('API_URL')."AllRouteDetails/".$company_id;
			$options = array(
			  'http'=>array(
				'method'=>"GET",
				'header'=>"Authorization: Bearer ".env('API_TOKEN')
			  )
			);
			//dd($url);
			$context = stream_context_create($options);
			$data = file_get_contents($url, false, $context);
			
			$decodeData = json_decode($data, true);
			
			//Terminal With State
			$urlTs = env('API_URL')."TerminalsWithState/".$company_id;
			$optionsTs = array(
			  'http'=>array(
				'method'=>"GET",
				'header'=>"Authorization: Bearer ".env('API_TOKEN')
			  )
			);
			
			$contextTs = stream_context_create($options);
			$dataTs = file_get_contents($urlTs, false, $contextTs);
			
			$decodeDataTs = json_decode($dataTs, true);
			
			$Response = array();
			$ResponseArr = array();
			$row = 1;
			$nOfRecords = 1;
			//echo "$row>=$page && $nOfRecords<=3<br />";
			
			$fromLoadingDestinationIDArray = array();
			foreach($decodeData["data"]["Routes"] as $index => $Routes)
			{
				if($Routes["LoadinID"] == $from_terminal)
				{
					array_push($fromLoadingDestinationIDArray, $Routes["DestinationID"]);
				}
			}
			
			//Get to city terminal
			$ToTerminalsDestination = array();
			foreach($decodeData["data"]["Destinations"] as $index => $Destinations)
			{
				if($to_city_id == $Destinations["StateID"])
				{
					array_push($ToTerminalsDestination, $Destinations["DestinationID"]);
				}
			} 
			
			//dd($fromLoadingDestinationIDArray);
			foreach($decodeData["data"]["Destinations"] as $index => $Destinations)
			{
				//print_r($Terminals);
				if(in_array($Destinations["DestinationID"], $fromLoadingDestinationIDArray) && in_array($Destinations["DestinationID"], $ToTerminalsDestination))
				{
					//dd($Destinations);
					$ResponseArr[] = array('rownum' => $row,
						'id' => $Destinations["DestinationID"], 
						'name' => $Destinations["Destination"],  
						'short_name' => $Destinations["Destination"], 
						'address' => $this->ParseTerminalData($decodeDataTs['data'], $Terminals["LoadinID"], 'TerminalAddress'),
						'email' => $this->ParseTerminalData($decodeDataTs['data'], $Terminals["LoadinID"], 'Terminal'),
						'phone_number' => $this->ParseTerminalData($decodeDataTs['data'], $Terminals["LoadinID"], 'TerminalPhone')
					);
					$nOfRecords ++;
					$row++;
				}
				
			}    
			$row = 1;
			$nOfRecords = 1;
			foreach($ResponseArr as $index => $Terminals)
			{
				if($row>$page && $nOfRecords<=$per_page)
				{
					$Response[] = array('rownum' => ($index+1),
						'id' => $Terminals["id"], 
						'name' => $Terminals["name"],  
						'short_name' => $Terminals["short_name"], 
					);
					$nOfRecords++;
				}
				$row++;
			}
			//dd($Response);
			if(sizeof($Response) == 0)
				return [];
			else
				return $this->convertToObject($Response);
		}
		else
		{
			if($userSession->company_ussd_type == "2")
			{
				$info_Company = $userSession->Company()->First();
				$baseURL = 	$info_Company->api_url;
				$apiToken = $info_Company->api_token;
			}
			else
			{
				$baseURL = 	env('API_URL');
				$apiToken = env('API_TOKEN');
			}
			
			$per_page = 7;
			//echo "<br />Call GetSelectedCompanyCityAPI<br />";
			$url = $baseURL."CityTerminals/".$userSession->company_id."?CityID=".$city_id;
			$options = array(
			  'http'=>array(
				'method'=>"GET",
				'header'=>"Authorization: Bearer ".$apiToken
			  )
			);
			
			$context = stream_context_create($options);
			$data = file_get_contents($url, false, $context);
			$decodeData = json_decode($data, true);
			
			$row = 1;
			$nOfRecords = 1;
			$Response = array();
			foreach($decodeData as $index => $Terminals)
			{
				if($from_terminal != $Terminals["id"])
				{
					if($row>$page && $nOfRecords<=$per_page)
					{
						//echo ($index+1).". ".$Cities["StateName"]."<br>";
						$Response[] = array('rownum' => ($index+1),
							'id' => $Terminals["id"], 
							'name' => $Terminals["name"],  
							'short_name' => $Terminals["short_name"], 
						);	
						$nOfRecords++;
					}
					$row++;
				}
			}   
			
			//dd($Response);
			if(sizeof($Response) == 0)
				return [];
			else
				return $this->convertToObject($Response);
		}
	}
	
	/*
	***********************
	GetSelectedToTerminals
	***********************
	*/
	public function GetSelectedToTerminals($userSession, $city_id, $to_city_id, $from_terminal, $rownum, $terminal_id=null)
	{
		if($userSession->company_id == "6")
		{
			//echo "<br />Call GetSelectedToTerminals<br />";
			$per_page = 7;
			$url = env('API_URL')."AllRouteDetails/".$company_id;
			$options = array(
			  'http'=>array(
				'method'=>"GET",
				'header'=>"Authorization: Bearer ".env('API_TOKEN')
			  )
			);
			//dd($url);
			$context = stream_context_create($options);
			$data = file_get_contents($url, false, $context);
			
			$decodeData = json_decode($data, true);
			
			//Terminal With State
			$urlTs = env('API_URL')."TerminalsWithState/".$company_id;
			$optionsTs = array(
			  'http'=>array(
				'method'=>"GET",
				'header'=>"Authorization: Bearer ".env('API_TOKEN')
			  )
			);
			
			$contextTs = stream_context_create($options);
			$dataTs = file_get_contents($urlTs, false, $contextTs);
			
			$decodeDataTs = json_decode($dataTs, true);
			
			$Response = array();
			$ResponseArr = array();
			$row = 1;
			$nOfRecords = 1;
			//echo "$row>=$page && $nOfRecords<=3<br />";
			
			$fromLoadingDestinationIDArray = array();
			foreach($decodeData["data"]["Routes"] as $index => $Routes)
			{
				if($Routes["LoadinID"] == $from_terminal)
				{
					array_push($fromLoadingDestinationIDArray, $Routes["DestinationID"]);
				}
			}
			
			//Get to city terminal
			$ToTerminalsDestination = array();
			foreach($decodeData["data"]["Destinations"] as $index => $Destinations)
			{
				if($to_city_id == $Destinations["StateID"])
				{
					array_push($ToTerminalsDestination, $Destinations["DestinationID"]);
				}
			} 
			
			//dd($fromLoadingDestinationIDArray);
			foreach($decodeData["data"]["Destinations"] as $index => $Destinations)
			{
				//print_r($Terminals);
				if(in_array($Destinations["DestinationID"], $fromLoadingDestinationIDArray) && in_array($Destinations["DestinationID"], $ToTerminalsDestination))
				{
					//dd($Destinations);
					$ResponseArr[] = array('rownum' => $row,
						'id' => $Destinations["DestinationID"], 
						'name' => $Destinations["Destination"],  
						'short_name' => $Destinations["Destination"], 
						'address' => $this->ParseTerminalData($decodeDataTs['data'], $Terminals["LoadinID"], 'TerminalAddress'),
						'email' => $this->ParseTerminalData($decodeDataTs['data'], $Terminals["LoadinID"], 'Terminal'),
						'phone_number' => $this->ParseTerminalData($decodeDataTs['data'], $Terminals["LoadinID"], 'TerminalPhone')
					);
					$nOfRecords ++;
					$row++;
				}
				
			}  
			
			if($terminal_id != null)
			{
				$row = 1;
				$nOfRecords = 1;
				foreach($ResponseArr as $index => $Terminals)
				{
					if($terminal_id == $Terminals["id"])
					{
						$Response[] = array('rownum' => ($index+1),
							'id' => $Terminals["id"], 
							'name' => $Terminals["name"],  
							'short_name' => $Terminals["short_name"], 
						);
						$nOfRecords++;
					}
					$row++;
				}

			}
			else
			{
				$row = 1;
				$nOfRecords = 1;
				foreach($ResponseArr as $index => $Terminals)
				{
					if($row == $rownum)
					{
						$Response[] = array('rownum' => ($index+1),
							'id' => $Terminals["id"], 
							'name' => $Terminals["name"],  
							'short_name' => $Terminals["short_name"], 
						);
						$nOfRecords++;
					}
					$row++;
				}
			}
			//dd($Response);
			if(sizeof($Response) == 0)
				return [];
			else
				return $this->convertToObject($Response);
		}
		else
		{
			if($userSession->company_ussd_type == "2")
			{
				$info_Company = $userSession->Company()->First();
				$baseURL = 	$info_Company->api_url;
				$apiToken = $info_Company->api_token;
			}
			else
			{
				$baseURL = 	env('API_URL');
				$apiToken = env('API_TOKEN');
			}
			
			$per_page = 7;
			//echo "<br />Call GetSelectedCompanyCityAPI<br />";
			$url = $baseURL."CityTerminals/".$userSession->company_id."?CityID=".$city_id;
			$options = array(
			  'http'=>array(
				'method'=>"GET",
				'header'=>"Authorization: Bearer ".$apiToken
			  )
			);
			
			$context = stream_context_create($options);
			$data = file_get_contents($url, false, $context);
			$decodeData = json_decode($data, true);
			
			if($terminal_id != null)
			{
				$row = 1;
				$nOfRecords = 1;
				$Response = array();
				foreach($decodeData as $index => $Terminals)
				{
					if($from_terminal != $Terminals["id"])
					{
						if($terminal_id == $Terminals["id"])
						{
							//echo ($index+1).". ".$Cities["StateName"]."<br>";
							$Response[] = array('rownum' => ($index+1),
								'id' => $Terminals["id"], 
								'name' => $Terminals["name"],  
								'short_name' => $Terminals["short_name"], 
							);	
							$nOfRecords++;
						}
						$row++;
					}
				}   

			}
			else
			{
				$row = 1;
				$nOfRecords = 1;
				$Response = array();
				foreach($decodeData as $index => $Terminals)
				{
					if($from_terminal != $Terminals["id"])
					{
						if($row == $rownum)
						{
							//echo ($index+1).". ".$Cities["StateName"]."<br>";
							$Response[] = array('rownum' => ($index+1),
								'id' => $Terminals["id"], 
								'name' => $Terminals["name"],  
								'short_name' => $Terminals["short_name"], 
							);	
							$nOfRecords++;
						}
						$row++;
					}
				}   
			}
			//dd($Response);
			if(sizeof($Response) == 0)
				return [];
			else
				return $this->convertToObject($Response);
		}
	}
}
