<?php

namespace App\Repositories;

use App\Models\Policy;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Hash;

class PolicyRepository {
	
	/**
	 * @var App\Models\Policy
	 */
	protected $db_policy;
		
    public function __construct(Policy $db_policy) 
    {
        $this->db_policy = $db_policy;
    }
	
	public function addPolicy($inputs)
    {
        $db_policy = $this->storePolicy(new $this->db_policy ,  $inputs);
        return $db_policy;
    }
	
	public function updatePolicy($inputs, $id)
	{
		$db_policy = $this->db_policy->findOrFail($id);
		$answer_id = $this->storePolicy($db_policy, $inputs, $id);
		return $answer_id;
	}
	
	public function deletePolicy($id)
    {
		$db_policy = $this->db_policy->findOrFail($id);
        $db_policy->delete();
        return true;
    }

	function storePolicy($db_policy , $inputs, $id = null)
	{	
		$db_policy->company_id = $inputs['company_id'];
		$db_policy->name = $inputs['name'];
		$db_policy->detail = $inputs['detail'];
		//Set status
		if(isset($inputs['status'])=="on")
			$db_policy->status = 1;
		else
			$db_policy->status = 0;
		$db_policy->save();
		return $db_policy;
	}
	
	public function getPolicy($id = null)
    {
		if($id==null)
		{
			$info_Policy = $this->db_policy->select('id', 'company_id', 'name', 'detail', 'status', 'created_at', 'updated_at')->orderBy('created_at', 'DESC')->get();
		}
		else
		{
			$info_Policy = $this->db_policy->select('id', 'company_id', 'name', 'detail', 'status', 'created_at', 'updated_at')->findOrFail($id);
		}
        return $info_Policy;
    }
	
	public function getCompanyPolicy($company_id)
    {
		$info_Policy = $this->db_policy->select('id', 'company_id', 'name', 'detail', 'status', 'created_at', 'updated_at')->where('company_id', '=', $company_id);
        return $info_Policy;
    }
}

