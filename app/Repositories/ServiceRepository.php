<?php

namespace App\Repositories;

use App\Models\Service;
use App\Models\Fare_type;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Hash;

class ServiceRepository {
	
	/**
	 * @var App\Models\Service
	 */
	protected $db_service;
	protected $db_fare_type;
		
    public function __construct(Service $db_service, Fare_type $db_fare_type) 
    {
        $this->db_service = $db_service;
		$this->db_fare_type = $db_fare_type;
    }
	
	public function addService($inputs)
    {
        $db_service = $this->storeService(new $this->db_service ,  $inputs);
        return $db_service;
    }
	
	public function updateService($inputs, $id)
	{
		$db_service = $this->db_service->findOrFail($id);
		$answer_id = $this->storeService($db_service, $inputs, $id);
		return $answer_id;
	}
	
	public function deleteService($id)
    {
		if($this->db_fare_type->where(['service_id' => $id])->Get()->Count()==0)
		{
			$db_service = $this->db_service->findOrFail($id);
        	$db_service->delete();
        	return true;
		}
		else
		{
			return false; 
		}
    }

	function storeService($db_service , $inputs, $id = null)
	{	
		$db_service->company_id = $inputs['company_id'];
		$db_service->name = $inputs['name'];
		$db_service->short_name = $inputs['short_name'];
		$db_service->detail = $inputs['detail'];
		//Set status
		if(isset($inputs['status'])=="on")
			$db_service->status = 1;
		else
			$db_service->status = 0;
		$db_service->save();
		return $db_service;
	}
	
	public function getService($id = null)
    {
		if($id==null)
		{
			$info_Service = $this->db_service->select('id', 'company_id', 'name', 'short_name', 'detail', 'status', 'created_at', 'updated_at')->orderBy('created_at', 'DESC')->get();
		}
		else
		{
			$info_Service = $this->db_service->select('id', 'company_id', 'name', 'short_name', 'detail', 'status', 'created_at', 'updated_at')->findOrFail($id);
		}
        return $info_Service;
    }
	
	public function getCompanyService($company_id)
    {
		$info_Service = $this->db_service->select('id', 'company_id', 'name', 'short_name', 'detail', 'status', 'created_at', 'updated_at')->where('company_id', '=', $company_id)->OrderBy('name');
        return $info_Service;
    }
}

