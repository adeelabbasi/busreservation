<?php

namespace App\Repositories;

use App\Models\Fare_type;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Hash;

class FareTypeRepository {
	
	/**
	 * @var App\Models\FareType
	 */
	protected $db_fare_type;
		
    public function __construct(Fare_type $db_fare_type) 
    {
        $this->db_fare_type = $db_fare_type;
    }
	
	public function addFareType($inputs)
    {
        $db_fare_type = $this->storeFareType(new $this->db_fare_type ,  $inputs);
        return $db_fare_type;
    }
	
	public function updateFareType($inputs, $id)
	{
		$db_fare_type = $this->db_fare_type->findOrFail($id);
		$answer_id = $this->storeFareType($db_fare_type, $inputs, $id);
		return $answer_id;
	}
	
	public function deleteFareType($id)
    {
		$db_fare_type = $this->db_fare_type->findOrFail($id);
        $db_fare_type->delete();
        return true;
    }

	function storeFareType($db_fare_type , $inputs, $id = null)
	{	
		$db_fare_type->company_id = $inputs['company_id'];
		$db_fare_type->schedule_id = $inputs['schedule_id'];
		$db_fare_type->service_id = $inputs['service_id'];
		$db_fare_type->name= $inputs['name'];
		
		$db_fare_type->save();
		return $db_fare_type;
	}
	
	public function getFareType($id = null)
    {
		if($id==null)
		{
			$info_FareType = $this->db_fare_type->select('id', 'company_id', 'schedule_id', 'service_id', 'name', 'created_at', 'updated_at')->orderBy('created_at', 'DESC')->get();
		}
		else
		{
			$info_FareType = $this->db_fare_type->select('id', 'company_id', 'schedule_id', 'service_id', 'name', 'created_at', 'updated_at')->findOrFail($id);
		}
        return $info_FareType;
    }
	
	public function getCompanyScheduleFareType($company_id, $schedule_id)
    {
		$info_FareType = $this->db_fare_type->select('id', 'company_id', 'schedule_id', 'service_id', 'name', 'created_at', 'updated_at')->where('company_id', '=', $company_id)->where('schedule_id', '=', $schedule_id);
        return $info_FareType;
    }
	
	public function getCompanyFareType($company_id)
    {
		$info_FareType = $this->db_fare_type->select('id', 'company_id', 'schedule_id', 'service_id', 'name', 'created_at', 'updated_at')->where('company_id', '=', $company_id);
        return $info_FareType;
    }
}

