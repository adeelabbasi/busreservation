<?php

namespace App\Repositories;

use App\Models\Country;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Hash;

class CountryRepository {
	
	/**
	 * @var App\Models\Country
	 */
	protected $db_country;
		
    public function __construct(Country $db_country) 
    {
        $this->db_country = $db_country;
    }
	
	public function addCountry($inputs)
    {
        $db_country = $this->storeCountry(new $this->db_country ,  $inputs);
        return $db_country;
    }
	
	public function updateCountry($inputs, $id)
	{
		$db_country = $this->db_country->findOrFail($id);
		$answer_id = $this->storeCountry($db_country, $inputs, $id);
		return $answer_id;
	}
	
	public function deleteCountry($id)
    {
		$db_country = $this->db_country->findOrFail($id);
        $db_country->delete();
        return true;
    }

	function storeCountry($db_country , $inputs, $id = null)
	{	
		$db_country->name = $inputs['name'];
		$db_country->short_name = $inputs['short_name'];
		//Set status
		if(isset($inputs['status'])=="on")
			$db_country->status = 1;
		else
			$db_country->status = 0;
		$db_country->save();
		return $db_country;
	}
	
	public function getCountry($id = null)
    {
		if($id==null)
		{
			$info_Country = $this->db_country->select('id', 'name', 'short_name', 'status', 'created_at', 'updated_at')->orderBy('created_at', 'DESC')->get();
		}
		else
		{
			$info_Country = $this->db_country->select('id', 'name', 'short_name', 'status', 'created_at', 'updated_at')->findOrFail($id);
		}
        return $info_Country;
    }
}

