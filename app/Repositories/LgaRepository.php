<?php

namespace App\Repositories;

use App\Models\Lga;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Hash;

class LgaRepository {
	
	/**
	 * @var App\Models\Lga
	 */
	protected $db_lga;
		
    public function __construct(Lga $db_lga) 
    {
        $this->db_lga = $db_lga;
    }
	
	public function addLga($inputs)
    {
        $db_lga = $this->storeLga(new $this->db_lga ,  $inputs);
        return $db_lga;
    }
	
	public function updateLga($inputs, $id)
	{
		$db_lga = $this->db_lga->findOrFail($id);
		$answer_id = $this->storeLga($db_lga, $inputs, $id);
		return $answer_id;
	}
	
	public function deleteLga($id)
    {
		$db_lga = $this->db_lga->findOrFail($id);
        $db_lga->delete();
        return true;
    }

	function storeLga($db_lga , $inputs, $id = null)
	{
		$db_lga->city_id = $inputs['city_id'];
		$db_lga->name = $inputs['name'];
		$db_lga->short_name = $inputs['short_name'];
		//Set status
		if(isset($inputs['status'])=="on")
			$db_lga->status = 1;
		else
			$db_lga->status = 0;
		$db_lga->save();
		return $db_lga;
	}
	
	public function getLga($id = null)
    {
		if($id==null)
		{
			$info_Lga = $this->db_lga->select('id', 'city_id', 'name', 'short_name', 'status', 'created_at', 'updated_at')->orderBy('created_at', 'DESC')->get();
		}
		else
		{
			$info_Lga = $this->db_lga->select('id', 'city_id', 'name', 'short_name', 'status', 'created_at', 'updated_at')->findOrFail($id);
		}
        return $info_Lga;
    }
	
	public function getCityLga($state_id)
    {
		$info_Lga = $this->db_lga->select('id', 'city_id', 'name', 'short_name', 'status', 'created_at', 'updated_at')->where('status', '=', '1')->where('city_id', '=', $state_id);
        return $info_Lga;
    }
}

