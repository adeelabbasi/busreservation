<?php

namespace App\Repositories;

use DB;
use Illuminate\Support\Facades\Hash;

class ApiRepository {
		
    public function __construct() 
    {
		
    }
	
	public function getAllRoutes()
	{
		return $Result  = DB::Select("SELECT id, name , (SELECT terminals.name FROM route_details join terminals on (route_details.terminal_id = terminals.id) where route_id=rou.id order by route_details.sequence limit 1) as 'from_terminal', (SELECT terminals.name FROM route_details join terminals on (route_details.terminal_id = terminals.id) where route_id=rou.id order by route_details.sequence desc limit 1) as 'to_terminal' FROM routes rou");
	}
	
	public function getAllRoutesByCompany($company_id)
	{
		return $Result  = DB::Select("SELECT id, name , (SELECT terminals.name FROM route_details join terminals on (route_details.terminal_id = terminals.id) where route_id=rou.id order by route_details.sequence limit 1) as 'from_terminal', (SELECT terminals.name FROM route_details join terminals on (route_details.terminal_id = terminals.id) where route_id=rou.id order by route_details.sequence desc limit 1) as 'to_terminal' FROM routes rou where company_id=".$company_id);
	}
}

