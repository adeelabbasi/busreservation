<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Input;
use App\Repositories\ImageRepository;
use App\Models\Userlog;
use DB;
use Auth; 

class GeneralRepository {
	
    /**
	 * @var App\Models\Log
	 */
	protected $db_log;
		
    public function __construct(Userlog $db_log) 
    {
        $this->db_log = $db_log;
    }
	
	public function addLog($inputs)
    {
        $db_log = $this->storeLog(new $this->db_log ,  $inputs);
        return $db_log;
    } 
	
	function storeLog($db_log , $inputs, $id = null)
	{
		
		if(Auth::guard('admin')->check())
		{
			$db_log->admin_id = Auth::guard('admin')->User()->id;
		}
		else
		{
			$db_log->user_id = Auth::guard('web')->User()->id;
		}
		$db_log->action = $inputs['action'];
		$db_log->save();
		return $db_log;
	}
}

