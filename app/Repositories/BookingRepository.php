<?php

namespace App\Repositories;

use App\Models\Booking;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Hash;

class BookingRepository {
	
	/**
	 * @var App\Models\Booking
	 */
	protected $db_booking;
		
    public function __construct(Booking $db_booking) 
    {
        $this->db_booking = $db_booking;
    }
	
	public function addBooking($inputs)
    {
        $db_booking = $this->storeBooking(new $this->db_booking ,  $inputs);
        return $db_booking;
    }
	
	public function updateBooking($inputs, $id)
	{
		$db_booking = $this->db_booking->findOrFail($id);
		$answer_id = $this->storeBooking($db_booking, $inputs, $id);
		return $answer_id;
	}
	
	public function deleteBooking($id)
    {
		$db_booking = $this->db_booking->findOrFail($id);
        $db_booking->delete();
        return true;
    }

	function storeBooking($db_booking , $inputs, $id = null)
	{	
		$db_booking->country_id = $inputs['country_id'];
		$db_booking->customer_id = $inputs['customer_id'];
		$db_booking->schedule_id = $inputs['schedule_id'];
		$db_booking->no_of_seats = $inputs['no_of_seats'];
		$db_booking->sub_total = $inputs['sub_total'];
		$db_booking->tax = $inputs['tax'];
		$db_booking->total = $inputs['total'];
		//Set status
		if(isset($inputs['status'])=="on")
			$db_booking->status = 1;
		else
			$db_booking->status = 0;
		$db_booking->save();
		return $db_booking;
	}
	
	public function getBooking($id = null)
    {
		if($id==null)
		{
			$info_Booking = $this->db_booking->select('id', 'company_id', 'customer_id', 'schedule_id', 'refcode', 'msisdn', 'TripID', 'SelectedSeats', 'MaxSeat', 'DestinationID', 'OrderID', 'Fullname', 'email', 'nextKin', 'nextKinPhone', 'Sex', 'sub_total', 'discount', 'tax', 'charge_type', 'channel_fee', 'gateway_fee', 'commission', 'total', 'traceId', 'from_city_id', 'from_city_name', 'from_terminal_id', 'from_terminal_name', 'to_city_id', 'to_city_name', 'to_terminal_id', 'to_terminal_name', 'schedule_date', 'gateway','new_customer', 'status', 'created_at', 'updated_at')->orderBy('created_at', 'DESC')->get();
		}
		else
		{
			$info_Booking = $this->db_booking->select('id', 'company_id', 'customer_id', 'schedule_id', 'refcode', 'msisdn', 'TripID', 'SelectedSeats', 'MaxSeat', 'DestinationID', 'OrderID', 'Fullname', 'email', 'nextKin', 'nextKinPhone', 'Sex', 'sub_total', 'discount', 'tax', 'charge_type', 'channel_fee', 'gateway_fee', 'commission', 'total', 'traceId', 'from_city_id', 'from_city_name', 'from_terminal_id', 'from_terminal_name', 'to_city_id', 'to_city_name', 'to_terminal_id', 'to_terminal_name', 'schedule_date', 'gateway','new_customer', 'status', 'created_at', 'updated_at')->findOrFail($id);
		}
        return $info_Booking;
    }
	
	public function getCompanyBooking($company_id)
    {
		$info_Booking = $this->db_booking->select('id', 'company_id', 'customer_id', 'schedule_id', 'refcode', 'msisdn', 'TripID', 'SelectedSeats', 'MaxSeat', 'DestinationID', 'OrderID', 'Fullname', 'email', 'nextKin', 'nextKinPhone', 'Sex', 'sub_total', 'discount', 'tax', 'charge_type', 'channel_fee', 'gateway_fee', 'commission', 'total', 'traceId', 'from_city_id', 'from_city_name', 'from_terminal_id', 'from_terminal_name', 'to_city_id', 'to_city_name', 'to_terminal_id', 'to_terminal_name', 'schedule_date', 'gateway','new_customer', 'status', 'created_at', 'updated_at')->where('company_id', '=', $company_id);
        return $info_Booking;
    }
}

