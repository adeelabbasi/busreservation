<?php

namespace App\Repositories;

use App\Models\Payment;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Hash;

class PaymentRepository {
	
	/**
	 * @var App\Models\Payment
	 */
	protected $db_payment;
		
    public function __construct(Payment $db_payment) 
    {
        $this->db_payment = $db_payment;
    }
	
	public function addPayment($inputs)
    {
        //
    }
	
	public function updatePayment($inputs, $id)
	{
		//
	}
	
	public function deletePayment($id)
    {
		//
    }

	function storePayment($db_payment , $inputs, $id = null)
	{	
		//
	}
	
	public function getPayment($id = null)
    {
		if($id==null)
		{
			$info_Payment = $this->db_payment->select('id', 'company_id', 'booking_id', 'bank_id', 'msisdn', 'channel_fee', 'gateway_fee', 'amount', 'status', 'paymentReference', 'traceId', 'customerRef', 'payment_provider', 'created_at', 'updated_at')->orderBy('created_at', 'DESC')->get();
		}
		else
		{
			$info_Payment = $this->db_payment->select('id', 'company_id', 'booking_id', 'bank_id', 'msisdn', 'channel_fee', 'gateway_fee', 'amount', 'status', 'paymentReference', 'traceId', 'customerRef', 'payment_provider', 'created_at', 'updated_at')->findOrFail($id);
		}
        return $info_Payment;
    }
	
	public function getCompanyPayment($company_id)
    {
		$info_Payment = $this->db_payment->select('id', 'company_id', 'booking_id', 'bank_id', 'msisdn', 'channel_fee', 'gateway_fee', 'amount', 'status', 'paymentReference', 'traceId', 'customerRef', 'payment_provider', 'created_at', 'updated_at')->where('company_id', '=', $company_id)->orderBy('created_at', 'DESC')->get();
        return $info_Payment;
    }
}

