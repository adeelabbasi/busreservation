<?php

namespace App\Repositories;
use App\Models\City;
use App\Models\Company;
use App\Models\Company_ussd;
use App\Models\Schedule;
use App\Models\Fare;
use App\Models\Terminal;
use App\Models\Subscription;
use App\Models\Charging_log;
use App\Models\Customer_log;
use App\Models\Booking;
use App\Models\Refcode;
use Carbon\Carbon;
use DateTime;
use DB;
use App\Jobs\CallBookingAPI;
use App\Jobs\FinalBookNewMessage;


class UssdRepository { 
 	
	//88111 
	//7433
	public function Level1($session_msisdn,$session_msg,$session_from,$gateway,$testing)
	{
		//Uncomment echo "Menu 1";
		$output['session_operation'] = "continue";
		$output['session_msg'] = "Welcome to ".$session_from." Travel Service\n".chr(64)."N10/SMS\n1. Bus\n2. Help\n3. Exit";
		
		$session_id = $this->addCustomerLog($testing, $session_msisdn, $session_from, 1);
		
		$ResultDel = DB::delete("delete from ussd_menu_session where msisdn = '$session_msisdn'");
		$ResultIns = DB::insert("INSERT INTO ussd_menu_session (session_id, msisdn, ussd_code, user_level, keyword, parent_id, last_parent_id, g_parent, is_child, level_1 ,record_on) VALUES ('$session_id', '$session_msisdn', '$session_msg', '1', '$session_msg', '1', '1', '1', '1', '$session_msg' ,  CURRENT_TIMESTAMP)");
		
		return $output; 
	}
	 
	public function companyLevel1($session_msisdn,$session_msg,$session_from,$company_name,$company_id,$gateway,$testing)
	{
		//c_parent -> To find that company menu start
  		//c_parent_id -> Current company menu
  		//c_last_parent_id -> Last company parent id
		
		//Uncomment echo "Menu 1";
		$output['session_operation'] = "continue";
		$output['session_msg'] = "Thanks for Choosing\n".$company_name."\nSelect\n1. Book Now\n2. My Bookings\n3. Terminals\n4. Contact Us"; 
		
		$info_Company = $this->GetCompanyPageNo($company_id);
		$session_id = $this->addCustomerLog($testing, $session_msisdn, $session_msg, 0, $info_Company[0]->id, $info_Company[0]->name);
		
		$ResultDel = DB::delete("delete from ussd_menu_session where msisdn = '$session_msisdn'");
		$ResultIns = DB::insert("INSERT INTO ussd_menu_session (session_id, msisdn, ussd_code, user_level, keyword, parent_id, last_parent_id, g_parent, s_parent, is_child, c_parent_id, company_id, company_name, company_ussd_type, company_api_url, company_api_token, level_4, record_on) VALUES ('$session_id', '$session_msisdn', '$session_msg', '4', '$session_msg', '".$info_Company[0]->rownum."', '2', '1', '1', '1', '".$info_Company[0]->rownum."' ,'$company_id','".$info_Company[0]->name."', '".$info_Company[0]->ussd_type."', '".$info_Company[0]->api_url."', '".$info_Company[0]->api_token."', '".$info_Company[0]->rownum."', CURRENT_TIMESTAMP)");		
		return $output;
	}
	
	public function Level2($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing)
	{
		switch($session_msg)
		{
			case 1: //Bus menu
				//Uncomment echo "Menu 2";
				$output['session_operation'] = "continue"; 
				$output['session_msg'] = $session_from." Bus Menu \n1. Transporters \n2. Schedule & Fare \n3. Promo & Deals\n4. Infotainment\n* Back";
				
				if(!$moreKeyword)
					$userSession->level_2=$session_msg;
				
				$userSession->user_level=2;
				$userSession->keyword=$session_msg;
				$userSession->parent_id=$session_msg;
				$userSession->last_parent_id=$session_msg;
				$userSession->g_parent=$session_msg;
				$userSession->is_child=1;
				$userSession->save();
			break;
			case 2: //Help
				$userSession->last_parent_id=$userSession->parent_id;
				$userSession->user_level=2;
				$userSession->save();
				
				$this->updateCustomerLog($testing, "update customer_logs set is_complete=1, action='Help' where id=".$userSession->session_id);
				$output['session_operation'] = "endcontinue";		
				$output['session_msg'] = "".$session_from." Travel Help\n\nEnjoy unlimited access to travel information, get scehdules, fares, promos from your preferred transporter.\n* Back";
				
			break;
			case 3: //Exit
				
				//Uncomment echo "Menu 2";
				$output['session_operation'] = "exit"; 
				$output['session_msg'] = "Thanks for choosing ".$session_from." Travel Service.\nDial us again soon!";
				
			break;
		}
	
		return $output;
	}
	public function Level3($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing)
	{
		if(!$moreKeyword)
			$userSession->level_3=$session_msg;
		//Uncomment echo "Menu 3";
		
		//2nd menu session will use in after 3rd menu
		$userSession->s_parent=$session_msg;
		
		//Change page no on level change, check if level is same else change page no
		if($userSession->user_level != 3)
		{
			$userSession->page_no = 0;
		}
		switch($session_msg)
		{
			
			case 1: //Transporters  > Companies menu
				
				if($moreKeyword)
				{
					$userSession->page_no = $userSession->page_no+4;
				}
				else
				{
					$userSession->last_parent_id=$userSession->parent_id;
				}
				
				$output['session_operation'] = "continue";
				$info_Companies = $this->GetCompanies($userSession->page_no);
				$menuCompany = "";
				foreach($info_Companies as $Company)
				{
					$menuCompany .= $Company->rownum.". ".$Company->name."\n";
				}
				
				$info_Companies = $this->GetCompanies($userSession->page_no+4);
				
				$userSession->more_keyword=0;
				if($info_Companies)
				{
					$menuCompany .= ($userSession->page_no+5).". More\n";
					$userSession->more_keyword=($userSession->page_no+5);
				}
				$output['session_msg'] = "Transporters".chr(58)."\nPlease select".chr(58)."\n".$menuCompany."* Back";
				
				$userSession->c_parent=0;
				$userSession->per_page=4;
				$userSession->user_level=3;
				$userSession->keyword=$session_msg;
				$userSession->parent_id=$session_msg;
				$userSession->is_child=1;
				$userSession->save();
				
			break;
			
			case 2: //Schedule & Fare > From city
				
				if($moreKeyword)
				{
					$userSession->page_no = $userSession->page_no+4;
				}
				else
				{
					$userSession->last_parent_id=$userSession->parent_id;
				}
				
				$output['session_operation'] = "continue";
				$info_Cities = $this->GetCities($userSession->page_no);
				$menuCity = "";
				foreach($info_Cities as $City)
				{
					$menuCity .= $City->rownum.". ".$City->name."\n";
				}
				
				$info_Cities = $this->GetCities($userSession->page_no+4);
				
				$userSession->more_keyword=0;
				if($info_Cities)
				{
					$menuCity .= ($userSession->page_no+5).". More\n";
					$userSession->more_keyword=($userSession->page_no+5);
				}
				$output['session_msg'] = "From".chr(58)."\n".$menuCity."* Back";
				
				$userSession->per_page=4;
				$userSession->user_level=3;
				$userSession->keyword=$session_msg;
				$userSession->parent_id=$session_msg;
				$userSession->is_child=1;
				$userSession->save();
				
			break;
			
			case 3: //Promo & Deals  > Companies menu
				
				if($moreKeyword)
				{
					$userSession->page_no = $userSession->page_no+4;
				}
				else
				{
					$userSession->last_parent_id=$userSession->parent_id;
				}
				
				$output['session_operation'] = "continue";
				$info_Companies = $this->GetCompanies($userSession->page_no);
				$menuCompany = "";
				foreach($info_Companies as $Company)
				{
					$menuCompany .= $Company->rownum.". ".$Company->name."\n";
				}
				
				$info_Companies = $this->GetCompanies($userSession->page_no+4);
				
				$userSession->more_keyword=0;
				if($info_Companies)
				{
					$menuCompany .= ($userSession->page_no+5).". More\n";
					$userSession->more_keyword=($userSession->page_no+5);
				}
				$output['session_msg'] = "Promo & Deals".chr(58)."company".chr(58)."\n".$menuCompany."* Back";
				$userSession->per_page=4;
				$userSession->user_level=3;
				$userSession->keyword=$session_msg;
				$userSession->parent_id=$session_msg;
				$userSession->is_child=1;
				$userSession->save();
				
			break;


			case 4: //Infotainment
				
				$userSession->last_parent_id=$userSession->parent_id;
				$userSession->user_level=3;
				$userSession->save();
				
				$this->updateCustomerLog($testing, "update customer_logs set is_complete=1, action='Infotainment' where id=".$userSession->session_id);
				$output['session_operation'] = "endcontinue";		
				$output['session_msg'] = "Infotainment. Thank you.\n* Back\n0 Main Menu";	
			break;
		}
		
		return $output;
	}
	
	public function Level4($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing)
	{
		if(!$moreKeyword)
			$userSession->level_4=$session_msg;
		//Change page no on level change, check if level is same else change page no
		if($userSession->user_level != 4)
		{
			$userSession->page_no = 0;
		}
		
		switch($userSession->s_parent)
		{
			
			case 1: //Transporters > 1st menu
				//****************************************
				// Start Transporters Level 1
				//****************************************
				if($moreKeyword)
				{
					$userSession->page_no = $userSession->page_no+4;
				}
				else
				{
					$userSession->last_parent_id=$userSession->parent_id;
				}
				
				$output['session_operation'] = "continue";
				$info_Company = $this->GetSelectedCompany($session_msg);
				$output['session_msg'] = "Thanks for Choosing\n".$info_Company[0]->name."\nSelect\n1. Book Now\n2. My Bookings\n3. Terminals\n4. Contact Us";  
				
				if($userSession->ussd_code=="*".$session_from)
				{
					$output['session_msg'] .= "\n"."* Back";
				}
				
				$userSession->c_parent_id=$session_msg;
				$userSession->more_keyword=0;
				$userSession->company_id=$info_Company[0]->id;
				$userSession->company_name=$info_Company[0]->name;
				$userSession->per_page=4;
				$userSession->company_ussd_type=$info_Company[0]->ussd_type;
				$userSession->company_api_url=$info_Company[0]->api_url;
				$userSession->company_api_token=$info_Company[0]->api_token;
				$userSession->user_level=4;
				$userSession->keyword=$session_msg;
				$userSession->parent_id=$session_msg;
				$userSession->is_child=1;
				$userSession->save();
				//****************************************
				// End Transporters
				//****************************************
			break;
			
			case 2: //Schedule & Fare > To Cities
				
				if($moreKeyword)
				{
					$userSession->page_no = $userSession->page_no+4;
					$info_FromCity = City::Where('id',$userSession->from_city_id)->Get();
				}
				else
				{
					$userSession->last_parent_id=$userSession->parent_id;
					$info_FromCity = $this->GetSelectedCity($session_msg);
				}
				
				$output['session_operation'] = "continue";
								
				$info_Cities = $this->GetToCities($info_FromCity[0]->id, $userSession->page_no);
				$menuCity = "";
				foreach($info_Cities as $City)
				{
					$menuCity .= $City->rownum.". ".$City->name."\n";
				}
				
				$info_Cities = $this->GetToCities($info_FromCity[0]->id, $userSession->page_no+4);
				
				$userSession->more_keyword=0;
				if($info_Cities)
				{
					$menuCity .= ($userSession->page_no+5).". More\n";
					$userSession->more_keyword=($userSession->page_no+5);
				}
				
				$output['session_msg'] = "To".chr(58)."\n".$menuCity."* Back";
				
				$userSession->from_city_id=$info_FromCity[0]->id;
				$userSession->from_city_name=$info_FromCity[0]->name;
				$userSession->per_page=4;
				$userSession->user_level=4;
				$userSession->keyword=$session_msg;
				$userSession->parent_id=$session_msg;
				$userSession->is_child=1;
				$userSession->save();
				
			break;
			
			case 3: //Promo & Deals > List
				
				if($moreKeyword)
				{
					$userSession->page_no = $userSession->page_no+4;
				}
				else
				{
					$userSession->last_parent_id=$userSession->parent_id;
				}
				
				$output['session_operation'] = "continue";
				$info_Company = $this->GetSelectedCompany($session_msg);
				
				$info_Deals = $this->GetDeals($info_Company[0]->id,$userSession->page_no);
				$countOfMenu = sizeof($info_Deals);
				$menuDeal = "";
				foreach($info_Deals as $Deal)
				{
					$menuDeal .= $Deal->rownum.". ".$Deal->name."\n";
				}
				
				$info_Deals = $this->GetDeals($info_Company[0]->id,$userSession->page_no+4);
				
				$userSession->more_keyword=0;
				if($info_Deals)
				{
					$menuDeal .= ($userSession->page_no+5).". More\n";
					$userSession->more_keyword=($userSession->page_no+5);
				}
				$output['session_msg'] = "Promo & Deals: ".$info_Company[0]->name."\nSelect to see details".chr(58)."\n".$menuDeal."* Back";
				
				if($countOfMenu==0)
				{
					$this->updateCustomerLog($testing, "update customer_logs set is_complete=0, action='Promo & Deals', company_id='".$info_Company[0]->id."', company_name='".$info_Company[0]->name."' where id=".$userSession->session_id);
					$output['session_operation'] = "endcontinue";
					$output['session_msg'] = "No promo at the moment, please check later. \n* Back\n0 Main Menu";
				}
				
				$userSession->company_id=$info_Company[0]->id;
				$userSession->company_name=$info_Company[0]->name;
				$userSession->per_page=4;
				$userSession->user_level=4;
				$userSession->keyword=$session_msg;
				$userSession->parent_id=$session_msg;
				$userSession->is_child=1;
				$userSession->save();
				
			break;
			case 4: //Infotainment
				//Already ended	
			break;
		}
		
		return $output;
	}
	
	public function Level5($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing)
	{
		if(!$moreKeyword)
			$userSession->level_5=$session_msg;
		//Uncomment echo "Menu 5";
		
		//Change page no on level change, check if level is same else change page no
		if($userSession->user_level != 5)
		{
			$userSession->page_no = 0;
		}
		
		switch($userSession->s_parent)
		{
			
			case 1: //Transporters
				//****************************************
				// Start Transporters Level 2
				//****************************************
				//Set company menu parent 
				$userSession->c_parent=$session_msg;
				switch($session_msg)
				{
					case 1: //Transporters > Schedule > From Cities
						$this->updateCustomerLog($testing, "update customer_logs set action='Book Now', company_id='".$userSession->company_id."', company_name='".$userSession->company_name."' where id=".$userSession->session_id);

						if($moreKeyword)
						{
							$userSession->page_no = $userSession->page_no+7;
						}
						else
						{
							$userSession->c_last_parent_id=$userSession->c_parent_id;
						}
						
						$output['session_operation'] = "continue";
						$per_page = 7;
						if($userSession->company_ussd_type == "1")
						{
							$info_Cities = $this->GetCompanyCities($userSession->company_id, $userSession->page_no);
						}
						else
						{
							$info_Cities = $this->GetCompanyCitiesAllRouteDetailsAPI($userSession->company_id, $userSession->page_no);
						}
						$menuCity = "";
						foreach($info_Cities as $City)
						{
							$menuCity .= $City->rownum.". ".ucfirst(strtolower($City->name))."\n";
						}
						
						if($userSession->company_ussd_type == "1")
						{
							$info_Cities = $this->GetCompanyCities($userSession->company_id, $userSession->page_no);
						}
						else
						{
							$info_Cities = $this->GetCompanyCitiesAllRouteDetailsAPI($userSession->company_id, $userSession->page_no+7);
						}
						//dd($info_Cities);
						$userSession->more_keyword=0;
						if($info_Cities)
						{
							$menuCity .= ($userSession->page_no+8).". More\n";
							$userSession->more_keyword=($userSession->page_no+8);
						}
						$output['session_msg'] = "From".chr(58)."\n".$menuCity."* Back";
						$userSession->per_page=$per_page;
						$userSession->user_level=5;
						$userSession->keyword=$session_msg;
						$userSession->c_parent_id=$session_msg;
						$userSession->is_child=1;
						$userSession->save();
					break;
					case 2: //Transporters > Bookings > Status/New
						$this->updateCustomerLog($testing, "update customer_logs set action='My Bookings', company_id='".$userSession->company_id."', company_name='".$userSession->company_name."' where id=".$userSession->session_id);

						if($moreKeyword)
						{
							$userSession->page_no = $userSession->page_no+4;
						}
						else
						{
							$userSession->c_last_parent_id=$userSession->c_parent_id;
						}
						
						$output['session_operation'] = "continue";
						$output['session_msg'] = "Select".chr(58)."\n1. New Booking\n2. Booking Status\n* Back";
						$userSession->per_page=4;
						$userSession->user_level=5;
						$userSession->keyword=$session_msg;
						$userSession->c_parent_id=$session_msg;
						$userSession->is_child=1;
						$userSession->save();
						
						if($userSession->ussd_code=="*".$session_from)
						{
							$output['session_msg'] .= "\n"."* Back";
						}
						
					break;
					case 3: //Transporters > Terminals > Terminals Cities
						
						$this->updateCustomerLog($testing, "update customer_logs set action='Terminals', company_id='".$userSession->company_id."', company_name='".$userSession->company_name."' where id=".$userSession->session_id);
						
						
						if($moreKeyword)
						{
							$userSession->page_no = $userSession->page_no+7;
						}
						else
						{
							$userSession->c_last_parent_id=$userSession->c_parent_id;
						}
						
						$per_page = 7;
						
						$output['session_operation'] = "continue";
						if($userSession->company_ussd_type == "1")
						{
							$info_Cities = $this->GetCompanyCities($userSession->company_id, $userSession->page_no);
						}
						else
						{
							$info_Cities = $this->GetCompanyCitiesAPI($userSession->company_id, $userSession->page_no);
						}
						
						$menuCity = "";
						foreach($info_Cities as $City)
						{
							//dd($City);
							$menuCity .= $City->rownum.". ".ucfirst(strtolower($City->name))."\n";
						}
						
						if($userSession->company_ussd_type == "1")
						{
							$info_Cities = $this->GetCompanyCities($userSession->company_id, $userSession->page_no+7);
						}
						else
						{
							$info_Cities = $this->GetCompanyCitiesAPI($userSession->company_id, $userSession->page_no+7);
							
						}
						
						$userSession->more_keyword=0;
						if($info_Cities)
						{
							$menuCity .= ($userSession->page_no+8).". More\n";
							$userSession->more_keyword=($userSession->page_no+8);
						}
						
						//dd($menuCity);
						$output['session_msg'] = "Terminal Location".chr(58)."\n".$menuCity."* Back";
						$userSession->per_page=$per_page;
						$userSession->user_level=5;
						$userSession->keyword=$session_msg;
						$userSession->c_parent_id=$session_msg;
						$userSession->is_child=1;
						$userSession->save();
						
					break;
					case 4: //Transporters > Services > Services Cities Change to contact US
						$this->updateCustomerLog($testing, "update customer_logs set is_complete=1, action='Contact US', company_id='".$userSession->company_id."', company_name='".$userSession->company_name."' where id=".$userSession->session_id);
						
						$info_Company = $userSession->Company()->First();
						$output['session_operation'] = "endcontinue";		
						
						$phone="";
						foreach($info_Company->Company_phone()->Get() as $Company_phone)
						{
							$phone = $phone.$Company_phone->number.",";
 						}
						$phone = substr($phone,0,-1);
						
						$whatsapp="";
						foreach($info_Company->Company_whatsapp()->Get() as $Company_whatsapp)
						{
							$whatsapp = $whatsapp.$Company_whatsapp->number.",";
 						}

						$smsMessage = "Thanks for Choosing\n".$info_Company->name."\nYou can reach us on ".$phone." Whatsapp line: ".$whatsapp." or visit our website: ".$info_Company->website; 
						$Charge = $this->ChargeUser($session_msisdn, "*".$session_from);
						if($Charge != 0)
						{
							$this->sendSMS($session_from,$session_msisdn, $smsMessage, $gateway);
							$output['session_msg'] = "Thanks, your request has being received. You will receive SMS response shortly";
						}
						else
						{
							$output = $this->emptyBalanceMessage($session_msisdn);
						}
						/*if($moreKeyword)
						{
							$userSession->page_no = $userSession->page_no+4;
						}
						else
						{
							$userSession->c_last_parent_id=$userSession->c_parent_id;
						}
						
						$output['session_operation'] = "continue";
						
						$info_Deals = $this->GetCservices($userSession->company_id,$userSession->page_no);
						$countOfMenu = sizeof($info_Deals);
						$menuDeal = "";
						foreach($info_Deals as $Deal)
						{
							$menuDeal .= $Deal->rownum.". ".$Deal->name."\n";
						}
						
						$info_Deals = $this->GetCservices($userSession->company_id,$userSession->page_no+4);
						
						
						$userSession->more_keyword=0;
						if($info_Deals)
						{
							$menuDeal .= ($userSession->page_no+5).". More\n";
							$userSession->more_keyword=($userSession->page_no+5);
						}
						$output['session_msg'] = "Services".chr(58)."\n".$menuDeal."* Back";
						if($countOfMenu==0)
						{
							$action = "Services";
							if($userSession->ussd_code=="*".$session_from)
							{
								$action = "Transporters";
							}
							$this->updateCustomerLog($testing, "update customer_logs set is_complete=0, action='$action' where id=".$userSession->session_id);
							
							$output['session_operation'] = "endcontinue";
							$output['session_msg'] = "No service at the moment, please check later. \n* Back\n0 Main Menu";
						}
						$userSession->per_page=4;				
						$userSession->user_level=5;
						$userSession->keyword=$session_msg;
						$userSession->c_parent_id=$session_msg;
						$userSession->is_child=1;
						$userSession->save();*/
					break;
					
					case 5: //Transporters > Promo & Deals > List
						if($moreKeyword)
						{
							$userSession->page_no = $userSession->page_no+4;
						}
						else
						{
							$userSession->c_last_parent_id=$userSession->c_parent_id;
						}
						
						$output['session_operation'] = "continue";
						
						$info_Deals = $this->GetDeals($userSession->company_id,$userSession->page_no);
						$countOfMenu = sizeof($info_Deals);
						$menuDeal = "";
						foreach($info_Deals as $Deal)
						{
							$menuDeal .= $Deal->rownum.". ".$Deal->name."\n";
						}
						
						$info_Deals = $this->GetDeals($userSession->company_id,$userSession->page_no+4);
						
						
						$userSession->more_keyword=0;
						if($info_Deals)
						{
							$menuDeal .= ($userSession->page_no+5).". More\n";
							$userSession->more_keyword=($userSession->page_no+5);
						}
						$output['session_msg'] = "Promo & Deals".chr(58)."\n".$menuDeal."* Back";
						if($countOfMenu==0)
						{
							$action = "Promo & Deals";
							if($userSession->ussd_code=="*".$session_from)
							{
								$action = "Transporters";
							}
							$this->updateCustomerLog($testing, "update customer_logs set is_complete=0, action='$action' where id=".$userSession->session_id);
							
							$output['session_operation'] = "endcontinue";
							$output['session_msg'] = "No promo at the moment, please check later. \n* Back\n0 Main Menu";
						}
						$userSession->per_page=4;				
						$userSession->user_level=5;
						$userSession->keyword=$session_msg;
						$userSession->c_parent_id=$session_msg;
						$userSession->is_child=1;
						$userSession->save();
					break;
					
					case 6: //Transporters > Policy Statement > List
						if($moreKeyword)
						{
							$userSession->page_no = $userSession->page_no+4;
						}
						else
						{
							$userSession->c_last_parent_id=$userSession->c_parent_id;
						}
						
						$output['session_operation'] = "continue";
						
						$info_Policies = $this->GetPolicies($userSession->company_id,$userSession->page_no);
						$countOfMenu = sizeof($info_Policies);
						$menuPolicy = "";
						foreach($info_Policies as $Policy)
						{
							$menuPolicy .= $Policy->rownum.". ".$Policy->name."\n";
						}
						
						$info_Policies = $this->GetPolicies($userSession->company_id,$userSession->page_no+4);
						
						$userSession->more_keyword=0;
						if($info_Policies)
						{
							$menuPolicy .= ($userSession->page_no+5).". More\n";
							$userSession->more_keyword=($userSession->page_no+5);
						}
						$output['session_msg'] = "Policies".chr(58)."\n".$menuPolicy."* Back";
						if($countOfMenu==0)
						{
							$action = "Policy Statement";
							if($userSession->ussd_code=="*".$session_from)
							{
								$action = "Transporters";
							}
							$this->updateCustomerLog($testing, "update customer_logs set is_complete=0, action='$action' where id=".$userSession->session_id);
							
							$output['session_operation'] = "endcontinue";
							$output['session_msg'] = "No policy at the moment, please check later. \n* Back\n0 Main Menu";
						}
						$userSession->per_page=4;				
						$userSession->user_level=5;
						$userSession->keyword=$session_msg;
						$userSession->c_parent_id=$session_msg;
						$userSession->is_child=1;
						$userSession->save();
					break;
					
					case 7: //Transporters > About > List
						if($moreKeyword)
						{
							$userSession->page_no = $userSession->page_no+4;
						}
						else
						{
							$userSession->c_last_parent_id=$userSession->c_parent_id;
						}
						
						$output['session_operation'] = "continue";
						
						$info_Deals = $this->GetAbouts($userSession->company_id,$userSession->page_no);
						$countOfMenu = sizeof($info_Deals);
						$menuDeal = "";
						foreach($info_Deals as $Deal)
						{
							$menuDeal .= $Deal->rownum.". ".$Deal->name."\n";
						}
						
						$info_Deals = $this->GetAbouts($userSession->company_id,$userSession->page_no+4);
						
						
						$userSession->more_keyword=0;
						if($info_Deals)
						{
							$menuDeal .= ($userSession->page_no+5).". More\n";
							$userSession->more_keyword=($userSession->page_no+5);
						}
						$output['session_msg'] = "About US".chr(58)."\n".$menuDeal."* Back";
						if($countOfMenu==0)
						{
							$action = "About US";
							if($userSession->ussd_code=="*".$session_from)
							{
								$action = "Transporters";
							}
							$this->updateCustomerLog($testing, "update customer_logs set is_complete=0, action='$action' where id=".$userSession->session_id);
							
							$output['session_operation'] = "endcontinue";
							$output['session_msg'] = "No About US at the moment, please check later. \n* Back\n0 Main Menu";
						}
						$userSession->per_page=4;				
						$userSession->user_level=5;
						$userSession->keyword=$session_msg;
						$userSession->c_parent_id=$session_msg;
						$userSession->is_child=1;
						$userSession->save();
					break;
					
					case 8: //Transporters > Exit
						$output['session_operation'] = "exit"; 
						$output['session_msg'] = "Thanks for choosing ".$session_from." Travel Service.\nDial us again soon!";
					break;
				}
				//****************************************
				// End Transporters
				//****************************************
			break;
			
			case 2: //Schedule & Fare > Dates
				//Mean get out from parent menu
				$userSession->c_parent=0;
				
				if($moreKeyword)
				{
					$userSession->page_no = $userSession->page_no+5;
					$info_ToCity = City::Where('id',$userSession->to_city_id)->Get();
				}
				else
				{
					$userSession->last_parent_id=$userSession->parent_id;
					$info_ToCity = $this->GetSelectedToCity($userSession->from_city_id,$session_msg);
					
					//Get schedules
					$info_Schedules = $this->GetScheduleCountByCities($userSession->from_city_id,$info_ToCity[0]->id);
				}
				
				$output['session_operation'] = "continue";
				
				$info_Company = $userSession->Company()->First();
				$booking_no_of_days = $info_Company->booking_no_of_days;
				
				$date = date('D d M', mktime(0, 0, 0, date("m") , date("d")-1,date("Y")));
				$menuDate = "";
				for($i=$userSession->page_no+1; $i < ($userSession->page_no+$booking_no_of_days); $i++)
				{
					$menuDate .= $i.". ".date('D d M', strtotime("+$i day", strtotime($date)))."\n";
				}
				//$menuDate .= $i.". More\n";
				//$userSession->more_keyword=$i;
				
				//$menuDate .= "Or Reply with dd/mm/yyyy\n";
				$userSession->more_keyword=0;
				
				$output['session_msg'] = "Date".chr(58)."\n".$menuDate."* Back";
				
				$userSession->to_city_id=$info_ToCity[0]->id;
				$userSession->to_city_name=$info_ToCity[0]->name;
				
				$userSession->per_page=5;
				$userSession->user_level=5;
				$userSession->keyword=$session_msg;
				$userSession->parent_id=$session_msg;
				$userSession->is_child=1;
				$userSession->save();
				
				if(sizeof($info_Schedules)==0)
				{
					$this->updateCustomerLog($testing, "update customer_logs set is_complete=0, action='Schedule & Fare', from_city_id='".$userSession->from_city_id."', from_city_name='".$userSession->from_city_name."', to_city_id='".$info_ToCity[0]->id."', to_city_name='".$info_ToCity[0]->name."' where id=".$userSession->session_id);
					$output['session_operation'] = "endcontinue";
					$output['session_msg'] = "Sorry the routes does not  exist".chr(58)."\n* Back\n 0 Main Menu";
				}
				
			break;
			
			case 3: //Promo & Deals > end
				
				$userSession->last_parent_id=$userSession->parent_id;
				$userSession->user_level=5;
				$userSession->save();
				
				$this->updateCustomerLog($testing, "update customer_logs set is_complete=1, action='Promo & Deals', company_id='".$userSession->company_id."', company_name='".$userSession->company_name."' where id=".$userSession->session_id);
				
				$userSession->c_parent=0;
				$info_Deal = $this->GetSelectedDeal($userSession->company_id,$session_msg);
				$output['session_operation'] = "endcontinue";		
				
				$smsMessage = $userSession->Company()->First()->name."\nPromo & Deals".chr(58)."\n".$info_Deal[0]->name."\n".$info_Deal[0]->detail;//."\n* Back\n0 Main Menu";
				$Charge = $this->ChargeUser($session_msisdn, "*".$session_from);
				if($Charge != 0)
				{
					$this->sendSMS($session_from,$session_msisdn, $smsMessage, $gateway);
					$output['session_msg'] = "Thanks, your request has being received. You will receive SMS response shortly";
				}
				else
				{
					$output = $this->emptyBalanceMessage($session_msisdn);
				}
				
			break;
			case 4: //Infotainment
				//Already ended	
			break;
		}
		
		return $output;
	}
	
	public function Level6($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing)
	{
		if(!$moreKeyword)
			$userSession->level_6=$session_msg;
		//Uncomment echo "Menu 6";
		
		//Change page no on level change, check if level is same else change page no
		if($userSession->user_level != 6)
		{
			$userSession->page_no = 0;
		}
		
		switch($userSession->s_parent)
		{
			
			case 1: //Transporters
				//****************************************
				// Start Transporters Level 3
				//****************************************
				switch($userSession->c_parent)
				{
					case 1: //Transporters > Schedule > To Cities
						
						$correctDate = 0;
						if($moreKeyword)
						{
							$userSession->page_no = $userSession->page_no+7;
							
							if($userSession->company_ussd_type == "1")
							{
								$info_FromCity = City::Where('id',$userSession->from_city_id)->Get();
							}
							else
							{
								$info_FromCity = $this->GetCompanyCityByIDAllRouteDetailsAPI($userSession->company_id, $userSession->from_city_id);
							}
						}
						else
						{
							$userSession->c_last_parent_id=$userSession->c_parent_id;
							//$info_FromCity = $this->GetSelectedCompanyCity($userSession->company_id, $session_msg);
							if($userSession->company_ussd_type == "1")
							{
								$info_FromCity = $this->GetSelectedCompanyCity($userSession->company_id,$session_msg);
							}
							else
							{
								$info_FromCity = $this->GetSelectedCompanyCityAllRouteDetailsAPI($userSession->company_id,$session_msg);
							}
						}
						
						$output['session_operation'] = "continue";
						
						
						if($userSession->company_ussd_type == "1")
						{
							$info_Terminals = $this->GetFromTerminals($userSession->company_id, $info_FromCity[0]->id, "", $userSession->page_no);
						}
						else
						{
							$info_Terminals = $this->GetFromTerminalsAPI($userSession->company_id, $info_FromCity[0]->id, "", $userSession->page_no);
						}
						$menuTerminal = "";
						foreach($info_Terminals as $Terminal)
						{
							$menuTerminal .= $Terminal->rownum.". ".ucfirst(strtolower($Terminal->name))."\n";
						}
						
						if($userSession->company_ussd_type == "1")
						{
							$info_Terminals = $this->GetFromTerminals($userSession->company_id, $info_FromCity[0]->id, "", $userSession->page_no+7);
						}
						else
						{
							$info_Terminals = $this->GetFromTerminalsAPI($userSession->company_id, $info_FromCity[0]->id, "", $userSession->page_no+7);
						}
						
						$userSession->more_keyword=0;
						if($info_Terminals)
						{
							$menuTerminal .= ($userSession->page_no+8).". More\n";
							$userSession->more_keyword=($userSession->page_no+8);
						}
						if($menuTerminal=="")

						{
							$output['session_msg'] = "No service\n* Back";
						}
						else
						{
							$output['session_msg'] = ucfirst($info_FromCity[0]->name)." Terminal".chr(58)."\n".$menuTerminal."* Back";
						}
						
						$userSession->from_city_id=$info_FromCity[0]->id;
						$userSession->from_city_name=$info_FromCity[0]->name;
						
						$userSession->per_page=7;
						$userSession->user_level=6;
						$userSession->keyword=$session_msg;
						$userSession->parent_id=$session_msg;
						$userSession->is_child=1;
						$userSession->save();	
					
						
					break;
					case 2: //Transporters > Bookings  > My Booking list
					
						if($userSession->level_6 == "1")//Booking
						{
							if($moreKeyword)
							{
								$userSession->page_no = $userSession->page_no+4;
							}
							else
							{
								$userSession->c_last_parent_id=$userSession->c_parent_id;
							}
							
							$output['session_operation'] = "continue";
							
							$info_SchLists = $this->GetScheduleSearch($userSession->msisdn,$userSession->page_no);
							$countOfSchLists = sizeof($info_SchLists);
							$menuSchLists = "";
							foreach($info_SchLists as $SchList)
							{
								$menuSchLists .= $SchList->rownum.". ".$SchList->from_terminal_name." to ".$SchList->to_terminal_name."\n";
							}
							
							$info_SchLists = $this->GetScheduleSearch($userSession->msisdn,$userSession->page_no+4);
							$userSession->more_keyword=0;
							if($info_SchLists)
							{
								$menuSchLists .= ($userSession->page_no+5).". More\n";
								$userSession->more_keyword=($userSession->page_no+5);
							}
							$output['session_msg'] = "Choose your Search".chr(58)."\n".$menuSchLists."* Back";
							if($countOfSchLists==0)
							{
								$action = "Booking";
								if($userSession->ussd_code=="*".$session_from)
								{
									$action = "Transporters";
								}
								$this->updateCustomerLog($testing, "update customer_logs set is_complete=0, action='$action' where id=".$userSession->session_id);
								
								$output['session_operation'] = "endcontinue";
								$output['session_msg'] = "No search schedule at the moment, please search schedule list. \n* Back\n0 Main Menu";
							}
							$userSession->per_page=4;				
							$userSession->user_level=6;
							$userSession->keyword=$session_msg;
							$userSession->c_parent_id=$session_msg;
							$userSession->is_child=1;
							$userSession->save();
							
							
						}
						if($userSession->level_6 == "3")//My Ticket
						{
							if($moreKeyword)
							{
								$userSession->page_no = $userSession->page_no+4;
							}
							else
							{
								$userSession->last_parent_id=$userSession->parent_id;
							}
							
							$output['session_operation'] = "continue";
							$info_Bookings = $this->GetActiveBooking($userSession->msisdn, $userSession->page_no);
							//dd($info_Bookings);
							$menuBooking = "";
							foreach($info_Bookings as $info_Booking)
							{
								$menuBooking .= $info_Booking->rownum.". ".$info_Booking->from_terminal_name." to ".$info_Booking->to_terminal_name."\n";
							}
							
							$info_Bookings = $this->GetActiveBooking($userSession->msisdn, $userSession->page_no+4);
							
							$userSession->more_keyword=0;
							if($info_Bookings)
							{
								$menuBooking .= ($userSession->page_no+5).". More\n";
								$userSession->more_keyword=($userSession->page_no+5);
							}
							$output['session_msg'] = "Choose your Booking".chr(58)."\nPlease select".chr(58)."\n".$menuBooking."* Back";
							$userSession->per_page=4;
							$userSession->user_level=6;
							$userSession->keyword=$session_msg;
							$userSession->parent_id=$session_msg;
							$userSession->is_child=1;
							$userSession->save();
							
						}
						else
						if($userSession->level_6 == "2")//Booking Status
						{
							$output['session_operation'] = "continue";							
							$output['session_msg'] = "Enter Booking Reference Code\n\n99. Main Menu";
							$userSession->per_page=4;
							$userSession->user_level=6;
							$userSession->keyword=$session_msg;
							$userSession->parent_id=$session_msg;
							$userSession->is_child=1;
							$userSession->save();
							
						}
						
					break;
					
					case 3: //Transporters > Terminals > Cities
						
						if($moreKeyword)
						{
							$userSession->page_no = $userSession->page_no+4;
							$info_FromCity = City::Where('id',$userSession->from_city_id)->Get();
						}
						else
						{
							$userSession->c_last_parent_id=$userSession->c_parent_id;
							
							if($userSession->company_ussd_type == "1")
							{
								$info_FromCity = $this->GetSelectedCompanyCity($userSession->company_id,$session_msg);
							}
							else
							{
								$info_FromCity = $this->GetSelectedCompanyCityAPI($userSession->company_id,$session_msg);
								
								//print_r($info_FromCity);
								//dd($info_FromCity[0]->id);
							}
						}
						
						$output['session_operation'] = "continue";
						
						$per_page = 4;
						if($userSession->company_ussd_type == "1")
						{
							$info_Terminals = $this->GetCompanyCityTerminals($userSession->company_id,$info_FromCity[0]->id, $userSession->page_no);
						}
						else
						{
							$info_Terminals = $this->GetCompanyCityTerminalsAPI($userSession->company_id,$info_FromCity[0]->id, $userSession->company_api_url, $userSession->company_api_token, $userSession->page_no, $per_page);
						}
						
						$menuTerminals = "";
						foreach($info_Terminals as $Terminals)
						{
							$menuTerminals .= $Terminals->rownum.". ".ucfirst(strtolower($Terminals->name))."\n";
						}
												
						if($userSession->company_ussd_type == "1")
						{
							$info_Terminals = $this->GetCompanyCityTerminals($userSession->company_id,$info_FromCity[0]->id, $userSession->page_no+4);
						}
						else
						{
							$info_Terminals = $this->GetCompanyCityTerminalsAPI($userSession->company_id,$info_FromCity[0]->id, $userSession->company_api_url, $userSession->company_api_token, $userSession->page_no+4, $per_page);
						}
						
						$userSession->more_keyword=0;
						if($info_Terminals)
						{
							$menuTerminals .= ($userSession->page_no+5).". More\n";
							$userSession->more_keyword=($userSession->page_no+5);
						}
						$output['session_msg'] = $info_FromCity[0]->name."\n".$menuTerminals."* Back";
						
						$userSession->from_city_id=$info_FromCity[0]->id;
						$userSession->from_city_name=$info_FromCity[0]->name;
						$userSession->per_page=$per_page;
						$userSession->user_level=6;
						$userSession->keyword=$session_msg;
						$userSession->c_parent_id=$session_msg;
						$userSession->is_child=1;
						$userSession->save();
					break;
					
					case 4: //Transporters > Service > Detail
						$userSession->c_last_parent_id=$userSession->c_parent_id;
						$userSession->user_level=6;
						$userSession->save();
						
						$action = "Services";
						if($userSession->ussd_code=="*".$session_from)
						{
							$action = "Transporters";
						}
						
						$this->updateCustomerLog($testing, "update customer_logs set is_complete=1, action='$action' where id=".$userSession->session_id);
						
						$info_Deal = $this->GetSelectedCservice($userSession->company_id,$session_msg);
						$output['session_operation'] = "endcontinue";		
						
						$smsMessage = $userSession->Company()->First()->name."\nService".chr(58)."\n".$info_Deal[0]->name."\n".$info_Deal[0]->detail;//."\n* Back\n0 Main Menu";

						$Charge = $this->ChargeUser($session_msisdn, "*".$session_from);
						
						if($Charge != 0)
						{
							$this->sendSMS($session_from,$session_msisdn, $smsMessage, $gateway);
							$output['session_msg'] = "Thanks, your request has being received. You will receive SMS response shortly";
						}
						else
						{
							$output = $this->emptyBalanceMessage($session_msisdn);
						}
					break;

					case 5: //Transporters > Promo & Deals
						
						$userSession->c_last_parent_id=$userSession->c_parent_id;
						$userSession->user_level=6;
						$userSession->save();
						
						$action = "Promo & Deals";
						if($userSession->ussd_code=="*".$session_from)
						{
							$action = "Transporters";
						}
						
						$this->updateCustomerLog($testing, "update customer_logs set is_complete=1, action='$action' where id=".$userSession->session_id);
						
						$info_Deal = $this->GetSelectedDeal($userSession->company_id,$session_msg);
						$output['session_operation'] = "endcontinue";		
						
						$smsMessage = $userSession->Company()->First()->name."\nPromo & Deals".chr(58)."\n".$info_Deal[0]->name."\n".$info_Deal[0]->detail;//."\n* Back\n0 Main Menu";
						$Charge = $this->ChargeUser($session_msisdn, "*".$session_from);
						if($Charge != 0)
						{
							$this->sendSMS($session_from,$session_msisdn, $smsMessage, $gateway);
							$output['session_msg'] = "Thanks, your request has being received. You will receive SMS response shortly";

						}
						else
						{
							$output = $this->emptyBalanceMessage($session_msisdn);
						}
						
					break;
					case 6: //Transporters > Policy Statement
						
						$userSession->c_last_parent_id=$userSession->c_parent_id;
						$userSession->user_level=6;
						$userSession->save();
						
						$action = "Policy Statement";
						if($userSession->ussd_code=="*".$session_from)
						{
							$action = "Transporters";
						}
						$this->updateCustomerLog($testing, "update customer_logs set is_complete=1, action='$action' where id=".$userSession->session_id);
						
						$info_Policy = $this->GetSelectedPolicy($userSession->company_id,$session_msg);
						$output['session_operation'] = "endcontinue";		
						
						$smsMessage = $userSession->Company()->First()->name."\nPolicy".chr(58)."\n".$info_Policy[0]->name."\n".$info_Policy[0]->detail;//."\n* Back\n0 Main Menu";
						$Charge = $this->ChargeUser($session_msisdn, "*".$session_from);
						if($Charge != 0)
						{
							$this->sendSMS($session_from,$session_msisdn, $smsMessage, $gateway);
							$output['session_msg'] = "Thanks, your request has being received. You will receive SMS response shortly";
						}
						else
						{
							$output = $this->emptyBalanceMessage($session_msisdn);
						}
						
					break;
					
					case 7: //Transporters > About
						
						$userSession->c_last_parent_id=$userSession->c_parent_id;
						$userSession->user_level=6;
						$userSession->save();
						
						$action = "About US";
						if($userSession->ussd_code=="*".$session_from)
						{
							$action = "Transporters";
						}
						$this->updateCustomerLog($testing, "update customer_logs set is_complete=1, action='$action' where id=".$userSession->session_id);
						
						$info_Policy = $this->GetSelectedAbout($userSession->company_id,$session_msg);
						$output['session_operation'] = "endcontinue";		
						
						$smsMessage = $userSession->Company()->First()->name."\nAbout US".chr(58)."\n".$info_Policy[0]->name."\n".$info_Policy[0]->detail;//."\n* Back\n0 Main Menu";
						$Charge = $this->ChargeUser($session_msisdn, "*".$session_from);
						if($Charge != 0)
						{
							$this->sendSMS($session_from,$session_msisdn, $smsMessage, $gateway);
							$output['session_msg'] = "Thanks, your request has being received. You will receive SMS response shortly";
						}
						else
						{
							$output = $this->emptyBalanceMessage($session_msisdn);
						}
						
					break;
					
					case 8: //Transporters > Exit
						//Already ended
					break;
				}
				//****************************************
				// End Transporters
				//****************************************
				
			break;
			
			case 2: //Schedule & Fare > Company List
				
				$correctDate = 0;
				if($moreKeyword)
				{
					$userSession->page_no = $userSession->page_no+4;
					$info_Date = $userSession->schedule_date;
				}
				else
				{
					$userSession->last_parent_id=$userSession->parent_id;
					if($session_msg >= 1 && $session_msg <= 5)
					{
						$correctDate = 1;
						$date = date('D d M', mktime(0, 0, 0, date("m") , date("d")-1,date("Y")));
						$info_Date = date('Y-m-d', strtotime("+$session_msg day", strtotime($date)));
					}
					else
					{
						if($this->isDate($session_msg))
						{
							$correctDate = 1;
							$date = DateTime::createFromFormat('d/m/Y', $session_msg);
							$info_Date = $date->format('Y-m-d');
						}
					}
				}
				
				$output['session_operation'] = "continue";
				
				if($correctDate == 1)
				{
					$info_Companies = $this->GetCompanies($userSession->page_no);
					$menuCompany = "";
					foreach($info_Companies as $Company)
					{
						$menuCompany .= $Company->rownum.". ".$Company->name."\n";
					}
					
					$info_Companies = $this->GetCompanies($userSession->page_no+4);
					
					$userSession->more_keyword=0;
					if($info_Companies)
					{
						$menuCompany .= ($userSession->page_no+5).". More\n";
						$userSession->more_keyword=($userSession->page_no+5);
					}
					$output['session_msg'] = "Transporters".chr(58)."\nPlease select".chr(58)."\n".$menuCompany."* Back";
					
					$userSession->schedule_date = $info_Date;
					$userSession->per_page=4;
					$userSession->user_level=6;
					$userSession->keyword=$session_msg;
					$userSession->parent_id=$session_msg;
					$userSession->is_child=1;
					$userSession->save();
				}
				else
				{
					$output['session_msg'] = "Date format should be dd/mm/yyyy";
				}
				
			break;
			
			case 3: //Promo & Deals
				//Already ended	
			break;
			case 4: //Infotainment
				//Already ended	
			break;
		}
		return $output;
	}
	
	public function Level7($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing)
	{
		if(!$moreKeyword)
			$userSession->level_7=$session_msg;
		//Uncomment echo "Menu 7";
		
		//Change page no on level change, check if level is same else change page no
		if($userSession->user_level != 7)
		{
			$userSession->page_no = 0;
		}
		
		switch($userSession->s_parent)
		{
			
			case 1: //Transporters
				//****************************************
				// Start Transporters Level 4
				//****************************************
				switch($userSession->c_parent)
				{
					
					case 1: //Transporters > Schedule > Date List
						
						if($moreKeyword)
						{
							$userSession->page_no = $userSession->page_no+$userSession->per_page;
							$perPage = $userSession->per_page;
							
							if($userSession->company_ussd_type == "1")
							{
								$info_FromTerminal = Terminal::Where('id',$userSession->from_terminal_id)->Get();
							}
							else
							{
								$info_FromTerminal = $this->GetSelectedFromTerminalByIDAPI($userSession->company_id, $userSession->from_terminal_id);
							}
							//dd($info_FromTerminal);
						}
						else
						{
							$userSession->last_parent_id=$userSession->parent_id;
							
							if($userSession->company_ussd_type == "1")
							{
								$info_FromTerminal = $this->GetSelectedFromTerminal($userSession->company_id, $userSession->from_city_id, $userSession->to_city_id, $session_msg);
							}
							else
							{
								$info_FromTerminal = $this->GetSelectedFromTerminalAPI($userSession->company_id, $userSession->from_city_id, $userSession->to_city_id, $session_msg);
							}
							
						}
						
						$output['session_operation'] = "continue";
						
						
						if($userSession->company_ussd_type == "1")
						{
							$info_Cities = $this->GetCompanyToCities($userSession->from_city_id, $userSession->company_id, $userSession->page_no);
						}
						else
						{
							$info_Cities = $this->GetCompanyToCitiesAllRouteDetailsAPI($userSession->from_city_id, $info_FromTerminal[0]->id, $userSession->company_id, $userSession->page_no);
						}
						//dd($info_Cities);
						$menuCity = "";
						foreach($info_Cities as $City)
						{
							$menuCity .= $City->rownum.". ".ucfirst(strtolower($City->name))."\n";
						}
						
						if($userSession->company_ussd_type == "1")
						{
							$info_Cities = $this->GetCompanyToCities($userSession->from_city_id, $userSession->company_id, $userSession->page_no+7);
						}
						else
						{
							$info_Cities = $this->GetCompanyToCitiesAllRouteDetailsAPI($userSession->from_city_id, $info_FromTerminal[0]->id, $userSession->company_id, $userSession->page_no+7);
						}
						
						$userSession->more_keyword=0;

						if($info_Cities)
						{
							$menuCity .= ($userSession->page_no+8).". More\n";
							$userSession->more_keyword=($userSession->page_no+8);
						}
						$output['session_msg'] = "To".chr(58)."\n".$menuCity."* Back";
						
						$userSession->from_terminal_id=$info_FromTerminal[0]->id;
						$userSession->from_terminal_name=$info_FromTerminal[0]->name;
						  
						$userSession->per_page=7;
						$userSession->user_level=7;
						$userSession->keyword=$session_msg;
						$userSession->c_parent_id=$session_msg;
						$userSession->is_child=1;
						$userSession->save();	
						
					break;
					case 2: //Transporters > My Bookings 
					
						if($userSession->level_6 == "1")//Booking
						{
							//Schedule List
							$perPage = 1000;
						
							$userSession->last_parent_id=$userSession->parent_id;
								
							$info_Search = $this->GetSelectedScheduleSearch($userSession->msisdn,$session_msg);
							//dd($info_Search);
							$userSession->from_city_id = $info_Search[0]->from_city_id;
							$userSession->from_city_name = $info_Search[0]->from_city_name;
							$userSession->from_terminal_id = $info_Search[0]->from_terminal_id;
							$userSession->from_terminal_name = $info_Search[0]->from_terminal_name;
							$userSession->to_city_id = $info_Search[0]->to_city_id;
							$userSession->to_city_name = $info_Search[0]->to_city_name;
							$userSession->to_terminal_id = $info_Search[0]->to_terminal_id;
							$userSession->to_terminal_name = $info_Search[0]->to_terminal_name;
							$userSession->schedule_date = $info_Search[0]->schedule_date;
							$userSession->company_id = $info_Search[0]->company_id;
							$userSession->company_name = $info_Search[0]->company_name;
							
							$userSession->discount_code = $info_Search[0]->discount_code;
							$userSession->couponvalue = $info_Search[0]->couponvalue;
							$userSession->usePercent = $info_Search[0]->usePercent;
							$userSession->couponpercent = $info_Search[0]->couponpercent;
							
							$userSession->save();
							
							$output['session_operation'] = "continue";
							
							
							$info_Company = $userSession->Company()->First();
							$company_Ussd_Code = $info_Company->Company_ussd()->First()->code;
							$output['session_msg'] = $info_Company->name."\n".$userSession->from_city_name." to ".$userSession->to_city_name."\n".date_format(date_create($userSession->schedule_date),"D d M Y")."\nTerminal: ".$userSession->to_terminal_name." \n\nEnter Bus No:";
							
							$userSession->per_page=$perPage;
							$userSession->user_level=7;
							$userSession->keyword=$session_msg;
							$userSession->c_parent_id=$session_msg;
							$userSession->is_child=1;
							$userSession->save();
						}
						if($userSession->level_6 == "3")//My Booking
						{
							
							$userSession->c_last_parent_id=$userSession->c_parent_id;
							$userSession->user_level=7;
							$userSession->save();
							
							$action = "My Bookings";
							if($userSession->ussd_code=="*".$session_from)
							{
								$action = "Transporters";
							}
							
							$this->updateCustomerLog($testing, "update customer_logs set is_complete=1, action='$action' where id=".$userSession->session_id);
							
							$info_Booking = $this->GetSelectedActiveBooking($userSession->msisdn,$session_msg);
							$info_Company = Company::FindOrFail($info_Booking[0]->company_id);
							$output['session_operation'] = "endcontinue";		
							$smsMessage = $info_Booking[0]->Fullname."\nRef: ".$info_Booking[0]->refcode." \n".$info_Company->name.": \n".$info_Booking[0]->from_terminal_name." to ".$info_Booking[0]->to_terminal_name." \n".date('M d Y', strtotime($info_Booking[0]->schedule_date))." \n".$info_Booking[0]->dept_time." \n".$info_Booking[0]->bus." \nSeat Number: ".$info_Booking[0]->SelectedSeats;
							//dd($smsMessage);
							$Charge = $this->ChargeUser($session_msisdn, "*".$session_from);
							if($Charge != 0)
							{
								$this->sendSMS($session_from,$session_msisdn, $smsMessage, $gateway);
								$output['session_msg'] = "Thanks, your request has being received. You will receive SMS response shortly";
							}
							else
							{
								$output = $this->emptyBalanceMessage($session_msisdn);
							}
						}
						else
						if($userSession->level_6 == "2")//Booking Status
						{   
							$userSession->c_last_parent_id=$userSession->c_parent_id;
							$userSession->user_level=7;
							$userSession->save();
							
							$action = "My Status";
							if($userSession->ussd_code=="*".$session_from)
							{
								$action = "Transporters";
							}
							
							$this->updateCustomerLog($testing, "update customer_logs set is_complete=1, action='$action' where id=".$userSession->session_id);
							
							//echo "<br />Call GetSelectedCompanyCityTerminalAPI<br />";
							$url = env('API_URL')."BookingStatus/".$userSession->company_id."?Phone=".$userSession->msisdn."&RefCode=".$session_msg;
							
							$options = array(
							  'http'=>array(
								'method'=>"GET",
								'header'=>"Authorization: Bearer ".env('API_TOKEN')
							  )
							);
							
							$context = stream_context_create($options);
							$data = file_get_contents($url, false, $context);
							
							$decodeData = json_decode($data, true);
							
							$output['session_operation'] = "endcontinue";							
							$smsMessage = $userSession->company_name."\nRef: ".$session_msg."\n".$decodeData["data"]["Details"][0]["Loading_Office"]." to ".$decodeData["data"]["Details"][0]["Destination"]."\nAddress: ".$decodeData["data"]["Details"][0]["Terminal"][0]["Address"]."\nDepart: ".$decodeData["data"]["Details"][0]["Departure_Time"]."\nSeat: ".$decodeData["data"]["Details"][0]["OrderDetails"]["ordertotalseat"]."\nAmount: ".$decodeData["data"]["Details"][0]["OrderDetails"]["fareamount"]."\nName: ".$decodeData["data"]["Details"][0]["CustomerDetails"][0]["Name"]."\nPhone: ".$decodeData["data"]["Details"][0]["OrderDetails"]["phonenumber"]."\nStatus: ".$decodeData["data"]["Details"][0]["OrderDetails"]["responsecode"];
														
							$Charge = $this->ChargeUser($session_msisdn, "*".$session_from);
							if($Charge != 0)
							{
								$this->sendSMS($session_from,$session_msisdn, $smsMessage, $gateway);
								$output['session_msg'] = "Thanks, your request has being received. You will receive SMS response shortly";
							}
							else
							{
								$output = $this->emptyBalanceMessage($session_msisdn);
							}
							
						}
					break;
					case 3: //Transporters > Terminals > Detail 
						$userSession->c_last_parent_id=$userSession->c_parent_id;
						$userSession->user_level=7;
						$userSession->save();
						
						$action = "Terminals";
						if($userSession->ussd_code=="*".$session_from)
						{
							$action = "Transporters";
						}
						
						$this->updateCustomerLog($testing, "update customer_logs set is_complete=1, action='$action', from_city_id='".$userSession->from_city_id."', from_city_name='".$userSession->from_city_name."' where id=".$userSession->session_id);

						if($userSession->company_ussd_type == "1")
						{
							$info_Terminal = $this->GetSelectedCompanyCityTerminal($userSession->company_id,$userSession->from_city_id, $session_msg);
						}
						else
						{
							$info_Terminal = $this->GetSelectedCompanyCityTerminalAPI($userSession->company_id,$userSession->from_city_id, $userSession->company_api_url, $userSession->company_api_token, $session_msg);
							
						}
						$output['session_operation'] = "endcontinue";		
						
						$smsMessage = $userSession->Company()->First()->name.": ".$info_Terminal[0]->name."\n".$info_Terminal[0]->address."\n".$info_Terminal[0]->phone_number;//."\n* Back\n0 Main Menu";
						$Charge = $this->ChargeUser($session_msisdn, "*".$session_from);
						
						if($Charge != 0)
						{
							$this->sendSMS($session_from,$session_msisdn, $smsMessage, $gateway);
							$output['session_msg'] = "Thanks, your request has being received. You will receive SMS response shortly";
						}
						else
						{
							$output = $this->emptyBalanceMessage($session_msisdn);
						}
					break;
					case 4: //Transporters > Company Services
							//Already ended	
					break;
					case 5: //Transporters > Promo & Deals
							//Already ended	
					break;
					
					case 6: //Transporters > Policy Statement
						//Already ended	
					break;
					
					case 7: //Transporters > About
						//Already ended	
					break;
					
					case 8: //Transporters > Exit
						//Already ended	
					break;
				}
				//****************************************
				// End Transporters
				//****************************************
				
			break;
			
			case 2: //Schedule & Fare > From Termonal
				
				if($moreKeyword)
				{
					$userSession->page_no = $userSession->page_no+4;
					$info_Company = Company::Where('id',$userSession->company_id)->Get();
				}
				else
				{
					$userSession->last_parent_id=$userSession->parent_id;
					$info_Company = $this->GetSelectedCompany($session_msg);
				}
				
				if($backKeyword)
				{
					$info_Company = Company::Where('id',$userSession->company_id)->Get();
				}
				
				$output['session_operation'] = "continue";
								
				
				if($userSession->company_ussd_type == "1")
				{
					$info_Terminals = $this->GetFromTerminals($info_Company[0]->id, $userSession->from_city_id, $userSession->to_city_id, $userSession->page_no);
				}
				else
				{
					$info_Terminals = $this->GetFromTerminalsAPI($info_Company[0]->id, $userSession->from_city_id, $userSession->to_city_id, $userSession->page_no);
				}
				$menuTerminal = "";
				foreach($info_Terminals as $Terminal)
				{
					$menuTerminal .= $Terminal->rownum.". ".$Terminal->name."\n";
				}
				
				if($userSession->company_ussd_type == "1")
				{
					$info_Terminals = $this->GetFromTerminals($info_Company[0]->id, $userSession->from_city_id, $userSession->to_city_id, $userSession->page_no+4);
				}
				else
				{
					$info_Terminals = $this->GetFromTerminalsAPI($info_Company[0]->id, $userSession->from_city_id, $userSession->to_city_id, $userSession->page_no+4);
				}
				
				$userSession->more_keyword=0;
				if($info_Terminals)
				{
					$menuTerminal .= ($userSession->page_no+5).". More\n";
					$userSession->more_keyword=($userSession->page_no+5);
				}
				if($menuTerminal=="")
				{
					$output['session_msg'] = "No service\n* Back";
				}
				else
				{
					$output['session_msg'] = "Departure Terminal".chr(58)."\n".$menuTerminal."* Back";
				}
				
				$userSession->company_id=$info_Company[0]->id;
				$userSession->company_name=$info_Company[0]->name;
				$userSession->per_page=4;
				$userSession->user_level=7;
				$userSession->keyword=$session_msg;
				$userSession->parent_id=$session_msg;
				$userSession->is_child=1;
				$userSession->save();
				
				
			break;
			
			case 3: //Promo & Deals
				//Already ended	
			break;
			case 4: //Infotainment
				//Already ended	
			break;
		}
		
		return $output;
	}
	
	public function Level8($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing)
	{
		if(!$moreKeyword)
			$userSession->level_8=$session_msg;
		//Uncomment echo "Menu 8";
		
		//Change page no on level change, check if level is same else change page no
		if($userSession->user_level != 8)
		{
			$userSession->page_no = 0;
		}
		
		switch($userSession->s_parent)
		{
			
			case 1: //Transporters
				//****************************************
				// Start Transporters Level 5
				//****************************************
				switch($userSession->c_parent)
				{
					case 1: //Transporters > Schedule > From Terminal
						$perPage = 1000;
						
						if($moreKeyword)
						{
							$userSession->page_no = $userSession->page_no+7;
							
							if($userSession->company_ussd_type == "1")
							{
								$info_ToCity = City::Where('id',$userSession->to_city_id)->Get();
							}
							else
							{
								$info_ToCity = $this->GetCompanyCityByIDAllRouteDetailsAPI($userSession->company_id, $userSession->to_city_id);
								$info_Schedules = ["1"=>"1"];
							}
							//dd($info_ToCity);
						}
						else
						{
							$userSession->c_last_parent_id=$userSession->c_parent_id;
							
							
							if($userSession->company_ussd_type == "1")
							{
								$info_ToCity = $this->GetSelectedCompanyToCity($userSession->from_city_id,$userSession->company_id,$session_msg);
								
								//Get schedules
								$info_Schedules = $this->GetScheduleCountByCitiesCompany($userSession->company_id,$userSession->from_city_id,$info_ToCity[0]->id);
							}
							else
							{
								$info_ToCity = $this->GetSelectedCompanyToCityAllRouteDetailsAPI($userSession->from_city_id,$userSession->from_terminal_id, $userSession->company_id,$session_msg);
								//dd($info_ToCity);
								$info_Schedules = ["1"=>"1"];
							}
							
							//dd($info_FromTerminal);
						}
 						$output['session_operation'] = "continue";
						
						if($userSession->company_ussd_type == "1")
						{
							$info_Terminals = $this->GetToTerminals($userSession->company_id, $userSession->from_city_id, $info_ToCity[0]->id, $userSession->page_no);
						}
						else
						{
							$info_Terminals = $this->GetToTerminalsAPI($userSession->company_id, $userSession->from_city_id, $info_ToCity[0]->id, $userSession->from_terminal_id, $userSession->page_no);
						}
						$menuTerminal = "";   
						foreach($info_Terminals as $Terminal)
						{
							$menuTerminal .= $Terminal->rownum.". ".ucfirst(strtolower($Terminal->name))."\n";
						}
						
						if($userSession->company_ussd_type == "1")
						{
							$info_Terminals = $this->GetToTerminals($userSession->company_id, $userSession->from_city_id, $info_ToCity[0]->id, $userSession->page_no+7);
						}
						else
						{
							$info_Terminals = $this->GetToTerminalsAPI($userSession->company_id, $userSession->from_city_id, $info_ToCity[0]->id, $userSession->from_terminal_id, $userSession->page_no+7);
						}
						//dd($info_Terminals);
						$userSession->more_keyword=0;
						if($info_Terminals)
						{
							$menuTerminal .= ($userSession->page_no+8).". More\n";
							$userSession->more_keyword=($userSession->page_no+8);
						}
						if($menuTerminal=="")

						{
							$output['session_msg'] = "No service\n* Back";
						}
						else
						{
							$output['session_msg'] = ucfirst($info_ToCity[0]->name)." Terminal".chr(58)."\n".$menuTerminal."* Back";
						}
						
						$userSession->to_city_id=$info_ToCity[0]->id;
						$userSession->to_city_name=$info_ToCity[0]->name;
						
						$userSession->per_page=7;
						$userSession->user_level=8;
						$userSession->keyword=$session_msg;
						$userSession->parent_id=$session_msg;
						$userSession->is_child=1;
						$userSession->save();
						
						if(sizeof($info_Schedules)==0)
						{
							$action = "Schedule & Fare";
							if($userSession->ussd_code=="*".$session_from)
							{
								$action = "Transporters";
							}
							
							$this->updateCustomerLog($testing, "update customer_logs set is_complete=0, action='$action', from_city_id='".$userSession->from_city_id."', from_city_name='".$userSession->from_city_name."', to_city_id='".$info_ToCity[0]->id."', to_city_name='".$info_ToCity[0]->name."' where id=".$userSession->session_id);
							
							$output['session_operation'] = "endcontinue";
							$output['session_msg'] = "The Route Selected for this company does not exist".chr(58)."\n* Back\n0 Main Menu";
						}
						
					break;
					case 2: //Transporters > Bookings > How many travellers
						if($userSession->level_6 == "1")//Booking
						{
							
							$info_CompanySchedule = $this->GetSelectedCompanyScheduleAPI($userSession->company_id,$userSession->schedule_date,$userSession->from_terminal_id,$userSession->to_terminal_id,$userSession->to_city_id,$session_msg);
							
							if($info_CompanySchedule)
							{
								$info_Company = $userSession->Company()->First();
								$output['session_operation'] = "continue";	
								
								//."\n".$userSession->from_city_name." to ".$userSession->to_city_name
								$output['session_msg'] = $userSession->company_name.chr(58)."\n".$userSession->from_terminal_name." to ".$userSession->to_terminal_name."\n".$userSession->schedule_date."\n".$info_CompanySchedule[0]["dept_time"]."\n".$info_CompanySchedule[0]["bus"]."\n".sizeof($info_CompanySchedule[0]["AvailableSeat"])." Seats\n"."1. Confirm\n99. Main Menu";
								//dd($output['session_msg']);
								$userSession->per_page=4;
								$userSession->available_seats=json_encode($info_CompanySchedule[0]["AvailableSeat"]);
								$userSession->block_seats=json_encode($info_CompanySchedule[0]["BlockedSeat"]);
								$userSession->fare=$info_CompanySchedule[0]["fare"];
								$userSession->tripid=$info_CompanySchedule[0]["TripID"];
								$userSession->destid=$info_CompanySchedule[0]["DestinationID"];
								$userSession->orderid=$info_CompanySchedule[0]["OrderID"];
								$userSession->user_level=8;
								$userSession->keyword=$session_msg;
								$userSession->c_parent_id=$session_msg;
								$userSession->is_child=1;
								//dd($userSession);
								$userSession->save();		
								
							}
							else
							{
								$output['session_operation'] = "endcontinue";
								$output['session_msg'] = "Invalid booking request";
							}
						}
						else //Status
						{
							$output['session_operation'] = "continue";
							$output['session_msg'] = "Booking Status"."* Back";
						}
					break;
					case 3: //Transporters > Company Services
						//Already ended	
					break;
					case 4: //Transporters > Terminals
							//Already ended
					break;
					case 5: //Transporters > Promo & Deals
							//Already ended	
					break;
					
					case 6: //Transporters > Policy Statement
						//Already ended	
					break;
					
					case 7: //Transporters > About
						//Already ended	
					break;
					
					case 8: //Transporters > Exit
						//Already ended	
					break;
				}
				//****************************************
				// End Transporters
				//****************************************
			break;
			
			case 2: //Schedule & Fare > Schedules list
				
				$perPage = 1000;
				
				if($moreKeyword)
				{
					$userSession->page_no = $userSession->page_no+$userSession->per_page;
					$perPage = $userSession->per_page;
					$info_FromTerminal = Terminal::Where('id',$userSession->from_terminal_id)->Get();
				}
				else
				{
					$userSession->last_parent_id=$userSession->parent_id;
					
					if($userSession->company_ussd_type == "1")
					{
						$info_FromTerminal = $this->GetSelectedFromTerminal($userSession->company_id, $userSession->from_city_id, $userSession->to_city_id, $session_msg);
					}
					else
					{
						$info_FromTerminal = $this->GetSelectedFromTerminal($userSession->company_id, $userSession->from_city_id, $userSession->to_city_id, $session_msg);




						//$info_FromTerminal = $this->GetSelectedFromTerminalAPI($userSession->company_id, $userSession->from_city_id, $userSession->to_city_id, $session_msg);
					}
				}

				$output['session_operation'] = "continue";
				
				$info_Schedules = $this->GetCompanySchedule($userSession->company_id,$userSession->schedule_date,$info_FromTerminal[0]->id,$userSession->to_city_id,$userSession->page_no, $perPage);
				$menuSchedule = "";
				$menuScheduleCount = 0;
				foreach($info_Schedules as $info_Schedule)
				{
					$menuScheduleCount++;
					$menuSchedule .= $info_Schedule['rownum'].", ".$info_Schedule['dept_time'].", ".$info_Schedule['bus'].", N".$info_Schedule['fare'].", Avail".chr(58)." ".$info_Schedule['AvailableSeatCounts']." Seats\n";
				}
				
				$info_SchedulesMore = $this->GetCompanySchedule($userSession->company_id,$userSession->schedule_date,$info_FromTerminal[0]->id,$userSession->to_city_id,$userSession->page_no+$perPage, $perPage);
				
				$userSession->more_keyword=0;
				if($info_SchedulesMore)
				{
					//$menuSchedule .= ($userSession->page_no+($perPage+1)).". More\n";
					$userSession->more_keyword=($userSession->page_no+($perPage+1));
				}
				if($menuSchedule!="")
				{
					$info_Company = $userSession->Company()->First();
					$company_Ussd_Code = $info_Company->Company_ussd()->First()->code;
					$output['session_msg'] = $info_Company->name."\n".$userSession->from_terminal_name." to ".$userSession->to_terminal_name."\n".date_format(date_create($userSession->schedule_date),"D d M Y")."\nSchedules".chr(58)." ".$info_Schedules[0]['terminal_name']."\nBus Found (".$menuScheduleCount.")\n".$menuSchedule."\n\n* To book a bus, dial *".$company_Ussd_Code."*BusNo# e.g. For Bus 1, dial *".$company_Ussd_Code."*1#";
				}
				
				if($menuSchedule!="" && strlen($output['session_msg'])>154)
				{
					$perPage = 1000;
					
					$info_Schedules = $this->GetCompanySchedule($userSession->company_id,$userSession->schedule_date,$info_FromTerminal[0]->id,$userSession->to_city_id,$userSession->page_no, $perPage);
					$menuSchedule = "";
					$menuScheduleCount=0;
					foreach($info_Schedules as $info_Schedule)
					{
						$menuScheduleCount++;
						$menuSchedule .= $info_Schedule['rownum'].", ".$info_Schedule['dept_time'].", ".$info_Schedule['bus'].", N".$info_Schedule['fare'].", Avail".chr(58)." ".$info_Schedule['AvailableSeatCounts']." Seats\n";
					}
					
					$info_SchedulesMore = $this->GetCompanySchedule($userSession->company_id,$userSession->schedule_date,$info_FromTerminal[0]->id,$userSession->to_city_id,$userSession->page_no+$perPage, $perPage);
					
					$userSession->more_keyword=0;
					if($info_SchedulesMore)
					{
						//$menuSchedule .= ($userSession->page_no+($perPage+1)).". More\n";
						$userSession->more_keyword=($userSession->page_no+($perPage+1));
					}
					if($menuSchedule!="")
					{
						$info_Company = $userSession->Company()->First();
						$company_Ussd_Code = $info_Company->Company_ussd()->First()->code;
						$output['session_msg'] = $info_Company->name."\n".$userSession->from_terminal_name." to ".$userSession->to_terminal_name."\n".date_format(date_create($userSession->schedule_date),"D d M Y")."\nSchedules".chr(58)." ".$info_Schedules[0]['terminal_name']."\nBus Found (".$menuScheduleCount.")\n".$menuSchedule."* To book a bus, dial *".$company_Ussd_Code."*BusNo# e.g. For Bus 1, dial *".$company_Ussd_Code."*1#";
					}
				}
				
				
				$userSession->from_terminal_id=$info_FromTerminal[0]->id;
				$userSession->from_terminal_name=$info_FromTerminal[0]->name;
				$userSession->per_page=$perPage;
				$userSession->user_level=8;
				$userSession->keyword=$session_msg;
				$userSession->parent_id=$session_msg;
				$userSession->is_child=1;
				$userSession->save();
				
				if($menuSchedule=="")
				{
					$this->updateCustomerLog($testing, "update customer_logs set is_complete=0, action='Schedule & Fare', company_id='".$userSession->company_id."', company_name='".$userSession->company_name."', from_city_id='".$userSession->from_city_id."', from_city_name='".$userSession->from_city_name."', to_city_id='".$userSession->to_city_id."', to_city_name='".$userSession->to_city_name."' where id=".$userSession->session_id);
					
					$output['session_operation'] = "endcontinue";
					$output['session_msg'] = "Sorry no schedule exist".chr(58)."\n".$menuSchedule."\n* Back\n0 Main Menu";
				}
				else
				{
					$smsMessage = $output['session_msg'];
					$Charge = $this->ChargeUser($session_msisdn, "*".$session_from);
					if($Charge != 0)
					{
						$this->sendSMS($session_from,$session_msisdn, $smsMessage, $gateway);
						$output['session_msg'] = "Thanks, your request has being received. You will receive SMS response shortly";
						
												
						//Store data for booking next
						
						$ResultIns = DB::insert("INSERT INTO ussd_menu_schedule_search (msisdn, ussd_code, from_city_id, from_city_name, from_terminal_id, from_terminal_name, to_terminal_id, to_terminal_name, to_city_id, to_city_name, schedule_date, company_id, company_name, schedule_id, fare_id, record_on) select msisdn, ussd_code, from_city_id, from_city_name, from_terminal_id, from_terminal_name, to_terminal_id, to_terminal_name, to_city_id, to_city_name, schedule_date, company_id, company_name, schedule_id, fare_id, record_on from ussd_menu_session where msisdn = '$session_msisdn'");
						
					}
					else
					{
						$output = $this->emptyBalanceMessage($session_msisdn);
					}
					
				}
			break;
			
			case 3: //Promo & Deals
				//Already ended	
			break;
			case 4: //Infotainment
				//Already ended	
			break;
		}
		
		return $output;
	}
	
	
	public function Level9($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing)
	{
		if(!$moreKeyword)
			$userSession->level_9=$session_msg;
		//Uncomment echo "Menu 9";
		
		//Change page no on level change, check if level is same else change page no
		if($userSession->user_level != 9)
		{
			$userSession->page_no = 0;
		}
		
		switch($userSession->s_parent)
		{
			
			case 1: //Transporters
				//****************************************
				// Start Transporters Level 6
				//****************************************
				switch($userSession->c_parent)
				{
					case 1: //Transporters > Schedule > To Terminal
						
						if($moreKeyword)
						{
							$userSession->page_no = $userSession->page_no+$userSession->per_page;
							$perPage = $userSession->per_page;
							if($userSession->company_ussd_type == "1")
							{
								$info_ToTerminal = Terminal::Where('id',$userSession->to_terminal_id)->Get();
							}
							else
							{
								$info_ToTerminal = $this->GetSelectedToTerminalByIDAPI($userSession->company_id, $userSession->to_terminal_id);
							}

						}
						else
						{
							$userSession->last_parent_id=$userSession->parent_id;
							
							if($userSession->company_ussd_type == "1")
							{
								$info_ToTerminal = $this->GetSelectedToTerminal($userSession->company_id, $userSession->from_city_id, $userSession->to_city_id, $session_msg);
								//$info_Schedules = $this->GetScheduleCountByCitiesCompany($userSession->company_id,$userSession->from_city_id,$userSession->from_city_id);
							}
							else
							{
								$info_ToTerminal = $this->GetSelectedToTerminalAPI($userSession->company_id, $userSession->from_city_id, $userSession->to_city_id, $userSession->from_terminal_id, $session_msg);
								//$info_Schedules = ["1"=>"1"];
							}
							
						}
						
						$output['session_operation'] = "continue";
						
						$date = date('D d M', mktime(0, 0, 0, date("m") , date("d")-1,date("Y")));
						$menuDate = "";
						
						$info_Company = $userSession->Company()->First();
						$booking_no_of_days = $info_Company->booking_no_of_days;
							
						if($userSession->company_ussd_type == "1")
						{
							
							for($i=$userSession->page_no+1; $i < ($userSession->page_no+$booking_no_of_days); $i++)
							{
								$menuDate .= $i.". ".date('D d M', strtotime("+$i day", strtotime($date)))."\n";
							}
						}
						else
						{
							$info_GetGeneralSetting = $this->GetGeneralSetting($userSession->company_id);						
							
							$begin = new DateTime($info_GetGeneralSetting['StartDate']);
							$end = new DateTime($info_GetGeneralSetting['EndDate']);
							
							$menuDateArr = array();
							$j = 0;
							for($i = $begin; $i <= $end; $i->modify('+1 day')){
								if($j <= $booking_no_of_days)
								{
									$menuDateArr[] = $i->format("D d M");
								}
								$j++;
							}
							
							for($i=$userSession->page_no+1; $i < sizeof($menuDateArr); $i++)
							{
								$menuDate .= $i.". ".$menuDateArr[$i-1]."\n";
							}
							//dd($menuDateArr);
						}
						//$menuDate .= $i.". More\n";
						//$userSession->more_keyword=$i;
						
						//$menuDate .= "Or Reply with dd/mm/yyyy\n";
						$userSession->more_keyword=0;
						
						$output['session_msg'] = "Date".chr(58)."\n".$menuDate."* Back";
						$userSession->to_terminal_id=$info_ToTerminal[0]->id;
						$userSession->to_terminal_name=$info_ToTerminal[0]->name;
						$userSession->per_page=5;
						$userSession->user_level=9;
						$userSession->keyword=$session_msg;
						$userSession->c_parent_id=$session_msg;
						$userSession->is_child=1;
						$userSession->save();
	
					break;
					case 2: //Transporters > Bookings > No of seats
					
						if($userSession->level_6 == "1")//Booking
						{
							if($moreKeyword)
							{
								$userSession->page_no = $userSession->page_no+$userSession->per_page;
								$infoSeatsList = $this->AvailableSeatPaging(json_decode($userSession->available_seats, true),json_decode($userSession->block_seats, true),$userSession->page_no);
							}
							else
							{
								$userSession->last_parent_id=$userSession->parent_id;
								$infoSeatsList = $this->AvailableSeatPaging(json_decode($userSession->available_seats, true),json_decode($userSession->block_seats, true),1);
							}
							
							$output['session_operation'] = "continue";
							
							if(sizeof(json_decode($userSession->available_seats, true)) > $userSession->page_no+20)
							{							
								$output['session_msg'] = "Seats (".implode(', ', $infoSeatsList).")\n Choose Seat(s)".chr(58)." (e.g 6 for seat 6)\n99 More\n* Back";
								$userSession->more_keyword=99;
							}
							else
							{
								$output['session_msg'] = "Seats (".implode(', ', $infoSeatsList).")\n Choose Seat(s)".chr(58)." (e.g 6 for seat 6)\n* Back";
								$userSession->more_keyword=0;
							}
							$userSession->per_page=20;
							$userSession->user_level=9;
							$userSession->keyword=$session_msg;
							$userSession->c_parent_id=$session_msg;
							$userSession->is_child=1;
							$userSession->save();	
						}
						else //Status
						{
							$output['session_operation'] = "continue";
							$output['session_msg'] = "Booking Status"."* Back";
						}
					break;
					case 3: //Transporters > Company Services
						//Already ended	
					break;
					case 4: //Transporters > Terminals
							//Already ended
					break;
					case 5: //Transporters > Promo & Deals
							//Already ended	
					break;
					
					case 6: //Transporters > Policy Statement
						//Already ended	
					break;
					
					case 7: //Transporters > About
						//Already ended	
					break;
					
					case 8: //Transporters > Exit
						//Already ended	
					break;
				}
				//****************************************
				// End Transporters
				//****************************************
			break;
			
			case 2: //Schedule & Fare > Schedule detail
				$userSession->c_last_parent_id=$userSession->c_parent_id;
				
				$output['session_operation'] = "endcontinue";
				
				$info_CompanySchedule = $this->GetSelectedCompanySchedule($userSession->company_id,$userSession->schedule_date,$userSession->from_terminal_id,$userSession->to_city_id,$session_msg);

				$info_Company = Company::FindOrFail($userSession->company_id);
				$info_Schedule = Schedule::FindOrFail($info_CompanySchedule[0]['sch_id']);
				$info_Fare =  Fare::FindOrFail($info_CompanySchedule[0]['fid']);
				
				$BusName = $info_Schedule->Bus()->First()->name;
				$FareType = $info_Fare->Fare_type()->First()->name;


				$ServiceName = $info_Fare->Fare_type()->First()->Service()->First()->name;
				
				$FirstSchTime = $info_Schedule->Schedule_time()->Orderby('sequence')->First();
				$DeptTime = date('h:iA',strtotime($FirstSchTime->departure_time));
				$DeptDate = date('D d M',strtotime($info_CompanySchedule[0]['dept_time']));				
				
				$LastSchTime = $info_Schedule->Schedule_time()->Orderby('sequence', 'desc')->First();
				$ArrvTime = date('h:iA',strtotime($LastSchTime->arrival_time));
				$time1 = strtotime($FirstSchTime->departure_time);
				$time2 = strtotime($LastSchTime->arrival_time);
				$interval = $time1-$time2;
				if($interval<0)
					$interval = abs($interval);
				
				$ArrvDate =  date('D d M',strtotime($FirstSchTime->departure_time)+$interval);
				
				$OtherSchTime="";
				foreach($info_Schedule->Schedule_time()->where('sequence','>','1')->Orderby('sequence')->Get() as $Schedult_time)
				{
					//$OtherSchTime .= $Schedult_time->Terminal()->First()->name." ".date('h:iA',strtotime($Schedult_time->arrival_time))."\n";
					//Just show the last one
					$OtherSchTime = $Schedult_time->Terminal()->First()->name." ".date('h:iA',strtotime($Schedult_time->arrival_time))."\n";
				}
				
				$this->updateCustomerLog($testing, "update customer_logs set is_complete=1, action='Schedule & Fare', company_id='".$userSession->company_id."', company_name='".$userSession->company_name."', from_city_id='".$userSession->from_city_id."', from_city_name='".$userSession->from_city_name."', to_city_id='".$userSession->to_city_id."', to_city_name='".$userSession->to_city_name."', route_id='".$info_Schedule->Route()->First()->id."', route_name='".$info_Schedule->Route()->First()->name."', schedule_id='".$info_Schedule->id."', schedule_name='".$info_Schedule->short_name."' where id=".$userSession->session_id);
				
				$output['session_msg'] = "1. ".$info_Company->name."".chr(58)."\n".$userSession->from_city_name." to ".$userSession->to_city_name."\n\n".$BusName."\n".$FareType." ".$ServiceName." N".$info_CompanySchedule[0]['fare']."\n\nDept".chr(58)."\n".$DeptDate."\n".$FirstSchTime->Terminal()->First()->name." ".$DeptTime."\n\nArrv".chr(58)."\n".$ArrvDate."\n".$OtherSchTime."\n* Back\n0 Main Menu";
				
				$userSession->schedule_id=$info_CompanySchedule[0]['sch_id'];
				$userSession->fare_id=$info_CompanySchedule[0]['fid'];

				$userSession->user_level=9;
				$userSession->keyword=$session_msg;
				$userSession->parent_id=$session_msg;
				$userSession->is_child=1;
				$userSession->save();
			break;
			
			case 3: //Promo & Deals
				//Already ended	
			break;
			case 4: //Infotainment
				//Already ended	
			break;
		}
		
		return $output;

	}
	
	public function Level10($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing)
	{
		if(!$moreKeyword)
			$userSession->level_10=$session_msg;
		//Uncomment echo "Menu 10";
		
		//Change page no on level change, check if level is same else change page no
		if($userSession->user_level != 10)
		{
			$userSession->page_no = 0;
		}
		
		switch($userSession->s_parent)
		{
			
			case 1: //Transporters
				//****************************************
				// Start Transporters Level 4
				//****************************************
				switch($userSession->c_parent)
				{
					case 1: //Transporters > Schedule > To Terminal
						
						$perPage = 1000;
						
						if($moreKeyword)
						{
							$userSession->page_no = $userSession->page_no+4;
							$info_Date = $userSession->schedule_date;
						}
						else
						{
							if($userSession->company_ussd_type == "1")
							{
								$userSession->c_last_parent_id=$userSession->c_parent_id;
								if($session_msg >= 1 && $session_msg <= 5)
								{
									$correctDate = 1;
									$date = date('D d M', mktime(0, 0, 0, date("m") , date("d")-1,date("Y")));
									$info_Date = date('Y-m-d', strtotime("+$session_msg day", strtotime($date)));
								}
								else
								{
									if($this->isDate($session_msg))
									{
										$correctDate = 1;
										$date = DateTime::createFromFormat('d/m/Y', $session_msg);
										$info_Date = $date->format('Y-m-d');
									}
								}
							}
							else
							{
								$info_GetGeneralSetting = $this->GetGeneralSetting($userSession->company_id);						
								
								$info_Company = $userSession->Company()->First();
								$booking_no_of_days = $info_Company->booking_no_of_days;
						
								$begin = new DateTime($info_GetGeneralSetting['StartDate']);
								$end = new DateTime($info_GetGeneralSetting['EndDate']);
								
								$menuDateArr = array();
								$j = 0;
								for($i = $begin; $i <= $end; $i->modify('+1 day')){
									if($j <= $booking_no_of_days)
									{
										$menuDateArr[] = $i->format("D d M Y");
									}
									$j++;
								}
								if($session_msg >= 1 && $session_msg <= 5)
								{
									$correctDate = 1;
									$info_Date = date('Y-m-d', strtotime($menuDateArr[$session_msg-1]));

								}
								else
								{
									if($this->isDate($session_msg))
									{
										$correctDate = 1;
										$date = DateTime::createFromFormat('d/m/Y', $session_msg);
										$info_Date = $date->format('Y-m-d');
									}
								}
							}
							
							//dd($info_ToTerminal);
						}
						
						$output['session_operation'] = "continue";
						
						if($correctDate == 1)
						{
							$output['session_operation'] = "endcontinue";
							$output['session_msg'] = "Thanks for choosing ".$userSession->Company()->First()->name.". You will receive SMS with Buses available shortly.";
							
							dispatch(new FinalBookNewMessage($userSession, $session_from,$session_msisdn, $gateway, $info_Date));
						}
					break;
					
					case 2: //Transporters > Bookings > Full name
					
						if($userSession->level_6 == "1")//Booking
						{
							$correctMessage = true;
							$partMessages = explode(",",$session_msg);
							//dd($partMessages);
							foreach($partMessages as $partMessage)
							{
								if(is_numeric($partMessage))
								{
									if(!in_array($partMessage, json_decode($userSession->available_seats, true)))
									{
										$correctMessage = false;
									}
								}
								else
								{
									if(!in_array("'".$partMessage."'", json_decode($userSession->available_seats, true)))
									{
										$correctMessage = false;
									}
								}
							}

							//dd($correctMessage);
							
							
							$blockMessage = false;
							foreach($partMessages as $partMessage)
							{
								if(is_numeric($partMessage))
								{
									
									if(in_array($partMessage, json_decode($userSession->block_seats, true)))
									{
										$blockMessage = true;
									}
								}
								else
								{
									if(in_array("'".$partMessage."'", json_decode($userSession->block_seats, true)))
									{
										$blockMessage = true;
									}
								}
							}
							//dd($blockMessage);
							if(!$correctMessage)
							{
								if($moreKeyword)
								{
									$userSession->page_no = $userSession->page_no+$userSession->per_page;
									$infoSeatsList = $this->AvailableSeatPaging(json_decode($userSession->available_seats, true),json_decode($userSession->block_seats, true),$userSession->page_no);
								}
								else
								{
									$userSession->last_parent_id=$userSession->parent_id;
									$infoSeatsList = $this->AvailableSeatPaging(json_decode($userSession->available_seats, true),json_decode($userSession->block_seats, true),1);
								}
								
								$output['session_operation'] = "continue";
								
								if(sizeof(json_decode($userSession->available_seats, true)) > $userSession->page_no+20)
								{							
									$output['session_msg'] = "Wrong Reply.\nSeats (".implode(', ', $infoSeatsList).")\n Choose Seat(s)".chr(58)." (e.g 6 for seat 6)\n99 More\n* Back";
									$userSession->more_keyword=99;
								}
								else
								{
									$output['session_msg'] = "Wrong Reply.\nSeats (".implode(', ', $infoSeatsList).")\n Choose Seat(s)".chr(58)." (e.g 6 for seat 6)\n* Back";
									$userSession->more_keyword=0;
								}
								$userSession->per_page=20;
								$userSession->user_level=9;
								$userSession->keyword=$session_msg;
								$userSession->c_parent_id=$session_msg;
								$userSession->is_child=1;
								$userSession->save();
							}
							else
							if($blockMessage)
							{
								//Check block seat////////////
								if($moreKeyword)
								{
									$userSession->page_no = $userSession->page_no+$userSession->per_page;
									$infoSeatsList = $this->AvailableSeatPaging(json_decode($userSession->available_seats, true),json_decode($userSession->block_seats, true),$userSession->page_no);
								}
								else
								{
									$userSession->last_parent_id=$userSession->parent_id;
									$infoSeatsList = $this->AvailableSeatPaging(json_decode($userSession->available_seats, true),json_decode($userSession->block_seats, true),1);
								}
								
								$output['session_operation'] = "continue";
								
								if(sizeof(json_decode($userSession->available_seats, true)) > $userSession->page_no+20)
								{							
									$output['session_msg'] = "Seat already taken.\nSeats (".implode(', ', $infoSeatsList).")\n Choose Seat(s)".chr(58)." (e.g 6 for seat 6)\n99 More\n* Back";
									$userSession->more_keyword=99;
								}
								else
								{
									$output['session_msg'] = "Seat already taken.\nSeats (".implode(', ', $infoSeatsList).")\n Choose Seat(s)".chr(58)." (e.g 6 for seat 6)\n* Back";
									$userSession->more_keyword=0;
								}
								$userSession->per_page=20;
								$userSession->user_level=9;
								$userSession->keyword=$session_msg;
								$userSession->c_parent_id=$session_msg;
								$userSession->is_child=1;
								$userSession->save();		
								/////////////////////////////
							}
							else
							{
								
								//Remove Seats
								/*$url = env('API_URL')."ActivateLockSeat/".$userSession->company_id."?Destinationid=".$userSession->destid."&SelectedSeats=".$session_msg."&TripID=".$userSession->tripid."&lockedby=".$userSession->orderid;
								$options = array(
								  'http'=>array(
									'method'=>"GET",
									'header'=>"Authorization: Bearer ".env('API_TOKEN')
								  )
								);
								
								$context = stream_context_create($options);
								$data = file_get_contents($url, false, $context);
								
								$decodeData = json_decode($data, true);*/
								//End Remove seats
								
								$output['session_operation'] = "continue";	
								$output['session_msg'] = "Enter Booking Phone number\nReply 1 for this phone number\n.* Back";
								
								$userSession->no_of_traveler=sizeof(explode(",",$session_msg));
								$userSession->no_of_seats=trim(str_replace(" ", "",$session_msg));				
								$userSession->user_level=10;
								$userSession->keyword=$session_msg;
								$userSession->c_parent_id=$session_msg;
								$userSession->is_child=1;
								$userSession->save();
							}
						}
						else //Status
						{
							$output['session_operation'] = "continue";
							$output['session_msg'] = "Booking Status"."* Back";
						}
					break;
					case 3: //Transporters > Company Services
						//Already ended	
					break;
					case 4: //Transporters > Terminals
							//Already ended
					break;
					case 5: //Transporters > Promo & Deals
							//Already ended	
					break;
					
					case 6: //Transporters > Policy Statement
						//Already ended	
					break;
					
					case 7: //Transporters > About
						//Already ended	
					break;
					
					case 8: //Transporters > Exit
						//Already ended	
					break;
				}
				//****************************************
				// End Transporters
				//****************************************
			break;
			
			case 2: //Schedule & Fare
				//Already ended
			break;
			
			case 3: //Promo & Deals
				//Already ended	
			break;
			case 4: //Infotainment
				//Already ended	
			break;
		}
		

		return $output;
	}
	
	public function Level11($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing)
	{
		if(!$moreKeyword)
			$userSession->level_11=$session_msg;
		//Uncomment echo "Menu 10";
		
		//Change page no on level change, check if level is same else change page no
		if($userSession->user_level != 11)
		{
			$userSession->page_no = 0;
		}
		
		switch($userSession->s_parent)
		{
			
			case 1: //Transporters
				//****************************************
				// Start Transporters Level 4
				//****************************************
				switch($userSession->c_parent)
				{
					case 1: //Transporters > Schedule > Schedule detail
						
						$userSession->c_last_parent_id=$userSession->c_parent_id;
						
						$output['session_operation'] = "endcontinue";
						
						
						//Database schedule
						if($userSession->company_ussd_type == "1")
						{
							$info_CompanySchedule = $this->GetSelectedCompanySchedule($userSession->company_id,$userSession->schedule_date,$userSession->from_terminal_id,$userSession->to_city_id,$session_msg);
							$info_Company = Company::FindOrFail($userSession->company_id);
							$info_Schedule = Schedule::FindOrFail($info_CompanySchedule[0]['sch_id']);
							$info_Fare =  Fare::FindOrFail($info_CompanySchedule[0]['fid']);
							
							$BusName = $info_Schedule->Bus()->First()->name;
							$FareType = $info_Fare->Fare_type()->First()->name;
							$ServiceName = $info_Fare->Fare_type()->First()->Service()->First()->name;
							
							$FirstSchTime = $info_Schedule->Schedule_time()->Orderby('sequence')->First();
							$DeptTime = date('h:iA',strtotime($FirstSchTime->departure_time));
							$DeptDate = date('D d M',strtotime($info_CompanySchedule[0]['dept_time']));				
							
							$LastSchTime = $info_Schedule->Schedule_time()->Orderby('sequence', 'desc')->First();
							$ArrvTime = date('h:iA',strtotime($LastSchTime->arrival_time));
							$time1 = strtotime($FirstSchTime->departure_time);
							$time2 = strtotime($LastSchTime->arrival_time);
							$interval = $time1-$time2;
							if($interval<0)
								$interval = abs($interval);
						
							$ArrvDate =  date('D d M',strtotime($FirstSchTime->departure_time)+$interval);
							
							$OtherSchTime="";
							foreach($info_Schedule->Schedule_time()->where('sequence','>','1')->Orderby('sequence')->Get() as $Schedult_time)
							{
								//$OtherSchTime .= $Schedult_time->Terminal()->First()->name." ".date('h:iA',strtotime($Schedult_time->arrival_time))."\n";
								//Just show the last one
								$OtherSchTime = $Schedult_time->Terminal()->First()->name." ".date('h:iA',strtotime($Schedult_time->arrival_time))."\n";
							}
							
							$action = "Schedule & Fare";
							if($userSession->ussd_code=="*".$session_from)
							{
								$action = "Transporters";
							}
							
							$this->updateCustomerLog($testing, "update customer_logs set is_complete=1, action='$action', from_city_id='".$userSession->from_city_id."', from_city_name='".$userSession->from_city_name."', to_city_id='".$userSession->to_city_id."', to_city_name='".$userSession->to_city_name."', route_id='".$info_Schedule->Route()->First()->id."', route_name='".$info_Schedule->Route()->First()->name."', schedule_id='".$info_Schedule->id."', schedule_name='".$info_Schedule->short_name."' where id=".$userSession->session_id);
							
							$output['session_msg'] = "1. ".$info_Company->name."".chr(58)."\n".$userSession->from_city_name." to ".$userSession->to_city_name."\n\n".$BusName."\n".$FareType." ".$ServiceName." N".$info_CompanySchedule[0]['fare']."\n\nDept".chr(58)."\n".$DeptDate."\n".$FirstSchTime->Terminal()->First()->name." ".$DeptTime."\n\nArrv".chr(58)."\n".$ArrvDate."\n".$OtherSchTime."\n* Back\n0 Main Menu";
						}
						else //API schedule
						{
							$info_CompanySchedule = $this->GetSelectedCompanyScheduleAPI($userSession->company_id,$userSession->schedule_date,$userSession->from_terminal_id,$userSession->to_terminal_id,$userSession->to_city_id,$session_msg);
													
							$output['session_msg'] = "1. ".$userSession->company_name."".chr(58)."\n".$userSession->from_city_name." to ".$userSession->to_city_name."\n\n".$info_CompanySchedule[0]["bus"]."\n N".$info_CompanySchedule[0]['fare']."\n\nDept".chr(58)."\n".$info_CompanySchedule[0]["dept_time"]."\n".$info_CompanySchedule[0]["terminal_name"]." ".$info_CompanySchedule[0]["dept_time"]."\n\n* Back\n0 Main Menu";
							
							//dd($info_CompanySchedule);
						}

						$userSession->schedule_id=$info_CompanySchedule[0]['sch_id'];
						$userSession->fare_id=$info_CompanySchedule[0]['fid'];
						
						$userSession->user_level=11;
						$userSession->keyword=$session_msg;
						$userSession->parent_id=$session_msg;
						$userSession->is_child=1;
						$userSession->save();
					break;
					
					case 2: //Transporters > Bookings >  Next of Kin Full name:
						if($userSession->level_6 == "1")//Booking
						{
							$output['session_operation'] = "continue";	
							$output['session_msg'] = "Enter Your Fullname (e.g. John Mark)\n.* Back";
							if($session_msg=="1")
								$userSession->booking_msisdn=$userSession->msisdn;				
							else
								$userSession->booking_msisdn=$session_msg;	
							$userSession->user_level=11;
							$userSession->keyword=$session_msg;
							$userSession->c_parent_id=$session_msg;
							$userSession->is_child=1;
							$userSession->save();
							
							//If user exist in booking the then skip name & kin info
							if($userSession->company_ussd_type == "1")
							{
								$info_Booking = Booking::Where('msisdn',$userSession->booking_msisdn)->First();
							}
							else
							{
								$url = env('API_URL')."ValidatePhone/".$userSession->company_id."?Msisdn=".$userSession->booking_msisdn;
								$options = array(
								  'http'=>array(
									'method'=>"GET",
									'header'=>"Authorization: Bearer ".env('API_TOKEN')
								  )
								);
								
								$context = stream_context_create($options);
								$data = file_get_contents($url, false, $context);
								
								$decodeData = json_decode($data, true);
								
								//dd($decodeData["data"][0]);
								$info_Booking = NULL;
								if(isset($decodeData["data"][0]))
								{
									$info_Booking = new \stdClass();
									$info_Booking->email = $decodeData["data"][0]["Email"];
									$info_Booking->Fullname = $decodeData["data"][0]["Name"];
									$info_Booking->nextKin = $decodeData["data"][0]["Next_Of_Kin_Name"];
									$info_Booking->nextKinPhone = $decodeData["data"][0]["Next_Of_Kin_Addy"];
									$info_Booking->Sex = $decodeData["data"][0]["Sex"];
								}
								
								//dd($info_Booking);
							}
							if($info_Booking && $info_Booking!=NULL)
							{
								$userSession->name=$info_Booking->Fullname;	
								$userSession->nextkin=$info_Booking->nextKin;	
								$userSession->nextkinnumber=$info_Booking->nextKinPhone;	
								//$userSession->Sex=$info_Booking->Sex;	
								//$userSession->email=$info_Booking->email;	
								$userSession->user_level=13;
								$userSession->keyword=$session_msg;
								$userSession->c_parent_id=$session_msg;
								$userSession->is_child=1;
								$userSession->last_skip_level=11;
								$userSession->new_customer='0'; 
								$userSession->save();
								
								$output = $this->Level14($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing);
								//dd($output);
								//dd(1);
							}
							else
							{
								$userSession->last_skip_level='';
								$userSession->new_customer='1'; 
								$userSession->save();
							}
						}
						else //Status
						{
							$output['session_operation'] = "continue";
							$output['session_msg'] = "Booking Status"."* Back";
						}
					break;
					case 3: //Transporters > Company Services
						//Already ended	
					break;
					case 4: //Transporters > Terminals
							//Already ended
					break;
					case 5: //Transporters > Promo & Deals
							//Already ended	
					break;
					
					case 6: //Transporters > Policy Statement
						//Already ended	
					break;
					
					case 7: //Transporters > About
						//Already ended	
					break;
					
					case 8: //Transporters > Exit
						//Already ended	
					break;
				}
				//****************************************
				// End Transporters
				//****************************************
			break;
			
			case 2: //Schedule & Fare
					//Already ended	
			break;
			
			case 3: //Promo & Deals
				//Already ended	
			break;
			case 4: //Infotainment
				//Already ended	
			break;
		}
		
		
		return $output;
	}
	
	public function Level12($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing)
	{
		if(!$moreKeyword)
			$userSession->level_12=$session_msg;
		//Uncomment echo "Menu 10";
		
		//Change page no on level change, check if level is same else change page no
		if($userSession->user_level != 12)
		{
			$userSession->page_no = 0;
		}
		
		switch($userSession->s_parent)
		{
			
			case 1: //Transporters
				//****************************************
				// Start Transporters Level 4
				//****************************************
				switch($userSession->c_parent)
				{
					case 1: //Transporters > Schedule > Schedule detail
						//Already ended	
					break;
					case 2: //Transporters > Bookings >  Next of Kin Full name:
						if($userSession->level_6 == "1")//Booking
						{
							$output['session_operation'] = "continue";	
							$output['session_msg'] = "Enter  Next of Kin  Full name and Phone no:\n(Full name,Phone no)\n.* Back";
							$userSession->name=$session_msg;				
							$userSession->user_level=13;
							$userSession->keyword=$session_msg;
							$userSession->c_parent_id=$session_msg;
							$userSession->is_child=1;
							$userSession->save();		
						}
						else //Status
						{
							$output['session_operation'] = "continue";
							$output['session_msg'] = "Booking Status"."* Back";
						}
					break;
					case 3: //Transporters > Company Services
						//Already ended	
					break;
					case 4: //Transporters > Terminals
							//Already ended
					break;
					case 5: //Transporters > Promo & Deals
							//Already ended	
					break;
					
					case 6: //Transporters > Policy Statement
						//Already ended	
					break;
					

					case 7: //Transporters > About
						//Already ended	
					break;
					
					case 8: //Transporters > Exit
						//Already ended	
					break;
				}
				//****************************************
				// End Transporters
				//****************************************
			break;
			
			case 2: //Schedule & Fare
					//Already ended	
			break;
			
			case 3: //Promo & Deals
				//Already ended	
			break;
			case 4: //Infotainment
				//Already ended	
			break;
		}
		

		return $output;
	}
	
	public function Level13($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing)
	{
		if(!$moreKeyword)
			$userSession->level_12=$session_msg;
		//Uncomment echo "Menu 10";
		
		//Change page no on level change, check if level is same else change page no
		if($userSession->user_level != 13)
		{
			$userSession->page_no = 0;
		}
		
		switch($userSession->s_parent)
		{
			
			case 1: //Transporters
				//****************************************
				// Start Transporters Level 4
				//****************************************
				switch($userSession->c_parent)
				{
					case 1: //Transporters > Schedule > Schedule detail
						//Already ended	
					break;
					case 2: //Transporters > Bookings > Fare Detail confirm
						if($userSession->level_6 == "1")//Booking
						{
							$output['session_operation'] = "continue";	
							$output['session_msg'] = "Enter Next of Kin phone no:\n.* Back";
							$userSession->nextkin=$session_msg;				
							$userSession->user_level=13;
							$userSession->keyword=$session_msg;
							$userSession->c_parent_id=$session_msg;
							$userSession->is_child=1;
							$userSession->save();								
						}
						else //Status
						{
							$output['session_operation'] = "continue";
							$output['session_msg'] = "Booking Status"."* Back";
						}
					break;
					case 3: //Transporters > Company Services
						//Already ended	
					break;
					case 4: //Transporters > Terminals
							//Already ended
					break;
					case 5: //Transporters > Promo & Deals
							//Already ended	
					break;
					
					case 6: //Transporters > Policy Statement
						//Already ended	
					break;
					
					case 7: //Transporters > About
						//Already ended	
					break;
					
					case 8: //Transporters > Exit
						//Already ended	
					break;
				}
				//****************************************
				// End Transporters
				//****************************************
			break;
			
			case 2: //Schedule & Fare
					//Already ended	
			break;
			
			case 3: //Promo & Deals
				//Already ended	
			break;
			case 4: //Infotainment
				//Already ended	
			break;
		}
		

		return $output;
	}
	
	public function Level14($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing)
	{
		if(!$moreKeyword)
			$userSession->level_14=$session_msg;
		//Uncomment echo "Menu 10";
		
		//Change page no on level change, check if level is same else change page no
		if($userSession->user_level != 14)
		{
			$userSession->page_no = 0;
		}
		
		switch($userSession->s_parent)
		{
			
			case 1: //Transporters
				//****************************************
				// Start Transporters Level 4
				//****************************************
				switch($userSession->c_parent)
				{
					case 1: //Transporters > Schedule > Schedule detail
						//Already ended	
					break;
					
					case 2: //Transporters > Bookings > Ticket Detail
						if($userSession->level_6 == "1")//Booking
						{
							//$info_CompanySchedule = $this->GetSelectedCompanyScheduleAPI($userSession->company_id,$userSession->schedule_date,$userSession->from_terminal_id,$userSession->to_terminal_id,$userSession->to_city_id,$userSession->level_8);
							if($userSession->last_skip_level == "")
							{
								$parseKin = explode(",",$session_msg);
								$userSession->nextkin=$parseKin[0];	
								$userSession->nextkinnumber=$parseKin[1];
								$userSession->save();
							}
							//dd($info_CompanySchedule);
							$info_Company = $userSession->Company()->First();
							
							$totalFare = 0;
							$channelAmount = 0;
							$gatewayAmount = 0;
							$ticketAmount = $userSession->no_of_traveler*$userSession->fare;
							
							if($info_Company->gateway_id=="1")//CorelPay
							{
								if($info_Company->gateway_fee_type=="2")//Set fees flat

								{
									$gatewayAmount = $info_Company->gateway_fee;
								}
								else //Percentage of ticket

								{
									$gatewayAmount = ($ticketAmount/100)*$info_Company->gateway_fee;
								}
							}
							
							if($info_Company->channel_fee_type=="1")//Set as percentage of total ticket amount  (e.g. 5% of ticket amount)

							{
								$channelAmount = ($ticketAmount/100)*$info_Company->channel_fee;
							}
							else
							if($info_Company->channel_fee_type=="2")//Set as flat amount per seat (e.g. 100*no of passengers)

							{
								$channelAmount = $userSession->no_of_traveler*$info_Company->channel_fee;
							}
							else //Set as a flat amount on total seats (e.g. 2000)

							{
								$channelAmount = $info_Company->channel_fee;
							}

							$output['session_operation'] = "continue";	
							
							$output['session_msg'] = "Confirm your details\nName: ".$userSession->name."\nPhone: ".$userSession->booking_msisdn."\nSeat Taken: ".$userSession->no_of_seats."\nNext of Kin: ".$userSession->nextkin."\nNext of Kin Phone: ".$userSession->nextkinnumber."\n1. Confirm\n* Back\n0 Main Menu";
							/*if($info_Company->charge_type == "1")//DO NOT ADD channel fee or gateway fee

							{
								//"\n".$userSession->from_city_name." to ".$userSession->to_city_name.
								//$output['session_msg'] = $userSession->company_name.chr(58)."\n".$userSession->from_terminal_name." to ".$userSession->to_terminal_name."\n".$userSession->schedule_date."\n".$info_CompanySchedule[0]["dept_time"]."\n".$info_CompanySchedule[0]["bus"]."\n".$userSession->no_of_traveler." Passengers"."\n"."Ticket Fee".chr(58)." N". ($ticketAmount) ."\n"."Seats ".$userSession->no_of_seats."\n"."1. Confirm\n* Back";
								
								
							}
							else //Inclusive in ticket amount (Total ticket alone)

							{
								//"\n".$userSession->from_city_name." to ".$userSession->to_city_name.
								//$output['session_msg'] = $userSession->company_name.chr(58)."\n".$userSession->from_terminal_name." to ".$userSession->to_terminal_name."\n".$userSession->schedule_date."\n".$info_CompanySchedule[0]["dept_time"]."\n".$info_CompanySchedule[0]["bus"]."\n".$userSession->no_of_traveler." Passengers"."\n"."Ticket Fee".chr(58)." N". ($ticketAmount + $gatewayAmount + $channelAmount) ."\n"."Seats ".$userSession->no_of_seats."\n"."1. Confirm\n* Back";
							}*/
							//dd($output['session_msg']);
							
							$userSession->gateway_fee = $gatewayAmount;	
							$userSession->channel_fee = $channelAmount;		
							$userSession->total_amount = ($ticketAmount + $gatewayAmount + $channelAmount);		
							$userSession->user_level=14;
							$userSession->keyword=$session_msg;
							$userSession->c_parent_id=$session_msg;
							$userSession->is_child=1;
							$userSession->save();						
						}
						else //Status
						{
							$output['session_operation'] = "continue";
							$output['session_msg'] = "Booking Status"."* Back";
						}
					break;
					case 3: //Transporters > Company Services
						//Already ended	
					break;
					case 4: //Transporters > Terminals
							//Already ended
					break;
					case 5: //Transporters > Promo & Deals
							//Already ended	
					break;
					
					case 6: //Transporters > Policy Statement
						//Already ended	
					break;
					
					case 7: //Transporters > About
						//Already ended	
					break;
					
					case 8: //Transporters > Exit
						//Already ended	
					break;
				}
				//****************************************
				// End Transporters
				//****************************************
			break;
			
			case 2: //Schedule & Fare
					//Already ended	
			break;
			
			case 3: //Promo & Deals
				//Already ended	
			break;
			case 4: //Infotainment
				//Already ended	
			break;
		}
		

		return $output;
	}
	
	public function Level15($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing)
	{
		if(!$moreKeyword)
			$userSession->level_15=$session_msg;
		//Uncomment echo "Menu 10";
		
		//Change page no on level change, check if level is same else change page no
		if($userSession->user_level != 15)
		{
			$userSession->page_no = 0;
		}
		
		switch($userSession->s_parent)
		{
			
			case 1: //Transporters
				//****************************************
				// Start Transporters Level 4
				//****************************************
				switch($userSession->c_parent)
				{
					case 1: //Transporters > Schedule > Schedule detail
						//Already ended	
					break;
					
					case 2: //Transporters > Bookings > Bank
						if($userSession->level_6 == "1")//Booking
						{
							
							if($moreKeyword)
							{
								$userSession->page_no = $userSession->page_no+6;
								//dd($moreKeyword);
							}
							else
							{
								$userSession->c_last_parent_id=$userSession->c_parent_id;
							}
							
							$output['session_operation'] = "continue";
							
							$info_Banks = $this->GetBanks($userSession->page_no);
							
							$countOfMenu = sizeof($info_Banks);
							$menuBanks = "";
							foreach($info_Banks as $Bank)
							{
								$menuBanks .= $Bank->rownum.". ".$Bank->name."\n";
							}
							
							$info_Banks = $this->GetBanks($userSession->page_no+6);
							
							
							$userSession->more_keyword=0;
							if($info_Banks)
							{
								$menuBanks .= ($userSession->page_no+7).". More\n";
								$userSession->more_keyword=($userSession->page_no+7);
							}
							$output['session_msg'] = "Choose Bank for payment".chr(58)."\n\n".$menuBanks."* Back";
							if($countOfMenu==0)
							{
								$action = "Banks";
								if($userSession->ussd_code=="*".$session_from)
								{
									$action = "Transporters";
								}
								$this->updateCustomerLog($testing, "update customer_logs set is_complete=0, action='$action' where id=".$userSession->session_id);
								
								$output['session_operation'] = "endcontinue";
								$output['session_msg'] = "No bank at the moment, please check later. \n* Back\n0 Main Menu";
							}
							//dd($output['session_msg']);
							$userSession->per_page=6;				
							$userSession->user_level=15;
							$userSession->keyword=$session_msg;
							$userSession->c_parent_id=$session_msg;
							$userSession->is_child=1;
							$userSession->save();							
						}
						else //Status
						{
							$output['session_operation'] = "continue";
							$output['session_msg'] = "Booking Status"."* Back";  
						}
					break;
					case 3: //Transporters > Company Services
						//Already ended	
					break;
					case 4: //Transporters > Terminals
							//Already ended
					break;
					case 5: //Transporters > Promo & Deals
							//Already ended	
					break;
					
					case 6: //Transporters > Policy Statement
						//Already ended	
					break;
					
					case 7: //Transporters > About
						//Already ended	
					break;

					
					case 8: //Transporters > Exit
						//Already ended	
					break;
				}
				//****************************************
				// End Transporters
				//****************************************
			break;
			
			case 2: //Schedule & Fare
					//Already ended	
			break;
			
			case 3: //Promo & Deals
				//Already ended	
			break;
			case 4: //Infotainment
				//Already ended	
			break;
		}
		

		return $output;
	}
	
	

	public function Level16($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing)
	{
		if(!$moreKeyword)
			$userSession->level_15=$session_msg;
		//Uncomment echo "Menu 10";
		
		//Change page no on level change, check if level is same else change page no
		if($userSession->user_level != 16)
		{
			$userSession->page_no = 0;
		}
		
		switch($userSession->s_parent)
		{
			
			case 1: //Transporters
				//****************************************
				// Start Transporters Level 4
				//****************************************
				switch($userSession->c_parent)
				{
					case 1: //Transporters > Schedule > Schedule detail
						//Already ended	
					break;
					
					case 2: //Transporters > Bookings > Fare Detail confirm
						if($userSession->level_6 == "1")//Booking
						{
							
							$info_Bank = $this->GetSelectedBank($session_msg);
							
							/*$url = env('API_URL')."SaveBookingOrder/".$userSession->company_id;
							
							$postdata = http_build_query(
								array(
									'TripID' => $userSession->tripid,
									'SelectedSeats' => $userSession->no_of_seats,
									'MaxSeat' => $userSession->no_of_traveler,
									'DestinationID' => $userSession->destid,
									'OrderID' => $userSession->orderid,
									'Fullname' => $userSession->name,
									'Phone' => '0'.substr($userSession->booking_msisdn,-10),
									'Email' => 'admin@iyconsoft.com',
									'nextKin' => '',
									'nextKinPhone' => '',
									'KinSelectedSeats' => '',
									'Sex' => $userSession->gender,
								)
							);
							$options = array(
							  'http'=>array(
								'header'=> "Content-type: application/x-www-form-urlencoded\r\n". "Content-Length: " . strlen($postdata) . "\r\nAuthorization: Bearer ".env('API_TOKEN'),
								'method'=>"POST",
								'content' => $postdata
							  )
							);
							$context = stream_context_create($options);
							$data = file_get_contents($url, false, $context);
							
							$decodeData = json_decode($data, true);*/
							
							//if($decodeData['status'] == '200')
							{
								$output['session_operation'] = "endcontinue";	
								$output['session_msg'] = "Thank you for choosing ".$userSession->company_name.". Your booking is being processed. You will receive your booking confirmation SMS shortly.";
								
								
								$info_Company = $userSession->Company()->First();
								$db_booking = new Booking;

								$totalFare = 0;
								$channelAmount = 0;
								$gatewayAmount = 0;
								$ticketAmount = $userSession->no_of_traveler*$userSession->fare;

								$discountAmount=0;
								$ActualAmount=$ticketAmount;
								
								if($userSession->usePercent == "1")
								{
									$ticketAmount = $ActualAmount - ($ticketAmount*($userSession->couponpercent/100));
									$discountAmount = $ActualAmount-$ticketAmount;
								}
								else
								{
									$ticketAmount = $ActualAmount - $userSession->couponvalue;
									$discountAmount = $ActualAmount-$ticketAmount;
								}

								$db_booking->discount = $discountAmount;
								
								if($info_Company->gateway_id=="1")//CorelPay
								{
									if($info_Company->gateway_fee_type=="2")//Set fees flat
	
									{
										$gatewayAmount = $info_Company->gateway_fee;
									}
									else //Percentage of ticket
	
									{
										$gatewayAmount = ($ticketAmount/100)*$info_Company->gateway_fee;
									}
								}
								
								if($info_Company->channel_fee_type=="1")//Set as percentage of total ticket amount  (e.g. 5% of ticket amount)
	
								{
									$channelAmount = ($ticketAmount/100)*$info_Company->channel_fee;
								}
								else
								if($info_Company->channel_fee_type=="2")//Set as flat amount per seat (e.g. 100*no of passengers)
	
								{
									$channelAmount = $userSession->no_of_traveler*$info_Company->channel_fee;
								}
								else //Set as a flat amount on total seats (e.g. 2000)
	
								{
									$channelAmount = $info_Company->channel_fee;
								}
								
								//Random Str
								$not_unique = true;
								$randomStr = '';
								while($not_unique)
								{
									$randomStr = $this->random_strings(4);
									$info_Refcode = Refcode::Create(['code' => $randomStr]);
									if($info_Refcode)
									{
										$not_unique=false;
									}
								}
								///////////////////////////////////////////////////
								//SAVE BOOKING
								///////////////////////////////////////////////////
								$db_booking->company_id = $userSession->company_id;
								//$db_booking->customer_id = 
								//$db_booking->schedule_id = 
								$db_booking->new_customer = $userSession->new_customer;
								$db_booking->msisdn = $userSession->booking_msisdn;
								$db_booking->TripID = $userSession->tripid;

								$db_booking->SelectedSeats = $userSession->no_of_seats;
								$db_booking->MaxSeat = $userSession->no_of_traveler;
								$db_booking->DestinationID = $userSession->destid;
								$db_booking->OrderID = $userSession->orderid;
								$db_booking->Fullname = $userSession->name;
								$db_booking->email = '';
								$db_booking->nextKin = $userSession->nextkin;
								$db_booking->nextKinPhone = $userSession->nextkinnumber;
								$db_booking->Sex = $userSession->gender;
								//$db_booking->tax = 
								$db_booking->charge_type = $info_Company->charge_type;
								$db_booking->channel_fee = $userSession->channel_fee;
								$db_booking->gateway_fee = $userSession->gateway_fee;
								
								$db_booking->refcode = '732'.$randomStr;
								$db_booking->status = 0;
								
								$db_booking->from_city_id = $userSession->from_city_id;
								$db_booking->from_city_name = $userSession->from_city_name;
								$db_booking->from_terminal_id = $userSession->from_terminal_id;
								$db_booking->from_terminal_name = $userSession->from_terminal_name;
								$db_booking->to_city_id = $userSession->to_city_id;
								$db_booking->to_city_name = $userSession->to_city_name;
								$db_booking->to_terminal_id = $userSession->to_terminal_id;
								$db_booking->to_terminal_name = $userSession->to_terminal_name;
								$db_booking->schedule_date = $userSession->schedule_date;
								$db_booking->dept_time = $userSession->dept_time;
								$db_booking->bus = $userSession->bus;
								
								$db_booking->gateway = $gateway;
								
								$db_booking->save();
								  
								
								///////////////////////////////////////////////////
								//END SAVE BOOKING
								///////////////////////////////////////////////////
								//"\n".$userSession->from_city_name." to ".$userSession->to_city_name.
								if($info_Company->charge_type == "1")//DO NOT ADD channel fee or gateway fee
								{	
									if(($gatewayAmount + $channelAmount) < $info_Company->minimum_fee)
									{
										$commission  = ($info_Company->minimum_fee);// * $userSession->no_of_traveler;
									}
									else
									{
										$commission = ($gatewayAmount + $channelAmount);
									}
									$db_booking->commission = $commission;
									
									$db_booking->sub_total = $ticketAmount;
									$db_booking->total = ($ticketAmount + $discountAmount);
									
									$BankAccount = $this->GetMonifyAccount($userSession->company_id, $db_booking->Fullname, $db_booking->email, $ticketAmount, $db_booking->id);
									$db_booking->traceId = $BankAccount->accountNumber;
									//"\n".$userSession->from_city_name." to ".$userSession->to_city_name.
									$smsMessage = "Dear ".$userSession->name.",\n\nPay with USSD. For ".$info_Bank[0]->name." Bank\ndial *".$info_Bank[0]->ussd_code."*000*732".($randomStr)."# or\nPay N".$db_booking->sub_total." with Bank Transfer, POS or ATM to GUO acct: ".$BankAccount->accountNumber.", Sterling Bank\n\nRef: ".$userSession->orderid."\n".$userSession->company_name.chr(58)."\n".$userSession->from_terminal_name." to ".$userSession->to_terminal_name."\n".$userSession->schedule_date."\n".$userSession->dept_time."\n".$userSession->bus."\n".$userSession->no_of_traveler." Passenger(s)"."\n"."T.Fee".chr(58)." N".($ticketAmount)."\nSeat No. ".$userSession->no_of_seats."\n\nPlease pay to validate booking. Booking valid for ".$info_Company->seat_block_time."mins.\n\nT.C. - 1.Arrive 30mins before departure 2.Max of 10kg luggage 3.No livestock.\nCall: +234 706 535 4195  if you need help.";
								}
								else //Exclusive in ticket amount (Total ticket alone)
	
								{
									if(($gatewayAmount + $channelAmount) < $info_Company->minimum_fee)
									{
										$commission  = ($info_Company->minimum_fee);// * $userSession->no_of_traveler;
									}
									else
									{
										$commission = ($gatewayAmount + $channelAmount);
									}
									$db_booking->commission = $commission;
									$db_booking->sub_total = $ticketAmount;
									$db_booking->total = ($ticketAmount + $commission);
									$BankAccount = $this->GetMonifyAccount($userSession->company_id, $db_booking->Fullname, $db_booking->email, $ticketAmount, $db_booking->id);
									$db_booking->traceId = $BankAccount->accountNumber;
									//"\n".$userSession->from_city_name." to ".$userSession->to_city_name.
									$smsMessage = "Dear ".$userSession->name.",\n\nPay with USSD. For ".$info_Bank[0]->name." Bank \ndial *".$info_Bank[0]->ussd_code."*000*732".($randomStr)."# or\nPay N".$db_booking->total." with Bank Transfer, POS or ATM to GUO acct: ".$BankAccount->accountNumber.", Sterling Bank\n\nRef: ".$userSession->orderid."\n".$userSession->company_name.chr(58)."\n".$userSession->from_terminal_name." to ".$userSession->to_terminal_name."\n".$userSession->schedule_date."\n".$userSession->dept_time."\n".$userSession->bus."\n".$userSession->no_of_traveler." Passenger(s)"."\n"."T. Fee".chr(58)." N".($ticketAmount)."\nSeat No. ".$userSession->no_of_seats."\n\nPlease pay to validate booking. Booking valid for ".$info_Company->seat_block_time."mins.\n\nT.C. - 1.Arrive 30mins before departure 2.Max of 10kg luggage 3.No livestock.\nCall: +234 706 535 4195  if you need help.";
								}
								
								\Log::info("Before CallBookingAPI gatewayAmount:".$gatewayAmount." channelAmount:".$channelAmount." charge_type:".$info_Company->charge_type." commission:".$db_booking->commission." sub_total:".$db_booking->sub_total." total:".$db_booking->total." ticketAmount:".$ticketAmount." userSession->fare:".$userSession->fare);
								$db_booking->save();
								//dd($smsMessage);
								dispatch(new CallBookingAPI($userSession));
								
								$this->sendSMS($session_from,$session_msisdn, $smsMessage, $gateway);
								
								$smsMessageNotify = $userSession->name."\n".$session_msisdn."\nRef: ".$userSession->orderid."\n".$userSession->company_name.chr(58)."\n".$userSession->from_terminal_name." to ".$userSession->to_terminal_name."\n".$userSession->schedule_date."\n".$userSession->no_of_traveler." Passengers"."\nSeat No. ".$userSession->no_of_seats."\n\n Payment String: dial *".$info_Bank[0]->ussd_code."*000*732".($randomStr)."#";
								$this->sendSMS($session_from,'2348096029576', $smsMessageNotify, '9mobile');
								$this->sendSMS($session_from,'2349063628281', $smsMessageNotify, 'mtn');
								$this->sendSMS($session_from,'2348066989805', $smsMessageNotify, '9mobile');
							}
							/*else
							{
								$output['session_operation'] = "endcontinue";	
								$output['session_msg'] = "Sorry Your booking was unsuccessful. Please book again";
							}*/
							
							$userSession->gender=$session_msg;				
							$userSession->user_level=16;
							$userSession->keyword=$session_msg;
							$userSession->c_parent_id=$session_msg;
							$userSession->is_child=1;
							$userSession->save();							
						}
						/*else //Status
						{
							$output['session_operation'] = "continue";
							$output['session_msg'] = "Booking Status"."* Back";
						}*/
					break;
					case 3: //Transporters > Company Services
						//Already ended	
					break;
					case 4: //Transporters > Terminals
							//Already ended
					break;
					case 5: //Transporters > Promo & Deals
							//Already ended	
					break;
					
					case 6: //Transporters > Policy Statement
						//Already ended	
					break;
					
					case 7: //Transporters > About
						//Already ended	
					break;

					
					case 8: //Transporters > Exit
						//Already ended	
					break;
				}
				//****************************************
				// End Transporters
				//****************************************
			break;
			
			case 2: //Schedule & Fare
					//Already ended	
			break;
			
			case 3: //Promo & Deals
				//Already ended	
			break;
			case 4: //Infotainment
				//Already ended	
			break;
		}
		

		return $output;
	}
	
	
	/*
	* /////////////////////////////////////////////////////////////
	* /////////////////////////////////////////////////////////////
	* /////////////////////////////////////////////////////////////
	*
	*	FUNTIONS STARTED
	*
	* /////////////////////////////////////////////////////////////
	* /////////////////////////////////////////////////////////////
	* /////////////////////////////////////////////////////////////
	*/
	public function GetCities($page)
	{
		$sql = "select * from (SELECT c.id, c.name, @rownum := @rownum + 1 AS rownum FROM cities c, (SELECT @rownum := 0) r order by sequence, name) data limit 4 offset ".$page;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	public function GetSelectedCity($rownum)
	{		
		$sql = "select * from (SELECT c.id, c.name, c.short_name, @rownum := @rownum + 1 AS rownum FROM cities c, (SELECT @rownum := 0) r order by sequence, name) data where rownum=".$rownum;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	
	
	public function GetFromTerminalsAPI($company_id, $city_id, $to_city_id, $page)
	{
		//echo "<br />Call GetFromTerminalsAPI<br />";
		$per_page = 7;
		$url = env('API_URL')."AllRouteDetails/".$company_id;
		$options = array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>"Authorization: Bearer ".env('API_TOKEN')
		  )
		);
		//dd($url);
		$context = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
		
		$decodeData = json_decode($data, true);
		
		$Response = array();
		$ResponseArr = array();
		$row = 1;
		$nOfRecords = 1;
		//echo "$row>=$page && $nOfRecords<=3<br />";
		
		foreach($decodeData["data"]["Terminals"] as $index => $Terminals)
		{
			//echo "$city_id ".$Terminals["StateID"]."<br />";
			if($city_id == $Terminals["StateID"])
			{
				$ResponseArr[] = array('rownum' => $row,
					'id' => $Terminals["LoadinID"], 
					'name' => $Terminals["Loading_Office"],  
					'short_name' => $Terminals["Loading_Office"], 
				);
				$nOfRecords ++;
				$row++;
			}
			
		}  
		//dd($ResponseArr);
		$row = 1;
		$nOfRecords = 1;
		foreach($ResponseArr as $index => $Terminals)
		{
			//echo "$row>$page && $nOfRecords<=$per_page<br />";
			if($row>$page && $nOfRecords<=$per_page)
			{
				$Response[] = array('rownum' => ($index+1),
					'id' => $Terminals["id"], 
					'name' => $Terminals["name"],  
					'short_name' => $Terminals["short_name"], 
				);
				$nOfRecords++;
			}
			$row++;
		}
		//if($page>0)
		//dd($Response);
		if(sizeof($Response) == 0)
			return [];
		else
			return $this->convertToObject($Response);
	}
	
	public function GetSelectedFromTerminalAPI($company_id, $city_id, $to_city_id, $rownum)
	{
		//echo "<br />Call GetFromTerminalsAPI<br />";
		$per_page = 7;
		$url = env('API_URL')."AllRouteDetails/".$company_id;
		$options = array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>"Authorization: Bearer ".env('API_TOKEN')
		  )
		);
		//dd($url);
		$context = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
		
		$decodeData = json_decode($data, true);
		
		$Response = array();
		$ResponseArr = array();
		$row = 1;
		$nOfRecords = 1;
		//echo "$row>=$page && $nOfRecords<=3<br />";
		
		
		foreach($decodeData["data"]["Terminals"] as $index => $Terminals)
		{
			if($city_id == $Terminals["StateID"])
			{
				$ResponseArr[] = array('rownum' => $row,
					'id' => $Terminals["LoadinID"], 
					'name' => $Terminals["Loading_Office"],  
					'short_name' => $Terminals["Loading_Office"], 
				);
				$nOfRecords ++;
				$row++;
			}
			
		}  
		
		$row = 1;
		$nOfRecords = 1;
		foreach($ResponseArr as $index => $Terminals)
		{
			if($row == $rownum)
			{
				$Response[] = array('rownum' => $index,
					'id' => $Terminals["id"], 
					'name' => $Terminals["name"],  
					'short_name' => $Terminals["short_name"], 
				);
			}
			$row++;
		}
		//dd($Response);
		if(sizeof($Response) == 0)
			return [];
		else
			return $this->convertToObject($Response);
	}
	
	public function GetToTerminalsAPI($company_id, $city_id, $to_city_id, $from_terminal, $page)
	{
		//echo "<br />Call GetToTerminalsAPI<br />";
		$per_page = 7;
		$url = env('API_URL')."AllRouteDetails/".$company_id;
		$options = array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>"Authorization: Bearer ".env('API_TOKEN')
		  )
		);
		//dd($url);
		$context = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
		
		$decodeData = json_decode($data, true);
		
		$Response = array();
		$ResponseArr = array();
		$row = 1;
		$nOfRecords = 1;
		//echo "$row>=$page && $nOfRecords<=3<br />";
		
		$fromLoadingDestinationIDArray = array();
		foreach($decodeData["data"]["Routes"] as $index => $Routes)
		{
			if($Routes["LoadinID"] == $from_terminal)
			{
				array_push($fromLoadingDestinationIDArray, $Routes["DestinationID"]);
			}
		}
		
		//Get to city terminal
		$ToTerminalsDestination = array();
		foreach($decodeData["data"]["Destinations"] as $index => $Destinations)
		{
			if($to_city_id == $Destinations["StateID"])
			{
				array_push($ToTerminalsDestination, $Destinations["DestinationID"]);
			}
		} 
		
		//dd($fromLoadingDestinationIDArray);
		foreach($decodeData["data"]["Destinations"] as $index => $Destinations)
		{
			//print_r($Terminals);
			if(in_array($Destinations["DestinationID"], $fromLoadingDestinationIDArray) && in_array($Destinations["DestinationID"], $ToTerminalsDestination))
			{
				//dd($Destinations);
				$ResponseArr[] = array('rownum' => $row,
					'id' => $Destinations["DestinationID"], 
					'name' => $Destinations["Destination"],  
					'short_name' => $Destinations["Destination"], 
				);
				$nOfRecords ++;
				$row++;
			}
 			
		}    
		$row = 1;
		$nOfRecords = 1;
		foreach($ResponseArr as $index => $Terminals)
		{
			if($row>$page && $nOfRecords<=$per_page)
			{
				$Response[] = array('rownum' => ($index+1),
					'id' => $Terminals["id"], 
					'name' => $Terminals["name"],  
					'short_name' => $Terminals["short_name"], 
				);
				$nOfRecords++;
			}
			$row++;
		}
		//dd($Response);
		if(sizeof($Response) == 0)
			return [];
		else
			return $this->convertToObject($Response);
	}
	
	public function GetSelectedToTerminalAPI($company_id, $city_id, $to_city_id, $from_terminal, $rownum)
	{
		//echo "<br />Call GetFromTerminalsAPI<br />";
		$per_page = 7;
		$url = env('API_URL')."AllRouteDetails/".$company_id;
		$options = array(
		  'http'=>array(

			'method'=>"GET",
			'header'=>"Authorization: Bearer ".env('API_TOKEN')
		  )
		);
		//dd($url);
		$context = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
		
		$decodeData = json_decode($data, true);
		
		$Response = array();
		$row = 1;
		$nOfRecords = 1;
		//echo "$row>=$page && $nOfRecords<=3<br />";
		
		$fromLoadingDestinationIDArray = array();
		foreach($decodeData["data"]["Routes"] as $index => $Routes)
		{
			if($Routes["LoadinID"] == $from_terminal)
			{
				array_push($fromLoadingDestinationIDArray, $Routes["DestinationID"]);
			}
		}
		
		//Get to city terminal
		$ToTerminalsDestination = array();
		foreach($decodeData["data"]["Destinations"] as $index => $Destinations)
		{
			if($to_city_id == $Destinations["StateID"])
			{
				array_push($ToTerminalsDestination, $Destinations["DestinationID"]);
			}
		} 
		
		foreach($decodeData["data"]["Destinations"] as $index => $Destinations)
		{
			//print_r($Terminals);
			if(in_array($Destinations["DestinationID"], $fromLoadingDestinationIDArray) && in_array($Destinations["DestinationID"], $ToTerminalsDestination))
			{
				//dd($Destinations);
				$ResponseArr[] = array('rownum' => $row,
					'id' => $Destinations["DestinationID"], 
					'name' => $Destinations["Destination"],  
					'short_name' => $Destinations["Destination"], 
				);
				$nOfRecords ++;
				$row++;
			}
 			
		}    
		$row = 1;
		$nOfRecords = 1;
		foreach($ResponseArr as $index => $Terminals)
		{
			if($row == $rownum)
			{
				$Response[] = array('rownum' => $index,
					'id' => $Terminals["id"], 
					'name' => $Terminals["name"],  
					'short_name' => $Terminals["short_name"], 
				);
			}
			$row++;
		}    
//		dd($ResponseArr);
		if(sizeof($Response) == 0)
			return [];
		else
			return $this->convertToObject($Response);
	}
	
	public function GetSelectedFromTerminalByIDAPI($company_id, $terminal_id)
	{
		//echo "<br />Call GetFromTerminalsAPI<br />";
		$per_page = 4;
		$url = env('API_URL')."AllRouteDetails/".$company_id;
		$options = array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>"Authorization: Bearer ".env('API_TOKEN')
		  )
		);
		//dd($url);
		$context = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
		
		$decodeData = json_decode($data, true);
		
		$Response = array();
		//echo "$row>=$page && $nOfRecords<=3<br />";
		foreach($decodeData["data"]["Terminals"] as $index => $Terminals)
		{
			//print_r($Terminals);
			//echo "<br />";
			if($terminal_id == $Terminals["LoadinID"])
			{
				$Response[] = array('rownum' => ($index+1),
					'id' => $Terminals["LoadinID"], 
					'name' => $Terminals["Loading_Office"],  
					'short_name' => $Terminals["Loading_Office"], 
				);
			}
		}    
		//dd($Response);
		if(sizeof($Response) == 0)
			return [];
		else
			return $this->convertToObject($Response);
	}
	
	public function GetSelectedToTerminalByIDAPI($company_id, $terminal_id)
	{
		//echo "<br />Call GetFromTerminalsAPI<br />";
		$per_page = 4;
		$url = env('API_URL')."AllRouteDetails/".$company_id;
		$options = array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>"Authorization: Bearer ".env('API_TOKEN')
		  )
		);
		//dd($url);
		$context = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
		
		$decodeData = json_decode($data, true);
		
		$Response = array();
		//echo "$row>=$page && $nOfRecords<=3<br />";
		foreach($decodeData["data"]["Terminals"] as $index => $Terminals)
		{
			//print_r($Terminals);
			if($terminal_id == $Terminals["LoadinID"])
			{
				$Response[] = array('rownum' => ($index+1),
					'id' => $Terminals["DestinationID"], 
					'name' => $Terminals["Loading_Office"],  
					'short_name' => $Terminals["Loading_Office"], 
				);
			}
		}    
		//dd($Response);
		if(sizeof($Response) == 0)
			return [];
		else
			return $this->convertToObject($Response);
	}
	
	public function GetFromTerminals($company_id, $city_id, $to_city_id, $page)
	{
		$sql = "select * from ( select id, name, short_name, @rownum := @rownum + 1 AS rownum from ( 
				select distinct t.id, t.name, t.short_name, t.sequence 
				FROM terminals t 
				join fares f ON (t.id = f.from_terminal_id)
				where t.status=1 and t.company_id='".$company_id."' and f.company_id='".$company_id."'  
				and
				f.from_terminal_id in (
					SELECT distinct terminals.id FROM 
					terminals 
					join lgas on (terminals.lga_id=lgas.id)
					join cities on (lgas.city_id=cities.id)
					where 
					terminals.status=1 and cities.id='".$city_id."'
				)
				and
				f.to_terminal_id in (
					SELECT distinct terminals.id FROM 
					terminals 
					join lgas on (terminals.lga_id=lgas.id)
					join cities on (lgas.city_id=cities.id)
					where 
					terminals.status=1 and cities.id='".$to_city_id."'
				)
			) data, (SELECT @rownum := 0) r order by sequence, name ) outerRow limit 4 offset ".$page;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	public function GetSelectedFromTerminal($company_id, $city_id, $to_city_id, $rownum)
	{		
		$sql = "select * from ( select id, name, short_name, @rownum := @rownum + 1 AS rownum from ( 
				select distinct t.id, t.name, t.short_name, t.sequence 
				FROM terminals t 
				join fares f ON (t.id = f.from_terminal_id)
				where t.status=1 and t.company_id='".$company_id."' and f.company_id='".$company_id."'  
				and
				f.from_terminal_id in (
					SELECT distinct terminals.id FROM 
					terminals 
					join lgas on (terminals.lga_id=lgas.id)
					join cities on (lgas.city_id=cities.id)
					where 
					terminals.status=1 and cities.id='".$city_id."'
				)
				and
				f.to_terminal_id in (
					SELECT distinct terminals.id FROM 
					terminals 
					join lgas on (terminals.lga_id=lgas.id)
					join cities on (lgas.city_id=cities.id)
					where 
					terminals.status=1 and cities.id='".$to_city_id."'
				)																						
			) data, (SELECT @rownum := 0) r order by sequence, name ) outerRow where rownum=".$rownum;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	
	public function GetToTerminals($company_id, $city_id, $to_city_id, $page)
	{
		$sql = "select * from ( select id, name, short_name, @rownum := @rownum + 1 AS rownum from ( 
				select distinct t.id, t.name, t.short_name, t.sequence 
				FROM terminals t 
				join fares f ON (t.id = f.to_terminal_id)
				where t.status=1 and t.company_id='".$company_id."' and f.company_id='".$company_id."'  
				and
				f.from_terminal_id in (
					SELECT distinct terminals.id FROM 
					terminals 
					join lgas on (terminals.lga_id=lgas.id)
					join cities on (lgas.city_id=cities.id)
					where 
					terminals.status=1 and cities.id='".$city_id."'
				)
				and
				f.to_terminal_id in (
					SELECT distinct terminals.id FROM 
					terminals 
					join lgas on (terminals.lga_id=lgas.id)
					join cities on (lgas.city_id=cities.id)
					where 
					terminals.status=1 and cities.id='".$to_city_id."'
				)
			) data, (SELECT @rownum := 0) r order by sequence, name ) outerRow limit 4 offset ".$page;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	public function GetSelectedToTerminal($company_id, $city_id, $to_city_id, $rownum)
	{		
		$sql = "select * from ( select id, name, short_name, @rownum := @rownum + 1 AS rownum from ( 
				select distinct t.id, t.name, t.short_name, t.sequence 
				FROM terminals t 
				join fares f ON (t.id = f.to_terminal_id)
				where t.status=1 and t.company_id='".$company_id."' and f.company_id='".$company_id."'  
				and
				f.from_terminal_id in (
					SELECT distinct terminals.id FROM 
					terminals 
					join lgas on (terminals.lga_id=lgas.id)
					join cities on (lgas.city_id=cities.id)
					where 
					terminals.status=1 and cities.id='".$city_id."'
				)
				and
				f.to_terminal_id in (
					SELECT distinct terminals.id FROM 
					terminals 
					join lgas on (terminals.lga_id=lgas.id)
					join cities on (lgas.city_id=cities.id)
					where 
					terminals.status=1 and cities.id='".$to_city_id."'
				)																						
			) data, (SELECT @rownum := 0) r order by sequence, name ) outerRow where rownum=".$rownum;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	
	public function GetToCities($city_id,$page)
	{	
		$sql = "select * from (SELECT c.id, c.name, @rownum := @rownum + 1 AS rownum FROM cities c, (SELECT @rownum := 0) r where id<>".$city_id." order by sequence, name) data limit 4 offset ".$page;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	public function GetSelectedToCity($city_id,$rownum)
	{
		$sql = "select * from (SELECT c.id, c.name, c.short_name, @rownum := @rownum + 1 AS rownum FROM cities c, (SELECT @rownum := 0) r where id<>".$city_id."  order by sequence, name) data where rownum=".$rownum;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	
	
	public function GetScheduleSearch($msisdn,$page)
	{	
		$sql = "select * from (SELECT c.from_city_id, c.from_city_name, c.from_terminal_id, c.from_terminal_name, c.to_terminal_id, c.to_terminal_name, c.to_city_id, c.to_city_name, @rownum := @rownum + 1 AS rownum FROM ussd_menu_schedule_search c, (SELECT @rownum := 0) r where msisdn='".$msisdn."' and schedule_date>DATE(NOW()) order by record_on desc) data limit 4 offset ".$page;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	public function GetSelectedScheduleSearch($msisdn,$rownum)
	{
		$sql = "select * from (SELECT c.*, @rownum := @rownum + 1 AS rownum FROM ussd_menu_schedule_search c, (SELECT @rownum := 0) r where msisdn='".$msisdn."' and schedule_date>DATE(NOW()) order by record_on desc) data where rownum=".$rownum;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	
	public function GetLastSelectedScheduleSearch($msisdn,$rownum)
	{
		$sql = "SELECT c.*, @rownum := @rownum + 1 AS rownum FROM ussd_menu_schedule_search c, (SELECT @rownum := 0) r where msisdn='".$msisdn."' and schedule_date>DATE(NOW()) order by record_on desc limit 1";
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	
	public function GetCompanyCitiesAPI($company_id,$page)
	{
		//echo "<br />Call GetCompanyCitiesAPI<br />";
		$per_page = 7;
		$url = env('API_URL')."TerminalStates/".$company_id;
		$options = array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>"Authorization: Bearer ".env('API_TOKEN')
		  )
		);
		//dd($url);
		$context = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
		
		$decodeData = json_decode($data, true);
		
		//echo "<pre>";
		//print_r($decodeData);
		//echo "</pre>";
		//print($decodeData["data"][9]["StateID"])."<br>";
		$Response = array();
		$row = 1;
		$nOfRecords = 1;
		//echo "$row>=$page && $nOfRecords<=3<br />";
		foreach($decodeData["data"] as $index => $Cities)
		{
			if($row>$page && $nOfRecords<=$per_page)
			{
				//echo ($index+1).". ".$Cities["StateName"]."<br>";
				$Response[] = array('rownum' => ($index+1),
					'id' => $Cities["StateID"], 
					'name' => $Cities["StateName"],  
					'short_name' => $Cities["StateName"], 
				);
				$nOfRecords ++;
			}
			$row++;
		}    
		//dd($Response);
		if(sizeof($Response) == 0)
			return [];
		else
			return $this->convertToObject($Response);
	}
	public function GetSelectedCompanyCityAPI($company_id,$rownum)
	{
		//echo "<br />Call GetSelectedCompanyCityAPI<br />";
		$url = env('API_URL')."TerminalStates/".$company_id;
		$options = array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>"Authorization: Bearer ".env('API_TOKEN')
		  )
		);
		
		$context = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
		
		$decodeData = json_decode($data, true);
		
		//echo "<pre>";
		//print_r($decodeData);
		//echo "</pre>";
		//print($decodeData["data"][9]["StateID"])."<br>";
		$Response = array();
		$Response[] = array('rownum' => (1),
		'id' => $decodeData["data"][($rownum-1)]["StateID"], 
		'name' => $decodeData["data"][($rownum-1)]["StateName"],  
		'short_name' => $decodeData["data"][($rownum-1)]["StateName"], 
		);
		
		if(sizeof($Response) == 0)
			return [];

		else
			return $this->convertToObject($Response);
	}
	
	public function GetCompanyCitiesAllRouteDetailsAPI($company_id,$page)
	{
		//echo "<br />Call GetCompanyCitiesAPI<br />";
		$per_page = 7;
		$url = env('API_URL')."AllRouteDetails/".$company_id;
		$options = array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>"Authorization: Bearer ".env('API_TOKEN')
		  )
		);
		//dd($url);
		$context = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
		
		$decodeData = json_decode($data, true);
		
		//echo "<pre>";
		//print_r($decodeData);
		//echo "</pre>";
		//print($decodeData["data"][9]["StateID"])."<br>";
		$Response = array();
		$row = 1;
		$nOfRecords = 1;
		//echo "$row>=$page && $nOfRecords<=3<br />";
		foreach($decodeData["data"]["States"] as $index => $Cities)
		{
			if($row>$page && $nOfRecords<=$per_page)
			{
				//echo ($index+1).". ".$Cities["StateName"]."<br>";
				$Response[] = array('rownum' => ($index+1),
					'id' => $Cities["StateID"], 
					'name' => $Cities["StateName"],  
					'short_name' => $Cities["StateName"], 
				);
				$nOfRecords ++;
			}
			$row++;
		}    
		//dd($Response);
		if(sizeof($Response) == 0)
			return [];
		else
			return $this->convertToObject($Response);
	}
	
	public function GetCompanyCityByIDAllRouteDetailsAPI($company_id, $city_id)
	{
		//echo "<br />Call GetSelectedCompanyCityAPI<br />";
		$url = env('API_URL')."AllRouteDetails/".$company_id;
		$options = array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>"Authorization: Bearer ".env('API_TOKEN')
		  )
		);
		
		$context = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
		
		$decodeData = json_decode($data, true);
		
		//echo "<pre>";
		//print_r($decodeData);
		//echo "</pre>";
		//print($decodeData["data"][9]["StateID"])."<br>";
		$Response = array();
		foreach($decodeData["data"]["States"] as $index => $Cities)
		{
			if($Cities["StateID"] == $city_id)
			{
				//echo ($index+1).". ".$Cities["StateName"]."<br>";
				$Response[] = array('rownum' => ($index+1),
					'id' => $Cities["StateID"], 
					'name' => $Cities["StateName"],  
					'short_name' => $Cities["StateName"], 
				);
			}
		}   
		
		if(sizeof($Response) == 0)
			return [];
		else
			return $this->convertToObject($Response);
	}
	
	public function GetSelectedCompanyCityAllRouteDetailsAPI($company_id,$rownum)
	{
		//echo "<br />Call GetSelectedCompanyCityAPI<br />";
		$url = env('API_URL')."AllRouteDetails/".$company_id;
		$options = array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>"Authorization: Bearer ".env('API_TOKEN')
		  )
		);
		
		$context = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
		
		$decodeData = json_decode($data, true);
		
		//echo "<pre>";
		//print_r($decodeData);
		//echo "</pre>";
		//print($decodeData["data"][9]["StateID"])."<br>";
		$Response = array();
		$Response[] = array('rownum' => (1),
		'id' => $decodeData["data"]["States"][($rownum-1)]["StateID"], 
		'name' => $decodeData["data"]["States"][($rownum-1)]["StateName"],  
		'short_name' => $decodeData["data"]["States"][($rownum-1)]["StateName"], 
		);
		
		if(sizeof($Response) == 0)
			return [];
		else
			return $this->convertToObject($Response);
	}
	
	public function GetCompanyCities($company_id,$page)
	{	
		//$sql = "select * from ( select id, name, short_name, @rownum := @rownum + 1 AS rownum from ( SELECT c.id, c.name, c.short_name, c.sequence FROM cities c where c.id in (SELECT distinct city_id FROM lgas JOIN terminals on (lgas.id=terminals.lga_id) where terminals.status=1 and terminals.company_id=".$company_id.")) data, (SELECT @rownum := 0) r order by sequence, name ) outerRow limit 4 offset ".$page;
		$sql = "select * from ( select id, name, short_name, @rownum := @rownum + 1 AS rownum from ( SELECT c.id, c.name, c.short_name, c.sequence FROM cities c where c.id in (select distinct city_id from lgas where id in
(
	select lga_id from terminals where id in
	(
		select route_details.terminal_id from
		routes join
		(select route_id, terminal_id from route_details where sequence = 1) route_details
		on route_details.route_id = routes.id
		where routes.company_id = ".$company_id."
	)
	and company_id=".$company_id." and status = 1
))) data, (SELECT @rownum := 0) r order by sequence, name ) outerRow limit 4 offset ".$page;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	public function GetSelectedCompanyCity($company_id,$rownum)
	{
		//$sql = "select * from ( select id, name, short_name, @rownum := @rownum + 1 AS rownum from ( SELECT c.id, c.name, c.short_name, c.sequence FROM cities c where c.id in (SELECT distinct city_id FROM lgas JOIN terminals on (lgas.id=terminals.lga_id) where terminals.status=1 and terminals.company_id=".$company_id.")) data, (SELECT @rownum := 0) r order by sequence, name ) outerRow where rownum=".$rownum;
		$sql = "select * from ( select id, name, short_name, @rownum := @rownum + 1 AS rownum from ( SELECT c.id, c.name, c.short_name, c.sequence FROM cities c where c.id in (select distinct city_id from lgas where id in
(
	select lga_id from terminals where id in
	(
		select route_details.terminal_id from
		routes join
		(select route_id, terminal_id from route_details where sequence = 1) route_details
		on route_details.route_id = routes.id
		where routes.company_id = ".$company_id."
	)
	and company_id=".$company_id." and status = 1
))) data, (SELECT @rownum := 0) r order by sequence, name ) outerRow where rownum=".$rownum;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	
	public function GetCompanyToCitiesAPI($city_id,$company_id,$page)
	{
		//echo "<br />Call GetCompanyToCitiesAPI<br />";
		$per_page = 4;
		$url = env('API_URL')."TerminalStates/".$company_id;
		$options = array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>"Authorization: Bearer ".env('API_TOKEN')
		  )
		);
		//dd($url);
		$context = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
		
		$decodeData = json_decode($data, true);
		
		//echo "<pre>";
		//print_r($decodeData);
		//echo "</pre>";
		//print($decodeData["data"][9]["StateID"])."<br>";
		$Response = array();
		$row = 1;
		$nOfRecords = 1;
		//echo "$row>=$page && $nOfRecords<=3<br />";
		foreach($decodeData["data"] as $index => $Cities)
		{
			if($city_id != $Cities["StateID"])//Exclude selected from city
			{
				if($row>$page && $nOfRecords<=$per_page)
				{
					//echo ($index+1).". ".$Cities["StateName"]."<br>";
					$Response[] = array('rownum' => ($row),
						'id' => $Cities["StateID"], 
						'name' => $Cities["StateName"],  
						'short_name' => $Cities["StateName"], 
					);
					$nOfRecords ++;
				}
				$row++;
			}
		}    
		//dd($Response);
		if(sizeof($Response) == 0)
			return [];
		else
			return $this->convertToObject($Response);
	}
	
	public function GetSelectedCompanyToCityAPI($city_id,$company_id,$rownum)
	{
		//echo "<br />Call GetSelectedCompanyToCityAPI<br />";
		$url = env('API_URL')."TerminalStates/".$company_id;
		$options = array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>"Authorization: Bearer ".env('API_TOKEN')
		  )
		);
		
		$context = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
		
		$decodeData = json_decode($data, true);
		
		$row = 1;
		$nOfRecords = 1;
		foreach($decodeData["data"] as $index => $Cities)
		{
			if($city_id != $Cities["StateID"])//Exclude selected from city
			{
				if($rownum == $row)
				{
					//echo ($index+1).". ".$Cities["StateName"]."<br>";
					$Response[] = array('rownum' => ($row),
						'id' => $Cities["StateID"], 
						'name' => $Cities["StateName"],  
						'short_name' => $Cities["StateName"], 
					);
					$nOfRecords ++;
				}
				$row++;
			}
		}

		if(sizeof($Response) == 0)
			return [];
		else
			return $this->convertToObject($Response);
	}
	
	public function GetCompanyToCitiesAllRouteDetailsAPI($city_id,$from_terminal,$company_id,$page)
	{
		//echo "<br />Call GetCompanyToCitiesAPI<br />";
		$per_page = 7;
		$url = env('API_URL')."AllRouteDetails/".$company_id;
		$options = array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>"Authorization: Bearer ".env('API_TOKEN')
		  )
		);
		//dd($url);
		$context = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
		
		$decodeData = json_decode($data, true);
		
		$urlTerminalStates = env('API_URL')."TerminalStates/".$company_id;
		$optionsTerminalStates = array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>"Authorization: Bearer ".env('API_TOKEN')
		  )
		);
		//dd($url);
		$contextTerminalStates = stream_context_create($options);
		$dataTerminalStates = file_get_contents($urlTerminalStates, false, $contextTerminalStates);
		
		$decodeDataTerminalStates = json_decode($dataTerminalStates, true);
		
		//echo "<pre>";
		//print_r($decodeData);
		//echo "</pre>";
		//print($decodeData["data"][9]["StateID"])."<br>";
		$Response = array();
		$row = 1;
		$nOfRecords = 1;
		//echo "$row>=$page && $nOfRecords<=3<br />";
		
		//Get from city terminal
		$FromTerminals = array();
		foreach($decodeData["data"]["Terminals"] as $index => $Terminals)
		{
			//print_r($Terminals);
			if($city_id == $Terminals["StateID"])
			{
				array_push($FromTerminals, $Terminals["LoadinID"]);
			}
		} 
		//dd($FromTerminals);
		//Get from city terminal destinations
		$FromDestinations = array();
		foreach($FromTerminals as $index => $FromTerminal)
		{			
			foreach($decodeData["data"]["Routes"] as $index => $Routes)
			{
				if($FromTerminal == $Routes["LoadinID"])
				{
					array_push($FromDestinations, $Routes["DestinationID"]);
				}
			} 
		}
		
		$FromDestinations = array();
		foreach($decodeData["data"]["Routes"] as $index => $Routes)
		{
			if($from_terminal == $Routes["LoadinID"])
			{
				array_push($FromDestinations, $Routes["DestinationID"]);
			}
		}
		
		//dd($FromDestinations);
		//Get destination terminal
		$ToTerminals = array();
		$ToTerminalsDestinationName = array();
		foreach($FromDestinations as $index => $FromDestination)
		{
			foreach($decodeData["data"]["Destinations"] as $index => $Destinations)
			{
				if($FromDestination == $Destinations["DestinationID"])
				{
					array_push($ToTerminals, $Destinations["MainTerminalID"]);
					array_push($ToTerminalsDestinationName, $Destinations["Destination"]);
				}
			} 
		}
		//dd($ToTerminalsDestinationName);
		//Get terminal cities
		$ToCities = array();
		foreach($ToTerminals as $index => $ToTerminal)
		{
			foreach($decodeData["data"]["Terminals"] as $index => $Terminals)
			{
				if($ToTerminal == $Terminals["LoadinID"] && !in_array($Terminals["StateID"], $ToCities))
				{
					array_push($ToCities, $Terminals["StateID"]);
				}
			} 
		}
		
		//dd($ToCities);
		foreach($decodeDataTerminalStates["data"] as $index => $Cities)
		{
			if($city_id != $Cities["StateID"] && ( in_array($Cities["StateID"], $ToCities) || in_array($Cities["StateName"], $ToTerminalsDestinationName) ))//Exclude selected from city & include only route to city
			{
				if($row>$page && $nOfRecords<=$per_page)
				{
					//echo ($index+1).". ".$Cities["StateName"]."<br>";
					$Response[] = array('rownum' => ($row),
						'id' => $Cities["StateID"], 
						'name' => $Cities["StateName"],  
						'short_name' => $Cities["StateName"], 
					);
					$nOfRecords ++;
				}
				$row++;
			}
		}
		//if($page>0)
		//dd($Response);
		if(sizeof($Response) == 0)
			return [];
		else
			return $this->convertToObject($Response);
	}
	
	public function GetSelectedCompanyToCityAllRouteDetailsAPI($city_id,$from_terminal,$company_id,$rownum)
	{
		//echo "<br />Call GetSelectedCompanyToCityAPI<br />";
		$url = env('API_URL')."AllRouteDetails/".$company_id;
		$options = array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>"Authorization: Bearer ".env('API_TOKEN')
		  )
		);
		
		$context = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
		
		$decodeData = json_decode($data, true);
		
		$urlTerminalStates = env('API_URL')."TerminalStates/".$company_id;
		$optionsTerminalStates = array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>"Authorization: Bearer ".env('API_TOKEN')
		  )
		);
		//dd($url);
		$contextTerminalStates = stream_context_create($options);
		$dataTerminalStates = file_get_contents($urlTerminalStates, false, $contextTerminalStates);
		
		$decodeDataTerminalStates = json_decode($dataTerminalStates, true);
		
		//Get from city terminal
		$FromTerminals = array();
		foreach($decodeData["data"]["Terminals"] as $index => $Terminals)
		{
			//print_r($Terminals);
			if($city_id == $Terminals["StateID"])
			{
				array_push($FromTerminals, $Terminals["LoadinID"]);
			}
		}     
		//dd($FromTerminals);
		//Get from city terminal destinations
		$FromDestinations = array();
		foreach($FromTerminals as $index => $FromTerminal)
		{			
			foreach($decodeData["data"]["Routes"] as $index => $Routes)
			{
				if($FromTerminal == $Routes["LoadinID"])
				{
					array_push($FromDestinations, $Routes["DestinationID"]);
				}
			} 
		}
		
		$FromDestinations = array();
		foreach($decodeData["data"]["Routes"] as $index => $Routes)
		{
			if($from_terminal == $Routes["LoadinID"])
			{
				array_push($FromDestinations, $Routes["DestinationID"]);
			}
		}
		//dd($FromDestinations);
		//Get destination terminal
		$ToTerminals = array();
		$ToTerminalsDestinationName = array();
		foreach($FromDestinations as $index => $FromDestination)
		{
			foreach($decodeData["data"]["Destinations"] as $index => $Destinations)
			{
				if($FromDestination == $Destinations["DestinationID"])
				{
					array_push($ToTerminals, $Destinations["MainTerminalID"]);
					array_push($ToTerminalsDestinationName, $Destinations["Destination"]);
				}
			} 
		}
		//dd($ToTerminalsDestinationName);
		//Get terminal cities
		$ToCities = array();
		foreach($ToTerminals as $index => $ToTerminal)
		{
			foreach($decodeData["data"]["Terminals"] as $index => $Terminals)
			{
				if($ToTerminal == $Terminals["LoadinID"] && !in_array($Terminals["StateID"], $ToCities))
				{
					array_push($ToCities, $Terminals["StateID"]);
				}
			} 
		}
		//dd($ToCities);
		$Response = array();
		$row = 1;
		$nOfRecords = 1;
		foreach($decodeDataTerminalStates["data"] as $index => $Cities)
		{
			if($city_id != $Cities["StateID"] && ( in_array($Cities["StateID"], $ToCities) || in_array($Cities["StateName"], $ToTerminalsDestinationName) ))//Exclude selected from city & include only route to city
			{
				if($rownum == $row)
				{
					//echo ($index+1).". ".$Cities["StateName"]."<br>";
					$Response[] = array('rownum' => ($row),
						'id' => $Cities["StateID"], 
						'name' => $Cities["StateName"],  
						'short_name' => $Cities["StateName"], 
					);
					$nOfRecords ++;
				}
				$row++;
			}
		}
		//dd($Response);
		if(sizeof($Response) == 0)
			return [];
		else
			return $this->convertToObject($Response);
	}
	
	public function GetCompanyToCities($city_id,$company_id,$page)
	{
		//$sql = "select * from ( select id, name, short_name, @rownum := @rownum + 1 AS rownum from ( SELECT c.id, c.name, c.short_name, c.sequence FROM cities c where id<>".$city_id." and c.id in (SELECT distinct city_id FROM lgas JOIN terminals on (lgas.id=terminals.lga_id) where terminals.company_id=".$company_id.")) data, (SELECT @rownum := 0) r order by sequence, name ) outerRow limit 4 offset ".$page;
		
		$sql = "select * from ( select id, name, short_name, @rownum := @rownum + 1 AS rownum from ( SELECT c.id, c.name, c.short_name, c.sequence FROM cities c where id<>".$city_id." and c.id in (SELECT distinct city_id FROM lgas JOIN terminals on (lgas.id=terminals.lga_id) where terminals.status=1 and terminals.company_id=".$company_id." and terminals.id in (SELECT to_terminal_id FROM fares where from_terminal_id in (SELECT DISTINCT terminals.id FROM lgas JOIN terminals ON (lgas.id=terminals.lga_id) WHERE terminals.status=1 and terminals.company_id=".$company_id." AND lgas.city_id=".$city_id.")))) data, (SELECT @rownum := 0) r order by sequence, name ) outerRow limit 4 offset ".$page;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	public function GetSelectedCompanyToCity($city_id,$company_id,$rownum)
	{
		//$sql = "select * from ( select id, name, short_name, @rownum := @rownum + 1 AS rownum from ( SELECT c.id, c.name, c.short_name, c.sequence FROM cities c where id<>".$city_id." and c.id in (SELECT distinct city_id FROM lgas JOIN terminals on (lgas.id=terminals.lga_id) where terminals.company_id=".$company_id.")) data, (SELECT @rownum := 0) r order by sequence, name ) outerRow where rownum=".$rownum;
		
		$sql = "select * from ( select id, name, short_name, @rownum := @rownum + 1 AS rownum from ( SELECT c.id, c.name, c.short_name, c.sequence FROM cities c where id<>".$city_id." and c.id in (SELECT distinct city_id FROM lgas JOIN terminals on (lgas.id=terminals.lga_id) where terminals.status=1 and terminals.company_id=".$company_id." and terminals.id in (SELECT to_terminal_id FROM fares where from_terminal_id in (SELECT DISTINCT terminals.id FROM lgas JOIN terminals ON (lgas.id=terminals.lga_id) WHERE terminals.status=1 and terminals.company_id=".$company_id." AND lgas.city_id=".$city_id.")))) data, (SELECT @rownum := 0) r order by sequence, name ) outerRow where rownum=".$rownum;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	
	public function GetCompanies($page)
	{
		$sql = "select * from (SELECT c.id, c.name, @rownum := @rownum + 1 AS rownum FROM companies c, (SELECT @rownum := 0) r order by sequence, name) data limit 4 offset ".$page;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	public function GetSelectedCompany($rownum)
	{
		$sql = "select * from (SELECT c.*, @rownum := @rownum + 1 AS rownum FROM companies c, (SELECT @rownum := 0) r order by sequence, name) data where rownum=".$rownum;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	
	public function GetCompanyPageNo($company_id)
	{
		$sql = "select * from (SELECT c.*, @rownum := @rownum + 1 AS rownum FROM companies c, (SELECT @rownum := 0) r order by sequence, name) data where id=".$company_id;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	
	public function GetDeals($company_id,$page)
	{
		$sql = "select * from (SELECT d.id, d.name, @rownum := @rownum + 1 AS rownum FROM deals d, (SELECT @rownum := 0) r where company_id=".$company_id." order by name) data limit 4 offset ".$page;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	public function GetSelectedDeal($company_id,$rownum)
	{
		$sql = "select * from (SELECT d.id, d.name, d.detail, @rownum := @rownum + 1 AS rownum FROM deals d, (SELECT @rownum := 0) r where company_id=".$company_id." order by name) data where rownum=".$rownum;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	
	public function GetBanks($page)
	{
		$sql = "select * from (SELECT d.id, d.name, @rownum := @rownum + 1 AS rownum FROM banks d, (SELECT @rownum := 0) r order by id) data limit 6 offset ".$page;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	public function GetSelectedBank($rownum)
	{
		$sql = "select * from (SELECT d.id, d.name, d.ussd_code, @rownum := @rownum + 1 AS rownum FROM banks d, (SELECT @rownum := 0) r order by id) data where rownum=".$rownum;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	
	public function GetPolicies($company_id,$page)
	{
		$sql = "select * from (SELECT p.id, p.name, @rownum := @rownum + 1 AS rownum FROM policies p, (SELECT @rownum := 0) r where company_id=".$company_id." order by name) data limit 4 offset ".$page;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	public function GetSelectedPolicy($company_id,$rownum)
	{
		$sql = "select * from (SELECT p.id, p.name, p.detail, @rownum := @rownum + 1 AS rownum FROM policies p, (SELECT @rownum := 0) r where company_id=".$company_id." order by name) data where rownum=".$rownum;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	
	public function GetAbouts($company_id,$page)
	{
		$sql = "select * from (SELECT a.id, a.name, @rownum := @rownum + 1 AS rownum FROM abouts a, (SELECT @rownum := 0) r where company_id=".$company_id." order by name) data limit 4 offset ".$page;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	public function GetSelectedAbout($company_id,$rownum)
	{
		$sql = "select * from (SELECT a.id, a.name, a.detail, @rownum := @rownum + 1 AS rownum FROM abouts a, (SELECT @rownum := 0) r where company_id=".$company_id." order by name) data where rownum=".$rownum;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	
	public function GetCservices($company_id,$page)
	{
		$sql = "select * from (SELECT c.id, c.name, @rownum := @rownum + 1 AS rownum FROM cservices c, (SELECT @rownum := 0) r where company_id=".$company_id." order by name) data limit 4 offset ".$page;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	public function GetSelectedCservice($company_id,$rownum)
	{
		$sql = "select * from (SELECT c.id, c.name, c.detail, @rownum := @rownum + 1 AS rownum FROM cservices c, (SELECT @rownum := 0) r where company_id=".$company_id." order by name) data where rownum=".$rownum;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	
	public function GetCompanyCityTerminalsAPI($company_id,$city_id,$api_url,$api_token,$page, $per_page)
	{
		//echo "<br />Call GetCompanyCityTerminalsAPI<br />";
		$url = env('API_URL')."TerminalsWithState/".$company_id;
		$options = array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>"Authorization: Bearer ".env('API_TOKEN')
		  )
		);
		
		$context = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
		
		$decodeData = json_decode($data, true);
		
		//echo "<pre>";
		//print_r($decodeData);
		//echo "</pre>";
		//print($decodeData["data"][9]["StateID"])."<br>";
		$Response = array();
		$row = 1;
		$nOfRecords = 1;
		//echo "$row>=$page && $nOfRecords<=3<br />";
		foreach($decodeData["data"] as $index => $CitiesTerminal)
		{
			if($CitiesTerminal["StateID"] == $city_id)
			{
				if($row>$page && $nOfRecords<=$per_page)
				{
					//echo ($index+1).". ".$Cities["StateName"]."<br>";
					$Response[] = array('rownum' => $row,
						'id' => $CitiesTerminal["TerminalID"], 
						'name' => $CitiesTerminal["Terminal"],  
						'address' => $CitiesTerminal["TerminalAddress"], 
						'email' => $CitiesTerminal["Terminal"], 
						'phone_number' => $CitiesTerminal["TerminalPhone"], 
					);
					$nOfRecords ++;
				}
				$row++;
			}
		}    
		//dd($Response);
		if(sizeof($Response) == 0)
			return [];
		else
			return $this->convertToObject($Response);
	}
	public function GetSelectedCompanyCityTerminalAPI($company_id,$city_id,$api_url,$api_token,$rownum)
	{
		//echo "<br />Call GetSelectedCompanyCityTerminalAPI<br />";
		$url = env('API_URL')."TerminalsWithState/".$company_id;
		$options = array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>"Authorization: Bearer ".env('API_TOKEN')
		  )
		);
		
		$context = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
		
		$decodeData = json_decode($data, true);
		
		//echo "<pre>";
		//print_r($decodeData);
		//echo "</pre>";
		//print($decodeData["data"][9]["StateID"])."<br>";
		$Response = array();
		$row = 1;
		$nOfRecords = 1;
		//echo "$row>=$page && $nOfRecords<=3<br />";
		foreach($decodeData["data"] as $index => $CitiesTerminal)
		{
			if($CitiesTerminal["StateID"] == $city_id)
			{
				if($row == $rownum)
				{
					//echo ($index+1).". ".$Cities["StateName"]."<br>";
					$Response[] = array('rownum' => $row,
						'id' => $CitiesTerminal["TerminalID"], 
						'name' => $CitiesTerminal["Terminal"],  
						'address' => $CitiesTerminal["TerminalAddress"], 
						'email' => $CitiesTerminal["Terminal"], 
						'phone_number' => $CitiesTerminal["TerminalPhone"], 
					);
					$nOfRecords ++;
				}
				$row++;
			}
		}    
		//dd($Response);
		if(sizeof($Response) == 0)
			return [];
		else
			return $this->convertToObject($Response);
	}
	public function GetCompanyCityTerminals($company_id,$city_id,$page)
	{
		$sql = "select * from (SELECT terminals.id, terminals.name, terminals.address, terminals.email, terminals.phone_number, @rownum := @rownum + 1 AS rownum FROM terminals join lgas on (lgas.id=terminals.lga_id), (SELECT @rownum := 0) r WHERE lgas.city_id=".$city_id." AND terminals.status=1 and terminals.company_id=".$company_id." ORDER by rownum, terminals.sequence, terminals.name) data limit 4 offset ".$page;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	public function GetSelectedCompanyCityTerminal($company_id,$city_id,$rownum)
	{
		$sql = "select * from (SELECT terminals.id, terminals.name, terminals.address, terminals.email, terminals.phone_number, @rownum := @rownum + 1 AS rownum FROM terminals join lgas on (lgas.id=terminals.lga_id), (SELECT @rownum := 0) r WHERE lgas.city_id=".$city_id." AND terminals.status=1 and terminals.company_id=".$company_id." ORDER by rownum, terminals.sequence, terminals.name) data where rownum=".$rownum;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	
	public function GetScheduleCountByCities($from_city_id,$to_city_id)
	{
		$sql = "SELECT 
					schedules.id as sch_id, fares.id as fid, 
					schedule_times.departure_time as dept_time, 
					services.short_name as service_name, 
					fares.fare as fare,
					terminals.name as terminal_name
					FROM 
					schedules 
					JOIN schedule_times ON (schedules.id=schedule_times.schedule_id)
					join fare_types on (schedules.id=fare_types.schedule_id) 
					join services on (fare_types.service_id=services.id) 
					join fares on (fare_types.id=fares.fare_type_id) 
					join terminals on (fares.from_terminal_id=terminals.id) 
					where 
					fares.from_terminal_id in (
						SELECT distinct terminals.id FROM 
						terminals 
						join lgas on (terminals.lga_id=lgas.id)
						join cities on (lgas.city_id=cities.id)
						where 
						terminals.status=1 and cities.id=".$from_city_id."
					)
					and
					fares.to_terminal_id in (
						SELECT distinct terminals.id FROM 
						terminals 
						join lgas on (terminals.lga_id=lgas.id)
						join cities on (lgas.city_id=cities.id)
						where 
						terminals.status=1 and cities.id=".$to_city_id."
					)
					and fares.fare <> 0
					order by schedule_times.departure_time";
		//echo "<br/>".$sql."<br />";
		$Results = DB::Select($sql);
		//print_r($Response);
		return $Results;
	}
	
	public function GetScheduleCountByCitiesCompany($company_id,$from_city_id,$to_city_id)
	{
		$sql = "SELECT 
					schedules.id as sch_id, fares.id as fid, 
					schedule_times.departure_time as dept_time, 
					services.short_name as service_name, 
					fares.fare as fare,
					terminals.name as terminal_name
					FROM 
					schedules 
					JOIN schedule_times ON (schedules.id=schedule_times.schedule_id)
					join fare_types on (schedules.id=fare_types.schedule_id) 
					join services on (fare_types.service_id=services.id) 
					join fares on (fare_types.id=fares.fare_type_id) 
					join terminals on (fares.from_terminal_id=terminals.id) 
					where 
					fare_types.company_id=".$company_id." 
					and
					fares.from_terminal_id in (
						SELECT distinct terminals.id FROM 
						terminals 
						join lgas on (terminals.lga_id=lgas.id)
						join cities on (lgas.city_id=cities.id)
						where 
						terminals.status=1 and terminals.company_id=".$company_id."
						and cities.id=".$from_city_id."
					)
					and
					fares.to_terminal_id in (
						SELECT distinct terminals.id FROM 
						terminals 
						join lgas on (terminals.lga_id=lgas.id)
						join cities on (lgas.city_id=cities.id)
						where 
						terminals.status=1 and terminals.company_id=".$company_id."
						and cities.id=".$to_city_id."
					)
					and fares.fare <> 0
					order by schedule_times.departure_time";
		//echo "<br/>".$sql."<br />";
		$Results = DB::Select($sql);
		//print_r($Response);
		return $Results;
	}
	
	public function GetCompanyScheduleAPI($company_id,$schedule_date,$from_terminal_id,$to_terminal_id,$to_city_id,$page,$per_page)
	{
		//echo "<br />Call GetCompanyCityTerminalsAPI<br />";
		$url = env('API_URL')."AvaiableBusWithSeatPrice/".$company_id."?LoadinID=$from_terminal_id&DestinationID=$to_terminal_id&TripDate=$schedule_date";
		\Log::info("GetCompanyScheduleAPI URL: ".$url);
		$options = array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>"Authorization: Bearer ".env('API_TOKEN')
		  )
		);
		
		$context = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
		
		$decodeData = json_decode($data, true);
		//dd($decodeData);
		$Response = array();
		$row = 1;
		$nOfRecords = 1;
		//echo "$row>=$page && $nOfRecords<=3<br />";
		if(isset($decodeData["data"]["BusList"]))
		foreach($decodeData["data"]["BusList"] as $index => $BusList)
		{
			//echo "$row>$page && $nOfRecords<=$per_page";
			if($row>$page && $nOfRecords<=$per_page)
			{
				//echo ($index+1).". ".$Cities["StateName"]."<br>";
				//$menuSchedule .= $info_Schedule['rownum'].", ".$info_Schedule['dept_time'].", ".$info_Schedule['dept_terminal'].", ".$info_Schedule['bus'].", N".$info_Schedule['fare']."\n";
				//dd($BusList);
				if((sizeof($BusList["AvailableSeat"])-sizeof($BusList["BlockedSeat"]))>0)
				{
					$Response[] = array('rownum' => $row,
						'sch_id' => $row, 
						'fid' => $BusList["ModelID"], 
						'dept_time' => date('h:iA',strtotime($BusList["Departure_Time"])), 
						'service_name' => $BusList["VIN"],  
						'fare' => $BusList["FareAmount"], 
						'terminal_name' => ucfirst(strtolower($BusList["Loading_Office"])),
						'dept_terminal' => ucfirst(strtolower($BusList["Loading_Office"])),
						'bus' => $BusList["VehicleGroupTag_Name"],
						'AvailableSeatCounts' => sizeof($BusList["AvailableSeat"])-sizeof($BusList["BlockedSeat"]),
						'TotalSeatCounts' => sizeof($BusList["AvailableSeat"]),
					);
					$nOfRecords ++;
				}
			}
			$row++;
		}    
		//dd($Response);
		if(sizeof($Response) == 0)
			return [];
		else
			return $Response;
	}
	public function GetSelectedCompanyScheduleAPI($company_id,$schedule_date,$from_terminal_id,$to_terminal_id,$to_city_id,$rownum)
	{
		//echo "<br />Call GetSelectedCompanyCityTerminalAPI<br />";
		$url = env('API_URL')."AvaiableBusWithSeatPrice/".$company_id."?LoadinID=$from_terminal_id&DestinationID=$to_terminal_id&TripDate=$schedule_date";
		
		$options = array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>"Authorization: Bearer ".env('API_TOKEN')
		  )
		);
		
		$context = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
		
		$decodeData = json_decode($data, true);
		//dd($decodeData);
		$Response = array();
		$row = 1;
		$nOfRecords = 1;
		//echo "$row>=$page && $nOfRecords<=3<br />";
		if(isset($decodeData["data"]["BusList"]))
		foreach($decodeData["data"]["BusList"] as $index => $BusList)
		{
			//dd( $rownum);
			//echo "$row>$page && $nOfRecords<=$per_page";
			if($row == $rownum)
			{
				$Response[] = array('rownum' => $row,
					'sch_id' => $row, 
					'fid' => $BusList["ModelID"], 
					'dept_time' => date('h:iA',strtotime($BusList["Departure_Time"])), 
					'service_name' => $BusList["VIN"],  
					'fare' => $BusList["FareAmount"], 
					'terminal_name' => ucfirst(strtolower($BusList["Loading_Office"])),
					'dept_terminal' => ucfirst(strtolower($BusList["Loading_Office"])),
					'bus' => $BusList["VehicleGroupTag_Name"],
					'AvailableSeat' => $BusList["AvailableSeat"],
					'BlockedSeat' => $BusList["BlockedSeat"],
					'TripID' => $BusList["TripID"],
					'OrderID' => $decodeData["data"]["RefCode"],
					'DestinationID' => $BusList["DestinationID"],
				);
				$nOfRecords ++;
			}
			$row++;
		}    
		// dd($Response);
		if(sizeof($Response) == 0)
			return [];
		else
			return $Response;
	}
	
	public function GetCompanySchedule($company_id,$schedule_date,$from_terminal_id,$to_city_id,$page,$per_page)
	{
		$timestamp = strtotime($schedule_date);
		$sql = "SELECT 
					schedules.id as sch_id, fares.id as fid, 
					schedule_times.departure_time as dept_time, 
					services.short_name as service_name, 
					fares.fare as fare,
					terminals.name as terminal_name
					FROM 
					schedules 
					JOIN schedule_times ON (schedules.id=schedule_times.schedule_id)
					join fare_types on (schedules.id=fare_types.schedule_id) 
					join services on (fare_types.service_id=services.id) 
					join fares on (fare_types.id=fares.fare_type_id) 
					join terminals on (fares.from_terminal_id=terminals.id) 
					where 
					schedules.".strtolower(date('l', $timestamp))."=1  and schedule_times.sequence=1 and 
					schedules.operatingend_date>'".$schedule_date."' and
					fare_types.company_id=".$company_id." 
					and
					fares.from_terminal_id =".$from_terminal_id."
					and
					fares.to_terminal_id in (
						SELECT distinct terminals.id FROM 
						terminals 
						join lgas on (terminals.lga_id=lgas.id)
						join cities on (lgas.city_id=cities.id)
						where 
						terminals.status=1 and terminals.company_id=".$company_id."
						and cities.id=".$to_city_id."
					)
					and fares.fare <> 0
					order by schedule_times.departure_time, schedules.id";
		//echo "<br/>".$sql."<br />";
		$Results = DB::Select($sql);
		// limit 3 offset ".$page
		$Response = array();
		$row = 1;
		$nOfRecords = 1;
		//print_r($Results);
		//echo "$row>=$page && $nOfRecords<=3<br />";
		foreach($Results as $Result)
		{			
			if($row>$page && $nOfRecords<=$per_page)
			{
				$info_Schedule = Schedule::FindOrFail($Result->sch_id);
				$dept_terminal = $info_Schedule->Schedule_time()->Orderby('sequence', 'desc')->First()->Terminal()->First()->name;
				$nOfRecords ++;
				$Response[] = array('rownum' => $row,
								'sch_id' => $Result->sch_id, 
								'fid' => $Result->fid,  
							 	'dept_time' => date('h:iA',strtotime($Result->dept_time)), 
								'service_name' => $Result->service_name,  
								'fare' => $Result->fare, 
								'terminal_name' => $Result->terminal_name,
								'dept_terminal' => $dept_terminal,
								'bus' => $info_Schedule->Bus()->First()->name,
								);
			}
			$row++;
		}
		//print_r($Response);
		return $Response;
	}

	
	public function GetSelectedCompanySchedule($company_id,$schedule_date,$from_terminal_id,$to_city_id,$rownum)
	{
		$timestamp = strtotime($schedule_date);
		$sql = "SELECT 
					schedules.id as sch_id, fares.id as fid, 
					schedule_times.departure_time as dept_time, 
					services.short_name as service_name, 
					fares.fare as fare,
					terminals.name as terminal_name
					FROM 
					schedules 
					JOIN schedule_times ON (schedules.id=schedule_times.schedule_id)
					join fare_types on (schedules.id=fare_types.schedule_id) 
					join services on (fare_types.service_id=services.id) 
					join fares on (fare_types.id=fares.fare_type_id) 
					join terminals on (fares.from_terminal_id=terminals.id) 
					where 
					schedules.".strtolower(date('l', $timestamp))."=1  and schedule_times.sequence=1 and 
					schedules.operatingend_date>'".$schedule_date."' and
					fare_types.company_id=".$company_id." 
					and
					fares.from_terminal_id =".$from_terminal_id."
					and
					fares.to_terminal_id in (
						SELECT distinct terminals.id FROM 
						terminals 
						join lgas on (terminals.lga_id=lgas.id)
						join cities on (lgas.city_id=cities.id)
						where 
						terminals.status=1 and terminals.company_id=".$company_id."
						and cities.id=".$to_city_id."
					)
					and fares.fare <> 0
					order by schedule_times.departure_time, schedules.id";
		//echo "<br/>".$sql."<br />";
		$Results = DB::Select($sql);
		// limit 3 offset ".$page
		$Response = array();
		$row = 1;
		foreach($Results as $Result)
		{			
			if($rownum==$row)
			{
				$Response[] = array('rownum' => $row,
								'sch_id' => $Result->sch_id, 
								'fid' => $Result->fid,  
							 	'dept_time' => $Result->dept_time, 
								'service_name' => $Result->service_name,  
								'fare' => $Result->fare, 
								'terminal_name' => $Result->terminal_name
								);
			}
			$row++;
		}
		//print_r($Response);
		return $Response;
	}
	
	public function GetActiveBooking($msisdn, $page)
	{
		$sql = "select * from (SELECT c.*, @rownum := @rownum + 1 AS rownum FROM bookings c, (SELECT @rownum := 0) r where msisdn=".$msisdn." and status=1 order by updated_at desc) data limit 4 offset ".$page;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	
	public function GetSelectedActiveBooking($msisdn,$rownum)
	{
		$sql = "select * from (SELECT c.*, @rownum := @rownum + 1 AS rownum FROM bookings c, (SELECT @rownum := 0) r where msisdn=".$msisdn." and status=1 order by updated_at desc) data where rownum=".$rownum;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	
	public function GetGeneralSetting($company_id)
	{
		//echo "<br />Call GetSelectedCompanyCityTerminalAPI<br />";
		$url = env('API_URL')."GeneralSetting/".$company_id;
		$options = array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>"Authorization: Bearer ".env('API_TOKEN')
		  )
		);
		
		$context = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
		
		$decodeData = json_decode($data, true);
		return $decodeData["data"];
	}
	
	public function GetMonifyAccount($company_id, $customer_name, $customer_email, $amount, $booking_id)
	{
		$customer_email = $customer_email == '' ? 'support@itavel.ng' : $customer_email;
		//echo "<br />Call GetSelectedCompanyCityTerminalAPI<br />";
		$url = env('API_URL')."GetMonifyAccount/".$company_id."?customer_name=".urlencode($customer_name)."&customer_email=$customer_email&amount=$amount&booking_id=$booking_id";
		//dd($url);
		$options = array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>"Authorization: Bearer ".env('API_TOKEN')
		  )
		);
		
		$context = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
		
		$decodeData = json_decode($data);
		return $decodeData;
	}
	
	public function getCompanyByUssd($ussd_code)
	{
		$info_Company_Ussd = Company_ussd::where('code',ltrim($ussd_code, '*'))->First();
		if($info_Company_Ussd)
			return Company::FindOrFail($info_Company_Ussd->company_id);
		else
			return null;
	}
	
	public function addCustomerLog($testing, $msisdn, $ussd_code, $is_main, $company_id=null, $company_name=null)
	{
		if($testing == "")
		{
			$info_Customer_Count = Customer_log::where('msisdn',$msisdn)->Get()->Count();
			
			$info_Customer_Log = new Customer_log;
			$info_Customer_Log->msisdn = $msisdn;
			$info_Customer_Log->ussd_code = $ussd_code;
			$info_Customer_Log->channel = 'USSD';
			$info_Customer_Log->is_main = $is_main;
			$info_Customer_Log->start_time = Carbon::now();
			$info_Customer_Log->end_time = Carbon::now();
			
			if($info_Customer_Count==0)
			{
				$info_Customer_Log->is_new = 1;
			}
			
			if($company_id!=null)
			{
				$info_Customer_Log->company_id = $company_id;
				$info_Customer_Log->company_name = $company_name;
			}
			
			$info_Customer_Log->save();
			
			return $info_Customer_Log->id;
		}
		else
		{
			return 0;
		}
	}
	
	public function updateCustomerLog($testing, $sql)
	{
		if($testing == "")
		{
			$Results = DB::Select($sql);
		}
	}
	
	
	public function checkUserSubscription($msisdn)
	{
		$info_Subscription_User = Subscription::where(['msisdn' => $msisdn, 'billing_date' => date('Y-m-d')])->First();
		
		if($info_Subscription_User)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public function ChargeUser($msisdn, $ussd_code)
	{
		$info_Subscription_User = Subscription::where(['msisdn' => $msisdn, 'billing_date' => date('Y-m-d')])->First();
		
		//if($info_Subscription_User)
		if(1==0)
		{
			return 2;
		}
		else
		{
			if(env('SHORT_CODE') == "88111123" && env("APP_ENV") != "adeel")
			{
				//Charge user
				$date = DateTime::createFromFormat('U.u', microtime(TRUE));
				$session =  $date->format('YmdHisu');
				$data = array(
					"serviceName" => "ITRVL10",
					"msisdn" => "08".substr($msisdn,-9),
					"id" => "ITRVL10-".$session,
					"amount" => "1000"
				);
				//print_r($data);
				$payload = json_encode($data);
				 
				// Prepare new cURL resource
				$ch = curl_init('https://directbill.9mobile.com.ng/sync');
		
				curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS
				curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1);  // RETURN THE CONTENTS OF THE CALL
		
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLINFO_HEADER_OUT, true);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
		
				//needed for https
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
				 
				// Set HTTP Header for POST request 
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					"Accept: application/json",
					"Content-Type: application/json",
					"username: IYCONSOFT",
					"Authorization: 9AFF9647F037D4E80A461A4B4A9656565FC50FCC2D5BF551A0444EDB4CACEDBF",
					"Ocp-Apim-Subscription-Key: ddbb55e046904b1eb5886622d24d3670"
					)
				);
				 
				// Submit the POST request
				$result = curl_exec($ch);
				 
				// Close cURL session handle
				curl_close($ch);
				$result = json_decode($result, true);
				//print_r($result);
			}
			else
			{
				$result["code"]=200;
			}

			if($result["code"]==200)
			{
				$info_Charge_Log = new Charging_log;
				$info_Charge_Log->msisdn = $msisdn;
				$info_Charge_Log->ussd_code = $ussd_code;
				$info_Charge_Log->billing_date = date('Y-m-d');
				$info_Charge_Log->amount = 10;
				$info_Charge_Log->is_charge = 1;
				$info_Charge_Log->created_at = date('Y-m-d H:i:s');
				$info_Charge_Log->save();
				
				$info_Subscription = new Subscription;
				$info_Subscription->msisdn = $msisdn;
				$info_Subscription->ussd_code = $ussd_code;
				$info_Subscription->billing_date = date('Y-m-d');
				$info_Subscription->amount = 10;
				$info_Subscription->created_at = date('Y-m-d H:i:s');
				$info_Subscription->save();
				
				return 1;
			}
			else
			{
				return 0;
			}
		}
	}
	
	public function emptyBalanceMessage($session_msisdn)
	{
		$output['session_operation'] = "endcontinue";		
		$output['session_msg'] = "Thanks your request has being recieved. You will recieve sms response shortly";
		
		$smsMessage = "Sorry, you do not have sufficient airtime for this service. Service cost N10/SMS. Please recharge and dial *".$session_from."#";
		$this->sendSMS($session_from,$session_msisdn, $smsMessage, $gateway);
		//
		return $output;
	}
	
	public function unAssignMessage($session_msisdn, $session_from, $testing)
	{
		$session_id = $this->addCustomerLog($testing, $session_msisdn, $session_from, 1);
		
		$output['session_operation'] = "endcontinue";		
		$output['session_msg'] = "THIS CODE IS CURRENTLY UNASSIGNED TO A COMPANY.\n* Go to MAIN";
		return $output;
	}
	
	public function balanceConfirmationMessage($session_msisdn,$session_msg,$session_from)
	{		
		$ResultDel = DB::delete("delete from ussd_menu_session where msisdn = '$session_msisdn'");
		$ResultIns = DB::insert("INSERT INTO ussd_menu_session (session_id, msisdn, ussd_code, user_level, keyword, parent_id, last_parent_id, g_parent, is_child, level_1 ,record_on) VALUES ('121212', '$session_msisdn', '$session_msg', '0', 'balMsg', '0', '0', '0', '0', '$session_msg' ,  CURRENT_TIMESTAMP)");
		
		$output['session_operation'] = "continue";		
		$output['session_msg'] = "You are about to access the ".$session_from." Travel service. Service cost 20/Day. Please press 1 to confirm";
		return $output;
	}
	
	public function sendSMS($from, $to, $message, $gateway)
	{
		$message_en = rawurlencode($message);
		$to = trim($to);
		
		if($from == "8014")
		{
			if(strtolower($gateway) == "9mobile")
			{
				$url = "http://174.143.201.191/sendsms/?username=route8014&password=p5KHA9l3rC&phoneNo=$to&message=$message_en&shortcode=$from&dtype=json&dlr=1";
			}
			else
			if(strtolower($gateway) == "glo")
			{
				$url = "http://174.143.201.191/gloSUB/sendsms/index_ninajojer.php?username=route8014&password=p5KHA9l3rC&msisdn=$to&message=$message_en&shortcode=$from";
			}
			else
			if(strtolower($gateway) == "airtel")
			{
				//$url = "https://app.multitexter.com/v2/app/sms?email=tech@iyconsoft.com&message=$message_en&recipients=$to&forcednd=1&password=sayntt123&sender_name=$from";
				//$url = "http://3.131.19.214:8802/?phonenumber=".$to."&text=".($message_en)."&sender=I-TRAVEL&user=selfserve&password=1234567891";
				$url = "http://174.143.201.191/airtelSUB/sendsms/?username=route8014&password=p5KHA9l3rC&msisdn=".$to."&message=".($message_en)."&shortcode=$from&dlr=1";
			}
			else
			{
				$url = "http://174.143.201.191/mtnSDP/sendsms/?username=route8014&password=p5KHA9l3rC&msisdn=$to&message=$message_en&shortcode=$from";
			}
		}
		else
		{
			//echo "<br />";
			$url = "http://localhost:60200/cgi-bin/sendsms?username=usms2_0&password=psms2_0&smsc=smsc-1&from=$from&to=$to&text=$message_en";
		}
		
		//echo "<br />$url";
		if(env("APP_ENV") != "adeel")
		{
			$urlresponse = file_get_contents($url);
			\Log::info($urlresponse);
			\Log::info("Send SMS URL: ".$url);
			\Log::info("Live: ".$message);
		}
		else
		{
			\Log::info($message);
		}
	}
	
	function curl_min($url)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}
	
	function isDate($date)
	{
		$matches = array();
		$pattern = '/^([0-9]{1,2})\\/([0-9]{1,2})\\/([0-9]{4})$/';
		if (!preg_match($pattern, $date, $matches)) return 0;
		if (!checkdate($matches[2], $matches[1], $matches[3])) return 0;
		return 1;
	}
	
	function convertToObject($array) {
        $mainArray = array();
		foreach ($array as $key => $value) {
            $mainArray[] =  $this->convertSingleArrayToObject($value);
        }	
        return $mainArray;
    }

	function convertSingleArrayToObject($array) {
        $object = new \stdClass();
		foreach ($array as $key => $value) {
            $object->$key = $value;
        }
        return $object;
    }
	// This function will return a random 
	// string of specified length 
	function random_strings($length_of_string) 
	{ 
		// String of all alphanumeric character 
		$str_result = '0123456789'; 
		// Shufle the $str_result and returns substring 
		// of specified length 
		return substr(str_shuffle($str_result), 0, $length_of_string); 
	} 
	
	// string of specified length 
	function AvailableSeatPaging($seatList, $blockList, $page_no) 
	{ 
		//dd($seatList);
		$infoSeatsList = array();
		$newSeatsList = array_diff($seatList,$blockList);
		for($i=$page_no-1; $i<$page_no+19; $i++)
		{
			if(isset($newSeatsList[$i]))
			{
				array_push($infoSeatsList, $seatList[$i]);
			}
		}
		//dd($infoSeatsList);
		return $infoSeatsList;
	} 
}
