<?php

namespace App\Repositories;

use App\Models\Route;
use App\Models\Route_detail;
use App\Models\Schedule;
use App\Models\Fare;
use App\Models\Fare_type;
use App\Models\Schedule_time;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Hash;

class RouteRepository {
	
	/**
	 * @var App\Models\Route
	 */
	protected $db_route;
	protected $db_route_detail;
	
	protected $db_schedule;
	protected $db_fare;
	protected $db_fare_type;
	protected $db_schedule_time;
		
    public function __construct(Route $db_route, Route_detail $db_route_detail, Schedule $db_schedule, Fare_type $db_fare_type, Fare $db_fare, Schedule_time $db_schedule_time) 
    {
        $this->db_route = $db_route;
		$this->db_route_detail = $db_route_detail;
		$this->db_schedule = $db_schedule;
		$this->db_fare_type = $db_fare_type;
		$this->db_fare = $db_fare;
		$this->db_schedule_time = $db_schedule_time;
    }
	
	public function addRoute($inputs)
    {
        $db_route = $this->storeRoute(new $this->db_route ,  $inputs);
        return $db_route;
    }
	
	public function updateRoute($inputs, $id)
	{
		$db_route = $this->db_route->findOrFail($id);
		$answer_id = $this->storeRoute($db_route, $inputs, $id);
		return $answer_id;
	}
	
	public function deleteRoute($id)
    {
		$db_schedules = $this->db_schedule->where(['route_id' => $id])->Get();
		foreach($db_schedules as $db_schedule)
		{
			$db_schedule->Bus()->detach();
			
			$db_fares = $this->db_fare->where(['schedule_id' => $db_schedule->id])->Get();
			foreach($db_fares as $db_fare)
			{
				$db_fare->delete();
			}
			
			$db_fare_types = $this->db_fare_type->where(['schedule_id' => $db_schedule->id])->Get();
			foreach($db_fare_types as $db_fare_type)
			{
				$db_fare_type->delete();
			}
			
			$db_schedule_times = $this->db_schedule_time->where(['schedule_id' => $db_schedule->id])->Get();
			foreach($db_schedule_times as $db_schedule_time)
			{
				$db_schedule_time->delete();
			}
			$db_schedule->delete();
		}
		
		
		$db_route_details = $this->db_route_detail->where(['route_id' => $id])->Get();
		foreach($db_route_details as $db_route_detail)
		{
			$db_route_detail->delete();
		}
		
		$db_route = $this->db_route->findOrFail($id);
        $db_route->delete();
        return true;
    }

	function storeRoute($db_route , $inputs, $id = null)
	{	
		$db_route->company_id = $inputs['company_id'];
		$db_route->name = $inputs['name'];
		$db_route->short_name = $inputs['short_name'];
		//Set status
		if(isset($inputs['status'])=="on")
			$db_route->status = 1;
		else
			$db_route->status = 0;
		$db_route->save();
		
		Route_detail::where('route_id',$db_route->id)->delete();
		if(isset($inputs['terminal_id']))
		{
			$route_details = $inputs['terminal_id'];
			$sequence = 1;
			foreach($route_details as $route_detail)
			{
				$db_route_detail = new $this->db_route_detail;
				$db_route_detail->route_id = $db_route->id;
				$db_route_detail->terminal_id = $route_detail;
				$db_route_detail->sequence = $sequence++;
				$db_route_detail->save();
			}
		}
		return $db_route;
	}
	
	public function getRoute($id = null)
    {
		if($id==null)
		{
			$info_Route = $this->db_route->select('id', 'company_id', 'name', 'short_name', 'status', 'created_at', 'updated_at')->orderBy('created_at', 'DESC')->get();
		}
		else
		{
			$info_Route = $this->db_route->select('id', 'company_id', 'name', 'short_name', 'status', 'created_at', 'updated_at')->findOrFail($id);
		}
        return $info_Route;
    }
	
	public function getCompanyRoute($company_id)
    {
		$info_Route = $this->db_route->select('id', 'company_id', 'name', 'short_name', 'status', 'created_at', 'updated_at')->where('company_id', '=', $company_id);
        return $info_Route;
    }
	
	public function getCompanyRouteDetail($route_id)
	{
		$info_Route_Details = $this->db_route_detail->select('id', 'route_id', 'terminal_id', 'sequence')->where('route_id', '=', $route_id)->OrderBy('sequence');
        return $info_Route_Details;
	}
}

