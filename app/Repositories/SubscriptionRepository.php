<?php

namespace App\Repositories;

use App\Models\Subscription;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Hash;

class SubscriptionRepository {
	
	/**
	 * @var App\Models\Subscription
	 */
	protected $db_subscription;
		
    public function __construct(Subscription $db_subscription) 
    {
        $this->db_subscription = $db_subscription;
    }
	
	public function addSubscription($inputs)
    {
        $db_subscription = $this->storeSubscription(new $this->db_subscription ,  $inputs);
        return $db_subscription;
    }
	
	public function updateSubscription($inputs, $id)
	{
		$db_subscription = $this->db_subscription->findOrFail($id);
		$answer_id = $this->storeSubscription($db_subscription, $inputs, $id);
		return $answer_id;
	}
	
	public function deleteSubscription($id)
    {
		$db_subscription = $this->db_subscription->findOrFail($id);
		$db_subscription->delete();
		return true;
    }

	function storeSubscription($db_subscription , $inputs, $id = null)
	{	
		$db_subscription->msisdn = $inputs['msisdn'];
		$db_subscription->ussd_code = $inputs['ussd_code'];
		$db_subscription->billing_date = $inputs['billing_date'];
		$db_subscription->amount = $inputs['amount'];
		
		return $db_subscription;
	}
	
	public function getSubscription($id = null)
    {
		if($id==null)
		{
			$info_Subscription = $this->db_subscription->select('id', 'msisdn', 'ussd_code', 'billing_date', 'amount', 'created_at', 'updated_at')->orderBy('created_at', 'DESC')->get();
		}
		else
		{
			$info_Subscription = $this->db_subscription->select('id', 'msisdn', 'ussd_code', 'billing_date', 'amount', 'created_at', 'updated_at')->findOrFail($id);
		}
        return $info_Subscription;
    }
	
	public function getCompanySubscription($ussd_code)
    {
		$info_Subscription = $this->db_subscription->select('id', 'msisdn', 'ussd_code', 'billing_date', 'amount', 'created_at', 'updated_at')->where('ussd_code', '=', '*'.$ussd_code)->orderBy('created_at', 'DESC')->get();
        return $info_Subscription;
    }
}

