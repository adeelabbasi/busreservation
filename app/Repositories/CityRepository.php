<?php

namespace App\Repositories;

use App\Models\City;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Hash;

class CityRepository {
	
	/**
	 * @var App\Models\City
	 */
	protected $db_city;
		
    public function __construct(City $db_city) 
    {
        $this->db_city = $db_city;
    }
	
	public function addCity($inputs)
    {
        $db_city = $this->storeCity(new $this->db_city ,  $inputs);
        return $db_city;
    }
	
	public function updateCity($inputs, $id)
	{
		$db_city = $this->db_city->findOrFail($id);
		$answer_id = $this->storeCity($db_city, $inputs, $id);
		return $answer_id;
	}
	
	public function deleteCity($id)
    {
		$db_city = $this->db_city->findOrFail($id);
        $db_city->delete();
        return true;
    }
	
	public function updateSequence($id, $sequence)
    {
		$db_city = $this->db_city->findOrFail($id);
        $db_city->sequence = $sequence;
		$db_city->save();
        return true;
    }

	function storeCity($db_city , $inputs, $id = null)
	{	
		$db_city->country_id = $inputs['country_id'];
		$db_city->name = $inputs['name'];
		$db_city->short_name = $inputs['short_name'];
		//Set status
		if(isset($inputs['status'])=="on")
			$db_city->status = 1;
		else
			$db_city->status = 0;
		$db_city->save();
		return $db_city;
	}
	
	public function getCity($id = null)
    {
		if($id==null)
		{
			$info_City = $this->db_city->select('id', 'country_id', 'name', 'short_name', 'status', 'sequence', 'created_at', 'updated_at')->orderBy('name')->get();
		}
		else
		{
			$info_City = $this->db_city->select('id', 'country_id', 'name', 'short_name', 'status', 'sequence', 'created_at', 'updated_at')->findOrFail($id);
		}
        return $info_City;
    }
	
	public function getCountryCity($country_id)
    {
		$info_City = $this->db_city->select('id', 'country_id', 'name', 'short_name', 'status', 'sequence', 'created_at', 'updated_at')->where('status', '=', '1')->where('country_id', '=', $country_id)->orderBy('name');
        return $info_City;
    }
}

