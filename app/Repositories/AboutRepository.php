<?php

namespace App\Repositories;

use App\Models\About;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Hash;

class AboutRepository {
	
	/**
	 * @var App\Models\About
	 */
	protected $db_about;
		
    public function __construct(About $db_about) 
    {
        $this->db_about = $db_about;
    }
	
	public function addAbout($inputs)
    {
        $db_about = $this->storeAbout(new $this->db_about ,  $inputs);
        return $db_about;
    }
	
	public function updateAbout($inputs, $id)
	{
		$db_about = $this->db_about->findOrFail($id);
		$answer_id = $this->storeAbout($db_about, $inputs, $id);
		return $answer_id;
	}
	
	public function deleteAbout($id)
    {
		$db_about = $this->db_about->findOrFail($id);
        $db_about->delete();
        return true;
    }

	function storeAbout($db_about , $inputs, $id = null)
	{	
		$db_about->company_id = $inputs['company_id'];
		$db_about->name = $inputs['name'];
		$db_about->detail = $inputs['detail'];
		//Set status
		if(isset($inputs['status'])=="on")
			$db_about->status = 1;
		else
			$db_about->status = 0;
		$db_about->save();
		return $db_about;
	}
	
	public function getAbout($id = null)
    {
		if($id==null)
		{
			$info_About = $this->db_about->select('id', 'company_id', 'name', 'detail', 'status', 'created_at', 'updated_at')->orderBy('created_at', 'DESC')->get();
		}
		else
		{
			$info_About = $this->db_about->select('id', 'company_id', 'name', 'detail', 'status', 'created_at', 'updated_at')->findOrFail($id);
		}
        return $info_About;
    }
	
	public function getCompanyAbout($company_id)
    {
		$info_About = $this->db_about->select('id', 'company_id', 'name', 'detail', 'status', 'created_at', 'updated_at')->where('company_id', '=', $company_id);
        return $info_About;
    }
}

