<?php

namespace App\Repositories;

use App\Models\Deal;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Hash;

class DealRepository {
	
	/**
	 * @var App\Models\Deal
	 */
	protected $db_deal;
		
    public function __construct(Deal $db_deal) 
    {
        $this->db_deal = $db_deal;
    }
	
	public function addDeal($inputs)
    {
        $db_deal = $this->storeDeal(new $this->db_deal ,  $inputs);
        return $db_deal;
    }
	
	public function updateDeal($inputs, $id)
	{
		$db_deal = $this->db_deal->findOrFail($id);
		$answer_id = $this->storeDeal($db_deal, $inputs, $id);
		return $answer_id;
	}
	
	public function deleteDeal($id)
    {
		$db_deal = $this->db_deal->findOrFail($id);
        $db_deal->delete();
        return true;
    }

	function storeDeal($db_deal , $inputs, $id = null)
	{	
		$db_deal->company_id = $inputs['company_id'];
		$db_deal->name = $inputs['name'];
		$db_deal->detail = $inputs['detail'];
		$db_deal->start_date = $inputs['start_date'];
		$db_deal->end_date = $inputs['end_date'];
		//Set status
		if(isset($inputs['status'])=="on")
			$db_deal->status = 1;
		else
			$db_deal->status = 0;
		$db_deal->save();
		return $db_deal;
	}
	
	public function getDeal($id = null)
    {
		if($id==null)
		{
			$info_Deal = $this->db_deal->select('id', 'company_id', 'name', 'detail', 'start_date', 'end_date', 'status', 'created_at', 'updated_at')->orderBy('created_at', 'DESC')->get();
		}
		else
		{
			$info_Deal = $this->db_deal->select('id', 'company_id', 'name', 'detail', 'start_date', 'end_date', 'status', 'created_at', 'updated_at')->findOrFail($id);
		}
        return $info_Deal;
    }
	
	public function getCompanyDeal($company_id)
    {
		$info_Deal = $this->db_deal->select('id', 'company_id', 'name', 'detail', 'start_date', 'end_date', 'status', 'created_at', 'updated_at')->where('company_id', '=', $company_id);
        return $info_Deal;
    }
}

