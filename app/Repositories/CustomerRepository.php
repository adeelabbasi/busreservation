<?php

namespace App\Repositories;

use App\Models\Customer;
use App\Models\Customer_log;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Hash;

class CustomerRepository {
	
	/**
	 * @var App\Models\Customer
	 */
	protected $db_customer;
	protected $db_Customerlog;
		
    public function __construct(Customer $db_customer, Customer_log $db_Customerlog) 
    {
        $this->db_customer = $db_customer;
		$this->db_Customerlog = $db_Customerlog;
    }
	
	public function addCustomer($inputs)
    {
        $db_customer = $this->storeCustomer(new $this->db_customer ,  $inputs);
        return $db_customer;
    }
	
	public function updateCustomer($inputs, $id)
	{
		$db_customer = $this->db_customer->findOrFail($id);
		$answer_id = $this->storeCustomer($db_customer, $inputs, $id);
		return $answer_id;
	}
	
	public function deleteCustomer($id)
    {
		$db_customer = $this->db_customer->findOrFail($id);
        $db_customer->delete();
        return true;
    }

	function storeCustomer($db_customer , $inputs, $id = null)
	{	
		$db_customer->title = $inputs['title'];
		$db_customer->first_name = $inputs['first_name'];
		$db_customer->last_name = $inputs['last_name'];
		$db_customer->other_name = $inputs['other_name'];
		$db_customer->address = $inputs['address'];
		$db_customer->martial_status = $inputs['martial_status'];
		$db_customer->gender = $inputs['gender'];
		$db_customer->work_status = $inputs['work_status'];
		$db_customer->mprs_gsm_no = $inputs['mprs_gsm_no'];
		$db_customer->gsm_no_2 = $inputs['gsm_no_2'];
		$db_customer->gsm_no_3 = $inputs['gsm_no_3'];
		$db_customer->email = $inputs['email'];
		
		if(isset($inputs['pin']) && $inputs['pin']!="")
			$db_customer->pin = $inputs['pin'];
		else
			$db_customer->pin = "";
			
			
		$db_customer->kin_fullname = $inputs['kin_fullname'];
		$db_customer->kin_phone = $inputs['kin_phone'];
		$db_customer->kin_address = $inputs['kin_address'];
		//Set status
		if(isset($inputs['status'])=="on")
			$db_customer->status = 1;
		else
			$db_customer->status = 0;
		$db_customer->save();
		return $db_customer;
	}
	
	public function getCustomer($id = null)
    {
		if($id==null)
		{
			$info_Customer = $this->db_customer->select('id', 'title', 'first_name', 'last_name', 'other_name', 'address', 'martial_status', 'gender', 'work_status', 'mprs_gsm_no', 'gsm_no_2', 'gsm_no_3', 'email', 'pin', 'kin_fullname', 'kin_phone', 'kin_address', 'status', 'created_at', 'updated_at')->orderBy('created_at', 'DESC')->get();
		}
		else
		{
			$info_Customer = $this->db_customer->select('id', 'title', 'first_name', 'last_name', 'other_name', 'address', 'martial_status', 'gender', 'work_status', 'mprs_gsm_no', 'gsm_no_2', 'gsm_no_3', 'email', 'pin', 'kin_fullname', 'kin_phone', 'kin_address', 'status', 'created_at', 'updated_at')->findOrFail($id);
		}
        return $info_Customer;
    }
	
	public function getCustomerLog($type)
    {
		if($type==1)
		{
			$info_Customerlog = $this->db_Customerlog->select('id', 'gateway', 'customer_id', 'msisdn', 'ussd_code', 'action', 'start_time', 'end_time', 'created_at', 'updated_at')->orderBy('created_at', 'DESC');
		}
		else
		{
			$info_Customerlog = $this->db_Customerlog->select('id', 'gateway',  'customer_id', 'msisdn', 'ussd_code', 'action', 'start_time', 'end_time', 'created_at', 'updated_at')->orderBy('created_at', 'DESC')->WhereNull('admin_id');
		}
        return $info_Customerlog;
    }
	
	public function getCompanyCustomerLog($ussd_code)
    {
		$info_Customerlog = $this->db_Customerlog->select('id', 'gateway',  'customer_id', 'msisdn', 'ussd_code', 'action', 'created_at', 'updated_at')->orderBy('created_at', 'DESC')->where('ussd_code', '*'.$ussd_code)->orderBy('created_at', 'DESC');
        return $info_Customerlog;
    }
	
	public function getSearchCustomerLog($type,$from,$to)
    {
		if($type==1)
		{
			$info_Customerlog = $this->db_Customerlog->select('id', 'gateway', 'customer_id', 'msisdn', 'ussd_code', 'action', 'start_time', 'end_time', 'created_at', 'updated_at')->Where('created_at', '>=', $from)->Where('created_at', '<=', $to)->orderBy('created_at', 'DESC');
		}
		else
		{
			$info_Customerlog = $this->db_Customerlog->select('id', 'gateway',  'customer_id', 'msisdn', 'ussd_code', 'action', 'start_time', 'end_time', 'created_at', 'updated_at')->Where('created_at', '>=', $from)->Where('created_at', '<=', $to)->orderBy('created_at', 'DESC')->WhereNull('admin_id');
		}
        return $info_Customerlog;
    }
	
	public function getSearchCompanyCustomerLog($ussd_code,$from,$to)
    {
		$info_Customerlog = $this->db_Customerlog->select('id', 'gateway',  'customer_id', 'msisdn', 'ussd_code', 'action', 'created_at', 'updated_at')->orderBy('created_at', 'DESC')->Where('created_at', '>=', $from)->Where('created_at', '<=', $to)->where('ussd_code', '*'.$ussd_code)->orderBy('created_at', 'DESC');
        return $info_Customerlog;
    }
}

