<?php

namespace App\Repositories;

use App\Models\Schedule;
use App\Models\Fare;
use App\Models\Fare_type;
use App\Models\Schedule_time;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Hash;

class ScheduleRepository {
	
	/**
	 * @var App\Models\Schedule
	 */
	protected $db_schedule;
	protected $db_fare;
	protected $db_fare_type;
	protected $db_schedule_time;
		
    public function __construct(Schedule $db_schedule, Fare_type $db_fare_type, Fare $db_fare, Schedule_time $db_schedule_time) 
    {
        $this->db_schedule = $db_schedule;
		$this->db_fare_type = $db_fare_type;
		$this->db_fare = $db_fare;
		$this->db_schedule_time = $db_schedule_time;
    }
	
	public function addSchedule($inputs)
    {
        $db_schedule = $this->storeSchedule(new $this->db_schedule ,  $inputs);
        return $db_schedule;
    }
	
	public function updateSchedule($inputs, $id)
	{
		$db_schedule = $this->db_schedule->findOrFail($id);
		$answer_id = $this->storeSchedule($db_schedule, $inputs, $id);
		return $answer_id;
	}
	
	public function deleteSchedule($id)
    {
		$db_fares = $this->db_fare->where(['schedule_id' => $id])->Get();
		foreach($db_fares as $db_fare)
		{
			$db_fare->delete();
		}
		
		$db_fare_types = $this->db_fare_type->where(['schedule_id' => $id])->Get();
		foreach($db_fare_types as $db_fare_type)
		{
			$db_fare_type->delete();
		}
		
		$db_schedule_times = $this->db_schedule_time->where(['schedule_id' => $id])->Get();
		foreach($db_schedule_times as $db_schedule_time)
		{
			$db_schedule_time->delete();
		}
				
		$db_schedule = $this->db_schedule->findOrFail($id);
		$db_schedule->Bus()->detach();
        $db_schedule->delete();
        return true;
    }

	function storeSchedule($db_schedule , $inputs, $id = null)
	{	
		$db_schedule->company_id = $inputs['company_id'];
		$db_schedule->route_id = $inputs['route_id'];
		$db_schedule->short_name = $inputs['short_name'];
		$db_schedule->departure_time = date("H:i", strtotime(date('Y-m-d')));
		$db_schedule->duration = '1';
		$db_schedule->effective_Date = $inputs['effective_Date'];
		$db_schedule->operatingend_date = $inputs['operatingend_date'];
		$db_schedule->monday = (isset($inputs['monday'])?1:0);
		$db_schedule->tuesday = (isset($inputs['tuesday'])?1:0);
		$db_schedule->wednesday = (isset($inputs['wednesday'])?1:0);
		$db_schedule->thursday = (isset($inputs['thursday'])?1:0);
		$db_schedule->friday = (isset($inputs['friday'])?1:0);
		$db_schedule->saturday = (isset($inputs['saturday'])?1:0);
		$db_schedule->sunday = (isset($inputs['sunday'])?1:0);
		//Set status
		if(isset($inputs['status'])=="on")
			$db_schedule->status = 1;
		else
			$db_schedule->status = 0;
		$db_schedule->save();
		
		$db_schedule->Bus()->detach();
		$db_schedule->Bus()->attach($inputs['bus_ids']);
		
		if($id!=null)
		{
			$db_schedule_times = $this->db_schedule_time->where(['schedule_id' => $db_schedule->id])->Get();
			foreach($db_schedule_times as $db_schedule_time)
			{
				$db_schedule_time->delete();
			}
		}
		
		foreach($inputs as $index => $value)
		{
			if (strpos($index, 'time_arrv') !== false) 
			{
				$Times = explode("_",$index);
			
				$db_schedule_time = $this->db_schedule_time->where(['company_id' => $db_schedule->company_id, 'schedule_id' => $db_schedule->id, 'route_id' => $Times[2], 'terminal_id' => $Times[3], 'sequence' => $Times[4]])->first();
				
				if(!$db_schedule_time)
				{
					$db_schedule_time = new $this->db_schedule_time;
				}
				$db_schedule_time->company_id = $db_schedule->company_id;
				$db_schedule_time->schedule_id = $db_schedule->id;
				$db_schedule_time->route_id = $Times[2];
				$db_schedule_time->terminal_id = $Times[3];
				$db_schedule_time->sequence = $Times[4];
				$db_schedule_time->arrival_time = date("H:i", strtotime($value));
				$db_schedule_time->save();
			}
			if (strpos($index, 'time_dept') !== false) 
			{
				$Times = explode("_",$index);
			
				$db_schedule_time = $this->db_schedule_time->where(['company_id' => $db_schedule->company_id, 'schedule_id' => $db_schedule->id, 'route_id' => $Times[2], 'terminal_id' => $Times[3], 'sequence' => $Times[4]])->first();
				
				if(!$db_schedule_time)
				{
					$db_schedule_time = new $this->db_schedule_time;
				}
				$db_schedule_time->company_id = $db_schedule->company_id;
				$db_schedule_time->schedule_id = $db_schedule->id;
				$db_schedule_time->route_id = $Times[2];
				$db_schedule_time->terminal_id = $Times[3];
				$db_schedule_time->sequence = $Times[4];
				$db_schedule_time->departure_time = date("H:i", strtotime($value));
				$db_schedule_time->save();
			}
		}
		
		return $db_schedule;
	}
	
	function copySchedule($id)
	{
		$db_schedule_main = $this->db_schedule->findOrFail($id);
		
		$db_schedule = new $this->db_schedule; 
		$db_schedule->company_id = $db_schedule_main->company_id;
		$db_schedule->route_id = $db_schedule_main->route_id;
		$db_schedule->short_name = $db_schedule_main->short_name;
		$db_schedule->departure_time = $db_schedule_main->departure_time;
		$db_schedule->duration = $db_schedule_main->duration;
		$db_schedule->effective_Date = $db_schedule_main->effective_Date;
		$db_schedule->operatingend_date = $db_schedule_main->operatingend_date;
		$db_schedule->monday = $db_schedule_main->monday;
		$db_schedule->tuesday = $db_schedule_main->tuesday;
		$db_schedule->wednesday = $db_schedule_main->wednesday;
		$db_schedule->thursday = $db_schedule_main->thursday;
		$db_schedule->friday = $db_schedule_main->friday;
		$db_schedule->saturday = $db_schedule_main->saturday;
		$db_schedule->sunday = $db_schedule_main->sunday;
		$db_schedule->status = 1;
		$db_schedule->save();
		
		$db_schedule->Bus()->attach($db_schedule_main->Bus()->Pluck('id'));
		
		foreach($db_schedule_main->Fare_type()->Get() as $db_fare_type_main)
		{
			$db_fare_type = new $this->db_fare_type;
			$db_fare_type->company_id = $db_fare_type_main->company_id;
			$db_fare_type->schedule_id = $db_schedule->id;
			$db_fare_type->service_id = $db_fare_type_main->service_id;
			$db_fare_type->name= $db_fare_type_main->name;
			$db_fare_type->save();
			foreach($db_fare_type_main->Fare()->Get() as $db_fare_main)
			{
				$db_fare = new $this->db_fare;
				$db_fare->company_id = $db_fare_main->company_id;
				$db_fare->schedule_id = $db_schedule->id;
				$db_fare->fare_type_id = $db_fare_type->id;
				$db_fare->route_id = $db_fare_main->route_id;
				$db_fare->from_terminal_id = $db_fare_main->from_terminal_id;
				$db_fare->to_terminal_id = $db_fare_main->to_terminal_id;
				$db_fare->fare = $db_fare_main->fare;
				$db_fare->fare_return = $db_fare_main->fare_return;
				$db_fare->save();
			}
		}
		
		foreach($db_schedule_main->Schedule_time()->Get() as $db_schedule_time_main)
		{
			$db_schedule_time = new $this->db_schedule_time;
			$db_schedule_time->company_id = $db_schedule_time_main->company_id;
			$db_schedule_time->schedule_id = $db_schedule->id;
			$db_schedule_time->route_id = $db_schedule_time_main->route_id;
			$db_schedule_time->terminal_id = $db_schedule_time_main->terminal_id;
			$db_schedule_time->sequence = $db_schedule_time_main->sequence;
			$db_schedule_time->arrival_time = $db_schedule_time_main->arrival_time;
			$db_schedule_time->departure_time = $db_schedule_time_main->departure_time;
			$db_schedule_time->save();
		}
		
		return $db_schedule;
	}
	
	public function getSchedule($id = null)
    {
		if($id==null)
		{
			$info_Schedule = $this->db_schedule->select('id', 'company_id', 'short_name', 'route_id', 'departure_time', 'duration', 'effective_Date', 'operatingend_date', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday', 'status', 'created_at', 'updated_at')->orderBy('created_at', 'DESC')->get();
		}
		else
		{
			$info_Schedule = $this->db_schedule->select('id', 'company_id', 'short_name', 'route_id', 'departure_time', 'duration', 'effective_Date', 'operatingend_date', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday', 'status', 'created_at', 'updated_at')->findOrFail($id);
		}
        return $info_Schedule;
    }
	
	public function getScheduleByIds($Ids=null)
    {
		$info_Schedule = $this->db_schedule->select('id', 'company_id', 'short_name', 'route_id', 'departure_time', 'duration', 'effective_Date', 'operatingend_date', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday', 'status', 'created_at', 'updated_at')->WhereIn('id',$Ids)->orderBy('created_at', 'DESC')->get();
        return $info_Schedule;
    }
	
	public function getCompanySchedule($company_id)
    {
		$info_Schedule = $this->db_schedule->select('id', 'company_id', 'short_name', 'route_id', 'departure_time', 'duration', 'effective_Date', 'operatingend_date', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday', 'status', 'created_at', 'updated_at')->where('company_id', '=', $company_id);
        return $info_Schedule;
    }
	
	public function getSearchSchedule($company_id, $FromCity, $ToCity, $FromTerminal, $ToTerminal, $effectiveDate)
    {
		$FromCityCheck = "";
		$ToCityCheck = "";
		$FromTerminalCheck = "";
		$ToTerminalCheck = "";
		$effectiveDateCheck = "";
		
		if($FromCity!="")
		{
			$FromCityCheck = " and fares.from_terminal_id in (
					SELECT distinct terminals.id FROM 
					terminals 
					join lgas on (terminals.lga_id=lgas.id)
					join cities on (lgas.city_id=cities.id)
					where terminals.status=1
					and cities.id=".$FromCity."
				)";
		}
		
		if($ToCity!="")
		{
			$ToCityCheck = " and fares.to_terminal_id in (
					SELECT distinct terminals.id FROM 
					terminals 
					join lgas on (terminals.lga_id=lgas.id)
					join cities on (lgas.city_id=cities.id)
					where terminals.status=1
					and cities.id=".$ToCity."
				)";
		}
				
		if($FromTerminal!="")
		{
			$FromTerminalCheck = " and fares.from_terminal_id = ".$FromTerminal;
		}
		
		if($ToTerminal!="")
		{
			$ToTerminalCheck = " and fares.to_terminal_id = ".$ToTerminal;
		}
		
		if($effectiveDate!="")
		{
			$effectiveDateCheck = " and schedules.".strtolower(date('l', strtotime($effectiveDate)))."=1  and schedule_times.sequence=1 and schedules.operatingend_date>'".strtotime($effectiveDate)."'";
		}
		
		if($company_id==null)
		{
			$sql = "SELECT 
				schedules.id
				FROM 
				schedules 
				JOIN schedule_times ON (schedules.id=schedule_times.schedule_id)
				join fare_types on (schedules.id=fare_types.schedule_id) 
				join services on (fare_types.service_id=services.id) 
				join fares on (fare_types.id=fares.fare_type_id) 
				join terminals on (fares.from_terminal_id=terminals.id) 
				where 1=1
				".$effectiveDateCheck."
				".$FromTerminalCheck."
				".$ToTerminalCheck."
				".$FromCityCheck."
				".$ToCityCheck."
				order by schedule_times.departure_time, schedules.id";
		}
		else
		{
			$sql = "SELECT 
				schedules.id
				FROM 
				schedules 
				JOIN schedule_times ON (schedules.id=schedule_times.schedule_id)
				join fare_types on (schedules.id=fare_types.schedule_id) 
				join services on (fare_types.service_id=services.id) 
				join fares on (fare_types.id=fares.fare_type_id) 
				join terminals on (fares.from_terminal_id=terminals.id) 
				where 
				fare_types.company_id=".$company_id." 
				".$effectiveDateCheck."
				".$FromTerminalCheck."
				".$ToTerminalCheck."
				".$FromCityCheck."
				".$ToCityCheck."
				order by schedule_times.departure_time, schedules.id";
		}
		//echo "<br/>".$sql."<br />";
		$Results = DB::Select($sql);
        return $Results;
    }
	
	public function getCompanyScheduleTime($company_id)
    {
		$info_Schedule_Time = $this->db_schedule_time->select('id', 'company_id', 'schedule_id', 'route_id', 'terminal_id', 'departure_time', 'arrival_time', 'created_at', 'updated_at')->where('company_id', '=', $company_id);
        return $info_Schedule_Time;
    }
}

