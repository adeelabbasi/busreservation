<?php

namespace App\Repositories;

use DB;
use Illuminate\Support\Facades\Hash;

class ReportRepository {
		
    public function __construct() 
    {
		
    }
	
	public function getCompany()
	{
		$Result  = DB::Select("select count(*) as counts from companies");
		return $Result[0]->counts;
	}
	
	public function getCountry()
	{
		$Result  = DB::Select("select count(*) as counts from countries");
		return $Result[0]->counts;
	}
	
	public function getCity()
	{
		$Result  = DB::Select("select count(*) as counts from cities");
		return $Result[0]->counts;
	}
	
	public function getLga()
	{
		$Result  = DB::Select("select count(*) as counts from lgas");
		return $Result[0]->counts;
	}
	
	public function getTerminalReport($company_id = null)
    {
		if($company_id==null)
		{
			$where = "";
		}
		else
		{
			$where = "companies.id='".$company_id."'";
		}
		
		$sql = "select companies.name as name, count(*) as counts from companies inner join terminals on ( companies.id = terminals.company_id) where 1=1 $where group by companies.name";
		
		return DB::Select($sql);
    }
	
	public function getFleetReport($company_id = null)
    {
		if($company_id==null)
		{
			$where = "";
		}
		else
		{
			$where = "companies.id='".$company_id."'";
		}
		
		$sql = "select companies.name as name, count(*) as counts from companies inner join buses on ( companies.id = buses.company_id) where 1=1 $where group by companies.name";
		
		return DB::Select($sql);
    }
	
	public function getServiceReport($company_id = null)
    {
		if($company_id==null)
		{
			$where = "";
		}
		else
		{
			$where = "companies.id='".$company_id."'";
		}
		
		$sql = "select companies.name as name, count(*) as counts from companies inner join services on ( companies.id = services.company_id) where 1=1 $where group by companies.name";
		
		return DB::Select($sql);
    }
	
	public function getRouteReport($company_id = null)
    {
		if($company_id==null)
		{
			$where = "";
		}
		else
		{
			$where = "companies.id='".$company_id."'";
		}
		
		$sql = "select companies.name as name, count(*) as counts from companies inner join routes on ( companies.id = routes.company_id) where 1=1 $where group by companies.name";
		
		return DB::Select($sql);
    }
	
	public function getScheduleReport($company_id = null)
    {
if($company_id==null)
		{
			$where = "";
		}
		else
		{
			$where = "companies.id='".$company_id."'";
		}			
		
		$sql = "select companies.name as name, count(*) as counts from companies inner join schedules on ( companies.id = schedules.company_id) where 1=1 $where group by companies.name";
		
		return DB::Select($sql);
    }
	
	public function getUserReport($company_id = null)
    {
		if($company_id==null)
		{
			$where = "";
		}
		else
		{
			$where = "companies.id='".$company_id."'";
		}
		
		$sql = "select companies.name as name, count(*) as counts from companies inner join users on ( companies.id = users.company_id) where 1=1 $where group by companies.name";
		
		return DB::Select($sql);
    }
	
	public function getScheduleByDayReport($company_id = null)
    {
		if($company_id==null)
		{
			$where = "";
		}
		else
		{
			$where = "companies.id='".$company_id."'";
		}
		
		$sql = "select companies.name as name, count(*) as counts, sum(schedules.monday) as monday, sum(schedules.tuesday) as tuesday, sum(schedules.wednesday) as wednesday, sum(schedules.thursday) as thursday, sum(schedules.friday) as friday, sum(schedules.saturday) as saturday, sum(schedules.sunday) as sunday from companies inner join schedules on ( companies.id = schedules.company_id) where 1=1 $where group by companies.name";
		
		return DB::Select($sql);
    }
	
	public function getCustomersReportCount($ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='".$ussd_code."'";
		}
		
		$sql = "SELECT 'Customers',
(SELECT count(msisdn) FROM customer_logs where channel='USSD' $where ) as Total,
(SELECT count(msisdn) FROM customer_logs where channel='USSD' $where group by DAY(created_at)) as Day,
(SELECT count(msisdn) FROM customer_logs where channel='USSD' $where group by MONTH(created_at)) as Month,
(SELECT count(msisdn) FROM customer_logs where channel='USSD' $where group by YEAR(created_at)) as Year";
		
		
		return DB::Select($sql);
    }
	
	public function getCustomersReportExport($type, $ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='".$ussd_code."'";
		}
		
		switch($type)
		{
			case 'Total':
				$sql = "SELECT msisdn FROM customer_logs where channel='USSD' $where";
			break;
			case 'Day':
				$sql = "SELECT msisdn FROM customer_logs where channel='USSD' $where and DAY(created_at) = DAY(CURRENT_DATE())";
			break;
			case 'Month':
				$sql = "SELECT msisdn FROM customer_logs where channel='USSD' $where and MONTH(created_at) = MONTH(CURRENT_DATE())";
			break;
			case 'Year':
				$sql = "SELECT msisdn FROM customer_logs where channel='USSD' $where and YEAR(created_at) = YEAR(CURRENT_DATE())";
			break;
		}
		
		return DB::Select($sql);
    }
	
	public function getUniqueCustomersReportCount($ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='".$ussd_code."'";
		}
		
		$sql = "SELECT 'Customers',
(SELECT count(distinct msisdn) FROM customer_logs where channel='USSD' $where ) as Total,
(SELECT count(distinct msisdn) FROM customer_logs where channel='USSD' $where group by DAY(created_at)) as Day,
(SELECT count(distinct msisdn) FROM customer_logs where channel='USSD' $where group by MONTH(created_at)) as Month,
(SELECT count(distinct msisdn) FROM customer_logs where channel='USSD' $where group by YEAR(created_at)) as Year";
		
		
		return DB::Select($sql);
    }
	
	public function getUniqueCustomersReportExport($type, $ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='".$ussd_code."'";
		}
		
		switch($type)
		{
			case 'Total':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' $where";
			break;
			case 'Day':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' $where and DAY(created_at) = DAY(CURRENT_DATE())";
			break;
			case 'Month':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' $where and MONTH(created_at) = MONTH(CURRENT_DATE())";
			break;
			case 'Year':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' $where and YEAR(created_at) = YEAR(CURRENT_DATE())";
			break;
		}
		
		return DB::Select($sql);
    }
	
	public function getShortcodeStringsReportCount($ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='".$ussd_code."'";
		}
		
		$sql = "Select ussd_codet Ussd, count as Total, countDay as Day, countMonth as Month, countYear as Year
from
(SELECT ussd_code as ussd_codet, count(msisdn) as count FROM customer_logs where channel='USSD' $where and ussd_code<>'' group by ussd_codet) t LEFT JOIN
(SELECT ussd_code as ussd_codea, count(msisdn) as countDay FROM customer_logs where channel='USSD' $where and ussd_code<>'' group by ussd_codea, DAY(created_at)) a on (t.ussd_codet=a.ussd_codea) LEFT JOIN
(SELECT ussd_code as ussd_codeb, count(msisdn) as countMonth FROM customer_logs where channel='USSD' $where and ussd_code<>'' group by ussd_codeb, MONTH(created_at)) b on (a.ussd_codea=b.ussd_codeb) LEFT JOIN
(SELECT ussd_code as ussd_codec, count(msisdn) as countYear FROM customer_logs where channel='USSD' $where and ussd_code<>'' group by ussd_codec, YEAR(created_at)) c on (b.ussd_codeb=c.ussd_codec)";
		
		
		return DB::Select($sql);
    }
	
	public function getShortcodeStringsReportExport($type, $ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='".$ussd_code."'";
		}
		
		switch($type)
		{
			case 'Total':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' $where";
			break;
			case 'Day':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' $where and DAY(created_at) = DAY(CURRENT_DATE())";
			break;
			case 'Month':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' $where and MONTH(created_at) = MONTH(CURRENT_DATE())";
			break;
			case 'Year':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' $where and YEAR(created_at) = YEAR(CURRENT_DATE())";
			break;
		}
		
		return DB::Select($sql);
    }
	
	public function getCompanyRequestsReportCount($ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='".$ussd_code."'";
		}
		
		$sql = "Select company_namet Company, ussd_codet as ussd_code, count as Total, countDay as Day, countMonth as Month, countYear as Year
from
(SELECT company_name as company_namet, ussd_code as ussd_codet, count(msisdn) as count FROM customer_logs where channel='USSD' and company_name<>'' and is_main=0 $where group by company_namet, ussd_codet) t LEFT JOIN
(SELECT company_name as company_namea, ussd_code as ussd_codea, count(msisdn) as countDay FROM customer_logs where channel='USSD' and company_name<>'' and is_main=0 $where group by company_namea, ussd_codea, DAY(created_at)) a on (t.company_namet=a.company_namea) LEFT JOIN
(SELECT company_name as company_nameb, ussd_code as ussd_codeb, count(msisdn) as countMonth FROM customer_logs where channel='USSD' and company_name<>'' and is_main=0 $where group by company_nameb, ussd_codeb, MONTH(created_at)) b on (a.company_namea=b.company_nameb) LEFT JOIN
(SELECT company_name as company_namec, ussd_code as ussd_codec, count(msisdn) as countYear FROM customer_logs where channel='USSD' and company_name<>'' and is_main=0 $where group by company_namec, ussd_codec, YEAR(created_at)) c on (b.company_nameb=c.company_namec)";
		
		
		return DB::Select($sql);
    }
	
	public function getCompanyRequestsReportExport($type, $company_name, $ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='".$ussd_code."'";
		}
		
		switch($type)
		{
			case 'Total':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and company_name='$company_name' and is_main=0  $where";
			break;
			case 'Day':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and company_name='$company_name' and is_main=0  $where and DAY(created_at) = DAY(CURRENT_DATE())";
			break;
			case 'Month':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and company_name='$company_name' and is_main=0  $where and MONTH(created_at) = MONTH(CURRENT_DATE())";
			break;
			case 'Year':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and company_name='$company_name' and is_main=0 $where and YEAR(created_at) = YEAR(CURRENT_DATE())";
			break;
		}
		
		return DB::Select($sql);
    }
	
	public function getRoutesReportCount($ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='".$ussd_code."'";
		}
		
		$sql = "Select company_namet Company, route_namet Route, count as Total, countDay as Day, countMonth as Month, countYear as Year
from
(SELECT company_name as company_namet, route_name as route_namet, count(msisdn) as count FROM customer_logs where channel='USSD' $where and route_name<>'' group by company_namet, route_namet) t LEFT JOIN
(SELECT company_name as company_namea, route_name as route_namea, count(msisdn) as countDay FROM customer_logs where channel='USSD' $where and route_name<>'' group by company_namea, route_namea, DAY(created_at)) a on (t.route_namet=a.route_namea) LEFT JOIN
(SELECT company_name as company_nameb, route_name as route_nameb, count(msisdn) as countMonth FROM customer_logs where channel='USSD' $where and route_name<>'' group by company_nameb, route_nameb, MONTH(created_at)) b on (a.route_namea=b.route_nameb) LEFT JOIN
(SELECT company_name as company_namec, route_name as route_namec, count(msisdn) as countYear FROM customer_logs where channel='USSD' $where and route_name<>'' group by company_namec, route_namec, YEAR(created_at)) c on (b.route_nameb=c.route_namec)";
		
		
		return DB::Select($sql);
    }
	
	public function getRoutesExport($type, $route_name, $company_name, $ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='".ussd_code."'";
		}
		
		switch($type)
		{
			case 'Total':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and route_name='$route_name' and company_name='$company_name'  $where";
			break;
			case 'Day':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and route_name='$route_name' and company_name='$company_name'  $where and DAY(created_at) = DAY(CURRENT_DATE())";
			break;
			case 'Month':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and route_name='$route_name' and company_name='$company_name'  $where and MONTH(created_at) = MONTH(CURRENT_DATE())";
			break;
			case 'Year':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and route_name='$route_name' and company_name='$company_name' $where and YEAR(created_at) = YEAR(CURRENT_DATE())";
			break;
		}
		
		return DB::Select($sql);
    }

	public function getUSSDMenuGeneralItemsReportCount($ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='".$ussd_code."'";
		}
		
		$sql = "Select actiont Main_USSD_Menu_Items, count as Total, countDay as Day, countMonth as Month, countYear as Year
from
(SELECT action as actiont, count(msisdn) as count FROM customer_logs where channel='USSD' $where and is_main=1 and action<>'' group by actiont) t LEFT JOIN
(SELECT action as actiona, count(msisdn) as countDay FROM customer_logs where channel='USSD' $where and is_main=1 and action<>'' group by actiona, DAY(created_at)) a on (t.actiont=a.actiona) LEFT JOIN
(SELECT action as actionb, count(msisdn) as countMonth FROM customer_logs where channel='USSD' $where and is_main=1 and action<>'' group by actionb, MONTH(created_at)) b on (a.actiona=b.actionb) LEFT JOIN
(SELECT action as actionc, count(msisdn) as countYear FROM customer_logs where channel='USSD' $where and is_main=1 and action<>'' group by actionc, YEAR(created_at)) c on (b.actionb=c.actionc)";
		
		
		return DB::Select($sql);
    }
	
	public function getUSSDMenuGeneralItemsExport($type, $action, $ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='".ussd_code."'";
		}
		
		switch($type)
		{
			case 'Total':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and action='$action'  $where";
			break;
			case 'Day':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and action='$action'  $where and DAY(created_at) = DAY(CURRENT_DATE())";
			break;
			case 'Month':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and action='$action'  $where and MONTH(created_at) = MONTH(CURRENT_DATE())";
			break;
			case 'Year':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and action='$action' $where and YEAR(created_at) = YEAR(CURRENT_DATE())";
			break;
		}
		
		return DB::Select($sql);
    }
	
	public function getUSSDMenuCompanyItemsReportCount($ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='".$ussd_code."'";
		}
		
		$sql = "Select actiont Company_USSD_Menu_Items, count as Total, countDay as Day, countMonth as Month, countYear as Year
from
(SELECT CONCAT(company_name,'-',action) as actiont, count(msisdn) as count FROM customer_logs where channel='USSD' $where and is_main=0 and action<>'' and company_name<>'' group by actiont) t LEFT JOIN
(SELECT CONCAT(company_name,'-',action) as actiona, count(msisdn) as countDay FROM customer_logs where channel='USSD' $where and is_main=0 and action<>'' and company_name<>'' group by actiona, DAY(created_at)) a on (t.actiont=a.actiona) LEFT JOIN
(SELECT CONCAT(company_name,'-',action) as actionb, count(msisdn) as countMonth FROM customer_logs where channel='USSD' $where and is_main=0 and action<>'' and company_name<>'' group by actionb, MONTH(created_at)) b on (a.actiona=b.actionb) LEFT JOIN
(SELECT CONCAT(company_name,'-',action) as actionc, count(msisdn) as countYear FROM customer_logs where channel='USSD' $where and is_main=0 and action<>'' and company_name<>'' group by actionc, YEAR(created_at)) c on (b.actionb=c.actionc)";
		
		
		return DB::Select($sql);
    }
	
	public function getUSSDMenuCompanyItemsExport($type, $company_name, $action, $ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='".ussd_code."'";
		}
		
		switch($type)
		{
			case 'Total':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and action='$action' and company_name='$company_name' $where";
			break;
			case 'Day':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and action='$action' and company_name='$company_name' $where and DAY(created_at) = DAY(CURRENT_DATE())";
			break;
			case 'Month':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and action='$action' and company_name='$company_name' $where and MONTH(created_at) = MONTH(CURRENT_DATE())";
			break;
			case 'Year':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and action='$action' and company_name='$company_name' $where and YEAR(created_at) = YEAR(CURRENT_DATE())";
			break;
		}
		
		return DB::Select($sql);
    }
}

