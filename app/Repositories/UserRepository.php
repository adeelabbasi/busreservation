<?php

namespace App\Repositories;

use App\Models\User;
use App\Models\Userlog;
use App\Models\Admin;
use Illuminate\Support\Facades\Input;
use App\Repositories\ImageRepository;
use DB;
use Illuminate\Support\Facades\Hash;
use Auth; 
//Importing laravel-permission models
use App\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Mail\AccountMail;
use Illuminate\Support\Facades\Mail;


class UserRepository {
	
	/**
	 * @var App\Models\User
	 */
	protected $db_user;
	protected $db_user_log;
	protected $db_admin;
	protected $imageRps;
	
	protected $uploadFolder = 'media/avatar/';
	
    public function __construct(User $db_user, Userlog $db_user_log, Admin $db_admin, ImageRepository $imageRps) 
    {
        $this->db_user = $db_user;
		$this->db_admin = $db_admin;
		$this->imageRps = $imageRps;
		$this->db_user_log = $db_user_log;
    }
	
	/*
	Company user
	*/
	public function addUser($inputs)
    {
        $db_user = $this->storeUser(new $this->db_user ,  $inputs);
        return $db_user;
    }
	
	public function updateUser($inputs, $id)
	{
		$db_user = $this->db_user->findOrFail($id);
		$db_user = $this->storeUser($db_user, $inputs, $id);
		return $db_user;
	}
	
	public function updateUserPassword($inputs, $id)
	{
		$db_user = $this->db_user->findOrFail($id);
		$db_user->password = Hash::make($inputs['newpassword']);
		$db_user->username = $inputs['username'];
		$user_id = $db_user->save();
		return $user_id;
	}
	
	public function deleteUser($id)
    {
		$db_user = $this->db_user->findOrFail($id);
		$this->imageRps->delete($db_user->avatar , $this->uploadFolder);
        $db_user->delete();
        return true;
    }

	function storeUser($db_user , $inputs, $id = null)
	{
		if ($id) 
		{
			$db_user->id = $id;
			if(isset($inputs['avatar']))
			{
				$this->imageRps->delete($db_admin->avatar, $this->uploadFolder);
				$db_user->avatar = $this->imageRps->upload($inputs['avatar'], $id, $this->uploadFolder, '300', '300');
			}
		}
		else
		{
			$db_user->avatar = "";
		}
		
		$db_user->company_id = $inputs['company_id'];
		$db_user->name = $inputs['name'];
		$db_user->username = $inputs['username'];
		$db_user->email = $inputs['email'];
		$db_user->type = '2';
		
		//Set status
		if(isset($inputs['status'])=="on")
			$db_user->status = 1;
		else
			$db_user->status = 0;
			
		if(isset($inputs['password']))
		{
			$db_user->password = Hash::make($inputs['password']);
			$db_user->cpassword = $inputs['password'];
		}
		$db_user->save();
		
		if($id == null)
		{
			$db_user->assignRole($inputs['role_id']);
			$info_Company = $db_user->Company()->First();
			$info_Company->username = $inputs['username'];
			$info_Company->password = $inputs['password'];
			$info_Company->ussd_code = $info_Company->Company_ussd()->First()->code;
			Mail::to($db_user)->send(new AccountMail($info_Company));
		}
		else
		{
			if(isset($inputs['role_id']))
				$db_user->roles()->sync($inputs['role_id']);
		}
			
		return $db_user;
	}
	
	public function getUser($id = null)
    {
		if($id==null)
		{
			$info_User = $this->db_user->select('id', 'company_id', 'name', 'email','username', 'avatar', 'type', 'password', 'cpassword', 'status', 'created_at', 'updated_at')->orderBy('created_at', 'DESC');
		}
		else
		{
			$info_User = $this->db_user->select('id', 'company_id', 'name', 'email','username', 'avatar', 'type', 'password', 'cpassword', 'status', 'created_at', 'updated_at')->findOrFail($id);
		}
        return $info_User;
    }
	
	public function getCompanyUser($company_id)
    {
		$info_User = $this->db_user->select('id', 'company_id', 'name', 'email','username', 'avatar', 'type', 'password', 'cpassword', 'status', 'created_at', 'updated_at')->where('company_id', '=', $company_id);
        return $info_User;
    }
	
	
	/*
	Admin user
	*/
	public function addAdmin($inputs)
    {
        $db_admin = $this->storeAdmin(new $this->db_admin ,  $inputs);
        return $db_admin;
    }
	
	public function updateAdmin($inputs, $id)
	{
		$db_admin = $this->db_admin->findOrFail($id);
		$db_admin = $this->storeAdmin($db_admin, $inputs, $id);
		return $db_admin;
	}
	
	public function updateAdminPassword($inputs, $id)
	{
		$db_admin = $this->db_admin->findOrFail($id);
		$db_admin->password = Hash::make($inputs['newpassword']);
		$db_admin->adminname = $inputs['adminname'];
		$admin_id = $db_admin->save();
		return $admin_id;
	}
	
	public function deleteAdmin($id)
    {
		$db_admin = $this->db_admin->findOrFail($id);
		$this->imageRps->delete($db_admin->avatar , $this->uploadFolder);
        $db_admin->delete();
        return true;
    }

	function storeAdmin($db_admin , $inputs, $id = null)
	{
		if ($id) 
		{
			$db_admin->id = $id;
			if(isset($inputs['avatar']))
			{
				$this->imageRps->delete($db_admin->avatar, $this->uploadFolder);
				$db_admin->avatar = $this->imageRps->upload($inputs['avatar'], $id, $this->uploadFolder, '300', '300');
			}
		}
		else
		{
			$db_admin->avatar = "";
		}
		
		$db_admin->name = $inputs['name'];
		$db_admin->username = $inputs['username'];
		$db_admin->email = $inputs['email'];
		
		//Set status
		if(isset($inputs['status'])=="on")
			$db_admin->status = 1;
		else
			$db_admin->status = 0;
			
		if(isset($inputs['password']))
		{
			$db_admin->password = Hash::make($inputs['password']);
			$db_admin->cpassword = $inputs['password'];
		}
		$db_admin->save();
			
		if($id == null)
		{
			$db_admin->assignRole($inputs['role_id']);
			Mail::to($db_admin)->send(new AccountMail());
		}
		else
		{
			if(isset($inputs['role_id']))
				$db_admin->roles()->sync($inputs['role_id']);
		}	
		return $db_admin;
	}
	
	public function getAdmin($id = null)
    {
		if($id==null)
		{
			$info_Admin = $this->db_admin->select('id', DB::RAW('"" as company_id'), 'name', 'email','username', 'avatar', DB::RAW('"" as type'), 'password', 'cpassword', 'status', 'created_at', 'updated_at')->orderBy('created_at', 'DESC');
		}
		else
		{
			$info_Admin = $this->db_admin->select('id', DB::RAW('"" as company_id'), 'name', 'email','username', 'avatar', DB::RAW('"" as type'), 'password', 'cpassword', 'status', 'created_at', 'updated_at')->findOrFail($id);
		}
        return $info_Admin;
    }
	
	public function getAdminRole()
    {
		return Role::where('guard_name','admin');
    }
	
	public function getUserRole($company_id)
    {
		return Role::Where('guard_name','web')->Where('company_id',$company_id);
    }
	
	public function getUserLog($type)
    {
		if($type==1)
		{
			$info_Userlog = $this->db_user_log->select('id', 'user_id', 'admin_id', 'action', 'created_at', 'updated_at')->orderBy('created_at', 'DESC')->get();
		}
		else
		{
			$info_Userlog = $this->db_user_log->select('id', 'user_id', 'admin_id', 'action', 'created_at', 'updated_at')->orderBy('created_at', 'DESC')->WhereNull('admin_id')->get();
		}
        return $info_Userlog;
    }
	
	public function getCompanyUserLog($UserIds)
    {
		$info_Userlog = $this->db_user_log->select('id', 'user_id', 'admin_id', 'action', 'created_at', 'updated_at')->orderBy('created_at', 'DESC')->WhereNull('admin_id')->whereIn('user_id', $UserIds)->orderBy('created_at', 'DESC')->get();
        return $info_Userlog;
    }
}