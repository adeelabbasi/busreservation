<?php

namespace App\Repositories;

use App\Models\Cservice;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Hash;

class CserviceRepository {
	
	/**
	 * @var App\Models\Cservice
	 */
	protected $db_cservice;
		
    public function __construct(Cservice $db_cservice) 
    {
        $this->db_cservice = $db_cservice;
    }
	
	public function addCservice($inputs)
    {
        $db_cservice = $this->storeCservice(new $this->db_cservice ,  $inputs);
        return $db_cservice;
    }
	
	public function updateCservice($inputs, $id)
	{
		$db_cservice = $this->db_cservice->findOrFail($id);
		$answer_id = $this->storeCservice($db_cservice, $inputs, $id);
		return $answer_id;
	}
	
	public function deleteCservice($id)
    {
		$db_cservice = $this->db_cservice->findOrFail($id);
        $db_cservice->delete();
        return true;
    }

	function storeCservice($db_cservice , $inputs, $id = null)
	{	
		$db_cservice->company_id = $inputs['company_id'];
		$db_cservice->name = $inputs['name'];
		$db_cservice->detail = $inputs['detail'];
		//Set status
		if(isset($inputs['status'])=="on")
			$db_cservice->status = 1;
		else
			$db_cservice->status = 0;
		$db_cservice->save();
		return $db_cservice;
	}
	
	public function getCservice($id = null)
    {
		if($id==null)
		{
			$info_Cservice = $this->db_cservice->select('id', 'company_id', 'name', 'detail', 'status', 'created_at', 'updated_at')->orderBy('created_at', 'DESC')->get();
		}
		else
		{
			$info_Cservice = $this->db_cservice->select('id', 'company_id', 'name', 'detail', 'status', 'created_at', 'updated_at')->findOrFail($id);
		}
        return $info_Cservice;
    }
	
	public function getCompanyCservice($company_id)
    {
		$info_Cservice = $this->db_cservice->select('id', 'company_id', 'name', 'detail', 'status', 'created_at', 'updated_at')->where('company_id', '=', $company_id);
        return $info_Cservice;
    }
}

