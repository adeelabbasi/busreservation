<?php

namespace App\Repositories;

use App\Models\Terminal;
use App\Models\Route_detail;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Hash;

class TerminalRepository {
	
	/**
	 * @var App\Models\Terminal
	 */
	protected $db_terminal;
	protected $db_route_detail;
	
    public function __construct(Terminal $db_terminal, Route_detail $db_route_detail) 
    {
        $this->db_terminal = $db_terminal;
		$this->db_route_detail = $db_route_detail;
    }
	
	public function addTerminal($inputs)
    {
        $db_terminal = $this->storeTerminal(new $this->db_terminal ,  $inputs);
        return $db_terminal;
    }
	
	public function updateTerminal($inputs, $id)
	{
		$db_terminal = $this->db_terminal->findOrFail($id);
		$answer_id = $this->storeTerminal($db_terminal, $inputs, $id);
		return $answer_id;
	}
	
	public function deleteTerminal($id)
    {
		if($this->db_route_detail->where(['terminal_id' => $id])->Get()->Count()==0)
		{
			$db_terminal = $this->db_terminal->findOrFail($id);
			$db_terminal->delete();
			return true;
		}
		else
		{
			return false; 
		}
    }
	
	public function updateSequence($id, $sequence)
    {
		$db_terminal = $this->db_terminal->findOrFail($id);
        $db_terminal->sequence = $sequence;
		$db_terminal->save();
        return true;
    }
	
	function storeTerminal($db_terminal , $inputs, $id = null)
	{	
		$db_terminal->company_id = $inputs['company_id'];
		$db_terminal->name = $inputs['name'];
		$db_terminal->short_name = $inputs['short_name'];
		$db_terminal->lga_id = $inputs['lga_id'];
		$db_terminal->address = $inputs['address'];
		$db_terminal->email = $inputs['email'];
		$db_terminal->phone_number = $inputs['phone_number'];
		//Set status
		if(isset($inputs['status'])=="on")
			$db_terminal->status = 1;
		else
			$db_terminal->status = 0;
		$db_terminal->save();
		return $db_terminal;
	}
	
	public function getTerminal($id = null)
    {
		if($id==null)
		{
			$info_Terminal = $this->db_terminal->select('id', 'company_id', 'lga_id', 'name', 'short_name', 'address', 'email', 'phone_number', 'status' , 'sequence', 'created_at', 'updated_at')->orderBy('created_at', 'DESC')->get();
		}
		else
		{
			$info_Terminal = $this->db_terminal->select('id', 'company_id', 'lga_id', 'name', 'short_name', 'address', 'email', 'phone_number', 'status', 'sequence', 'created_at', 'updated_at')->findOrFail($id);
		}
        return $info_Terminal;
    }
	
	public function getCompanyTerminal($company_id)
    {
		$info_Terminal = $this->db_terminal->select('id', 'company_id', 'lga_id', 'name', 'short_name', 'address', 'email', 'phone_number', 'status', 'sequence', 'created_at', 'updated_at')->where('company_id', '=', $company_id);
        return $info_Terminal;
    }
	
	public function getCompanyCityTerminal($company_id, $lga_ids)
    {
		$info_Terminal = $this->db_terminal->select('id', 'company_id', 'lga_id', 'name', 'short_name', 'address', 'email', 'phone_number', 'status', 'sequence', 'created_at', 'updated_at')->where('company_id', '=', $company_id)->whereIn('lga_id',$lga_ids);
        return $info_Terminal;
    }
}

