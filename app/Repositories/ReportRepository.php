<?php

namespace App\Repositories;

use DB;
use Illuminate\Support\Facades\Hash;

class ReportRepository {
		
    public function __construct() 
    {
		
    }
	
	public function getCompany()
	{
		$Result  = DB::Select("select count(*) as counts from companies");
		return $Result[0]->counts;
	}
	
	public function getCountry()
	{
		$Result  = DB::Select("select count(*) as counts from countries");
		return $Result[0]->counts;
	}
	
	public function getCity()
	{
		$Result  = DB::Select("select count(*) as counts from cities");
		return $Result[0]->counts;
	}
	
	public function getLga()
	{
		$Result  = DB::Select("select count(*) as counts from lgas");
		return $Result[0]->counts;
	}
	
	public function getTerminalReport($company_id = null)
    {
		if($company_id==null)
		{
			$where = "";
		}
		else
		{
			$where = "and companies.id='".$company_id."'";
		}
		
		$sql = "select companies.name as name, count(*) as counts from companies inner join terminals on ( companies.id = terminals.company_id) where 1=1 $where group by companies.name";
		
		return DB::Select($sql);
    }
	
	public function getFleetReport($company_id = null)
    {
		if($company_id==null)
		{
			$where = "";
		}
		else
		{
			$where = "and companies.id='".$company_id."'";
		}
		
		$sql = "select companies.name as name, count(*) as counts from companies inner join buses on ( companies.id = buses.company_id) where 1=1 $where group by companies.name";
		
		return DB::Select($sql);
    }
	
	public function getServiceReport($company_id = null)
    {
		if($company_id==null)
		{
			$where = "";
		}
		else
		{
			$where = "and companies.id='".$company_id."'";
		}
		
		$sql = "select companies.name as name, count(*) as counts from companies inner join services on ( companies.id = services.company_id) where 1=1 $where group by companies.name";
		
		return DB::Select($sql);
    }
	
	public function getRouteReport($company_id = null)
    {
		if($company_id==null)
		{
			$where = "";
		}
		else
		{
			$where = "and companies.id='".$company_id."'";
		}
		
		$sql = "select companies.name as name, count(*) as counts from companies inner join routes on ( companies.id = routes.company_id) where 1=1 $where group by companies.name";
		
		return DB::Select($sql);
    }
	
	public function getScheduleReport($company_id = null)
    {
if($company_id==null)
		{
			$where = "";
		}
		else
		{
			$where = "and companies.id='".$company_id."'";
		}			
		
		$sql = "select companies.name as name, count(*) as counts from companies inner join schedules on ( companies.id = schedules.company_id) where 1=1 $where group by companies.name";
		
		return DB::Select($sql);
    }
	
	public function getUserReport($company_id = null)
    {
		if($company_id==null)
		{
			$where = "";
		}
		else
		{
			$where = "and companies.id='".$company_id."'";
		}
		
		$sql = "select companies.name as name, count(*) as counts from companies inner join users on ( companies.id = users.company_id) where 1=1 $where group by companies.name";
		
		return DB::Select($sql);
    }
	
	public function getScheduleByDayReport($company_id = null)
    {
		if($company_id==null)
		{
			$where = "";
		}
		else
		{
			$where = "and companies.id='".$company_id."'";
		}
		
		$sql = "select companies.name as name, count(*) as counts, sum(schedules.monday) as monday, sum(schedules.tuesday) as tuesday, sum(schedules.wednesday) as wednesday, sum(schedules.thursday) as thursday, sum(schedules.friday) as friday, sum(schedules.saturday) as saturday, sum(schedules.sunday) as sunday from companies inner join schedules on ( companies.id = schedules.company_id) where 1=1 $where group by companies.name";
		
		return DB::Select($sql);
    }
	///////////
	//USSD
	///////////
	
	/*
	Customers Report
	*/
	public function getCustomersReport($type, $ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='*".$ussd_code."'";
		}
		
		switch($type)
		{
			case 'Total':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y-%m-%d') DataVar, count(msisdn) Count FROM customer_logs where channel='USSD' $where group by DAY(created_at)";
			break;
			case 'Day':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y-%m-%d') DataVar, count(msisdn) Count FROM customer_logs where channel='USSD' $where AND MONTH(created_at) = MONTH(CURRENT_DATE()) group by DAY(created_at)";
			break;
			case 'Month':
				$sql = "SELECT DATE_FORMAT(created_at, '%M') DataVar, count(msisdn) Count FROM customer_logs where channel='USSD' $where AND YEAR(created_at) = YEAR(CURRENT_DATE()) group by MONTH(created_at)";
			break;
			case 'Year':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y') DataVar, count(msisdn) Count FROM customer_logs where channel='USSD' $where group by YEAR(created_at)";
			break;
		}
		
		return DB::Select($sql);
    }
	
	/*
	Customers Report Export
	*/
	public function getCustomersReportExport($type, $ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='*".$ussd_code."'";
		}
		
		switch($type)
		{
			case 'Total':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' $where";
			break;
			case 'Day':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' $where and MONTH(created_at) = MONTH(CURRENT_DATE())";
			break;
			case 'Month':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' $where and YEAR(created_at) = YEAR(CURRENT_DATE())";
			break;
			case 'Year':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' $where";
			break;
		}
		
		return DB::Select($sql);
    }
	
	/*
	Unique Customers Report
	*/
	public function getUniqueCustomersReport($type, $ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='*".$ussd_code."'";
		}
		
		switch($type)
		{
			case 'Total':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y-%m-%d') DataVar, count(distinct msisdn) Count FROM customer_logs where channel='USSD' $where group by DAY(created_at)";
			break;
			case 'Day':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y-%m-%d') DataVar, count(distinct msisdn) Count FROM customer_logs where channel='USSD' $where AND MONTH(created_at) = MONTH(CURRENT_DATE()) group by DAY(created_at)";
			break;
			case 'Month':
				$sql = "SELECT DATE_FORMAT(created_at, '%M') DataVar, count(distinct msisdn) Count FROM customer_logs where channel='USSD' $where AND YEAR(created_at) = YEAR(CURRENT_DATE()) group by MONTH(created_at)";
			break;
			case 'Year':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y') DataVar, count(distinct msisdn) Count FROM customer_logs where channel='USSD' $where group by YEAR(created_at)";
			break;
		}
		
		return DB::Select($sql);
    }
	
	/*
	Unique Customers Report Export
	*/
	public function getUniqueCustomersReportExport($type, $ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='*".$ussd_code."'";
		}
		
		switch($type)
		{
			case 'Total':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' $where";
			break;
			case 'Day':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' $where and MONTH(created_at) = MONTH(CURRENT_DATE())";
			break;
			case 'Month':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' $where and YEAR(created_at) = YEAR(CURRENT_DATE())";
			break;
			case 'Year':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' $where";
			break;
		}
		
		return DB::Select($sql);
    }
	
	/*
	Company Requests Report
	*/
	public function getCompanyRequestsReport($type, $ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='*".$ussd_code."'";
		}
		
		switch($type)
		{
			case 'Total':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y-%m-%d') DataVar, company_name, ussd_code, count(msisdn) Count FROM customer_logs where channel='USSD' and company_name<>'' and is_main=0 $where group by company_name, ussd_code, DAY(created_at) order by company_name, ussd_code, DAY(created_at)";
			break;
			case 'Day':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y-%m-%d') DataVar, company_name, ussd_code, count(msisdn) Count FROM customer_logs where channel='USSD' and company_name<>'' and is_main=0 $where AND MONTH(created_at) = MONTH(CURRENT_DATE()) group by  company_name, ussd_code, DAY(created_at) order by company_name, ussd_code, DAY(created_at)";
			break;
			case 'Month':
				$sql = "SELECT DATE_FORMAT(created_at, '%M') DataVar, company_name, ussd_code, count(msisdn) Count FROM customer_logs where channel='USSD' and company_name<>'' and is_main=0 $where AND YEAR(created_at) = YEAR(CURRENT_DATE()) group by company_name, ussd_code, MONTH(created_at) order by company_name, ussd_code, MONTH(created_at)";
			break;
			case 'Year':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y') DataVar, company_name, ussd_code, count(msisdn) Count FROM customer_logs where channel='USSD' and company_name<>'' and is_main=0 $where group by company_name, ussd_code, YEAR(created_at) order by company_name, ussd_code, YEAR(created_at)";
			break;
		}
		
		return DB::Select($sql);
    }
	
	/*
	Company Requests Report Export
	*/
	public function getCompanyRequestsReportExport($type, $ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='*".$ussd_code."'";
		}
		
		switch($type)
		{
			case 'Total':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and company_name<>'' and is_main=0 $where";
			break;
			case 'Day':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and company_name<>'' and is_main=0 $where and MONTH(created_at) = MONTH(CURRENT_DATE())";
			break;
			case 'Month':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and company_name<>'' and is_main=0 $where and YEAR(created_at) = YEAR(CURRENT_DATE())";
			break;
			case 'Year':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and company_name<>'' and is_main=0 $where";
			break;
		}
		
		return DB::Select($sql);
    }
	
	/*
	Short code Strings Report
	*/
	public function getShortcodeStringsReport($type, $ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='*".$ussd_code."'";
		}
		
		switch($type)
		{
			case 'Total':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y-%m-%d') DataVar, ussd_code, count(msisdn) Count FROM customer_logs where channel='USSD' and ussd_code<>'' $where group by ussd_code, DAY(created_at) order by ussd_code, DAY(created_at)";
			break;
			case 'Day':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y-%m-%d') DataVar, ussd_code, count(msisdn) Count FROM customer_logs where channel='USSD' and ussd_code<>'' $where AND MONTH(created_at) = MONTH(CURRENT_DATE()) group by  ussd_code, DAY(created_at) order by ussd_code, DAY(created_at)";
			break;
			case 'Month':
				$sql = "SELECT DATE_FORMAT(created_at, '%M') DataVar, ussd_code, count(msisdn) Count FROM customer_logs where channel='USSD' and ussd_code<>'' $where AND MONTH(created_at) = MONTH(CURRENT_DATE()) group by ussd_code, MONTH(created_at) order by ussd_code, MONTH(created_at)";
			break;
			case 'Year':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y') DataVar, ussd_code, count(msisdn) Count FROM customer_logs where channel='USSD' and ussd_code<>'' $where group by ussd_code, YEAR(created_at) order by ussd_code, YEAR(created_at)";
			break;
		}
		
		return DB::Select($sql);
    }
	/*
	Short code Strings Report Export
	*/
	public function getShortcodeStringsReportExport($type, $ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='*".$ussd_code."'";
		}
		
		switch($type)
		{
			case 'Total':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and ussd_code<>'' $where";
			break;
			case 'Day':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and ussd_code<>'' $where and MONTH(created_at) = MONTH(CURRENT_DATE())";
			break;
			case 'Month':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and ussd_code<>'' $where and YEAR(created_at) = YEAR(CURRENT_DATE())";
			break;
			case 'Year':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and ussd_code<>'' $where";
			break;
		}
		
		return DB::Select($sql);
    }
	/*
	Routes
	*/
	public function getRoutesReport($type, $ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='*".$ussd_code."'";
		}
		
		switch($type)
		{
			case 'Total':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y-%m-%d') DataVar, route_name, count(msisdn) Count FROM customer_logs where channel='USSD' and route_name<>'' $where group by route_name, DAY(created_at) order by route_name, DAY(created_at)";
			break;
			case 'Day':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y-%m-%d') DataVar, route_name, count(msisdn) Count FROM customer_logs where channel='USSD' and route_name<>'' $where AND MONTH(created_at) = MONTH(CURRENT_DATE()) group by  route_name, DAY(created_at) order by route_name, DAY(created_at)";
			break;
			case 'Month':
				$sql = "SELECT DATE_FORMAT(created_at, '%M') DataVar, route_name, count(msisdn) Count FROM customer_logs where channel='USSD' and route_name<>'' $where AND YEAR(created_at) = YEAR(CURRENT_DATE()) group by route_name, MONTH(created_at) order by route_name, MONTH(created_at)";
			break;
			case 'Year':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y') DataVar, route_name, count(msisdn) Count FROM customer_logs where channel='USSD' and route_name<>'' $where group by route_name, YEAR(created_at) order by route_name, DAY(created_at)";
			break;
		}
		
		return DB::Select($sql);
    }
	/*
	Routes Export
	*/
	public function getRoutesExport($type, $ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='".ussd_code."'";
		}
		
		switch($type)
		{
			case 'Total':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and route_name<>'' $where";
			break;
			case 'Day':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and route_name<>'' $where and MONTH(created_at) = MONTH(CURRENT_DATE())";
			break;
			case 'Month':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and route_name<>'' $where and YEAR(created_at) = YEAR(CURRENT_DATE())";
			break;
			case 'Year':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and route_name<>'' $where";
			break;
		}
		
		return DB::Select($sql);
    }
	
	/*
	Schedules
	*/
	public function getSchedulesReport($type, $ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='*".$ussd_code."'";
		}
		
		switch($type)
		{
			case 'Total':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y-%m-%d') DataVar, CONCAT(company_name,'(',schedule_name,')') as schedule_name, count(msisdn) Count FROM customer_logs where channel='USSD' and schedule_name<>'' $where group by company_name, schedule_name, DAY(created_at) order by company_name, schedule_name, DAY(created_at)";
			break;
			case 'Day':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y-%m-%d') DataVar, CONCAT(company_name,'(',schedule_name,')') as schedule_name, count(msisdn) Count FROM customer_logs where channel='USSD' and schedule_name<>'' $where AND MONTH(created_at) = MONTH(CURRENT_DATE()) group by  company_name, schedule_name, DAY(created_at) order by company_name, schedule_name, DAY(created_at)";
			break;
			case 'Month':
				$sql = "SELECT DATE_FORMAT(created_at, '%M') DataVar, CONCAT(company_name,'(',schedule_name,')') as schedule_name, count(msisdn) Count FROM customer_logs where channel='USSD' and schedule_name<>'' $where AND YEAR(created_at) = YEAR(CURRENT_DATE()) group by company_name, schedule_name, MONTH(created_at) order by company_name, schedule_name, MONTH(created_at)";
			break;
			case 'Year':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y') DataVar, CONCAT(company_name,'(',schedule_name,')') as schedule_name, count(msisdn) Count FROM customer_logs where channel='USSD' and schedule_name<>'' $where group by company_name, schedule_name, YEAR(created_at) order by company_name, schedule_name, YEAR(created_at)";
			break;
		}
		
		return DB::Select($sql);
    }
	/*
	Schedules Export
	*/
	public function getSchedulesExport($type, $ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='".ussd_code."'";
		}
		
		switch($type)
		{
			case 'Total':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and schedule_name<>'' $where";
			break;
			case 'Day':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and schedule_name<>'' $where and MONTH(created_at) = MONTH(CURRENT_DATE())";
			break;
			case 'Month':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and schedule_name<>'' $where and YEAR(created_at) = YEAR(CURRENT_DATE())";
			break;
			case 'Year':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and schedule_name<>'' $where";
			break;
		}
		
		return DB::Select($sql);
    }
	
	/*
	USSD Menu General Items Report
	*/
	public function getUSSDMenuGeneralItemsReport($type, $ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='*".$ussd_code."'";
		}
		
		switch($type)
		{
			case 'Total':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y-%m-%d') DataVar, action, count(msisdn) Count FROM customer_logs where channel='USSD' and action<>'' and is_main=1 $where group by action, DAY(created_at) order by action, DAY(created_at)";
			break;
			case 'Day':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y-%m-%d') DataVar, action, count(msisdn) Count FROM customer_logs where channel='USSD' and action<>'' and is_main=1 $where AND MONTH(created_at) = MONTH(CURRENT_DATE()) group by  action, DAY(created_at) order by action, DAY(created_at)";
			break;
			case 'Month':
				$sql = "SELECT DATE_FORMAT(created_at, '%M') DataVar, action, count(msisdn) Count FROM customer_logs where channel='USSD' and action<>'' and is_main=1 $where AND YEAR(created_at) = YEAR(CURRENT_DATE()) group by action, MONTH(created_at) order by action, MONTH(created_at)";
			break;
			case 'Year':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y') DataVar, action, count(msisdn) Count FROM customer_logs where channel='USSD' and action<>'' and is_main=1 $where group by action, YEAR(created_at) order by action, YEAR(created_at)";
			break;
		}
		

		return DB::Select($sql);
    }
	
	/*
	USSD Menu General Items Report Export
	*/
	public function getUSSDMenuGeneralItemsExport($type, $ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='".ussd_code."'";
		}
		
		switch($type)
		{
			case 'Total':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and action<>'' and is_main=1 $where";
			break;
			case 'Day':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and action<>'' and is_main=1 $where and MONTH(created_at) = MONTH(CURRENT_DATE())";
			break;
			case 'Month':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and action<>'' and is_main=1 $where and YEAR(created_at) = YEAR(CURRENT_DATE())";
			break;
			case 'Year':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and action<>'' and is_main=1 $where";
			break;
		}
		
		return DB::Select($sql);
    }
	
	/*
	USSD Menu Company Items Report
	*/
	public function getUSSDMenuCompanyItemsReport($type, $ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='*".$ussd_code."'";
		}
		
		switch($type)
		{
			case 'Total':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y-%m-%d') DataVar, CONCAT(company_name,'(',action,')') as action, count(msisdn) Count FROM customer_logs where channel='USSD' and action<>'' and is_main=0 $where group by action, DAY(created_at) order by action, DAY(created_at)";
			break;
			case 'Day':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y-%m-%d') DataVar, CONCAT(company_name,'(',action,')') as action, count(msisdn) Count FROM customer_logs where channel='USSD' and action<>'' and is_main=0 $where AND MONTH(created_at) = MONTH(CURRENT_DATE()) group by action, DAY(created_at) order by action, DAY(created_at)";
			break;
			case 'Month':
				$sql = "SELECT DATE_FORMAT(created_at, '%M') DataVar, CONCAT(company_name,'(',action,')') as action, count(msisdn) Count FROM customer_logs where channel='USSD' and action<>'' and is_main=0 $where AND YEAR(created_at) = YEAR(CURRENT_DATE()) group by action, MONTH(created_at) order by action, MONTH(created_at)";
			break;
			case 'Year':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y') DataVar, CONCAT(company_name,'(',action,')') as action, count(msisdn) Count FROM customer_logs where channel='USSD' and action<>'' and is_main=0 $where group by action, YEAR(created_at) order by action, YEAR(created_at)";
			break;
		}
		
		return DB::Select($sql);
    }
	
	/*
	USSD Menu Company Items Report Export
	*/
	public function getUSSDMenuCompanyItemsExport($type, $ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='".ussd_code."'";
		}
		
		switch($type)
		{
			case 'Total':
				echo $sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and action<>'' and is_main=0 $where";
			break;
			case 'Day':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and action<>'' and is_main=0 $where and MONTH(created_at) = MONTH(CURRENT_DATE())";
			break;
			case 'Month':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and action<>'' and is_main=0 $where and YEAR(created_at) = YEAR(CURRENT_DATE())";
			break;
			case 'Year':
				$sql = "SELECT distinct msisdn FROM customer_logs where channel='USSD' and action<>'' and is_main=0 $where";
			break;
		}
		
		return DB::Select($sql);
    }
	
	/*
	Subscriptions
	*/
	public function getSubscriptionsReport($type, $ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='*".$ussd_code."'";
		}
		
		switch($type)
		{
			case 'Total':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y-%m-%d') DataVar, count(msisdn) Count FROM subscriptions where 1=1 $where group by DAY(created_at) order by DAY(created_at)";
			break;
			case 'Day':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y-%m-%d') DataVar, count(msisdn) Count FROM subscriptions where 1=1 $where AND MONTH(created_at) = MONTH(CURRENT_DATE()) group by  DAY(created_at) order by DAY(created_at)";
			break;
			case 'Month':
				$sql = "SELECT DATE_FORMAT(created_at, '%M') DataVar, count(msisdn) Count FROM subscriptions where 1=1 $where AND YEAR(created_at) = YEAR(CURRENT_DATE()) group by MONTH(created_at) order by MONTH(created_at)";
			break;
			case 'Year':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y') DataVar, count(msisdn) Count FROM subscriptions where 1=1 $where group by YEAR(created_at) order by  YEAR(created_at)";
			break;
		}
		
		return DB::Select($sql);
    }
	
	/*
	Subscriptions Export
	*/
	public function getSubscriptionsExport($type, $ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='".ussd_code."'";
		}
		
		switch($type)
		{
			case 'Total':
				echo $sql = "SELECT distinct msisdn FROM subscriptions where 1=1 $where";
			break;
			case 'Day':
				$sql = "SELECT distinct msisdn FROM subscriptions where 1=1 $where and MONTH(created_at) = MONTH(CURRENT_DATE())";
			break;
			case 'Month':
				$sql = "SELECT distinct msisdn FROM subscriptions where 1=1 $where and YEAR(created_at) = YEAR(CURRENT_DATE())";
			break;
			case 'Year':
				$sql = "SELECT distinct msisdn FROM subscriptions where 1=1 $where";
			break;
		}
		
		return DB::Select($sql);
    }
	
	/*
	Subscriptions Company
	*/
	public function getSubscriptionsCompanyReport($type, $ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='*".$ussd_code."'";
		}
		
		switch($type)
		{
			case 'Total':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y-%m-%d') DataVar, ussd_code, count(msisdn) Count FROM subscriptions where 1=1 $where group by ussd_code, DAY(created_at) order by ussd_code, DAY(created_at)";
			break;
			case 'Day':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y-%m-%d') DataVar, ussd_code, count(msisdn) Count FROM subscriptions where 1=1 $where AND MONTH(created_at) = MONTH(CURRENT_DATE()) group by  ussd_code, DAY(created_at) order by ussd_code, DAY(created_at)";
			break;
			case 'Month':
				$sql = "SELECT DATE_FORMAT(created_at, '%M') DataVar, ussd_code, count(msisdn) Count FROM subscriptions where $where 1=1 AND MONTH(created_at) = MONTH(CURRENT_DATE()) group by ussd_code, MONTH(created_at) order by ussd_code, MONTH(created_at)";
			break;
			case 'Year':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y') DataVar, ussd_code, count(msisdn) Count FROM subscriptions where $where 1=1 group by ussd_code, YEAR(created_at) order by ussd_code, YEAR(created_at)";
			break;
		}
		
		return DB::Select($sql);
    }
	
	/*
	Subscriptions Company Export
	*/
	public function getSubscriptionsCompanyExport($type, $ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='".ussd_code."'";
		}
		
		switch($type)
		{
			case 'Total':
				echo $sql = "SELECT distinct msisdn FROM subscriptions where 1=1 $where";
			break;
			case 'Day':
				$sql = "SELECT distinct msisdn FROM subscriptions where 1=1 $where and MONTH(created_at) = MONTH(CURRENT_DATE())";
			break;
			case 'Month':
				$sql = "SELECT distinct msisdn FROM subscriptions where 1=1 $where and YEAR(created_at) = YEAR(CURRENT_DATE())";
			break;
			case 'Year':
				$sql = "SELECT distinct msisdn FROM subscriptions where 1=1 $where";
			break;
		}
		
		return DB::Select($sql);
    }
	
	/*
	Revenues
	*/
	public function getRevenuesReport($type, $ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='*".$ussd_code."'";
		}
		
		switch($type)
		{
			case 'Total':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y-%m-%d') DataVar, sum(amount) Count FROM subscriptions where 1=1 $where group by DAY(created_at) order by DAY(created_at)";
			break;
			case 'Day':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y-%m-%d') DataVar, sum(amount) Count FROM subscriptions where 1=1 $where AND MONTH(created_at) = MONTH(CURRENT_DATE()) group by  DAY(created_at) order by DAY(created_at)";
			break;
			case 'Month':
				$sql = "SELECT DATE_FORMAT(created_at, '%M') DataVar, sum(amount) Count FROM subscriptions where 1=1 $where AND YEAR(created_at) = YEAR(CURRENT_DATE()) group by MONTH(created_at) order by MONTH(created_at)";
			break;
			case 'Year':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y') DataVar, sum(amount) Count FROM subscriptions where 1=1 $where group by YEAR(created_at) order by  YEAR(created_at)";
			break;
		}
		
		return DB::Select($sql);
    }
	
	/*
	Revenues Export
	*/
	public function getRevenuesExport($type, $ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='".ussd_code."'";
		}
		
		switch($type)
		{
			case 'Total':
				echo $sql = "SELECT distinct msisdn FROM subscriptions where 1=1 $where";
			break;
			case 'Day':
				$sql = "SELECT distinct msisdn FROM subscriptions where 1=1 $where and MONTH(created_at) = MONTH(CURRENT_DATE())";
			break;
			case 'Month':
				$sql = "SELECT distinct msisdn FROM subscriptions where 1=1 $where and YEAR(created_at) = YEAR(CURRENT_DATE())";
			break;
			case 'Year':
				$sql = "SELECT distinct msisdn FROM subscriptions where 1=1 $where";
			break;
		}
		
		return DB::Select($sql);
    }
	
	/*
	Revenues Company
	*/
	public function getRevenuesCompanyReport($type, $ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='*".$ussd_code."'";
		}
		
		switch($type)
		{
			case 'Total':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y-%m-%d') DataVar, ussd_code, sum(amount) Count FROM subscriptions where 1=1 $where group by ussd_code, DAY(created_at) order by ussd_code, DAY(created_at)";
			break;
			case 'Day':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y-%m-%d') DataVar, ussd_code, sum(amount) Count FROM subscriptions where 1=1 $where AND MONTH(created_at) = MONTH(CURRENT_DATE()) group by  ussd_code, DAY(created_at) order by ussd_code, DAY(created_at)";
			break;
			case 'Month':
				$sql = "SELECT DATE_FORMAT(created_at, '%M') DataVar, ussd_code, sum(amount) Count FROM subscriptions where $where 1=1 AND MONTH(created_at) = MONTH(CURRENT_DATE()) group by ussd_code, MONTH(created_at) order by ussd_code, MONTH(created_at)";
			break;
			case 'Year':
				$sql = "SELECT DATE_FORMAT(created_at, '%Y') DataVar, ussd_code, sum(amount) Count FROM subscriptions where 1=1 $where group by ussd_code, YEAR(created_at) order by ussd_code, YEAR(created_at)";
			break;
		}
		
		return DB::Select($sql);
    }
	
	/*
	Revenues Company Export
	*/
	public function getRevenuesCompanyExport($type, $ussd_code = null)
    {
		if($ussd_code==null)
		{
			$where = "";
		}
		else
		{
			$where = "and ussd_code='".ussd_code."'";
		}
		
		switch($type)
		{
			case 'Total':
				echo $sql = "SELECT distinct msisdn FROM subscriptions where 1=1 $where";
			break;
			case 'Day':
				$sql = "SELECT distinct msisdn FROM subscriptions where 1=1 $where and MONTH(created_at) = MONTH(CURRENT_DATE())";
			break;
			case 'Month':
				$sql = "SELECT distinct msisdn FROM subscriptions where 1=1 $where and YEAR(created_at) = YEAR(CURRENT_DATE())";
			break;
			case 'Year':
				$sql = "SELECT distinct msisdn FROM subscriptions where 1=1 $where";
			break;
		}
		
		return DB::Select($sql);
    }
}

