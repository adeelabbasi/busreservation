<?php

namespace App\Repositories;

use App\Models\Fare;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Hash;

class FareRepository {
	
	/**
	 * @var App\Models\Fare
	 */
	protected $db_fare;
		
    public function __construct(Fare $db_fare) 
    {
        $this->db_fare = $db_fare;
    }
	
	public function addFare($inputs)
    {
        $db_fare = $this->storeFare(new $this->db_fare ,  $inputs);
        return $db_fare;
    }
	
	public function updateFare($inputs, $id)
	{
		$db_fare = $this->db_fare->findOrFail($id);
		$answer_id = $this->storeFare($db_fare, $inputs, $id);
		return $answer_id;
	}
	
	public function deleteFare($company_id, $schedule_id, $fare_type_id)
    {
		$db_fares = $this->db_fare->where(['company_id' => $company_id, 'schedule_id' => $schedule_id, 'fare_type_id' => $fare_type_id])->Get();
        
		foreach($db_fares as $db_fare)
		{
			$db_fare->delete();
		}
        return true;
    }

	function storeFare($db_fare , $inputs, $id = null)
	{	
		$db_fare->company_id = $inputs['company_id'];
		$db_fare->schedule_id = $inputs['schedule_id'];
		$db_fare->fare_type_id = $inputs['fare_type_id'];
		$db_fare->route_id = $inputs['route_id'];
		$db_fare->from_terminal_id = $inputs['from_terminal_id'];
		$db_fare->to_terminal_id = $inputs['to_terminal_id'];
		$db_fare->fare = $inputs['fare'];
		$db_fare->fare_return = $inputs['fare_return'];
		
		$db_fare->save();
		return $db_fare;
	}
	
	
	public function getFare($id = null)
    {
		if($id==null)
		{
			$info_Fare = $this->db_fare->select('id', 'company_id', 'schedule_id', 'fare_type_id', 'route_id', 'from_terminal_id', 'to_terminal_id', 'fare', 'fare_return', 'created_at', 'updated_at')->orderBy('created_at', 'DESC')->get();
		}
		else
		{
			$info_Fare = $this->db_fare->select('id', 'company_id', 'schedule_id', 'fare_type_id', 'route_id', 'from_terminal_id', 'to_terminal_id', 'fare', 'fare_return', 'created_at', 'updated_at')->findOrFail($id);
		}
        return $info_Fare;
    }
	
	public function getCompanyScheduleFare($company_id, $schedule_id, $fare_type_id)
    {
		$info_Fare = $this->db_fare->select('id', 'company_id', 'schedule_id', 'fare_type_id', 'route_id', 'from_terminal_id', 'to_terminal_id', 'fare', 'fare_return', 'created_at', 'updated_at')->where('company_id', '=', $company_id)->where('schedule_id', '=', $schedule_id)->where('fare_type_id', '=', $fare_type_id);
        return $info_Fare;
    }
	
	public function getCompanyFare($company_id)
    {
		$info_Fare = $this->db_fare->select('id', 'company_id', 'schedule_id', 'fare_type_id', 'route_id', 'from_terminal_id', 'to_terminal_id', 'fare', 'fare_return', 'created_at', 'updated_at')->where('company_id', '=', $company_id);
        return $info_Fare;
    }
}

