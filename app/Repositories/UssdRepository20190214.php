<?php

namespace App\Repositories;
use App\Models\City;
use App\Models\Company;
use App\Models\Company_ussd;
use App\Models\Schedule;
use App\Models\Fare;
use App\Models\Subscription;
use App\Models\Charging_log;
use App\Models\Customer_log;
use DB;


class UssdRepository20190214 { 
 
	public function Level1($session_msisdn,$session_msg,$session_from)
	{
		//Uncomment echo "Menu 1";
		$output['session_operation'] = "continue";
		$output['session_msg'] = "Welcome to I-Travel\nService\nPlease Select\n1. Bus\n2. Airline\n3. Train\n4. City Bus (BRT)\n5. City Train (Metro)";
		
		$ResultDel = DB::delete("delete from ussd_menu_session where msisdn = '$session_msisdn'");
		$ResultIns = DB::insert("INSERT INTO ussd_menu_session (msisdn, ussd_code, user_level, keyword, parent_id, last_parent_id, g_parent, is_child, record_on) VALUES ('$session_msisdn', '$session_from', '1', '$session_msg', '1', '1', '1', '1', CURRENT_TIMESTAMP)");
		
		return $output; 
	}
	
	public function companyLevel1($session_msisdn,$session_msg,$session_from,$company_name,$company_id)
	{
		//c_parent -> To find that company menu start
  		//c_parent_id -> Current company menu
  		//c_last_parent_id -> Last company parent id
		
		//Uncomment echo "Menu 1";
		$output['session_operation'] = "continue";
		$output['session_msg'] = "Thanks for Choosing\n".$company_name."\nSelect\n1. Schedule\n2. Bookings\n3. Terminals\n4. Contact Us\n5. Promo & Deals"."\n6. Policy Statement\n"."* Back"; 
		
		$info_Company = $this->GetCompanyPageNo($company_id);
		
		$ResultDel = DB::delete("delete from ussd_menu_session where msisdn = '$session_msisdn'");
		$ResultIns = DB::insert("INSERT INTO ussd_menu_session (msisdn, ussd_code, user_level, keyword, parent_id, last_parent_id, g_parent, s_parent, is_child, c_parent_id, company_id, record_on) VALUES ('$session_msisdn', '$session_msg', '4', '$session_msg', '".$info_Company[0]->rownum."', '2', '1', '2', '1', '".$info_Company[0]->rownum."' ,'$company_id' , CURRENT_TIMESTAMP)");
		
		
		return $output;
	}
	
	public function Level2($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword)
	{
		//Uncomment echo "Menu 2";
		$output['session_operation'] = "continue"; 
		$output['session_msg'] = "I-travel Bus Menu \nPlease select \n1. Schedule & Fare \n2. Transporters \n3. Promo & Deals\n4. Infotainment\n* Back";
		
		$userSession->user_level=2;
		$userSession->keyword=$session_msg;
		$userSession->parent_id=$session_msg;
		$userSession->last_parent_id=$session_msg;
		$userSession->g_parent=$session_msg;
		$userSession->is_child=1;
		$userSession->save();
		
		return $output;
	}
	public function Level3($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword)
	{
		//Uncomment echo "Menu 3";
		
		//2nd menu session will use in after 3rd menu
		$userSession->s_parent=$session_msg;
		
		//Change page no on level change, check if level is same else change page no
		if($userSession->user_level != 3)
		{
			$userSession->page_no = 0;
		}
		switch($session_msg)
		{
			case 1: //Schedule & Fare > From city
				
				if($moreKeyword)
				{
					$userSession->page_no = $userSession->page_no+4;
				}
				else
				{
					$userSession->last_parent_id=$userSession->parent_id;
				}
				
				$output['session_operation'] = "continue";
				$info_Cities = $this->GetCities($userSession->page_no);
				$menuCity = "";
				foreach($info_Cities as $City)
				{
					$menuCity .= $City->rownum.". ".$City->name."\n";
				}
				
				$info_Cities = $this->GetCities($userSession->page_no+4);
				
				$userSession->more_keyword=0;
				if($info_Cities)
				{
					$menuCity .= ($userSession->page_no+5).". More\n";
					$userSession->more_keyword=($userSession->page_no+5);
				}
				$output['session_msg'] = "From".chr(58)."\nPlease select \n".$menuCity."* Back";
				
				$userSession->per_page=4;
				$userSession->user_level=3;
				$userSession->keyword=$session_msg;
				$userSession->parent_id=$session_msg;
				$userSession->is_child=1;
				$userSession->save();
				
			break;
			case 2: //Transporters  > Companies menu
				
				if($moreKeyword)
				{
					$userSession->page_no = $userSession->page_no+4;
				}
				else
				{
					$userSession->last_parent_id=$userSession->parent_id;
				}
				
				$output['session_operation'] = "continue";
				$info_Companies = $this->GetCompanies($userSession->page_no);
				$menuCompany = "";
				foreach($info_Companies as $Company)
				{
					$menuCompany .= $Company->rownum.". ".$Company->name."\n";
				}
				
				$info_Companies = $this->GetCompanies($userSession->page_no+4);
				
				$userSession->more_keyword=0;
				if($info_Companies)
				{
					$menuCompany .= ($userSession->page_no+5).". More\n";
					$userSession->more_keyword=($userSession->page_no+5);
				}
				$output['session_msg'] = "Transporters".chr(58)."\nPlease select".chr(58)."\n".$menuCompany."* Back";
				
				$userSession->c_parent=0;
				$userSession->per_page=4;
				$userSession->user_level=3;
				$userSession->keyword=$session_msg;
				$userSession->parent_id=$session_msg;
				$userSession->is_child=1;
				$userSession->save();
				
			break;
			case 3: //Promo & Deals  > Companies menu
				
				if($moreKeyword)
				{
					$userSession->page_no = $userSession->page_no+4;
				}
				else
				{
					$userSession->last_parent_id=$userSession->parent_id;
				}
				
				$output['session_operation'] = "continue";
				$info_Companies = $this->GetCompanies($userSession->page_no);
				$menuCompany = "";
				foreach($info_Companies as $Company)
				{
					$menuCompany .= $Company->rownum.". ".$Company->name."\n";
				}
				
				$info_Companies = $this->GetCompanies($userSession->page_no+4);
				
				$userSession->more_keyword=0;
				if($info_Companies)
				{
					$menuCompany .= ($userSession->page_no+5).". More\n";
					$userSession->more_keyword=($userSession->page_no+5);
				}
				$output['session_msg'] = "Promo & Deals".chr(58)."\nPlease select company".chr(58)."\n".$menuCompany."* Back";
				$userSession->per_page=4;
				$userSession->user_level=3;
				$userSession->keyword=$session_msg;
				$userSession->parent_id=$session_msg;
				$userSession->is_child=1;
				$userSession->save();
				
			break;
			case 4: //Infotainment
				$this->customerLog($userSession->msisdn, $userSession->ussd_code, "Infotainment");
				$output['session_operation'] = "endcontinue";		
				$output['session_msg'] = "Infotainment. Thank you.\n*Main Menu";	
			break;
		}
		
		return $output;
	}
	
	public function Level4($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword)
	{		
		//Change page no on level change, check if level is same else change page no
		if($userSession->user_level != 4)
		{
			$userSession->page_no = 0;
		}
		
		switch($userSession->s_parent)
		{
			case 1: //Schedule & Fare > From Terminals
				
				------------
				if($moreKeyword)
				{
					$userSession->page_no = $userSession->page_no+4;
					$info_FromCity = City::Where('id',$userSession->from_city_id)->Get();
				}
				else
				{
					$userSession->last_parent_id=$userSession->parent_id;
					$info_FromCity = $this->GetSelectedCity($session_msg);
				}
				
				$output['session_operation'] = "continue";
								
				$info_Cities = $this->GetToCities($userSession->from_city_id, $userSession->page_no);
				$menuCity = "";
				foreach($info_Cities as $City)
				{
					$menuCity .= $City->rownum.". ".$City->name."\n";
				}
				
				$info_Cities = $this->GetToCities($userSession->from_city_id, $userSession->page_no+4);
				
				$userSession->more_keyword=0;
				if($info_Cities)
				{
					$menuCity .= ($userSession->page_no+5).". More\n";
					$userSession->more_keyword=($userSession->page_no+5);
				}
				$output['session_msg'] = "To".chr(58)."\nPlease select \n".$menuCity."* Back";
				
				$userSession->from_city_id=$info_FromCity[0]->id;
				$userSession->from_city_name=$info_FromCity[0]->short_name;
				$userSession->per_page=4;

				$userSession->user_level=5;
				$userSession->keyword=$session_msg;
				$userSession->parent_id=$session_msg;
				$userSession->is_child=1;
				$userSession->save();
				------------
				
				if($moreKeyword)
				{
					$userSession->page_no = $userSession->page_no+4;
					$info_FromCity = City::Where('id',$userSession->from_city_id)->Get();
				}
				else
				{
					$userSession->last_parent_id=$userSession->parent_id;
					$info_FromCity = $this->GetSelectedCity($session_msg);
				}
				
				$output['session_operation'] = "continue";
								
				$info_Terminals = $this->GetFromTerminals($info_FromCity[0]->id, $userSession->page_no);
				$menuTerminal = "";
				foreach($info_Terminals as $Terminal)
				{
					$menuTerminal .= $Terminal->rownum.". ".$Terminal->name."\n";
				}
				
				$info_Terminals = $this->GetFromTerminals($info_FromCity[0]->id, $userSession->page_no+4);
				
				$userSession->more_keyword=0;
				if($info_Terminals)
				{
					$menuTerminal .= ($userSession->page_no+5).". More\n";
					$userSession->more_keyword=($userSession->page_no+5);
				}
				$output['session_msg'] = "Departure Terminal".chr(58)."\nPlease select \n".$menuTerminal."* Back";
				
				$userSession->from_city_id=$info_FromCity[0]->id;
				$userSession->from_city_name=$info_FromCity[0]->short_name;
				$userSession->per_page=4;

				$userSession->user_level=4;
				$userSession->keyword=$session_msg;
				$userSession->parent_id=$session_msg;
				$userSession->is_child=1;
				$userSession->save();
				
			break;
			case 2: //Transporters > 1st menu
				//****************************************
				// Start Transporters Level 1
				//****************************************
				if($moreKeyword)
				{
					$userSession->page_no = $userSession->page_no+4;
				}
				else
				{
					$userSession->last_parent_id=$userSession->parent_id;
				}
				
				$output['session_operation'] = "continue";
				$info_Company = $this->GetSelectedCompany($session_msg);
				$output['session_msg'] = "Thanks for Choosing\n".$info_Company[0]->name."\n1. Schedule\n2. Bookings\n3. Terminals\n4. Contact Us\n5. Promo & Deals"."\n6. Policy Statement\n"."* Back"; 
				
				$userSession->c_parent_id=$session_msg;
				$userSession->more_keyword=0;
				$userSession->company_id=$info_Company[0]->id;
				$userSession->per_page=4;
				$userSession->user_level=4;
				$userSession->keyword=$session_msg;
				$userSession->parent_id=$session_msg;
				$userSession->is_child=1;
				$userSession->save();
				//****************************************
				// End Transporters
				//****************************************
			break;
			case 3: //Promo & Deals > List
				
				if($moreKeyword)
				{
					$userSession->page_no = $userSession->page_no+4;
				}
				else
				{
					$userSession->last_parent_id=$userSession->parent_id;
				}
				
				$output['session_operation'] = "continue";
				$info_Company = $this->GetSelectedCompany($session_msg);
				
				$info_Deals = $this->GetDeals($info_Company[0]->id,$userSession->page_no);
				$countOfMenu = sizeof($info_Deals);
				$menuDeal = "";
				foreach($info_Deals as $Deal)
				{
					$menuDeal .= $Deal->rownum.". ".$Deal->name."\n";
				}
				
				$info_Deals = $this->GetDeals($info_Company[0]->id,$userSession->page_no+4);
				
				$userSession->more_keyword=0;
				if($info_Deals)
				{
					$menuDeal .= ($userSession->page_no+5).". More\n";
					$userSession->more_keyword=($userSession->page_no+5);
				}
				$output['session_msg'] = "Promo & Deals: ".$info_Company[0]->name."\nSelect to see details".chr(58)."\n".$menuDeal."* Back";
				
				if($countOfMenu==0)
				{
					$output['session_operation'] = "endcontinue";
					$output['session_msg'] = "No promo at the moment, please check later. \n*Main Menu";
				}
				
				$userSession->company_id=$info_Company[0]->id;
				$userSession->per_page=4;
				$userSession->user_level=4;
				$userSession->keyword=$session_msg;
				$userSession->parent_id=$session_msg;
				$userSession->is_child=1;
				$userSession->save();
				
			break;
			case 4: //Infotainment
				//Already ended	
			break;
		}
		
		return $output;
	}
	
	public function Level5($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword)
	{
		//Uncomment echo "Menu 5";
		
		//Change page no on level change, check if level is same else change page no
		if($userSession->user_level != 5)
		{
			$userSession->page_no = 0;
		}
		
		switch($userSession->s_parent)
		{
			case 1: //Schedule & Fare > To Cities
				--------------------
				//Mean get out from parent menu
				$userSession->c_parent=0;
				if($moreKeyword)
				{
					$userSession->page_no = $userSession->page_no+4;
					$info_FromTerminal = Terminal::Where('id',$userSession->from_terminal_id)->Get();
				}
				else
				{
					$userSession->last_parent_id=$userSession->parent_id;
					$info_FromTerminal = $this->GetSelectedFromTerminal($userSession->from_city_id, $session_msg);
				}
				
				$output['session_operation'] = "continue";
								
				$info_Cities = $this->GetToCities($userSession->from_city_id, $userSession->page_no);
				$menuCity = "";
				foreach($info_Cities as $City)
				{
					$menuCity .= $City->rownum.". ".$City->name."\n";
				}
				
				$info_Cities = $this->GetToCities($userSession->from_city_id, $userSession->page_no+4);
				
				$userSession->more_keyword=0;
				if($info_Cities)
				{
					$menuCity .= ($userSession->page_no+5).". More\n";
					$userSession->more_keyword=($userSession->page_no+5);
				}
				$output['session_msg'] = "To".chr(58)."\nPlease select \n".$menuCity."* Back";
				
				$userSession->from_terminal_id=$info_FromTerminal[0]->id;
				$userSession->from_terminal_name=$info_FromTerminal[0]->name;
				$userSession->per_page=4;

				$userSession->user_level=5;
				$userSession->keyword=$session_msg;
				$userSession->parent_id=$session_msg;
				$userSession->is_child=1;
				$userSession->save();
			break;
			case 2: //Transporters
				//****************************************
				// Start Transporters Level 2
				//****************************************
				//Set company menu parent 
				$userSession->c_parent=$session_msg;
				switch($session_msg)
				{
					case 1: //Transporters > Schedule >  > From Cities
						if($moreKeyword)
						{
							$userSession->page_no = $userSession->page_no+4;
						}
						else
						{
							$userSession->c_last_parent_id=$userSession->c_parent_id;
						}
						
						$output['session_operation'] = "continue";
						$info_Cities = $this->GetCompanyCities($userSession->company_id, $userSession->page_no);
						$menuCity = "";
						foreach($info_Cities as $City)
						{
							$menuCity .= $City->rownum.". ".$City->name."\n";
						}
						
						$info_Cities = $this->GetCompanyCities($userSession->company_id, $userSession->page_no+4);
						
						$userSession->more_keyword=0;
						if($info_Cities)
						{
							$menuCity .= ($userSession->page_no+5).". More\n";
							$userSession->more_keyword=($userSession->page_no+5);
						}
						$output['session_msg'] = "From".chr(58)."\nPlease select \n".$menuCity."* Back";
						$userSession->per_page=4;
						$userSession->user_level=5;
						$userSession->keyword=$session_msg;
						$userSession->c_parent_id=$session_msg;
						$userSession->is_child=1;
						$userSession->save();
					break;
					case 2: //Transporters > Bookings
						//$this->customerLog($userSession->msisdn, $userSession->ussd_code, "Bookings");
						$output['session_operation'] = "endcontinue";		
						$output['session_msg'] = "Sorry, Bookings menu is unavailable for now.\n*To Main Menu";
					break;
					case 3: //Transporters > Terminals > Terminals Cities
						if($moreKeyword)
						{
							$userSession->page_no = $userSession->page_no+4;
						}
						else
						{
							$userSession->c_last_parent_id=$userSession->c_parent_id;
						}
						
						$output['session_operation'] = "continue";
						$info_Cities = $this->GetCompanyCities($userSession->company_id, $userSession->page_no);
						$menuCity = "";
						foreach($info_Cities as $City)
						{
							$menuCity .= $City->rownum.". ".$City->name."\n";
						}
						
						$info_Cities = $this->GetCompanyCities($userSession->company_id, $userSession->page_no+4);
						
						$userSession->more_keyword=0;
						if($info_Cities)
						{
							$menuCity .= ($userSession->page_no+5).". More\n";
							$userSession->more_keyword=($userSession->page_no+5);
						}
						$output['session_msg'] = "Terminal Location".chr(58)."\nPlease select \n".$menuCity."* Back";
						$userSession->per_page=4;
						$userSession->user_level=5;
						$userSession->keyword=$session_msg;
						$userSession->c_parent_id=$session_msg;
						$userSession->is_child=1;
						$userSession->save();
					break;
					case 4: //Transporters > Contact Us
						
						$this->customerLog($userSession->msisdn, $userSession->ussd_code, "Contact Us");
						$info_Company = Company::FindOrFail($userSession->company_id);
						$output['session_operation'] = "endcontinue";		
						$output['session_msg'] = "Contact Us".chr(58)."\nAddress: ".$info_Company->address."\nWhatapps:".$info_Company->Company_whatsapp()->First()->number."\nEmail:".$info_Company->email."\nPhone".$info_Company->Company_phone()->First()->number."\n*Main Menu";
					break;
					case 5: //Transporters > Promo & Deals > List
						if($moreKeyword)
						{
							$userSession->page_no = $userSession->page_no+4;
						}
						else
						{
							$userSession->c_last_parent_id=$userSession->c_parent_id;
						}
						
						$output['session_operation'] = "continue";
						
						$info_Deals = $this->GetDeals($userSession->company_id,$userSession->page_no);
						$menuDeal = "";
						foreach($info_Deals as $Deal)
						{
							$menuDeal .= $Deal->rownum.". ".$Deal->name."\n";
						}
						
						$info_Deals = $this->GetDeals($userSession->company_id,$userSession->page_no+4);
						$countOfMenu = sizeof($info_Deals);
						
						$userSession->more_keyword=0;
						if($info_Deals)
						{
							$menuDeal .= ($userSession->page_no+5).". More\n";
							$userSession->more_keyword=($userSession->page_no+5);
						}
						$output['session_msg'] = "Promo & Deals".chr(58)."\nPlease Select.\n".$menuDeal."* Back";
						if($countOfMenu==0)
						{
							$output['session_operation'] = "endcontinue";
							$output['session_msg'] = "No promo at the moment, please check later. \n*Main Menu";
						}
						$userSession->per_page=4;				
						$userSession->user_level=5;
						$userSession->keyword=$session_msg;
						$userSession->c_parent_id=$session_msg;
						$userSession->is_child=1;
						$userSession->save();
					break;
					case 6: //Transporters > Policy Statement > List
						if($moreKeyword)
						{
							$userSession->page_no = $userSession->page_no+4;
						}
						else
						{
							$userSession->c_last_parent_id=$userSession->c_parent_id;
						}
						
						$output['session_operation'] = "continue";
						
						$info_Policies = $this->GetPolicies($userSession->company_id,$userSession->page_no);
						$countOfMenu = sizeof($info_Policies);
						$menuPolicy = "";
						foreach($info_Policies as $Policy)
						{
							$menuPolicy .= $Policy->rownum.". ".$Policy->name."\n";
						}
						
						$info_Policies = $this->GetPolicies($userSession->company_id,$userSession->page_no+4);
						
						$userSession->more_keyword=0;
						if($info_Policies)
						{
							$menuPolicy .= ($userSession->page_no+5).". More\n";
							$userSession->more_keyword=($userSession->page_no+5);
						}
						$output['session_msg'] = "Policies".chr(58)."\nPlease Select.\n".$menuPolicy."* Back";
						if($countOfMenu==0)
						{
							$output['session_operation'] = "endcontinue";
							$output['session_msg'] = "No policy at the moment, please check later. \n*Main Menu";
						}
						$userSession->per_page=4;				
						$userSession->user_level=5;
						$userSession->keyword=$session_msg;
						$userSession->c_parent_id=$session_msg;
						$userSession->is_child=1;
						$userSession->save();
					break;
				}
				//****************************************
				// End Transporters
				//****************************************
			break;
			case 3: //Promo & Deals > end
				$this->customerLog($userSession->msisdn, $userSession->ussd_code, "Promo & Deals");
				$userSession->c_parent=0;
				$info_Deal = $this->GetSelectedDeal($userSession->company_id,$session_msg);
				$output['session_operation'] = "endcontinue";		
				$output['session_msg'] = "Promo & Deals".chr(58)."\n".$info_Deal[0]->name."\n".$info_Deal[0]->detail."\n*Main Menu";
			break;
			case 4: //Infotainment
				//Already ended	
			break;
		}
		
		return $output;
	}
	
	public function Level6($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword)
	{
		//Uncomment echo "Menu 6";
		
		//Change page no on level change, check if level is same else change page no
		if($userSession->user_level != 6)
		{
			$userSession->page_no = 0;
		}
		
		switch($userSession->s_parent)
		{
			case 1: //Schedule & Fare > Dates
				
				if($moreKeyword)
				{
					$userSession->page_no = $userSession->page_no+5;
					$info_ToCity = City::Where('id',$userSession->to_city_id)->Get();
				}
				else
				{
					$userSession->last_parent_id=$userSession->parent_id;
					$info_ToCity = $this->GetSelectedToCity($userSession->from_city_id,$session_msg);
					
					//Get schedules
					$info_Schedules = $this->GetScheduleCountByCities($userSession->from_terminal_id,$info_ToCity[0]->id);
				}
				
				if(sizeof($info_Schedules)>0)
				{
					$output['session_operation'] = "continue";
					
					$date = date('D d M', mktime(0, 0, 0, date("m") , date("d")-1,date("Y")));
					$menuDate = "";
					for($i=$userSession->page_no+1; $i < ($userSession->page_no+6); $i++)
					{
						$menuDate .= $i.". ".date('D d M', strtotime("+$i day", strtotime($date)))."\n";
					}
					$menuDate .= $i.". More\n";
					$userSession->more_keyword=$i;
					
					$output['session_msg'] = "Date".chr(58)."\nPlease select \n".$menuDate."* Back";
					
					$userSession->to_city_id=$info_ToCity[0]->id;

					$userSession->to_city_name=$info_ToCity[0]->short_name;
					
					$userSession->per_page=5;
					$userSession->user_level=6;
					$userSession->keyword=$session_msg;
					$userSession->parent_id=$session_msg;
					$userSession->is_child=1;
					$userSession->save();
				}
				else
				{
					$output['session_operation'] = "endcontinue";
					$output['session_msg'] = "Sorry the routes does not  exist".chr(58)."\n* Back";
				}
				
			break;
			case 2: //Transporters
				//****************************************
				// Start Transporters Level 3
				//****************************************
				switch($userSession->c_parent)
				{
					case 1: //Transporters > Schedule > From Terminal
						
						if($moreKeyword)
						{
							$userSession->page_no = $userSession->page_no+4;
							$info_FromCity = City::Where('id',$userSession->from_city_id)->Get();
						}
						else
						{
							$userSession->c_last_parent_id=$userSession->c_parent_id;
							$info_FromCity = $this->GetSelectedCompanyCity($userSession->company_id, $session_msg);
						}
						
						$output['session_operation'] = "continue";
										
						$info_Terminals = $this->GetFromTerminals($info_FromCity[0]->id, $userSession->page_no);
						$menuTerminal = "";
						foreach($info_Terminals as $Terminal)
						{
							$menuTerminal .= $Terminal->rownum.". ".$Terminal->name."\n";
						}
						
						$info_Terminals = $this->GetFromTerminals($info_FromCity[0]->id, $userSession->page_no+4);
						
						$userSession->more_keyword=0;
						if($info_Terminals)
						{
							$menuTerminal .= ($userSession->page_no+5).". More\n";
							$userSession->more_keyword=($userSession->page_no+5);
						}
						$output['session_msg'] = "Departure Terminal".chr(58)."\nPlease select \n".$menuTerminal."* Back";
						
						$userSession->from_city_id=$info_FromCity[0]->id;
						$userSession->from_city_name=$info_FromCity[0]->short_name;
						$userSession->per_page=4;
		
						$userSession->user_level=6;
						$userSession->keyword=$session_msg;
						$userSession->parent_id=$session_msg;
						$userSession->is_child=1;
						$userSession->save();
						
					break;
					case 2: //Transporters > Bookings
							//NA
					break;
					case 3: //Transporters > Terminals > Cities
						if($moreKeyword)
						{
							$userSession->page_no = $userSession->page_no+4;
							$info_FromCity = City::Where('id',$userSession->from_city_id)->Get();
						}
						else
						{
							$userSession->c_last_parent_id=$userSession->c_parent_id;
							$info_FromCity = $this->GetSelectedCompanyCity($userSession->company_id,$session_msg);
						}
						
						$output['session_operation'] = "continue";
						
						$info_Terminals = $this->GetCompanyCityTerminals($userSession->company_id,$info_FromCity[0]->id, $userSession->page_no);
						$menuTerminals = "";
						foreach($info_Terminals as $Terminals)
						{
							$menuTerminals .= $Terminals->rownum.". ".$Terminals->name."\n";
						}
						
						$info_Terminals = $this->GetCompanyCityTerminals($userSession->company_id,$info_FromCity[0]->id, $userSession->page_no+4);
						
						$userSession->more_keyword=0;
						if($info_Terminals)
						{
							$menuTerminals .= ($userSession->page_no+5).". More\n";
							$userSession->more_keyword=($userSession->page_no+5);
						}
						$output['session_msg'] = $info_FromCity[0]->name."\nPlease select \n".$menuTerminals."* Back";
						
						$userSession->from_city_id=$info_FromCity[0]->id;
						$userSession->from_city_name=$info_FromCity[0]->name;
						$userSession->per_page=4;
						$userSession->user_level=6;
						$userSession->keyword=$session_msg;
						$userSession->c_parent_id=$session_msg;
						$userSession->is_child=1;
						$userSession->save();
					break;
					case 4: //Transporters > Contact Us
							//Already ended	
					break;
					case 5: //Transporters > Promo & Deals
						$this->customerLog($userSession->msisdn, $userSession->ussd_code, "Promo & Deals");
						$info_Deal = $this->GetSelectedDeal($userSession->company_id,$session_msg);
						$output['session_operation'] = "endcontinue";		
						$output['session_msg'] = "Promo & Deals".chr(58)."\n".$info_Deal[0]->name."\n".$info_Deal[0]->detail."\n*Main Menu";
					break;
					case 6: //Transporters > Policy Statement
						$this->customerLog($userSession->msisdn, $userSession->ussd_code, "Policy Statement");
						$info_Policy = $this->GetSelectedPolicy($userSession->company_id,$session_msg);
						$output['session_operation'] = "endcontinue";		
						$output['session_msg'] = "Policy".chr(58)."\n".$info_Policy[0]->name."\n".$info_Policy[0]->detail."\n*Main Menu";
					break;
				}
				//****************************************
				// End Transporters
				//****************************************
				
			break;
			case 3: //Promo & Deals
				//Already ended	
			break;
			case 4: //Infotainment
				//Already ended	
			break;
		}
		return $output;
	}
	
	public function Level7($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword)
	{
		//Uncomment echo "Menu 7";
		
		//Change page no on level change, check if level is same else change page no
		if($userSession->user_level != 7)
		{
			$userSession->page_no = 0;
		}
		
		switch($userSession->s_parent)
		{
			case 1: //Schedule & Fare > Companies List
				
				if($moreKeyword)
				{
					$userSession->page_no = $userSession->page_no+4;
					$info_Date = $userSession->schedule_date;
				}
				else
				{
					$userSession->last_parent_id=$userSession->parent_id;
					$date = date('D d M', mktime(0, 0, 0, date("m") , date("d")-1,date("Y")));
					$info_Date = date('Y-m-d', strtotime("+$session_msg day", strtotime($date)));
				}
				
				$output['session_operation'] = "continue";
				$info_Companies = $this->GetCompanies($userSession->page_no);
				$menuCompany = "";
				foreach($info_Companies as $Company)
				{
					$menuCompany .= $Company->rownum.". ".$Company->name."\n";
				}
				
				$info_Companies = $this->GetCompanies($userSession->page_no+4);
				
				$userSession->more_keyword=0;
				if($info_Companies)
				{
					$menuCompany .= ($userSession->page_no+5).". More\n";
					$userSession->more_keyword=($userSession->page_no+5);
				}
				$output['session_msg'] = "Transporters".chr(58)."\nPlease select".chr(58)."\n".$menuCompany."* Back";
				
				$userSession->schedule_date = $info_Date;
				$userSession->per_page=4;
				$userSession->user_level=7;
				$userSession->keyword=$session_msg;
				$userSession->parent_id=$session_msg;
				$userSession->is_child=1;
				$userSession->save();
				
				
			break;
			case 2: //Transporters
				
				//****************************************
				// Start Transporters Level 4
				//****************************************
				switch($userSession->c_parent)
				{
					case 1: //Transporters > Schedule > To Cities
						
						if($moreKeyword)
						{
							$userSession->page_no = $userSession->page_no+4;
							$info_FromTerminal = Terminal::Where('id',$userSession->from_terminal_id)->Get();
						}
						else
						{
							$userSession->last_parent_id=$userSession->parent_id;
							$info_FromTerminal = $this->GetSelectedFromTerminal($userSession->from_city_id, $session_msg);
						}
						
						$output['session_operation'] = "continue";
						
						$info_Cities = $this->GetCompanyToCities($userSession->from_city_id, $userSession->company_id, $userSession->page_no);
						$menuCity = "";
						foreach($info_Cities as $City)
						{
							$menuCity .= $City->rownum.". ".$City->name."\n";
						}
						
						$info_Cities = $this->GetCompanyToCities($userSession->from_city_id, $userSession->company_id, $userSession->page_no+4);
						
						$userSession->more_keyword=0;
						if($info_Cities)
						{
							$menuCity .= ($userSession->page_no+5).". More\n";
							$userSession->more_keyword=($userSession->page_no+5);
						}
						$output['session_msg'] = "To".chr(58)."\nPlease select \n".$menuCity."* Back";
						
						$userSession->from_terminal_id=$info_FromTerminal[0]->id;
						$userSession->from_terminal_name=$info_FromTerminal[0]->name;
						  
						$userSession->per_page=4;
						$userSession->user_level=7;
						$userSession->keyword=$session_msg;
						$userSession->c_parent_id=$session_msg;
						$userSession->is_child=1;
						$userSession->save();
						
					break;
					case 2: //Transporters > Bookings
							//NA
					break;
					case 3: //Transporters > Terminals
						$this->customerLog($userSession->msisdn, $userSession->ussd_code, "Terminals");
						$info_Terminal = $this->GetSelectedCompanyCityTerminal($userSession->company_id,$userSession->from_city_id, $session_msg);
						$output['session_operation'] = "endcontinue";		
						$output['session_msg'] = $info_Terminal[0]->name."\n".$info_Terminal[0]->address."\n".$info_Terminal[0]->phone_number."\n*Main Menu";
						
					break;
					case 4: //Transporters > Contact Us
							//Already ended	
					break;
					case 5: //Transporters > Promo & Deals
							//Already ended	
					break;
				}
				//****************************************
				// End Transporters
				//****************************************
				
			break;
			case 3: //Promo & Deals
				//Already ended	
			break;
			case 4: //Infotainment
				//Already ended	
			break;
		}
		
		return $output;
	}
	
	public function Level8($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword)
	{
		//Uncomment echo "Menu 8";
		
		//Change page no on level change, check if level is same else change page no
		if($userSession->user_level != 8)
		{
			$userSession->page_no = 0;
		}
		
		switch($userSession->s_parent)
		{
			case 1: //Schedule & Fare > Schedules list
				
				
				if($moreKeyword)
				{
					$userSession->page_no = $userSession->page_no+4;
					$info_Company = Company::Where('id',$userSession->company_id)->Get();
				}
				else
				{
					$userSession->last_parent_id=$userSession->parent_id;
					$info_Company = $this->GetSelectedCompany($session_msg);
				}
				
				if($backKeyword)
				{
					$info_Company = Company::Where('id',$userSession->company_id)->Get();
				}
				
				$output['session_operation'] = "continue";
				
				$info_Schedules = $this->GetCompanySchedule($info_Company[0]->id,$userSession->schedule_date,$userSession->from_terminal_id,$userSession->to_city_id,$userSession->page_no);
				$menuSchedule = "";
				foreach($info_Schedules as $info_Schedule)
				{
					$menuSchedule .= $info_Schedule['rownum'].". ".$info_Schedule['dept_time'].", ".$info_Schedule['terminal_name']." - ".$info_Schedule['dept_terminal'].", N".$info_Schedule['fare']."\n";
				}
				
				$info_Schedules = $this->GetCompanySchedule($info_Company[0]->id,$userSession->schedule_date,$userSession->from_terminal_id,$userSession->to_city_id,$userSession->page_no+4);
				
				$userSession->more_keyword=0;
				if($info_Schedules)
				{
					$menuSchedule .= ($userSession->page_no+5).". More\n";
					$userSession->more_keyword=($userSession->page_no+5);
				}
				$output['session_msg'] = "Schedules".chr(58)."\n".$menuSchedule."* Back";
				
				$userSession->company_id=$info_Company[0]->id;
				$userSession->per_page=4;
				$userSession->user_level=8;
				$userSession->keyword=$session_msg;
				$userSession->parent_id=$session_msg;
				$userSession->is_child=1;
				$userSession->save();
				
				if($menuSchedule=="")
				{
					$output['session_operation'] = "endcontinue";
					$output['session_msg'] = "Sorry no schedule exist".chr(58)."\n".$menuSchedule."\n*Main Menu";
				}
			break;
			case 2: //Transporters
				//****************************************
				// Start Transporters Level 5
				//****************************************
				switch($userSession->c_parent)
				{
					case 1: //Transporters > Schedule > Date list
						
						if($moreKeyword)
						{
							$userSession->page_no = $userSession->page_no+5;
							$info_ToCity = City::Where('id',$userSession->to_city_id)->Get();

						}
						else
						{
							$userSession->c_last_parent_id=$userSession->c_parent_id;
							$info_ToCity = $this->GetSelectedCompanyToCity($userSession->from_city_id,$userSession->company_id,$session_msg);
							
							//Get schedules
							$info_Schedules = $this->GetScheduleCountByCitiesCompany($userSession->company_id,$userSession->from_terminal_id,$info_ToCity[0]->id);
						}
						
						if(sizeof($info_Schedules)>0)
						{
							$output['session_operation'] = "continue";
							
							$date = date('D d M', mktime(0, 0, 0, date("m") , date("d")-1,date("Y")));
							$menuDate = "";
							for($i=$userSession->page_no+1; $i < ($userSession->page_no+6); $i++)
							{
								$menuDate .= $i.". ".date('D d M', strtotime("+$i day", strtotime($date)))."\n";
							}
							$menuDate .= $i.". More\n";
							$userSession->more_keyword=$i;
							
							$output['session_msg'] = "Date".chr(58)."\nPlease select \n".$menuDate."* Back";
							
							$userSession->to_city_id=$info_ToCity[0]->id;
							$userSession->to_city_name=$info_ToCity[0]->short_name;
							$userSession->per_page=5;
							$userSession->user_level=8;
							$userSession->keyword=$session_msg;
							$userSession->c_parent_id=$session_msg;
							$userSession->is_child=1;
							$userSession->save();
						}
						else
						{
							$output['session_operation'] = "endcontinue";
							$output['session_msg'] = "The Route Selected for this company does not exist".chr(58)."\n*Main Menu";
						}						
						
					break;
					case 2: //Transporters > Bookings
							//NA
					break;
					case 3: //Transporters > Terminals
							//Already ended
					break;
					case 4: //Transporters > Contact Us
							//Already ended	
					break;
					case 5: //Transporters > Promo & Deals
							//Already ended	
					break;
				}
				//****************************************
				// End Transporters
				//****************************************
			break;
			case 3: //Promo & Deals
				//Already ended	
			break;
			case 4: //Infotainment
				//Already ended	
			break;
		}
		
		return $output;
	}
	
	
	public function Level9($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword)
	{
		//Uncomment echo "Menu 9";
		
		//Change page no on level change, check if level is same else change page no
		if($userSession->user_level != 9)
		{
			$userSession->page_no = 0;
		}
		
		switch($userSession->s_parent)
		{
			case 1: //Schedule & Fare > Schedule detail
				$output['session_operation'] = "endcontinue";
				
				$info_CompanySchedule = $this->GetSelectedCompanySchedule($userSession->company_id,$userSession->schedule_date,$userSession->from_terminal_id,$userSession->to_city_id,$session_msg);

				$info_Company = Company::FindOrFail($userSession->company_id);
				$info_Schedule = Schedule::FindOrFail($info_CompanySchedule[0]['sch_id']);
				$info_Fare =  Fare::FindOrFail($info_CompanySchedule[0]['fid']);
				
				$BusName = $info_Schedule->Bus()->First()->name;
				$FareType = $info_Fare->Fare_type()->First()->name;
				$ServiceName = $info_Fare->Fare_type()->First()->Service()->First()->name;
				
				$FirstSchTime = $info_Schedule->Schedule_time()->Orderby('sequence')->First();
				$DeptTime = date('h:iA',strtotime($FirstSchTime->departure_time));
				$DeptDate = date('D d M',strtotime($info_CompanySchedule[0]['dept_time']));				
				
				$LastSchTime = $info_Schedule->Schedule_time()->Orderby('sequence', 'desc')->First();
				$ArrvTime = date('h:iA',strtotime($LastSchTime->arrival_time));
				$time1 = strtotime($FirstSchTime->departure_time);
				$time2 = strtotime($LastSchTime->arrival_time);
				$interval = $time1-$time2;
				if($interval<0)
					$interval = abs($interval);
				
				$ArrvDate =  date('D d M',strtotime($FirstSchTime->departure_time)+$interval);
				
				$OtherSchTime="";
				foreach($info_Schedule->Schedule_time()->where('sequence','>','1')->Orderby('sequence')->Get() as $Schedult_time)
				{
					$OtherSchTime .= $Schedult_time->Terminal()->First()->name." ".date('h:iA',strtotime($Schedult_time->arrival_time))."\n";
				}
				
				$output['session_msg'] = "1. ".$info_Company->name."".chr(58)."\n".$userSession->from_city_name." to ".$userSession->to_city_name."\n\n".$BusName."\n".$FareType." ".$ServiceName." N".$info_CompanySchedule[0]['fare']."\n\nDept".chr(58)."\n".$DeptDate."\n".$FirstSchTime->Terminal()->First()->name." ".$DeptTime."\n\nArrv".chr(58)."\n".$ArrvDate."\n".$OtherSchTime."\n*Main Menu";
				
				$userSession->schedule_id=$info_CompanySchedule[0]['sch_id'];
				$userSession->fare_id=$info_CompanySchedule[0]['fid'];

				$userSession->user_level=9;
				$userSession->keyword=$session_msg;
				$userSession->parent_id=$session_msg;
				$userSession->is_child=1;
				$userSession->save();
			break;
			case 2: //Transporters
				//****************************************
				// Start Transporters Level 6
				//****************************************
				switch($userSession->c_parent)
				{
					case 1: //Transporters > Schedule > Schedule list
						
						if($moreKeyword)
						{
							$userSession->page_no = $userSession->page_no+4;
							$info_Date = $userSession->schedule_date;
						}
						else
						{
							$userSession->c_last_parent_id=$userSession->c_parent_id;
							$date = date('D d M', mktime(0, 0, 0, date("m") , date("d")-1,date("Y")));
							$info_Date = date('Y-m-d', strtotime("+$session_msg day", strtotime($date)));
						}
						
						$output['session_operation'] = "continue";
						
						$info_Schedules = $this->GetCompanySchedule($userSession->company_id,$info_Date,$userSession->from_terminal_id,$userSession->to_city_id,$userSession->page_no);
						$menuSchedule = "";
						foreach($info_Schedules as $info_Schedule)
						{
							$menuSchedule .= $info_Schedule['rownum'].". ".$info_Schedule['dept_time'].", ".$info_Schedule['terminal_name']." - ".$info_Schedule['dept_terminal'].", N".$info_Schedule['fare']."\n";
						}
						
						$info_Schedules = $this->GetCompanySchedule($userSession->company_id,$info_Date,$userSession->from_terminal_id,$userSession->to_city_id,$userSession->page_no+4);
						
						$userSession->more_keyword=0;
						if($info_Schedules)
						{
							$menuSchedule .= ($userSession->page_no+5).". More\n";
							$userSession->more_keyword=($userSession->page_no+5);
						}
						$output['session_msg'] = "Schedules".chr(58)."\n".$menuSchedule."* Back";
						
						$userSession->schedule_date = $info_Date;
						$userSession->per_page=4;
						$userSession->user_level=9;
						$userSession->keyword=$session_msg;
						$userSession->c_parent_id=$session_msg;
						$userSession->is_child=1;
						$userSession->save();
						
						if($menuSchedule=="")
						{
							$output['session_operation'] = "endcontinue";
							$output['session_msg'] = "Sorry no schedule exist".chr(58)."\n".$menuSchedule."\n*Main Menu";
						}
					break;
					case 2: //Transporters > Bookings
							//NA
					break;
					case 3: //Transporters > Terminals
							//Already ended
					break;
					case 4: //Transporters > Contact Us
							//Already ended	
					break;
					case 5: //Transporters > Promo & Deals
							//Already ended	
					break;
				}
				//****************************************
				// End Transporters
				//****************************************
			break;
			case 3: //Promo & Deals
				//Already ended	
			break;
			case 4: //Infotainment
				//Already ended	
			break;
		}
		
		return $output;
	}
	
	public function Level10($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword)
	{
		//Uncomment echo "Menu 10";
		
		//Change page no on level change, check if level is same else change page no
		if($userSession->user_level != 9)
		{
			$userSession->page_no = 0;
		}
		
		switch($userSession->s_parent)
		{
			case 1: //Schedule & Fare
					//Already ended	
			break;
			case 2: //Transporters
				//****************************************
				// Start Transporters Level 4
				//****************************************
				switch($userSession->c_parent)
				{
					case 1: //Transporters > Schedule > Schedule detail
							
						$output['session_operation'] = "endcontinue";
						
						$info_CompanySchedule = $this->GetSelectedCompanySchedule($userSession->company_id,$userSession->schedule_date,$userSession->from_terminal_id,$userSession->to_city_id,$session_msg);
						
						$info_Company = Company::FindOrFail($userSession->company_id);
						$info_Schedule = Schedule::FindOrFail($info_CompanySchedule[0]['sch_id']);
						$info_Fare =  Fare::FindOrFail($info_CompanySchedule[0]['fid']);
						
						$BusName = $info_Schedule->Bus()->First()->name;
						$FareType = $info_Fare->Fare_type()->First()->name;
						$ServiceName = $info_Fare->Fare_type()->First()->Service()->First()->name;
						
						$FirstSchTime = $info_Schedule->Schedule_time()->Orderby('sequence')->First();
						$DeptTime = date('h:iA',strtotime($FirstSchTime->departure_time));
						$DeptDate = date('D d M',strtotime($info_CompanySchedule[0]['dept_time']));				
						
						$LastSchTime = $info_Schedule->Schedule_time()->Orderby('sequence', 'desc')->First();
						$ArrvTime = date('h:iA',strtotime($LastSchTime->arrival_time));
						$time1 = strtotime($FirstSchTime->departure_time);
						$time2 = strtotime($LastSchTime->arrival_time);
						$interval = $time1-$time2;
						if($interval<0)
							$interval = abs($interval);
					
						$ArrvDate =  date('D d M',strtotime($FirstSchTime->departure_time)+$interval);
						
						$OtherSchTime="";
						foreach($info_Schedule->Schedule_time()->where('sequence','>','1')->Orderby('sequence')->Get() as $Schedult_time)
						{
							$OtherSchTime .= $Schedult_time->Terminal()->First()->name." ".date('h:iA',strtotime($Schedult_time->arrival_time))."\n";
						}
						
						$output['session_msg'] = "1. ".$info_Company->name."".chr(58)."\n".$userSession->from_city_name." to ".$userSession->to_city_name."\n\n".$BusName."\n".$FareType." ".$ServiceName." N".$info_CompanySchedule[0]['fare']."\n\nDept".chr(58)."\n".$DeptDate."\n".$FirstSchTime->Terminal()->First()->name." ".$DeptTime."\n\nArrv".chr(58)."\n".$ArrvDate."\n".$OtherSchTime."\n*Main Menu";
						
						$userSession->schedule_id=$info_CompanySchedule[0]['sch_id'];
						$userSession->fare_id=$info_CompanySchedule[0]['fid'];
						
						$userSession->user_level=10;
						$userSession->keyword=$session_msg;
						$userSession->parent_id=$session_msg;
						$userSession->is_child=1;
						$userSession->save();
					break;
					case 2: //Transporters > Bookings
							//NA
					break;
					case 3: //Transporters > Terminals
							//Already ended
					break;
					case 4: //Transporters > Contact Us
							//Already ended	
					break;
					case 5: //Transporters > Promo & Deals
							//Already ended	
					break;
				}
				//****************************************
				// End Transporters
				//****************************************
			break;
			case 3: //Promo & Deals
				//Already ended	
			break;
			case 4: //Infotainment
				//Already ended	
			break;
		}
		

		return $output;
	}
	
	public function GetCities($page)
	{
		$sql = "select * from (SELECT c.id, c.name, @rownum := @rownum + 1 AS rownum FROM cities c, (SELECT @rownum := 0) r order by name) data limit 4 offset ".$page;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	public function GetSelectedCity($rownum)
	{		
		$sql = "select * from (SELECT c.id, c.name, c.short_name, @rownum := @rownum + 1 AS rownum FROM cities c, (SELECT @rownum := 0) r order by name) data where rownum=".$rownum;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	
	public function GetFromTerminals($city_id, $page)
	{
		$sql = "select * from ( select id, name, short_name, @rownum := @rownum + 1 AS rownum from ( SELECT t.id, t.name, t.short_name FROM terminals t where t.lga_id in (select id from lgas where city_id='".$city_id."')) data, (SELECT @rownum := 0) r order by name ) outerRow limit 4 offset ".$page;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	public function GetSelectedFromTerminal($city_id, $rownum)
	{		
		$sql = "select * from ( select id, name, short_name, @rownum := @rownum + 1 AS rownum from ( SELECT t.id, t.name, t.short_name FROM terminals t where t.lga_id in (select id from lgas where city_id='".$city_id."')) data, (SELECT @rownum := 0) r order by name ) outerRow where rownum=".$rownum;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	
	public function GetToCities($city_id,$page)
	{	
		$sql = "select * from (SELECT c.id, c.name, @rownum := @rownum + 1 AS rownum FROM cities c, (SELECT @rownum := 0) r where id<>".$city_id." order by name) data limit 4 offset ".$page;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	public function GetSelectedToCity($city_id,$rownum)
	{
		$sql = "select * from (SELECT c.id, c.name, c.short_name, @rownum := @rownum + 1 AS rownum FROM cities c, (SELECT @rownum := 0) r where id<>".$city_id."  order by name) data where rownum=".$rownum;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	
	public function GetCompanyCities($company_id,$page)
	{	
		$sql = "select * from ( select id, name, short_name, @rownum := @rownum + 1 AS rownum from ( SELECT c.id, c.name, c.short_name FROM cities c where c.id in (SELECT distinct city_id FROM lgas JOIN terminals on (lgas.id=terminals.lga_id) where terminals.company_id=".$company_id.")) data, (SELECT @rownum := 0) r order by name ) outerRow limit 4 offset ".$page;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	public function GetSelectedCompanyCity($company_id,$rownum)
	{
		$sql = "select * from ( select id, name, short_name, @rownum := @rownum + 1 AS rownum from ( SELECT c.id, c.name, c.short_name FROM cities c where c.id in (SELECT distinct city_id FROM lgas JOIN terminals on (lgas.id=terminals.lga_id) where terminals.company_id=".$company_id.")) data, (SELECT @rownum := 0) r order by name ) outerRow where rownum=".$rownum;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	
	public function GetCompanyToCities($city_id,$company_id,$page)
	{
		$sql = "select * from ( select id, name, short_name, @rownum := @rownum + 1 AS rownum from ( SELECT c.id, c.name, c.short_name FROM cities c where id<>".$city_id." and c.id in (SELECT distinct city_id FROM lgas JOIN terminals on (lgas.id=terminals.lga_id) where terminals.company_id=".$company_id.")) data, (SELECT @rownum := 0) r order by name ) outerRow limit 4 offset ".$page;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	public function GetSelectedCompanyToCity($city_id,$company_id,$rownum)
	{
		$sql = "select * from ( select id, name, short_name, @rownum := @rownum + 1 AS rownum from ( SELECT c.id, c.name, c.short_name FROM cities c where id<>".$city_id." and c.id in (SELECT distinct city_id FROM lgas JOIN terminals on (lgas.id=terminals.lga_id) where terminals.company_id=".$company_id.")) data, (SELECT @rownum := 0) r order by name ) outerRow where rownum=".$rownum;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	
	public function GetCompanies($page)
	{
		$sql = "select * from (SELECT c.id, c.name, @rownum := @rownum + 1 AS rownum FROM companies c, (SELECT @rownum := 0) r order by name) data limit 4 offset ".$page;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	public function GetSelectedCompany($rownum)
	{
		$sql = "select * from (SELECT c.id, c.name, @rownum := @rownum + 1 AS rownum FROM companies c, (SELECT @rownum := 0) r order by name) data where rownum=".$rownum;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	
	public function GetCompanyPageNo($company_id)
	{
		$sql = "select * from (SELECT c.id, c.name, @rownum := @rownum + 1 AS rownum FROM companies c, (SELECT @rownum := 0) r order by name) data where id=".$company_id;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	
	public function GetDeals($company_id,$page)
	{
		$sql = "select * from (SELECT d.id, d.name, @rownum := @rownum + 1 AS rownum FROM deals d, (SELECT @rownum := 0) r where company_id=".$company_id." order by name) data limit 4 offset ".$page;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	public function GetSelectedDeal($company_id,$rownum)
	{
		$sql = "select * from (SELECT d.id, d.name, d.detail, @rownum := @rownum + 1 AS rownum FROM deals d, (SELECT @rownum := 0) r where company_id=".$company_id." order by name) data where rownum=".$rownum;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	
	public function GetPolicies($company_id,$page)
	{
		$sql = "select * from (SELECT p.id, p.name, @rownum := @rownum + 1 AS rownum FROM policies p, (SELECT @rownum := 0) r where company_id=".$company_id." order by name) data limit 4 offset ".$page;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	public function GetSelectedPolicy($company_id,$rownum)
	{
		$sql = "select * from (SELECT p.id, p.name, p.detail, @rownum := @rownum + 1 AS rownum FROM policies p, (SELECT @rownum := 0) r where company_id=".$company_id." order by name) data where rownum=".$rownum;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	
	public function GetCompanyCityTerminals($company_id,$city_id,$page)
	{
		$sql = "select * from (SELECT terminals.id, terminals.name, terminals.address, terminals.email, terminals.phone_number, @rownum := @rownum + 1 AS rownum FROM terminals join lgas on (lgas.id=terminals.lga_id), (SELECT @rownum := 0) r WHERE lgas.city_id=".$city_id." AND terminals.company_id=".$company_id." ORDER by rownum, terminals.name) data limit 4 offset ".$page;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	public function GetSelectedCompanyCityTerminal($company_id,$city_id,$rownum)
	{
		$sql = "select * from (SELECT terminals.id, terminals.name, terminals.address, terminals.email, terminals.phone_number, @rownum := @rownum + 1 AS rownum FROM terminals join lgas on (lgas.id=terminals.lga_id), (SELECT @rownum := 0) r WHERE lgas.city_id=".$city_id." AND terminals.company_id=".$company_id." ORDER by rownum, terminals.name) data where rownum=".$rownum;
		//echo "<br/>".$sql."<br />";
		return DB::Select($sql);
	}
	
	public function GetScheduleCountByCities($from_terminal_id,$to_city_id)
	{
		$sql = "SELECT 
					schedules.id as sch_id, fares.id as fid, 
					schedule_times.departure_time as dept_time, 
					services.short_name as service_name, 
					fares.fare as fare,
					terminals.name as terminal_name
					FROM 
					schedules 
					JOIN schedule_times ON (schedules.id=schedule_times.schedule_id)
					join fare_types on (schedules.id=fare_types.schedule_id) 
					join services on (fare_types.service_id=services.id) 
					join fares on (fare_types.id=fares.fare_type_id) 
					join terminals on (fares.from_terminal_id=terminals.id) 
					where 
					fares.from_terminal_id =".$from_terminal_id."
					and
					fares.to_terminal_id in (
						SELECT distinct terminals.id FROM 
						terminals 
						join lgas on (terminals.lga_id=lgas.id)
						join cities on (lgas.city_id=cities.id)
						where 
						cities.id=".$to_city_id."
					)
					order by schedule_times.departure_time";
		//echo "<br/>".$sql."<br />";
		$Results = DB::Select($sql);
		//print_r($Response);
		return $Results;
	}
	
	public function GetScheduleCountByCitiesCompany($company_id,$from_terminal_id,$to_city_id)
	{
		$sql = "SELECT 
					schedules.id as sch_id, fares.id as fid, 
					schedule_times.departure_time as dept_time, 
					services.short_name as service_name, 
					fares.fare as fare,
					terminals.name as terminal_name
					FROM 
					schedules 
					JOIN schedule_times ON (schedules.id=schedule_times.schedule_id)
					join fare_types on (schedules.id=fare_types.schedule_id) 
					join services on (fare_types.service_id=services.id) 
					join fares on (fare_types.id=fares.fare_type_id) 
					join terminals on (fares.from_terminal_id=terminals.id) 
					where 
					fare_types.company_id=".$company_id." 
					and
					fares.from_terminal_id =".$from_terminal_id."
					and
					fares.to_terminal_id in (
						SELECT distinct terminals.id FROM 
						terminals 
						join lgas on (terminals.lga_id=lgas.id)
						join cities on (lgas.city_id=cities.id)
						where 
						terminals.company_id=".$company_id."
						and cities.id=".$to_city_id."
					)
					order by schedule_times.departure_time";
		//echo "<br/>".$sql."<br />";
		$Results = DB::Select($sql);
		//print_r($Response);
		return $Results;
	}
	
	public function GetCompanySchedule($company_id,$schedule_date,$from_terminal_id,$to_city_id,$page)
	{
		$timestamp = strtotime($schedule_date);
		$sql = "SELECT 
					schedules.id as sch_id, fares.id as fid, 
					schedule_times.departure_time as dept_time, 
					services.short_name as service_name, 
					fares.fare as fare,
					terminals.name as terminal_name
					FROM 
					schedules 
					JOIN schedule_times ON (schedules.id=schedule_times.schedule_id)
					join fare_types on (schedules.id=fare_types.schedule_id) 
					join services on (fare_types.service_id=services.id) 
					join fares on (fare_types.id=fares.fare_type_id) 
					join terminals on (fares.from_terminal_id=terminals.id) 
					where 
					schedules.".strtolower(date('l', $timestamp))."=1  and schedule_times.sequence=1 and 
					schedules.operatingend_date>'".$schedule_date."' and
					fare_types.company_id=".$company_id." 
					and
					fares.from_terminal_id =".$from_terminal_id."
					and
					fares.to_terminal_id in (
						SELECT distinct terminals.id FROM 
						terminals 
						join lgas on (terminals.lga_id=lgas.id)
						join cities on (lgas.city_id=cities.id)
						where 
						terminals.company_id=".$company_id."
						and cities.id=".$to_city_id."
					)
					order by schedule_times.departure_time";
		//echo "<br/>".$sql."<br />";
		$Results = DB::Select($sql);
		// limit 3 offset ".$page
		$Response = array();
		$row = 1;
		$nOfRecords = 1;
		//print_r($Results);
		//echo "$row>=$page && $nOfRecords<=3<br />";
		foreach($Results as $Result)
		{			
			if($row>$page && $nOfRecords<=4)
			{
				$info_Schedule = Schedule::FindOrFail($Result->sch_id);
				$dept_terminal = $info_Schedule->Schedule_time()->Orderby('sequence', 'desc')->First()->Terminal()->First()->name;
				$nOfRecords ++;
				$Response[] = array('rownum' => $row,
								'sch_id' => $Result->sch_id, 
								'fid' => $Result->fid,  
							 	'dept_time' => date('h:iA',strtotime($Result->dept_time)), 
								'service_name' => $Result->service_name,  
								'fare' => $Result->fare, 
								'terminal_name' => $Result->terminal_name,
								'dept_terminal' => $dept_terminal,
								);
			}
			$row++;
		}
		//print_r($Response);
		return $Response;
	}

	
	public function GetSelectedCompanySchedule($company_id,$schedule_date,$from_terminal_id,$to_city_id,$rownum)
	{
		$timestamp = strtotime($schedule_date);
		$sql = "SELECT 
					schedules.id as sch_id, fares.id as fid, 
					schedule_times.departure_time as dept_time, 
					services.short_name as service_name, 
					fares.fare as fare,
					terminals.name as terminal_name
					FROM 
					schedules 
					JOIN schedule_times ON (schedules.id=schedule_times.schedule_id)
					join fare_types on (schedules.id=fare_types.schedule_id) 
					join services on (fare_types.service_id=services.id) 
					join fares on (fare_types.id=fares.fare_type_id) 
					join terminals on (fares.from_terminal_id=terminals.id) 
					where 
					schedules.".strtolower(date('l', $timestamp))."=1  and schedule_times.sequence=1 and 
					schedules.operatingend_date>'".$schedule_date."' and
					fare_types.company_id=".$company_id." 
					and
					fares.from_terminal_id =".$from_terminal_id."
					and
					fares.to_terminal_id in (
						SELECT distinct terminals.id FROM 
						terminals 
						join lgas on (terminals.lga_id=lgas.id)
						join cities on (lgas.city_id=cities.id)
						where 
						terminals.company_id=".$company_id."
						and cities.id=".$to_city_id."
					)
					order by schedule_times.departure_time";
		//echo "<br/>".$sql."<br />";
		$Results = DB::Select($sql);
		// limit 3 offset ".$page
		$Response = array();
		$row = 1;
		foreach($Results as $Result)
		{			
			if($rownum==$row)
			{
				$Response[] = array('rownum' => $row,
								'sch_id' => $Result->sch_id, 
								'fid' => $Result->fid,  
							 	'dept_time' => $Result->dept_time, 
								'service_name' => $Result->service_name,  
								'fare' => $Result->fare, 
								'terminal_name' => $Result->terminal_name
								);
			}
			$row++;
		}
		//print_r($Response);
		return $Response;
	}
	
	public function getCompanyByUssd($ussd_code)
	{
		$info_Company_Ussd = Company_ussd::where('code',ltrim($ussd_code, '*'))->First();
		if($info_Company_Ussd)
			return Company::FindOrFail($info_Company_Ussd->company_id);
		else
			return null;
	}
	
	public function customerLog($msisdn, $ussd_code, $action)
	{
		$info_Customer_Log = new Customer_log;
		$info_Customer_Log->msisdn = $msisdn;
		$info_Customer_Log->ussd_code = $ussd_code;
		$info_Customer_Log->action = $action;
		$info_Customer_Log->save();
	}
	
	public function ChargeUser($msisdn, $ussd_code)
	{
		$info_Subscription_User = Subscription::where(['msisdn' => $msisdn, 'ussd_code' => $ussd_code, 'billing_date' => date('Y-m-d')])->First();
		
		if($info_Subscription_User)
		{
			return 1;
		}
		else
		{
			$info_Charge_Log = new Charging_log;
			$info_Charge_Log->msisdn = $msisdn;
			$info_Charge_Log->ussd_code = $ussd_code;
			$info_Charge_Log->billing_date = date('Y-m-d');
			$info_Charge_Log->amount = 10;
			$info_Charge_Log->is_charge = 1;
			$info_Charge_Log->save();
			
			$info_Subscription = new Subscription;
			$info_Subscription->msisdn = $msisdn;
			$info_Subscription->ussd_code = $ussd_code;
			$info_Subscription->billing_date = date('Y-m-d');
			$info_Subscription->amount = 10;
			$info_Subscription->save();
			
			return 1;
		}
	}
	
	public function emptyBalanceMessage()
	{
		$output['session_operation'] = "end";		
		$output['session_msg'] = "Sorry insufficient balance";
		
		return $output;
	}
	
	public function unAssignMessage()
	{
		$output['session_operation'] = "endcontinue";		
		$output['session_msg'] = "THIS CODE IS CURRENTLY UNASSIGNED TO A COMPANY.\n* Go to MAIN";
		return $output;
	}
	
	public function sendSMS($from = "88111", $to, $message, $smsc, $session_operation)
	{
		$message = rawurlencode($message);
		$to = trim($to);
		
		if($session_operation=="end")
		{
			$ussd_op = "?smpp?ussd_service_op=00";
		}
		else
		{
			$ussd_op = "?smpp?ussd_service_op=02";
		}
				
		//$metadata = rawurlencode($ussd_op);
		//echo "<br />";
		//$url = "http://localhost:16140/cgi-bin/sendsms?username=ussd&password=ussd&smsc=$smsc&from=$from&to=$to&text=$message&meta-data=$metadata";
		//echo "<br />";
		//$this->curl_min($url);

		//$arrContextOptions = array(
		//	"ssl" => array(
		//		"verify_peer" => false,
		//		"verify_peer_name" => false,
		//	),
		//);            
		//$urlresponse = file_get_contents($url, false, stream_context_create($arrContextOptions));
	}
	
	function curl_min($url)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}
}
