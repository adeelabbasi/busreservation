<?php

namespace App\Repositories;

use App\Models\Bus;
use App\Models\Schedule;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Hash;

class BusRepository {
	
	/**
	 * @var App\Models\Bus
	 */
	protected $db_bus;
	protected $db_schedule;
		
    public function __construct(Bus $db_bus, Schedule $db_schedule) 
    {
        $this->db_bus = $db_bus;
		$this->db_schedule = $db_schedule;
    }
	
	public function addBus($inputs)
    {
        $db_bus = $this->storeBus(new $this->db_bus ,  $inputs);
        return $db_bus;
    }
	
	public function updateBus($inputs, $id)
	{
		$db_bus = $this->db_bus->findOrFail($id);
		$answer_id = $this->storeBus($db_bus, $inputs, $id);
		return $answer_id;
	}
	
	public function deleteBus($id)
    {
		if($this->db_schedule->where(['bus_id' => $id])->Get()->Count()==0)
		{
			$db_bus = $this->db_bus->findOrFail($id);
			$db_bus->delete();
			return true;
		}
		else
		{
			return false; 
		}
    }

	function storeBus($db_bus , $inputs, $id = null)
	{	
		$db_bus->company_id = $inputs['company_id'];
		$db_bus->name = $inputs['name'];
		$db_bus->seats = $inputs['seats'];
		//Set status
		if(isset($inputs['status'])=="on")
			$db_bus->status = 1;
		else
			$db_bus->status = 0;
		$db_bus->save();
		return $db_bus;
	}
	
	public function getBus($id = null)
    {
		if($id==null)
		{
			$info_Bus = $this->db_bus->select('id', 'company_id', 'name', 'map', 'seats', 'status', 'created_at', 'updated_at')->orderBy('created_at', 'DESC')->get();
		}
		else
		{
			$info_Bus = $this->db_bus->select('id', 'company_id', 'name', 'map', 'seats', 'status', 'created_at', 'updated_at')->findOrFail($id);
		}
        return $info_Bus;
    }
	
	public function getCompanyBus($company_id)
    {
		$info_Bus = $this->db_bus->select('id', 'company_id', 'name', 'map', 'seats', 'status', 'created_at', 'updated_at')->where('company_id', '=', $company_id);
        return $info_Bus;
    }
}

