<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Repositories\UssdRepository;
use DB;

class FinalBookNewMessage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	
	private $userSession;
	private $session_from;
	private $session_msisdn;
	private $gateway;
	private $ussdRps;
	private $info_Date;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userSession, $session_from,$session_msisdn, $gateway, $info_Date)
    {
        $this->userSession = $userSession;
		$this->session_from = $session_from;
		$this->session_msisdn = $session_msisdn;
		$this->gateway = $gateway;
		$this->info_Date = $info_Date;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
		
		//Get discount
		$url = env('API_URL')."GeneralSetting/".$this->userSession->company_id;
		$options = array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>"Authorization: Bearer ".env('API_TOKEN')
		  )
		);
		$context = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
		$decodeData = json_decode($data, true);
		
		$url = env('API_URL')."getDiscountAmount/".$this->userSession->company_id."?DiscountCode=".$decodeData["data"]["DiscountCode"];
		$options = array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>"Authorization: Bearer ".env('API_TOKEN')
		  )
		);
		$context = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
		$decodeDataDiscount = json_decode($data, true);
		\Log::info("Call FinalBookNewMessage decodeDataDiscount: ".$data);
		//////////////
		
		$this->ussdRps = new UssdRepository();
		$perPage = 4;
        if($this->userSession->company_ussd_type == "1")
		{
			$info_Schedules = $this->ussdRps->GetCompanySchedule($this->userSession->company_id,$this->info_Date ,$this->userSession->from_terminal_id, $this->userSession->to_terminal_id,$this->userSession->to_city_id,$this->userSession->page_no, $perPage);
		}
		else
		{
			$info_Schedules = $this->ussdRps->GetCompanyScheduleAPI($this->userSession->company_id,$this->info_Date ,$this->userSession->from_terminal_id, $this->userSession->to_terminal_id,$this->userSession->to_city_id,$this->userSession->page_no, $perPage);
		}
		
		$menuSchedule = "";
		$menuScheduleCount=0;
		
		$i=1;
		foreach($info_Schedules as $info_Schedule)
		{
			if($i <= 3)
			{
				$ticketAmount = $info_Schedule['fare'];
				if($decodeDataDiscount["data"][0]["usePercent"] == "1")
				{
					$ticketAmount = $ticketAmount - ($info_Schedule['fare']*($decodeDataDiscount["data"][0]["couponpercent"]/100));
				}
				else
				{
					$ticketAmount = $info_Schedule['fare'] - $decodeDataDiscount["data"][0]["couponvalue"];
				}
				
				$menuScheduleCount++;
				$menuSchedule .= $info_Schedule['rownum'].", ".$info_Schedule['dept_time'].", ".$info_Schedule['bus'].", N".$ticketAmount.", Avail".chr(58)." ".$info_Schedule['AvailableSeatCounts']." Seats\n";
			}
			$i++;
		}
		$lastmessage = "";
		$sessionMessage = '';
		if($menuSchedule!="")
		{
			$info_Company = $this->userSession->Company()->First();
			$company_Ussd_Code = $info_Company->Company_ussd()->First()->code;
			$lastmessage = date_format(date_create($this->info_Date),"D d M Y")."\nSchedules".chr(58)." ".$info_Schedules[0]['terminal_name']."\nBus Found (".$menuScheduleCount.")\n".$menuSchedule;
			$output['session_msg'] = $info_Company->name."\n".$this->userSession->from_terminal_name." to ".$this->userSession->to_terminal_name."\n".date_format(date_create($this->info_Date),"D d M Y")."\nSchedules".chr(58)." ".$info_Schedules[0]['terminal_name']."\nBus Found (".$menuScheduleCount.")\n".$menuSchedule."\n\nDial *8011*8# to continue booking";
			//$output['session_msg'] = "Available Bus".chr(58).$this->userSession->from_terminal_name." to ".$this->userSession->to_terminal_name."\n".date_format(date_create($this->info_Date),"D d M Y")."\nPlease select\n".$menuSchedule."\nPlease, dial *8014*8# to choose a preferred bus";
			//$sessionMessage = "Available Bus".chr(58).$this->userSession->from_terminal_name." to ".$this->userSession->to_terminal_name."\n".date_format(date_create($this->info_Date),"D d M Y")."\nPlease select\n".$menuSchedule."\n* Back\n0 Main Menu";
			$sessionMessage = $output['session_msg'];
			
		}
		\Log::info("lastmessage ".$lastmessage);
		$this->userSession->schedule_date = $this->info_Date;
		
		$this->userSession->per_page=$perPage;
		$this->userSession->user_level=10;
		$this->userSession->keyword='SchMenu';
		//$this->userSession->c_parent_id=$this->session_msg;
		$this->userSession->is_child=1;
		$this->userSession->lastmessage=$lastmessage;
		
		$this->userSession->discount_code = $decodeDataDiscount["data"][0]["couponname"];
		$this->userSession->couponvalue = $decodeDataDiscount["data"][0]["couponvalue"];
		$this->userSession->usePercent = $decodeDataDiscount["data"][0]["usePercent"];
		$this->userSession->couponpercent = $decodeDataDiscount["data"][0]["couponpercent"];
		
		$this->userSession->save();
		\Log::info("Call FinalBookNewMessage menuSchedule: ".$menuSchedule);
		if($menuSchedule=="")
		{
			$action = "Schedule & Fare";
			if($this->userSession->ussd_code=="*".$this->session_from)
			{
				$action = "Transporters";
			}
			$this->ussdRps->updateCustomerLog('0', "update customer_logs set is_complete=0, action='$action', from_city_id='".$this->userSession->from_city_id."', from_city_name='".$this->userSession->from_city_name."', to_city_id='".$this->userSession->to_city_id."', to_city_name='".$this->userSession->to_city_name."' where id=".$this->userSession->session_id);
			
			$output['session_operation'] = "endcontinue";
			$output['session_msg'] = "Sorry no schedule available for this date";
			
			$smsMessage = $output['session_msg'];
			\Log::info("Call FinalBookNewMessage Send SMS None: ".$this->session_msisdn." Gateway:".$this->gateway);
			$this->ussdRps->sendSMS($this->session_from,$this->session_msisdn, $smsMessage, $this->gateway);
		}
		else
		{
			
			$smsMessage = $output['session_msg'];
			$this->ussdRps->sendSMS($this->session_from,$this->session_msisdn, $smsMessage, $this->gateway);
			\Log::info("Call FinalBookNewMessage Send SMS: ".$this->session_msisdn." Gateway:".$this->gateway);
			//$output['session_msg'] = "Thanks, your request has being received. You will receive SMS response shortly";
			$output['session_msg'] = "Thanks for choosing ".$this->userSession->Company()->First()->name.". You will receive SMS with Buses available shortly.";
									
			//Store data for booking next
			\Log::info("Call FinalBookNewMessage Search Query: INSERT INTO ussd_menu_schedule_search (msisdn, ussd_code, from_city_id, from_city_name, from_terminal_id, from_terminal_name, o_terminal_id, to_terminal_name, to_city_id, to_city_name, schedule_date, company_id, company_name, schedule_id, fare_id, discount_code, couponvalue, usePercent, couponpercent, ecord_on) select msisdn, ussd_code, from_city_id, from_city_name, from_terminal_id, from_terminal_name, to_terminal_id, to_terminal_name, to_city_id, to_city_name, schedule_date, ompany_id, company_name, schedule_id, fare_id, '".$decodeDataDiscount["data"][0]["couponname"]."' as discount_code, '".$decodeDataDiscount["data"][0]["couponvalue"]."' as couponvalue, ".$decodeDataDiscount["data"][0]["usePercent"]."' as usePercent, '".$decodeDataDiscount["data"][0]["couponpercent"]."' as couponpercent,  record_on from ussd_menu_session where msisdn = ".$this->session_msisdn."'");

			$ResultIns = DB::insert("INSERT INTO ussd_menu_schedule_search (msisdn, ussd_code, from_city_id, from_city_name, from_terminal_id, from_terminal_name, to_terminal_id, to_terminal_name, to_city_id, to_city_name, schedule_date, company_id, company_name, schedule_id, fare_id, discount_code, couponvalue, usePercent, couponpercent, record_on) select msisdn, ussd_code, from_city_id, from_city_name, from_terminal_id, from_terminal_name, to_terminal_id, to_terminal_name, to_city_id, to_city_name, schedule_date, company_id, company_name, schedule_id, fare_id, '".$decodeDataDiscount["data"][0]["couponname"]."' as discount_code, '".$decodeDataDiscount["data"][0]["couponvalue"]."' as couponvalue, '".$decodeDataDiscount["data"][0]["usePercent"]."' as usePercent, '".$decodeDataDiscount["data"][0]["couponpercent"]."' as couponpercent,  record_on from ussd_menu_session where msisdn = '".$this->session_msisdn."'");
			
		}
		\Log::info("Call FinalBookNewMessage job: ".$this->session_msisdn." Gateway:".$this->gateway);
    }
}
