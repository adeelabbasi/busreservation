<?php

namespace App\Http\Controllers\Ussd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\UssdRepository;
use App\Models\Ussd_menu_session;
use GuzzleHttp\Client;
use App\Models\Charging;
use App\Models\Charging_log;

class oldUssdController extends Controller
{
	protected $ussdRps;
	
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UssdRepository $ussdRps)
    { 
		$this->ussdRps = $ussdRps;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$session_msisdn = $request->session_msisdn;
		$session_operation = $request->session_operation; /* begin, continue, end */
		$session_type = $request->session_type; /* 1, 2, 3, 4 */
		$session_msg = trim($request->session_msg);
		$session_id = $request->session_id;
		$session_from = $request->session_from; /* shortcode */
		$moreKeyword = false;
		$backKeyword = false;
		$output = [];
		
		if (strcmp($session_operation, "begin") == 0)
		{
			/*$client = new Client(['verify' => false, 'debug' => true]);
			$options = [
					'headers' => [
						'username' => 'vendor5',
						'Authentication' => 'uZG9yNTp2ZW5kb3JwYXNzd2Q=',
						'Ocp-Apim-Subscription-Key' => 'un05a0ia0123449ae470di967371372c',
						],
					'form_params' => [
						'serviceName' => 'BusReservationSystem',
						'msisdn' => '48099444309',
						'id' => $session_id,
						'amount' => '10',
						],
                 ];
			print_r($options);
			$res = $client->request('POST', 'https://directbill.etisalat.com.ng/async', $options);
			echo $res->getStatusCode();
			// 200
			echo $res->getHeader('content-type');
			// 'application/json; charset=utf8'
			echo $res->getBody();
			
			return 123;*/
			$info_Company = $this->ussdRps->getCompanyByUssd($session_msg);
			if($info_Company)
			{
				$Charge = $this->ussdRps->ChargeUser($session_msisdn, $session_msg);
				if($Charge)
				{
					$output = $this->ussdRps->companyLevel1($session_msisdn,$session_operation,$session_type,$session_msg,$session_id,$session_from,$info_Company->name,$info_Company->id);
				}
				else
				{
					$output = $this->ussdRps->emptyBalanceMessage($session_id);
				}
			}
			else
			{
				$Charge = $this->ussdRps->ChargeUser($session_msisdn, "*".$session_from);
				if($Charge)
				{
					$output = $this->ussdRps->Level1($session_msisdn,$session_operation,$session_type,$session_msg,$session_id,"*".$session_from);
				}
				else
				{
					$output = $this->ussdRps->emptyBalanceMessage($session_id);
				}
			}
			
			/*$info_Company = $this->ussdRps->getCompanyByUssd($session_msg);
			if($info_Company)
			{
				$output = $this->ussdRps->companyLevel1($session_msisdn,$session_operation,$session_type,$session_msg,$session_id,$session_from,$info_Company->name,$info_Company->id);
			}
			else
			{
				$output = $this->ussdRps->Level1($session_msisdn,$session_operation,$session_type,$session_msg,$session_id,$session_from);
			}*/
			
		} 
		elseif (strcmp($session_operation, "continue") == 0)
		{
			$userSession = Ussd_menu_session::Where('msisdn',$session_msisdn)->First();
			
			//echo "<br />";
			//print_r($userSession->toArray());
			//echo"<br />";
			
			$menuLevel = $userSession->user_level;
			if($session_msg=="*")
			{
				//If back add back=ture
				$backKeyword = true;
				if($userSession->page_no>0)
				{
					$session_msg = $userSession->last_parent_id;
					$userSession->page_no = $userSession->page_no-$userSession->per_page;
					$menuLevel = $userSession->user_level-1;
				}
				else
				{
					$session_msg = $userSession->last_parent_id;
					$userSession->page_no = 0;
					$menuLevel = $userSession->user_level-2;
				}
				//Move to prev level
				//echo "<br />";
				//echo "Back: ".$menuLevel;
				//echo "<br />";
			}
			
			if($userSession->more_keyword == $session_msg)
			{
				//If more add more=ture
				$moreKeyword = true;
				//Set last parent keyword
				//If Transporters menu
				if($userSession->c_parent==0)
					$session_msg = $userSession->s_parent;
				else
					$session_msg = $userSession->c_parent;
					
				$menuLevel = $userSession->user_level-1;
				//Move to same level
				//echo "<br />";
				//echo "More: ".$menuLevel;
				//echo "<br />";
			}

			switch($menuLevel)
			{
				case 0:
					$output = $this->ussdRps->Level1($session_msisdn,$session_operation,$session_type,$session_msg,$session_id,$session_from);
				break;
				case 1:
					$output = $this->ussdRps->Level2($userSession,$session_msisdn,$session_operation,$session_type,$session_msg,$session_id,$session_from,$moreKeyword,$backKeyword);
				break;
				case 2:
					$output = $this->ussdRps->Level3($userSession,$session_msisdn,$session_operation,$session_type,$session_msg,$session_id,$session_from,$moreKeyword,$backKeyword);
				break;
				case 3:
					$output = $this->ussdRps->Level4($userSession,$session_msisdn,$session_operation,$session_type,$session_msg,$session_id,$session_from,$moreKeyword,$backKeyword);
				break;
				case 4:
					$output = $this->ussdRps->Level5($userSession,$session_msisdn,$session_operation,$session_type,$session_msg,$session_id,$session_from,$moreKeyword,$backKeyword);
				break;
				case 5:
					$output = $this->ussdRps->Level6($userSession,$session_msisdn,$session_operation,$session_type,$session_msg,$session_id,$session_from,$moreKeyword,$backKeyword);
				break;
				case 6:
					$output = $this->ussdRps->Level7($userSession,$session_msisdn,$session_operation,$session_type,$session_msg,$session_id,$session_from,$moreKeyword,$backKeyword);
				break;
				case 7:
					$output = $this->ussdRps->Level8($userSession,$session_msisdn,$session_operation,$session_type,$session_msg,$session_id,$session_from,$moreKeyword,$backKeyword);
				break;
				case 8:
					$output = $this->ussdRps->Level9($userSession,$session_msisdn,$session_operation,$session_type,$session_msg,$session_id,$session_from,$moreKeyword,$backKeyword);
				break;
				case 9:
					$output = $this->ussdRps->Level10($userSession,$session_msisdn,$session_operation,$session_type,$session_msg,$session_id,$session_from,$moreKeyword,$backKeyword);
				break;
			}
			
			
			//echo "<br />";
			//print_r($userSession->toArray());
			//echo"<br />";
		}
		return $output;
    }
}
