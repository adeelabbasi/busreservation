<?php

namespace App\Http\Controllers\Ussd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\UssdRepository;
use App\Models\Ussd_menu_session;
use GuzzleHttp\Client;
use App\Models\Charging;
use App\Models\Charging_log;
use Carbon\Carbon;
use DB;

class UssdController extends Controller
{
	protected $ussdRps;
	
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UssdRepository $ussdRps)
    { 
		$this->ussdRps = $ussdRps;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$moreKeyword = false;
		$backKeyword = false;
		$output = [];
		$session_msisdn = $request->msisdn;
		$session_from = $request->shortcode;
		$session_msg = $request->message;
		$gateway = isset($request->gateway) ? $request->gateway : '';
		$smsc = $request->smsc;
		
		$testing = $request->testing;
		
		$userSession = Ussd_menu_session::Where('msisdn',$session_msisdn)->First();
		
		$ussdParts = explode("*",$session_msg);
		
		//dd($userSession);
		//if(!$userSession || strpos($session_msg,$session_from)>0)
		if(!$userSession || $userSession->is_delete == "1" || (strpos($session_msg,$session_from)>0 && $userSession->user_level == "1") || ($session_msg == "99" && $userSession->user_level == "3"))
		{
			if(($session_msg == "99" && $userSession->user_level == "3"))
			{
				$info_Company = $userSession->Company()->First();
			}
			else
			{
				$info_Company = $this->ussdRps->getCompanyByUssd('*'.$ussdParts[1].'*'.$ussdParts[2]);
			}
			//dd($session_msg);
			//Third string will be for BUS no.
			
			if(isset($ussdParts[3]) && $ussdParts[3] !="")
			{
				
				$info_CompanyPageNo = $this->ussdRps->GetCompanyPageNo($info_Company->id);
				$session_id = $this->ussdRps->addCustomerLog($testing, $session_msisdn, $session_msg, 0, $info_CompanyPageNo[0]->id, $info_CompanyPageNo[0]->name);
				
				$ResultDel = DB::delete("delete from ussd_menu_session where msisdn = '$session_msisdn'");
				$ResultIns = DB::insert("INSERT INTO ussd_menu_session (session_id, msisdn, ussd_code, user_level, keyword, parent_id, last_parent_id, g_parent, s_parent, is_child, c_parent_id, company_id, company_name, company_ussd_type, company_api_url, company_api_token, level_4, record_on) VALUES ('$session_id', '$session_msisdn', '$session_msg', '4', '$session_msg', '".$info_CompanyPageNo[0]->rownum."', '2', '1', '1', '1', '".$info_CompanyPageNo[0]->rownum."' ,'".$info_Company->id."','".$info_CompanyPageNo[0]->name."', '".$info_CompanyPageNo[0]->ussd_type."', '".$info_CompanyPageNo[0]->api_url."', '".$info_CompanyPageNo[0]->api_token."', '".$info_CompanyPageNo[0]->rownum."', CURRENT_TIMESTAMP)");
				
				
				$userSession = Ussd_menu_session::Where('msisdn',$session_msisdn)->First();
				$userSession->msisdn = $session_msisdn;
				$userSession->record_on = date('Y-m-d');
				$info_Search = $this->ussdRps->GetLastSelectedScheduleSearch($session_msisdn,$ussdParts[3]);
				//dd($info_Search);
				$userSession->from_city_id = $info_Search[0]->from_city_id;
				$userSession->from_city_name = $info_Search[0]->from_city_name;
				$userSession->from_terminal_id = $info_Search[0]->from_terminal_id;
				$userSession->from_terminal_name = $info_Search[0]->from_terminal_name;
				$userSession->to_city_id = $info_Search[0]->to_city_id;
				$userSession->to_city_name = $info_Search[0]->to_city_name;
				$userSession->to_terminal_id = $info_Search[0]->to_terminal_id;
				$userSession->to_terminal_name = $info_Search[0]->to_terminal_name;
				$userSession->schedule_date = $info_Search[0]->schedule_date;
				$userSession->company_id = $info_Search[0]->company_id;
				$userSession->company_name = $info_Search[0]->company_name;
				
				$userSession->discount_code = $info_Search[0]->discount_code;
				$userSession->couponvalue = $info_Search[0]->couponvalue;
				$userSession->usePercent = $info_Search[0]->usePercent;
				$userSession->couponpercent = $info_Search[0]->couponpercent;
				
				
				$userSession->per_page=4;				
				$userSession->user_level=4;
				$userSession->level_6=1;
				$userSession->level_7=$ussdParts[3];
				$userSession->c_parent=2;
				$userSession->keyword=$ussdParts[3];
				$userSession->c_parent_id=$ussdParts[3];
				$userSession->is_child=1;
				$userSession->save();
				
				$session_msg = $ussdParts[3];
				
				$output = $this->ussdRps->Level5($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing);
			}
			else
			{
				$output = $this->ussdRps->Level1($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$info_Company,$testing);
			}
		} 
		else
		if(isset($ussdParts[3]) && $ussdParts[3] !="")
		{
			$info_Company = $this->ussdRps->getCompanyByUssd('*'.$ussdParts[1].'*'.$ussdParts[2]);
			$info_CompanyPageNo = $this->ussdRps->GetCompanyPageNo($info_Company->id);
			$session_id = $this->ussdRps->addCustomerLog($testing, $session_msisdn, $session_msg, 0, $info_CompanyPageNo[0]->id, $info_CompanyPageNo[0]->name);
			
			$ResultDel = DB::delete("delete from ussd_menu_session where msisdn = '$session_msisdn'");
			$ResultIns = DB::insert("INSERT INTO ussd_menu_session (session_id, msisdn, ussd_code, user_level, keyword, parent_id, last_parent_id, g_parent, s_parent, is_child, c_parent_id, company_id, company_name, company_ussd_type, company_api_url, company_api_token, level_4, record_on) VALUES ('$session_id', '$session_msisdn', '$session_msg', '4', '$session_msg', '".$info_CompanyPageNo[0]->rownum."', '2', '1', '1', '1', '".$info_CompanyPageNo[0]->rownum."' ,'".$info_Company->id."','".$info_CompanyPageNo[0]->name."', '".$info_CompanyPageNo[0]->ussd_type."', '".$info_CompanyPageNo[0]->api_url."', '".$info_CompanyPageNo[0]->api_token."', '".$info_CompanyPageNo[0]->rownum."', CURRENT_TIMESTAMP)");
			
			
			$userSession = Ussd_menu_session::Where('msisdn',$session_msisdn)->First();
			$userSession->msisdn = $session_msisdn;
			$userSession->record_on = date('Y-m-d');
			$info_Search = $this->ussdRps->GetLastSelectedScheduleSearch($session_msisdn,$ussdParts[3]);
			//dd($info_Search);
			$userSession->from_city_id = $info_Search[0]->from_city_id;
			$userSession->from_city_name = $info_Search[0]->from_city_name;
			$userSession->from_terminal_id = $info_Search[0]->from_terminal_id;
			$userSession->from_terminal_name = $info_Search[0]->from_terminal_name;
			$userSession->to_city_id = $info_Search[0]->to_city_id;
			$userSession->to_city_name = $info_Search[0]->to_city_name;
			$userSession->to_terminal_id = $info_Search[0]->to_terminal_id;
			$userSession->to_terminal_name = $info_Search[0]->to_terminal_name;
			$userSession->schedule_date = $info_Search[0]->schedule_date;
			$userSession->company_id = $info_Search[0]->company_id;
			$userSession->company_name = $info_Search[0]->company_name;

			$userSession->discount_code = $info_Search[0]->discount_code;
			$userSession->couponvalue = $info_Search[0]->couponvalue;
			$userSession->usePercent = $info_Search[0]->usePercent;
			$userSession->couponpercent = $info_Search[0]->couponpercent;
			
			
			$userSession->per_page=4;				
			$userSession->user_leve4;
			$userSession->level_6=1;
			$userSession->level_7=$ussdParts[3];
			$userSession->c_parent=2;
			$userSession->keyword=$ussdParts[3];
			$userSession->c_parent_id=$ussdParts[3];
			$userSession->is_child=1;
			$userSession->save();
			
			$session_msg = $ussdParts[3];
			
			$output = $this->ussdRps->Level5($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing);
		}
		else
		{
			
			//If new ussd call then set previod level
			if(strpos($session_msg,$session_from)>0)
			{
				$level = "level_".$userSession->user_level;
				$session_msg = $userSession->$level;
				$userSession->user_level = $userSession->user_level-1;
				if($userSession->user_level > 4)
				{
					
				}
				//$session_msg='*';
				//dd($session_msg);	
			}
			
			 //echo "<br />";
			 //print_r($userSession->toArray());
			// echo"<br />";
			
			$menuLevel = $userSession->user_level;
			//dd($session_msg);
			if($session_msg=="*")
			{
				//If back add back=ture
				$backKeyword = true;
				if($userSession->page_no>0)
				{
					
					//if($userSession->c_parent!=0 && $userSession->user_level>4)
					//	$session_msg = $userSession->c_parent_id;
					//else
					//	$session_msg = $userSession->parent_id;
					
					$userSession->page_no = $userSession->page_no-$userSession->per_page;
					$menuLevel = $userSession->user_level-1;
					
				}
				else
				{
					//if($userSession->c_parent!=0 && $userSession->user_level>4)
					//	$session_msg = $userSession->c_last_parent_id;
					//else
					//	$session_msg = $userSession->last_parent_id;
					
					
					$userSession->page_no = 0;
					$menuLevel = $userSession->user_level-2;
					
					
					if($userSession->last_skip_level==11 && $userSession->level_3 == "1")
					{
						$menuLevel = $userSession->user_level-5;
						//dd($menuLevel);
					}
				}

				//Select last selected keyword
				switch($menuLevel)
				{
					case 1:
						$session_msg = $userSession->level_2;
					break;
					case 2:
						$session_msg = $userSession->level_3;
					break;
					case 3:
						$session_msg = $userSession->level_4;
					break;
					case 4:
						$session_msg = $userSession->level_5;
					break;
					case 5:
						$session_msg = $userSession->level_6;
					break;
					case 6:
						$session_msg = $userSession->level_7;
					break;
					case 7:
						$session_msg = $userSession->level_8;
					break;
					case 8:
						$session_msg = $userSession->level_9;
					break;
					case 9:
						$session_msg = $userSession->level_10;
					break;
					case 10:
						$session_msg = $userSession->level_11;
					break;
					case 11:
						$session_msg = $userSession->level_12;
					break;
					case 12:
						$session_msg = $userSession->level_13;
					break;
				}
				//Move to prev level
				//echo "<br />";
				//echo "Back: ".$menuLevel;
				//echo "<br />";
			}
			$userSession->more_keyword = ($userSession->more_keyword==0?NULL:$userSession->more_keyword);
			if($userSession->more_keyword == $session_msg)
			{
				//If more add more=ture
				$moreKeyword = true;
				//Set last parent keyword
				//If Transporters menu
				if($userSession->c_parent!=0)
					$session_msg = $userSession->c_parent;
					
				$menuLevel = $userSession->user_level-1;
				//Move to same level
				//echo "<br />";
				//echo "More: ".$menuLevel;
				//echo "<br />";
			}
			
			$this->ussdRps->updateCustomerLog($testing, "update customer_logs set end_time = '".Carbon::now()."' where id=".$userSession->session_id);
			
			//echo "<br />$session_msg<br />";
			//dd($session_msg);
			switch($menuLevel)
			{
				case 0:
					$output = $this->ussdRps->Level1($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$userSession->Company()->First(),$testing);
				break;
				case 1:
					$output = $this->ussdRps->Level2($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing);
				break;
				case 2:
					$output = $this->ussdRps->Level3($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing);
				break;
				case 3:
					$output = $this->ussdRps->Level4($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing);
				break;
				case 4:
					$output = $this->ussdRps->Level5($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing);
				break;
				case 5:
					$output = $this->ussdRps->Level6($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing);
				break;
				case 6:
					$output = $this->ussdRps->Level7($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing);
				break;
				case 7:
					$output = $this->ussdRps->Level8($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing);
				break;
				case 8:
					$output = $this->ussdRps->Level9($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing);
				break;
				case 9:
					$output = $this->ussdRps->Level10($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing);
				break;
				case 10:
					$output = $this->ussdRps->Level11($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing);
				break;
				case 11:
					$output = $this->ussdRps->Level12($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing);
				break;
				case 12:
					$output = $this->ussdRps->Level13($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$gateway,$testing);
				break;
			}
			//echo "<br />";
			//print_r($userSession->toArray());
			//echo"<br />";
		}
		
		if($output['session_operation'] == "end")
		{
			if(isset($userSession))
			{
				$userSession->delete();
			}
		}
		
		if($output['session_operation'] == "endcontinue")
		{
			try
			{
				//Set level -1 mean last session has been ended
				if(isset($userSession))
				{
					$userSession->is_delete = "1";
					$userSession->save();
					//$userSession->delete();
				}
			}
			catch(Exception $e){}
			//$output['session_operation'] = "continue";
		}
		
		return $output;
    }
}
