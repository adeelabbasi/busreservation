<?php

namespace App\Http\Controllers\Ussd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\UssdRepository;
use App\Models\Ussd_menu_session;
use GuzzleHttp\Client;
use App\Models\Charging;
use App\Models\Charging_log;
use Carbon\Carbon;

class UssdController extends Controller
{
	protected $ussdRps;
	
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UssdRepository $ussdRps)
    { 
		$this->ussdRps = $ussdRps;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		/*$data = array(
			"serviceName" => "ITRVL10",
			"msisdn" => "08099444309",
			"id" => "ITRVL10-1519932277-6152090",
			"amount" => "1000"
		);
		 
		$payload = json_encode($data);
		 
		// Prepare new cURL resource
		$ch = curl_init('https://directbillstage.9mobile.com.ng/stg/asyncbilling');

		curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS
		curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1);  // RETURN THE CONTENTS OF THE CALL

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLINFO_HEADER_OUT, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

		//needed for https
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		 
		// Set HTTP Header for POST request 
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			"Accept: application/json",
			"Content-Type: application/json",
			"username: IYCONSOFT",
			"Authorization: 1599D72D31758321739570C98C439AF91FE1A7EF72A9B440D8E379EC5B705552",
			"Ocp-Apim-Subscription-Key: deb3c7c967484adc94ad8852dcd80b22"
			)
		);
		 
		// Submit the POST request
		$result = curl_exec($ch);
		 
		// Close cURL session handle
		curl_close($ch);
		return $result;*/



		$moreKeyword = false;
		$backKeyword = false;
		$output = [];
		$session_msisdn = $request->msisdn;
		$session_from = $request->shortcode;
		$session_msg = $request->message;
		$smsc = $request->smsc;
		
		$testing = $request->testing;
		
		$userSession = Ussd_menu_session::Where('msisdn',$session_msisdn)->First();
		
		
		if(!$userSession || strpos($session_msg,env("SHORT_CODE"))>0)
		{
			if(1==0) //!$this->ussdRps->checkUserSubscription($session_msisdn)) //If not charge send confirm balance message
			{
				$output = $this->ussdRps->balanceConfirmationMessage($session_msisdn,$session_msg,$session_from);
			}
			else //Send 1st menu
			{
				if($session_msg=="*".env("SHORT_CODE") || $session_msg=='*')
				{
					if($testing!="")
					{
						$output = $this->ussdRps->Level1($session_msisdn,$session_msg,"*".$session_from,$testing);
					}
					else
					{
						$output = $this->ussdRps->Level1($session_msisdn,$session_msg,"*".$session_from,$testing);
					}
				}
				else
				{
					$info_Company = $this->ussdRps->getCompanyByUssd($session_msg);
					if($info_Company)
					{
						if($testing!="")
						{
							$output = $this->ussdRps->companyLevel1($session_msisdn,$session_msg,$session_from,$info_Company->name,$info_Company->id,$testing);
						}
						else
						{
							$output = $this->ussdRps->companyLevel1($session_msisdn,$session_msg,$session_from,$info_Company->name,$info_Company->id,$testing);
						}
					}
					else
					{
						$output = $this->ussdRps->unAssignMessage($session_msisdn,$session_msg,$testing);
					}
				}
			}
		} 
		else
		/*if($session_msg == "*".env("SHORT_CODE")."*Y" || $userSession->keyword == "balMsg") //If balance message confirm
		{
			if($session_msg <> "1")
			{
				$output['session_operation'] = "continue";		
				$output['session_msg'] = "Please press 1 to confirm";
			}
			else
			if($userSession->ussd_code == '*'.env("SHORT_CODE") || $session_msg=='*')
			{
				if($testing!="")
				{
					$output = $this->ussdRps->Level1($session_msisdn,$session_msg,"*".$session_from,$testing);
				}
				else
				{
					$Charge = $this->ussdRps->ChargeUser($session_msisdn, "*".$session_from);
					if($Charge != 0)
					{
						if($Charge == 1)
						{
							$message = "Hello, your request for the ".env("SHORT_CODE")." Travel service was successful @ N20/day. Thank you. Please dial $session_msg# to continue";
							//$this->ussdRps->sendSMS(env("SHORT_CODE"),$session_msisdn, $message);
						}
						$output = $this->ussdRps->Level1($session_msisdn,$session_msg,"*".$session_from,$testing);
					}
					else
					{
						$output = $this->ussdRps->emptyBalanceMessage();
					}
				}
			}
			else
			{
				$info_Company = $this->ussdRps->getCompanyByUssd($userSession->ussd_code);
				if($info_Company)
				{
					if(isset($request->testing))
					{
						$output = $this->ussdRps->companyLevel1($session_msisdn,$session_msg,$session_from,$info_Company->name,$info_Company->id,$testing);
					}
					else
					{
						$Charge = $this->ussdRps->ChargeUser($session_msisdn, $session_msg);
						if($Charge != 0)
						{
							if($Charge == 1)
							{
								$message = "Hello, your request for the ".env("SHORT_CODE")." Travel service was successful @ N20/day. Thank you. Please dial $session_msg# to continue";
								//$this->ussdRps->sendSMS(env("SHORT_CODE"),$session_msisdn, $message);
							}
							$output = $this->ussdRps->companyLevel1($session_msisdn,$session_msg,$session_from,$info_Company->name,$info_Company->id,$testing);
						}
						else
						{
							$output = $this->ussdRps->emptyBalanceMessage();
						}
					}
				}
				else
				{
					$output = $this->ussdRps->unAssignMessage($session_msisdn,$session_msg,$testing);
				}
			}
		}
		else*/
		if($userSession->is_delete == "1" && $session_msg=="0")
		{
			//If last session ended get last ussd code
			$session_msg = $userSession->ussd_code;
			
			
			if($session_msg=="*".env("SHORT_CODE") || $session_msg=='*')
			{
				if($testing!="")
				{
					$output = $this->ussdRps->Level1($session_msisdn,$session_msg,"*".$session_from,$testing);
				}
				else
				{
					$output = $this->ussdRps->Level1($session_msisdn,$session_msg,"*".$session_from,$testing);
				}
			}
			else
			{
				$info_Company = $this->ussdRps->getCompanyByUssd($session_msg);
				if($info_Company)
				{
					if($testing!="")
					{
						$output = $this->ussdRps->companyLevel1($session_msisdn,$session_msg,$session_from,$info_Company->name,$info_Company->id,$testing);
					}
					else
					{
						$output = $this->ussdRps->companyLevel1($session_msisdn,$session_msg,$session_from,$info_Company->name,$info_Company->id,$testing);
					}
				}
				else
				{
					$output = $this->ussdRps->unAssignMessage($session_msisdn,$session_msg,$testing);
				}
			}
		}
		else
		{
			//echo "<br />";
			//print_r($userSession->toArray());
			//echo"<br />";
			
			$menuLevel = $userSession->user_level;
			if($session_msg=="*")
			{
				//If back add back=ture
				$backKeyword = true;
				if($userSession->page_no>0)
				{
					
					//if($userSession->c_parent!=0 && $userSession->user_level>4)
					//	$session_msg = $userSession->c_parent_id;
					//else
					//	$session_msg = $userSession->parent_id;
					
					$userSession->page_no = $userSession->page_no-$userSession->per_page;
					$menuLevel = $userSession->user_level-1;
					
				}
				else
				{
					//if($userSession->c_parent!=0 && $userSession->user_level>4)
					//	$session_msg = $userSession->c_last_parent_id;
					//else
					//	$session_msg = $userSession->last_parent_id;
						
					$userSession->page_no = 0;
					$menuLevel = $userSession->user_level-2;
					
					//Set selected company rownum
					if($userSession->c_parent!=0 && $userSession->user_level==5)
					{
						$info_Company = $this->ussdRps->GetCompanyPageNo($userSession->company_id);
						$session_msg = $info_Company[0]->rownum;
					}
				}

				//Select last selected keyword
				switch($menuLevel)
				{
					case 1:
						$session_msg = $userSession->level_2;
					break;
					case 2:
						$session_msg = $userSession->level_3;
					break;
					case 3:
						$session_msg = $userSession->level_4;
					break;
					case 4:
						$session_msg = $userSession->level_5;
					break;
					case 5:
						$session_msg = $userSession->level_6;
					break;
					case 6:
						$session_msg = $userSession->level_7;
					break;
					case 7:
						$session_msg = $userSession->level_8;
					break;
					case 8:
						$session_msg = $userSession->level_9;
					break;
					case 9:
						$session_msg = $userSession->level_10;
					break;
					case 10:
						$session_msg = $userSession->level_11;
					break;
					case 11:
						$session_msg = $userSession->level_12;
					break;
					/*case 12:
						$session_msg = $userSession->level_13;
					break;
					case 13:
						$session_msg = $userSession->level_14;
					break;*/
				}
				//Move to prev level
				//echo "<br />";
				//echo "Back: ".$menuLevel;
				//echo "<br />";
			}
			$userSession->more_keyword = ($userSession->more_keyword==0?NULL:$userSession->more_keyword);
			if($userSession->more_keyword == $session_msg)
			{
				//If more add more=ture
				$moreKeyword = true;
				//Set last parent keyword
				//If Transporters menu
				if($userSession->c_parent!=0 && $userSession->user_level>4)
					$session_msg = $userSession->c_parent;
				else
					$session_msg = $userSession->s_parent;
					
				$menuLevel = $userSession->user_level-1;
				//Move to same level
				//echo "<br />";
				//echo "More: ".$menuLevel;
				//echo "<br />";
			}
			
			$this->ussdRps->updateCustomerLog($testing, "update customer_logs set end_time = '".Carbon::now()."' where id=".$userSession->session_id);
			
			//echo "<br />$session_msg<br />";
			switch($menuLevel)
			{
				case 0:
					$output = $this->ussdRps->Level1($session_msisdn,$session_msg,$session_from,$testing);
				break;
				case 1:
					$output = $this->ussdRps->Level2($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$testing);
				break;
				case 2:
					$output = $this->ussdRps->Level3($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$testing);
				break;
				case 3:
					$output = $this->ussdRps->Level4($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$testing);
				break;
				case 4:
					$output = $this->ussdRps->Level5($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$testing);
				break;
				case 5:
					$output = $this->ussdRps->Level6($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$testing);
				break;
				case 6:
					$output = $this->ussdRps->Level7($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$testing);
				break;
				case 7:
					$output = $this->ussdRps->Level8($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$testing);
				break;
				case 8:
					$output = $this->ussdRps->Level9($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$testing);
				break;
				case 9:
					$output = $this->ussdRps->Level10($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$testing);
				break;
				case 10:
					$output = $this->ussdRps->Level11($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$testing);
				break;
				case 11:
					$output = $this->ussdRps->Level12($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$testing);
				break;
				case 12:
					$output = $this->ussdRps->Level13($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$testing);
				break;
				case 13:
					$output = $this->ussdRps->Level14($userSession,$session_msisdn,$session_msg,$session_from,$moreKeyword,$backKeyword,$testing);
				break;
			}
			//echo "<br />";
			//print_r($userSession->toArray());
			//echo"<br />";
		}
		
		if($output['session_operation'] == "end")
		{
			if(isset($userSession))
			{
				$userSession->delete();
			}
		}
		
		if($output['session_operation'] == "endcontinue")
		{
			try
			{
				//Set level -1 mean last session has been ended
				if(isset($userSession))
				{
					$userSession->is_delete = "1";
					$userSession->save();
				}
			}
			catch(Exception $e){}
			$output['session_operation'] = "continue";
		}
		
		/*if($output["session_msg"]!="")
		{
			$message = str_replace("\n", chr(13),$output["session_msg"]);
			if($testing=="")
			{
				$this->ussdRps->sendSMS("88111", $session_msisdn, $message, $smsc, $output['session_operation']);
			}
		}*/
		
		return $output;
    }
}
