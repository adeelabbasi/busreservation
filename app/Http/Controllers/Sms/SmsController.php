<?php

namespace App\Http\Controllers\Sms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\UssdRepository;


class SmsController extends Controller
{
	protected $ussdRps;
	
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UssdRepository $ussdRps)
    { 
		$this->ussdRps = $ussdRps;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$from = $request->from;
		$msisdn = $request->msisdn;
		$keyword = strtoupper($request->keyword);
		
		switch($keyword)
		{
			case "GO":
				$Charge = $this->ussdRps->ChargeUser($msisdn, "*".$from);
				if($Charge == "2")
				{
					$message = "You are already subscribed to the service! Please Dial *".env("SHORT_CODE")."# to enjoy the service";
				}
				if($Charge == "1")
				{
					$message = "Hello, your request for the ".env("SHORT_CODE")." Travel service was successful @ N20/day. Thank you. Please dial *".env("SHORT_CODE")."# to continue";
				}
				else
				{
					$message = "Sorry, You have insufficient airtime for this service. Service cost @ N20/day Please recharge and dial *".env("SHORT_CODE")."# or Send HELP to ".env("SHORT_CODE");
				}
				
			break;
			
			case "HELP":
				$message = "".env("SHORT_CODE")." HELP For One day access to ".env("SHORT_CODE")." Travel Service Text GO to ".env("SHORT_CODE").". Service Cost N20/day.";
			break;
			
			default:
				$message = "You have sent an invalid keyword. Kindly send  HELP to ".env("SHORT_CODE")." for more details about our services";
			break;
			
		}
		
		if($message != "")
		{
			$this->ussdRps->sendSMS($from, $msisdn, $message);
		}
		return "";
    }
}
