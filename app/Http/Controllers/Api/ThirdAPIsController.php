<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Repositories\ApiRepository;
use Illuminate\Http\Request;
use App\Models\Company;
use App\Models\ApiCache;
use Carbon\Carbon;


class ThirdAPIsController extends Controller
{
	protected $apiRps;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ApiRepository $apiRps)
    {
  		$this->apiRps = $apiRps;
    }
	
	public function UpdateCache($CompanyID, $APIMethod ,$Response)
	{
		$db_ApiCache = ApiCache::Where('name', $APIMethod)->Where('company_id',$CompanyID)->First();
		if($db_ApiCache)
		{
			$db_ApiCache->response = $Response;
			$db_ApiCache->created_at = date('Y-m-d H:i:s');
			$db_ApiCache->save();
		}
		else
		{
			$db_ApiCache = new ApiCache;
			$db_ApiCache->name = $APIMethod;
			$db_ApiCache->company_id = $CompanyID;
			$db_ApiCache->response = $Response;
			$db_ApiCache->created_at = date('Y-m-d H:i:s');
			$db_ApiCache->save();
		}
	}
	
	public function GetCache($CompanyID, $APIMethod)
	{
		$db_ApiCache = ApiCache::Where('name', $APIMethod)->Where('company_id',$CompanyID)->WhereDate('created_at', Carbon::today())->First();
		if($db_ApiCache)
		{
			return $db_ApiCache->response;
		}
		return '';
	}
	
	public function TerminalStates($CompanyID)
    {
		$data = $this->GetCache($CompanyID, 'TerminalStates');
		if($data == '')
		{
			$info_Company = Company::Where('id',$CompanyID)->First();
			
			$url = $info_Company->api_url."TerminalStates";
			$options = array(
			  'http'=>array(
				'method'=>"GET",
				'header'=>"Authorization: Bearer ".$info_Company->api_token,
			  )
			);
			
			$context = stream_context_create($options);
			$data = file_get_contents($url, false, $context);
			
			$this->UpdateCache($CompanyID, 'TerminalStates', $data);
		}

		//return $decodeData = json_decode($data, true);
		return $data;
    }
	
	public function AllRouteDetails($CompanyID)
    {
		$data = $this->GetCache($CompanyID, 'AllRouteDetails');
		if($data == '')
		{
			$info_Company = Company::Where('id',$CompanyID)->First();
			
			$url = $info_Company->api_url."AllRouteDetails";
			$options = array(
			  'http'=>array(
				'method'=>"GET",
				'header'=>"Authorization: Bearer ".$info_Company->api_token,
			  )
			);
			
			$context = stream_context_create($options);
			$data = file_get_contents($url, false, $context);
			$this->UpdateCache($CompanyID, 'AllRouteDetails', $data);
		}
		//return $decodeData = json_decode($data, true);
		return $data;
	}
	
	public function GeneralSetting($CompanyID)
    {
		$info_Company = Company::Where('id',$CompanyID)->First();
		
		$url = $info_Company->api_url."GeneralSetting"; 
		$options = array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>"Authorization: Bearer ".$info_Company->api_token,
		  )
		);
		
		$context = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
		
		//return $decodeData = json_decode($data, true);
		return $data;
	}
	
	public function TerminalsWithState($CompanyID)
    {
		$info_Company = Company::Where('id',$CompanyID)->First();
		
		$url = $info_Company->api_url."TerminalsWithState";
		$options = array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>"Authorization: Bearer ".$info_Company->api_token,
		  )
		);
		
		$context = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
		
		//return $decodeData = json_decode($data, true);
		return $data;
	}
	
	public function AvaiableBusWithSeatPrice(Request $request, $CompanyID)
    {
		/*return '{"code":1,"status":200,"data":{"RefCode":"63fb45a9","BusList":[{"TripID":"289596","TripNo":"450289596","Trip_Date":"2020-02-10 00:00:00","BookingPeriodID":"1","Departure_Time":"2020-02-10 07:30:00","isInterRoute":"0","VehicleGroupTagID":"3","VIN":"1st Bus (15)","ModelID":"11","Loading_Office":"UMUAHIA","DestinationID":"63","Destination":"LAGOS - AJAH","BookingPeriod":"Morning","VehicleGroupTag_Name":"Hiace","lockedby":"63fb45a9","AvailableSeat":[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],"BlockedSeat":[2,3,14],"FareAmount":"7000.00","SpecialSeatPricing":[]}]}}';*/
		$info_Company = Company::Where('id',$CompanyID)->First();
		$url = $info_Company->api_url."AvaiableBusWithSeatPrice?LoadinID=".$request->LoadinID."&DestinationID=".$request->DestinationID."&TripDate=".$request->TripDate;
		$options = array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>"Authorization: Bearer ".$info_Company->api_token,
		  )
		);
		
		$context = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
		
		//return $decodeData = json_decode($data, true);
		return $data;
	}
	
	public function SaveBookingOrder(Request $request, $CompanyID)
    {
		$info_Company = Company::Where('id',$CompanyID)->First();
		$url = $info_Company->api_url."SaveBookingOrder";
		
		$postdata = http_build_query(
			array(
				'TripID' => $request->TripID,
				'SelectedSeats' => $request->SelectedSeats,
				'MaxSeat' => $request->MaxSeat,
				'DestinationID' => $request->DestinationID,
				'OrderID' => $request->OrderID,
				'Customers[0][Fullname]' => $request->Fullname,
				'Customers[0][Phone]' => $request->Phone,
				'Customers[0][Email]' => $request->Email,
				'Customers[0][nextKin]' => $request->nextKin,
				'Customers[0][nextKinPhone]' => $request->nextKinPhone,
				'Customers[0][SelectedSeats]' => $request->KinSelectedSeats,
				'Customers[0][Sex]' => $request->Sex,
			)
		);
		
		$options = array(
		  'http'=>array(
			'header'=> "Content-type: application/x-www-form-urlencoded\r\n". "Content-Length: " . strlen($postdata) . "\r\nAuthorization: Bearer ".$info_Company->api_token,
			'Content-type' => 'application/x-www-form-urlencoded',
			'method'=>"POST",
			'content' => $postdata
		  )
		);
		$context = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
		
		//return $decodeData = json_decode($data, true);
		return $data;
	}
	
	public function BookingStatus(Request $request, $CompanyID)
    {
		$info_Company = Company::Where('id',$CompanyID)->First();
		$url = $info_Company->api_url."BookingStatus?Phone=".$request->Phone."&RefCode=".$request->RefCode;
		$options = array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>"Authorization: Bearer ".$info_Company->api_token,
		  )
		);
		
		$context = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
		
		//return $decodeData = json_decode($data, true);
		return $data;
	}
	
	public function ActivateLockSeat(Request $request, $CompanyID)
    {
		$info_Company = Company::Where('id',$CompanyID)->First();
		$url = $info_Company->api_url."activateLockSeat?Destinationid=".$request->DestinationID."&SelectedSeats=".$request->SelectedSeats."&TripID=".$request->TripID."&lockedby=".$request->LockedBy;
		$options = array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>"Authorization: Bearer ".$info_Company->api_token,
		  )
		);
		
		$context = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
		
		//return $decodeData = json_decode($data, true);
		return $data;
	}
	
	public function getDiscountAmount(Request $request, $CompanyID)
    {
		$info_Company = Company::Where('id',$CompanyID)->First();
		$url = $info_Company->api_url."getDiscountAmount?DiscountCode=".$request->DiscountCode;
		$options = array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>"Authorization: Bearer ".$info_Company->api_token,
		  )
		);
		
		$context = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
		
		//return $decodeData = json_decode($data, true);
		return $data;
	}
	
	public function ValidatePhone(Request $request, $CompanyID)
    {
		$info_Company = Company::Where('id',$CompanyID)->First();
		$url = $info_Company->api_url."validatePhone?Phone=".'0'.substr($request->Msisdn,-10);
		$options = array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>"Authorization: Bearer ".$info_Company->api_token,
		  )
		);
		
		$context = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
		
		//return $decodeData = json_decode($data, true);
		return $data;
	}
}
