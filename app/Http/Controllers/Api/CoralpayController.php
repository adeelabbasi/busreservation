<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Company;
use App\Models\Booking;
use App\Models\Payment;
use DB;

class CoralpayController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
  		//
    }
	/*
	create table _sequences
	(
	name varchar(100),
	next int,
	inc int
	);
	
	DELIMITER //
	CREATE FUNCTION `NextVal`(vname VARCHAR(30) CHARSET utf16) RETURNS int(11)
		DETERMINISTIC
	BEGIN
		UPDATE _sequences SET next = (@next := next) + 1 WHERE name = vname;
		RETURN @next;
	END//
	DELIMITER ;
	
	INSERT INTO `_sequences` (`name`, `next`, `inc`) VALUES ('GetDetail', 10000, 1);	
	*/
	public function GetDetails(Request $request)
	{
		\Log::info("GetDetails: ".json_encode($request->all()));
		//$info_Booking = Booking::Where('refcode',substr($request->customerRef,3))->First();	
		$info_Booking = Booking::Where('refcode',$request->customerRef)->First();	
		$info_Company = $info_Booking->Company()->First();
		$traceId = "ITR".DB::Select("SELECT NextVal('GetDetail') as 'Seq'")[0]->Seq;
		if($info_Booking)
		{
			
			//Already Paid
			if($info_Booking->status=="1")
			{
				\Log::info("GetDetails Response: ".json_encode(array('traceId' => $traceId, 'customerName' => $info_Booking->Fullname, 'amount' => 0, 'displayMessage' => 'Already paid', 'responseCode' => '01')));
				return \Response::json(array('traceId' => $traceId, 'customerName' => $info_Booking->Fullname, 'amount' => 0, 'displayMessage' => 'Already paid', 'responseCode' => '01'));
			}
			
			//Locktime
			if(\Carbon\Carbon::now() > ($info_Booking->created_at->addMinutes($info_Company->seat_block_time)))
			{
				\Log::info("GetDetails Response: ".json_encode(array('traceId' => $traceId, 'customerName' => $info_Booking->Fullname, 'amount' => 0, 'displayMessage' => 'Already paid', 'responseCode' => '01')));
				return \Response::json(array('traceId' => $traceId, 'customerName' => $info_Booking->Fullname, 'amount' => 0, 'displayMessage' => 'Please book again, book seat is unlock now', 'responseCode' => '01'));
			}
			

			$amount = $info_Booking->sub_total;
			if($info_Booking->charge_type == 2)
			{
				$amount = $info_Booking->total;
			}
		
			\Log::info("GetDetails Response: ".json_encode(array('traceId' => $traceId, 'customerName' => $info_Booking->Fullname, 'amount' => number_format((float)$amount, 2, '.', ''), 'displayMessage' => 'Your Payment is successful', 'responseCode' => '00')));
			
			return \Response::json(array('traceId' => $traceId, 'customerName' => $info_Booking->Fullname, 'amount' => number_format((float)$amount, 2, '.', ''), 'displayMessage' => 'Ticket Payment for '.$info_Company->short_name.' '.$info_Booking->from_terminal_name.'  to '.$info_Booking->to_terminal_name.' '.$info_Booking->schedule_date, 'responseCode' => '00'));
		}
		else
		{
			\Log::info("GetDetails Response: ".json_encode(array('traceId' => $traceId, 'customerName' => '', 'amount' => 0, 'displayMessage' => 'Incorrect customerRef', 'responseCode' => '01')));
			return \Response::json(array('traceId' => $traceId, 'customerName' => '', 'amount' => 0, 'displayMessage' => 'Incorrect customerRef', 'responseCode' => '01'));
		}
	}
	
	public function PaymentNotification(Request $request)
	{ 
		\Log::info("PaymentNotification: ".json_encode($request->all()));
		$conCat = $request->paymentReference.$request->customerRef.$request->responseCode.$request->merchantId.number_format((float)$request->amount, 2, '.', '').'|48ec6c24-22ad-48dc-aac6-965e93863bc3';		
		if(hash_equals( hash('sha256',$conCat) ,$request->hash))
		{
			//$info_Booking = Booking::Where('refcode',substr($request->customerRef,3))->First();		
			$info_Booking = Booking::Where('refcode',$request->customerRef)->First();		
			if($info_Booking)
			{
				$info_Booking->traceId = $request->traceId;
				$info_Booking->save();
				
				$info_Booking->status=1;
				$info_Booking->save();
				
				$db_payment = new Payment;
				$db_payment->company_id = $info_Booking->company_id;
				$db_payment->booking_id = $info_Booking->id;
				//$db_payment->bank_id = 
				$db_payment->paymentReference = $request->paymentReference;
				$db_payment->traceId = $request->traceId;
				$db_payment->customerRef = $request->customerRef;
				
				$db_payment->payment_provider = 'Coralpay';
				
				$db_payment->msisdn = $info_Booking->msisdn;
				$db_payment->channel_fee = $info_Booking->channel_fee;
				$db_payment->gateway_fee = $info_Booking->gateway_fee;
				$db_payment->amount = $request->amount;
				$db_payment->status = 1;
				$db_payment->save();
				
				$smsMessage = "GUO transport\nPayment Recieved, Your Ticket is Confirmed Ref no: ".$info_Booking->OrderID."\n"."GUO TRANSPORT LTD:\n".$info_Booking->from_terminal_name." to ".$info_Booking->to_terminal_name."\n".date('D d M Y', strtotime($info_Booking->schedule_date))."\n".$info_Booking->dept_time."\n".$info_Booking->bus."\nSeat Number: ".$info_Booking->SelectedSeats."\nContact GUO Transport on 08075090620, 08174594910";
				$this->sendSMS('8014', $info_Booking->msisdn, $smsMessage, $info_Booking->gateway);
				
				
				//Notify GUO
				
				$curl = curl_init();
				$postGUOData = '{
						"paymentReference": "'.$request->paymentReference.'",
						"customerRef": "'.$info_Booking->OrderID.'",
						"responseCode": "00",
						"MerchantID": "GUO",
						"mobileNumber": "0'.substr($db_payment->msisdn,-10).'",
						"email": "admin@iyconsoft.com",
						"amount": "'.$info_Booking->sub_total.'",
						"transactionDate": "'.$db_payment->created_at->format('Y-m-d h:i:s').'",
						"traceId": "'.$request->traceId.'",
						"shortCode": "8014",
						"currency": "NGN",
						"Channel": "USSD"
					}';
				\Log::info("postGUOData: ".$postGUOData);
				curl_setopt_array($curl, array(
					CURLOPT_URL => 'http://138.197.97.95/iyconhook.php',
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'POST',
					CURLOPT_POSTFIELDS => $postGUOData,
					CURLOPT_HTTPHEADER => array(
						'Authorization: Basic R1VPOkd1MC00YTMxODEwMiMh',
						'Content-Type: application/json'
					),
				));
				$response = curl_exec($curl);
				curl_close($curl);		
				\Log::info("postGUOData Response: ".$response);
				//echo $response;
				//END Notigy GUO
				
				\Log::info("GetDetails Response: ".json_encode(array('responseCode' => '00', 'ResponseMessage' => 'Successful')));
				return \Response::json(array('responseCode' => '00', 'ResponseMessage' => 'Successful'));
			}
			else
			{
				
				\Log::info("GetDetails Response: ".json_encode(array('responseCode' => '01', 'ResponseMessage' => 'Incorrect customerRef')));
				return \Response::json(array('responseCode' => '01', 'ResponseMessage' => 'Incorrect customerRef'));
			}
		}
		else
		{
			\Log::info("GetDetails Response: ".json_encode(array('responseCode' => '01', 'ResponseMessage' => 'Data integrity failures')));
			return \Response::json(array('responseCode' => '01', 'ResponseMessage' => 'Data integrity failures'));
		}
		
	}
	
	public function sendSMS($from, $to, $message, $gateway)
	{
		$message_en = rawurlencode($message);
		$to = trim($to);
		
		if($from == "8014")
		{
			if(strtolower($gateway) == "9mobile")
			{
				$url = "http://174.143.201.191/sendsms/?username=route8014&password=p5KHA9l3rC&phoneNo=$to&message=$message_en&shortcode=$from&dtype=json&dlr=1";
			}
			else
			if(strtolower($gateway) == "glo")
			{
				$url = "http://174.143.201.191/gloSUB/sendsms/index_ninajojer.php?username=route8014&password=p5KHA9l3rC&msisdn=$to&message=$message_en&shortcode=$from";
			}
			else
			if(strtolower($gateway) == "airtel")
			{
				//$url = "https://app.multitexter.com/v2/app/sms?email=tech@iyconsoft.com&message=$message_en&recipients=$to&forcednd=1&password=sayntt123&sender_name=$from";
				//$url = "http://3.131.19.214:8802/?phonenumber=".$to."&text=".($message_en)."&sender=I-TRAVEL&user=selfserve&password=1234567891";
				$url = "http://174.143.201.191/airtelSUB/sendsms/?username=route8014&password=p5KHA9l3rC&msisdn=".$to."&message=".($message_en)."&shortcode=8014&dlr=1";
			}
			else
			{
				$url = "http://174.143.201.191/mtnSDP/sendsms/?username=route8014&password=p5KHA9l3rC&msisdn=$to&message=$message_en&shortcode=$from";
			}
		}
		else
		{
			//echo "<br />";
			$url = "http://localhost:60200/cgi-bin/sendsms?username=usms2_0&password=psms2_0&smsc=smsc-1&from=$from&to=$to&text=$message_en";
		}
		
		//echo "<br />$url";
		if(env("APP_ENV") != "adeel")
		{
			$urlresponse = file_get_contents($url);
			\Log::info($urlresponse);
			\Log::info("Send SMS URL: ".$url);
			\Log::info("Live: ".$message);
		}
		else
		{
			\Log::info($message);
		}
	}
}
