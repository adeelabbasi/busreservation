<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Company;
use App\Models\Booking;
use App\Models\Payment;
use DB;

class GUOController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
  		//
    }
	/*
	create table _sequences
	(
	name varchar(100),
	next int,
	inc int
	);
	
	DELIMITER //
	CREATE FUNCTION `NextVal`(vname VARCHAR(30) CHARSET utf16) RETURNS int(11)
		DETERMINISTIC
	BEGIN
		UPDATE _sequences SET next = (@next := next) + 1 WHERE name = vname;
		RETURN @next;
	END//
	DELIMITER ;
	
	INSERT INTO `_sequences` (`name`, `next`, `inc`) VALUES ('GetDetail', 10000, 1);	
	*/
	public function TransactionQuery($traceId, Request $request)
	{ 
		\Log::info("TransactionQuery: ".json_encode($traceId));
		$info_Payment = Payment::Where('traceId',$traceId)->First();
		if($info_Payment)
		{
			$info_Booking = $info_Payment->Booking()->First();
			
			//$conCat = $info_Payment->paymentReference.$info_Payment->customerRef.'00'.$request->traceId.number_format((float)$request->amount, 2, '.', '').'|aa79129a-2e4b-11eb-adc1-0242ac120002';	
			//"hash": '.hash('sha256',$conCat).'
			$response = array("paymentReference" => $info_Payment->paymentReference , "customerRef" => $info_Booking->OrderID ,"MerchantID" => "GUO" , "email" => "admin@iyconsoft.com", "mobileNumber" => '0'.substr($info_Payment->msisdn,-10) , "amount" => $info_Booking->sub_total ,"transactionDate" => $info_Payment->created_at->format('Y-m-d h:i:s') ,"traceId" => $traceId , "shortCode" => "889", "currency" => "NGN","Channel" => "USSD" , "responseCode" => "00" , "responseMessage" => "Succesful");
			
		}
		else
		{
			$response = array("paymentReference" => "" , "customerRef" => "" ,"MerchantID" => "" , "mobileNumber" => "" , "amount" => "" ,"transactionDate" => "" ,"traceId" => "" , "shortCode" => "", "currency" => "","Channel" => "" , "responseCode" => "01" , "responseMessage" => "Invalid Trace ID");
		}
		
		return response()->json($response);
	}
}
