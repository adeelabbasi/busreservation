<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Repositories\ApiRepository;
use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
	protected $apiRps;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ApiRepository $apiRps)
    {
  		$this->apiRps = $apiRps;
    }
	
	public function GetCompany($CompanyID)
	{
		$sql = "SELECT c.* FROM companies c where id=".$CompanyID." order by name";
		//echo "<br/>".$sql."<br />";
		return response()->json(DB::Select($sql));
	}
	
	public function GetCities($CompanyID)
	{
		$sql = "select id, name, short_name from ( SELECT c.id, c.name, c.short_name, c.sequence FROM cities c where c.id in (select distinct city_id from lgas where id in
(
	select lga_id from terminals where id in
	(
		select route_details.terminal_id from
		routes join
		(select route_id, terminal_id from route_details where sequence = 1) route_details
		on route_details.route_id = routes.id
		where routes.company_id = ".$CompanyID."
	)
	and company_id=".$CompanyID." and status = 1
))) data, (SELECT @rownum := 0) r order by sequence, name";
		//echo "<br/>".$sql."<br />";
		return response()->json(DB::Select($sql));
	}
	
	public function GetCityTerminals(Request $request, $CompanyID)
	{
		$sql = "SELECT distinct terminals.id, terminals.name, terminals.short_name, terminals.address, terminals.email, terminals.phone_number FROM 
				terminals 
				join lgas on (terminals.lga_id=lgas.id)
				join cities on (lgas.city_id=cities.id)
				where 
				terminals.status=1 and terminals.company_id='".$CompanyID."' and cities.id='".$request->CityID."' order by terminals.name";
		//echo "<br/>".$sql."<br />";
		return response()->json(DB::Select($sql));
	}
	
	public function GetFromToCityTerminals(Request $request, $CompanyID)
	{
		$sql = "select * from ( select id, name, short_name from ( 
				select distinct t.id, t.name, t.short_name, t.sequence 
				FROM terminals t 
				join fares f ON (t.id = f.to_terminal_id)
				where t.status=1 and t.company_id='".$CompanyID."' and f.company_id='".$CompanyID."'  
				and
				f.from_terminal_id in (
					SELECT distinct terminals.id FROM 
					terminals 
					join lgas on (terminals.lga_id=lgas.id)
					join cities on (lgas.city_id=cities.id)
					where 
					terminals.status=1 and terminals.id='".$request->FromTerminalID."' and cities.id='".$request->FromCityID."'
				)
				and
				f.to_terminal_id in (
					SELECT distinct terminals.id FROM 
					terminals 
					join lgas on (terminals.lga_id=lgas.id)
					join cities on (lgas.city_id=cities.id)
					where 
					terminals.status=1 and cities.id='".$request->ToCityID."'
				)
			) data, (SELECT @rownum := 0) r order by sequence, name ) outerRow order by name";
		//echo "<br/>".$sql."<br />";
		return response()->json(DB::Select($sql));
	}
	
	public function GetDeals($CompanyID)
	{
		$sql = "SELECT d.id, d.name, d.detail FROM deals d where company_id=".$CompanyID." order by name";
		//echo "<br/>".$sql."<br />";
		return response()->json(DB::Select($sql));
	}
	
	public function GetBanks($CompanyID)
	{
		$sql = "SELECT b.id, b.name, b.ussd_code FROM banks b order by name";
		//echo "<br/>".$sql."<br />";
		return response()->json(DB::Select($sql));
	}
	
	public function GetPolicies($CompanyID)
	{
		$sql = "SELECT p.id, p.name, p.detail FROM policies p where company_id=".$CompanyID." order by name";
		//echo "<br/>".$sql."<br />";
		return response()->json(DB::Select($sql));
	}
	
	public function GetAbouts($CompanyID)
	{
		$sql = "SELECT a.id, a.name, a.detail FROM abouts a where company_id=".$CompanyID." order by name";
		//echo "<br/>".$sql."<br />";
		return response()->json(DB::Select($sql));
	}
	
	public function GetServices($CompanyID)
	{
		$sql = "SELECT c.id, c.name, c.detail FROM services c where company_id=".$CompanyID." order by name";
		//echo "<br/>".$sql."<br />";
		return response()->json(DB::Select($sql));
	}
	
	public function GetSchedulesByTerminals(Request $request, $CompanyID)
	{
		$timestamp = strtotime($request->ScheduleDate);
		$sql = "SELECT 
				schedules.id as schedule_id, fares.id as fare_id, 
				schedule_times.departure_time as dept_time, 
				services.short_name as service_name, 
				fares.fare as fare,
				(select name from terminals where id=fares.from_terminal_id) as terminal_name,
				(select name from terminals where id=fares.to_terminal_id) as arr_terminal_name,
				'' as 'available_seat',
				'' as 'blocked_seat'
				FROM 
				schedules 
				JOIN schedule_times ON (schedules.id=schedule_times.schedule_id)
				join fare_types on (schedules.id=fare_types.schedule_id) 
				join services on (fare_types.service_id=services.id) 
				join fares on (fare_types.id=fares.fare_type_id)  
				where 
				fare_types.company_id=".$CompanyID." 
				and
				fares.from_terminal_id =".$request->FromTerminalID."
				and
				fares.to_terminal_id = ".$request->ToTerminalID."
				and fares.fare <> 0
				order by schedule_times.departure_time, schedules.id
			 ";
			 //schedules.".strtolower(date('l', $timestamp))."=1  and schedule_times.sequence=1 and 
			 //schedules.operatingend_date>'".$request->ScheduleDate."' and
		//echo "<br/>".$sql."<br />";
		return response()->json(DB::Select($sql));
	}
	
	public function GetSchedulesByCities(Request $request, $CompanyID)
	{
		$timestamp = strtotime($request->ScheduleDate);
		$sql = "SELECT 
					schedules.id as sch_id, fares.id as fid, 
					schedule_times.departure_time as dept_time, 
					services.short_name as service_name, 
					fares.fare as fare,
					terminals.name as terminal_name
					FROM 
					schedules 
					JOIN schedule_times ON (schedules.id=schedule_times.schedule_id)
					join fare_types on (schedules.id=fare_types.schedule_id) 
					join services on (fare_types.service_id=services.id) 
					join fares on (fare_types.id=fares.fare_type_id) 
					join terminals on (fares.from_terminal_id=terminals.id) 
					where 
					fare_types.company_id=".$CompanyID." 
					and
					fares.from_terminal_id in (
						SELECT distinct terminals.id FROM 
						terminals 
						join lgas on (terminals.lga_id=lgas.id)
						join cities on (lgas.city_id=cities.id)
						where 
						terminals.status=1 and terminals.company_id=".$CompanyID."
						and cities.id=".$request->FromCityID."
					)
					and
					fares.to_terminal_id in (
						SELECT distinct terminals.id FROM 
						terminals 
						join lgas on (terminals.lga_id=lgas.id)
						join cities on (lgas.city_id=cities.id)
						where 
						terminals.status=1 and terminals.company_id=".$CompanyID."
						and cities.id=".$request->ToCityID."
					)
					and fares.fare <> 0
					order by schedule_times.departure_time";
		//echo "<br/>".$sql."<br />";
		return response()->json(DB::Select($sql));
	}
	
	public function SaveBooking($CompanyID)
	{
		$sql = "SELECT c.* FROM companies c order by name";
		//echo "<br/>".$sql."<br />";
		return response()->json(DB::Select($sql));
	}
	
	public function BookingStatus($CompanyID)
	{
		$sql = "SELECT c.* FROM companies c order by name";
		//echo "<br/>".$sql."<br />";
		return response()->json(DB::Select($sql));
	}
	
	public function LockSeat($CompanyID)
	{
		$sql = "SELECT c.* FROM companies c order by name";
		//echo "<br/>".$sql."<br />";
		return response()->json(DB::Select($sql));
	}
	
	public function ValidatePhone(Request $request, $CompanyID)
	{
		$sql = "SELECT fullname as 'Name', msisdn as 'Phone', email as 'Email', nextkin as 'Next_Of_Kin_Name', nextkinphone as 'Next_Of_Kin_Addy', sex as 'Sex' FROM bookings b where msisdn='".'234'.substr($request->Msisdn,-10)."' limit 1";
		//echo "<br/>".$sql."<br />";
		return response()->json(DB::Select($sql));
	}
	
}
