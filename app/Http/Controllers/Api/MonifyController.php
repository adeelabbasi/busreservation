<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Company;
use App\Models\Booking;
use App\Models\Payment;
use DB;

class MonifyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
  		//
    }
	
	
	public function TestGUOLog()
	{
		$db_payments = Payment::Where('id', '>', '815')->Get();
		
		foreach($db_payments as $db_payment)
		{
			$info_Booking = Booking::FindorFail($db_payment->id);

			echo $postGUOData = '{
					"paymentReference": "'.$db_payment->paymentReference.'",
					"customerRef": "'.$info_Booking->OrderID.'",
					"responseCode": "00",
					"MerchantID": "GUO",
					"mobileNumber": "0'.substr($db_payment->msisdn,-10).'",
					"email": "admin@iyconsoft.com",
					"amount": "'.$info_Booking->sub_total.'",
					"transactionDate": "'.$db_payment->created_at->format('Y-m-d h:i:s').'",
					"traceId": "'.$info_Booking->traceId.'",
					"shortCode": "8014",
					"currency": "NGN",
					"Channel": "USSD"
				}';
			
			
			echo "<br /><br />";
			
			
			$curl = curl_init();
			\Log::info("postGUOData: ".$postGUOData);
			curl_setopt_array($curl, array(
				CURLOPT_URL => 'http://138.197.97.95/iyconhook.php',
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => '',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'POST',
				CURLOPT_POSTFIELDS => $postGUOData,
				CURLOPT_HTTPHEADER => array(
					'Authorization: Basic R1VPOkd1MC00YTMxODEwMiMh',
					'Content-Type: application/json'
				),
			));
			$response = curl_exec($curl);
			curl_close($curl);	
			
			echo "<br />postGUOData Response: ".$response."<br />";
			\Log::info("postGUOData Response: ".$response);
		}
	}
	
	public function GetMonifyAccount(Request $request, $CompanyID)
    {
		$incomeSplitconfig[] = array ("subAccountCode" => "MFY_SUB_595826437644", "feePercentage" => 100, "splitPercentage" => 100, "feeBearer" => true);
		
		$uniqeID = $request->booking_id.'-'.time();
		$data = json_encode(array
					(
					  "amount" => $request->amount,
					  "customerName" => $request->customer_name,
					  "customerEmail" => $request->customer_email,
					  "paymentReference" => $uniqeID,
					  "paymentDescription" => "GUO",
					  "currencyCode" => "NGN",
					  "contractCode" => "479772873893",
					  "redirectUrl" => url('api/monify/PaymentCallback/'.$request->booking_id.'?paymentReference='.$uniqeID),
					  "paymentMethods" => array("CARD","ACCOUNT_TRANSFER"),
					  "incomeSplitConfig" => $incomeSplitconfig
					));
		\Log::info("GetMonifyAccount Request:".$data);
		$init_url = "https://api.monnify.com/api/v1/merchant/transactions/init-transaction";
		$username = "MK_PROD_FG7U53GQSM";
		$password = "Z8WERK5PNW836PUMB8C8YEZQXDAJHSSM";
		$curl = curl_init();
	
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $init_url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS => $data,
		  CURLOPT_HTTPHEADER => array(
			"Authorization: Basic " . base64_encode("$username:$password"),
			'Content-Type: application/json'
		  ),
		));
		
		$response = curl_exec($curl);
		//\Log::info("GetMonifyAccount Response:".$response);
		curl_close($curl);
		$responseData = json_decode($response);
		if($responseData->responseCode == 0)
		{
			///
			
			$data = '{"transactionReference": "'.$responseData->responseBody->transactionReference.'", "bankCode": "058" }';
		
			$init_url = "https://api.monnify.com/api/v1/merchant/bank-transfer/init-payment";
			$username = "MK_PROD_FG7U53GQSM";
			$password = "Z8WERK5PNW836PUMB8C8YEZQXDAJHSSM";
			$curl = curl_init();
		
			curl_setopt_array($curl, array(
			  CURLOPT_URL => $init_url,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => '',
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => 'POST',
			  CURLOPT_POSTFIELDS => $data,
			  CURLOPT_HTTPHEADER => array(
				"Authorization: Basic " . base64_encode("$username:$password"),
				'Content-Type: application/json'
			  ),
			));
			
			$response = curl_exec($curl);
			curl_close($curl);
			$responseData = json_decode($response,true);
			if($responseData['responseCode'] == 0)
			{
				return $responseData['responseBody'];
			}
			else
			{
				return $response;
			}
			///
		}
		else
		{
			return $response;
		}
	}
	
	public function PaymentCallback(Request $request)
	{ 
		$json = (file_get_contents('php://input'));
		$decodeData = json_decode($json);
		
		\Log::info('Monify PaymentCallback:'.$json);
		
		$paymentReference = explode('-',$decodeData->paymentReference);
		$info_Booking = Booking::Where('id',$paymentReference[0])->First();		
		if($info_Booking)
		{
			//$info_Booking->traceId = $request->paymentReference;
			$info_Booking->save();
			
			$info_Booking->status=1;
			$info_Booking->save();
			
			$db_payment = new Payment;
			$db_payment->company_id = $info_Booking->company_id;
			$db_payment->booking_id = $info_Booking->id;
			$db_payment->paymentReference = $decodeData->paymentReference;
			$db_payment->traceId = $info_Booking->traceId;
			$db_payment->customerRef = $request->transactionReference;
			
			$db_payment->payment_provider = 'Monify';
			
			$db_payment->msisdn = $info_Booking->msisdn;
			$db_payment->channel_fee = $info_Booking->channel_fee;
			$db_payment->gateway_fee = $info_Booking->gateway_fee;
			$db_payment->amount = $decodeData->totalPayable;
			$db_payment->status = 1;
			$db_payment->save();
			
			$smsMessage = "GUO transport\nPayment Recieved, Your Ticket is Confirmed Ref no: ".$info_Booking->OrderID."\n"."GUO TRANSPORT LTD:\n".$info_Booking->from_terminal_name." to ".$info_Booking->to_terminal_name."\n".date('D d M Y', strtotime($info_Booking->schedule_date))."\n".$info_Booking->dept_time."\n".$info_Booking->bus."\nSeat Number: ".$info_Booking->SelectedSeats."\nContact GUO Transport on 08075090620, 08174594910";
			$this->sendSMS('8014', $info_Booking->msisdn, $smsMessage, $info_Booking->gateway);
			
			
			//Notify GUO
			$curl = curl_init();
			$postGUOData = '{
					"paymentReference": "'.$request->paymentReference.'",
					"customerRef": "'.$info_Booking->OrderID.'",
					"responseCode": "00",
					"MerchantID": "GUO",
					"mobileNumber": "0'.substr($db_payment->msisdn,-10).'",
					"email": "admin@iyconsoft.com",
					"amount": "'.$info_Booking->sub_total.'",
					"transactionDate": "'.$db_payment->created_at->format('Y-m-d h:i:s').'",
					"traceId": "'.$info_Booking->traceId.'",
					"shortCode": "8014",
					"currency": "NGN",
					"Channel": "USSD"
				}';
			\Log::info("postGUOData: ".$postGUOData);
			curl_setopt_array($curl, array(
				CURLOPT_URL => 'http://138.197.97.95/iyconhook.php',
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => '',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'POST',
				CURLOPT_POSTFIELDS => $postGUOData,
				CURLOPT_HTTPHEADER => array(
					'Authorization: Basic R1VPOkd1MC00YTMxODEwMiMh',
					'Content-Type: application/json'
				),
			));
			$response = curl_exec($curl);
			curl_close($curl);		
			\Log::info("postGUOData Response: ".$response);
			//echo $response;
			//END Notigy GUO
			
			\Log::info("GetDetails Response: ".json_encode(array('responseCode' => '00', 'ResponseMessage' => 'Successful')));
			return \Response::json(array('responseCode' => '00', 'ResponseMessage' => 'Successful'));
		}
		else
		{
			
			\Log::info("GetDetails Response: ".json_encode(array('responseCode' => '01', 'ResponseMessage' => 'Incorrect Booking ID')));
			return \Response::json(array('responseCode' => '01', 'ResponseMessage' => 'Incorrect Booking ID'));
		}
		
		
	}
	
	public function sendSMS($from, $to, $message, $gateway)
	{
		$message_en = rawurlencode($message);
		$to = trim($to);
		
		if($from == "8014")
		{
			if(strtolower($gateway) == "9mobile")
			{
				$url = "http://174.143.201.191/sendsms/?username=route8014&password=p5KHA9l3rC&phoneNo=$to&message=$message_en&shortcode=$from&dtype=json&dlr=1";
			}
			else
			if(strtolower($gateway) == "glo")
			{
				$url = "http://174.143.201.191/gloSUB/sendsms/index_ninajojer.php?username=route8014&password=p5KHA9l3rC&msisdn=$to&message=$message_en&shortcode=$from";
			}
			else
			if(strtolower($gateway) == "airtel")
			{
				//$url = "https://app.multitexter.com/v2/app/sms?email=tech@iyconsoft.com&message=$message_en&recipients=$to&forcednd=1&password=sayntt123&sender_name=$from";
				//$url = "http://3.131.19.214:8802/?phonenumber=".$to."&text=".($message_en)."&sender=I-TRAVEL&user=selfserve&password=1234567891";
				$url = "http://174.143.201.191/airtelSUB/sendsms/?username=route8014&password=p5KHA9l3rC&msisdn=".$to."&message=".($message_en)."&shortcode=8014&dlr=1";
			}
			else
			{
				$url = "http://174.143.201.191/mtnSDP/sendsms/?username=route8014&password=p5KHA9l3rC&msisdn=$to&message=$message_en&shortcode=$from";
			}
		}
		else
		{
			//echo "<br />";
			$url = "http://localhost:60200/cgi-bin/sendsms?username=usms2_0&password=psms2_0&smsc=smsc-1&from=$from&to=$to&text=$message_en";
		}
		
		//echo "<br />$url";
		if(env("APP_ENV") != "adeel")
		{
			$urlresponse = file_get_contents($url);
			\Log::info($urlresponse);
			\Log::info("Send SMS URL: ".$url);
			\Log::info("Live: ".$message);
		}
		else
		{
			\Log::info($message);
		}
	}
}
