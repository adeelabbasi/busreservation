<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ReportRepository;
use Response;
use Auth;

class HomeController extends Controller
{
	public $guard = "admin";
	public $guard_url = "/admin/";
	protected $reportRps;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ReportRepository $reportRps)
    {
		if(str_replace("/","",request()->route()->getPrefix())=="admin")
		{
			$this->guard = "admin";
			$this->guard_url = "/admin/";
		}
		else
		{
			$this->guard = "web";
			$this->guard_url = "/company/";
		}
		
  		$this->reportRps = $reportRps;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		if(Auth::guard('admin')->check())
		{
			$data['CompanyCount'] = $this->reportRps->getCompany();
			$data['CountryCount'] = $this->reportRps->getCountry();
			$data['CityCount'] = $this->reportRps->getCity();
			$data['LgaCount'] = $this->reportRps->getLga();
		}
		else
		{
			$data['CompanyCount'] = 1;
			$data['CountryCount'] = $this->reportRps->getCountry();
			$data['CityCount'] = $this->reportRps->getCity();
			$data['LgaCount'] = $this->reportRps->getLga();
		}
		return view('cms.home', array('data' => $data));
    }
	
	public function dashboard()
    {
		if(Auth::guard('admin')->check())
		{
			$data['CompanyCount'] = $this->reportRps->getCompany();
			$data['CountryCount'] = $this->reportRps->getCountry();
			$data['CityCount'] = $this->reportRps->getCity();
			$data['LgaCount'] = $this->reportRps->getLga();
			$data['TerminalReports'] = $this->reportRps->getTerminalReport();
			$data['FleetReports'] = $this->reportRps->getFleetReport();
			$data['ServiceReports'] = $this->reportRps->getServiceReport();
			$data['RouteReports'] = $this->reportRps->getRouteReport();
			$data['ScheduleReports'] = $this->reportRps->getScheduleReport();
			$data['UserReports'] = $this->reportRps->getUserReport();
			$data['ScheduleByDayReports'] = $this->reportRps->getScheduleByDayReport();
			
			
		}
		else
		{
			$company_id = Auth::guard($this->guard)->user()->company_id;
			$data['CompanyCount'] = 1;
			$data['CountryCount'] = $this->reportRps->getCountry();
			$data['CityCount'] = $this->reportRps->getCity();
			$data['LgaCount'] = $this->reportRps->getLga();
			$data['TerminalReports'] = $this->reportRps->getTerminalReport($company_id);
			$data['FleetReports'] = $this->reportRps->getFleetReport($company_id);
			$data['ServiceReports'] = $this->reportRps->getServiceReport($company_id);
			$data['RouteReports'] = $this->reportRps->getRouteReport($company_id);
			$data['ScheduleReports'] = $this->reportRps->getScheduleReport($company_id);
			$data['UserReports'] = $this->reportRps->getUserReport($company_id);
			$data['ScheduleByDayReports'] = $this->reportRps->getScheduleByDayReport($company_id);
		}
		return view('cms.dashboard', array('data' => $data));
    }
	
	public function chartReport(Request $request, $report, $type)
	{
		switch($report)
		{
			case 'Customer':
								
				if(Auth::guard('admin')->check())
				{
					$Results = $this->reportRps->getCustomersReport($type);
				}
				else
				{
					$info_Company = Auth::guard($this->guard)->user()->Company()->First();
					
					$Results = $this->reportRps->getCustomersReport($type, $info_Company->Company_ussd()->First()->code);
				}
				
				/*if($type=='Day')
				{
					$Labels = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24");
				}
				else*/
				{
					$Labels = array_unique(array_column($Results, 'DataVar'));
					sort($Labels);
				}
				
				if($type=='Day')
				{
					$ResultsArr = array();
					foreach($Results as $Result)
					{
						array_push($ResultsArr,array("DataVar" => $Result->DataVar, "Count" => $Result->Count));
					}
					foreach($Labels as $Label)
					{
						foreach($ResultsArr as $Item)
						{
							if(!in_array($Label,array_column($ResultsArr, "DataVar")))
							{
								array_push($ResultsArr,array("DataVar" => $Label, "Count" => 0));
							}
						}
						sort($ResultsArr);			
					}
					
				}
				else
				{
					$ResultsArr = $Results;
				}
				
				return array("Labels" => $Labels, "Data" => $ResultsArr);
			break;
			
			case 'Unique_Customer':
								
				if(Auth::guard('admin')->check())
				{
					$Results = $this->reportRps->getUniqueCustomersReport($type);
				}
				else
				{
					$info_Company = Auth::guard($this->guard)->user()->Company()->First();
					
					$Results = $this->reportRps->getUniqueCustomersReport($type, $info_Company->Company_ussd()->First()->code);
				}
				
				/*if($type=='Day')
				{
					$Labels = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24");
				}
				else*/
				{
					$Labels = array_unique(array_column($Results, 'DataVar'));
					sort($Labels);
				}
				
				if($type=='Day')
				{
					$ResultsArr = array();
					foreach($Results as $Result)
					{
						array_push($ResultsArr,array("DataVar" => $Result->DataVar, "Count" => $Result->Count));
					}
					foreach($Labels as $Label)
					{
						foreach($ResultsArr as $Item)
						{
							if(!in_array($Label,array_column($ResultsArr, "DataVar")))
							{
								array_push($ResultsArr,array("DataVar" => $Label, "Count" => 0));
							}
						}
						sort($ResultsArr);			
					}
					
				}
				else
				{
					$ResultsArr = $Results;
				}
				
				return array("Labels" => $Labels, "Data" => $ResultsArr);
			break;
			
			case 'Company_Requests_Report':
								
				if(Auth::guard('admin')->check())
				{
					$Results = $this->reportRps->getCompanyRequestsReport($type,$request->company_name);
				}
				else
				{
					$info_Company = Auth::guard($this->guard)->user()->Company()->First();
					
					$Results = $this->reportRps->getCompanyRequestsReport($type, $request->company_name, $info_Company->Company_ussd()->First()->code);
				}
				
				/*if($type=='Day')
				{
					$Labels = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24");
				}
				else*/
				{
					$Labels = array_unique(array_column($Results, 'DataVar'));
					sort($Labels);
				}
				
				$CC = "";
				$ResultsArr = array();
				$i=-1;
				foreach($Results as $Result)
				{
					if($CC!=$Result->ussd_code)
					{
						$i++;
						$ResultsArr[$i] = array();
						$CC=$Result->ussd_code;
					}
					array_push($ResultsArr[$i],array("ussd_code" => $Result->ussd_code, "DataVar" => $Result->DataVar, "Count" => $Result->Count));
				}

				foreach($Labels as $Label)
				{
					$i=0;
					foreach($ResultsArr as $Items)
					{
						foreach($Items as $Item)
						{
							if(!in_array($Label,array_column($ResultsArr[$i], "DataVar")))
							{
								array_push($ResultsArr[$i],array("ussd_code" => $Item['ussd_code'], "DataVar" => $Label, "Count" => 0));
							}
						}
						sort($ResultsArr[$i]);
						$i++;
					}					
				}
				return array("Labels" => $Labels, "Data" => $ResultsArr);
			break;
			
			case 'Shortcode_Strings_Report':
								
				if(Auth::guard('admin')->check())
				{
					$Results = $this->reportRps->getShortcodeStringsReport($type);
				}
				else
				{
					$info_Company = Auth::guard($this->guard)->user()->Company()->First();
					
					$Results = $this->reportRps->getShortcodeStringsReport($type, $info_Company->Company_ussd()->First()->code);
				}
				
				/*if($type=='Day')
				{
					$Labels = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24");
				}
				else*/
				{
					$Labels = array_unique(array_column($Results, 'DataVar'));
					sort($Labels);
				}
				
				$CC = "";
				$ResultsArr = array();
				$i=-1;
				foreach($Results as $Result)
				{
					if($CC!=$Result->ussd_code)
					{
						$i++;
						$ResultsArr[$i] = array();
						$CC=$Result->ussd_code;
					}
					array_push($ResultsArr[$i],array("ussd_code" => $Result->ussd_code, "DataVar" => $Result->DataVar, "Count" => $Result->Count));
				}

				foreach($Labels as $Label)
				{
					$i=0;
					foreach($ResultsArr as $Items)
					{
						foreach($Items as $Item)
						{
							if(!in_array($Label,array_column($ResultsArr[$i], "DataVar")))
							{
								array_push($ResultsArr[$i],array("ussd_code" => $Item['ussd_code'], "DataVar" => $Label, "Count" => 0));
							}
						}
						sort($ResultsArr[$i]);
						$i++;
					}					
				}
				return array("Labels" => $Labels, "Data" => $ResultsArr);
			break;
			
			case 'Route_Report':
								
				if(Auth::guard('admin')->check())
				{
					$Results = $this->reportRps->getRoutesReport($type);
				}
				else
				{
					$info_Company = Auth::guard($this->guard)->user()->Company()->First();
					
					$Results = $this->reportRps->getRoutesReport($type, $info_Company->Company_ussd()->First()->code);
				}
				
				/*if($type=='Day')
				{
					$Labels = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24");
				}
				else*/
				{
					$Labels = array_unique(array_column($Results, 'DataVar'));
					sort($Labels);
				}
				
				$CC = "";
				$ResultsArr = array();
				$i=-1;
				foreach($Results as $Result)
				{
					if($CC!=$Result->route_name)
					{
						$i++;
						$ResultsArr[$i] = array();
						$CC=$Result->route_name;
					}
					array_push($ResultsArr[$i],array("route_name" => $Result->route_name, "DataVar" => $Result->DataVar, "Count" => $Result->Count));
				}

				foreach($Labels as $Label)
				{
					$i=0;
					foreach($ResultsArr as $Items)
					{
						foreach($Items as $Item)
						{
							if(!in_array($Label,array_column($ResultsArr[$i], "DataVar")))
							{
								array_push($ResultsArr[$i],array("route_name" => $Item['route_name'], "DataVar" => $Label, "Count" => 0));
							}
						}
						sort($ResultsArr[$i]);
						$i++;
					}					
				}
				return array("Labels" => $Labels, "Data" => $ResultsArr);
			break;
			
			case 'Schedules_Report':
								
				if(Auth::guard('admin')->check())
				{
					$Results = $this->reportRps->getSchedulesReport($type);
				}
				else
				{
					$info_Company = Auth::guard($this->guard)->user()->Company()->First();
					
					$Results = $this->reportRps->getSchedulesReport($type, $info_Company->Company_ussd()->First()->code);
				}
				
				/*if($type=='Day')
				{
					$Labels = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24");
				}
				else*/
				{
					$Labels = array_unique(array_column($Results, 'DataVar'));
					sort($Labels);
				}
				
				$CC = "";
				$ResultsArr = array();
				$i=-1;
				foreach($Results as $Result)
				{
					if($CC!=$Result->schedule_name)
					{
						$i++;
						$ResultsArr[$i] = array();
						$CC=$Result->schedule_name;
					}
					array_push($ResultsArr[$i],array("schedule_name" => $Result->schedule_name, "DataVar" => $Result->DataVar, "Count" => $Result->Count));
				}

				foreach($Labels as $Label)
				{
					$i=0;
					foreach($ResultsArr as $Items)
					{
						foreach($Items as $Item)
						{
							if(!in_array($Label,array_column($ResultsArr[$i], "DataVar")))
							{
								array_push($ResultsArr[$i],array("schedule_name" => $Item['schedule_name'], "DataVar" => $Label, "Count" => 0));
							}
						}
						sort($ResultsArr[$i]);
						$i++;
					}					
				}
				return array("Labels" => $Labels, "Data" => $ResultsArr);
			break;
			
			case 'USSD_Menu_General_Items_Report':
								
				if(Auth::guard('admin')->check())
				{
					$Results = $this->reportRps->getUSSDMenuGeneralItemsReport($type,$request->company_name);
				}
				else
				{
					$info_Company = Auth::guard($this->guard)->user()->Company()->First();
					
					$Results = $this->reportRps->getUSSDMenuGeneralItemsReport($type, $request->company_name, $info_Company->Company_ussd()->First()->code);
				}
				
				/*if($type=='Day')
				{
					$Labels = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24");
				}
				else*/
				{
					$Labels = array_unique(array_column($Results, 'DataVar'));
					sort($Labels);
				}
				
				$CC = "";
				$ResultsArr = array();
				$i=-1;
				foreach($Results as $Result)
				{
					if($CC!=$Result->action)
					{
						$i++;
						$ResultsArr[$i] = array();
						$CC=$Result->action;
					}
					array_push($ResultsArr[$i],array("action" => $Result->action, "DataVar" => $Result->DataVar, "Count" => $Result->Count));
				}
				foreach($Labels as $Label)
				{
					$i=0;
					foreach($ResultsArr as $Items)
					{
						foreach($Items as $Item)
						{
							if(!in_array($Label,array_column($ResultsArr[$i], "DataVar")))
							{
								array_push($ResultsArr[$i],array("action" => $Item['action'], "DataVar" => $Label, "Count" => 0));
							}
						}
						sort($ResultsArr[$i]);
						$i++;
					}					
				}
				return array("Labels" => $Labels, "Data" => $ResultsArr);
			break;
			
			case 'USSD_Menu_Company_Items_Report':
								
				if(Auth::guard('admin')->check())
				{
					$Results = $this->reportRps->getUSSDMenuCompanyItemsReport($type);
				}
				else
				{
					$info_Company = Auth::guard($this->guard)->user()->Company()->First();
					
					$Results = $this->reportRps->getUSSDMenuCompanyItemsReport($type, $info_Company->Company_ussd()->First()->code);
				}
				
				/*if($type=='Day')
				{
					$Labels = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24");
				}
				else*/
				{
					$Labels = array_unique(array_column($Results, 'DataVar'));
					sort($Labels);
				}
				
				$CC = "";
				$ResultsArr = array();
				$i=-1;
				foreach($Results as $Result)
				{
					if($CC!=$Result->action)
					{
						$i++;
						$ResultsArr[$i] = array();
						$CC=$Result->action;
					}
					array_push($ResultsArr[$i],array("action" => $Result->action, "DataVar" => $Result->DataVar, "Count" => $Result->Count));
				}
				foreach($Labels as $Label)
				{
					$i=0;
					foreach($ResultsArr as $Items)
					{
						foreach($Items as $Item)
						{
							if(!in_array($Label,array_column($ResultsArr[$i], "DataVar")))
							{
								array_push($ResultsArr[$i],array("action" => $Item['action'], "DataVar" => $Label, "Count" => 0));
							}
						}
						sort($ResultsArr[$i]);
						$i++;
					}					
				}
				return array("Labels" => $Labels, "Data" => $ResultsArr);
			break;
			
			case 'Subscriptions_Report':
								
				if(Auth::guard('admin')->check())
				{
					$Results = $this->reportRps->getSubscriptionsReport($type);
				}
				else
				{
					$info_Company = Auth::guard($this->guard)->user()->Company()->First();
					
					$Results = $this->reportRps->getSubscriptionsReport($type, $info_Company->Company_ussd()->First()->code);
				}
				
				/*if($type=='Day')
				{
					$Labels = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24");
				}
				else*/
				{
					$Labels = array_unique(array_column($Results, 'DataVar'));
					sort($Labels);
				}
				
				if($type=='Day')
				{
					$ResultsArr = array();
					foreach($Results as $Result)
					{
						array_push($ResultsArr,array("DataVar" => $Result->DataVar, "Count" => $Result->Count));
					}
					foreach($Labels as $Label)
					{
						foreach($ResultsArr as $Item)
						{
							if(!in_array($Label,array_column($ResultsArr, "DataVar")))
							{
								array_push($ResultsArr,array("DataVar" => $Label, "Count" => 0));
							}
						}
						sort($ResultsArr);			
					}
					
				}
				else
				{
					$ResultsArr = $Results;
				}
				
				return array("Labels" => $Labels, "Data" => $ResultsArr);
			break;
			
			case 'Subscriptions_Company_Report':
								
				if(Auth::guard('admin')->check())
				{
					$Results = $this->reportRps->getSubscriptionsCompanyReport($type);
				}
				else
				{
					$info_Company = Auth::guard($this->guard)->user()->Company()->First();
					
					$Results = $this->reportRps->getSubscriptionsCompanyReport($type, $info_Company->Company_ussd()->First()->code);
				}
				
				/*if($type=='Day')
				{
					$Labels = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24");
				}
				else*/
				{
					$Labels = array_unique(array_column($Results, 'DataVar'));
					sort($Labels);
				}
				
				$CC = "";
				$ResultsArr = array();
				$i=-1;
				foreach($Results as $Result)
				{
					if($CC!=$Result->ussd_code)
					{
						$i++;
						$ResultsArr[$i] = array();
						$CC=$Result->ussd_code;
					}
					array_push($ResultsArr[$i],array("ussd_code" => $Result->ussd_code, "DataVar" => $Result->DataVar, "Count" => $Result->Count));
				}

				foreach($Labels as $Label)
				{
					$i=0;
					foreach($ResultsArr as $Items)
					{
						foreach($Items as $Item)
						{
							if(!in_array($Label,array_column($ResultsArr[$i], "DataVar")))
							{
								array_push($ResultsArr[$i],array("ussd_code" => $Item['ussd_code'], "DataVar" => $Label, "Count" => 0));
							}
						}
						sort($ResultsArr[$i]);
						$i++;
					}					
				}
				return array("Labels" => $Labels, "Data" => $ResultsArr);
			break;
			
			case 'Revenues_Report':
								
				if(Auth::guard('admin')->check())
				{
					$Results = $this->reportRps->getRevenuesReport($type);
				}
				else
				{
					$info_Company = Auth::guard($this->guard)->user()->Company()->First();
					
					$Results = $this->reportRps->getRevenuesReport($type, $info_Company->Company_ussd()->First()->code);
				}
				
				/*if($type=='Day')
				{
					$Labels = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24");
				}
				else*/
				{
					$Labels = array_unique(array_column($Results, 'DataVar'));
					sort($Labels);
				}
				
				if($type=='Day')
				{
					$ResultsArr = array();
					foreach($Results as $Result)
					{
						array_push($ResultsArr,array("DataVar" => $Result->DataVar, "Count" => $Result->Count));
					}
					foreach($Labels as $Label)
					{
						foreach($ResultsArr as $Item)
						{
							if(!in_array($Label,array_column($ResultsArr, "DataVar")))
							{
								array_push($ResultsArr,array("DataVar" => $Label, "Count" => 0));
							}
						}
						sort($ResultsArr);			
					}
					
				}
				else
				{
					$ResultsArr = $Results;
				}
				
				return array("Labels" => $Labels, "Data" => $ResultsArr);
			break;
			
			case 'Revenues_Company_Report':
								
				if(Auth::guard('admin')->check())
				{
					$Results = $this->reportRps->getRevenuesCompanyReport($type);
				}
				else
				{
					$info_Company = Auth::guard($this->guard)->user()->Company()->First();
					
					$Results = $this->reportRps->getRevenuesCompanyReport($type, $info_Company->Company_ussd()->First()->code);
				}
				
				/*if($type=='Day')
				{
					$Labels = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24");
				}
				else*/
				{
					$Labels = array_unique(array_column($Results, 'DataVar'));
					sort($Labels);
				}
				
				$CC = "";
				$ResultsArr = array();
				$i=-1;
				foreach($Results as $Result)
				{
					if($CC!=$Result->ussd_code)
					{
						$i++;
						$ResultsArr[$i] = array();
						$CC=$Result->ussd_code;
					}
					array_push($ResultsArr[$i],array("ussd_code" => $Result->ussd_code, "DataVar" => $Result->DataVar, "Count" => $Result->Count));
				}

				foreach($Labels as $Label)
				{
					$i=0;
					foreach($ResultsArr as $Items)
					{
						foreach($Items as $Item)
						{
							if(!in_array($Label,array_column($ResultsArr[$i], "DataVar")))
							{
								array_push($ResultsArr[$i],array("ussd_code" => $Item['ussd_code'], "DataVar" => $Label, "Count" => 0));
							}
						}
						sort($ResultsArr[$i]);
						$i++;
					}					
				}
				return array("Labels" => $Labels, "Data" => $ResultsArr);
			break;
		}
	}
	
	public function exportMsisdn(Request $request, $report, $type)
	{		
		switch($report)
		{
			case 'Customer':
				
				$fileName = $type."_".$report;
				
				if(Auth::guard('admin')->check())
				{
					$Results = $this->reportRps->getCustomersReportExport($type);
				}
				else
				{
					$info_Company = Auth::guard($this->guard)->user()->Company()->First();
					
					$Results = $this->reportRps->getCustomersReportExport($type, $info_Company->Company_ussd()->First()->code);
				}
			break;
			
			case 'Unique_Customer':
				
				$fileName = $type."_".$report;
				
				if(Auth::guard('admin')->check())
				{
					$Results = $this->reportRps->getUniqueCustomersReportExport($type);
				}
				else
				{
					$info_Company = Auth::guard($this->guard)->user()->Company()->First();
					
					$Results = $this->reportRps->getUniqueCustomersReportExport($type, $info_Company->Company_ussd()->First()->code);
				}
			break;
			
			case 'Company_Requests_Report':
				
				$fileName = $type."_".$report;
				
				if(Auth::guard('admin')->check())
				{
					$Results = $this->reportRps->getCompanyRequestsReportExport($type);
				}
				else
				{
					$info_Company = Auth::guard($this->guard)->user()->Company()->First();
					
					$Results = $this->reportRps->getCompanyRequestsReportExport($type, $info_Company->Company_ussd()->First()->code);
				}
			break;
			
			case 'Shortcode_Strings_Report':
				
				$fileName = $type."_".$report;
				
				if(Auth::guard('admin')->check())
				{
					$Results = $this->reportRps->getShortcodeStringsReportExport($type,$request->ussd_code);
				}
				else
				{
					$info_Company = Auth::guard($this->guard)->user()->Company()->First();
					
					$Results = $this->reportRps->getShortcodeStringsReportExport($type, $info_Company->Company_ussd()->First()->code);
				}
			break;
			
			case 'Routes_Report':
				
				$fileName = $type."_".$report;
				
				if(Auth::guard('admin')->check())
				{
					$Results = $this->reportRps->getRoutesExport($type);
				}
				else
				{
					$info_Company = Auth::guard($this->guard)->user()->Company()->First();
					
					$Results = $this->reportRps->getRoutesExport($type, $info_Company->Company_ussd()->First()->code);
				}
			break;
			
			case 'Schedules_Report':
				
				$fileName = $type."_".$report;
				
				if(Auth::guard('admin')->check())
				{
					$Results = $this->reportRps->getSchedulesExport($type);
				}
				else
				{
					$info_Company = Auth::guard($this->guard)->user()->Company()->First();
					
					$Results = $this->reportRps->getSchedulesExport($type, $info_Company->Company_ussd()->First()->code);
				}
			break;
			
			case 'USSD_Menu_General_Items_Report':
				
				$fileName = $type."_".$report;
				
				if(Auth::guard('admin')->check())
				{
					$Results = $this->reportRps->getUSSDMenuGeneralItemsExport($type);
				}
				else
				{
					$info_Company = Auth::guard($this->guard)->user()->Company()->First();
					
					$Results = $this->reportRps->getUSSDMenuGeneralItemsExport($type, $info_Company->Company_ussd()->First()->code);
				}
			break;
			
			case 'USSD_Menu_Company_Items_Report':
				
				$fileName = $type."_".$report;
				
				if(Auth::guard('admin')->check())
				{
					$Results = $this->reportRps->getUSSDMenuCompanyItemsExport($type);
				}
				else
				{
					$info_Company = Auth::guard($this->guard)->user()->Company()->First();
					
					$Results = $this->reportRps->getUSSDMenuCompanyItemsExport($type, $info_Company->Company_ussd()->First()->code);
				}
			break;
			
			case 'Subscriptions_Report':
				
				$fileName = $type."_".$report;
				
				if(Auth::guard('admin')->check())
				{
					$Results = $this->reportRps->getSubscriptionsExport($type);
				}
				else
				{
					$info_Company = Auth::guard($this->guard)->user()->Company()->First();
					
					$Results = $this->reportRps->getSubscriptionsExport($type, $info_Company->Company_ussd()->First()->code);
				}
			break;
			
			case 'Subscriptions_Company_Report':
				
				$fileName = $type."_".$report;
				
				if(Auth::guard('admin')->check())
				{
					$Results = $this->reportRps->getSubscriptionsCompanyExport($type);
				}
				else
				{
					$info_Company = Auth::guard($this->guard)->user()->Company()->First();
					
					$Results = $this->reportRps->getSubscriptionsCompanyExport($type, $info_Company->Company_ussd()->First()->code);
				}
			break;
			
			case 'Revenues_Report':
				
				$fileName = $type."_".$report;
				
				if(Auth::guard('admin')->check())
				{
					$Results = $this->reportRps->getRevenuesExport($type);
				}
				else
				{
					$info_Company = Auth::guard($this->guard)->user()->Company()->First();
					
					$Results = $this->reportRps->getRevenuesExport($type, $info_Company->Company_ussd()->First()->code);
				}
			break;
			
			case 'Revenues_Company_Report':
				
				$fileName = $type."_".$report;
				
				if(Auth::guard('admin')->check())
				{
					$Results = $this->reportRps->getRevenuesCompanyExport($type);
				}
				else
				{
					$info_Company = Auth::guard($this->guard)->user()->Company()->First();
					
					$Results = $this->reportRps->getRevenuesCompanyExport($type, $info_Company->Company_ussd()->First()->code);
				}
			break;
		}
		
		//return $Results;
		$Output = "Msisdn\n";
		foreach ($Results as $Result) 
		{
			$Output .=  $Result->msisdn."\n";
		}
		$headers = array(
			'Content-Type' => 'text/csv',
			'Content-Disposition' => 'attachment; filename="'.$fileName.'.csv"',
		);
		
		return Response::make(rtrim($Output, "\n"), 200, $headers);
	}
}
