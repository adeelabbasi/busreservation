<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ReportRepository;
use Response;
use Auth;

class oldHomeController extends Controller
{
	public $guard = "admin";
	public $guard_url = "/admin/";
	protected $reportRps;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ReportRepository $reportRps)
    {
		if(str_replace("/","",request()->route()->getPrefix())=="admin")
		{
			$this->guard = "admin";
			$this->guard_url = "/admin/";
		}
		else
		{
			$this->guard = "web";
			$this->guard_url = "/company/";
		}
		
  		$this->reportRps = $reportRps;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		if(Auth::guard('admin')->check())
		{
			$data['CompanyCount'] = $this->reportRps->getCompany();
			$data['CountryCount'] = $this->reportRps->getCountry();
			$data['CityCount'] = $this->reportRps->getCity();
			$data['LgaCount'] = $this->reportRps->getLga();
		}
		else
		{
			$data['CompanyCount'] = 1;
			$data['CountryCount'] = $this->reportRps->getCountry();
			$data['CityCount'] = $this->reportRps->getCity();
			$data['LgaCount'] = $this->reportRps->getLga();
		}
		return view('cms.home', array('data' => $data));
    }
	
	public function dashboard()
    {
		if(Auth::guard('admin')->check())
		{
			$data['CompanyCount'] = $this->reportRps->getCompany();
			$data['CountryCount'] = $this->reportRps->getCountry();
			$data['CityCount'] = $this->reportRps->getCity();
			$data['LgaCount'] = $this->reportRps->getLga();
			$data['TerminalReports'] = $this->reportRps->getTerminalReport();
			$data['FleetReports'] = $this->reportRps->getFleetReport();
			$data['ServiceReports'] = $this->reportRps->getServiceReport();
			$data['RouteReports'] = $this->reportRps->getRouteReport();
			$data['ScheduleReports'] = $this->reportRps->getScheduleReport();
			$data['UserReports'] = $this->reportRps->getUserReport();
			$data['ScheduleByDayReports'] = $this->reportRps->getScheduleByDayReport();
			
			//Ussd
			$data['getCustomersReportCounts'] = $this->reportRps->getCustomersReportCount();
			$data['getUniqueCustomersReportCounts'] = $this->reportRps->getUniqueCustomersReportCount();
			$data['getCompanyRequestsReportCounts'] = $this->reportRps->getCompanyRequestsReportCount();
			$data['getShortcodeStringsReportCounts'] = $this->reportRps->getShortcodeStringsReportCount();
			$data['getRoutesReportCounts'] = $this->reportRps->getRoutesReportCount();
			$data['getUSSDMenuGeneralItemsReportCounts'] = $this->reportRps->getUSSDMenuGeneralItemsReportCount();
			$data['getUSSDMenuCompanyItemsReportCounts'] = $this->reportRps->getUSSDMenuCompanyItemsReportCount();
		}
		else
		{
			$company_id = Auth::guard($this->guard)->user()->company_id;
			$data['CompanyCount'] = 1;
			$data['CountryCount'] = $this->reportRps->getCountry();
			$data['CityCount'] = $this->reportRps->getCity();
			$data['LgaCount'] = $this->reportRps->getLga();
			$data['TerminalReports'] = $this->reportRps->getTerminalReport($company_id);
			$data['FleetReports'] = $this->reportRps->getFleetReport($company_id);
			$data['ServiceReports'] = $this->reportRps->getServiceReport($company_id);
			$data['RouteReports'] = $this->reportRps->getRouteReport($company_id);
			$data['ScheduleReports'] = $this->reportRps->getScheduleReport($company_id);
			$data['UserReports'] = $this->reportRps->getUserReport($company_id);
			$data['ScheduleByDayReports'] = $this->reportRps->getScheduleByDayReport($company_id);
		}
		return view('cms.dashboard', array('data' => $data));
    }
	
	
	public function exportMsisdn(Request $request, $report, $type)
	{		
		switch($report)
		{
			case 'Customer':
				
				$fileName = $type."_".$report;
				
				if(Auth::guard('admin')->check())
				{
					$Results = $this->reportRps->getCustomersReportExport($type);
				}
				else
				{
					$company_id = Auth::guard($this->guard)->user()->company_id;
					
					$Results = $this->reportRps->getCustomersReportExport($type, $company_id->Ussd_code()->First()->code);
				}
			break;
			
			case 'Unique_Customer':
				
				$fileName = $type."_".$report;
				
				if(Auth::guard('admin')->check())
				{
					$Results = $this->reportRps->getUniqueCustomersReportExport($type);
				}
				else
				{
					$company_id = Auth::guard($this->guard)->user()->company_id;
					
					$Results = $this->reportRps->getUniqueCustomersReportExport($type, $company_id->Ussd_code()->First()->code);
				}
			break;
			
			case 'Company_Requests_Report':
				
				$fileName = $type."_".$report;
				
				if(Auth::guard('admin')->check())
				{
					$Results = $this->reportRps->getCompanyRequestsReportExport($type,$request->company_name);
				}
				else
				{
					$company_id = Auth::guard($this->guard)->user()->company_id;
					
					$Results = $this->reportRps->getCompanyRequestsReportExport($type, $request->company_name, $company_id->Ussd_code()->First()->code);
				}
			break;
			
			case 'Shortcode_Strings':
				
				$fileName = $type."_".$report;
				
				if(Auth::guard('admin')->check())
				{
					$Results = $this->reportRps->getShortcodeStringsReportExport($type,$request->ussd_code);
				}
				else
				{
					$company_id = Auth::guard($this->guard)->user()->company_id;
					
					$Results = $this->reportRps->getShortcodeStringsReportExport($type,$request->ussd_code);
				}
			break;
			
			case 'Routes':
				
				$fileName = $type."_".$report;
				
				if(Auth::guard('admin')->check())
				{
					$Results = $this->reportRps->getRoutesExport($type,$request->route_name,$request->company_name);
				}
				else
				{
					$company_id = Auth::guard($this->guard)->user()->company_id;
					
					$Results = $this->reportRps->getRoutesExport($type,$request->route_name,$request->company_name,$request->ussd_code);
				}
			break;
			
			case 'USSD_Menu_General_Items':
				
				$fileName = $type."_".$report;
				
				if(Auth::guard('admin')->check())
				{
					$Results = $this->reportRps->getUSSDMenuGeneralItemsExport($type,$request->action);
				}
				else
				{
					$company_id = Auth::guard($this->guard)->user()->company_id;
					
					$Results = $this->reportRps->getUSSDMenuGeneralItemsExport($type,$request->action,$request->ussd_code);
				}
			break;
			
			case 'USSD_Menu_Company_Items':
				
				$fileName = $type."_".$report;
				
				if(Auth::guard('admin')->check())
				{
					$Results = $this->reportRps->getUSSDMenuCompanyItemsExport($type,$request->company_name,$request->action);
				}
				else
				{
					$company_id = Auth::guard($this->guard)->user()->company_id;
					
					$Results = $this->reportRps->getUSSDMenuCompanyItemsExport($type,$request->company_name,$request->action,$request->ussd_code);
				}
			break;
		}
		
		return $Results;
		$Output = "Msisdn\n";
		foreach ($Results as $Result) 
		{
			$Output .=  $Result->msisdn."\n";
		}
		$headers = array(
			'Content-Type' => 'text/csv',
			'Content-Disposition' => 'attachment; filename="'.$fileName.'.csv"',
		);
		
		return Response::make(rtrim($Output, "\n"), 200, $headers);
	}
}
