<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CityRequest;
use Session;
use DataTables;
use App\Repositories\CountryRepository;
use App\Repositories\GeneralRepository;
use App\Repositories\CityRepository;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Auth;

class CityController extends Controller
{
	protected $cityRps;
	protected $countryRps;
	protected $generalRps;
	public $guard = "admin";
	public $guard_url = "/admin/";
	protected $viewPermission = "View State/City";
	protected $addPermission = "Add State/City";
	protected $updatePermission = "Update State/City";
	protected $deletePermission = "Delete State/City";
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(GeneralRepository $generalRps, CityRepository $cityRps, CountryRepository $countryRps)
    {
		if(str_replace("/","",request()->route()->getPrefix())=="admin")
		{
			$this->guard = "admin";
			$this->guard_url = "/admin/";
		}
		else
		{
			$this->guard = "web";
			$this->guard_url = "/company/";
		}
		$this->middleware('role_and_permission:Admin|'.$this->viewPermission.','.$this->guard);
		$this->countryRps = $countryRps;
		$this->cityRps = $cityRps;
		$this->generalRps = $generalRps;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('cms.city.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		if (!Auth::guard($this->guard)->user()->can($this->addPermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->addPermission]);
		}
        $info_Countries = $this->countryRps->getCountry();
        return view('cms.city.add', array('info_Countries' => $info_Countries));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CityRequest $request)
    {
		if (!Auth::guard($this->guard)->user()->can($this->addPermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->addPermission]);
		}
		$this->generalRps->addLog(['action' => 'Add city']);
		//return $request->all();
        $this->cityRps->addCity($request->all());
		Session::flash('flash_message', 'City successfully added!');
		return redirect($this->guard_url.'city');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\City  $foreman
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $info_City = $this->cityRps->getCity($id);
		return view('cms.city.show' ,array('info_City' => $info_City));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\City  $foreman
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->updatePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
		$info_Countries = $this->countryRps->getCountry();
        $info_City = $this->cityRps->getCity($id);
		return view('cms.city.edit' ,array('info_Countries' => $info_Countries, 'info_City' => $info_City));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\City  $foreman
     * @return \Illuminate\Http\Response
     */
    public function update(CityRequest $request, $id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->updatePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
		$this->generalRps->addLog(['action' => 'Update city']);
        $this->cityRps->updateCity($request->all() , $id);
		Session::flash('flash_message', 'City successfully updated!');
		return redirect($this->guard_url.'city');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\City  $foreman
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->deletePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
		$this->generalRps->addLog(['action' => 'Delete city']);
        $this->cityRps->deleteCity($id);
    }
	
	
	public function updateSequence(Request $request, $id)
	{
		$info_City = $this->cityRps->updateSequence($id, $request->sequence);
		
		return [];
	}
	/**
     * Create datatable grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function grid()
    {
	   $info_City = $this->cityRps->getCity();
	   return Datatables::of($info_City)
		->editColumn('status', function ($info_City) {
			 if($info_City->status==1)
				return "Active";
			 else
				return "Inactive";
				 	
        })
		->editColumn('country_id', function ($info_City) {
			return $info_City->Country()->First()->name;
				 	
        })
		->editColumn('sequence', function ($info_City) {
				 return '<input class="sequence_id" data-city_id="'.$info_City->id.'" value="'.$info_City->sequence.'" />';
        })
		->addColumn('edit', function ($info_City) {
				 $updateButton = "";
				 $deleteButton = "";
				 if (Auth::guard($this->guard)->user()->can($this->updatePermission)) 
				 {
					$updateButton = '<a class="btn-floating waves-effect waves-light green" style="margin-right:2px;" href="'.url($this->guard_url.'city/'.$info_City->id.'/edit').'" title="Edit Data"><i class="large material-icons">mode_edit</i></a>';
				 }
				 if (Auth::guard($this->guard)->user()->can($this->deletePermission)) 
				 {
					$deleteButton = '<a class="btn-floating waves-effect waves-light red" href="javascript(0)" title="Delete Data" id="btnDelete" name="btnDelete" data-message="Are you sure to delete city?" data-remote="'.$this->guard_url.'city/' . $info_City->id . '"><i class="material-icons">clear</i></a>';
				 }
				 return '<div class="btn-group btn-group-action">
								<a class="btn-floating waves-effect waves-light blue" style="margin-right:2px;" href="'.url($this->guard_url.'city/'.$info_City->id).'" title="View Detail"><i class="large material-icons">pageview</i></a> 
								'.$updateButton.$deleteButton.'
								
						</div>';
        })
		->escapeColumns([])
		->make(true);
    }
	
	public function countryCity($country_id)
	{
		$info_City = $this->cityRps->getCountryCity($country_id)->pluck("name","id");
		return response()->json($info_City);
	}
}
