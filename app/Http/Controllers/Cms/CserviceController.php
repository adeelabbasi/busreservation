<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\CserviceRepository;
use App\Http\Requests\CserviceRequest;
use App\Repositories\GeneralRepository;
use App\Repositories\CompanyRepository;
use DataTables;
use Session;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Auth;

class CserviceController extends Controller
{
	protected $companyRps;
	protected $cserviceRps;
	protected $generalRps;
	public $guard = "admin";
	public $guard_url = "/admin/";
	protected $viewPermission = "View Cservice";
	protected $addPermission = "Add Cservice";
	protected $updatePermission = "Update Cservice";
	protected $deletePermission = "Delete Cservice";
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(GeneralRepository $generalRps, CompanyRepository $companyRps, CserviceRepository $cserviceRps)
    {
		if(str_replace("/","",request()->route()->getPrefix())=="admin")
		{
			$this->guard = "admin";
			$this->guard_url = "/admin/";
		}
		else
		{
			$this->guard = "web";
			$this->guard_url = "/company/";
		}
		$this->middleware('role_and_permission:Admin|'.$this->viewPermission.','.$this->guard);		$this->companyRps = $companyRps;
        $this->cserviceRps = $cserviceRps;
		$this->generalRps = $generalRps;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('cms.cservice.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		if (!Auth::guard($this->guard)->user()->can($this->addPermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->addPermission]);
		}
		if($this->guard == "admin")
		{
			$info_Companies = $this->companyRps->getCompany();
		}
		else
		{
			$info_Companies = [];
		}
		return view('cms.cservice.add', array('info_Companies' => $info_Companies));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CserviceRequest $request)
    {
		if (!Auth::guard($this->guard)->user()->can($this->addPermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->addPermission]);
		}
		$this->generalRps->addLog(['action' => 'Add cservice']);
        $this->cserviceRps->addCservice($request->all());
		Session::flash('flash_message', 'Cservice successfully added!');
		return redirect($this->guard_url.'cservice');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cservice  $foreman
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$info_Cservice = $this->cserviceRps->getCservice($id);
		return view('cms.cservice.show' ,array('info_Cservice' => $info_Cservice));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cservice  $foreman
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->updatePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
        $info_Cservice = $this->cserviceRps->getCservice($id);
		if($this->guard == "admin")
		{
			$info_Companies = $this->companyRps->getCompany();
		}
		else
		{
			$info_Companies = [];
		}
		return view('cms.cservice.edit' ,array('info_Cservice' => $info_Cservice, 'info_Companies' => $info_Companies));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cservice  $foreman
     * @return \Illuminate\Http\Response
     */
    public function update(CserviceRequest $request, $id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->updatePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
		$this->generalRps->addLog(['action' => 'Update cservice']);
        $this->cserviceRps->updateCservice($request->all() , $id);
		Session::flash('flash_message', 'Cservice successfully updated!');
		return redirect($this->guard_url.'cservice');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cservice  $foreman
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->deletePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
        $this->generalRps->addLog(['action' => 'Delete cservice']);
        $this->cserviceRps->deleteCservice($id);
    }
	
	/**
     * Create datatable grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function grid()
    {
	   
	   	if($this->guard == "admin")
		{
			 $info_Cservice = $this->cserviceRps->getCservice();
		}
		else
		{
			 $info_Cservice = $this->cserviceRps->getCompanyCservice(\Auth::guard($this->guard)->User()->Company()->First()->id);
		}
	   	return Datatables::of($info_Cservice)
		->editColumn('company_id', function ($info_Cservice) {
			return $info_Cservice->Company()->First()->name;
        })
		->editColumn('status', function ($info_Cservice) {
			 if($info_Cservice->status==1)
				return "Active";
			 else
				return "Inactive";
				 	
        })
		->addColumn('edit', function ($info_Cservice) {
				 $updateButton = "";
				 $deleteButton = "";
				 if (Auth::guard($this->guard)->user()->can($this->updatePermission)) 
				 {
					$updateButton = '<a class="btn-floating waves-effect waves-light green" style="margin-right:2px;" href="'.url($this->guard_url.'cservice/'.$info_Cservice->id.'/edit').'" title="Edit Data"><i class="large material-icons">mode_edit</i></a>';
				 }
				 if (Auth::guard($this->guard)->user()->can($this->deletePermission)) 
				 {
					$deleteButton = '<a class="btn-floating waves-effect waves-light red" href="javascript(0)" title="Delete Data" id="btnDelete" name="btnDelete" data-message="Are you sure to delete cservice?" data-remote="'.$this->guard_url.'cservice/' . $info_Cservice->id . '"><i class="material-icons">clear</i></a>';
				 }
				 return '<div class="btn-group btn-group-action">
								<a class="btn-floating waves-effect waves-light blue" style="margin-right:2px;" href="'.url($this->guard_url.'cservice/'.$info_Cservice->id).'" title="View Detail"><i class="large material-icons">pageview</i></a> 
								'.$updateButton.$deleteButton.'
								
						</div>';
        })
		->escapeColumns([])
		->make(true);
    }
	
	public function getCompanyCservice($company_id)
	{
		$info_Cservice = $this->cserviceRps->getCompanyCservice($company_id)->pluck("name","id");
		return response()->json($info_Cservice);
	}
}
