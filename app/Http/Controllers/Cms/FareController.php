<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\FareRepository;
use App\Http\Requests\FareRequest;
use App\Repositories\GeneralRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\RouteRepository;
use App\Repositories\ScheduleRepository;
use DataTables;
use Session;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Auth;

class FareController extends Controller
{
	protected $companyRps;
	protected $fareRps;
	protected $generalRps;
	protected $routeRps;
	protected $serviceRps;
	protected $scheduleRps;
	public $guard = "admin";
	public $guard_url = "/admin/";
	protected $viewPermission = "View Schedule";
	protected $addPermission = "Add Schedule";
	protected $updatePermission = "Update Schedule";
	protected $deletePermission = "Delete Schedule";
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(GeneralRepository $generalRps, CompanyRepository $companyRps, RouteRepository $routeRps, FareRepository $fareRps, ScheduleRepository $scheduleRps)
    {
		if(str_replace("/","",request()->route()->getPrefix())=="admin")
		{
			$this->guard = "admin";
			$this->guard_url = "/admin/";
		}
		else
		{
			$this->guard = "web";
			$this->guard_url = "/company/";
		}
		$this->middleware('role_and_permission:Admin|'.$this->viewPermission.','.$this->guard);		
		
		$this->companyRps = $companyRps;
        $this->fareRps = $fareRps;
		$this->routeRps = $routeRps;
		$this->scheduleRps = $scheduleRps;
		$this->generalRps = $generalRps;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		if(!isset($request->ScheduleID) || $request->ScheduleID=="")
		{
			return 404;
		}
		$info_Schedule = $this->scheduleRps->getSchedule($request->ScheduleID);
		if($info_Schedule)
		{
			$info_FareTypes = $info_Schedule->Fare_type()->OrderBy('name')->Get();
			return view('cms.fare.index', array('info_Schedule' => $info_Schedule, 'info_FareTypes' => $info_FareTypes));
		}
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		if (!Auth::guard($this->guard)->user()->can($this->addPermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->addPermission]);
		}
		
		$this->generalRps->addLog(['action' => 'Add fare']);
		
		$inputs = $request->all();
		
		$this->fareRps->deleteFare($inputs['company_id'], $inputs['schedule_id'], $inputs['fare_type_id']);
		
		foreach($request->all() as $index => $value)
		{
			if (strpos($index, 'terminal_') !== false) 
			{
				
				$Terminals = explode("_",$index);
				$inputs['from_terminal_id'] = $Terminals[1];
				$inputs['to_terminal_id'] = $Terminals[2];
				$inputs['fare'] = $value;
				
				$this->fareRps->addFare($inputs);
			}
		}

		Session::flash('flash_message', 'Fare successfully added!');
		return redirect($this->guard_url.'fare?ScheduleID='.$request->schedule_id);
    }

	public function getCompanyScheduleFare(Request $request, $company_id)
	{
		$info_Fares = $this->fareRps->getCompanyScheduleFare($company_id, $request->schedule_id, $request->fare_type_id)->Get();
		$Response = array();
		foreach($info_Fares as $info_Fare)
		{
			$Response[] = ["id" => "terminal_".$info_Fare->from_terminal_id."_".$info_Fare->to_terminal_id, "price" => $info_Fare->fare, "fare_return" => $info_Fare->fare_return];
		}
		return response()->json($Response);
	}
	
	public function getCompanyFare($company_id)
	{
		$info_Fare = $this->fareRps->getCompanyFare($company_id)->pluck("fare","id");
		return response()->json($info_Fare);
	}
}
