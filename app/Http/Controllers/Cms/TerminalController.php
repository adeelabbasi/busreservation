<?php 

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\TerminalRepository;
use App\Http\Requests\TerminalRequest;
use App\Repositories\GeneralRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\CountryRepository;
use App\Repositories\CityRepository;
use App\Repositories\LgaRepository;
use DataTables;
use Session;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Auth;

class TerminalController extends Controller
{
	protected $companyRps;
	protected $terminalRps;
	protected $generalRps;
	protected $countryRps;
	protected $cityRps;
	protected $lgaRps;
	public $guard = "admin";
	public $guard_url = "/admin/";
	protected $viewPermission = "View Terminal";
	protected $addPermission = "Add Terminal";
	protected $updatePermission = "Update Terminal";
	protected $deletePermission = "Delete Terminal";
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(GeneralRepository $generalRps, CompanyRepository $companyRps, TerminalRepository $terminalRps, CountryRepository $countryRps, CityRepository $cityRps, LgaRepository $lgaRps)
    { 
		if(str_replace("/","",request()->route()->getPrefix())=="admin")
		{
			$this->guard = "admin";
			$this->guard_url = "/admin/";
		}
		else
		{
			$this->guard = "web";
			$this->guard_url = "/company/";
		}
		$this->middleware('role_and_permission:Admin|'.$this->viewPermission.','.$this->guard);
		$this->companyRps = $companyRps;
        $this->terminalRps = $terminalRps;
		$this->countryRps = $countryRps;
		$this->cityRps = $cityRps;
		$this->lgaRps = $lgaRps;
		$this->generalRps = $generalRps;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('cms.terminal.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		if (!Auth::guard($this->guard)->user()->can($this->addPermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->addPermission]);
		}
		$info_Countries = $this->countryRps->getCountry();
		if($this->guard == "admin")
		{
			$info_Companies = $this->companyRps->getCompany();
		}
		else
		{
			$info_Companies = [];
		}
		return view('cms.terminal.add', array('info_Companies' => $info_Companies, 'info_Countries' => $info_Countries));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TerminalRequest $request)
    {
		if (!Auth::guard($this->guard)->user()->can($this->addPermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->addPermission]);
		}
		$this->generalRps->addLog(['action' => 'Add terminal']);
        $this->terminalRps->addTerminal($request->all());
		Session::flash('flash_message', 'Terminal successfully added!');
		return redirect($this->guard_url.'terminal');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Terminal  $foreman
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$info_Terminal = $this->terminalRps->getTerminal($id);
		return view('cms.terminal.show' ,array('info_Terminal' => $info_Terminal));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Terminal  $foreman
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->updatePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
        $info_Terminal = $this->terminalRps->getTerminal($id);
		$info_Terminal->country_id = $info_Terminal->Lga()->First()->City()->First()->Country()->First()->id;
		$info_Terminal->city_id = $info_Terminal->Lga()->First()->City()->First()->id;
		$info_Countries = $this->countryRps->getCountry();
		if($this->guard == "admin")
		{
			$info_Companies = $this->companyRps->getCompany();
		}
		else
		{
			$info_Companies = [];
		}
		$info_Cities = $this->cityRps->getCity();
		$info_Lgas = $this->lgaRps->getLga();
		return view('cms.terminal.edit' ,array('info_Terminal' => $info_Terminal, 'info_Companies' => $info_Companies, 'info_Countries' => $info_Countries, 'info_Cities' => $info_Cities, 'info_Lgas' => $info_Lgas));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Terminal  $foreman
     * @return \Illuminate\Http\Response
     */
    public function update(TerminalRequest $request, $id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->updatePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
		$this->generalRps->addLog(['action' => 'Update terminal']);
        $this->terminalRps->updateTerminal($request->all() , $id);
		Session::flash('flash_message', 'Terminal successfully updated!');
		return redirect($this->guard_url.'terminal');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Terminal  $foreman
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->deletePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
		$this->generalRps->addLog(['action' => 'Delete terminal']);
        $this->terminalRps->deleteTerminal($id);
    }
	
	public function updateSequence(Request $request, $id)
	{
		$info_Terminal = $this->terminalRps->updateSequence($id, $request->sequence);
		
		return [];
	}
	
	/**
     * Create datatable grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function grid()
    {
		if($this->guard == "admin")
		{
			 $info_Terminal = $this->terminalRps->getTerminal();
		}
		else
		{
			 $info_Terminal = $this->terminalRps->getCompanyTerminal(\Auth::guard($this->guard)->User()->Company()->First()->id);
		}
	  
	   	return Datatables::of($info_Terminal)
		->editColumn('company_id', function ($info_Terminal) {
			return $info_Terminal->Company()->First()->name;
        })
		->addColumn('country_id', function ($info_Terminal) {
			return $info_Terminal->Lga()->First()->City()->First()->Country()->First()->name;
        })
		->addColumn('city_id', function ($info_Terminal) {
			return $info_Terminal->Lga()->First()->City()->First()->name;
        })
		->editColumn('lga_id', function ($info_Terminal) {
			return $info_Terminal->Lga()->First()->name;
        })
		->editColumn('status', function ($info_Terminal) {
			 if($info_Terminal->status==1)
				return "Active";
			 else
				return "Inactive";
				 	
        })
		->editColumn('sequence', function ($info_Terminal) {
				 return '<input class="sequence_id" data-terminal_id="'.$info_Terminal->id.'" value="'.$info_Terminal->sequence.'" />';
        })
		->addColumn('edit', function ($info_Terminal) {
				 $updateButton = "";
				 $deleteButton = "";
				 if (Auth::guard($this->guard)->user()->can($this->updatePermission)) 
				 {
					$updateButton = '<a class="btn-floating waves-effect waves-light green" style="margin-right:2px;" href="'.url($this->guard_url.'terminal/'.$info_Terminal->id.'/edit').'" title="Edit Data"><i class="large material-icons">mode_edit</i></a>';
				 }
				 if (Auth::guard($this->guard)->user()->can($this->deletePermission)) 
				 {
					$deleteButton = '<a class="btn-floating waves-effect waves-light red" href="javascript(0)" title="Delete Data" id="btnDelete" name="btnDelete" data-message="Please sure that you have deleted all routes against this terminal!" data-remote="'.$this->guard_url.'terminal/' . $info_Terminal->id . '"><i class="material-icons">clear</i></a>';
				 }
				 return '<div class="btn-group btn-group-action">
								<a class="btn-floating waves-effect waves-light blue" style="margin-right:2px;" href="'.url($this->guard_url.'terminal/'.$info_Terminal->id).'" title="View Detail"><i class="large material-icons">pageview</i></a> 
								'.$updateButton.$deleteButton.'
								
						</div>';
        })
		->escapeColumns([])
		->make(true);
    }
	
	public function getCompanyTerminal($company_id)
	{
		$info_Terminal = $this->terminalRps->getCompanyTerminal($company_id)->pluck("name","id");
		return response()->json($info_Terminal);
	}
	
	public function getCompanyCityTerminal(Request $request, $company_id)
	{
		$input = $request->all();
		$lga_ids =  $this->lgaRps->getCityLga($input['city_id'])->pluck('id');
		return $info_Terminal = $this->terminalRps->getCompanyCityTerminal($company_id, $lga_ids)->pluck("name","id");
		return response()->json($info_Terminal);
	}
}
