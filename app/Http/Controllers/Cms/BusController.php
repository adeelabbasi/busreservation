<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\BusRepository;
use App\Http\Requests\BusRequest;
use App\Repositories\GeneralRepository;
use App\Repositories\CompanyRepository;
use DataTables;
use Session;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Auth;

class BusController extends Controller
{
	protected $companyRps;
	protected $busRps;
	protected $generalRps;
	public $guard = "admin";
	public $guard_url = "/admin/";
	protected $viewPermission = "View Fleet";
	protected $addPermission = "Add Fleet";
	protected $updatePermission = "Update Fleet";
	protected $deletePermission = "Delete Fleet";
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(GeneralRepository $generalRps, CompanyRepository $companyRps, BusRepository $busRps)
    { 
		if(str_replace("/","",request()->route()->getPrefix())=="admin")
		{
			$this->guard = "admin";
			$this->guard_url = "/admin/";
		}
		else
		{
			$this->guard = "web";
			$this->guard_url = "/company/";
		}
		$this->middleware('role_and_permission:Admin|'.$this->viewPermission.','.$this->guard);		$this->companyRps = $companyRps;
        $this->busRps = $busRps;
		$this->generalRps = $generalRps;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('cms.bus.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		if (!Auth::guard($this->guard)->user()->can($this->addPermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->addPermission]);
		}
		if($this->guard == "admin")
		{
			$info_Companies = $this->companyRps->getCompany();
		}
		else
		{
			$info_Companies = [];
		}
		return view('cms.bus.add', array('info_Companies' => $info_Companies));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BusRequest $request)
    {
		if (!Auth::guard($this->guard)->user()->can($this->addPermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->addPermission]);
		}
		$this->generalRps->addLog(['action' => 'Add bus']);
        $this->busRps->addBus($request->all());
		Session::flash('flash_message', 'Bus successfully added!');
		return redirect($this->guard_url.'bus');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bus  $foreman
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$info_Bus = $this->busRps->getBus($id);
		return view('cms.bus.show' ,array('info_Bus' => $info_Bus));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bus  $foreman
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->updatePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
        $info_Bus = $this->busRps->getBus($id);
		if($this->guard == "admin")
		{
			$info_Companies = $this->companyRps->getCompany();
		}
		else
		{
			$info_Companies = [];
		}
		return view('cms.bus.edit' ,array('info_Bus' => $info_Bus, 'info_Companies' => $info_Companies));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bus  $foreman
     * @return \Illuminate\Http\Response
     */
    public function update(BusRequest $request, $id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->updatePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
		$this->generalRps->addLog(['action' => 'Update bus']);
        $this->busRps->updateBus($request->all() , $id);
		Session::flash('flash_message', 'Bus successfully updated!');
		return redirect($this->guard_url.'bus');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bus  $foreman
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->deletePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
        $this->generalRps->addLog(['action' => 'Delete bus']);
        $this->busRps->deleteBus($id);
    }
	
	/**
     * Create datatable grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function grid()
    {
	   	if($this->guard == "admin")
		{
			 $info_Bus = $this->busRps->getBus();
		}
		else
		{
			 $info_Bus = $this->busRps->getCompanyBus(\Auth::guard($this->guard)->User()->Company()->First()->id);
		}
	   	return Datatables::of($info_Bus)
		->editColumn('company_id', function ($info_Bus) {
			return $info_Bus->Company()->First()->name;
        })
		->editColumn('status', function ($info_Bus) {
			 if($info_Bus->status==1)
				return "Active";
			 else
				return "Inactive";
				 	
        })
		->addColumn('edit', function ($info_Bus) {
				 $updateButton = "";
				 $deleteButton = "";
				 if (Auth::guard($this->guard)->user()->can($this->updatePermission)) 
				 {
					$updateButton = '<a class="btn-floating waves-effect waves-light green" style="margin-right:2px;" href="'.url($this->guard_url.'bus/'.$info_Bus->id.'/edit').'" title="Edit Data"><i class="large material-icons">mode_edit</i></a>';
				 }
				 if (Auth::guard($this->guard)->user()->can($this->deletePermission)) 
				 {
					$deleteButton = '<a class="btn-floating waves-effect waves-light red" href="javascript(0)" title="Delete Data" id="btnDelete" name="btnDelete" data-message="Please sure that you have deleted all schedules against this fleet!" data-remote="'.$this->guard_url.'bus/' . $info_Bus->id . '"><i class="material-icons">clear</i></a>';
				 }
				 return '<div class="btn-group btn-group-action">
								<a class="btn-floating waves-effect waves-light blue" style="margin-right:2px;" href="'.url($this->guard_url.'bus/'.$info_Bus->id).'" title="View Detail"><i class="large material-icons">pageview</i></a> 
								'.$updateButton.$deleteButton.'
								
						</div>';
        })
		->escapeColumns([])
		->make(true);
    }
	
	public function getCompanyBus($company_id)
	{
		$info_Bus = $this->busRps->getCompanyBus($company_id)->pluck("name","id");
		return response()->json($info_Bus);
	}
}
