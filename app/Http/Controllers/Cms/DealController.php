<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\DealRepository;
use App\Http\Requests\DealRequest;
use App\Repositories\GeneralRepository;
use App\Repositories\CompanyRepository;
use DataTables;
use Session;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Auth;

class DealController extends Controller
{
	protected $companyRps;
	protected $dealRps;
	protected $generalRps;
	public $guard = "admin";
	public $guard_url = "/admin/";
	protected $viewPermission = "View Promotion/Deals";
	protected $addPermission = "Add Promotion/Deals";
	protected $updatePermission = "Update Promotion/Deals";
	protected $deletePermission = "Delete Promotion/Deals";
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(GeneralRepository $generalRps, CompanyRepository $companyRps, DealRepository $dealRps)
    {
		if(str_replace("/","",request()->route()->getPrefix())=="admin")
		{
			$this->guard = "admin";
			$this->guard_url = "/admin/";
		}
		else
		{
			$this->guard = "web";
			$this->guard_url = "/company/";
		}
		$this->middleware('role_and_permission:Admin|'.$this->viewPermission.','.$this->guard);		$this->companyRps = $companyRps;
        $this->dealRps = $dealRps;
		$this->generalRps = $generalRps;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('cms.deal.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		if (!Auth::guard($this->guard)->user()->can($this->addPermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->addPermission]);
		}
		if($this->guard == "admin")
		{
			$info_Companies = $this->companyRps->getCompany();
		}
		else
		{
			$info_Companies = [];
		}
		return view('cms.deal.add', array('info_Companies' => $info_Companies));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DealRequest $request)
    {
		if (!Auth::guard($this->guard)->user()->can($this->addPermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->addPermission]);
		}
		$this->generalRps->addLog(['action' => 'Add deal']);
        $this->dealRps->addDeal($request->all());
		Session::flash('flash_message', 'Deal successfully added!');
		return redirect($this->guard_url.'deal');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Deal  $foreman
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$info_Deal = $this->dealRps->getDeal($id);
		return view('cms.deal.show' ,array('info_Deal' => $info_Deal));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Deal  $foreman
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->updatePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
        $info_Deal = $this->dealRps->getDeal($id);
		if($this->guard == "admin")
		{
			$info_Companies = $this->companyRps->getCompany();
		}
		else
		{
			$info_Companies = [];
		}
		return view('cms.deal.edit' ,array('info_Deal' => $info_Deal, 'info_Companies' => $info_Companies));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Deal  $foreman
     * @return \Illuminate\Http\Response
     */
    public function update(DealRequest $request, $id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->updatePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
		$this->generalRps->addLog(['action' => 'Update deal']);
        $this->dealRps->updateDeal($request->all() , $id);
		Session::flash('flash_message', 'Deal successfully updated!');
		return redirect($this->guard_url.'deal');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Deal  $foreman
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->deletePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->deletePermission]);
		}
        $this->generalRps->addLog(['action' => 'Delete deal']);
        $this->dealRps->deleteDeal($id);
    }
	
	/**
     * Create datatable grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function grid()
    {
	   	if($this->guard == "admin")
		{
			 $info_Deal = $this->dealRps->getDeal();
		}
		else
		{
			 $info_Deal = $this->dealRps->getCompanyDeal(\Auth::guard($this->guard)->User()->Company()->First()->id);
		}
	   	return Datatables::of($info_Deal)
		->editColumn('company_id', function ($info_Deal) {
			return $info_Deal->Company()->First()->name;
        })
		->editColumn('status', function ($info_Deal) {
			 if($info_Deal->status==1)
				return "Active";
			 else
				return "Inactive";
				 	
        })
		->addColumn('edit', function ($info_Deal) {
				 $updateButton = "";
				 $deleteButton = "";
				 if (Auth::guard($this->guard)->user()->can($this->updatePermission)) 
				 {
					$updateButton = '<a class="btn-floating waves-effect waves-light green" style="margin-right:2px;" href="'.url($this->guard_url.'deal/'.$info_Deal->id.'/edit').'" title="Edit Data"><i class="large material-icons">mode_edit</i></a>';
				 }
				 if (Auth::guard($this->guard)->user()->can($this->deletePermission)) 
				 {
					$deleteButton = '<a class="btn-floating waves-effect waves-light red" href="javascript(0)" title="Delete Data" id="btnDelete" name="btnDelete" data-message="Are you sure to delete deal?" data-remote="'.$this->guard_url.'deal/' . $info_Deal->id . '"><i class="material-icons">clear</i></a>';
				 }
				 return '<div class="btn-group btn-group-action">
								<a class="btn-floating waves-effect waves-light blue" style="margin-right:2px;" href="'.url($this->guard_url.'deal/'.$info_Deal->id).'" title="View Detail"><i class="large material-icons">pageview</i></a> 
								'.$updateButton.$deleteButton.'
								
						</div>';
        })
		->escapeColumns([])
		->make(true);
    }
	
	public function getCompanyDeal($company_id)
	{
		$info_Deal = $this->dealRps->getCompanyDeal($company_id)->pluck("name","id");
		return response()->json($info_Deal);
	}
}
