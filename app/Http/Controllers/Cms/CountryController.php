<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\CountryRepository;
use App\Http\Requests\CountryRequest;
use App\Repositories\GeneralRepository;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Auth;
use DataTables;
use Session;

class CountryController extends Controller
{
	protected $countryRps;
	protected $generalRps;
	public $guard = "admin";
	public $guard_url = "/admin/";
	protected $viewPermission = "View Country";
	protected $addPermission = "Add Country";
	protected $updatePermission = "Update Country";
	protected $deletePermission = "Delete Country";
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(GeneralRepository $generalRps, CountryRepository $countryRps)
    { 
		if(str_replace("/","",request()->route()->getPrefix())=="admin")
		{
			$this->guard = "admin";
			$this->guard_url = "/admin/";
		}
		else
		{
			$this->guard = "web";
			$this->guard_url = "/company/";
		}
		$this->middleware('role_and_permission:Admin|'.$this->viewPermission.','.$this->guard);
        $this->countryRps = $countryRps;
		$this->generalRps = $generalRps;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('cms.country.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		if (!Auth::guard($this->guard)->user()->can($this->addPermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->addPermission]);
		}
        return view('cms.country.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CountryRequest $request)
    {
		if (!Auth::guard($this->guard)->user()->can($this->addPermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->addPermission]);
		}
		$this->generalRps->addLog(['action' => 'Add country']);
        $this->countryRps->addCountry($request->all());
		Session::flash('flash_message', 'Country successfully added!');
		return redirect($this->guard_url.'country');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Country  $foreman
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$info_Country = $this->countryRps->getCountry($id);
		return view('cms.country.show' ,array('info_Country' => $info_Country));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Country  $foreman
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->updatePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
        $info_Country = $this->countryRps->getCountry($id);
		return view('cms.country.edit' ,array('info_Country' => $info_Country));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Country  $foreman
     * @return \Illuminate\Http\Response
     */
    public function update(CountryRequest $request, $id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->updatePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
		$this->generalRps->addLog(['action' => 'Update country']);
        $this->countryRps->updateCountry($request->all() , $id);
		Session::flash('flash_message', 'Country successfully updated!');
		return redirect($this->guard_url.'country');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Country  $foreman
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->deletePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->deletePermission]);
		}
		$this->generalRps->addLog(['action' => 'Delete country']);
        $this->countryRps->deleteCountry($id);
    }
	
	/**
     * Create datatable grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function grid()
    {
	   $info_Country = $this->countryRps->getCountry();
	   return Datatables::of($info_Country)
		->editColumn('status', function ($info_Country) {
			 if($info_Country->status==1)
				return "Active";
			 else
				return "Inactive";
				 	
        })
		->addColumn('edit', function ($info_Country) {
				 $updateButton = "";
				 $deleteButton = "";
				 if (Auth::guard($this->guard)->user()->can($this->updatePermission)) 
				 {
					$updateButton = '<a class="btn-floating waves-effect waves-light green" style="margin-right:2px;" href="'.url($this->guard_url.'country/'.$info_Country->id.'/edit').'" title="Edit Data"><i class="large material-icons">mode_edit</i></a>';
				 }
				 if (Auth::guard($this->guard)->user()->can($this->deletePermission)) 
				 {
					$deleteButton = '<a class="btn-floating waves-effect waves-light red" href="javascript(0)" title="Delete Data" id="btnDelete" name="btnDelete" data-message="Are you sure to delete country?" data-remote="'.$this->guard_url.'country/' . $info_Country->id . '"><i class="material-icons">clear</i></a>';
				 }
				 return '<div class="btn-group btn-group-action">
								<a class="btn-floating waves-effect waves-light blue" style="margin-right:2px;" href="'.url($this->guard_url.'country/'.$info_Country->id).'" title="View Detail"><i class="large material-icons">pageview</i></a> 
								'.$updateButton.$deleteButton.'
								
						</div>';
        })
		->escapeColumns([])
		->make(true);
    }
}
