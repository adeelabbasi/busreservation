<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\PaymentRepository;
use App\Repositories\GeneralRepository;
use DataTables;
use Session;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Auth;

class PaymentController extends Controller
{
	protected $paymentRps;
	protected $generalRps;
	public $guard = "admin";
	public $guard_url = "/admin/";
	protected $viewPermission = "View Payment";
	protected $addPermission = "Add Payment";
	protected $updatePermission = "Update Payment";
	protected $deletePermission = "Delete Payment";
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(GeneralRepository $generalRps, PaymentRepository $paymentRps)
    { 
		if(str_replace("/","",request()->route()->getPrefix())=="admin")
		{
			$this->guard = "admin";
			$this->guard_url = "/admin/";
		}
		else
		{
			$this->guard = "web";
			$this->guard_url = "/company/";
		}
		$this->middleware('role_and_permission:Admin|'.$this->viewPermission.','.$this->guard);		
        $this->paymentRps = $paymentRps;
		$this->generalRps = $generalRps;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('cms.payment.index');
    }

	/**
     * Create datatable grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function grid(Request $request)
    {
	   	if($this->guard == "admin")
		{
			 $info_Payment = $this->paymentRps->getPayment();
		}
		else
		{
			 $info_Payment = $this->paymentRps->getCompanyPayment(\Auth::guard($this->guard)->User()->Company()->First()->id);
		}
		if($request->Search)
		{
			$info_Payment = $info_Payment->Where('created_at', '>=' ,$request->from_date)->Where('created_at', '<=' ,$request->to_date);
		}
	   	return Datatables::of($info_Payment)
		->addColumn('company_id', function ($info_Payment) {
			return $info_Payment->Company()->First()->name;
				 	
        })
		->editColumn('msisdn', function ($info_Booking) {
				return "0".substr($info_Booking->msisdn,-10);
				 	
        })
		->editColumn('discount', function ($info_Payment) {
			$discount = $info_Payment->Booking()->First()->total-$info_Payment->amount;
			return $discount < 0 ? 0 : $discount ;
				 	
        })
		->editColumn('commission', function ($info_Payment) {
			$commission = $info_Payment->Booking()->First()->commission;
			return $commission < 0 ? 0 : $commission ;
				 	
        })
		->editColumn('no_of_seats', function ($info_Payment) {
			$discount = $info_Payment->Booking()->First()->SelectedSeats;
			return $discount < 0 ? 0 : $discount ;
				 	
        })
		->editColumn('booking_id', function ($info_Payment) {
			return $info_Payment->Booking()->First()->from_terminal_name." to ".$info_Payment->Booking()->First()->to_terminal_name;
				 	
        })
		->editColumn('status', function ($info_Payment) {
			return 'Paid';
				 	
        })
		->escapeColumns([])
		->make(true);
    }

}
