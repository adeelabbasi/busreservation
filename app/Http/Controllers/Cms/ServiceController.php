<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ServiceRepository;
use App\Http\Requests\ServiceRequest;
use App\Repositories\GeneralRepository;
use App\Repositories\CompanyRepository;
use DataTables;
use Session;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Auth;

class ServiceController extends Controller
{
	protected $companyRps;
	protected $serviceRps;
	protected $generalRps;
	public $guard = "admin";
	public $guard_url = "/admin/";
	protected $viewPermission = "View Service";
	protected $addPermission = "Add Service";
	protected $updatePermission = "Update Service";
	protected $deletePermission = "Delete Service";
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(GeneralRepository $generalRps, CompanyRepository $companyRps, ServiceRepository $serviceRps)
    { 
		if(str_replace("/","",request()->route()->getPrefix())=="admin")
		{
			$this->guard = "admin";
			$this->guard_url = "/admin/";
		}
		else
		{
			$this->guard = "web";
			$this->guard_url = "/company/";
		}
		$this->middleware('role_and_permission:Admin|'.$this->viewPermission.','.$this->guard);		$this->companyRps = $companyRps;
        $this->serviceRps = $serviceRps;
		$this->generalRps = $generalRps;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('cms.service.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		if (!Auth::guard($this->guard)->user()->can($this->addPermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->addPermission]);
		}
		if($this->guard == "admin")
		{
			$info_Companies = $this->companyRps->getCompany();
		}
		else
		{
			$info_Companies = [];
		}
		return view('cms.service.add', array('info_Companies' => $info_Companies));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceRequest $request)
    {
		if (!Auth::guard($this->guard)->user()->can($this->addPermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->addPermission]);
		}
		$this->generalRps->addLog(['action' => 'Add service']);
        $this->serviceRps->addService($request->all());
		Session::flash('flash_message', 'Service successfully added!');
		return redirect($this->guard_url.'service');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $foreman
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$info_Service = $this->serviceRps->getService($id);
		return view('cms.service.show' ,array('info_Service' => $info_Service));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $foreman
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->updatePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
        $info_Service = $this->serviceRps->getService($id);
		if($this->guard == "admin")
		{
			$info_Companies = $this->companyRps->getCompany();
		}
		else
		{
			$info_Companies = [];
		}
		return view('cms.service.edit' ,array('info_Service' => $info_Service, 'info_Companies' => $info_Companies));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $foreman
     * @return \Illuminate\Http\Response
     */
    public function update(ServiceRequest $request, $id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->updatePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
		$this->generalRps->addLog(['action' => 'Update service']);
        $this->serviceRps->updateService($request->all() , $id);
		Session::flash('flash_message', 'Service successfully updated!');
		return redirect($this->guard_url.'service');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $foreman
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->deletePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
        $this->generalRps->addLog(['action' => 'Delete service']);
        $this->serviceRps->deleteService($id);
    }
	
	/**
     * Create datatable grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function grid()
    {
	   	if($this->guard == "admin")
		{
			 $info_Service = $this->serviceRps->getService();
		}
		else
		{
			 $info_Service = $this->serviceRps->getCompanyService(\Auth::guard($this->guard)->User()->Company()->First()->id);
		}
	   	return Datatables::of($info_Service)
		->editColumn('company_id', function ($info_Service) {
			return $info_Service->Company()->First()->name;
        })
		->editColumn('status', function ($info_Service) {
			 if($info_Service->status==1)
				return "Active";
			 else
				return "Inactive";
				 	
        })
		->addColumn('edit', function ($info_Service) {
				 $updateButton = "";
				 $deleteButton = "";
				 if (Auth::guard($this->guard)->user()->can($this->updatePermission)) 
				 {
					$updateButton = '<a class="btn-floating waves-effect waves-light green" style="margin-right:2px;" href="'.url($this->guard_url.'service/'.$info_Service->id.'/edit').'" title="Edit Data"><i class="large material-icons">mode_edit</i></a>';
				 }
				 if (Auth::guard($this->guard)->user()->can($this->deletePermission)) 
				 {
					$deleteButton = '<a class="btn-floating waves-effect waves-light red" href="javascript(0)" title="Delete Data" id="btnDelete" name="btnDelete" data-message="Please sure that you have deleted all fare types against this service!" data-remote="'.$this->guard_url.'service/' . $info_Service->id . '"><i class="material-icons">clear</i></a>';
				 }
				 return '<div class="btn-group btn-group-action">
								<a class="btn-floating waves-effect waves-light blue" style="margin-right:2px;" href="'.url($this->guard_url.'service/'.$info_Service->id).'" title="View Detail"><i class="large material-icons">pageview</i></a> 
								'.$updateButton.$deleteButton.'
								
						</div>';
        })
		->escapeColumns([])
		->make(true);
    }
	
	public function getCompanyService($company_id)
	{
		$info_Service = $this->serviceRps->getCompanyService($company_id)->pluck("name","id");
		return response()->json($info_Service);
	}
}
