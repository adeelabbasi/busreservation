<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
//Importing laravel-permission models
use App\Models\Role;
use Spatie\Permission\Models\Permission;
use DataTables;
use DB;
use Spatie\Permission\Exceptions\UnauthorizedException;
use App\Repositories\GeneralRepository;
use App\Repositories\CompanyRepository;
use Auth;

class RoleController extends Controller
{
	public $guard = "admin";
	public $guard_url = "/admin/";
	protected $generalRps;
	protected $companyRps;
	protected $viewPermission = "View User Role";
	protected $addPermission = "Add User Role";
	protected $updatePermission = "Update User Role";
	protected $deletePermission = "Delete User Role";
	
    function __construct(GeneralRepository $generalRps, CompanyRepository $companyRps)
    {
		if(str_replace("/","",request()->route()->getPrefix())=="admin")
		{
			$this->guard = "admin";
			$this->guard_url = "/admin/";
		}
		else
		{
			$this->guard = "web";
			$this->guard_url = "/company/";
		}
		$this->middleware('role_and_permission:Admin|'.$this->viewPermission.','.$this->guard);		
		$this->companyRps = $companyRps;
		$this->generalRps = $generalRps;
    }

    public function index()
    {
        //$info_Roles = Role::where('guard_name','admin')->orderBy('id','DESC')->paginate(5);
        return view('cms.role.index');
    }


    public function create()
    {
		if (!Auth::guard($this->guard)->user()->can($this->addPermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->addPermission]);
		}
		$this->generalRps->addLog(['action' => 'Add role']);
		if($this->guard == "admin")
		{
			$info_Companies = $this->companyRps->getCompany();
			$info_Permissions = Permission::where('guard_name','admin')->Get();
			$info_PermissionsWeb = Permission::where('guard_name','web')->Get();
		}
		else
		{
			$info_Companies = [];
			//$info_Permissions = Permission::where('guard_name',$this->guard)->where('company_id',\Auth::guard($this->guard)->User()->Company()->First()->id)->Get();
			$info_Permissions = [];
			$info_PermissionsWeb = Permission::where('guard_name','web')->Get();
		}
        
        return view('cms.role.add',array('info_Permissions' => $info_Permissions, 'info_PermissionsWeb' => $info_PermissionsWeb, 'info_Companies' => $info_Companies ));
    }


    public function store(Request $request)
    {
		//return $request->all();
		if (!Auth::guard($this->guard)->user()->can($this->addPermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->addPermission]);
		}
        $this->validate($request, [
            'name' => 'required',
            'permission' => 'required',
        ]);
		
		if($request->input('company_id')=="-1")
	        $info_Roles = Role::create(['guard_name' => 'admin', 'name' => $request->input('name')]);
		else
			$info_Roles = Role::create(['guard_name' => 'web', 'company_id' => $request->input('company_id'), 'name' => $request->input('name')]);
			
        $info_Roles->syncPermissions($request->input('permission'));

		return redirect($this->guard_url.'role')->with('success','Role created successfully');
		
    }


    public function show($id)
    {
        $info_Role = Role::find($id);
		
		if($this->guard == "admin")
		{
			if($info_Role->company_id=="")
				$info_Permissions = Permission::where('guard_name','admin')->Get();
			else
				$info_Permissions = Permission::where('guard_name','web')->Get();
				
        	$rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')->all();
		}
		else
		{
			$info_Permissions = Permission::where('guard_name','web')->Get();
        	$rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')->all();
		}

        return view('cms.role.show',array('info_Role' => $info_Role,'info_Permissions' => $info_Permissions,'rolePermissions' => $rolePermissions));
    }


    public function edit($id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->updatePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
        $info_Role = Role::find($id);
		
		if($this->guard == "admin")
		{
			$info_Role->company_id=($info_Role->company_id==""?"-1":$info_Role->company_id);
			$info_Companies = $this->companyRps->getCompany();
			$info_Permissions = Permission::where('guard_name','admin')->Get();
			$rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')->all();
			
			$info_PermissionsWeb = Permission::where('guard_name','web')->Get();
			$rolePermissionsWeb = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')->all();
		}
		else
		{
			$info_Companies = [];
			$info_Permissions = [];
			$rolePermissions = [];
			
			$info_PermissionsWeb = Permission::where('guard_name','web')->Get();
			$rolePermissionsWeb = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')->all();
		}
        
		
        return view('cms.role.edit',array('info_Companies' => $info_Companies,'info_Role' => $info_Role,'info_Permissions' => $info_Permissions,'rolePermissions' => $rolePermissions,'info_PermissionsWeb' => $info_PermissionsWeb,'rolePermissionsWeb' => $rolePermissionsWeb));
    }


    public function update(Request $request, $id)
    {
	  //return $request->input('permission');
       if (!Auth::guard($this->guard)->user()->can($this->updatePermission)) 
	   {
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
	   }
	   $this->generalRps->addLog(['action' => 'Update role']);
	   $this->validate($request, [
            'name' => 'required',
            'permission' => 'required',
        ]);

        $info_Roles = Role::find($id);
        $info_Roles->name = $request->input('name');
		if($request->input('company_id')=="-1")
	    {  
			$info_Roles->company_id = NULL;
			$info_Roles->guard_name = 'admin';
			
		}
		else
		{
			$info_Roles->company_id = $request->input('company_id');
			$info_Roles->guard_name = 'web';
		}
		
        $info_Roles->save();
		//return $request->input('permission');
        $info_Roles->syncPermissions($request->input('permission'));

        return redirect($this->guard_url.'role')->with('success','Role updated successfully');
                        
    }


    public function destroy($id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->deletePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
        $this->generalRps->addLog(['action' => 'Delete role']);
        DB::table("roles")->where('id',$id)->delete();
        return redirect($this->guard_url.'bus')->with('success','Role deleted successfully');
    }
	
	public function grid()
    {
	   
	   	if($this->guard == "admin")
		{
			 $info_Roles = Role::all();
		}
		else
		{
			 $info_Roles = Role::where('company_id',\Auth::guard($this->guard)->User()->Company()->First()->id)->Get();
		}
	   	return Datatables::of($info_Roles)
		->addColumn('guard_name', function ($info_Roles) {
			if($info_Roles->guard_name=="web")
			{
				if($info_Roles->company_id!='' && $info_Roles->Company()->First())
				{
					return $info_Roles->Company()->First()->name;
				}
				else
				{
					return 'Superadmin';
				}
			}
			else
			{
				return "Superadmin";
			}
        })
		->addColumn('edit', function ($info_Roles) {
				 $updateButton = "";
				 $deleteButton = "";
				 if (Auth::guard($this->guard)->user()->can($this->updatePermission)) 
				 {
					$updateButton = '<a class="btn-floating waves-effect waves-light green" style="margin-right:2px;" href="'.url($this->guard_url.'role/'.$info_Roles->id.'/edit').'" title="Edit Data"><i class="large material-icons">mode_edit</i></a>';
				 }
				 if (Auth::guard($this->guard)->user()->can($this->deletePermission)) 
				 {
					$deleteButton = '<a class="btn-floating waves-effect waves-light red" href="javascript(0)" title="Delete Data" id="btnDelete" name="btnDelete" data-message="Are you sure to delete role?" data-remote='.$this->guard_url.'role/' . $info_Roles->id . '"><i class="material-icons">clear</i></a>';
				 }
				 return '<div class="btn-group btn-group-action">
								'.$updateButton.$deleteButton.'
								
						</div>';
        })
		->escapeColumns([])
		->make(true);
    }
	
	public function getUserTypeRole($company_id)
	{
		if($company_id=="-1")
		{
			$info_Role = Role::Where('guard_name','admin')->pluck("name","id");
		}
		else
		{
			$info_Role = Role::Where('company_id',$company_id)->pluck("name","id");
		}
		return response()->json($info_Role);
	}
}