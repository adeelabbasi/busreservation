<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\RouteRepository;
use App\Http\Requests\RouteRequest;
use App\Repositories\GeneralRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\CityRepository;
use App\Repositories\TerminalRepository;
use DataTables;
use Session;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Auth;

class RouteController extends Controller
{
	protected $companyRps;
	protected $routeRps;
	protected $generalRps;
	protected $terminalRps;
	protected $cityRps;
	public $guard = "admin";
	public $guard_url = "/admin/";
	protected $viewPermission = "View Route";
	protected $addPermission = "Add Route";
	protected $updatePermission = "Update Route";
	protected $deletePermission = "Delete Route";
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(GeneralRepository $generalRps, CompanyRepository $companyRps, RouteRepository $routeRps, TerminalRepository $terminalRps, CityRepository $cityRps)
    { 
		if(str_replace("/","",request()->route()->getPrefix())=="admin")
		{
			$this->guard = "admin";
			$this->guard_url = "/admin/";
		}
		else
		{
			$this->guard = "web";
			$this->guard_url = "/company/";
		}
		$this->middleware('role_and_permission:Admin|'.$this->viewPermission.','.$this->guard);		$this->companyRps = $companyRps;
        $this->routeRps = $routeRps;
		$this->terminalRps = $terminalRps;
		$this->cityRps = $cityRps;
		$this->generalRps = $generalRps;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('cms.route.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		if (!Auth::guard($this->guard)->user()->can($this->addPermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->addPermission]);
		}
		if($this->guard == "admin")
		{
			$info_Companies = $this->companyRps->getCompany();
		}
		else
		{
			$info_Companies = [];
		}
		$info_Cities = $this->cityRps->getCity();
		return view('cms.route.add', array('info_Companies' => $info_Companies, 'info_Cities' => $info_Cities));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RouteRequest $request)
    {
		if (!Auth::guard($this->guard)->user()->can($this->addPermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->addPermission]);
		}
		$this->generalRps->addLog(['action' => 'Add route']);
        $this->routeRps->addRoute($request->all());
		Session::flash('flash_message', 'Route successfully added!');
		return redirect($this->guard_url.'route');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Route  $foreman
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$info_Route = $this->routeRps->getRoute($id);
		return view('cms.route.show' ,array('info_Route' => $info_Route));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Route  $foreman
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Auth::guard($this->guard)->user()->can($this->updatePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
		$info_Route = $this->routeRps->getRoute($id);
		
		if($this->guard == "admin")
		{
			$info_Companies = $this->companyRps->getCompany();
		}
		else
		{
			$info_Companies = [];
		}
		$info_Terminals = $this->terminalRps->getCompanyTerminal($info_Route->company_id)->Get();
		$info_Cities = $this->cityRps->getCity();
		
		return view('cms.route.edit' ,array('info_Route' => $info_Route, 'info_Companies' => $info_Companies, 'info_Terminals' => $info_Terminals, 'info_Cities' => $info_Cities));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Route  $foreman
     * @return \Illuminate\Http\Response
     */
    public function update(RouteRequest $request, $id)
    {
        if (!Auth::guard($this->guard)->user()->can($this->updatePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
		$this->generalRps->addLog(['action' => 'Update route']);
		$this->routeRps->updateRoute($request->all() , $id);
		Session::flash('flash_message', 'Route successfully updated!');
		return redirect($this->guard_url.'route');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Route  $foreman
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->deletePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
        $this->generalRps->addLog(['action' => 'Delete route']);
        $this->routeRps->deleteRoute($id);
    }
	
	/**
     * Create datatable grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function grid()
    {
	   
	   if($this->guard == "admin")
		{
			 $info_Route = $this->routeRps->getRoute();
		}
		else
		{
			 $info_Route = $this->routeRps->getCompanyRoute(\Auth::guard($this->guard)->User()->Company()->First()->id);
		}
	   	return Datatables::of($info_Route)
		->editColumn('company_id', function ($info_Route) {
			return $info_Route->Company()->First()->name;
        })
		->addColumn('from_terminal_id', function ($info_Route) {
			return $info_Route->Route_detail()->OrderBy('sequence')->First()->Terminal()->First()->Lga()->First()->City()->First()->name."(".$info_Route->Route_detail()->OrderBy('sequence')->First()->Terminal()->First()->name.")";
        })
		->addColumn('to_terminal_id', function ($info_Route) {
			return $info_Route->Route_detail()->OrderBy('sequence','desc')->First()->Terminal()->First()->Lga()->First()->City()->First()->name."(".$info_Route->Route_detail()->OrderBy('sequence','desc')->First()->Terminal()->First()->name.")";
        })
		->editColumn('status', function ($info_Route) {
			 if($info_Route->status==1)
				return "Active";
			 else
				return "Inactive";
				 	
        })
		->addColumn('edit', function ($info_Route) {
				 $updateButton = "";
				 $deleteButton = "";
				 if (Auth::guard($this->guard)->user()->can($this->updatePermission)) 
				 {
					$updateButton = '<a class="btn-floating waves-effect waves-light green" style="margin-right:2px;" href="'.url($this->guard_url.'route/'.$info_Route->id.'/edit').'" title="Edit Data"><i class="large material-icons">mode_edit</i></a>';
				 }
				 if (Auth::guard($this->guard)->user()->can($this->deletePermission)) 
				 {
					$deleteButton = '<a class="btn-floating waves-effect waves-light red" href="javascript(0)" title="Delete Data" id="btnDelete" name="btnDelete" data-message="Are you sure to delete route?" data-remote="'.$this->guard_url.'route/' . $info_Route->id . '"><i class="material-icons">clear</i></a>';
				 }
				 return '<div class="btn-group btn-group-action">
								<a class="btn-floating waves-effect waves-light blue" style="margin-right:2px;" href="'.url($this->guard_url.'route/'.$info_Route->id).'" title="View Detail"><i class="large material-icons">pageview</i></a> 
								'.$updateButton.$deleteButton.'
								
						</div>';
        })
		->escapeColumns([])
		->make(true);
    }
	
	public function getCompanyRoute($company_id)
	{
		$info_Routes = $this->routeRps->getCompanyRoute($company_id)->Get();
		$Response = array();
		foreach($info_Routes as $info_Route)
		{
			$Response[] = ["id" => $info_Route->id, "name" => $info_Route->name." -> ".$info_Route->Route_detail()->OrderBy('sequence')->First()->Terminal()->First()->Lga()->First()->City()->First()->name."(".$info_Route->Route_detail()->OrderBy('sequence')->First()->Terminal()->First()->name.") to ".$info_Route->Route_detail()->OrderBy('sequence','desc')->First()->Terminal()->First()->Lga()->First()->City()->First()->name."(".$info_Route->Route_detail()->OrderBy('sequence','desc')->First()->Terminal()->First()->name.")"];
		}
		return response()->json($Response);
	}
	
	public function getCompanyRouteDetail($route_id)
	{
		$info_Route_Details = $this->routeRps->getCompanyRouteDetail($route_id)->Get();
		$Response = array();
		foreach($info_Route_Details as $info_Route_Detail)
		{
			$Response[] = ["route_id" => $info_Route_Detail->route_id, "terminal_id" => $info_Route_Detail->terminal_id, "terminal_name" => $info_Route_Detail->Terminal()->First()->name, "sequence" => $info_Route_Detail->sequence];
		}
		return response()->json($Response);
	}
}
