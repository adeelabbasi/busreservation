<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\AboutRepository;
use App\Http\Requests\AboutRequest;
use App\Repositories\GeneralRepository;
use App\Repositories\CompanyRepository;
use DataTables;
use Session;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Auth;

class AboutController extends Controller
{
	protected $companyRps;
	protected $aboutRps;
	protected $generalRps;
	public $guard = "admin";
	public $guard_url = "/admin/";
	protected $viewPermission = "View About";
	protected $addPermission = "Add About";
	protected $updatePermission = "Update About";
	protected $deletePermission = "Delete About";
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(GeneralRepository $generalRps, CompanyRepository $companyRps, AboutRepository $aboutRps)
    {
		if(str_replace("/","",request()->route()->getPrefix())=="admin")
		{
			$this->guard = "admin";
			$this->guard_url = "/admin/";
		}
		else
		{
			$this->guard = "web";
			$this->guard_url = "/company/";
		}
		$this->middleware('role_and_permission:Admin|'.$this->viewPermission.','.$this->guard);		$this->companyRps = $companyRps;
        $this->aboutRps = $aboutRps;
		$this->generalRps = $generalRps;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('cms.about.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		if (!Auth::guard($this->guard)->user()->can($this->addPermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->addPermission]);
		}
		if($this->guard == "admin")
		{
			$info_Companies = $this->companyRps->getCompany();
		}
		else
		{
			$info_Companies = [];
		}
		return view('cms.about.add', array('info_Companies' => $info_Companies));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AboutRequest $request)
    {
		if (!Auth::guard($this->guard)->user()->can($this->addPermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->addPermission]);
		}
		$this->generalRps->addLog(['action' => 'Add about']);
        $this->aboutRps->addAbout($request->all());
		Session::flash('flash_message', 'About successfully added!');
		return redirect($this->guard_url.'about');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\About  $foreman
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$info_About = $this->aboutRps->getAbout($id);
		return view('cms.about.show' ,array('info_About' => $info_About));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\About  $foreman
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->updatePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
        $info_About = $this->aboutRps->getAbout($id);
		if($this->guard == "admin")
		{
			$info_Companies = $this->companyRps->getCompany();
		}
		else
		{
			$info_Companies = [];
		}
		return view('cms.about.edit' ,array('info_About' => $info_About, 'info_Companies' => $info_Companies));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\About  $foreman
     * @return \Illuminate\Http\Response
     */
    public function update(AboutRequest $request, $id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->updatePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
		$this->generalRps->addLog(['action' => 'Update about']);
        $this->aboutRps->updateAbout($request->all() , $id);
		Session::flash('flash_message', 'About successfully updated!');
		return redirect($this->guard_url.'about');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\About  $foreman
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->deletePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
        $this->generalRps->addLog(['action' => 'Delete about']);
        $this->aboutRps->deleteAbout($id);
    }
	
	/**
     * Create datatable grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function grid()
    {
	   
	   	if($this->guard == "admin")
		{
			 $info_About = $this->aboutRps->getAbout();
		}
		else
		{
			 $info_About = $this->aboutRps->getCompanyAbout(\Auth::guard($this->guard)->User()->Company()->First()->id);
		}
	   	return Datatables::of($info_About)
		->editColumn('company_id', function ($info_About) {
			return $info_About->Company()->First()->name;
        })
		->editColumn('status', function ($info_About) {
			 if($info_About->status==1)
				return "Active";
			 else
				return "Inactive";
				 	
        })
		->addColumn('edit', function ($info_About) {
				 $updateButton = "";
				 $deleteButton = "";
				 if (Auth::guard($this->guard)->user()->can($this->updatePermission)) 
				 {
					$updateButton = '<a class="btn-floating waves-effect waves-light green" style="margin-right:2px;" href="'.url($this->guard_url.'about/'.$info_About->id.'/edit').'" title="Edit Data"><i class="large material-icons">mode_edit</i></a>';
				 }
				 if (Auth::guard($this->guard)->user()->can($this->deletePermission)) 
				 {
					$deleteButton = '<a class="btn-floating waves-effect waves-light red" href="javascript(0)" title="Delete Data" id="btnDelete" name="btnDelete" data-message="Are you sure to delete about?" data-remote="'.$this->guard_url.'about/' . $info_About->id . '"><i class="material-icons">clear</i></a>';
				 }
				 return '<div class="btn-group btn-group-action">
								<a class="btn-floating waves-effect waves-light blue" style="margin-right:2px;" href="'.url($this->guard_url.'about/'.$info_About->id).'" title="View Detail"><i class="large material-icons">pageview</i></a> 
								'.$updateButton.$deleteButton.'
								
						</div>';
        })
		->escapeColumns([])
		->make(true);
    }
	
	public function getCompanyAbout($company_id)
	{
		$info_About = $this->aboutRps->getCompanyAbout($company_id)->pluck("name","id");
		return response()->json($info_About);
	}
}
