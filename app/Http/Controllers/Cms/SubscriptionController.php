<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\SubscriptionRepository;
use App\Repositories\GeneralRepository;
use DataTables;
use Session;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Auth;

class SubscriptionController extends Controller
{
	protected $subscriptionRps;
	protected $generalRps;
	public $guard = "admin";
	public $guard_url = "/admin/";
	protected $viewPermission = "View Subscription";
	protected $addPermission = "Add Subscription";
	protected $updatePermission = "Update Subscription";
	protected $deletePermission = "Delete Subscription";
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(GeneralRepository $generalRps, SubscriptionRepository $subscriptionRps)
    { 
		if(str_replace("/","",request()->route()->getPrefix())=="admin")
		{
			$this->guard = "admin";
			$this->guard_url = "/admin/";
		}
		else
		{
			$this->guard = "web";
			$this->guard_url = "/company/";
		}
		$this->middleware('role_and_permission:Admin|'.$this->viewPermission.','.$this->guard);		
        $this->subscriptionRps = $subscriptionRps;
		$this->generalRps = $generalRps;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('cms.subscription.index');
    }

	/**
     * Create datatable grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function grid()
    {
	   	if($this->guard == "admin")
		{
			 $info_Subscription = $this->subscriptionRps->getSubscription();
		}
		else
		{
			 return $info_Subscription = $this->subscriptionRps->getCompanySubscription(\Auth::guard($this->guard)->User()->Company()->First()->Company_ussd()->First()->code);
		}
	   	return Datatables::of($info_Subscription)
		->escapeColumns([])
		->make(true);
    }

}
