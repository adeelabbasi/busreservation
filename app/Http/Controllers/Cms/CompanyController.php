<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\CompanyRepository;
use App\Repositories\CountryRepository;
use App\Http\Requests\CompanyRequest;
use Illuminate\Support\Facades\Validator;
use App\Repositories\GeneralRepository;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Auth;
use Session;
use DataTables;
use Hash;

class CompanyController extends Controller 
{
	
	protected $companyRps;
	protected $countryRps;
	protected $generalRps;
	public $guard = "admin";
	public $guard_url = "/admin/";
	protected $viewPermission = "View Company";
	protected $addPermission = "Add Company";
	protected $updatePermission = "Update Company";
	protected $deletePermission = "Delete Company";
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(GeneralRepository $generalRps, CompanyRepository $companyRps, CountryRepository $countryRps)
    {
		if(str_replace("/","",request()->route()->getPrefix())=="admin")
		{
			$this->guard = "admin";
			$this->guard_url = "/admin/";
		}
		else
		{
			$this->guard = "web";
			$this->guard_url = "/company/";
		}
		$this->middleware('role_and_permission:Admin|'.$this->viewPermission.','.$this->guard);  		$this->companyRps = $companyRps;
		$this->countryRps = $countryRps;
		$this->generalRps = $generalRps;
    }

    /**
     * Show the compnay index.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {	
		return view('cms.company.index');
    }
	
	/**
     * Create company
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {	
		
		if (!Auth::guard($this->guard)->user()->can($this->addPermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->addPermission]);
		}
		$info_Countries = $this->countryRps->getCountry();
		return view('cms.company.add', array('info_Countries' => $info_Countries));
    }
	
	/**
     * Show the compnay index.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyRequest $request)
    {	
		if (!Auth::guard($this->guard)->user()->can($this->addPermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->addPermission]);
		}
		//return $request->all();
		$this->generalRps->addLog(['action' => 'Add company']);
		$this->companyRps->addCompany($request->all());
		Session::flash('flash_message', 'Company successfully added!');
		return redirect($this->guard_url.'company');
    }

	/**
     * Display the specified resource.
     *
     * @param  \App\Foreman  $foreman
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$this->generalRps->addLog(['action' => 'Create company']);
        $info_Company = $this->companyRps->getCompany($id);
		return view('cms.company.show' ,array('info_Company' => $info_Company));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Foreman  $foreman
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->updatePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
        $info_Company = $this->companyRps->getCompany($id);
		$info_Countries = $this->countryRps->getCountry();
		return view('cms.company.edit' ,array('info_Countries' => $info_Countries, 'info_Company' => $info_Company));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Foreman  $foreman
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->updatePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
		//return $request->all();
		$this->generalRps->addLog(['action' => 'Update company']);
		$rules = [
			//'email' => 'unique:users,email,'.$request->user_id.'|required|email|max:255',
			'email' => 'unique:companies,email,'.$id.'|required|email|max:255',
			'name' => 'unique:companies,name,'.$id.'|required|string|max:200',
			'short_name' => 'required|string|max:10', 
			'type' => 'required|numeric',
			'coverage' => 'required|numeric',
			'country_id' => 'required|numeric',
			'description' => 'required|string|max:1000', 
			'address' => 'required|string', 
			'local_govt' => 'required|numeric', 
			'website' => 'required|string|max:100', 
			'twitter' => 'required|string|max:100', 
			'ig' => 'required|string|max:100', 
			'linkedin' => 'required|string|max:100', 
			'logo' => 'image|mimes:jpg,png,jpeg|min:1|max:2048',
			'rc_no' => 'required|string|max:100', 
			'reg_date' => 'required|date', 
			'contact_person' => 'required|string|max:100', 
			'contact_person_no' => 'required|string|max:100', 
			'rtss_reg_no' => 'required|string|max:100', 
			'rtss_cert_code' => 'required|string|max:100', 
			'rtss_certification' => 'required|string|max:100', 
			'whatsapp.*' => 'required|string|max:50',
			'phone_number.*' => 'required|string|max:50', 
			
        ];
		$validator = Validator::make($request->all(), $rules);
		if($validator->fails())
            return redirect()->back()->withErrors($validator)->withInput();
			
			
        $this->companyRps->updateCompany($request->all() , $id);
		Session::flash('flash_message', 'Company successfully updated!');
		return redirect($this->guard_url.'company');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Foreman  $foreman
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
		if(Hash::check($request->password,Auth::guard($this->guard)->user()->password))
		{
			if (!Auth::guard($this->guard)->user()->can($this->deletePermission)) 
			{
				throw UnauthorizedException::forRolesOrPermissions([$this->deletePermission]);
			}
			$this->generalRps->addLog(['action' => 'Delete company']);
			$this->companyRps->deleteCompany($id);
		}
		else
		{
			return 0;
		}
    }
	
	public function updateSequence(Request $request, $id)
	{
		$info_Company = $this->companyRps->updateSequence($id, $request->sequence);
		
		return [];
	}
	
	/**
     * Create datatable grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function grid()
    {
	   $info_Company = $this->companyRps->getAllCompanies();
	   return Datatables::of($info_Company)
		->editColumn('type', function ($info_Company) {
				 switch($info_Company->type)
				 {
					 case '1':
					 	return "Airline";
					 break;
					 case '2':
					 	return "Bus";
					 break;
					 case '3':
					 	return "Train";
					 break;
					 case '4':
					 	return "Hotel";
					 break;
				 }
        })
		->editColumn('coverage', function ($info_Company) {
				 switch($info_Company->coverage)
				 {
					 case '1':
					 	return "Domestic";
					 break;
					 case '2':
					 	return "International";
					 break;
				 }
        })
		->editColumn('sequence', function ($info_Company) {
				 return '<input class="sequence_id" data-company_id="'.$info_Company->id.'" value="'.$info_Company->sequence.'" />';
        })
		->editColumn('status', function ($info_Bus) {
			 if($info_Bus->status==1)
				return "Active";
			 else
				return "Inactive";
				 	
        })
		->editColumn('country_id', function ($info_Company) {
				 return $info_Company->Country()->First()->name;
        })
		->addColumn('phone_number', function ($info_Company) {
				 return $info_Company->Company_phone()->First()->number;
        })
		->addColumn('edit', function ($info_Company) {
				 $updateButton = "";
				 $deleteButton = "";
				 if (Auth::guard($this->guard)->user()->can($this->updatePermission)) 
				 {
					$updateButton = '<a class="btn-floating waves-effect waves-light green" style="margin-right:2px;" href="'.url($this->guard_url.'company/'.$info_Company->id.'/edit').'" title="Edit Data"><i class="large material-icons">mode_edit</i></a>';
				 }
				 if (Auth::guard($this->guard)->user()->can($this->deletePermission)) 
				 {$deleteButton = '<a class="btn-floating waves-effect waves-light red" href="javascript(0)" title="Delete Data" id="btnDeletePassword" name="btnDeletePassword" data-message="Are you sure to delete company, it will delete all data associated with company?<br />Please re-enter your password" data-name="'.$info_Company->name.'" data-remote="'.$this->guard_url.'company/' . $info_Company->id . '"><i class="material-icons">clear</i></a>';
				 }
				 return '<div class="btn-group btn-group-action">
								<a class="btn-floating waves-effect waves-light blue" style="margin-right:2px;" href="'.url($this->guard_url.'company/'.$info_Company->id).'" title="View Detail"><i class="large material-icons">pageview</i></a> 
								'.$updateButton.$deleteButton.'
								
						</div>';
        })
		->removeColumn('id')
		->escapeColumns([])
		->make(true);
    }
	
	public function getCompaniesList()
	{
		if($this->guard == "admin")
		{
			return $info_Company = $this->companyRps->getCompany()->Pluck('name','id');
		}
		else
		{
			$info_Company = $this->companyRps->getCompany(Auth::guard($this->guard)->user()->id);
			return [$info_Company->id => $info_Company->name];
		}
	}
}

