<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\LgaRequest;
use Auth;
use Session;
use DataTables;
use App\Repositories\CountryRepository;
use App\Repositories\CityRepository;
use App\Repositories\GeneralRepository;
use App\Repositories\LgaRepository;
use Spatie\Permission\Exceptions\UnauthorizedException;

class LgaController extends Controller
{
	protected $lgaRps;
	protected $countryRps;
	protected $cityRps;
	protected $generalRps;
	public $guard = "admin";
	public $guard_url = "/admin/";
	protected $viewPermission = "View LGA/Municipal";
	protected $addPermission = "Add LGA/Municipal";
	protected $updatePermission = "Update LGA/Municipal";
	protected $deletePermission = "Delete LGA/Municipal";
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(GeneralRepository $generalRps, CountryRepository $countryRps, CityRepository $cityRps, LgaRepository $lgaRps)
    {
		if(str_replace("/","",request()->route()->getPrefix())=="admin")
		{
			$this->guard = "admin";
			$this->guard_url = "/admin/";
		}
		else
		{
			$this->guard = "web";
			$this->guard_url = "/company/";
		}
		$this->middleware('role_and_permission:Admin|'.$this->viewPermission.','.$this->guard);
		$this->lgaRps = $lgaRps;
		$this->countryRps = $countryRps;
		$this->cityRps = $cityRps;
		$this->generalRps = $generalRps;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('cms.lga.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
		if (!Auth::guard($this->guard)->user()->can($this->addPermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->addPermission]);
		}
		$info_Countries = $this->countryRps->getCountry();
		$info_Citys = $this->cityRps->getCity();
        return view('cms.lga.add', array('info_Countries' =>  $info_Countries, 'info_Citys' => $info_Citys));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LgaRequest $request)
    {
		if (!Auth::guard($this->guard)->user()->can($this->addPermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->addPermission]);
		}
		$this->generalRps->addLog(['action' => 'Add lga']);
		//return $request->all();
        $this->lgaRps->addLga($request->all());
		Session::flash('flash_message', 'Lga successfully added!');
		return redirect($this->guard_url.'lga');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Lga  $foreman
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$info_Lga = $this->lgaRps->getLga($id);
		return view('cms.lga.show' ,array('info_Lga' => $info_Lga));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lga  $foreman
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->updatePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
        $info_Lga = $this->lgaRps->getLga($id);
		$info_Countries = $this->countryRps->getCountry();
		$info_Citys = $this->cityRps->getCity();
		return view('cms.lga.edit' ,array('info_Countries' =>  $info_Countries, 'info_Citys' => $info_Citys, 'info_Lga' => $info_Lga));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lga  $foreman
     * @return \Illuminate\Http\Response
     */
    public function update(LgaRequest $request, $id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->updatePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
		$this->generalRps->addLog(['action' => 'Update lga']);
        $this->lgaRps->updateLga($request->all() , $id);
		Session::flash('flash_message', 'Lga successfully updated!');
		return redirect($this->guard_url.'lga');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lga  $foreman
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->deletePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->deletePermission]);
		}
		$this->generalRps->addLog(['action' => 'Delete lga']);
        $this->lgaRps->deleteLga($id);
    }
	
	/**
     * Create datatable grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function grid()
    {
	   $info_Lga = $this->lgaRps->getLga();
	   return Datatables::of($info_Lga)
		->editColumn('status', function ($info_Lga) {
			 if($info_Lga->status==1)
				return "Active";
			 else
				return "Inactive";
				 	
        })
		->addColumn('country_id', function ($info_Lga) {
			$info_City = $info_Lga->City()->First();
			return $info_City->Country()->First()->name;
				 	
        })
		->editColumn('city_id', function ($info_Lga) {
			return $info_Lga->City()->First()->name;
				 	
        })
		->addColumn('edit', function ($info_Lga) {
				 $updateButton = "";
				 $deleteButton = "";
				 if (Auth::guard($this->guard)->user()->can($this->updatePermission)) 
				 {
					$updateButton = '<a class="btn-floating waves-effect waves-light green" style="margin-right:2px;" href="'.url($this->guard_url.'lga/'.$info_Lga->id.'/edit').'" title="Edit Data"><i class="large material-icons">mode_edit</i></a>';
				 }
				 if (Auth::guard($this->guard)->user()->can($this->deletePermission)) 
				 {
					$deleteButton = '<a class="btn-floating waves-effect waves-light red" href="javascript(0)" title="Delete Data" id="btnDelete" name="btnDelete" data-message="Are you sure to delete lga?" data-remote="'.$this->guard_url.'lga/' . $info_Lga->id . '"><i class="material-icons">clear</i></a>';
				 }
				 return '<div class="btn-group btn-group-action">
								<a class="btn-floating waves-effect waves-light blue" style="margin-right:2px;" href="'.url($this->guard_url.'lga/'.$info_Lga->id).'" title="View Detail"><i class="large material-icons">pageview</i></a> 
								'.$updateButton.$deleteButton.'
								
						</div>';
        })
		->escapeColumns([])
		->make(true);
    }
	
	public function cityLga($city_id)
	{
		$info_Lga = $this->lgaRps->getCityLga($city_id)->pluck("name","id");
		return response()->json($info_Lga);
	}
}

