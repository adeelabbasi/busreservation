<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Validator;
use Auth;

class ProfileController extends Controller
{
	protected $userRps;
	public $guard = "admin";
	public $guard_url = "/admin/";
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $userRps)
    {
  		if(str_replace("/","",request()->route()->getPrefix())=="admin")
		{
			$this->guard = "admin";
			$this->guard_url = "/admin/";
		}
		else
		{
			$this->guard = "web";
			$this->guard_url = "/company/";
		}
		$this->userRps = $userRps;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {	
		return view('cms.profile.edit');
    }
	
	public function update(Request $request, $id)
    {
		
		$rules = [
            'name' => 'required|string|max:255',
            'email' =>  'unique:users,email,'.$id.'|required|email',
            'username' =>  'unique:users,username,'.$id.'|required|alpha_dash',
			'avatar' => 'image|mimes:jpg,png,jpeg|min:1|max:2048',
			'password' => 'nullable|string|min:6|confirmed',
        ];
		
		$validator = Validator::make($request->all(), $rules);
		if($validator->fails())
            return redirect($this->guard_url.'/profile')->withErrors($validator)->withInput();
		
		$inputs = $request->all();
		$inputs["status"] = "on";
		if($this->guard=="admin")
		{
        	$db_user = $this->userRps->updateAdmin($inputs, $id);
		}
		else
		{
			$db_user = $this->userRps->updateUser($inputs, $id);
		}
		
		return redirect($this->guard_url.'profile');
    }
}
