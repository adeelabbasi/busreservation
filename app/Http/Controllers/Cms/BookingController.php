<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\BookingRepository;
use App\Repositories\GeneralRepository;
use DataTables;
use Session;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Auth;

class BookingController extends Controller
{
	protected $bookingRps;
	protected $generalRps;
	public $guard = "admin";
	public $guard_url = "/admin/";
	protected $viewPermission = "View Booking";
	protected $addPermission = "Add Booking";
	protected $updatePermission = "Update Booking";
	protected $deletePermission = "Delete Booking";
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(GeneralRepository $generalRps, BookingRepository $bookingRps)
    { 
		if(str_replace("/","",request()->route()->getPrefix())=="admin")
		{
			$this->guard = "admin";
			$this->guard_url = "/admin/";
		}
		else
		{
			$this->guard = "web";
			$this->guard_url = "/company/";
		}
		$this->middleware('role_and_permission:Admin|'.$this->viewPermission.','.$this->guard);		
        $this->bookingRps = $bookingRps;
		$this->generalRps = $generalRps;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('cms.booking.index');
    }

	/**
     * Create datatable grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function grid(Request $request)
    {
	   	if($this->guard == "admin")
		{
			 $info_Booking = $this->bookingRps->getBooking();
		}
		else
		{
			 $info_Booking = $this->bookingRps->getCompanyBooking(\Auth::guard($this->guard)->User()->Company()->First()->id);
		}
		if($request->Search)
		{
			$info_Booking = $info_Booking->Where('created_at', '>=' ,$request->from_date)->Where('created_at', '<=' ,$request->to_date);
		}
	   	return Datatables::of($info_Booking)
		->addColumn('company_id', function ($info_Booking) {
			if($info_Booking->Company()->First())
				return $info_Booking->Company()->First()->name;
			else
				return '';
				 	
        })
		->addColumn('from_city_name', function ($info_Booking) {
				return $info_Booking->from_city_name. '('.$info_Booking->from_terminal_name.')';
				 	
        })
		->editColumn('msisdn', function ($info_Booking) {
				return "0".substr($info_Booking->msisdn,-10);
				 	
        })
		->addColumn('to_city_name', function ($info_Booking) {
				return $info_Booking->tocity_name. '('.$info_Booking->to_terminal_name.')';
				 	
        })
		/*->addColumn('discount', function ($info_Booking) {
			if($info_Booking->charge_type == "1")
				$discount = $info_Booking->total-$info_Booking->sub_total;
			else
				$discount = $info_Booking->total-$info_Booking->sub_total-$info_Booking->commission;
			return $discount < 0 ? 0 : $discount ;
				 	
        })*/
		->addColumn('amount', function ($info_Booking) {
			if($info_Booking->charge_type == "1")
				return $info_Booking->sub_total;
			else
				return $info_Booking->total;
				 	
        })
		->editColumn('status', function ($info_Booking) {
			if($info_Booking->status == '0')
				return 'Payment Pending';
			if($info_Booking->status == '1')
				return 'Paid';
			
				 	
        })
		->editColumn('new_customer', function ($info_Booking) {
			return $info_Booking->new_customer;
			if($info_Booking->new_customer == '0')
				return 'No';
			if($info_Booking->new_customer == '1')
				return 'Yes';
			
				 	
        })
		->escapeColumns([])
		->make(true);
    }

}
