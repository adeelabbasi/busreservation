<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ScheduleRepository;
use App\Http\Requests\ScheduleRequest;
use App\Repositories\GeneralRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\RouteRepository;
use App\Repositories\BusRepository;
use App\Repositories\FareRepository;
use App\Repositories\TerminalRepository;
use App\Repositories\CityRepository;
use DataTables;
use Session;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Auth;

class ScheduleController extends Controller
{
	protected $companyRps;
	protected $scheduleRps;
	protected $generalRps;
	protected $routeRps;
	protected $busRps;
	protected $fareRps;
	protected $terminalRps;
	protected $cityRps;
	public $guard = "admin";
	public $guard_url = "/admin/";
	protected $viewPermission = "View Schedule";
	protected $addPermission = "Add Schedule";
	protected $updatePermission = "Update Schedule";
	protected $deletePermission = "Delete Schedule";
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(GeneralRepository $generalRps, CompanyRepository $companyRps, ScheduleRepository $scheduleRps, RouteRepository $routeRps, BusRepository $busRps, FareRepository $fareRps, TerminalRepository $terminalRps, CityRepository $cityRps)
    {
		if(str_replace("/","",request()->route()->getPrefix())=="admin")
		{
			$this->guard = "admin";
			$this->guard_url = "/admin/";
		}
		else
		{
			$this->guard = "web";
			$this->guard_url = "/company/";
		}
		$this->middleware('role_and_permission:Admin|'.$this->viewPermission.','.$this->guard);		
		$this->companyRps = $companyRps;
        $this->scheduleRps = $scheduleRps;
		$this->routeRps = $routeRps;
		$this->busRps = $busRps;
		$this->fareRps = $fareRps;
		$this->terminalRps = $terminalRps;
		$this->generalRps = $generalRps;
		$this->cityRps = $cityRps;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		if($this->guard == "admin")
		{
			$info_Terminals = $this->terminalRps->getTerminal();
		}
		else
		{
			$info_Terminals = $this->terminalRps->getCompanyTerminal(\Auth::guard($this->guard)->User()->Company()->First()->id);
		}
		
		$info_Cities = $this->cityRps->getCity();
		
		return view('cms.schedule.index', array('info_Cities' => $info_Cities, 'info_Terminals' => $info_Terminals));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		if (!Auth::guard($this->guard)->user()->can($this->addPermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->addPermission]);
		}
		if($this->guard == "admin")
		{
			$info_Companies = $this->companyRps->getCompany();
		}
		else
		{
			$info_Companies = [];
		}
		return view('cms.schedule.add', array('info_Companies' => $info_Companies));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ScheduleRequest $request)
    {
		//return $request->all();
		if (!Auth::guard($this->guard)->user()->can($this->addPermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->addPermission]);
		}
		$this->generalRps->addLog(['action' => 'Add schedule']);
        $info_Schedule = $this->scheduleRps->addSchedule($request->all());
		Session::flash('flash_message', 'Schedule successfully added!');
		return redirect($this->guard_url.'fare_type?ScheduleID='.$info_Schedule->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Schedule  $foreman
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$info_Schedule = $this->scheduleRps->getSchedule($id);
		return view('cms.schedule.show' ,array('info_Schedule' => $info_Schedule));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Schedule  $foreman
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->updatePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
        $info_Schedule = $this->scheduleRps->getSchedule($id);
		
		if($this->guard == "admin")
		{
			$info_Companies = $this->companyRps->getCompany();
		}
		else
		{
			$info_Companies = [];
		}
		$info_Routes = $this->routeRps->getCompanyRoute($info_Schedule->company_id)->OrderBy('name', 'ASC')->Get();
		$Response = array();
		foreach($info_Routes as $info_Route)
		{
			$Response[] = ["id" => $info_Route->id, "name" => $info_Route->name." -> ".$info_Route->Route_detail()->OrderBy('sequence')->First()->Terminal()->First()->Lga()->First()->City()->First()->name."(".$info_Route->Route_detail()->OrderBy('sequence')->First()->Terminal()->First()->name.") to ".$info_Route->Route_detail()->OrderBy('sequence','desc')->First()->Terminal()->First()->Lga()->First()->City()->First()->name."(".$info_Route->Route_detail()->OrderBy('sequence','desc')->First()->Terminal()->First()->name.")"];
		}
		
		$info_Buses = $this->busRps->getCompanyBus($info_Schedule->company_id)->Get();

		return view('cms.schedule.edit' ,array('info_Schedule' => $info_Schedule, 'info_Buses' => $info_Buses, 'info_Companies' => $info_Companies, 'info_Routes' => $Response));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Schedule  $foreman
     * @return \Illuminate\Http\Response
     */
    public function update(ScheduleRequest $request, $id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->updatePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
		$this->generalRps->addLog(['action' => 'Update schedule']);
        $this->scheduleRps->updateSchedule($request->all() , $id);
		Session::flash('flash_message', 'Schedule successfully updated!');
		return redirect($this->guard_url.'fare_type?ScheduleID='.$id);
		//return redirect($this->guard_url.'schedule/'.$id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Schedule  $foreman
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->deletePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
        $this->generalRps->addLog(['action' => 'Delete schedule']);
        $this->scheduleRps->deleteSchedule($id);
    }
	
	public function copySchedule($id)
    {
		$db_schedule = $this->scheduleRps->copySchedule($id);
		return redirect($this->guard_url.'schedule/'.$db_schedule->id.'/edit');
    }
	
	/**
     * Create datatable grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function grid(Request $request)
    {
	   	$FromCity = $request->FromCity;
		$ToCity = $request->ToCity;
		$FromTerminal = $request->FromTerminal;
		$ToTerminal = $request->ToTerminal;
		$effectiveDate = $request->effectiveDate;
		
		if($request->Search)
		{
			$Ids = array();
			if($this->guard == "admin")
			{
				 $info_ScheduleIDs = $this->scheduleRps->getSearchSchedule(null, $FromCity, $ToCity, $FromTerminal, $ToTerminal, $effectiveDate);
			}
			else
			{
				 $info_ScheduleIDs = $this->scheduleRps->getSearchSchedule(\Auth::guard($this->guard)->User()->Company()->First()->id, $FromCity, $ToCity, $FromTerminal, $ToTerminal, $effectiveDate);
			}
			$Ids = array_column($info_ScheduleIDs, 'id');
			$info_Schedule = $this->scheduleRps->getScheduleByIds($Ids);
		}
		else
		{
			if($this->guard == "admin")
			{
				 $info_Schedule = $this->scheduleRps->getSchedule();
			}
			else
			{
				 $info_Schedule = $this->scheduleRps->getCompanySchedule(\Auth::guard($this->guard)->User()->Company()->First()->id);
			}
		}
		
	   	return Datatables::of($info_Schedule)
		->editColumn('company_id', function ($info_Schedule) {
			return $info_Schedule->Company()->First()->name;
        })
		->addColumn('depart_city', function ($info_Schedule) {
			$departure_time="";
			if($info_Schedule->Schedule_time()->Get()->Count()>0)
			{
				$departure_time = $info_Schedule->Schedule_time()->Orderby('sequence')->First()->departure_time;
				$departure_time = date('h:iA', strtotime($departure_time));
			}
				
			return $info_Schedule->Route()->First()->Route_detail()->OrderBy('sequence')->First()->Terminal()->First()->Lga()->First()->City()->First()->name."(".$info_Schedule->Route()->First()->Route_detail()->OrderBy('sequence')->First()->Terminal()->First()->name.")<br />".$departure_time;

        })
		->addColumn('arrive_city', function ($info_Schedule) {
			$arrival_time="";
			if($info_Schedule->Schedule_time()->Orderby('sequence', 'desc')->Get()->Count()>0)
			{
				$arrival_time = $info_Schedule->Schedule_time()->Orderby('sequence', 'desc')->First()->arrival_time;
				$arrival_time = date('h:iA', strtotime($arrival_time));
			}
			
			return $info_Schedule->Route()->First()->Route_detail()->OrderBy('sequence','desc')->First()->Terminal()->First()->Lga()->First()->City()->First()->name."(".$info_Schedule->Route()->First()->Route_detail()->OrderBy('sequence','desc')->First()->Terminal()->First()->name.")<br />".$arrival_time;
			
		})
		->editColumn('departure_time', function ($info_Schedule) {
			return date("h:iA", strtotime($info_Schedule->departure_time))."<br />(".$info_Schedule->duration." Hrs)";
        })
		->editColumn('effective_Date', function ($info_Schedule) {
			return $info_Schedule->effective_Date."<br /><b>Up to</b><br />".$info_Schedule->operatingend_date;
        })
		->editColumn('duration', function ($info_Schedule) {
			return $info_Schedule->duration." Hrs";
        })
		->addColumn('bus_id', function ($info_Schedule) {
			$BusesName = "";
			foreach($info_Schedule->Bus()->Get() as $Bus)
			{
				$BusesName .= $Bus->name.", ";
			}
			$BusesName = substr($BusesName,0,-2);
			return $BusesName;
        })
		->editColumn('monday', function ($info_Schedule) {
			 $DayData = "";
			 if($info_Schedule->monday==1)
				$DayData .= "Monday:<i class='material-icons' style='color:#0F0'>done</i><br />";
			 else
				$DayData .= "Monday:<i class='material-icons' style='color:#F00'>clear</i><br />";
				 	
        
			 if($info_Schedule->tuesday==1)
				$DayData .= "Tuesday:<i class='material-icons' style='color:#0F0'>done</i><br />";
			 else
				$DayData .= "Tuesday:<i class='material-icons' style='color:#F00'>clear</i><br />";

			 if($info_Schedule->wednesday==1)
				$DayData .= "Wednesday:<i class='material-icons' style='color:#0F0'>done</i><br />";
			 else
				$DayData .= "Wednesday:<i class='material-icons' style='color:#F00'>clear</i><br />";

			 if($info_Schedule->thursday==1)
				$DayData .= "Thursday:<i class='material-icons' style='color:#0F0'>done</i><br />";
			 else
				$DayData .= "Thursday:<i class='material-icons' style='color:#F00'>clear</i><br />";

			 if($info_Schedule->friday==1)
				$DayData .= "Friday:<i class='material-icons' style='color:#0F0'>done</i><br />";
			 else
				$DayData .= "Friday:<i class='material-icons' style='color:#F00'>clear</i><br />";

			 if($info_Schedule->saturday==1)
				$DayData .= "Saturday:<i class='material-icons' style='color:#0F0'>done</i><br />";
			 else
				$DayData .= "Saturday:<i class='material-icons' style='color:#F00'>clear</i><br />";

			 if($info_Schedule->sunday==1)
				$DayData .= "Sunday:<i class='material-icons' style='color:#0F0'>done</i><br />";
			 else
				$DayData .= "Sunday:<i class='material-icons' style='color:#F00'>clear</i><br />";
			
			return $DayData;
				 	
        })
		->editColumn('status', function ($info_Schedule) {
			 if($info_Schedule->status==1)
				return "Active";
			 else
				return "Inactive";
				 	
        })
		->addColumn('edit', function ($info_Schedule) {
				 $updateButton = "";
				 $deleteButton = "";
				 if (Auth::guard($this->guard)->user()->can($this->updatePermission)) 
				 {
					$updateButton = '<a class="btn-floating waves-effect waves-light green" style="margin-right:2px;" href="'.url($this->guard_url.'schedule/'.$info_Schedule->id.'/edit').'" title="Edit Data"><i class="large material-icons">mode_edit</i></a>
									<a class="btn-floating waves-effect waves-light gray" style="margin-right:2px;" href="'.url($this->guard_url.'schedule/'.$info_Schedule->id.'/copy').'" title="Copy Data"><i class="material-icons">content_copy</i>Copy</a>';
				 }
				 if (Auth::guard($this->guard)->user()->can($this->deletePermission)) 
				 {
					$deleteButton = '<a class="btn-floating waves-effect waves-light red" href="javascript(0)" title="Delete Data" id="btnDelete" name="btnDelete" data-message="Are you sure to delete schedule?" data-remote="'.$this->guard_url.'schedule/' . $info_Schedule->id . '"><i class="material-icons">clear</i></a>';
				 }
				 return '<div class="btn-group btn-group-action">
								<a class="btn-floating waves-effect waves-light blue" style="margin-right:2px;" href="'.url($this->guard_url.'schedule/'.$info_Schedule->id).'" title="View Detail" target="_blank"><i class="large material-icons">pageview</i></a> 
								'.$updateButton.$deleteButton.'
								
						</div>';
        })
		->escapeColumns([])
		->make(true);
    }
	
	public function getCompanySchedule($company_id)
	{
		$info_Schedule = $this->scheduleRps->getCompanySchedule($company_id)->pluck("short_name","id");
		return response()->json($info_Schedule);
	}
	
	public function getCompanyScheduleTime($company_id)
	{
		$info_Schedule = $this->scheduleRps->getCompanyScheduleTime($company_id);
		return response()->json($info_Schedule);
	}
}
