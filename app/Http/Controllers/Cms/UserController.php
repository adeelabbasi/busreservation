<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Http\Requests\AdminUserRequest;
use App\Repositories\GeneralRepository;
use App\Repositories\CompanyRepository;
use Illuminate\Support\Facades\Validator;
use DataTables;
use Session;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Auth;

class UserController extends Controller
{
	protected $companyRps;
	protected $userRps;
	protected $generalRps;
	public $guard = "admin";
	public $guard_url = "/admin/";
	protected $viewPermission = "View User";
	protected $addPermission = "Add User";
	protected $updatePermission = "Update User";
	protected $deletePermission = "Delete User";
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(GeneralRepository $generalRps, CompanyRepository $companyRps, UserRepository $userRps)
    { 
		if(str_replace("/","",request()->route()->getPrefix())=="admin")
		{
			$this->guard = "admin";
			$this->guard_url = "/admin/";
		}
		else
		{
			$this->guard = "web";
			$this->guard_url = "/company/";
		}
		$this->middleware('role_and_permission:Admin|'.$this->viewPermission.','.$this->guard);		
		$this->companyRps = $companyRps;
        $this->userRps = $userRps;
		$this->generalRps = $generalRps;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('cms.user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		if (!Auth::guard($this->guard)->user()->can($this->addPermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->addPermission]);
		}
		if($this->guard == "admin")
		{
			$info_Companies = $this->companyRps->getCompany();
		}
		else
		{
			$info_Companies = [];
		}
		return view('cms.user.add', array('info_Companies' => $info_Companies));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminUserRequest $request)
    {
		if (!Auth::guard($this->guard)->user()->can($this->addPermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->addPermission]);
		}
		$this->generalRps->addLog(['action' => 'Add user']);
        //return $request->all();
		if($request->company_id=="-1")
			$this->userRps->addAdmin($request->all());
		else
			$this->userRps->addUser($request->all());
			
		Session::flash('flash_message', 'User successfully added!');
		return redirect($this->guard_url.'user');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $foreman
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
		if($request->type=="1")
			$info_User = $this->userRps->getAdmin($id);
		else
			$info_User = $this->userRps->getUser($id)->First();
			
		return view('cms.user.show' ,array('info_User' => $info_User));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $foreman
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->updatePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
		if($request->type=="1")
		{
			$info_User = $this->userRps->getAdmin($id);
			$info_User->company_id="-1";
			$info_User->role_id=$info_User->roles->First()->id;
			$info_Roles = $this->userRps->getAdminRole()->get();
		}
		else
		{
			$info_User = $this->userRps->getUser($id);
			$info_User->role_id=$info_User->roles->First()->id;
			$info_Roles = $this->userRps->getUserRole($info_User->company_id)->get();
		}
			
		$info_Companies = $this->companyRps->getCompany();
		return view('cms.user.edit' ,array('info_User' => $info_User, 'info_Roles' => $info_Roles, 'info_Companies' => $info_Companies));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $foreman
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->updatePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
		$this->generalRps->addLog(['action' => 'Update user']);
		$rules = [
            'name' => 'required|string|max:255',
            'email' =>  'unique:users,email,'.$id.'|required|email',
            'username' =>  'unique:users,username,'.$id.'|required|alpha_dash',
			'avatar' => 'image|mimes:jpg,png,jpeg|min:1|max:2048',
			'password' => 'nullable|string|min:6',
        ];
		
		$validator = Validator::make($request->all(), $rules);
		if($validator->fails())
            return redirect()->back()->withErrors($validator)->withInput();
        
		if($request->company_id=="-1")
			$this->userRps->updateAdmin($request->all() , $id);
		else
			$this->userRps->updateUser($request->all() , $id);
			
		Session::flash('flash_message', 'User successfully updated!');
		return redirect($this->guard_url.'user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $foreman
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->deletePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
		if($request->type=="1")
		{
			if(Auth::guard($this->guard)->user()->id!=$id)
				$this->userRps->deleteAdmin($id);
		}
		else
		{
			//$info_User = $this->userRps->getUser($id)->First();
			//if($info_User->type==2)
				$this->userRps->deleteUser($id);
		}
			
        $this->generalRps->addLog(['action' => 'Delete user']);
        
    }
	
	/**
     * Create datatable grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function grid()
    {
	   	if($this->guard == "admin")
		{
			$info_Admin = $this->userRps->getAdmin();
	   		$info_User = $this->userRps->getUser()->union($info_Admin)->orderBy('created_at', 'DESC')->Get();
		}
		else
		{
			 $info_User = $this->userRps->getCompanyUser(\Auth::guard($this->guard)->User()->Company()->First()->id);
		}
		
	   	return Datatables::of($info_User)
		->addColumn('role_id', function ($info_User) {
			if($info_User->company_id=="")
			{
				$user = $this->userRps->getAdmin($info_User->id);
	   			return $user->roles->First()->name;
			}
			else
			{
				$user = $this->userRps->getUser($info_User->id);
	   			return $user->roles->First()->name;
			}
        })
		->editColumn('company_id', function ($info_User) {
			if($info_User->company_id=="")
			{
				return "Super Admin";
			}
			else
			{
				$user = $this->userRps->getUser($info_User->id);
				return $user->Company()->First()->name;
			}
        })
		->editColumn('status', function ($info_User) {
			 if($info_User->status==1)
				return "Active";
			 else
				return "Inactive";
				 	
        })
		->addColumn('edit', function ($info_User) {
			if($info_User->company_id=="")
			{
				$updateButton = "";
				$deleteButton = "";
				if (Auth::guard($this->guard)->user()->can($this->updatePermission)) 
				{
				$updateButton = '<a class="btn-floating waves-effect waves-light green" style="margin-right:2px;" href="'.url($this->guard_url.'user/'.$info_User->id.'/edit?type=1').'" title="Edit Data"><i class="large material-icons">mode_edit</i></a>';
				}
				if (Auth::guard($this->guard)->user()->can($this->deletePermission)) 
				{
				$deleteButton = '<a class="btn-floating waves-effect waves-light red" href="javascript(0)" title="Delete Data" id="btnDelete" name="btnDelete" data-message="Are you sure to delete user?" data-remote="'.$this->guard_url.'user/' . $info_User->id . '?type=1"><i class="material-icons">clear</i></a>';
				}
				return '<div class="btn-group btn-group-action">
							<a class="btn-floating waves-effect waves-light blue" style="margin-right:2px;" href="'.url($this->guard_url.'user/'.$info_User->id).'?type=1" title="View Detail"><i class="large material-icons">pageview</i></a> 
							'.$updateButton.$deleteButton.'
							
					</div>';
			}
			else
			{
				$updateButton = "";
				$deleteButton = "";
				if (Auth::guard($this->guard)->user()->can($this->updatePermission)) 
				{
				$updateButton = '<a class="btn-floating waves-effect waves-light green" style="margin-right:2px;" href="'.url($this->guard_url.'user/'.$info_User->id.'/edit?type=2').'" title="Edit Data"><i class="large material-icons">mode_edit</i></a>';
				}
				if (Auth::guard($this->guard)->user()->can($this->deletePermission)) 
				{
				$deleteButton = '<a class="btn-floating waves-effect waves-light red" href="javascript(0)" title="Delete Data" id="btnDelete" name="btnDelete" data-message="Are you sure to delete user?" data-remote="'.$this->guard_url.'user/' . $info_User->id . '?type=2"><i class="material-icons">clear</i></a>';
				}
				return '<div class="btn-group btn-group-action">
							<a class="btn-floating waves-effect waves-light blue" style="margin-right:2px;" href="'.url($this->guard_url.'user/'.$info_User->id).'?type=2" title="View Detail"><i class="large material-icons">pageview</i></a> 
							'.$updateButton.$deleteButton.'
							
					</div>';
			}
        })
		->escapeColumns([])
		->make(true);
    }
	
	public function showUserLogs()
    {
		if (!Auth::guard($this->guard)->user()->can('View User Activity')) 
		{
			throw UnauthorizedException::forRolesOrPermissions(['View User Activity']);
		}
		return view('cms.user.userlogs');
    }
	/**
     * Create datatable user log grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function gridLog()
    {
		if (!Auth::guard($this->guard)->user()->can('View User Activity')) 
		{
			throw UnauthorizedException::forRolesOrPermissions(['View User Activity']);
		}
		if($this->guard == "admin")
		{
			$info_Userlogs = $this->userRps->getUserLog(1);
		}
		else
		{
			$info_Userlogs = $this->userRps->getCompanyUserLog(\Auth::guard($this->guard)->User()->Company()->First()->User()->Get()->Pluck('id'));
		}
		
		return Datatables::of($info_Userlogs)
		->editColumn('parent', function ($info_Userlogs) {
			if($info_Userlogs->user_id=="")
			{
				return "Super Admin";
			}
			else
			{
				$user = $this->userRps->getUser($info_Userlogs->user_id);
				return $user->Company()->First()->name;
			}
		})
		->editColumn('user_id', function ($info_Userlogs) {
			if($info_Userlogs->user_id!=NULL)
				return $info_Userlogs->User->First()->name;
		})
		->editColumn('admin_id', function ($info_Userlogs) {
			 if($info_Userlogs->admin_id!=NULL)
				return $info_Userlogs->Admin->First()->name;				 	
		})
		->escapeColumns([])
		->make(true);
    }
}
