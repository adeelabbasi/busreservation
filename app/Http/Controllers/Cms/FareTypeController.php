<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\FareTypeRepository;
use App\Repositories\ScheduleRepository;
use App\Http\Requests\FareTypeRequest;
use App\Repositories\GeneralRepository;
use App\Repositories\ServiceRepository;
use DataTables;
use Session;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Auth;

class FareTypeController extends Controller
{
	protected $fareTypeRps;
	protected $generalRps;
	protected $serviceRps;
	public $guard = "admin";
	public $guard_url = "/admin/";
	protected $viewPermission = "View Schedule";
	protected $addPermission = "Add Schedule";
	protected $updatePermission = "Update Schedule";
	protected $deletePermission = "Delete Schedule";
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(GeneralRepository $generalRps, FareTypeRepository $fareTypeRps, ServiceRepository $serviceRps, ScheduleRepository $scheduleRps)
    {
		if(str_replace("/","",request()->route()->getPrefix())=="admin")
		{
			$this->guard = "admin";
			$this->guard_url = "/admin/";
		}
		else
		{
			$this->guard = "web";
			$this->guard_url = "/company/";
		}
		$this->middleware('role_and_permission:Admin|'.$this->viewPermission.','.$this->guard);
        $this->fareTypeRps = $fareTypeRps;
		$this->serviceRps = $serviceRps;
		$this->scheduleRps = $scheduleRps;
		$this->generalRps = $generalRps;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$info_Schedule = $this->scheduleRps->getSchedule($request->ScheduleID);
		if($info_Schedule)
		{
			$info_FareTypes = $info_Schedule->Fare_type()->OrderBy('name')->Get();
			$info_Services = $this->serviceRps->GetCompanyService($info_Schedule->company_id)->Get();
			return view('cms.faretype.index', array('info_Schedule' => $info_Schedule, 'info_FareTypes' => $info_FareTypes,'info_Services' => $info_Services));
		}
		else
			return 404;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FareTypeRequest $request)
    {
		if (!Auth::guard($this->guard)->user()->can($this->addPermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->addPermission]);
		}
		$this->generalRps->addLog(['action' => 'Add Fare Type']);
        $this->fareTypeRps->addFareType($request->all());
		Session::flash('flash_message', 'Fare Type successfully added!');
		return redirect($this->guard_url.'fare_type?ScheduleID='.$request->schedule_id);
    }

   

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FareType  $foreman
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->deletePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
        $this->generalRps->addLog(['action' => 'Delete Fare Type']);
        $this->fareTypeRps->deleteFareType($id);
    }
	
	public function getCompanyFareType($company_id)
	{
		$info_FareType = $this->fareTypeRps->getCompanyFareType($company_id)->pluck("fareType","id");
		return response()->json($info_FareType);
	}
}
