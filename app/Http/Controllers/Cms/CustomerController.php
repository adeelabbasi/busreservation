<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\CustomerRepository;
use App\Http\Requests\CustomerRequest;
use App\Repositories\GeneralRepository;
use DataTables;
use Session;
use Auth;

class CustomerController extends Controller
{
	protected $customerRps;
	protected $generalRps;
	public $guard = "admin";
	public $guard_url = "/admin/";
	protected $viewPermission = "View Customer";
	protected $addPermission = "Add Customer";
	protected $updatePermission = "Update Customer";
	protected $deletePermission = "Delete Customer";
    /** 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(GeneralRepository $generalRps, CustomerRepository $customerRps)
    { 
		if(str_replace("/","",request()->route()->getPrefix())=="admin")
		{
			$this->guard = "admin";
			$this->guard_url = "/admin/";
		}
		else
		{
			$this->guard = "web";
			$this->guard_url = "/company/";
		}
		$this->middleware('role_and_permission:Admin|'.$this->viewPermission.','.$this->guard);        $this->customerRps = $customerRps;
		$this->generalRps = $generalRps;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('cms.customer.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		if (!Auth::guard($this->guard)->user()->can($this->addPermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->addPermission]);
		}
        return view('cms.customer.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerRequest $request)
    {
		if (!Auth::guard($this->guard)->user()->can($this->addPermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->addPermission]);
		}
		$this->generalRps->addLog(['action' => 'Add customer']);
        $this->customerRps->addCustomer($request->all());
		Session::flash('flash_message', 'Customer successfully added!');
		return redirect($this->guard_url.'customer');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $foreman
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		
		$info_Customer = $this->customerRps->getCustomer($id);
		return view('cms.customer.show' ,array('info_Customer' => $info_Customer));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $foreman
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->updatePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
        $info_Customer = $this->customerRps->getCustomer($id);
		return view('cms.customer.edit' ,array('info_Customer' => $info_Customer));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $foreman
     * @return \Illuminate\Http\Response
     */
    public function update(CustomerRequest $request, $id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->updatePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->updatePermission]);
		}
		$this->generalRps->addLog(['action' => 'Update country']);
        $this->customerRps->updateCustomer($request->all() , $id);
		Session::flash('flash_message', 'Customer successfully updated!');
		return redirect($this->guard_url.'customer');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $foreman
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		if (!Auth::guard($this->guard)->user()->can($this->deletePermission)) 
		{
			throw UnauthorizedException::forRolesOrPermissions([$this->deletePermission]);
		}
		$this->generalRps->addLog(['action' => 'Delete country']);
        $this->customerRps->deleteCustomer($id);
    }
	
	/**
     * Create datatable grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function grid()
    {
	   $info_Customer = $this->customerRps->getCustomer();
	   return Datatables::of($info_Customer)
		->editColumn('status', function ($info_Customer) {
			 if($info_Customer->status==1)
				return "Active";
			 else
				return "Inactive";
				 	
        })
		->addColumn('edit', function ($info_Customer) {
				 $updateButton = "";
				 $deleteButton = "";
				 if (Auth::guard($this->guard)->user()->can($this->updatePermission)) 
				 {
					$updateButton = '<a class="btn-floating waves-effect waves-light green" style="margin-right:2px;" href="'.url($this->guard_url.'customer/'.$info_Customer->id.'/edit').'" title="Edit Data"><i class="large material-icons">mode_edit</i></a>';
				 }
				 if (Auth::guard($this->guard)->user()->can($this->deletePermission)) 
				 {
					$deleteButton = '<a class="btn-floating waves-effect waves-light red" href="javascript(0)" title="Delete Data" id="btnDelete" name="btnDelete" data-message="Are you sure to delete customer?" data-remote="'.$this->guard_url.'customer/' . $info_Customer->id . '"><i class="material-icons">clear</i></a>';
				 }
				 return '<div class="btn-group btn-group-action">
								<a class="btn-floating waves-effect waves-light blue" style="margin-right:2px;" href="'.url($this->guard_url.'customer/'.$info_Customer->id).'" title="View Detail"><i class="large material-icons">pageview</i></a> 
								'.$updateButton.$deleteButton.'
								
						</div>';
        })
		->escapeColumns([])
		->make(true);
    }
	public function showCustomerLogs()
    {
		if (!Auth::guard($this->guard)->user()->can('View Customer Activity')) 
		{
			throw UnauthorizedException::forRolesOrPermissions(['View Customer Activity']);
		}
		return view('cms.customer.customerlogs');
    }
	/**
     * Create datatable user log grid
     *
     * 
     * @return \Illuminate\Http\Datatable
     */
	public function gridLog(Request $request)
    {
		$draw = $request->get('draw');
		$start = $request->get("start");
		$rowperpage = $request->get("length"); // Rows display per page

		$columnIndex_arr = $request->get('order');
		$columnName_arr = $request->get('columns');
		$order_arr = $request->get('order');
		$search_arr = $request->get('search');

		$columnIndex = $columnIndex_arr[0]['column']; // Column index
		$columnName = $columnName_arr[$columnIndex]['data']; // Column name
		$columnSortOrder = $order_arr[0]['dir']; // asc or desc
		$searchValue = $search_arr['value']; // Search value
		
		$from = $request->from;
		$to = $request->to;
		if (!Auth::guard($this->guard)->user()->can('View Customer Activity')) 
		{
			throw UnauthorizedException::forRolesOrPermissions(['View Customer Activity']);
		}
		
		if($request->Search)
		{
			if($this->guard == "admin")
			{
				$info_Customerlogs = $this->customerRps->getSearchCustomerLog(1,$from,$to);
			}
			else
			{
				$info_Customerlogs = $this->customerRps->getSearchCompanyCustomerLog(\Auth::guard($this->guard)->User()->Company()->First()->Company_ussd()->First()->code,$from,$to);
			}
		}
		else
		{
			if($this->guard == "admin")
			{
				$info_Customerlogs = $this->customerRps->getCustomerLog(1);
			}
			else
			{
				$info_Customerlogs = $this->customerRps->getCompanyCustomerLog(\Auth::guard($this->guard)->User()->Company()->First()->Company_ussd()->First()->code);
			}
		}
		//return $info_Customerlogs;
		$totalRecords = $totalRecordswithFilter = $info_Customerlogs->Get()->count();
		
		if($rowperpage == "-1")
		{
			$info_Customerlogs = $info_Customerlogs
			->orderby($columnName, $columnSortOrder)
			->get();
		}
		else
		{
			$info_Customerlogs = $info_Customerlogs
			->orderby($columnName, $columnSortOrder)
			->skip($start)
			->take($rowperpage)
			->get();

		}
		foreach($info_Customerlogs as $info_Customerlog)
		{
			$timeFirst  = strtotime($info_Customerlog->start_time);
			$timeSecond = strtotime($info_Customerlog->end_time);
			$differenceInSeconds = $timeSecond - $timeFirst." Sec";
			
			$info_Customerlog->session_time = $differenceInSeconds;
			$info_Customerlog->session_cost = 'N10';			
		}
		
		return $response = array(
			"draw" => intval($draw),
			"iTotalRecords" => $totalRecords,
			"iTotalDisplayRecords" => $totalRecordswithFilter,
			"aaData" => $info_Customerlogs
		);
    }
}
