<?php
namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Str;
class GUOMiddleware
{
    public function handle($request, Closure $next, $guard = null)
    {
		$AUTH_USER = 'GUO';
        $AUTH_PASS = 'Gu0-4a318102#!';
        header('Cache-Control: no-cache, must-revalidate, max-age=0');
        $has_supplied_credentials = !(empty(\Request::getUser()) && empty(\Request::getPassword()));
        $is_not_authenticated = (
            !$has_supplied_credentials ||
            \Request::getUser() != $AUTH_USER ||
            \Request::getPassword()   != $AUTH_PASS
        );
        if ($is_not_authenticated) {
            header('HTTP/1.1 401 Authorization Required');
            header('WWW-Authenticate: Basic realm="Access denied"');
            exit;
        }
        return $next($request);
    }
}