<?php
namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Str;
class ApiMiddleware
{
    public function handle($request, Closure $next, $guard = null)
    {
		$token = "";
        $header = $request->header('Authorization', '');
		if (Str::startsWith($header, 'Bearer '))
		{
				$token = Str::substr($header, 7);
		}
        
        if($token == "")
		{
            // Unauthorized response if token not there
			return response()->json([ "success" => false, 'message' => 'Token not provided.'], 401);
        }
		
        if($token != "AdlgkndfkjHYGjuyJHBcfAJKFb")
		{
			return response()->json([ "success" => false, 'message' => 'Invalid token'], 401);
		}
        return $next($request);
    }
}