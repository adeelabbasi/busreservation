<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Spatie\Permission\Exceptions\PermissionDoesNotExist;

class RoleAndPermissionMiddleware
{
    public function handle($request, Closure $next, $roleOrPermission, $guard)
    {
        if (Auth::guard($guard)->guest()) {
            throw UnauthorizedException::notLoggedIn();
        }

        $rolesOrPermissions = is_array($roleOrPermission)
            ? $roleOrPermission
            : explode('|', $roleOrPermission);

        try {
            /*if (! Auth::guard($guard)->user()->hasAnyRole($rolesOrPermissions[0]) || ! Auth::guard($guard)->user()->can($rolesOrPermissions[1])) {
                throw UnauthorizedException::forRolesOrPermissions($rolesOrPermissions);
            }*/
			if (! Auth::guard($guard)->user()->can($rolesOrPermissions[1])) {
                throw UnauthorizedException::forRolesOrPermissions($rolesOrPermissions);
            }
        } catch (PermissionDoesNotExist $exception) {
        }

        return $next($request);
    }
}
