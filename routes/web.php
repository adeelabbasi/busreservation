<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/delsession/{msisdn}', function ($msisdn) {
    DB::Table('ussd_menu_session')->Where('msisdn', $msisdn)->Delete();
	Return "Done";
});

Route::get('/ussd.php', 'Ussd\UssdController@index');
Route::get('/ussd_company.php', 'Ussd\UssdController@index');

Route::get('/sms.php', 'Sms\SmsController@index');

Auth::routes();

Route::group(['prefix' => 'admin'], function () {
  Route::get('/login', 'AdminAuth\LoginController@showLoginForm');
  Route::post('/login', 'AdminAuth\LoginController@login');
  Route::post('/logout', 'AdminAuth\LoginController@logout');

  Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm');
  Route::post('/register', 'AdminAuth\RegisterController@register');

  Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail');
  Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset');
  Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm');
  Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
});

$cmsRoutes =  function () {
	
	Route::get('/chart/{report}/{type}', 'Cms\HomeController@chartReport');
	Route::get('/export/{report}/{type}', 'Cms\HomeController@exportMsisdn');
	Route::get('/dashboard', 'Cms\HomeController@dashboard');
	Route::resource('/', 'Cms\HomeController');
	
	Route::resource('/profile', 'Cms\ProfileController');
	
	Route::get('/company/list', 'Cms\CompanyController@getCompaniesList');
	Route::post('/company/sequence/{id}', 'Cms\CompanyController@updateSequence');
	Route::get('/company/grid', 'Cms\CompanyController@grid');
	Route::resource('/company', 'Cms\CompanyController');
	
	Route::get('company/bus/{company_id}', 'Cms\BusController@getCompanyBus');
	Route::get('/bus/grid', 'Cms\BusController@grid');
	Route::resource('/bus', 'Cms\BusController');
	
	Route::get('company/service/{company_id}', 'Cms\ServiceController@getCompanyService');
	Route::get('/service/grid', 'Cms\ServiceController@grid');
	Route::resource('/service', 'Cms\ServiceController');
	
	Route::post('/terminal/sequence/{id}', 'Cms\TerminalController@updateSequence');
	Route::get('company/city_terminal/{company_id}', 'Cms\TerminalController@getCompanyCityTerminal');
	Route::get('company/terminal/{company_id}', 'Cms\TerminalController@getCompanyTerminal');
	Route::get('/terminal/grid', 'Cms\TerminalController@grid');
	Route::resource('/terminal', 'Cms\TerminalController');
	
	Route::get('company/route_detail/{route_id}', 'Cms\RouteController@getCompanyRouteDetail');
	Route::get('company/route/{company_id}', 'Cms\RouteController@getCompanyRoute');
	Route::get('/route/grid', 'Cms\RouteController@grid');
	Route::resource('/route', 'Cms\RouteController');
	
	Route::get('company/schedule/{company_id}', 'Cms\ScheduleController@getCompanySchedule');
	Route::get('/schedule/{id}/copy', 'Cms\ScheduleController@copySchedule');
	Route::get('/schedule/grid', 'Cms\ScheduleController@grid');
	Route::resource('/schedule', 'Cms\ScheduleController');
	
	Route::get('company/schedule/fare/{company_id}', 'Cms\FareController@getCompanyScheduleFare');
	Route::get('company/fare/{company_id}', 'Cms\FareController@getCompanyFare');
	Route::get('/fare/grid', 'Cms\FareController@grid');
	Route::resource('/fare', 'Cms\FareController');
	
	Route::get('company/fare_type/{company_id}', 'Cms\FareController@getCompanyFare');
	Route::resource('/fare_type', 'Cms\FareTypeController');
	
	Route::get('/country/grid', 'Cms\CountryController@grid');
	Route::resource('/country', 'Cms\CountryController');
	
	Route::post('/city/sequence/{id}', 'Cms\CityController@updateSequence');
	Route::get('/city/grid', 'Cms\CityController@grid');
	Route::get('/country_city/{country_id}', 'Cms\CityController@countryCity');
	Route::resource('/city', 'Cms\CityController');
	
	Route::get('/lga/grid', 'Cms\LgaController@grid');
	Route::get('/city_lga/{state_id}', 'Cms\LgaController@cityLga');
	Route::resource('/lga', 'Cms\LgaController');
	
	Route::get('/userloggrid', 'Cms\UserController@gridLog');
	Route::get('/userlog', 'Cms\UserController@showUserLogs');
	
	Route::get('/user/grid', 'Cms\UserController@grid');
	Route::resource('/user', 'Cms\UserController');
	
	Route::get('/role/user/{type}', 'Cms\RoleController@getUserTypeRole');
	Route::get('/role/grid', 'Cms\RoleController@grid');
	Route::resource('/role', 'Cms\RoleController');
	
	Route::get('/deal/grid', 'Cms\DealController@grid');
	Route::resource('/deal', 'Cms\DealController');
	
	Route::get('/policy/grid', 'Cms\PolicyController@grid');
	Route::resource('/policy', 'Cms\PolicyController');
	
	Route::get('/about/grid', 'Cms\AboutController@grid');
	Route::resource('/about', 'Cms\AboutController');
	
	Route::get('/cservice/grid', 'Cms\CserviceController@grid');
	Route::resource('/cservice', 'Cms\CserviceController');
	
	Route::get('/customerloggrid', 'Cms\CustomerController@gridLog');
	Route::get('/customerlog', 'Cms\CustomerController@showCustomerLogs');
	Route::get('/customer/grid', 'Cms\CustomerController@grid');
	Route::resource('/customer', 'Cms\CustomerController');
	
	Route::get('/subscription/grid', 'Cms\SubscriptionController@grid');
	Route::resource('/subscription', 'Cms\SubscriptionController');
	
	Route::get('/booking/grid', 'Cms\BookingController@grid');
	Route::resource('/booking', 'Cms\BookingController');
	
	Route::get('/payment/grid', 'Cms\PaymentController@grid');
	Route::resource('/payment', 'Cms\PaymentController');
};
Route::group(['prefix' => 'admin', 'middleware' => 'admin'], $cmsRoutes);
Route::group(['prefix' => 'company', 'middleware' => 'auth'], $cmsRoutes);